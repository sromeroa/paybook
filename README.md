

#Lineamientos codificación frontend

Librerías a Utilizar

- JQuery
- AngularJS


Se utilizará una arquitectura horizontal de módulos, es decir, los modulos serán por paquete de funcionalidades en cada módulo se integraran los controladores, filtros, directivas etc.

Modulos Propuestos:

- OordenBase
- Usuarios
- Organizaciones
- Operaciones

(pueden definirse más modulos adelante)

Para código se definen las siguietes políticas
 - Es preferible utilizar codigo (creado por nosotros) de angular que plugins de Angular
 - Es preferible utilizar plugins de Angular que plugins de JQuery/JS


Todo lo que sea utilizable a lo largo de todo el proyecto cae en OordenBase
Lo demas va en los modulos que corresponda

##Arquitectura de carpetas

    frontend/
        src/
            common/
                00-start.js
                10-injector.js
                99-end.js

            modulos/
                base/
                    controllers
                    directives
                    services
                    filters
                    base.src.js

                usuarios/
                    controllers
                    directives
                    services
                    filters
                    usuarios.src.js

                organizaciones/
                    controllers
                    directivesç
                    services
                    filters
                    organizaciones.src.js

                operaciones/
                    controllers
                    directives
                    services
                    filters
                    operaciones.src.js

        out/
            oordenBase.js
            oordenUsuarios.js
            oordenOrganizaciones.js
            oordenOperaciones.js
            oorden.js

            min/
                oordenBase.js
                oordenUsuarios.js
                oordenOperaciones.js

##Cómo utilizar

Primero que nada necesitamos nodejs instalado (esto puede ser y sugiero que sea en la máquina host).

```bash

#supongamos que el folder está montado en /mnt/oorden

#instalamos gulp que es el manejador de tareas
sudo npm install --global gulp

#accedemos al folder
cd /mnt/oorden/oorden-web

#instalamos las demas dependecias
npm install

#dejamos corriendo el servicio 'gulp' mientras desarrollamos
#el servicio se pondra a observar los archivos y cuando cambien
#generarrá los concatenados y compilados
gulp


```
