window.TablaConsulta = TablaConsulta;
window.MtTable = MtTable;

function TablaConsulta (params) {
    var columnas = m.prop(params.columnas);
    var include  = m.prop(params.include || '');
    var defCol   = params.defCol || {};
    var items    = m.prop();

    return {
        view : view,
        controller : controller,
        getItems : getItems,
    };

    function getItems (consulta) {
        var qParams  = angular.extend({
            modelo : params.modeloConsulta,
            include : include()
        }, consulta ,params.parametros);

        return m.request({
            method:'GET',
            url:Modelo.apiPrefix() + '?' + $.param(qParams),
            unwrapSuccess : function (d) {
                return d.data.map(function (i) {
                    i.checked = m.prop(false);
                    return i;
                });
            }
        }).then(items);
    }

    function controller () {
        var self     = this;

        self.columnas = columnas;
        self.items = items;
        self.getItems = getItems;

        self.getValue = function(item, colName) {
            var columna = defCol[colName];

            if(columna && (typeof columna.value === 'function')){
                return columna.value.call(self, item);
            }

            if(typeof item[colName] === 'function') {
                return item[colName];
            }

            return item[colName];
        };

        self.getTitle = function (colName) {
             var columna = defCol[colName];

            if(columna && (typeof columna.caption != 'undefined')) {
                return (typeof columna.caption == 'function') ? columna.caption.call(self) : columna.caption;
            }

            return colName;
        };


        self.getHeading = function (colName) {
            var columna = defCol[colName];
            var title = self.getTitle(colName);

            console.log(columna);

            if(columna.sortable === false) return title;

            return  [
                title, m('i.ion-arrow-up-b')
            ]
        }

        //m.startComputation();
    }

    function view (ctrl, params) {
        return m('div.table-responsive', [
            m('table.table', [
                m('thead', tHead(ctrl)),
                m('tbody', ctrl.items() && tBody(ctrl))
            ])
        ]);
    }

    function tBody (ctrl) {
        return ctrl.items().map(function (item) {
            return m('tr', {'class' : item.checked() ? 'info' : ' '},
                ctrl.columnas().map(function (columna) {
                    return m('td'.concat('.cell-', columna) , ctrl.getValue(item,columna) );
                })
            );
        });
    }

    function tHead (ctrl) {
        return m('tr', [
            ctrl.columnas().map(function (columna) {
                return m('th'.concat('.cell-', columna), ctrl.getHeading(columna));
            })
        ]);

    }
}



function MtTable (args) {
    var columnas  = m.prop(args.columnas || []);
    var items     = m.prop(args.items || []);
    var defCol    = args.defCol;

    view.items    = items;
    view.columnas = columnas;

    view.tFoot = function () {};
    view.tBody = tBody;
    view.tHead = tHead;
    view.sortColumn  = m.prop(null);
    view.sortOrder  = m.prop(null);

    view.itemTrTag = function () {
        return 'tr'
    };

    view.itemTrParams = function () {
        return null
    };

    view.sort = function (colName, order) {
        if(view.sortColumn() == colName && view.sortOrder() == order) return;
        view.sortColumn(colName);
        view.sortOrder(order);

        var factor = order == 'asc' ? 1 : -1;

        view.items().sort(function (a,b) {
            var result = 0;
            var aVal = view.getSortValue(a,colName);
            var bVal = view.getSortValue(b,colName);

            if(aVal > bVal) {
                result = 1;
            } else if(bVal > aVal){
                result = -1;
            }

            return result * factor;
        });
    };


    view.getSortValue = function (item, colName) {
        var sortVal = view.getRawValue(item, colName);
        console.log(sortVal);
        
        if(typeof sortVal === 'undefined') sortVal = view.getValue(item,colName);
        if(typeof sortVal === 'string') return sortVal.latinize().toLowerCase();
        return sortVal;
    };


    view.getValue = function(item, colName) {
        var columna = defCol[colName];
        if(columna && (typeof columna.value === 'function')){
            return columna.value.call(self, item);
        }
        return view.getRawValue(item, colName);
    };

    view.getRawValue = function (item, colName) {
        if(typeof item[colName] === 'function') {
            return item[colName]();
        }
        return item[colName];
    };

    view.getTitle = function (colName) {
        var columna = defCol[colName];
        if(columna && (typeof columna.caption != 'undefined')) {
            return (typeof columna.caption == 'function') ? columna.caption.call() : columna.caption;
        }
        return colName;
    };


    view.getHeading = function (colName) {
        var columna = defCol[colName];
        var title = view.getTitle(colName);
        var isSortable = (!columna || typeof columna.sortable == 'undefined')? true : Boolean(columna.sortable);
        var sorted = view.sortColumn() === colName;
        var order = view.sortOrder ? '.'.concat(view.sortOrder()) : '';

        return  m('div'.concat(isSortable ? '.sortable' : '', sorted ? '.sorted' : '', order), [
            title,
            ' ',
            isSortable ? m('i.ion-arrow-up-b.arrow-sort.asc', {
                onclick:view.sort.bind(null, colName, 'asc')
            }) : '',

            isSortable ? m('i.ion-arrow-down-b.arrow-sort.desc', {
                onclick:view.sort.bind(null,colName ,'desc')
            }) : ''
        ]);
    };

    view.colHeader = function (colName) {
        return m('th.cell-'.concat(colName), view.getHeading(colName));
    };

    function view ( params) {
        return m('div.table-responsive', params, [
            m('table.table', [
                m('thead', view.tHead()),
                m('tbody', items() && view.tBody()),
                m('tfoot', view.tFoot() )
            ])
        ]);
    }

    function tBody () {
        return items().map(function (item) {
            return m(view.itemTrTag(item), view.itemTrParams(item),
                columnas().map(function (columna) {
                    return m('td'.concat('.cell-', columna), [
                        view.getValue(item,columna)
                    ]);
                })
            );
        });
    }

    function tHead () {
        return m('tr', [
            columnas().map(view.colHeader)
        ]);
    }

    return view;
}
