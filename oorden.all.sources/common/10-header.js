/**
 * Aqui se configura el injector para mas ejemplos de uso ver cada uno de los módulos
 */
var DIRECTIVE  = 'directive';
var CONTROLLER = 'controller';
var FACTORY    = 'factory';
var SERVICE    = 'service';
var PROVIDER   = 'provider';
var FILTER   = 'filter';
var INJECT     = '$inject';

var VALUE      = 'value';
var CONSTANT   = 'constant';

var RUN        = 'run';
var CONFIG     = 'config';


function createInjector () 
{
    var items = [];

    function inject (type, name, deps, fn) 
    {
        if(!type || !name) 
        {
            console.log('error on dependency');
        }

        items.push({
            type : type,
            name : name,
            fn   : (fn || angular.noop),
            deps : (deps || new Array())
        }); 
    };

    inject.mount = function (module) 
    {
        items.forEach(function(item)
        {
            if(item.type != VALUE && item.type != CONSTANT) 
            {
                item.fn[INJECT] = item.deps;
            }

            
            if(item.type == RUN || item.type == CONFIG) 
            {
                module[item.type](item.fn);
            } 
            else
            {
                module[item.type](item.name,item.fn);
            }
        });
    };

    return inject;
}