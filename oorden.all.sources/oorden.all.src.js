import 'common/00-start.js';

import './TablaConsulta.js';

//INCLUIMOS LOS HEADERS NECESARIOS (HELPER PARA INYECCIONES)
import 'common/10-header.js';

import 'modules/base/base.src.js';

//import 'modules/oorden-usuarios/oorden-usuarios.src.js';

import 'modules/organizaciones/organizaciones.src.js';

import 'modules/operaciones/oorden-operaciones.src.js';

angular.module('OordenWeb', ['oordenBase', 'oordenOrganizaciones', 'oordenOperaciones', 'oorden-web']);


/** 
 * MESSAGE API
 */
angular.module('OordenWeb').run([
    '$rootScope',
    '$compile',
    function ($rootScope, $compile) {
        $rootScope.__compiler__ = $compile;
    }
]);

import 'common/99-end.js';


