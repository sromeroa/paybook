inject(DIRECTIVE, 'oorTercero', ['Tercero'], function (Tercero) {
    return {
        scope : {
            tercero : '=oorTercero'
        },
        templateUrl : '/partials/terceros/oor-tercero.html',
        link : link,
        controller : ['$scope','oorden',oorTerceroCtrl],
        controllerAs : 'tCtrl'
    };

    function link () {

    }

    function oorTerceroCtrl ($scope, oorden) {
        var tCtrl = this;

        oorden.inicializar().then(function () {
            $scope.$watch('tercero', function (tercero) {
                if(!tercero) return;
                initializetercero();
            });
        });

        function initializetercero () {
            tCtrl.tipoDeCambio = oorden.tipoDeCambio.base;
        }

        $scope.guardarTercero = function () {
            if($scope.t3Form.$valid == false) {
                toastr.error('Faltan campos');
                return;
            }

            var tercero = angular.extend({}, $scope.tercero, {
                direccion:$scope.direccion
            });

            Tercero.guardar(tercero, $scope.direccion).then(function (t3) {
                $scope.$emit('$tercero', t3);
            });
        };

        $scope.$on('guardarTercero', function () {
            $scope.guardarTercero();
        })
        

    }
})