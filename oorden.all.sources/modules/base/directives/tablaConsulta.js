inject(DIRECTIVE, 'tablaConsulta', [], function () {
    return {
        scope : true,
        link : link,
        terminal : true
    }

    function link (scope, element, attrs) {

        var startWatch = scope.$watch(attrs.defCol, function (cols) {
            if(!cols) return;

            startWatch();

            var tabla = TablaConsulta({
                parametros : scope.$eval(attrs.parametros),
                modeloConsulta : attrs.modeloConsulta,
                include: 'operaciones.tipoDocumento,operaciones.tercero',
                columnas : ['checkbox','fecha','documento','tercero','referencia', 'total','estatus'],
                defCol : scope.$eval(attrs.defCol)
            });

            m.mount(element[0], tabla);

            scope.$watch(attrs.consulta, function (consulta) {
                tabla.getItems(consulta);
                tabla.ready();
            });

        });
       
    }
})