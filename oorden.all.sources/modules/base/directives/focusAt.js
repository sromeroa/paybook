inject(DIRECTIVE, 'focusAt', [], function () {

    return function (scope, element, attrs) {
        
        scope.$watch(attrs.focusAt, function (focus) {
            if(!focus) return;
            setFocus();
        });

        function setFocus () {
            scope.$evalAsync(function () {
                setTimeout(function () {
                    element[0].focus();
                },100);
            });
        };
    };

});