inject(DIRECTIVE,'selectTwo', ['oorden','$parse'], function (oorden, $parse) {
     var selectores = {
        cuentas : cuentasSelector,
        impuestos : impuestosSelector,
        retenciones : retencionesSelector
    };

    return {
        link : link,
        template : '<select></select>',
        terminal:true
    }

    function link (scope, el, attrs) {
        var selElement  = $('select', el)[0];
        var selector    = selectores[attrs.selectTwo](modelFn, onchange);
        var selected    = null;
        var selectedKey = null;
        var parser      = $parse(attrs.selectTwoSelected);
        var ngChange    = attrs.onChange ? $parse(attrs.onChange) : angular.noop;

        selector.config(selElement, false);

        scope.$watch(attrs.selectTwoSelected, function (val) {
            if(val) {
                selected    = val;
                selectedKey = selected[selector.key];

                if(selectedKey != selector.element().select2('val')) {
                    selector.element().select2('val', selectedKey);
                }
            } else {
                selector.element().select2('val', null);
            }
        });
        
        function modelFn () {
            if(arguments.length) {
                selectedKey = arguments[0];
            }
            return selectedKey ? selectedKey : null;
        }

        function onchange () {
            selected = selector.selected();
            parser.assign(scope, selected);
            ngChange(scope);
            console.log(scope.$$phase);
            scope.$$phase || scope.$apply();
        }
    }


    function cuentasSelector (modelFn, onchange) {
        var cuentas = oorden.cuentasContables.map(function (s) { return s; });
        return oor.cuentasSelect2({
            cuentas : cuentas,
            model : modelFn,
            key : 'cuenta_contable_id',
            onchange : onchange
        });
    }

    function impuestosSelector (modelFn, onchange) {
        return oor.impuestosSelect2({
            impuestos : oorden.impuestos,
            model : modelFn,
            key : 'impuesto_conjunto_id',
            onchange : onchange
        });
    }

    function retencionesSelector (modelFn, onchange) {
        return oor.retencionesSelect2({
            retenciones :oorden.retenciones,
            model : modelFn,
            key : 'retencion_conjunto_id',
            onchange : onchange
        });
    }

    function centrosDeCostoSelector (modelFn, oncahnge) {
        return oor.retencionesSelect2({
            
        })
    }
});