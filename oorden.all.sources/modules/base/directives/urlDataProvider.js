inject(DIRECTIVE, 'urlDataProvider', ['$parse'], function ($parse) {
    'use strict';

    var name = 'urlDataProvider';

    return {
        require : name,
        controller : ['$scope', '$http', '$attrs', '$timeout', urlDataProviderCtrl],
        link : link
    };

    function link (scope, element, attrs, ctrl) {
        ctrl.name = attrs.providerName || name;
        $parse(ctrl.name).assign(scope, ctrl);
    }



    function urlDataProviderCtrl ($scope, $http, $attrs, $timeout) {
        var ctrl     = this;
        var autoLoad = angular.isDefined($attrs.autoLoad);

        ctrl.load = (function () {
            var t;
            return function () {
                if(t) $timeout.cancel(t);
                t = $timeout(load,100);
            }
        })();

        $scope.$watch(watchUrl, function (url) {
            ctrl.url = url;
            if(autoLoad) ctrl.load();
        });

        if(autoLoad && angular.isDefined($attrs.providerParams)) {
            $scope.$watch($attrs.providerParams, function () {
                 ctrl.load();
            }, true);
        }


        function watchUrl () { return $attrs.providerUrl };

        function getParams () {
            return $parse($attrs.providerParams)($scope);
        };

        function load () {
            ctrl.busy = true;

            $http.get(ctrl.url, {params : getParams()}).success(function (d) {
                ctrl.data = d;
                ctrl.busy = false;
                $parse($attrs.onLoad)($scope);
            });
        };
    }




});