inject(DIRECTIVE, 'adjuntarDocumentos', [], function () {
    
    return {
        templateUrl : '/partials/base/adjuntar-documentos.html',
        scope : {
            nombreRecurso : '@',
            idRecurso : '@',
            idOrganizacion : '@'
        },
        link : link,
        controller : ['$scope', '$http', adjuntarDocumentosCtrl]
    };

    function link (scope, element, attrs) {

        scope.$watch(function () {
            if(!scope.nombreRecurso || !scope.idRecurso || !scope.idOrganizacion) return false;
            return scope.idOrganizacion + '/' + scope.nombreRecurso + 's/' + scope.idRecurso; 
        }, function (n) {
            console.log(n);
            if(!n) return;
            scope.inicializar(n);
        });

        var iframe = $('iframe', element);

    
        scope.iframe = iframe;

        iframe.bind('load', function () {
            var d;
            try{
                d = iframe[0].contentWindow.location;
                if(d.pathname.indexOf('/api/documento/') == 0) {
                    scope.recargarDocumentos();
                }
            } catch (e) {
                if(scope.ruta) scope.recargarDocumentos();
            }
        });
    }


    function adjuntarDocumentosCtrl ($scope, $http) {

        $scope.inicializar = function (d) {
            $scope.ruta = d;
            $scope.postUrl = d;
            $scope.formUrl = '/documentos/adjuntar?postUrl=' + $scope.postUrl;
            $scope.recargarDocumentos();
        };

        $scope.recargarDocumentos = function () {
            $http.get('/api/documentos/' + $scope.ruta).success(function (r) {
                $scope.documentos = r.data ? r.data.filter(function (d) { return d.type == 'file' }) : [];
                $scope.documentos.sort(function (d, b) {
                    return d.timestamp - b.timestamp;
                });
            });

            $scope.iframe.attr('src', $scope.formUrl);
        }

        $scope.eliminarDocumento = function (path) {
            $http.delete('/api/borrardocumento?path='.concat(path)).success(function () {
                $scope.recargarDocumentos();
            });
        }
    }
})