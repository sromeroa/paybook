inject(DIRECTIVE, 'oorAutocomplete', ['$q', '$http', '$compile', 'Organizacion'], function ($q, $http, $compile, Organizacion) {

    var types = {

        'cuentasContables' : {
            pk : 'cuenta_contable_id',
            controller : controller_cuentasContables,
            name : function (c) { return c.cuenta + ' ' + c.nombre}
        },

        'elementosDeCosto' : {
            pk : 'elemento_ccosto_id', 
            controller: controller_elementosDeCosto,
            name  : 'elemento'
        },

        'impuestos'        : {
            pk : 'impuesto_conjunto_id',
           // controller : controller_impuestos,
            name : 'impuesto_conjunto'
        }, 

        'retenciones'      : {
            pk : 'retencion_conjunto_id',
            //controller : controller_retenciones,
            name : 'retencion_conjunto'
        },


    };

    //terceros, cliente, proveedor, es="cliente|provedor"

    return {
        scope      : true,
        template   : '<md-autocomplete md-items="item in ac.search()" md-item-text="ac.caption(item)" md-selected-item="ac.selected" md-search-text="ac.searchText"> {{ ac.caption(item) }} </md-autocomplete>',
        controller : ['$scope','$attrs',controller],
        require    : 'ngModel',
        link       : link,
        terminal : true,
        controllerAs : 'ac'
    };



    function link (scope, element, attrs, ngModel) {
        //Algo le pone el tab index, se quita
        //para que no sea seleccionable este nodo
        setTimeout(function () {
            element.removeAttr('tabindex')
         },200);

       
        var rmReady = scope.$watch('ac.$ready', function (r) {
            if(!r) return;

            rmReady();

            ngModel.$render = function () {
                scope.ac.setValue(ngModel.$viewValue);
            };

            $compile(element.contents())(scope);
            ngModel.$render();

        });
        

        scope.$watch(scope.ac.selectedKey() , function (selected) {
            if(!selected) return;
            ngModel.$setViewValue(selected);
        });

        if(angular.isDefined(attrs.label)) {
            $('md-autocomplete',element).attr('md-floating-label', attrs.label);
        }
    }
    

    function controller ($scope, $attrs) {
        var ac   = this;
        var type = $attrs.oorAutocomplete;
        var typeDef = types[$attrs.oorAutocomplete];

        ac.items = [];

        if(!typeDef) 
        {
            throw 'oorAutocomplete, el tipo no está configurado ' +  type;
        }



        ac.search = function () {
            return ac.items.filter( ac.createQueryFilter(ac.searchText) );
        };

        ac.caption = function (item) {
            if(!item) return ' ';

            if(angular.isFunction(typeDef.name)) {
                return typeDef.name(item);
            }

            return item[typeDef.name || typeDef.pk];
        };

        ac.setValue = function (value) {
            if(!value) return;

            if(!ac.$ready) {
                ac.renderValue = value;
                return;
            }

            ac.renderValue = null;

            ac.items.forEach(function (i) {
                if(i[typeDef.pk] == value) {
                    ac.selected = i;
                }
            });
        };

        var filters = {};

        ac.createQueryFilter = function (search) {
            var text = angular.lowercase(search);
            
            if(!filters[text]) {
                filters[text] =  function (item) {
                   return angular.lowercase(ac.caption(item)).indexOf(text) > -1;
                }
            }

            return filters[text];
        };

        ac.selectedKey = function () {
            return 'ac.selected.' + typeDef.pk;
        };

        if(typeDef.controller) {
           typeDef.controller.call(this, $scope, $attrs);
        }
    }


    function controller_elementosDeCosto ($scope, $attrs) {
        var ac = this;

        Organizacion.centrosDeCosto().then(function (cc) {
            var rm = $scope.$watch($attrs.centroCosto, function (centro) {
                if(!centro) return;
                ac.$ready = true;
                rm();

                if(cc.centro[centro]) {
                    ac.items = cc.centro[centro].elementos;
                }
                else {
                    ac.items = [];
                }
                
            });
        });

    }

    function controller_cuentasContables ($scope, $attrs) {
        var ac = this;

        function esDetalle (cc) {
            return Number(cc.acumulativa) === 0;
        }

        ac.search = function () {
            return ac.items
                     .filter( esDetalle )
                     .filter( ac.createQueryFilter(ac.searchText) );
        };

        Organizacion.cuentasContables().then(function (cuentas) {
            ac.items  = cuentas;
            ac.$ready = true;
        });
    }

    
});



