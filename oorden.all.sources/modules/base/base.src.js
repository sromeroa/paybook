/* MODULO BASE */
(function () {

var module = angular.module('oordenBase', ['ngMaterial']);
var inject = createInjector();


import './services/oorden.js';
import './services/uSetting.js';
import './services/uuid.js';
import './services/fecha.js';

inject(DIRECTIVE, 'niceNumberInput',['$parse'], function ($parse) {
    var matcher = /[^0-9\.]/g;
    
    function dotReplacer () {
        var times = 0;
        return function () {
            if(times++ == 0) return '.';
            return '';
        }
    }

    return function (scope, element, attrs) {
        element.bind('blur', function () {
            var val = ($(this).val() || '');

            val = val.replace(/\./g, dotReplacer());
            val = Number(val.replace(matcher, ''));

            if(!angular.isUndefined(attrs.decimales)){
                val = val.toFixed(attrs.decimales);
            }

            $parse(attrs.ngModel).assign(scope, val);
            scope.$apply();
        });
    }
});

inject(FILTER, 'contable', ['numberFilter'], function (nf)  {
    return function (d) {
        if(d === null) return '----';
        return nf(d,2);
    }
});


inject(FILTER, 'fechaFromTime', ['fecha'], function (fecha) {
    var cache = {};

    return function (t) {
        if(!cache[t]) {
            cache[t] = fecha.str(new Date(t*1000));   
        }
        return cache[t];
    }
});


import './directives/focusAt.js';
import './directives/oorAutocomplete.js';
import './directives/adjuntarDocumentos.js';
import './directives/urlDataProvider.js';
import './directives/oorTercero.js';
import './directives/oorDireccion.js';
import './directives/tablaConsulta.js';

import './directives/select2Directive.js';


inject.mount(module);
})();
/* FIN MODULO BASE */ 
