inject(FACTORY, 'oorden', ['$q', '$http'], function ($q,$http) {

    return new Oorden();

    function Oorden () {
        var oor = this;
        oor.dNum = dNumber;

        /** 
         * Regresa un tipo de cambio
         */
        var tiposDeCambio = [];
        var tiposDeCambioMap = {}; 
        var getTipoDeCambio = createGetter(tiposDeCambioMap);

        var centrosDeCosto = [];
        var centrosDeCostoActivos = [];
        var centrosDeCostoMap = {};
        var getCentroDeCosto = createGetter(centrosDeCostoMap);

        var elementosDeCosto    = [];
        var elementosDeCostoMap = {};
        var getElementoDeCosto = createGetter(elementosDeCostoMap);

        var cuentasContables = [];
        var cuentasContablesMap = {};
        var getCuentaContable = createGetter(cuentasContablesMap);


        var retenciones = [];
        var retencionesMap = {};

        var impuestos = [];
        var impuestosMap = {};


        var metodosDePago = [];
        var metodosDePagoMap = {};
        var getMetodoDePago = createGetter(metodosDePagoMap);

        
        function centroDeCosto (id) {
            var ccto = getCentroDeCosto(id);
            if(ccto) return ccto;
            else return getCentroDeCosto(getElementoDeCosto(id).centro_de_costo_id);
        };

        oor.metodosDePago = metodosDePago;
        oor.metodoDePago = getMetodoDePago;

        oor.retenciones = retenciones;
        oor.retencion = createGetter(retencionesMap);


        oor.impuestos = impuestos;
        oor.impuesto = createGetter(impuestosMap);


        oor.centroDeCosto   = centroDeCosto;
        oor.centrosDeCosto  = centrosDeCosto;
        oor.centrosDeCostoActivos = centrosDeCostoActivos;
        oor.elementoDeCosto = getElementoDeCosto;
        oor.elementosDeCosto = elementosDeCosto;

        oor.cuentaContable  = getCuentaContable;
        oor.cuentasContables = cuentasContables;

        oor.tipoDeCambio    = getTipoDeCambio;
        oor.tiposDeCambio   = tiposDeCambio;

        oor.obtenerTasa  = obtenerTasa;

        var $ready = false;
        oor.ready = function () { return $ready };

        var defer;

        oor.inicializar = function () {
            if(!defer) {
                defer = $q.defer();
                $http.get('/organizacion/config').success(function (r) {
                    inicializar(r);
                    $ready = true;
                    defer.resolve(true);
                });
            }
            return defer.promise;
        };


        function obtenerTasa (tipoDeCambio) {
            var defer = $q.defer();

            if(! Boolean(Number(tipoDeCambio.base)) ) {
                $http.get('/api/tasa/' + tipoDeCambio.tipo_de_cambio).success(function (r) {
                    defer.resolve(r.ultimaTasa);
                });
            } else {
                defer.resolve({tasa_de_cambio : '1.00000'})
            }

            return defer.promise;
        }

        function cifrasFn (org) {
            var nDecimales  = Number(org.numero_decimales) || 0;
            return function () {
                return nDecimales;
            }
        }


        //Inciializacion
        function inicializar (data) {
            window.OORDEN = oor;
            
            oor.organizacion = data.organizacion;
            oor.organizacion.cifras = cifrasFn(data.organizacion);

            oor.sucursal     = data.sucursal;
            oor.usuario      = data.usuario;
            
            inicializarTiposDeCambio(data.tiposDeCambio);
            inicializarCentrosDeCosto(data.centrosDeCosto);
            inicializarCuentasContables(data.cuentasContables);
            inicializarRetenciones(data.retenciones);
            inicializarImpuestos(data.impuestos);
            inicializarMetodosDePago(data.metodosDePago);
        }

        function inicializarMetodosDePago (data) {
            _inicializar({
                arr  : metodosDePago,
                map  : metodosDePagoMap,
                data : data,
                id   : 'id' 
            });
        }

        function inicializarImpuestos (data) {
            _inicializar({
                arr  : impuestos,
                map  : impuestosMap,
                data : data,
                id   : 'impuesto_conjunto_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarRetenciones (data) {
            _inicializar({
                arr  : retenciones,
                map  : retencionesMap,
                data : data,
                id   : 'retencion_conjunto_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarCuentasContables (data) {
            _inicializar({
                arr  : cuentasContables,
                map  : cuentasContablesMap,
                data : data,
                id   : 'cuenta_contable_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarTiposDeCambio (data) {
            _inicializar({
                arr  : tiposDeCambio,
                map  : tiposDeCambioMap,
                data : data,
                id   : 'tipo_de_cambio_id' 
            });

            tiposDeCambio.forEach(function (d) {
                getTipoDeCambio[d.tipo_de_cambio] = d;
                
                if(d.base == '1') {
                    getTipoDeCambio.base = d;
                }
            });
        }

        function inicializarCentrosDeCosto (data) {
             _inicializar({
                arr  : centrosDeCosto,
                map  : centrosDeCostoMap,
                data : data,
                id   : 'centro_de_costo_id' 
            });

             centrosDeCosto.forEach(function (cc) {
                cc.elementos.forEach(function (e) {
                    var elemento = angular.extend({}, e, {
                        id : e.elemento_ccosto_id
                    });
                    elementosDeCosto.push(elemento);
                    elementosDeCostoMap[elemento.id] = elemento;
                });
             });

             centrosDeCosto.forEach(function (cto) {
                if(cto.activo == "1") centrosDeCostoActivos.push(cto);
             });
        }

        function _inicializar (params) {
            params.data.forEach(function (d) {
                var item = angular.extend({}, d, {id : d[params.id]});
                params.arr.push(item);
                params.map[item.id] = item;
            });
        }
    };

    function prop (iVal) {
        var val = iVal;
        return function () {
            if(arguments.length) {
                val = arguments[0];
                return;
            }
            return val;
        }
    };

    function createGetter(map) {
        return function (id) {
            return map[id];
        }
    };


    function dNumber (decimales) {
        if (!(this instanceof dNumber)) return new dNumber(decimales);
        var value = 0;

        this.value = function () {
            if(!arguments.length) {
                return value;
            };

            value = Number.round(arguments[0]);
            return this;
        };

        this.asDecimal = function () {
            var str = String(value);
            return [
                str.substring(0, str.length - decimales),
                str.substring(st.length - decimales),
            ].join('.');
        };

        this.asFloat = function () {
            return Number.round(Number(this.asDecimal()));
        };

        this.fromString = function (str) {
            var arr = str.split('.');
            var ent = arr[0];
            var dec = comoDecimales(arr[1]);
            
            if(!dec) {
                dec = '';
            };

            dec = dec.substring(0, decimales);

            value = Number(ent.concat(dec));
        }

        function comoDecimales (d) {
            if(d.length == decimales) return d;

            if(d.length > decimales)  {
                return d.substring(0,decimales);
            }

            if(d.length < decimales) {
                return d.concat(String(Math.pow(10, decimales)).substring(1));
            }
        }

    };


});