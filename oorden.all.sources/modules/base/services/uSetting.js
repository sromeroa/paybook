inject(FACTORY, 'uSetting', ['$http', '$q'], function ($http, $q) {
    return {
        //get      : getSettings,
        get      : getSetting, 
        set      : setSettings,
        usuario  : usuarioSetting,
        sucursal : sucursalSetting
    };

    function setSettings (r1, r2, settings) {
        var defer = $q.defer();
        var url   = getUrl(r1,r2);

        $http.post(url, settings).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    }

    function getSetting (r1, r2, namespaces)
    {
        var defer = $q.defer();

        var url    = getUrl(r1,r2);
        var params = getParams(namespaces);

        $http.get(url, {params:params}).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;

    }

    function getParams (namespaces) {
        return { namespaces : angular.isArray(namespaces) ? namespaces.split(',') : namespaces};
    }



    function getUrl (r1, r2) {
        var url = '/u-settings/index/';

        url += angular.isArray(r1) ? r1.join(':') : String(r1);
       
        if(r2){
            url += '/';
            url += angular.isArray(r2) ? r2.join(':') : String(r2);
        }

      
        return url;
    }

    function getSettings (settings) {
        var defer = $q.defer();

        console.log('inpit-settings', settings, getUrlParams(settings));

        $http.get('/u-settings', {params :getUrlParams(settings)}).success(function (r) {
            defer.resolve(makeSettings(settings, r.data));
        });

        return defer.promise;
    }

    function makeSettings(iSettings, data) {
        var oSettings = {};

        iSettings.forEach(function (setting) {
            if(!oSettings[setting.type]) {
                oSettings[setting.type] = {};
            }
            oSettings[setting.type][setting.namespace] = {};
        });

        Object.keys(data).forEach(function (k) {
            var keys  = k.split('/');
            var sKeys = keys[2].split('.');
            oSettings[ keys[0] ][ sKeys[0] ][ sKeys[1] ] = data[k];
        });

        return oSettings;
    }


    function getUrlParams (settings) {
        return settings.map(function (setting) {
            if(angular.isString(setting)) return setting;
            var str = setting.type + '/' + setting[setting.type];
            str += (setting.namespace) ? '/' + setting.namespace : '';
            return str;
        }).reduce(function (obj, str, i) {
            obj['setting_' + i] = str;
            return obj;
        }, {});
    }

    function usuarioSetting (opts) {
        return createSetting(opts, {type : 'usuario'});
    }

    function sucursalSetting (opts) {
        return createSetting(opts, {type : 'sucursal'});
    }

    function createSetting (opts, defOpts) {
        return angular.extend({}, defOpts, opts);
    }
});