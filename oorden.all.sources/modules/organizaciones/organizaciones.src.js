/* MODULO ORGANIZACIONEs */
(function () {
var module = angular.module('oordenOrganizaciones', ['oordenBase']);
var inject = createInjector();


inject(FILTER, 'numeroCifras', ['$filter'], function ($filter) {
    var memo = {};
    var numberFilter = $filter('number');

    return function (numero, cifras) {
        return numberFilter(Number(numero) / getMult(cifras), cifras);
    };

    function getMult (cifras) {
        if(!memo[cifras]) {
            memo[cifras] = Math.pow(10, cifras);
        }

        return memo[cifras];
    };
})

import './services/models/Organizacion.js';
import './services/models/Poliza.js';
import './services/models/Operacion.js';
import './services/models/Tercero.js';

//import './services/models/Cliente.js';

import './services/dialogos.js';

import './controllers/crearOrganizacionCtrl.js';
import './controllers/TiposDeCambioCtrl.js';
import './controllers/CategoriasCtrl.js';
import './controllers/ProductosCtrl.js';

import './controllers/OorPolizasIndexCtrl.js';
import './controllers/OorOperacionesIndexCtrl.js';

inject.mount(module);
})();
/* FIN MODULO ORGANIZACIONEs */
