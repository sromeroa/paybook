inject(FACTORY, 'oorDialogoProducto', ['$mdDialog'], function ($mdDialog) {
   
    function oorDialogoProducto ($event) {
        var parentEl = angular.element(document.body);

        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/partials/productos/dialogo-producto.html',
            controller : ['$scope', '$mdDialog', oorDialogoProductoCtrl]
        });
    };

    function oorDialogoProductoCtrl ($scope, $mdDialog) {

        $scope.closeDialog  = function () {
            $mdDialog.hide({pro:'mesa'});
        }

    }


    return oorDialogoProducto;
}); 