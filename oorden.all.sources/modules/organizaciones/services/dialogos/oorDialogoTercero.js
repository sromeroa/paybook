inject(FACTORY, 'oorDialogoTercero', [ '$mdDialog', 'uuid', 'oorden'], function ($mdDialog,uuid,oorden) {
    
    return showDialog;

    function showDialog($event) {
        var parentEl = angular.element(document.body);
        
        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            template:
               '<md-dialog aria-label="List dialog">' +
               '  <md-content>'+
               '    <div oor-tercero="tercero" style="min-width:800px"></div>' +
               '  </md-content>' +
               '  <div class="md-actions">' +
               '    <md-button ng-click="closeDialog()">' +
               '      Cancelar' +
               '    </md-button>' +
               '    <md-button class="md-primary" ng-click="guardarTercero(t3Form)">' +
               '      Guardar' +
               '    </md-button>' +
               '  </div>' +
               '</md-dialog>',
            locals: {
            
            },
            controller: ['$scope','Tercero', AuxiliarDialogCtrl]
        });
    }

    function AuxiliarDialogCtrl (scope, Tercero) {
        scope.closeDialog = function() {
            $mdDialog.hide(null);
        };

        scope.tercero   = { 
            tercero_id : uuid(),
            es_cliente : showDialog.TERCERO_CLIENTE ? true : false,
            es_proveedor : showDialog.TERCERO_PROVEEDOR ? true : false
        };

        scope.direccion = {
            codigo_pais : oorden.organizacion.codigo_pais
        };

        scope.guardarTercero = function (t3Form) {
          console.log(t3Form);
          
            var tercero = angular.extend({}, scope.tercero, {direccion:scope.direccion});
            Tercero.guardar(tercero, scope.direccion).then(function (t3) {
                $mdDialog.hide(t3);
            });
        };
    }

});