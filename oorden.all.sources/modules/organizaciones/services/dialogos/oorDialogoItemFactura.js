inject(FACTORY, 'oorDialogoItemFactura', ['$mdDialog'], function ($mdDialog) {

    function oorDialogoItemFactura ($event, item, factura) {

        var parentEl = angular.element(document.body);

        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/partials/operaciones/dialogo-item-factura.html',
            locals : {
                iItem : item,
                factura : factura
            },
            controller : ['$scope', '$mdDialog', 'iItem', 'factura', oorDialogoItemFacturaCtrl]
        });

    }

    var fields = 'producto|concepto|cantidad|unidad|precioUnitario|descuento|importe|impuesto|retencion|cuenta|elementosDeCosto'.split('|');

    function oorDialogoItemFacturaCtrl ($scope, mdDialog, iItem, factura) {
        var item =  $scope.item =  {};
        $scope.factura = factura;

        angular.forEach(fields, function (key) {
            item[key] = iItem[key];
        });

        if(!item.elementosDeCosto) item.elementosDeCosto = [null, null];

        $scope.closeDialog  = function () {
            $mdDialog.hide(null);
        };

        $scope.aplicarCambios = function () {
            $mdDialog.hide($scope.item);
        };

        $scope.asignarProducto = function () {
            var item = $scope.item;
            var prod = item.producto;
            if(!prod) return;

            item.precioUnitario = prod.precio_unitario_venta;
            item.unidad = prod.unidad;
            item.concepto = prod.nombre;
        }

        var decimales = 4;
        var fDec = 1000;

        $scope.calcularImporte = function () {
            var item = $scope.item;
            var descuento = (100 - Number(item.descuento  || 0))/100;
            var importe   = Number(item.cantidad) * Number(item.precioUnitario) * descuento;

            importe = Math.round(fDec*importe)/fDec;
            $scope.item.importe = importe;
        };
    };


    return oorDialogoItemFactura;

});