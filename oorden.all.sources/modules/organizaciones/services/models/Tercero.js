inject(FACTORY, 'Tercero', ['$q', '$http', 'oorden', 'uuid', '$timeout'] , function ($q, $http, oorden, uuid, $timeout) {

    function Tercero () {
        if(!(this instanceof Tercero)) return new Tercero;
    };

    Tercero.obtener = function (id) {
        var defer = $q.defer();

        $http.get('/api/terceros/' + id).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    };

    
    Tercero.crear = function (datos) {
        var defer  = $q.defer();

        Tercero.preparar().then(function () {
            var tercero   = Tercero();

            angular.extend(tercero, {
                tercero_id : uuid(),
                $nuevo     : true
            }, datos);

            defer.resolve(tercero);
        });

        return defer.promise;
    };


    Tercero.preparar = (function () {
        var prepararDefer;

        return function () {
            if(!prepararDefer) {
                prepararDefer = $q.defer(); 
                oorden.inicializar().then(function () {
                   prepararDefer.resolve(true);
                });
            }
            return prepararDefer.promise;
        }
    })();


    Tercero.guardar = function (tercero, dir) {
        var defer = $q.defer();
        var direccion;

        if(dir) {
            direccion = angular.extend({}, dir, {
                direccion_id: uuid(), 
                pertenece_a_id : tercero.tercero_id,
                tipo_id : 3
            });
        }

        if(direccion) {
            guardarDireccion(direccion).then(function () {
                var t = angular.extend({}, tercero, {
                    ultima_direccion_fiscal : direccion.direccion_id
                });
                guardarTercero(t).then(resolve);
            });
        } else {
            guardarTercero(tercero).then(resolve);
        }

        function resolve (rTercero) { defer.resolve(rTercero); }
    
        return defer.promise;
    };


    Tercero.obtenerDirecciones = function (id) {
        var defer = $q.defer();

        $http.get('/api/direcciones/' +id).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    };


    function guardarTercero (tercero) {
        var defer = $q.defer();

        $http.post('/api/terceros/agregar', tercero).success(function (t) {
            defer.resolve(t.data);
        });

        return defer.promise;
    }


    function guardarDireccion (direccion) {
        var defer = $q.defer();

        $http.post('/api/direcciones/agregar', direccion).success(function (d) {
            defer.resolve(d.data);
        });

        return defer.promise;
    }

    return Tercero;
});