inject(FACTORY, 'Operacion', ['$q', '$rootScope', '$http', 'oorden', 'Organizacion', 'uuid', 'fecha'] , function ($q, $rootScope, $http, oorden, Organizacion, uuid, fecha) {


    Operacion.hayCopia = function () {
        return false;
    };

    // Obtener operacion
    Operacion.obtener = function (id) {
        var defer = $q.defer();
        $http.get('/api/operacion/' + id)
        .success(function (d) {
            defer.resolve({ operacion : d.data });
        })
        .error(function (d) {
            defer.reject(d);
        });

        return defer.promise;
    };

    // Obtener datos extra de la operación
    Operacion.obtenerDatosExtra = function (id) {
        var defer = $q.defer();

        $http.get('/api/operacionesextra/' + id).success(function (e) {
            defer.resolve(e);
        });

        return defer.promise;
    };
    

    Operacion.crear = function (datos) {
        var defer  = $q.defer();

        Operacion.preparar().then(function () {
            var operacion   = Operacion();
        
            angular.extend(operacion, {
                operacion_id : uuid(),
                items        : [],
                $new         : true
            }, datos);

            defer.resolve(operacion);
        });

        return defer.promise;
    };

    //Se agrega el item
    Operacion.prototype.agregarItem = function () {
        this.items.push({
            operacion_item_id : uuid()
        });
    };

    function Operacion () {
        if(!(this instanceof Operacion)) return new Operacion;
    };

    // Guardar Operacion
    Operacion.guardar = function (operacion) {
        var defer = $q.defer();

        $http.post('/api/operacion/agregar', operacion)
            .success(function (r) { defer.resolve(r); })
            .error(function (r) { defer.reject(r); });

        return defer.promise;
    };

    // Guardar Estatus de la operació 
    Operacion.estatus = function (operacion, estatus) {
        var defer = $q.defer();

        $http.post('/api/operacion/estado_operacion/' + operacion.operacion_id + '/' + estatus)
            .success(function (r) {
                defer.resolve(); 
            })
            .error(function (r) {
                defer.reject(r);
            })

        return defer.promise;
    };

    Operacion.preparar = (function () {
        var prepararDefer;

        var estatuses = [
            { estatus : 'P', nombre : 'En Preparacion', color : 'blue'},
            { estatus : 'T', nombre : 'Por Autorizar', color : 'amber'},
            { estatus : 'A', nombre : 'Autorizada', color : 'green'},
            { estatus : 'S', nombre : 'Saldada', color:'grey'},
            { estatus : 'X', nombre : 'Eliminada', color:'red'}
        ];

        estatuses.estatus = estatuses.reduce(function (r,e) {
            r[e.estatus] = e;
            return r;
        }, {});

        return function () {
            if(!prepararDefer) {
                prepararDefer = $q.defer(); 

                Operacion.estatuses = estatuses;

                oorden.inicializar().then(function () {
                    Operacion.centrosDeCosto   = oorden.centrosDeCosto;
                    Operacion.tiposDeCambio    = oorden.tiposDeCambio;
                    Operacion.cuentasContables = oorden.cuentasContables;
                    check();
                });

                
            }

            return prepararDefer.promise;
        }

        function check () {
            if(Boolean(Operacion.tiposDeCambio) && Boolean(Operacion.centrosDeCosto) && Boolean(Operacion.cuentasContables)){
                prepararDefer.resolve(true);
            }
        }

    })();

    return Operacion;
});