inject(FACTORY, 'Organizacion', ['$q', '$http', '$timeout'] , function ($q, $http, $timeout) {

    /**
     * Este modelo es provisional no va a functionar al final
     */
    var org = {
        centrosDeCosto    : getCentrosDeCosto(),
        cuentasContables  : getCuentasContables(),
        tiposDeCambio     : getTiposDeCambio(),
        impuestos : getImpuestos(),
        retenciones : getRetenciones(),
        sucursales : getSucursales(),
        vendedores : getVendeodres(),
         productos : getProductos(),
        configuracion     : config
    };

    org.terceros = getTerceros();

    return org;



    /**
     * Función falsa 
     */
    function config () {
        var defer = $q.defer();

        $timeout(function () {
            var config = getOrganizacionConfig();
            defer.resolve(config);
        },100);

        return defer.promise;
    };

    
    /**
     * Configuracion de la organizacion
     */
    function getOrganizacionConfig () {
        return {
            mesesContables : [
                {mes : '01', nombre : 'Enero',   mesContable : '01'},
                {mes : '02', nombre : 'Febrero', mesContable : '02'},
                {mes : '03', nombre : 'Marzo',   mesContable : '03'},
                {mes : '04', nombre : 'Abril',   mesContable : '04'},
                {mes : '05', nombre : 'Mayo',    mesContable : '05'},
                {mes : '06', nombre : 'Junio',   mesContable : '06'},

                {mes : '07', nombre : 'Julio',      mesContable : '07'},
                {mes : '08', nombre : 'Agosto',     mesContable : '08'},
                {mes : '09', nombre : 'Septiembre', mesContable : '09'},
                {mes : '10', nombre : 'Octubre',    mesContable : '10'},
                {mes : '11', nombre : 'Noviembre',  mesContable : '11'},
                {mes : '12', nombre : 'Diciembre',  mesContable : '12'},
                {mes : '13', nombre : 'Mes 13',  mesContable : '13'},
                {mes : '14', nombre : 'Mes 14',  mesContable : '14'},
            ]
        }
    };


    function getTiposDeCambio () {
        var defer;
        var tiposDeCambio = {
            tipos : [],
            base : null,
            porTipoDeCambio : function (tc) {
                var s = tiposDeCambio.tipos.filter(function (t) {
                    return t.tipo_de_cambio == tc;
                })[0];
                return s;
            }
        };

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();
                
                $http.get('/api/tiposdecambio').success(function (r) {
                    r.tipos.forEach(function (tipo) {
                        tiposDeCambio.tipos.push(tipo);
                        if(tipo.base == 1) tiposDeCambio.base = tipo;
                    });

                    defer.resolve(tiposDeCambio);
                });
            }

            return defer.promise;
        }
    }

    /** 
     * Centros De Costo
     */
    function getCentrosDeCosto () {
        var defer;

        var ccosto = {
            centros : [],
            elementos : [],
            centro : {},
            elemento : {}
        };

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/centrosdecosto').success(function (r) {

                    (r.centros || []).forEach(function (d) {
                        ccosto.centros.push(d);
                        ccosto.centro[d.centro_de_costo_id] = d;
                        d.elementos = [];
                    });


                    (r.elementos || []).forEach(function (d) {
                        ccosto.elemento[d.elemento_ccosto_id] = d;
                        if(ccosto.centro[d.centro_de_costo_id]){
                            ccosto.centro[d.centro_de_costo_id].elementos.push(d);
                        }
                    });

                    ready = true;

                    defer.resolve(ccosto);
                });

            }
            return defer.promise
        }
    };

    /**
     * Cuentas Contables
     */
    function getCuentasContables () {
        var defer;
        var cuentas = [];
         cuentas.cuenta = {};

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/json/cuentascontables').success(function (r) {

                    r.data.forEach(function (cuenta) {
                        cuentas.push(cuenta);
                        cuentas.cuenta[cuenta.cuenta_contable_id] = cuenta;
                    });

                    defer.resolve(cuentas);
                });

            }
            return defer.promise
        }
    }


    /** 
     * Terceros
     */

    function getTerceros () {
        var defer;
        var terceros = [];
        terceros.tercero = {};
        org.Terceros = terceros;

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/terceros').success(function (r) {
                    
                    r.data.forEach(function (tercero) {
                        terceros.push(tercero);
                        terceros.tercero[tercero.tercero_contable_id] = tercero;
                    });

                    defer.resolve(terceros);
                });
            }

            return defer.promise
        }
    }


    function getImpuestos () {
        var defer;
        var impuestos = [];

        impuestos.impuesto = {};
      

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/impuestos').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        impuestos.push(imp);
                        impuestos.impuesto[imp.impuesto_conjunto_id] = imp;
                    });

                    defer.resolve(impuestos);
                });
            }

            return defer.promise
        }
    }

    function getRetenciones () {
        var defer;
        var impuestos = [];

        impuestos.impuesto = {};
      

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/retenciones').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        impuestos.push(imp);
                        impuestos.impuesto[imp.retencion_conjunto_id] = imp;
                    });

                    defer.resolve(impuestos);
                });
            }

            return defer.promise
        }
    }

    function getSucursales () {
        var defer;
        var sucursales = [];

    
        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/sucursales').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        sucursales.push(imp);
                    });

                    defer.resolve(sucursales);
                });
            }

            return defer.promise
        }
    }


    function getVendeodres () {
        var defer;
        var vendedores = [];

    
        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/vendedores').success(function (r) {
                    
                    r.data.forEach(function (vnd) {
                        vendedores.push(vnd);
                    });

                    defer.resolve(vendedores);
                });
            }

            return defer.promise
        }
    }


    function getProductos () {
        var defer;
        var productos = [];

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/productos').success(function (r) {
                    
                    r.data.forEach(function (vnd) {
                        productos.push(vnd);
                    });

                    defer.resolve(productos);
                });
            }

            return defer.promise
        }
    }

});