inject(FACTORY, 'Poliza', ['$q', '$rootScope', '$http', 'Organizacion', 'uuid', 'fecha'] , function ($q, $rootScope, $http, Organizacion, uuid, fecha) {

    var campos = 'poliza_id.organizacion_id.concepto.total.total_m_base.poliza_id.ano_contable.mes_contable.referencia.numero.tasa_de_cambio.notas.manual';

  
    Poliza.takeRaw = function (raw) {
        return campos.split('.').reduce(function (result, k) { 
            result[k] = raw[k]; 
            return result; 
        }, {});
    }

    function obtenerCentroElemento (e) {
        var elemento = Poliza.centrosDeCosto.elemento[e];
        if(!elemento) return null;
        return Poliza.centrosDeCosto.centro[elemento.centro_de_costo_id];
    }

    Poliza.fromRaw = function (poliza, raw) {
        var centroDeCosto1, centroDeCosto2;
        //Extiendo los valores a heredar directamente
        angular.extend(poliza, Poliza.takeRaw(raw));
        
        //Tipo de Cambio
        poliza.tipoDeCambio = Poliza.tiposDeCambio.porTipoDeCambio(raw.tipo_de_cambio);
        poliza.seleccionarMoneda = poliza.tipoDeCambio.base != '1';
        
        //Obtengo los centros de costo a partir de los elementos
        var partida = raw.partidas[0];
        
        if(partida) {
            poliza.centroDeCosto1 = obtenerCentroElemento(partida.ccto_1_id);
            poliza.centroDeCosto2 = obtenerCentroElemento(partida.ccto_2_id);
        }
        
        //Obtengo las partidas
        poliza.partidas = [];

        raw.partidas.forEach(function (p) {
            poliza.partidas.push(p);
        });

        poliza.tipo = Poliza.tipos.tipo[raw.tipo];
        poliza.fecha = fecha.desdeSQL(raw.fecha);

        //Condición para a falta de estatus, poner el default, no debe ser asi
        poliza.estatus = Poliza.estatuses.estatus[raw.estatus] || Poliza.estatuses.estatus['P'] ;
    };


    Poliza.hayCopia = function () {
        return false;
    };

    Poliza.establecerCopia = function (poliza) {
        localStorage.setItem('Poliza-copia', poliza.poliza_id);
    };

    Poliza.obtener = function (id) {
        var defer = $q.defer();

        Poliza.preparar().then(function () {
            console.log(id);
        
            $http.get('/api/poliza/' + id).success(function (d) {
                
                if(!d.data.partidas) d.data.partidas = [];
                
                //Ordena las partidas
                d.data.partidas.sort(function (p, d) {
                    return Number(p.posicion_partida) - Number(d.posicion_partida);
                }).forEach(function (p) {
                    p.$integrable = true;
                })

                var poliza = new Poliza;

                Poliza.fromRaw(poliza, d.data);

                poliza.actualizarPartidas();
                defer.resolve(poliza);
            }).error(function (d) {
                defer.reject(d);
            })
        });

        return defer.promise;
    }

    /** 
     * Crea una tasa nueva 
     */
    Poliza.crear = function (datos) {
        var defer  = $q.defer();

        Poliza.preparar().then(function () {
            var poliza   = Poliza();
            var fecha    = new Date;
        

            angular.extend(poliza, {
                poliza_id : uuid(),
                partidas  : [],
                concepto  : '',
                fecha     : fecha,
                tipo      : Poliza.tipos.tipo['D'],
                estatus   : Poliza.estatuses.estatus['P'],
                $local    : true
            }, datos);


            //Consulta de los centros de costo para asignar
            if(Poliza.centrosDeCosto.centros) {
                var centros = Poliza.centrosDeCosto.centros.filter(function (c) {return c.activo == "1"});
                poliza.centroDeCosto1 = centros[0];
                poliza.centroDeCosto2 = centros[1];
            }

            //Consulta del tipo de cambio para asignar
            poliza.tipoDeCambio   = Poliza.tiposDeCambio.base;
            poliza.tasa_de_cambio = poliza.tipoDeCambio.tasa.tasa_de_cambio;

            defer.resolve(poliza);
        });

        return defer.promise;
    };


    var estatuses = [
        { estatus : 'P', nombre : 'En Preparacion', color : 'blue', nombre_corto: 'PRP'},
        { estatus : 'T', nombre : 'Por Autorizar', color : 'amber', nombre_corto: 'PDTE'},
        { estatus : 'A', nombre : 'Autorizada', color : 'green',  nombre_corto : 'AUT'},
        { estatus : 'X', nombre : 'Eliminada', color : 'red', nombre_corto : 'ELIM'},
    ];

    estatuses.estatus = estatuses.reduce(function (r,e) { r[e.estatus] = e; return r;}, {});

    var tipos = [
        { tipo : 'D', nombre : 'Diario'},
        { tipo : 'I', nombre : 'Ingreso'},
        { tipo : 'E', nombre : 'Egreso'},
    ];

    tipos.tipo = tipos.reduce(function (r,t) { r[t.tipo] = t; return r;}, {});


    var prepararDefer;

    function check () {
        if(Boolean(Poliza.tiposDeCambio) && Boolean(Poliza.centrosDeCosto) && Boolean(Poliza.cuentasContables)){
            prepararDefer.resolve(true);
        }
    };

    Poliza.preparar = function () {
        if(!prepararDefer) {
            prepararDefer = $q.defer(); 

            Organizacion.centrosDeCosto().then(function (cc) {
                Poliza.centrosDeCosto = cc;
                check();
            });

            Organizacion.tiposDeCambio().then(function (tc) {
                Poliza.tiposDeCambio = tc;
                check();
            });

            Organizacion.cuentasContables().then(function (cc) {
                Poliza.cuentasContables = cc;
                check();
            });

            Poliza.estatuses = estatuses;
            Poliza.tipos     = tipos;
        }

        return prepararDefer.promise;
    };


    function Poliza () {
        if(!(this instanceof Poliza)) return new Poliza;
        var poliza = this;
    };

    /**
     * Actualiza las partidas para su manejo
     */
    Poliza.prototype.actualizarPartidas = function () {
        var poliza = this;

        poliza.partidas.forEach(function (partida) {
            partida.moneda         = poliza.tipoDeCambio.codigo_moneda;
            partida.tasa_de_cambio = poliza.tasa_de_cambio;
            partida.importe_m_base = Number(partida.tasa_de_cambio) * partida.importe;
            
            if(partida.debe_o_haber == 1) {
                partida.debe  = partida.importe;
                partida.haber  = null;
            } else if(partida.debe_o_haber == -1) {
                partida.haber = partida.importe;
                partida.debe = null;
            }
        });

        poliza.calcularTotales();
    };


    Poliza.prototype.agregarPartida = function() {
        this.partidas.push({
            poliza_id : this.poliza_id, 
            poliza_partida_id : uuid(),
            cuenta_id : null,
            concepto : null,
            debe_o_haber : 1,
            importe : 0,
            tasa_de_cambio : this.tasa_de_cambio,
            moneda : this.moneda,
            operacion_item_id : null,
            importe_m_base : 0
        });
    };

    Poliza.prototype.eliminarPartida = function(partida) {
        var index = this.partidas.indexOf(partida);
        if(index === -1) return;

        this.partidas.splice(index,1);
        
        if(!this.partidasAEliminar) {
            this.partidasAEliminar = [];
        }

        this.partidasAEliminar.push(partida);
        this.calcularTotales();
    };

    /**
     *
     * Cálculo de totales en Debe, haber y moneda base
     */
    Poliza.prototype.calcularTotales = function () {
        var total = {'1': 0, '-1': 0};
            
        this.partidas.forEach(function (partida){
            total[String(partida.debe_o_haber)] += Number(partida.importe);
        });

        this.totalDebe  = total['1'];
        this.totalHaber = total['-1'];
        this.total = this.totalDebe;
        this.total_m_base = this.tasa_de_cambio * this.total;
    };

    Poliza.prototype.asignarTipoDeCambio = function (tipoDeCambio, tasaDeCambio) {
        this.tipoDeCambio = tipoDeCambio;
        this.tasa_de_cambio = tasaDeCambio;
        this.seleccionarMoneda = this.tipoDeCambio.base != '1';
        this.actualizarPartidas();
    };


    Poliza.prototype.asignarDebe = function(partida, debe) {
        partida.debe     = debe;
        partida.haber    = null;
        partida.importe  = debe;
        partida.debe_o_haber = 1;

        this.calcularTotales();
    };
    

    Poliza.prototype.asignarHaber = function(partida, haber) {
        partida.debe     = null;
        partida.haber    = haber;
        partida.importe  = haber;
        partida.debe_o_haber = -1;

        this.calcularTotales();
    };

    Poliza.prototype.elementoCosto = function (partida, num) {
        num = Number(num);
        if(num !== 1 && num !== 2) num = 1;
        return Poliza.centrosDeCosto.elemento[partida['ccto_'+ num +'_id']];
    };

    Poliza.prototype.cuentaContable = function (partida) {
        if(!partida.cuenta_id) return '';
        return Poliza.cuentasContables.cuenta[partida['cuenta_id']].nombre;
    };

    Poliza.prototype.cta = function (partida) {
        if(!partida.cuenta_id) return '';
        return Poliza.cuentasContables.cuenta[partida['cuenta_id']];
    };

    Poliza.prototype.finalizar = function () {
        this.estatus = Poliza.estatuses.estatus['T'];
        return this.guardar();
    };

    Poliza.prototype.aplicar = function () {
        this.estatus = Poliza.estatuses.estatus['A'];
        return this.guardar();
    };

    Poliza.prototype.eliminar = function () {
        return $http.post('/api/poliza/eliminar/' + this.poliza_id).success(function (R) {
            console.log(R);
        });
    };

    Poliza.prototype.cantidadCctos = function () {
        var num = 0;
        if(this.centroDeCosto1) num++;
        if(this.centroDeCosto2) num++;
        return num;
    }
    

    /**
     * Guarda la poliza
     * @return $q().promise
     */
    Poliza.prototype.guardar = function  () {
        if(this.$guardando) return;

        var self = this;
        self.$guardando = true;
        
        var defer             = $q.defer();
        var poliza            = angular.extend({}, Poliza.takeRaw(self));
        var partidas          = self.partidas;
        
        //Agregar partidas
        poliza.partidas = partidas.filter(function (p) { return p.$integrable });
        
        //Partidas a Eliminar
        if(self.partidasAEliminar) {
            poliza.eliminar = self.partidasAEliminar.map(function (p) { return p.poliza_partida_id });
        }
        
        //Tipo de Cambio y moneda
        poliza.tipo_de_cambio = self.tipoDeCambio.tipo_de_cambio;
        poliza.moneda         = self.tipoDeCambio.codigo_moneda;

        //Tipo
        poliza.tipo           = self.tipo.tipo;
        //Estatus
        poliza.estatus        = self.estatus.estatus;

        poliza.fecha          = fecha.toSQL(self.fecha);

        var url = (!self.$local) ? '/v1/api/polizas/editar/' + self.poliza_id : '/api/poliza/agregar';
        
        //Guardar
        $http.post(url, poliza).success(function (d) {
            $http.post('/api/poliza/estado_poliza/' + poliza.poliza_id + '/' + poliza.estatus).success(function () {
                Poliza.obtener(poliza.poliza_id).then(function (result) {
                    self.$guardando = false;
                    defer.resolve(result);
                });
            });
        })
        .error(function (d) {
            console.log(d);
            defer.reject(d);
        })

        return defer.promise;
    };

    return Poliza;
});