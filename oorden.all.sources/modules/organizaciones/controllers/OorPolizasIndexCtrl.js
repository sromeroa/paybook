inject(CONTROLLER, 'OorPolizasIndexCtrl', ['$scope', 'Organizacion', '$http', 'Poliza', 'fecha'], function ($scope, Organizacion, $http, Poliza, fecha) {

    this.selectedTabIndex = 0;

    $scope.contable   = {};
    $scope.control    = { active_all : false };
    $scope.Poliza     = Poliza;
    $scope.formulario = { tipo : '__todas__'};


    this.firstLoaded = false;


    Poliza.preparar().then(function () {
        $scope.estatuses = [{ estatus : 'Todas', nombre : 'Todas' }];
        Organizacion.configuracion().then(function (d) {
            $scope.mesesContables = d.mesesContables;
            
            Poliza.estatuses.forEach(function (d) {
                $scope.estatuses.push(d);
                $scope.$ready = true;
            });


            $scope.tipos =  Poliza.tipos.map(function (c){ return c});
            $scope.tipos.unshift({tipo:'__todas__', nombre : 'Todas'})
        });   
    });
    
    function to10(n) {
        var d = String(n);
        return (d.length == 1) ? '0'.concat(d) : d; 
    }


    $scope.aplicarBusquedaFormulario = function () {
        var formulario  = angular.copy($scope.formulario);
        var params = {};

        if(formulario.fechaInicial) {
            params['fecha>'] = fecha.toSQL(formulario.fechaInicial);
        }

        if(formulario.tipo != '__todas__') {
            params['tipo'] = formulario.tipo;
        }

        if(formulario.fechaFinal) {
            params['fecha<'] = fecha.toSQL(formulario.fechaFinal);
        }

        if(formulario.concepto) {
            params['concepto*'] = formulario.concepto;
        }

        if(formulario.referencia) {
            params['referencia*'] = formulario.referencia;
        }

        if(formulario.numero) {
            params['numero'] = formulario.numero;
        }
        $scope.buscar(params);
        
    };
    
    $scope.cambiarEstatus = function (estatus) {
        if(!$scope.busquedaFormulario) {
            $scope.aplicarBusquedaContable();
        } else {
            $scope.formulario.estatus = estatus.estatus;
            $scope.aplicarBusquedaFormulario();
        }
    };

    $scope.buscar = function (busqueda) {
        if($scope.buscando) return;
        $scope.buscando = true;
        $scope.polizas = null;
        $scope.control.active_all = false;

        angular.extend(busqueda, {modelo:'polizas'});

        if(busqueda.estatus == 'Todas') {
            delete(busqueda.estatus);
            busqueda['estatus!'] = 'X';
        }

        return $http.get('/apiv2', {params : busqueda})
            .success(function (d) {
                $scope.buscando = false;
                $scope.index.firstLoaded = true;
                $scope.polizas  = [];
                if(d.data) {
                    d.data.forEach(function (p) { $scope.polizas.push(p); });
                }
            });
    };


    $scope.aplicarBusquedaContable = function () {
        $scope.buscar({
            mes_contable : $scope.contable.mes, 
            ano_contable : $scope.contable.ano,
            estatus      : getSelectedStatus().estatus
        });

    }

    $scope.activarFormulario = function () {
        $scope.busquedaFormulario =true;
        $scope.formulario = { estatus : getSelectedStatus().estatus };
    } 

    $scope.desactivarFormulario = function () {
        $scope.busquedaFormulario = false;
        $scope.formulario = null;
        $scope.aplicarBusquedaContable();
    }
    

    function getSelectedStatus () {
        return $scope.estatuses[$scope.index.selectedTabIndex];
    };


    var removeReadyWatcher = $scope.$watch('$ready', function (ready) {
        if(!ready) return;

        removeReadyWatcher();

        //Se asignan las fechas contables a "hoy"
        var hoy = new Date();
        $scope.contable.ano = hoy.getFullYear();
        $scope.contable.mes = hoy.getMonth() + 1;

        //Se reacciona a los cambios en el cambio de estatus
        $scope.$watch('index.selectedTabIndex', function (d, o) {
            if(d === o) return;
            $scope.cambiarEstatus(getSelectedStatus());
        });

        $scope.$watch('control.active_all', function (activeAll) {
            if(! $scope.polizas ) {
                return;
            }
            if(activeAll === true || activeAll === false) {
                $scope.polizas.forEach(function (p) {
                    p.$seleccionada = activeAll;
                });
            }
        });

        $scope.aplicarBusquedaContable();
    });

});