inject(CONTROLLER, 'CategoriasCtrl', ['$scope', '$http'], function ($scope, $http) {

    var baseUrl = '/categorias/';
    var ctrl = this;

    $scope.guardar = guardar;
    $scope.abrir = abrirCategoria;
    $scope.eliminar = eliminar;

    obtenerCategorias();

    function eliminar (categoria) {
        var uri = baseUrl.concat('delete/', categoria.categoria_id);

        $http.delete(uri)
        .success(function (r) {
            $http.delete(uri.concat('/', r.token)).success(function (d) {
                ctrl.categoria = null;
                obtenerCategorias();
            });
        });
    }

    function abrirCategoria (categoria) {
        ctrl.categoria = angular.copy(categoria);
    }

    function obtenerCategorias () {
        $http.get(baseUrl).success(function (r) {
            $scope.categorias = r;
        });
    }

    function guardarCategoria (categoria) {
        var url = categoria.categoria_id ? 'edit/' + categoria.categoria_id : 'create';
        var method = categoria.categoria_id ? 'put' : 'post';
        return $http[method](baseUrl.concat(url), categoria);
    }

    function guardar (categoria) {
        var request;

        if(categoria.$saving) {
            return;
        } 

        categoria.$saving = true;
        request = guardarCategoria(categoria);
      
        request
        .success(function (rCat) {
            categoria.$saving = false;
            ctrl.categoria = rCat;
        })
        .success(obtenerCategorias);
        

        return request;
    }

});