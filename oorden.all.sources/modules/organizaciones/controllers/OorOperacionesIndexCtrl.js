inject(CONTROLLER, 'OorOperacionesIndexCtrl', ['$scope', '$attrs', 'Organizacion', 'Operacion', 'fecha'], function ($scope, $attrs, Organizacion, Operacion, fecha) {
   
    var index = this;
    index.selectedTabIndex = 0;
    index.firstSearch = false;

    $scope.Operacion  = Operacion; 

    Operacion.preparar().then(function () {
        Organizacion.configuracion().then(function (d) {
            $scope.mesesContables = d.mesesContables;
            $scope.estatuses = [{estatus : 'Todas', nombre : 'Todas'}];

            Operacion.estatuses.forEach(function (d) {
                $scope.estatuses.push(d);
            });

            index.$ready = true;
        }); 
    });

    $scope.seccion = $attrs.seccion;


    function getSelectedStatus () {
        return $scope.estatuses[index.selectedTabIndex];
    };

    
    var removeReadyWatcher = $scope.$watch(isReady, function (ready) {
        if(!ready) return;

        removeReadyWatcher();
        
        //Se asignan las fechas contables a "hoy"
        var hoy = new Date();

        index.estatus = getSelectedStatus();

        index.busqueda = {
            fecha_inicial : fecha.inicialMes(hoy),
            fecha_final   : fecha.finalMes(hoy),
            estatus       : index.estatus.estatus,
            seccion       : $attrs.seccion,
            modelo        : 'operaciones',
            include       : 'operaciones.tercero:embed,operaciones.tipoDocumento:embed'
        };

        //Se reacciona a los cambios en el cambio de estatus
        $scope.$watch('index.selectedTabIndex', function (d, o) {
            if(d === o) return;
            index.estatus = getSelectedStatus();
            index.busqueda.estatus = index.estatus.estatus;
        });


        $scope.$watch('index.busqueda', function (b) {
            $scope.parametros = angular.copy(b);
        
            if(angular.isUndefined(b.estatus) || b.estatus == 'Todas') {
                $scope.parametros['estatus!'] = 'X';
                delete($scope.parametros.estatus);
            }

            if($scope.parametros.fecha_inicial){
                $scope.parametros['fecha>'] = fecha.toSQL($scope.parametros.fecha_inicial);
                delete($scope.parametros.fecha_inicial);
            }

            if($scope.parametros.fecha_final){
                $scope.parametros['fecha<'] = fecha.toSQL($scope.parametros.fecha_final);
                delete($scope.parametros.fecha_final);
            }
            
            index.provider.load();
        }, true);

        $scope.$watch('index.provider.data.data', function (d) {
            index.operaciones = d;
            index.firstSearch = true;
        });
    });

    function isReady () {
        return index.provider && index.$ready;
    }

    

});