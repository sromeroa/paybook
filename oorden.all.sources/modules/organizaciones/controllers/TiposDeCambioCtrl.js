inject(CONTROLLER, 'TiposDeCambioCtrl', ['$scope', '$http', '$timeout', '$window'], function ($scope, $http, $timeout, $window) {
    
    var baseUrl = '/tipos-de-cambio/';
    var Monedas = {};

    $scope.Monedas = Monedas;

    //Obtener las monedas
    $http.get(baseUrl.concat('monedas')).success(function (monedas) {
        monedas.forEach(function (moneda) {
            Monedas[moneda.codigo_moneda] = moneda;
        });
    });

    //Obtener los tipos de cambio
    $scope.getTiposDeCambio = function () {
        $http.get(baseUrl).success(function (r) {
            $scope.tiposDeCambio = r.data.filter(function (t) { return t.base != '1' });
            $scope.tcBase = r.data.filter(function (t) { return t.base == '1' })[0];
            $scope.grupos = agruparTiposDeCambio($scope.tiposDeCambio);
        });
    };

    /*
    $scope.agregar = function (tipoDeCambio) {
        if(tipoDeCambio.$saving) {
            return;
        }
        
        tipoDeCambio.$saving = true;
    
        $http.post(baseUrl.concat('/create'), tipoDeCambio)
        .success(function (r) {

            angular.extend(tipoDeCambio, {$saving:false, $recent:true}, r);
            inactiveRecent(tipoDeCambio);

            $scope.tiposDeCambio.push(tipoDeCambio);
            $scope.grupos = agruparTiposDeCambio($scope.tiposDeCambio);
            $scope.tipoDeCambio = null;
        })
        .error(function (r) {
            alert(r[0].message);
        });
    };

    $scope.editarTC = function (tc) 
    {
        tc.sufijo = tc.tipo_de_cambio.split(tc.codigo_moneda).join('');
        tc.$edit_tc = true;
    }

    $scope.editarNombre = function (tc) 
    {
        tc.nombre_old = tc.nombre;
        tc.$edit_nombre = true;
    }

    $scope.cancelarEdicionNombre = function (tc) 
    {
        tc.nombre = tc.nombre_old;
        tc.$edit_nombre = false;
    }

    $scope.guardarTC = function (tc) 
    {
        tc.$busy = true;

        $http.put(baseUrl.concat('/edit/', tc.tipo_de_cambio_id), { sufijo : tc.sufijo })
        .success(function (r) {
            tc.tipo_de_cambio = r.tipo_de_cambio;
            tc.$busy = false;
            tc.$edit_tc = false;
        });
    }

    $scope.guardarNombre = function (tc)
    {
        tc.$busy = true;

        $http.put(baseUrl.concat('/edit/', tc.tipo_de_cambio_id), { nombre : tc.nombre })
        .success(function (r) {
            tc.nombre = r.nombre;
            tc.$busy = false;
            tc.$edit_nombre = false;
        });
    }

    $scope.asignarRevaluaciones = function (tc)
    {
        return $http.post(baseUrl.concat('revaluaciones/' + tc.tipo_de_cambio_id )).success($scope.getTiposDeCambio);
    }

    $scope.get = function (id) {
        return $http.get(baseUrl.concat('get/', id));
    }
    */

    $scope.agregarTasa = function (tc, tasa) {
        var tcId = tc.tipo_de_cambio_id;
        tc.$busy = true;

        $http.post(baseUrl.concat('tasa/',tcId), {tasa : tasa}).success(function () {
            /*
            $scope.get(tcId).success(function (rTC) {
                angular.extend(tc, rTC);
                tc.$busy = false;
            });
            */
            $window.location.reload();
        });
    };

    /*
    
    $scope.eliminarTc = function (tc) {
        $http.delete(baseUrl.concat('delete/', tc.tipo_de_cambio_id)).success(function (d) {
            var c = confirm("¿Desea Eliminar?");
            if(c) {
                $http.delete(baseUrl.concat('delete/', tc.tipo_de_cambio_id, '/', d.token))
                    .success(function () {
                        $scope.getTiposDeCambio()
                    })
            }
        });
    }
    */

   
    $scope.getTiposDeCambio();

    /*
    function inactiveRecent(tipoDeCambio) {
        $timeout(function () {
            tipoDeCambio.$recent = false;
        },1000);
    }
    */

    function agruparTiposDeCambio (tiposDeCambio) {
        var grupos = {};

        tiposDeCambio.forEach(function (tipo) {
            grupos[tipo.codigo_moneda] = true;
        });

        function byCodigoMoneda (cm) {
            return function (tc) {
                return tc.codigo_moneda == cm; 
            };
        };

        return Object.keys(grupos).map(function (grupo) {
            return {
                codigo_moneda : grupo, 
                tiposDeCambio : tiposDeCambio.filter(byCodigoMoneda(grupo))
            };
        });
    }

});