inject(CONTROLLER, 'OorNdcFacturaCtrl', ['$scope', '$mdDialog', '$http', 'uuid', 'Operacion'], function ($scope, $mdDialog, $http, uuid, Operacion) {

    /** 
     * Modificar la factura de origen de
     */
    $scope.modificarFactura = function ($event) {

        if($scope.opCtrl.items.length){
            var confirmar = confirm('Se eliminarán todas las líneas del documento y se crearán nuevas con la nueva información.');
            if(!confirmar) return;
        }

        showDialog($event).then(function (op) {
            if(!op) return;

            $scope.opCtrl.operacionAnterior = op;

            Operacion.obtenerDatosExtra(op.operacion_id).then(function (extra) {
                $scope.opCtrl.eliminarTodosLosItems();
                $scope.opCtrl.items = obtenerItemsDevolucion(op, extra.data);

                $scope.operacion.referencia = op.referencia;
                $scope.operacion.tipo_de_cambio = op.tipo_de_cambio;
                $scope.operacion.moneda = op.moneda;
                $scope.operacion.tasa_de_cambio = op.tasa_de_cambio;
                //Se actualiza la operacion y luego se aplica al scope para actualizar la vista
                setTimeout(function () {
                    $scope.actualizarOperacion();
                    $scope.$apply();
                }, 500);
            });
         
        });
    };



    function obtenerItemsDevolucion(d, datosExtra) {
        return d.items.filter(incluir).map(function (i) {
            var producto = datosExtra.productos.filter(function (p) { 
                return p.producto_id == i.producto_id 
            })[0];

            return {
                operacion_item_id : uuid(),
                
                //referencias al origen
                operacion_anterior_item_id : i.operacion_item_id,
                itemAnterior : i,
                
                //producto
                producto :  producto,

                //Cantidad sería el máximo posible

                cantidad : Number(i.cantidad) - Number(i.cantidad_devuelta || 0),

                //se cambia la cta por la de nota de crédito
                cuenta_id : $scope.opCtrl.ccProducto(producto),

                //los valores que se copian
                producto_nombre   : i.producto_nombre,
                unidad : i.unidad,
                impuesto_conjunto_id : i.impuesto_conjunto_id,
                retencion_conjunto_id : i.retencion_conjunto_id,
                ccto_1_id : i.ccto_1_id,
                ccto_2_id : i.ccto_2_id,
                precio_unitario : i.precio_unitario,
                descuento : i.descuento   
            };
        });
    }

    function incluir (i) { return i.$incluir; }
    

    function showDialog($event) {
        var parentEl = angular.element(document.body);
        
        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl : '/partials/operaciones/dialogo-ndc-factura.html',
            locals: {
                tercero : $scope.opCtrl.tercero,
                operacionAnterior : $scope.opCtrl.operacionAnterior,
                tipo_operacion : $scope.opCtrl.VNC ? 'VTA' : 'COM',
                itemsAnteriores : $scope.opCtrl.items.map(function (i) { return i.operacion_anterior_item_id })
            },
            controller: ['$scope', 'tercero', 'Operacion', 'itemsAnteriores' ,'operacionAnterior' , 'tipo_operacion',AuxiliarDialogCtrl]
        });
    }


    function AuxiliarDialogCtrl ($scope, tercero, Operacion, itemsAnteriores, operacionAnterior, tipo_operacion) {
        $scope.tercero = tercero;

        if(operacionAnterior) {
            //si hay operacion anterior se muestra
            $scope.facturaSeleccionada = operacionAnterior;
        } else {
            //se cargan las facturas
            cargarFacturas();
        }

        function cargarFacturas () {
            var params = {
                tercero_id : tercero.tercero_id,
                tipo_operacion : tipo_operacion
                //estatus : 'A,S'
            };

            $http.get('/api/operaciones/', {params : params}).success(function (d) {
                $scope.facturas = d.data.filter(function (op) {
                    return ['A','S'].indexOf( angular.uppercase(op.estatus) ) > -1;
                });
            });
        }

        $scope.abrirFactura = function (factura) {
            $scope.facturaSeleccionada = factura;
        };

        $scope.cerrarFactura = function () {
            $scope.facturaSeleccionada = null;
            if(!$scope.facturas) {
                cargarFacturas();
            }
        };

        $scope.finalizar = function (operacion) {
            if(!operacion) return;
            $mdDialog.hide(operacion);
        };

        $scope.cancelar = function () {
            $mdDialog.hide(null);
        };

        /**
         * Al seleccionar una factura, se carga la operacion
         * y se llena una tabla con los items que ya estan en referenciados
         */
        $scope.$watch('facturaSeleccionada', function (facturaSeleccionada) {
            $scope.operacion = null;

            if(!facturaSeleccionada) return;

            $scope.cargandoOperacion = true;

            //Obtiene la operación seleccionada
            Operacion.obtener(facturaSeleccionada.operacion_id).then(function (data) {
                $scope.operacion = data.operacion;
                $scope.cargandoOperacion = false;

                //por cada item se busca si ya está en la operación para asinarle $incluir
                $scope.operacion.items.forEach(function (it) {
                    it.$incluir = Boolean( (itemsAnteriores.indexOf(it.operacion_item_id)) + 1);
                });
            });
            
        });
    }
});