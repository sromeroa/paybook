inject(CONTROLLER, 'TiposDeCambioAutoCompleteCtrl', ['$scope','$http'], function ($scope, $http) {
    var ac = this;

    ac.items = [];
    ac.itemsByTipo = {};

    ac.search = function (k) {
        var q = angular.lowercase(k);

        return ac.items.sort(function (item, item2) {
            return ac.caption(item2) - ac.caption(item);
        }).filter(function (item) {
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        if(!item) return '';
        return item.tipo_de_cambio + ' ' + item.nombre;
    }


    $http.get('/api/tiposdecambio').success(function (t) {
        ac.items = t.tipos;
    }); 

});