inject(CONTROLLER, 'OorFacturaItemCtrl', ['$scope', 'oorDialogoProducto', '$timeout', 'oorden'], function ($scope, oorDialogoProducto, $timeout, oorden) {
    
    var ctrl = this;
    var mult = 10000;
    var facturaCtrl = $scope.facturaCtrl;

    //Valores de cálculo iniciales
    ctrl.importeAsInt = 0;

    ctrl.montoDescuentoAsInt  = 0;
    ctrl.sinImpuestosAsInt    = 0;

    ctrl.impuestosAgrupados   = [];
    ctrl.retencionesAgrupadas = [];
    ctrl.elementosDeCosto     = [];


    //Metodos del scope
    angular.extend($scope, {
        asignarProducto : asignarProducto,
        actualizarItem : actualizarItem,
        asignarImpuesto : asignarImpuesto
    });

    //Métodos del controlador
    angular.extend(ctrl, {
        getItem : getItem,
        actualizar : actualizar
    });

    function asignarProducto (producto) {
        ctrl.producto = producto;
        if(!producto) return;

        $scope.item.precio_unitario = producto.precio_unitario_venta;
        $scope.item.unidad         = producto.unidad;
        $scope.item.concepto       = producto.nombre;
        
        //La cuenta viene en producto
        var cuenta = $scope.oorden.cuentaContable(ctrl.producto.cuenta_venta);
        asignarCuenta(cuenta);

        // El impuesto, según los criteros
        var impuestoId;
        var impuesto  = null;

        if(facturaCtrl.cliente && facturaCtrl.cliente.impuesto_venta_id) {
            impuestoId = facturaCtrl.cliente.impuesto_venta_id;
        } else if(ctrl.producto && ctrl.producto.impuesto_venta) {
            impuestoId = ctrl.producto.impuesto_venta;
        } else if (ctrl.cuenta && ctrl.cuenta.impuesto_conjunto_id) {
            impuestoId = ctrl.cuenta.impuesto_conjunto_id;
        }

        if(impuestoId) {
            impuesto = $scope.impuestos.filter(function (i) {
                return i.impuesto_conjunto_id == impuestoId;
            })[0];
        }
        
        //Retención
        if($scope.$index != 0) {
            asignarRetencion($scope.factura.items[$scope.$index - 1].retencion);
        }

        asignarImpuesto(impuesto || null);
        actualizarItem();
    };


    function asignarCuenta (cuenta) {
        ctrl.cuenta = cuenta;
    };


    function asignarImpuesto (impuesto) {
        ctrl.impuesto = impuesto;
        actualizarItem();
    };

    function asignarRetencion (retencion) {
        ctrl.retencion = retencion;
        if(!retencion) return;
        $scope.item.retencion_conjunto_id = retencion.retencion_conjunto_id;
        actualizarItem();
    };



    var actualizarTimeout;

    function actualizarItem () {
        actualizarTimeout && $timeout.cancel(actualizarTimeout);

        actualizarTimeout=$timeout(function () {
            actualizar();
            $scope.actualizarFactura(); //ActualizarFactura
            actualizarTimeout = null;
        }, 200);

    }; 


    function getItem () {
        var item = angular.copy($scope.item);

        item.producto_id            = ctrl.producto ? ctrl.producto.producto_id : null;
        item.producto_variante_id   = ctrl.variante ? ctrl.variante.producto_variante_id : null; 
        item.producto_nombre        = item.concepto;

        item.importe = String(ctrl.importeAsInt / mult);

        item.cuenta_id             = ctrl.cuenta ? ctrl.cuenta.cuenta_contable_id : null;
        item.impuesto_conjunto_id  = ctrl.impuesto? ctrl.impuesto.impuesto_conjunto_id : null;
        item.retencion_conjunto_id = ctrl.retencion ? ctrl.retencion.retencion_conjunto_id : null;

        item.ccto_1_id = ctrl.elementosDeCosto[0] ? ctrl.elementosDeCosto[0].elemento_ccosto_id : null;
        item.ccto_2_id = ctrl.elementosDeCosto[1] ? ctrl.elementosDeCosto[1].elemento_ccosto_id : null;
    
        return item;
    };

    function actualizar () {
        var item = $scope.item;
        var cantidad            = Number(item.cantidad ) || 0;
        var precioUnitario      = Number(item.precio_unitario) || 0;
        var descuento           = (Number(item.descuento) || 0) / 100;
        var impuestosIncluidos  = Boolean($scope.factura.precios_con_impuestos);
        var importe;

        importe = Math.round(cantidad * precioUnitario * mult);

        ctrl.montoDescuentoAsInt  = Math.round(importe * descuento);
        ctrl.importeAsInt         = importe - ctrl.montoDescuentoAsInt;
        ctrl.impuestosAgrupados   = [];
        ctrl.retencionesAgrupadas = [];

        if(ctrl.impuesto) {
            ctrl.sinImpuestosAsInt = sinImpuestos(ctrl.importeAsInt, impuestosIncluidos, ctrl.impuesto.tasa_total);

            ctrl.impuesto.componentes.forEach(function (componente) {
                var nombre    = componente.componente + ' ' + String(Number(componente.tasa)) + '%';
                var tasa      = Number(componente.tasa) / 100;
                var montoInt  = Math.round(ctrl.sinImpuestosAsInt * tasa);

                ctrl.impuestosAgrupados.push({
                    nombre     : nombre,
                    tasa       : tasa,
                    monto      : montoInt / mult,
                    montoAsInt : montoInt
                });
            });
        } else {
            ctrl.sinImpuestosAsInt = ctrl.importeAsInt;
        }
    };


    function sinImpuestos(importe, impuestosIncluidos, tasa) {
        if(!impuestosIncluidos) return importe;
        return Math.round(importe / (1 + (tasa / 100)));
    };


    $scope.$watch('item', function (item) {
        if(!item) return;

        ctrl.elementosDeCosto = [];

        ctrl.impuesto = $scope.impuestos.filter(function (i) {
            return i.impuesto_conjunto_id == item.impuesto_conjunto_id;
        })[0];

        ctrl.retencion = $scope.retenciones.filter(function (i) {
            return i.retencion_conjunto_id == item.retencion_conjunto_id;
        })[0];

        ctrl.cuenta = oorden.cuentaContable(item.cuenta_id);

        if(item.producto_id) {
            ctrl.producto = $scope.productos.filter(function (i) {
                return i.producto_id == item.producto_id;
            })[0];
        }

        if(item.producto_variante_id) {
            ctrl.producto = $scope.variantes.filter(function (i) {
                return i.producto_variante_id == item.producto_variante_id;
            })[0];
        }

        if(item.ccto_1_id) {
            ctrl.elementosDeCosto[0] = oorden.elementoDeCosto(item.ccto_1_id);
        }

        if(item.ccto_2_id) {
            ctrl.elementosDeCosto[1] = oorden.elementoDeCosto(item.ccto_2_id);
        }

        item.concepto = item.producto_nombre;

        actualizar();

        ctrl.$ready = true;
        $scope.registrarItemCtrl(ctrl);
    });

});

