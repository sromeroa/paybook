inject(CONTROLLER, 'RetencionesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function () {
        return ac.items;
    };


    Organizacion.retenciones().then(function (t) {
        ac.items = t;
    });


});