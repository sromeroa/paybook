inject(CONTROLLER, 'SucursalesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function (k) {
        var q = angular.lowercase(k);

        return ac.items.filter(function (item) {
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        return item.clave + ' - ' + item.nombre;
    }


    Organizacion.sucursales().then(function (sucursales) {
        ac.items = sucursales;
    });
});