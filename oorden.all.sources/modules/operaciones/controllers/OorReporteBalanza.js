inject(CONTROLLER, 'oorReporteBalanzaCtrl', ['$scope', '$http', 'fecha', '$mdDialog'], function ($scope, $http, fecha, $mdDialog) {
    var alert;

    $scope.auxiliar = showDialog;

    function showDialog(cta, $event) {
        var parentEl = angular.element(document.body);

        $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            template:
               '<md-dialog aria-label="List dialog">' +
               '  <md-content>'+
               '    <h2>{{cuenta.cuenta}} - {{cuenta.nombre}}</h2>' +
               '    <h4 ng-if="query.mes_contable"> Periodo: {{query.ano_contable}} {{query.mes_contable}}</h4>' +
               '    <h4 ng-if="query.fecha_inicial">Desde: {{query.fecha_inicial}} - Hasta:{{query.fecha_final}}</h4>' +
               '    <reporte-auxiliar parametros="query" reporte-nombre="reporte"></reporte-auxiliar>' +
               '  </md-content>' +
               '  <div class="md-actions">' +
               '    <md-button ng-click="closeDialog()">' +
               '      Cerrar' +
               '    </md-button>' +
               '  </div>' +
               '</md-dialog>',
            locals: {
                cuenta : cta,
                params : $scope.parametros,
             },
            controller: AuxiliarDialogCtrl
        });
    }

    function AuxiliarDialogCtrl (scope, $mdDialog, cuenta, params) {
        scope.cuenta = cuenta;

        scope.query = angular.extend({
            cuenta : cuenta.cuenta_contable_id
        },params);
     
        scope.closeDialog = function() {
            $mdDialog.hide();
        };
    }
    
});