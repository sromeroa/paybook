inject(CONTROLLER, 'TercerosAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog', '$http','$q', 'uuid', '$parse', '$element', 'oorden'], function ($scope, Organizacion, $mdDialog, $http, $q, uuid, $parse, $element, oorden) {
    var ac = this;
    var seccion = $scope.opCtrl ? $scope.opCtrl.seccion() : '';

    ac.search = function (buscar) {
        var defer = $q.defer();
        var params = {buscar : buscar};

        $http.get('/api/terceros/search', {params : params}).success(function (r) {
            defer.resolve(r.data || []);
        });

        return defer.promise;
    }

    $scope.agregarTercero = showDialog;

    function showDialog($event) {
        var parentEl = angular.element(document.body);

        $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            template:
               '<md-dialog aria-label="List dialog">' +
               '  <md-content>'+
               '    <div oor-tercero="tercero" style="min-width:800px"></div>' +
               '  </md-content>' +
               '  <div class="md-actions">' +
               '    <md-button ng-click="closeDialog()">' +
               '      Cancelar' +
               '    </md-button>' +
               '    <md-button class="md-primary" ng-click="guardarTercero(t3Form)">' +
               '      Guardar' +
               '    </md-button>' +
               '  </div>' +
               '</md-dialog>',
            locals: {
            
            },
            controller: ['$scope', 'Tercero',AuxiliarDialogCtrl]
        }).then(function (tercero) {
            var sel = $('md-autocomplete', $element).attr('md-selected-item');
            console.log(tercero);
            $parse(sel).assign($scope, tercero);
            console.log($scope);
        });
    }

    function AuxiliarDialogCtrl (scope, Tercero) {
        scope.closeDialog = function() {
            $mdDialog.hide(null);
        };

        scope.tercero   = { 
            tercero_id : uuid(),
            es_cliente : seccion == 'V',
            es_proveedor : seccion == 'C'
        };

        scope.direccion = {
            codigo_pais : oorden.organizacion.codigo_pais
        };

        scope.guardarTercero = function () {
          scope.$broadcast('guardarTercero')
        }

        scope.$on('$tercero', function ($event,val) {
          $mdDialog.hide(val);
        })


    }

});