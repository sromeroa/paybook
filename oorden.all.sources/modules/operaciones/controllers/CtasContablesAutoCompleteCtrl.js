inject(CONTROLLER, 'CtasContablesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function (k) {
        var q = angular.lowercase(k);
        
        return ac.items.filter(function (item) {
            if(item.acumulativa == "1") return false;
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        return item.cuenta + ' - ' + item.nombre;
    };

    Organizacion.cuentasContables().then(function (c) {
        ac.items = c;
    });
});