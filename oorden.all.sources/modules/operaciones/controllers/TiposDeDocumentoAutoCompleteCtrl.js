inject(CONTROLLER, 'TiposDeDocumentoAutoCompleteCtrl', ['$scope','$http'], function ($scope, $http) {
    var ac = this;

    ac.items = [];
    ac.itemsByTipo = {};

    ac.search = function () {
        return ac.items;
    };

    ac.filterItems = function (tipoDoc) {
        if(!tipoDoc) return ac.items;
        return ac.itemsByTipo[tipoDoc];
    };

    $scope.buscar = function (tipoDocId){
        return ac.items.filter(function (t) { 
            return t.tipo_documento_id == tipoDocId;
        })[0];
    }

    $http.get('/api/tipodocumentos').success(function (t) {
        ac.items = t.data;
    
        ac.items.forEach(function (item) {
            if(!ac.itemsByTipo[item.tipo_operacion]) {
                ac.itemsByTipo[item.tipo_operacion] = [];   
            }

            ac.itemsByTipo[item.tipo_operacion].push(item);
        })
    }); 
});