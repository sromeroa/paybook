inject(CONTROLLER, 'OorOperacionPantallaCtrl', ['$scope', '$attrs', '$window', 'Operacion', 'oorden', 'fecha', '$http', '$q', 'Tercero', 'uSetting', 'uuid'], function ($scope, $attrs, $window, Operacion, oorden, fecha, $http, $q, Tercero, uSetting, uuid) {
    
    var pathname = $window.location.pathname.replace($attrs.path, '');
    var match    = /^(crear|[\d|\-|a|b|c|d|e|f]{36})(\?|\/)?$/.exec(pathname);
    var id       = false;
    var _oorinit = false;


    console.log(pathname, match);

    
    angular.extend($scope, {guardar : guardar});

    //Inicializa los servicios de oorden
    oorden.inicializar().then(function () {
        _oorinit = true;     
    });

    //Carga los impuestos
    $http.get('/api/impuestos').success(function (r) {
        $scope.impuestos = r.data;
    });

    //Carga las retenciones
    $http.get('/api/retenciones').success(function (r) {
        $scope.retenciones = r.data;
    });


    $scope.$watch(function () {
        return _oorinit && Boolean($scope.retenciones) && Boolean($scope.impuestos);
    }, function (isReady) {
        if(!isReady) return;
        if(!match) {
            $scope.notFound = {status:{message:'No encontramos la operacion'}};
            return;
        }
        
        id = match[0];

        if(id === 'crear') {
            //if(Operacion.hayCopia()) {
            //    Operacion.obtenerCopia().then(setOperacion);
            //} else {
                Operacion.crear().then(function (op) {
                    nuevaOperacion(op).then(setOperacion); 
                });
            //}
        } else if(id) {
    
            Operacion.obtener(id).then(function (rOp){
                var op = rOp.operacion;

                if(op.tipo_operacion != angular.uppercase($attrs.tipoOperacion)) {
                    //No es el tipo de operacion correcto, se redirige
                    location.pathname = '/operaciones/' + angular.lowercase(op.tipo_operacion) + '/' + id;
                    return;
                }

                op.$editar = false;

                $http.get('/api/operacionesextra/' + id).success(function (e) {
                    $scope.productos = e.data.productos;
                    $scope.poliza    = e.data.poliza;


                    if(!op.operacion_anterior_id){
                        //Si no hay operacion anterior ya está listo
                        setOperacion(op);
                        return;
                    }

                    Operacion.obtener(op.operacion_anterior_id).then(function (rOpAnterior) {
                        //Asignar la op Anterior
                        op.operacionAnterior = rOpAnterior.operacion;
                        setOperacion(op);
                    });
                });
            }, function (a) {
        

                $scope.notFound = a;

            });
        }
    });


    function nuevaOperacion (operacion) {
        var defer = $q.defer();

        uSetting.get(['usuario', oorden.usuario.usuario_id], null, 'factura').then(function (r) {
            var fSettings = r.factura || {};

            $http.get('/api/tipodocumentos').success(function (t) {

                

                angular.extend(operacion, {
                    precios_con_impuestos : Boolean(Number(fSettings.precios_con_impuestos)),
                    mostrar_descuento :     Boolean(Number(fSettings.mostrar_descuento)),
                    mostrar_cctos :         Boolean(Number(fSettings.mostrar_cctos)),
                    mostrar_cuentas :       Boolean(Number(fSettings.mostrar_cuentas)),
                    mostrar_retenciones :   Boolean(Number(fSettings.mostrar_retenciones)),
                    mostrar_impuestos :     Boolean(Number(fSettings.mostrar_impuestos)),

                    tipo_de_cambio : oorden.tipoDeCambio.base.tipo_de_cambio,
                    tasa_de_cambio : 1,
                    sucursal_id : oorden.sucursal.sucursal_id,
                    $new : true,
                    $editar : true,
                    estatus : 'P',

                    items : [
                        {
                            operacion_item_id : uuid(),
                            posicion : 0
                        },
                        {
                            operacion_item_id : uuid(),
                            posicion : 1
                        }
                    ]
                });
                
                operacion.tipo_operacion = angular.uppercase($attrs.tipoOperacion);
            
                var T = t.data.filter(function (t) {
                    return operacion.tipo_operacion == t.tipo_operacion;
                })[0];
         
                operacion.tipo_documento =  T ? T.tipo_documento_id :null;
                operacion.serie = T? T.serie : null;
                defer.resolve(operacion);
            }); 
        });

        return defer.promise;
    }

    function guardar (capturarNueva) {
        var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }

        Operacion.guardar(factura).then(function (r) {
            uSetting.set(['usuario', oorden.usuario.usuario_id], null, {
                factura : {
                    mostrar_impuestos : Number(factura.mostrar_impuestos),
                    mostrar_retenciones : Number(factura.mostrar_retenciones),
                    mostrar_descuento : Number(factura.mostrar_descuento),
                    mostrar_cctos : Number(factura.mostrar_cctos),
                    mostrar_cuentas : Number(factura.mostrar_cuentas)
                }
            })
            .then(function () {
                afterSave(capturarNueva, factura.$new, factura)(r);
            });
        }, saveError);

    };


    function __afterSave (r, factura) {
           
    }

    function saveError (errResponse) {
        toastr.error(errResponse.status.message); 
    }

    $scope.aplicar = function (redirigir) {
         var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }

        Operacion.guardar(factura).then(function () {
            Operacion.estatus($scope.operacion, 'A').then(afterSave(redirigir, true, factura), saveError);
        }, saveError);
        
    };

    $scope.porAutorizar = function (redirigir) {
         var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }
        
        Operacion.guardar(factura).then(function () {
            return Operacion.estatus($scope.operacion, 'T').then(afterSave(redirigir, true, factura), saveError);
        },saveError);
    }

    /** 
     * Despues de guardar...
     */
    function afterSave (redirigir, esNuevo, factura) {
        return function (p) {
            $scope.lsMessage({text : 'Operacion Guardada'});
            
            if(redirigir){
                location.pathname = '/operaciones/' + factura.tipo_operacion.toLowerCase() + '/crear';
                return;
            }
            if(esNuevo || true) {
                location.pathname = '/operaciones/' + angular.lowercase(factura.seccion);
                return;
            }
        }
    }

    function setOperacion (p) {
        $scope.operacion = p; 
        if($scope.puede.editar()) {
            $scope.editar();
        }
    }


    /**
     * Permisos
     */
    $scope.puede = {
        eliminar : function () {
            return $scope.operacion.estatus != 'S';
        },
        editar : function () {
            return ($scope.operacion.estatus != 'X' && $scope.operacion.estatus != 'A' && $scope.operacion.estatus != 'S'); 
        },
        aplicar : function () {
            return $scope.operacion.estatus === 'T' ||  $scope.operacion.estatus === 'P'; 
        },
        porAutorizar : function () {
            return $scope.operacion.estatus === 'P';
        }
    };


    /**
     * Acciones
     */
   
    $scope.cancelar = function () {
        location.pathname = '/operaciones/' + angular.lowercase($scope.operacion.tipo_operacion) + '/' + $scope.operacion.operacion_id
    };

    $scope.eliminar = function () {
        if(! confirm('¿Desea Eliminar la Operación?')) return;

        $http.post('/api/operacion/eliminar/' + $scope.operacion.operacion_id).success(function (d) {
            $scope.lsMessage({type:'warning', text : 'Oepración Eliminada'});
            location.pathname = '/operaciones/'+ $attrs.tipoOperacion[0];
        });
    };

    $scope.editar = function () {
        $scope.operacion.$editar = true;
    };
});

