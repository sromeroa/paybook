inject(CONTROLLER, 'OorPolizaPantallaCtrl', ['$scope', '$attrs', '$window', 'Poliza'], function ($scope, $attrs, $window, Poliza) {

    var pathname = $window.location.pathname.replace($attrs.path, '');
    var match    = /^(crear|[\d|\-|a|b|c|d|e|f]{36})(\?|\/)?$/.exec(pathname);
    var id = false;

    function cargarPoliza () {
        if(!match) {
           $scope.notFound = {status:{message : 'No Encontramos la Póliza'}};
           return;
        }

        id = match[0];

        if(id == 'crear') {
            //if(Poliza.hayCopia()) {
            //    Poliza.obtenerCopia().then(setPoliza);
            //} else {
                Poliza.crear().then(setPoliza);
            //}
        } else if(id) {
            Poliza.obtener(id).then(setPoliza, function () {
                $scope.notFound = {status:{message : 'No Encontramos la Póliza'}};
            });
        }
    }

    $scope.puedeEliminar = function () {
        if(!$scope.poliza) return false;
        if($scope.poliza.$local) return false;
        if($scope.poliza.manual == '0') return false;
        return true;
    }

    $scope.editar = function () {
        if(!$scope.poliza) return false;
        $scope.poliza.$editar  = true ;
    } 

    $scope.cancelar = function () {
        if(!$scope.poliza) return false;
        $scope.poliza.$editar  = false;
    } 

    $scope.puedeEditar = function () {
        if(!$scope.poliza) return false;
        if($scope.poliza.$local) return false;
        return ['A','X'].indexOf($scope.poliza.estatus.estatus) == -1;
    }

    $scope.puedeValidar = function () {
        if(!$scope.poliza) return false;
        return true;
    }

    $scope.puedeFinalizar = function () {
        if(!$scope.poliza) return false;
        return false;
    }

    $scope.puedeAplicar = function () {
        if(!$scope.poliza) return false;
        return $scope.poliza.estatus.estatus != 'A';
    }

    

    $scope.guardar = function (redirigir) {
        var esNuevo = $scope.poliza.$local;
        $scope.poliza.busy = 'guardar';
        $scope.poliza.guardar().then(afterSave(redirigir, esNuevo));
    };

    $scope.finalizar = function (redirigir) {
        var esNuevo = $scope.poliza.$local;
        $scope.poliza.busy = 'finalizar';

        $scope.poliza.guardar().then(function () {
            $scope.poliza.finalizar().then(afterSave(redirigir, true));
        });
        
    };

    $scope.aplicar = function (redirigir) {
        var esNuevo = $scope.poliza.$local;
        $scope.poliza.busy = 'aplicar';
       

        $scope.poliza.guardar().then(function () {
            $scope.poliza.aplicar().then(afterSave(redirigir, true));
        });
    };

    $scope.eliminar = function () {
        if(! confirm('¿Desea Eliminar la Póliza?')) return;
        $scope.poliza.eliminar().then(function () {
            location.pathname = '/polizas';
        });
    };

    var tieneErrores = false;


    $scope.tieneErrores = function () {
        return tieneErrores;
    };


    $scope.$watch(function () {
        tieneErrores = false;

        if($scope.poliza) {
            if($scope.poliza.totalDebe != $scope.poliza.totalHaber) {
                tieneErrores = true;
            }
        }

        ($scope.poliza && $scope.poliza.partidas || []).forEach(function (partida) {
            if(Number(partida.importe) && partida.cuenta_id == null) {
                tieneErrores = true;
                partida.$faltaCuenta = true;
            } else {

             partida.$faltaCuenta = false;
         }
        });


     });


    function setPoliza (p) { 
        p.$editar = Boolean(id == 'crear');

        if(!p.items) p.items = [];
        $scope.poliza = p; 
        if($scope.puedeEditar()) {
            $scope.editar();
        }
    };

    function afterSave (redirigir, esNuevo) {
    
        return function (p) {
            $scope.poliza.busy = false;
            $scope.lsMessage({text:'Póliza Guardada'});
            
            if(redirigir){
                location.pathname = '/polizas/poliza/crear';

                return;
            }
            //if(esNuevo) {
                location.pathname = '/polizas';
              //  return;
            //}
            //setPoliza(p);
        }

    };


    cargarPoliza();

});