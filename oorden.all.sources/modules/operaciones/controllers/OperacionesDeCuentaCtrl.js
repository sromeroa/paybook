inject(CONTROLLER, 'OperacionesDeCuentaCtrl', ['$scope','Operacion'], function ($scope, Operacion) {
    
    Operacion.preparar().then(function () {
        var eMap = Operacion.estatuses.estatus;

        $scope.definiciones = {
            
            checkbox : {
                caption : function () { 
                    var items = this.items();

                    return m('input[type="checkbox"]');
                },
                value : function (item) {
                    return m('input[type="checkbox"]', { 
                        checked  : item.checked(),
                        onchange : m.withAttr('checked', item.checked)
                    });
                }
            },

            tercero : {
                caption : 'Destinatario',
                value : function (item) {
                    return item.tercero ? item.tercero.nombre : '';
                }
            },

            documento : {
                caption : 'Documento',
                value : function (item) {
                    return m('a', {
                            href:'/operaciones/' + item.tipo_operacion.toLowerCase() + '/'.concat(item.operacion_id)
                        }, 
                        m('strong',item.tipoDocumento.nombre.concat(' ', item.serie, ' #', item.numero ? item.numero : '<auto>'))
                    );
                }
            },

            total : {
                value : function (item) {
                    return m('div.text-right', Number(item.total).toFixed(2));
                }
            },

            estatus : {
                caption : 'Estatus',
                value : function (item) {
                    var estatus = eMap[item.estatus || 'P'];
                    return m('span.pull-right.text-'.concat(estatus.color), estatus.nombre);
                }
            }

        };

    });


});