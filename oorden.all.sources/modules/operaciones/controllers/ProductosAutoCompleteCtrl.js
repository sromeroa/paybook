inject(CONTROLLER, 'ProductosAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function () {
        return ac.items;
    };

    Organizacion.productos().then(function (t) {
        ac.items = t;
    });
});