inject(DIRECTIVE, 'oorTransaccion', [ 'uuid', 'fecha', 'oorDialogoTercero'], function (uuid, fecha, oorDialogoTercero) {
    'use strict';

    return  {
        link : link,
        terminal :true
    };

    function link (scope, element, attrs, ctrls) {
        
        var opComponent = m.component(operaciones.OperacionComponent, {
            cuentaId          : attrs.cuentaId,
            operacionId       : attrs.operacionId,
            tipoOperacion     : attrs.tipoOperacion
        });

        /**
         * Inicializamos oorden y luego se carga el componente en el DOM
         */
        oorden().then(function () {
            oorden.getVendedores().then(function () {
                m.mount(element[0], opComponent);
            });
        });
    };
});
