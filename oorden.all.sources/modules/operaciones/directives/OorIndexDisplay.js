inject(DIRECTIVE, 'oorIndexDisplay', ['$compile'], function ($compile) {
    
    return {
        terminal : true,
        link : link,
        controller : ['$scope', '$http', '$element', '$q', '$timeout', controller],
        controllerAs : 'index'
    };

    function link (scope, element) {
        var html = element.html();
        element.html('');
        
        scope.index.prepare().then(function () {
            element.html(html);
            $compile(element.contents())(scope);
        });

    }


    function controller ($scope, $http, $element, $q, $timeout) {
        var index = this;
        var prepareDefer;

        this.prepare = function () {
            if(!prepareDefer) {
                prepareDefer = $q.defer();
                $timeout(function () {
                    prepareDefer.resolve(true);
                },10);
            }
            return prepareDefer.promise;
        }


        $scope.$watch('conf.selectedTabIndex', function (tabIndex) {
            if(tabIndex === undefined) return;
        });

        
    }



});