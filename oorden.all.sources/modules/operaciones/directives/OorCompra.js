inject(DIRECTIVE, 'oorCompra', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-compra.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorCompra'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{COM : true});
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'COM';
        }

        function seccion () {
            return 'C'; 
        }

        function ccProducto(producto) {
            return producto.cuenta_compra;
        }
    };

});