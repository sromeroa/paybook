inject(DIRECTIVE, 'oorOperacion', ['oorden','Operacion','uuid','Organizacion','Tercero','uSetting', 'fecha'], function (oorden, Operacion, uuid, Organizacion, Tercero, uSetting, fecha) {
    'use strict';
    
    return  {
        scope : true,
        restrict : 'A',
        controller : ['$scope', OorOperacionCtrl],
        controllerAs : 'opCtrl',
    };

    function OorOperacionCtrl ($scope) {
        var ctrl           = this;
        var diasDeCredito  = 0;
        var totalColumnas  = 10;
        var totalNColumnas = 6;
        var itemCtrls      = [];
        var errItems       = [];


        ctrl.errors = {};

        ctrl.eliminar = [];

        

        


        //Asigno el controlador al scope padre
        //para que pueda acceder a los métodos
        $scope.$parent.opCtrl = ctrl;

        $scope.Operacion = Operacion;

        Operacion.preparar().then(function () {
            $scope.Operacion = Operacion;
        });

        $scope.cifras = function () {
            return oorden.organizacion.cifras() 
        };

        var mult = 10;

        angular.extend($scope, {
            asignarTercero : asignarTercero,
            asignarCliente : asignarCliente,
            asignarVencimiento : asignarVencimiento,
            asignarFecha : asignarFecha,
            asignarSucursal : asignarSucursal,
            asignarTipoDeCambio : asignarTipoDeCambio,

            actualizarColumnas : actualizarColumnas,
            registrarItemCtrl : registrarItemCtrl,
            eliminarItemCtrl : eliminarItemCtrl,
            actualizarOperacion : actualizarOperacion,
            asignarTipoDeDocumento : asignarTipoDeDocumento
        });


        angular.extend(ctrl, {
            obtenerFactura : obtenerFactura,
            mount : mount,
            addErrItem : addErrItem,
            remErrItem : remErrItem, 
            hasErrors :  hasErrors,
            eliminarItem : eliminarItem,
            eliminarTodosLosItems : eliminarTodosLosItems
        });

        function addErrItem (itemCtrl) {
            if(errItems.indexOf(itemCtrl) == -1) {
                errItems.push(itemCtrl);
            }
        }

        function remErrItem (itemCtrl) {
            var index = errItems.indexOf(itemCtrl)
            if(index+1) {
                errItems.splice(index,1);
            }
        }


        function validar () {
            ctrl.errors = {};

            if(!$scope.operacion.tipo_documento) {
                ctrl.errors.noTipoDoc = true;
            }
        }

        function hasErrors () {
            return errItems.length + Object.keys(ctrl.errors).length;
        }


        //Espera a que la operacion esté lista para inicializar
        function mount () {
            $scope.$watch('operacion', function (operacion) {
                if(!operacion) return;
                initializeCtrl();
            });
        }

        var camposBooleanos = ['precios_con_impuestos','mostrar_descuento','mostrar_cctos','mostrar_cuentas','mostrar_retenciones','mostrar_impuestos'];
        /**
         * Inicializa el controlador cargando todos los campos que corresponden
         * Ajustando columnas
         * Verificar que los items esten listos
         */
        function initializeCtrl () {
            var testReadyWatcher;

            
            angular.extend($scope, {
                impuestos : $scope.$parent.impuestos,
                retenciones : $scope.$parent.retenciones,
                productos : $scope.$parent.productos,
                Operacion : Operacion,
                oorden : oorden
            });

            mult = Math.pow(10, $scope.cifras());

            ctrl.centrosDeCosto = [];
            //Se ordenan los items
            ctrl.items = $scope.operacion.items.sort(function(a,b) {
                return Number(a.posicion_item) - Number(b.posicion_item);
            });

            delete($scope.operacion.items);

            //Campos booleanos necesitan transformatse
            camposBooleanos.forEach(function (c) {
                $scope.operacion[c] = Boolean(Number($scope.operacion[c]));
            });

            //Cargar la sucursal
            if($scope.operacion.sucursal_id) {
                Organizacion.sucursales().then(function (sucursales) {
                    ctrl.sucursal = sucursales.filter(function (s) { return s.sucursal_id == $scope.operacion.sucursal_id; })[0];
                });
            } else {
                ctrl.sucursal = null;
            }

            ctrl.operacionAnterior = $scope.operacion.operacionAnterior;

            //Ajustar la fecha 
            if($scope.operacion.fecha){
                ctrl.fecha = fecha.fromSQL($scope.operacion.fecha);
            }else if($scope.operacion.$new) {
                ctrl.fecha = new Date;
                ctrl.vence = new Date;
            }

            if($scope.operacion.fecha_vencimiento) {
                ctrl.vence = fecha.fromSQL($scope.operacion.fecha_vencimiento);
            }

            //Cargar el Tercero
            if($scope.operacion.tercero_id) {
                Tercero.obtener($scope.operacion.tercero_id).then(function (tercero) {
                    ctrl.tercero = tercero[0];
                });
            }

            //Cargar la dirección
            
            if($scope.operacion.tercero_direccion_id) {
                Tercero.obtenerDirecciones($scope.operacion.tercero_id).then(function (direcciones) {

                    direcciones.forEach(function (d) {
                        if(d.direccion_id == $scope.operacion.tercero_direccion_id) {
                            ctrl.direccionFiscal = d;
                        }

                        if(d.direccion_id == $scope.operacion.direccion_envio) {
                            ctrl.direccionEnvio = d;
                        }
                    });

                    ctrl.seleccionarDireccionFiscal = ( $scope.operacion.direccion_envio == $scope.operacion.tercero_direccion_id);
                });
            }
            

            //Cargar el tipo de cambio
            if($scope.operacion.tipo_de_cambio) {
                ctrl.tipoDeCambio = oorden.tipoDeCambio[$scope.operacion.tipo_de_cambio];
            }


            if($scope.operacion.$new) {
                //Cuando la factura es nueva, los centros de costo son los activos
                oorden.centrosDeCostoActivos.forEach(function (cc) {
                    cc && ctrl.centrosDeCosto.push(cc);
                });
            } else if(ctrl.items[0]){
                //Cuando la factura ya existe lee los centros de costo de las partidas
                if(ctrl.items[0].ccto_1_id) {
                    ctrl.centrosDeCosto.push(oorden.centroDeCosto(ctrl.items[0].ccto_1_id));
                }
                if(ctrl.items[0].ccto_2_id) {
                    ctrl.centrosDeCosto.push(oorden.centroDeCosto(ctrl.items[0].ccto_2_id));
                }
            }

            
            if(ctrl.items.length == 0){
                //Si no hay items, inmediatamente está listo
                ctrl.$ready = true;
            } else {
                //Si hay items debe checar que todos se reporten listos
                testReadyWatcher = $scope.$watch(allItemCtrlsAreReady, function (isReady) {
                    if(!isReady) return;
                    testReadyWatcher();
                    actualizarOperacion(true);
                });
            }

            //Las columnas de la tabla
            actualizarColumnas();
            $scope.$watch(validar);
        }

        /**
         * checa si todos items están listos
         */
        function allItemCtrlsAreReady () {
            if(!itemCtrls.length) return false;
            return itemCtrls.reduce(function (o,i) {
                return o && Boolean(i.$ready);
            }, true);
        }

        /**
         * Devuelve un objeto de factura listo para el api
         */
        function obtenerFactura () {
            var factura = angular.copy($scope.operacion);

            delete(factura.operacionAnterior);

            if(factura.$new) {
                factura.estatus = 'P';
            }

            factura.numero = (factura.numero == '<auto>') ? null : factura.numero;

            factura.operacion_anterior_id = ctrl.operacionAnterior ? ctrl.operacionAnterior.operacion_id : null;

            factura.sucursal_id         = ctrl.sucursal ? ctrl.sucursal.sucursal_id : null;
            factura.fecha               = fecha.toSQL(ctrl.fecha);
            factura.fecha_vencimiento   = fecha.toSQL(ctrl.vence);
            factura.tercero_id          = ctrl.tercero ? ctrl.tercero.tercero_id : null;

            factura.tipo_de_cambio      = ctrl.tipoDeCambio ? ctrl.tipoDeCambio.tipo_de_cambio : null;
            factura.moneda              = ctrl.tipoDeCambio ? ctrl.tipoDeCambio.codigo_moneda: null;

            factura.tercero_direccion_id= ctrl.direccionFiscal ? ctrl.direccionFiscal.direccion_id : null
            factura.direccion_envio     = ctrl.direccionEnvio ? ctrl.direccionEnvio.direccion_id : null;
            factura.vendedor_id         = ctrl.vendedor ? ctrl.vendedor.usuario_id : null;

            factura.subtotal            = ctrl.subtotalAsInt / mult;
            factura.total               = ctrl.totalAsInt / mult;
            factura.saldo = factura.total;
            
            factura.descuentos          = ctrl.descuentoAsInt / mult;
            factura.impuestos           = ctrl.totalImpuestosAsInt / mult;
            factura.retenciones         = ctrl.totalRetencionesAsInt / mult;

            factura.tipo_operacion      = ctrl.tipoDeOperacion();
            factura.seccion             = ctrl.seccion();

            factura.eliminar            = ctrl.eliminar;



            factura.items = itemCtrls.filter(esIntegrable).map(function (itemCtrl) {
                return itemCtrl.getItem();
            });

            return factura;
        }

        function esIntegrable(itemCtrl) {
             return itemCtrl.$integrable === true;
        }

        /** 
         * para que los items de los controladores se registren
         */
        function registrarItemCtrl (itemCtrl) {
            itemCtrls.push(itemCtrl);
        }

        /**
         * para que los items de  los controladores se des-registren
         */
        function eliminarItemCtrl (itemCtrl) {
            var idx = itemCtrls.indexOf(itemCtrl);
            itemCtrls.splice(idx, 1);
        }


        function eliminarItem (item) {
            var idx = ctrl.items.indexOf(item);

            if(idx > -1) {
                ctrl.items.splice(idx,1);
                ctrl.eliminar.push(item.operacion_item_id);
            }
        }


        function eliminarTodosLosItems () {
            ctrl.items.forEach(function (item) {
                eliminarItem(item);
            });
        }


        function obtenerTasa () {
            var factura = $scope.operacion;
            factura.tasa_de_cambio = null;
            if(!ctrl.tipoDeCambio) return;

            oorden.obtenerTasa(ctrl.tipoDeCambio).then(function (tasa) {
                factura.tasa_de_cambio = tasa.tasa_de_cambio;
            });
        }

        function asignarTipoDeDocumento (tipoDoc) {
            $scope.operacion.serie = null;
            $scope.opCtrl.tipoDoc = tipoDoc;

           

            if(!tipoDoc) return;

            $scope.operacion.serie = tipoDoc.serie;
            $scope.operacion.numero =  $scope.operacion.numero ?  $scope.operacion.numero : null;
        }   

        function asignarTipoDeCambio () {
            obtenerTasa();
        }

        function asignarSucursal (sucursal) {
            ctrl.sucursal = sucursal;
            $scope.operacion.sucursal_id = sucursal.sucursal_id;
        }

        function asignarTercero (tercero) {
            if(ctrl.seccion() == 'V') return asignarCliente(tercero);
            if(ctrl.seccion() == 'C') return asignarProveedor(tercero);
        }

        function asignarCliente (cliente) {
            ctrl.tercero = cliente;

            if(cliente == null) {
                $scope.operacion.tercero_id = null;
                return;
            }

            $scope.operacion.tercero_id = cliente.tercero_id;

            //Asigno los dias de vencimiento
            diasDeCredito = Number(cliente.dias_credito_venta) || 0;
            asignarVencimiento();

        
            if(cliente.clave_moneda) {
                var d = oorden.tipoDeCambio[cliente.clave_moneda];

                if(d) {
                    ctrl.tipoDeCambio = d;
                } else {
                    ctrl.tipoDeCambio = oorden.tipoDeCambio.base;
                }
            }

            ctrl.items.forEach(function (item) {
                item.descuento = cliente.descuento;
            });
            
            
            Tercero.obtenerDirecciones(cliente.tercero_id).then(function (d) {
                ctrl.direcciones = d;

                ctrl.direcciones.forEach(function (dir) {
                    if(cliente.ultima_direccion_fiscal == dir.direccion_id) {
                        ctrl.direccionFiscal = dir;
                    }
                });
            });
        }


        function asignarProveedor (proveedor) {
            ctrl.tercero = proveedor;

            if(proveedor == null) {
                $scope.operacion.tercero_id = null;
                return;
            }

            $scope.operacion.tercero_id = proveedor.tercero_id;

            //Asigno los dias de vencimiento
            diasDeCredito = Number(proveedor.dias_credito_compra) || 0;
            asignarVencimiento();

        
            if(proveedor.clave_moneda) {
                var d = oorden.tipoDeCambio[proveedor.clave_moneda];

                if(d) {
                    ctrl.tipoDeCambio = d;
                } else {
                    ctrl.tipoDeCambio = oorden.tipoDeCambio.base;
                }
            }

            ctrl.items.forEach(function (item) {
                item.descuento = proveedor.descuento;
            });
            
          
            
            Tercero.obtenerDirecciones(proveedor.tercero_id).then(function (d) {
                ctrl.direcciones = d;

                ctrl.direcciones.forEach(function (dir) {
                    if(proveedor.ultima_direccion_fiscal == dir.direccion_id) {
                        ctrl.direccionFiscal = dir;
                    }
                });

    
            });
        }

        function asignarFecha (date) {
            ctrl.fecha = date;
            ctrl.fecha_sql = $scope.operacion.fecha = fecha.toSQL(date);
            asignarVencimiento();
        }

        function asignarVencimiento () {
            ctrl.vence = new Date(ctrl.fecha);
            ctrl.vence.setDate(ctrl.fecha.getDate() + Number(diasDeCredito));
            ctrl.vence_sql = $scope.operacion.vence = fecha.toSQL(ctrl.vence);
        }

        function actualizarColumnas () {
            var cctoLength = ctrl.centrosDeCosto.length;
            var factura = $scope.operacion;
            
            factura.columnas = totalColumnas - 4;

            factura.columnas += Number(factura.mostrar_cctos);
            factura.columnas += Number(factura.mostrar_descuento);
            factura.columnas += Number(factura.mostrar_cuentas);
            factura.columnas += Number(factura.mostrar_retenciones || factura.mostrar_impuestos);


            console.log( factura.columnas );
    

            factura.mostrar_contable = Boolean(factura.mostrar_cuenta) || Boolean(factura.mostrar_impuestos) || Boolean(factura.mostrar_retenciones);

            factura.nColumnas = totalNColumnas - 2;
            factura.nColumnas += Number(factura.mostrar_contable);
            factura.nColumnas += Number(factura.mostrar_cctos);

        
        };

        $scope.agregarItemDirecto = function ($event) {
            ctrl.items.push({ 
                operacion_item_id : uuid() ,
                descuento : ctrl.cliente ? Number(ctrl.cliente.descuento)  : 0
            });
        };

        function actualizarOperacion (actualizarItems) {
            var subtotal = 0;
            var descuentos = 0;
            var impuestosNoIncluidos = Number(!Boolean($scope.operacion.precios_con_impuestos));
            var sumaImportes = 0;

            itemCtrls.forEach(function (iCtrl) {
                actualizarItems && iCtrl.actualizar();

                subtotal    += iCtrl.sinImpuestosAsInt;
                descuentos  += iCtrl.montoDescuentoAsInt;
                sumaImportes+= iCtrl.importeAsInt;
            });


            actualizarImpuestos();
            actualizarRetenciones();

            ctrl.subtotalAsInt = subtotal;
            ctrl.sumaImportesAsInt = sumaImportes;
           
            if(impuestosNoIncluidos) {
                ctrl.totalAsInt = subtotal + ctrl.totalImpuestosAsInt - ctrl.totalRetencionesAsInt;
            } else {
                ctrl.totalAsInt = sumaImportes - ctrl.totalRetencionesAsInt;
            }

            ctrl.descuentoAsInt = descuentos;

     
        };

        

        function actualizarImpuestos () {
            var impuestos = {};
            var grupoImpuestos = [];
            var totalImpuestos = 0;

            itemCtrls.forEach(function (item) {
                item.impuestosAgrupados.forEach(function (grupo) {
                    if(!impuestos[grupo.nombre]) {
                        impuestos[grupo.nombre] = 0;
                    }
                    impuestos[grupo.nombre] += grupo.montoAsInt;
                    totalImpuestos          += grupo.montoAsInt;
                });
            });

            angular.forEach(impuestos, function (montoAsInt, grupo) {
                grupoImpuestos.push({
                    grupo : grupo, 
                    montoAsInt : montoAsInt,
                    monto : montoAsInt / mult
                });
            });

            ctrl.grupoImpuestos      = grupoImpuestos;
            ctrl.totalImpuestosAsInt = totalImpuestos;
        };

        function actualizarRetenciones () {
            var retenciones = {};
            var grupoRetenciones = [];
            var totalRetenciones = 0;

            itemCtrls.forEach(function (item) {
                item.retencionesAgrupadas.forEach(function (grupo) {
                    if(!retenciones[grupo.nombre]) {
                        retenciones[grupo.nombre] = 0;
                    }
                    retenciones[grupo.nombre] += grupo.montoAsInt;
                    totalRetenciones          += grupo.montoAsInt;
                });
            });

            angular.forEach(retenciones, function (montoAsInt, grupo) {
                grupoRetenciones.push({
                    grupo : grupo, 
                    montoAsInt : montoAsInt,
                    monto : montoAsInt / mult
                });
            });

            ctrl.grupoRetenciones      = grupoRetenciones;
            ctrl.totalRetencionesAsInt = totalRetenciones;
        }


        $scope.tcModificable = function () {
            if(!Operacion.tiposDeCambio) return;
            if(ctrl.operacionAnterior) return false;
            return Operacion.tiposDeCambio.length > 1;
        };

        $scope.monedaModificable = function () {
            return false;
        };

        $scope.tasaModificable = function () {
            if(!$scope.operacion) return;
            if(!ctrl.tipoDeCambio) return false;

            if(Number(ctrl.tipoDeCambio.base) == 1) return false;
            return true;
        };
    };
});