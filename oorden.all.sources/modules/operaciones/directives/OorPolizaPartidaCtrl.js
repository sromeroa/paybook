inject(CONTROLLER, 'oorPolizaPartidaCtrl', ['$scope'], function ($scope) {
     
    var PartidaCtrl = this;
  
    /**
     * Inicializa el concepto copiandolo desde el concepto de la póliza
     */
     $scope.incializarConcepto = function () {
         if($scope.partida.concepto === null || $scope.partida.concepto === '') {
            $scope.partida.concepto = $scope.PolizaCtrl.poliza().concepto;
         } 
    }


    $scope.ajustarDebeHaber = function () {
        var debe =  (Number($scope.partida.debe)  || 0 ) * 100;
        var haber = (Number($scope.partida.haber) || 0 ) * 100;

       if(debe) {
            $scope.partida.debe = (Math.round(debe)  / 100).toFixed(2);
        }

        if(haber) {
            $scope.partida.haber = (Math.round(haber) / 100).toFixed(2);
        }
    };

        /**
         * Asigna la propiedad $integrable, si corresponde
         */
    $scope.$watch(function (d) {
        $scope.partida.$integrable = Boolean($scope.partida.concepto) || Boolean($scope.partida.importe);
    });

  
});