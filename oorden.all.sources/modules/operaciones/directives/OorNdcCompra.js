inject(DIRECTIVE, 'oorNdcCompra', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-ndc-compra.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorNdcCompra'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{CNC : true}) ;
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'CNC';
        }

        function seccion () {
            return 'C'; 
        }

        function ccProducto (producto) {
            return producto && producto.cuenta_nc_compra;
        }
    };

});