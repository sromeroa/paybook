inject(DIRECTIVE, 'oorPoliza', ['Poliza','Organizacion', 'oorden'], function (Poliza, Organizacion, oorden) {
    
    'use strict';
    
    return  {
        templateUrl : '/partials/polizas/oor-poliza.html',
        restrict : 'A',
        controller : ['$scope', 'uuid', '$location','$attrs', OorPolizaCtrl],
        controllerAs : 'PolizaCtrl',
    };


    function OorPolizaCtrl ($scope, uuid, $location, $attrs) {
        var PolizaCtrl = this;
        var editar, editable;
        
        PolizaCtrl.$agregarNota = false;
        PolizaCtrl.$integrables = 0;

        PolizaCtrl.editable = function () { return editable; };
        PolizaCtrl.editar   = function () { return editar; };
        PolizaCtrl.poliza   = function () { return $scope.poliza; };

        $scope.Poliza = Poliza;


        angular.extend($scope, {
            asignarTipoDeCambio : asignarTipoDeCambio
        });


        /**
         * Inicialización
         */
        oorden.inicializar().then(function () {
            Organizacion.configuracion().then(function (d) {
                PolizaCtrl.mesesContables = d.mesesContables;
                Poliza.preparar().then(function () {
                    PolizaCtrl.$ready = true;
                    PolizaCtrl.tipoDeCambio = $scope.poliza.tipoDeCambio;
                });
            });
        });
        
        function obtenerTasa () {
            var factura = $scope.poliza;

            factura.tasa_de_cambio = null;
            if(!PolizaCtrl.tipoDeCambio) return;

            oorden.obtenerTasa(PolizaCtrl.tipoDeCambio).then(function (tasa) {
                factura.tasa_de_cambio = tasa.tasa_de_cambio;
            });
        }

        function asignarTipoDeCambio () {
            if($scope.poliza.tipoDeCambio.tipo_de_cambio != PolizaCtrl.tipoDeCambio.tipo_de_cambio) {
                
                $scope.poliza.tipoDeCambio = PolizaCtrl.tipoDeCambio;
                obtenerTasa();
                
            }
            
        }


        var removeReadyWatcher = $scope.$watch('PolizaCtrl.$ready', function (ready) {
            if(!ready) return;
            removeReadyWatcher();

            $scope.$watch('poliza.fecha', function (fecha) {
                if(!fecha) return;

                var mes = fecha.getMonth() + 1;

                //Mes contable segun la configuración de la organizacion
                var mesContable = PolizaCtrl.mesesContables.filter(function (m) { 
                    return Number(mes) === Number(m.mes) ;
                })[0];

                $scope.poliza.mes_contable = mesContable.mesContable;
            });


            //Cambia el mes contable, checar si se muestra o no el selector
            $scope.$watch('poliza.mes_contable', function (mesContable) {
                if(!mesContable) return; 
                PolizaCtrl.$mostrarMesContable = ['12','13','14'].indexOf(mesContable) > -1;
            });


            $scope.$watch($attrs.editar, function (e) { 
                editar = (e) ? true : false; 
            });

            $scope.$watch($attrs.editable, function (e) { 
                editable = (e) ? true : false; 
            });


            $scope.$watch(function () {
                if(!$scope.poliza) return;

                var integrables = $scope.poliza.partidas.reduce(function (a, partida) { 
                    return a + Number(partida.$integrable) 
                }, 0);

                if(editar && $scope.poliza.partidas.length == integrables) {
                    $scope.poliza.agregarPartida();
                }
            });
        });
    };

});