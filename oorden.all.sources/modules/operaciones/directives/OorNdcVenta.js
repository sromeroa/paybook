inject(DIRECTIVE, 'oorNdcVenta', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-ndc-venta.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorNdcVenta'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{VNC : true}) ;
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'VNC';
        }

        function seccion () {
            return 'V'; 
        }

        function ccProducto (producto) {
            return producto ? producto.cuenta_nc_venta : '';
        }
    };

});