inject(DIRECTIVE, 'oorPolizaPartida', [], function () {
    'use strict';

    return  {
        templateUrl : '/partials/polizas/oor-poliza-partida.html',
        scope : {
            partida : '='
        },
        restrict : 'A',
        require : '^oorPoliza',
        link : link,
        controller : ['$scope','OorPolizaPartidaCtrl'],
        controllerAs : 'PartidaCtrl',
    };

    function link (scope, element, attrs, oorPoliza) {
        //Asigna el controlador de Poliza
        scope.PolizaCtrl = oorPoliza;
    }
});