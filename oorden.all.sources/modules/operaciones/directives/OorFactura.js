inject(DIRECTIVE, 'oorFactura', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-factura.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorFacturaCtrl],
        require : ['^oorOperacion', 'oorFactura'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1], {VTA : true});
        ctrls[0].mount();
        
    }

    function OorFacturaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });

        function tipoDeOperacion () {
            return 'VTA'; //FACTURA "VENTAS"
        }

        function seccion () {
            return 'V'; //VENTAS
        }

        function ccProducto(producto) {
            return producto.cuenta_venta;
        }
    };

});