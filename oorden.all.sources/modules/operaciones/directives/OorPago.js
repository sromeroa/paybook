inject(DIRECTIVE, 'oorPago', ['oorden', 'uuid', 'fecha', 'oorDialogoTercero'], function (oorden, uuid, fecha, oorDialogoTercero) {
    'use strict';
    
    return  {
        link : link,
        terminal :true
    };

    function link (scope, element, attrs, ctrls) {
        var pagoComponent = {};

        var component = m.component(PagoComponent(), {
            cuentaId          : attrs.cuentaId,
            tipoOperacion     : 'EPP',
            seccion           : 'E',
            operacionId       : attrs.operacionId
        });

        /** 
         * La vista del componente de transaccion
         */ 
        pagoComponent.view = function () {
            return m('div', component);
        };

        /**
         * Inicializamos oorden y luego se carga el componente en el DOM
         */
        oorden.inicializar().then(function () {
            m.mount(element[0], pagoComponent);
        });
    };


    function defCol () {
        return {
            documento : {
                caption : 'Documento',
                value : function (item) {
                    return item.tipoDocumento.nombre.concat(' ', item.serie, '#',item.numero)
                }
            }
        };
    }

    function VMPago(args) {
        var operacion = args.pago;
        var form      = this;
        
        this.tercerosSelector = oor.tercerosSelect2({
            onchange : function (tercero_id) {
                operacion.asignarTercero( form.tercerosSelector.selected() );
                args.ctrl.cargarDocumentos();
            },
            model : args.pago.tercero_id
        });


    
        this.inputTercero = function () {
            return this.tercerosSelector.view()
        };
    }


    function VMDocInicializar (args) {
        var vmDoc = this;

        this.aplicar  = Modelo.numberProp(m.prop(),2);

        this.incluir  = function () {
            if(typeof vmDoc.aplicar() === 'undefined') {
                vmDoc.aplicar.fix( args.documento.saldo );
            }
        };
    }

    function PagoComponent () {
        return  {controller:controller, view:view};

        function controller (attrs) {
            this.pago = new modelo.Operacion({ operacion_id:uuid() });
            this.vm   = new VMPago({pago : this.pago, ctrl:this}); 

            /** 
             * Para seleccionar Terceros, al cambiar cargar los documentos que se le deben 
             */
            this.tercerosSelector = operacion.tercerosSelector();
            this.tercerosSelector.onchange = wrap(this.tercerosSelector.onchange, function (a) {
                this.call(ctrl.tercerosSelector,a);
                ctrl.cargarDocumentos();
            });

            var cols    = defCol();
            var docsVM  = {};
            var ctrl    = this;


            this.docVM = function (documento) {
                if(!docsVM[documento.operacion_id]) {
                    docsVM[documento.operacion_id] = new VMDocInicializar({ documento:documento });
                }
                return docsVM[documento.operacion_id];
            };


            ctrl.total = Modelo.numberProp(m.prop(),2);

            ctrl.actualizar = function () {
                var total = 0;
                ctrl.documentos.items().map(function (item) { 
                    var vmItem = ctrl.docVM(item);
                    total += vmItem.aplicar.$int();
                });
                ctrl.total.$int(total);
            };

            cols.aplicar = {
                caption : 'Aplicar',
                value : function (item) {
                    var docVM = ctrl.docVM(item);

                    return m('input.text-right', {
                        value : docVM.aplicar() ? docVM.aplicar() : '', 
                        onfocus : function () {
                            docVM.incluir();
                            ctrl.actualizar();
                        },
                        onchange : m.withAttr('value', function (v) {
                            docVM.aplicar(v);
                            ctrl.actualizar();
                        })
                    });
                }
            };


            cols.aplicar_m_base = {
                caption : 'Aplicar MXN',
                value : function (item) {
                    var docVM = ctrl.docVM(item);
                    return docVM.aplicar();
                }
            }

            this.documentos = MtTable({
                columnas : ['fecha','referencia','documento','saldo','moneda','aplicar','aplicar_m_base'],
                defCol : cols
            });


            this.documentos.tFoot = function () {
                return m('tr', [
                    m('td.text-right[colspan=6]', 'Total'),
                     m('td.text-right',  ctrl.total())
                ]);
            };

            this.cargarDocumentos = function () {
                m.request({
                    method:'GET',
                    url:'/apiv2?' + m.route.buildQueryString({
                        modelo:'operaciones', 
                        tercero_id:ctrl.pago.tercero_id(),
                        include : 'operaciones.tercero,operaciones.tipoDocumento'
                    })
                }).then(function (r) {
                    ctrl.documentos.items(
                        r.data
                            .filter(function (item) { return item.moneda == 'MXN'; })
                            .map(function (item) { item.checked = m.prop(false); return item; })
                    );
                });
            }
        }

        function view (ctrl, attrs) {
           return m('div', [
                m('div', ctrl.vm.inputTercero() ), 
                m('div', ctrl.documentos({style:'font-size:14px'}) )
            ]);
        }
    }

});
