inject(DIRECTIVE, 'oorSelectorTipoDeCambio', ['Organizacion', '$parse'], function (Organizacion, $parse) {
    
    'use strict';

    var template = '<div class="row">\
    <div class="col-sm-6" ng-show="$modificar">\
        <label> Tipo De Cambio </label>\
        <select ng-model="tcCtrl.tipoDeCambio" class="form-control" ng-options="tc.tipo_de_cambio as tc.tipo_de_cambio for tc in tiposDeCambio.tipos" ng-change="cargarTasa()">\
        <select>\
    </div>\
    <div class="col-sm-6" ng-hide="$modificar">\
        <label> Tipo De Cambio </label>\
        <input type="text" class="form-control" readonly ng-model="tcCtrl.tipoDeCambio">\
    </div>\
    <div class="col-sm-3">\
        <md-input-container flex="33">\
            <label>Tasa De Cambio</label>\
            <input ng-model="tcCtrl.tasaDeCambio" type="text" ng-readonly="!($modificar && $modificarTasa)" focus-at="$modificarTasa">\
        </md-input-container>\
    </div>\
     <div class="col-sm-3">\
        <div style="margin-top:20px;" class="pull-right">\
        <div class="btn btn-sm btn-success" ng-click="aplicar()" ng-show="$modificar">Aplicar</div>\
        <div class="btn btn-sm btn-default" ng-click="modificar(false)" ng-show="$modificar">Cancelar</div>\
        <div class="btn btn-sm btn-default" ng-click="modificar()" ng-show="modificable && !$modificar">Modificar</div>\
        </div>\
    </div>\
</div>';


    return  {
        scope : {
            gatillo : '=',
            fnAplicar : '&aplicar',
            fnCancelar : '&cancelar',
            modificable : '='
        },
        template : template,
        require: 'oorSelectorTipoDeCambio',
        link : link,
        controllerAs : 'tcCtrl',
        controller   : ['$scope','$attrs',controller]
    };

    function link (scope, element, attrs, tcCtrl) {
        //Consulta los tipos de cambio y los asigna al scope
        Organizacion.tiposDeCambio().then(function (tiposDeCambio) {
            scope.tiposDeCambio = tiposDeCambio;
            tcCtrl.inicializar();
        });
    }

    function controller ($scope, $attrs) { 
        var tcCtrl = this;
        var inicializado = false;

        tcCtrl.inicializar = function () {
            var tipoDeCambio      = $parse($attrs.tipoDeCambio)($scope.$parent);
            tcCtrl.tipoDeCambio   = tipoDeCambio.tipo_de_cambio;
            tcCtrl.tasaDeCambio   = $parse($attrs.tasaDeCambio)($scope.$parent);

            //Cada que cambia modifcar, actualiza los datos de afuera
            if(!inicializado) {
                $scope.$watch('$modificar', function () {
                   tcCtrl.inicializar();
                });
                inicializado = true;
            }
        }

        function tcActual () {
            return $scope.tiposDeCambio.tipos.filter(function (d) { 
                return d.tipo_de_cambio == tcCtrl.tipoDeCambio 
            })[0];
        }

        $scope.modificarTasa = function (input) {
            $scope.$modificarTasa = (input === false)? false : true;
        };

        $scope.modificar = function (input) {
            $scope.$modificar = (input === false)? false : true;
            $scope.$modificarTasa =  $scope.$modificar && Boolean(tcActual().base == '0');
        }

        $scope.cancelar = function () {
            $scope.fnCancelar();
        };

        $scope.aplicar = function () {
            $scope.fnAplicar({
                tipoDeCambio : tcActual(),
                tasaDeCambio : tcCtrl.tasaDeCambio
            });

            $scope.modificar(false);
        };

        $scope.cargarTasa = function () {
            var tc = tcActual();
            tcCtrl.tasaDeCambio = tc.tasa.tasa_de_cambio;
            $scope.modificarTasa(tc.base != '1');
         
        }


        $scope.$watch('gatillo', function (gatillo, gatilloOld) {
            if(!gatillo) return;
            if(!gatilloOld) {
                $scope.$evalAsync(function () {
                    $scope.modificar();
                });
            }
        });

        $scope.$watch('modificable', function (modificable) {
            if(!modificable) $scope.modificar(false);
        })

        

       
    }


});