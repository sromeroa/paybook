inject(DIRECTIVE, 'reporteAuxiliar', ['$parse'], function ($parse) {
    'use strict';

    return {
        templateUrl : '/partials/reportes/auxiliar.html',
        scope : {
            parametros : '='
        },
        controller : ['$scope', '$attrs', controller],
        controllerAs : 'reporte',
        require : 'reporteAuxiliar',
        link : link
    };

    function link (scope, element, attrs, reporte) {
        if(attrs.reporteNombre) {
            $parse(attrs.reporteNombre).assign(scope.$parent, reporte);
        }
    }


    function controller ($scope, $attrs) {
        var reporte = this;
        var inheritedParams = {};


        $scope.establecerDatos = function () {
            var provider = $scope.reporte.provider.data;

            $scope.movimientos = provider.movimientos;
            
            reporte.cuenta = $scope.cuenta  = provider.cuenta;

            $scope.saldo_inicial = provider.saldo_inicial;
            var totales = {"-1" : 0, '1' : 0};

            $scope.movimientos.forEach(function (m) {
                totales[m.debe_o_haber] += m.importe;
            });

            $scope.totales = {'debe' : totales['1'], 'haber' : totales['-1']};
        }

        var rmInitWatcher = $scope.$watch('parametros && reporte.provider', function (p) {
            if(!p) return;

            rmInitWatcher();

            $scope.$watch('parametros', function (parametros) {
                console.log('parametros');
                

                if(parametros) reporte.provider.load();
            });
            
        });
    } 
});


inject(DIRECTIVE, 'reporteAuxiliarFormulario', ['fecha', '$parse'], function (fecha, $parse) {

    return {
        templateUrl :  '/partials/reportes/auxiliar-formulario.html',
        scope : {},
        controller : ['$scope', '$attrs',controller],
        controllerAs : 'formulario'
    };

    function controller ($scope, $attrs) {
        var formulario = this;
    

        
        formulario.aplicar = function () {
            var params = null;

            if($scope.bFParametros && formulario.consulta && formulario.consulta.cuenta) {
                params = angular.extend({}, $scope.bFParametros, {
                    cuenta : formulario.consulta.cuenta
                });
            }

            formulario.parametros = params;

            $parse($attrs.parametros).assign($scope.$parent, angular.copy(params));
        };



        $scope.$watch('formulario.consulta.cuenta', function () {
            formulario.aplicar();
        });

        $scope.$watch('$parent.cuenta', function (cta) {
            console.log(cta);
            if(!cta)return;

            formulario.consulta = {
                cuenta : cta.cuenta_contable_id
            }
        });

        $scope.$watch('bFParametros', function () {
            console.log($scope.bFParametros);
            formulario.aplicar();
        }, true);
    }
});


