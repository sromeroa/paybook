inject(DIRECTIVE, 'reporteBalanza', ['$parse'], function ($parse) {
    'use strict';

    return {
        templateUrl : '/partials/reportes/balanza.html',
        scope : {
            parametros : '='
        },
        controller : ['$scope', '$attrs', controller],
        controllerAs : 'reporte'
    };


    function controller ($scope, $attrs) {
        var reporte = this;
        var cuentasByParent;

        /** BUSQUEDA **/
        var consulta;

        $scope.aplicarBusqueda = function (buscar) {
            if(buscar == null || buscar == ''){
                consulta = null;
                return;
            }
            consulta = angular.uppercase(buscar);
        };

        $scope.seleccionarCuenta = function (cta, $event) {
            $parse($attrs.seleccionarCuenta)($scope.$parent, {cuenta :cta, $event:$event })
        };

        $scope.ctaBusqueda = function (cta) {
            if(!consulta) return true;
            var str = angular.uppercase(cta.cuenta + ' ' + cta.nombre);
            return str.indexOf(consulta) >= 0;
        };

        /** CUENTAS */

        $scope.ajustarCuentas = function () {
            reporte.cuentas = reporte.provider.data.cuentas;
            ajustarCuentas(reporte.cuentas);
        }


        $scope.urlParams = function () {
            return  m.route.buildQueryString($scope.parametros);
            //return 'PARAMS';
        }


        var rmInitWatcher = $scope.$watch('parametros && reporte.provider', function (p) {
            if(!p) return;

            rmInitWatcher();

            $scope.$watch('parametros', function (parametros) {
                if(parametros) reporte.provider.load();
            });
            
        });

        function ajustarCuentas (cuentas) {
            reporte.nMax = 0;

            if(!cuentas) return;

            cuentasByParent = {};

            cuentas.forEach(function (cta) {
                var subcuenta_de = cta.subcuenta_de == null || cta.subcuenta_de == "" ? 0 : cta.subcuenta_de;
                if(!cuentasByParent[subcuenta_de]){
                    cuentasByParent[subcuenta_de] = [];
                }
                cuentasByParent[subcuenta_de].push(cta);

                if(Number(cta.nivel) > reporte.nMax){
                    reporte.nMax = Number(cta.nivel);
                }
            });

            cuentas.forEach(function (cta) {
                calcularTotales(cta);
            });
        }

        function calcularTotales (cta) {
            if(cta.saldos) return;
            
            cta.saldos = {
                saldo_inicial : 0,
                saldo_final : 0,
                debe : 0,
                haber : 0
            };

            var hijos = cuentasByParent[cta.cuenta_contable_id];
           
            if(!hijos) return;

            hijos.forEach(function (hijo) {
                calcularTotales(hijo);

                cta.saldos.saldo_inicial  += Number(hijo.saldos.saldo_inicial);
                cta.saldos.saldo_final    += Number(hijo.saldos.saldo_final);
                cta.saldos.debe           += Number(hijo.saldos.debe);
                cta.saldos.haber          += Number(hijo.saldos.haber);
            });
           
        }
    }
});

inject(DIRECTIVE, 'reporteBalanzaFormulario', ['fecha', '$parse'], function (fecha, $parse) {

    return {
        templateUrl :  '/partials/reportes/balanza-formulario.html',
        scope : {},
        controller : ['$scope', '$attrs',controller],
        controllerAs : 'formulario',
        link : link
    };

    function link (scope, elment, attrs) {
        $parse(attrs.formulario).assign(scope.$parent, scope.formulario);
    }

    function controller ($scope, $attrs) {
       var formulario = this;
       var date = new Date();
       var inheritedParams = {};

        $scope.consulta = { 
            tipo : 'contable',
            mes : fecha.to10(date.getMonth() + 1),
            ano : String(date.getFullYear())
        };

        angular.extend(inheritedParams, m.route.parseQueryString(location.search));


        if(inheritedParams.mes_contable) {
            $scope.consulta.mes = inheritedParams.mes_contable;

        }

        if(inheritedParams.ano_contable) {
            $scope.consulta.ano = inheritedParams.ano_contable;
        }


        var hoy = new Date;

        //ESTE MES -> asinga el mes actual y ejecuta el reporte
        $scope.esteMes = function (fechaReferencia) {
            angular.extend($scope.consulta, {
                tipo : 'fecha',
                fechaInicial : fecha.inicialMes(fechaReferencia || hoy),
                fechaFinal : fecha.finalMes(fechaReferencia || hoy)
            });

            $scope.aplicar();
        };


        //Este ANO -> asigna fechas de inicio y fin de año y genera
        $scope.esteAno = function (fechaReferencia) {
            angular.extend($scope.consulta, {
                tipo : 'fecha',
                fechaInicial : fecha.inicialAno(fechaReferencia || hoy),
                fechaFinal : fecha.finalAno(fechaReferencia || hoy)
            });

            $scope.aplicar();
        };

        $scope.busquedaContable = function () {
            var fMes = null;

            if(Number($scope.consulta.mes) < 13) {
                fMes = $scope.consulta.ano + '-' + $scope.consulta.mes + '-01';
                fMes = fecha.fromSQL(fMes);
            }

            angular.extend($scope.consulta,{
                tipo : 'contable',
                fechaInicial : fMes ? fecha.inicialMes(fMes) : null,
                fechaFinal : fMes ? fecha.finalMes(fMes) : null
            });

            $scope.aplicar();
        }

        $scope.aplicar = function () {
            formulario.consulta = angular.copy($scope.consulta);
            formulario.parametros = getParams(formulario.consulta);
            $parse($attrs.parametros).assign($scope.$parent, formulario.parametros);
        };


        $scope.busquedaContable();
    }

    //Funcion que convierte de busqueda  a parametros de consulta
    function getParams (origin) {
        var params = {};
        
        if(origin.tipo == 'fecha') {
            params.fecha_inicial = fecha.toSQL(origin.fechaInicial)
            params.fecha_final = fecha.toSQL(origin.fechaFinal)
        } else {
            params.mes_contable = origin.mes;
            params.ano_contable = origin.ano;

        }

        return params;
    }
});

