inject(DIRECTIVE, 'oorPeriodoContable', [], function () {
    
    'use strict';

    return  {
        scope : {
            mesContable : '=',
            anoContable : '='
        },

        templateUrl : '/partials/base/periodo-contable.html',
        controller : ['$scope', '$parse', '$attrs', 'Organizacion', oorSelectorMesesContablesCtrl],
        link : link
    };

    function link (scope, element, attrs, ngModel) {
        
    }

    function oorSelectorMesesContablesCtrl ($scope, $parse, $attrs, Organizacion) {
        
        $scope.seleccionarMes = function (mes, ano, notifyChange) {
            if(mes) {
                $scope.mesSeleccionado = mes;
                $parse($attrs.mesContable).assign($scope.$parent, mes.mes);
            }
            
            if(ano) {
                $parse($attrs.anoContable).assign($scope.$parent, ano);
            }

            if(notifyChange !== false) {
                $scope.$evalAsync(function () {
                    $parse($attrs.onChange)($scope.$parent);
                });
            }
        };

        function getMes (iMes) {
            return $scope.meses.filter(function (mes) {
                return mes.mes == iMes;
            })[0];
        }

        $scope.siguienteMes = function () {
            var mes = getMes($scope.mesSeleccionado.mes);
            var idx = $scope.meses.indexOf(mes);
            var ano = $scope.anoContable;
          
            if(++idx == $scope.meses.length) {
                idx = 0;
                ano++;
            }

            $scope.seleccionarMes($scope.meses[idx],ano);
        };

        $scope.anteriorMes = function () {
            var mes = getMes($scope.mesSeleccionado.mes);
            var idx = $scope.meses.indexOf(mes);
            var ano = $scope.anoContable;
            
            if(--idx < 0) {
                idx = $scope.meses.length - 1;
                ano--;
            }
            
            $scope.seleccionarMes($scope.meses[idx], ano);
        };

        $scope.anteriorAno = function () {
            var ano = $scope.anoContable;
            $scope.seleccionarMes(null, --ano);
        };

        $scope.siguienteAno = function () {
            var ano = $scope.anoContable;
            $scope.seleccionarMes(null, ++ano);
        };

        Organizacion.configuracion().then(function (cfg) {
            $scope.meses = cfg.mesesContables;
            $scope.$watch('mesContable', function (mes) {
                $scope.mesSeleccionado = getMes(mes);
            });
        });
    };
});