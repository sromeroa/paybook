inject(DIRECTIVE, 'oorPoliza', ['oorden', 'uuid', 'fecha', 'oorDialogoTercero'], function (oorden, uuid, fecha, oorDialogoTercero) {
    'use strict';

    return  {
        link : link,
        terminal :true
    };

    function link (scope, element, attrs, ctrls) {
        
        var polizaComp = m.component(operaciones.PolizaComponent, {
            polizaId : attrs.polizaId
        });


        console.log( operaciones.PolizaComponent );

        /**
         * Inicializamos oorden y luego se carga el componente en el DOM
         */
        oorden.inicializar().then(function () {
            m.mount(element[0], polizaComp);
        });
    };

});
