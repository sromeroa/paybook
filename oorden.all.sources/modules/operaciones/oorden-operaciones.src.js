/* MODULO Operaciones */
(function () {

var module = angular.module('oordenOperaciones', ['oordenBase']);
var inject = createInjector();

import './controllers/OperacionesDeCuentaCtrl.js';
/**
 * POLIZAS
 */
//import './controllers/OorPolizaPantallaCtrl.js';
import './directives/OorPolizaMT.js';
//import './directives/OorPolizaPartidaCtrl.js';
//import './directives/OorPolizaPartida.js';

/**
* OPERACIONES
*/
import './controllers/OorOperacionPantallaCtrl.js';
import './controllers/OorOperacionItemCtrl.js';
//DIRECTIVAS
import './directives/OorOperacion.js';
import './directives/OorFactura.js';
import './directives/OorNdcVenta.js';

import './controllers/OorNdcFacturaCtrl.js';
import './directives/OorCompra.js';
import './directives/OorNdcCompra.js';

import './directives/OorTransaccion.js';
//import './directives/OorPago.js';


/**
 * REPORTES
 */
//import './directives/reportes/reporteBalanza.js';
//import './directives/reportes/reporteAuxiliar.js';
//import './controllers/OorReporteBalanza.js';


/**
* AUTOCOMPLETES
*/
import './controllers/TercerosAutoCompleteCtrl.js';
import './controllers/ProductosAutoCompleteCtrl.js';
import './controllers/CtasContablesAutoCompleteCtrl.js';
import './controllers/ImpuestosAutoCompleteCtrl.js';
import './controllers/RetencionesAutoCompleteCtrl.js';
import './controllers/SucursalesAutoCompleteCtrl.js';
import './controllers/VendedoresAutoCompleteCtrl.js';
import './controllers/TiposDeDocumentoAutoCompleteCtrl.js';
import './controllers/TiposDeCambioAutoCompleteCtrl.js';

/**
* SELECTORES
*/

//import './directives/OorSelectorMesesContables.js';
//import './directives/selectores/oorSelectorTipoDeCambio.js';

inject.mount(module);

})();
/* FIN MODULO Operaciones */
