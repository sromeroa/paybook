var gulp       = require('gulp');
var shell      = require('gulp-shell');
var uglify     = require('gulp-uglify');
var browserify = require('gulp-browserify');
var livereload = require('gulp-livereload');
var exorcist   = require('exorcist');
var path = require('path');
var mapfile = path.join(__dirname, 'dist/oorden.js.map');
var transform = require('vinyl-transform');
var gutil = require('gulp-util');



//var jsmin = require('gulp-jsmin');
//var rename = require('gulp-rename');

gulp.task('default', ['concat','scripts'], function() {
    gulp.watch('src/**/*.js', ['scripts']);
    livereload.listen();
});


//Smash TASK
gulp.task('concat', shell.task([
    './node_modules/smash/smash oorden.all.sources/oorden.all.src.js > dist/oorden.all.js'
]));


gulp.task('scripts', function() {
    // Single entry point to browserify
    gulp.src('src/oorden.js')
        .pipe( browserify({insertGlobals:false,debug:true}) )
        //.pipe(transform(function () {
        //    return exorcist('dist/oorden.js.map');
        //}))
        .pipe( gulp.dest('./dist') )
        .pipe( livereload() );


    gulp.src('src/oorden-v1.0.js')
        .pipe( browserify({insertGlobals:false,debug:true}) )
        .pipe( gulp.dest('./dist') )
        .pipe( livereload() );
});


gulp.task('compress', ['concat','scripts'], function() {
    /*
    gulp.src('out/oorden.all.js')
        .pipe(uglify())
        .pipe(gulp.dest('out/min'))
        */
    gulp.src('dist/oorden.js')
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(gulp.dest('dist/min'))
});


// gulp.task('default', function () {
//     gulp.src('src/**/*.js')
//         .pipe(jsmin())
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest('dist'));
// });
