module.exports = {
    CuentaContable : require('./modelos/CuentaContable.js'),
    Operacion : require('./modelos/Operacion.js'),
    //OperacionItem : require('./modelos/OperacionItem.js'),
    TipoDeDocumento : require('./modelos/TipoDeDocumento.js')
};

require('./AplicacionDePago.js');
require('./Operacion.js');
require('./OperacionItem.js');
require('./Poliza.js');
require('./Partida.js');
require('./Tercero.js');
require('./Direccion.js');