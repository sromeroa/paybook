module.exports = MtModal;

MtModal.opened = m.prop([]);

MtModal.open = function (component) {
    MtModal.opened().push(MtModal(component));
 }

function MtModal (component) {
    function view(ctrl) {
        return m('.mt-modal', _.extend({config:open}, component.modalAttrs), [
            m('.mt-modal-top', top(ctrl)),
            m('.mt-modal-content', content(ctrl)),
            m('.mt-modal-bottom', bottom(ctrl))
        ])
    }


    function open (element, isInitialized) {
        if(!isInitialized && component.el) {
            modal.element = element;

            var el = component.el;
            var elRect= el.getBoundingClientRect();

            modal.elRect = elRect;

            var width = element.offsetWidth;
            var height = element.offsetHeight;

            var bodyHeight = document.body.offsetHeight;
            var bodyWidth = document.body.offsetWidth;


            element.style.top = ''.concat(elRect.top,'px');
            element.style['margin-top'] = 0;

            element.style.left = ''.concat(elRect.left,'px');
            element.style['margin-left'] = 0;

            modal.zoom = 'scale('
            modal.zoom += elRect.width / width;
            modal.zoom += ', ';
            modal.zoom += elRect.height / height;
            modal.zoom += ')';

            d3.select(element)
                .style('transform', modal.zoom)
                .style('transform-origin', '0 0')
                .style('opacity', 0)
                .transition().duration(500)
                    .style('transform', 'scale(1, 1)')
                    .style('opacity', 1)
                    .style('left', '' + (bodyWidth-width)/2 + 'px' )
                    .style('top', ''.concat((bodyHeight-height)/2,'px'))
        }
    }

    function content (ctrl) {
        if (typeof component.content === 'function') {
            return component.content(ctrl);
        }
        return component.content;
    }


    function top (ctrl) {
        if (typeof component.top === 'function') {
            return component.top(ctrl);
        }
        return component.top;
    }

    function bottom (ctrl) {
        if (typeof component.bottom === 'function') {
            return component.bottom(ctrl);
        }
        return component.bottom;
    }

    function closeModal() {
        var idx = MtModal.opened().indexOf(modal);
        if(idx > -1) MtModal.opened().splice(idx,1);
    }

    function close () {
        if(!component.el || !modal.element) return closeModal();

        //m.startComputation();

        d3.select(modal.element)
            .transition().duration(500)
                .style('opacity', 0)
                .tween('transform', function () {
                    var i = d3.interpolate('scale(1, 1)', modal.zoom) ;
                    return function (t) { this.style['transform'] = i(t); }
                })
                .style('top', ''.concat(modal.elRect.top + 'px'))
                .style('left', ''.concat(modal.elRect.left + 'px'))
                .each('end', _.once(function () {
                    closeModal();
                    m.redraw();
                }))

    }

    var modal = {
        controller : function () {
            var ctrl = new component.controller(component.args);
            ctrl.$modal = modal;
            return ctrl;
        },
        view : view,
        close : close
    }


    console.log(modal)

    return modal;
}

MtModal.initialize = function () {
    $(document).keyup(function (ev) {
        if(ev.keyCode == 27 && MtModal.opened().length) {
            MtModal.opened()[MtModal.opened().length -1].close()
        }
    })

    MtModal.initialize = nh.noop;
}

MtModal.view = function () {
    MtModal.initialize();
    return MtModal.opened().length ? [MtModal.overlay(), MtModal.opened()] : '';
}

MtModal.overlay = function () { return m('div.mt-modal-overlay') }
