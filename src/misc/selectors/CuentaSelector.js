var CuentaSelector = module.exports = {};
var CrearCuenta = require('../../ajustes/cuentas-contables/crearCuenta');
var ASSelector  = require('../../misc/ASSelector');

var AGREGAR_CUENTA = 'AGREGAR_CUENTA';


function buildRecord (d) {
    var nombre =  (d[1] || '');
    return {
        cuenta_contable_id: d[4],

        cuenta : d[0],
        nombre :nombre,
        estatus : d[2],
        tacum : d[3],

        search : (d[0] + ' ' + nombre).toLowerCase().latinize(),
        searchCuenta : d[0].split('-').join('').split('.').join('')
    };
}


CuentaSelector.cuentas = m.prop();

CuentaSelector.cargando = m.prop(false);

CuentaSelector.cargar = function () {
    if(CuentaSelector.cuentas() || CuentaSelector.cargando()) {
        return false;
    }

    CuentaSelector.cargando(true);

    return oor.request('/cuentas-contables/datos?ids=true')
        .then(function (r) {
            CuentaSelector.cuentas(true);
            CuentaSelector.cargando(false);
            CuentaSelector.childrenMap = {};

            var root;

            Object.keys(r).forEach(function (name) {
                if(name == 'root') {
                     root = r[name];
                     root.search = '';
                     return;
                }
                CuentaSelector.childrenMap[name] = r[name].map(buildRecord);
                CuentaSelector.childrenMap[name] = CuentaSelector.childrenMap[name].sort(function (d1,d2) {
                    return d2.cuenta > d1.cuenta ? -1 : 1
                });
            });

            var tree = d3.layout.tree().children(function (d) {
                return CuentaSelector.childrenMap[d.cuenta || d.id];
            });


            var maxDepth = 0;

            CuentaSelector.cuentas(tree.nodes(root).filter(function (cta) {
                if(maxDepth < cta.depth) {
                    maxDepth = cta.depth;
                }
                return cta.depth > 2;
            }));

            CuentaSelector.maxDepth = m.prop(maxDepth);
        });
}




function K (v) {
    return function () { return v; }
}

function fTacum(v) {
    return function (d) { return (d.data && d.data.tacum) === v; }
}



CuentaSelector.controller = function (attrs) {
    var ctx = this;

    if(['*', 'D', 'A'].indexOf(attrs.seleccionar) == -1) {
        console.log('seleccionar inválido:', attrs.seleccionar)
        attrs.seleccionar = 'D';
    }

    ctx.FILTER =  attrs.seleccionar === '*' ? K(true) : fTacum(attrs.seleccionar);
    ctx.DETALLE = attrs.seleccionar === 'D' ? true : false;

    ctx.config = CuentaSelector.config(ctx);
    ctx.cuenta = m.prop(attrs.cuenta || null);
    ctx.model  = attrs.model;
    ctx.id     = attrs.id ? attrs.id : f('cuenta_contable_id');

    ctx.onchange = function () {
        if(this.value() == AGREGAR_CUENTA) {
            var oldValue = ctx.cuenta();

            ctx.cuenta(null);
            this.value(AGREGAR_CUENTA,'');

            CuentaSelector.openModal(ctx,
                CuentaSelector.oncancel(ctx, this, oldValue),
                CuentaSelector.onsave(ctx, this)
            );

            m.redraw();
            return;
        }

        ctx.cuenta( this.selected().data );
        if(ctx.onchange.mute()) return;
        ctx.model( this.value() );

        if(attrs.onchange) {
            attrs.onchange(this.value(), this.selected());
        }

        m.redraw();
    }

    ctx.onchange.mute = m.prop(false);
}

CuentaSelector.view  = function (ctx, attrs) {
    CuentaSelector.cargar();

    return CuentaSelector.cargando() ? oor.loading() : m('div', {config:CuentaSelector.configGlobal}, [
        attrs.disabled ? m('input[type=text][readonly]', {
            style:'border-color:transparent;',
            value: ctx.cuenta() ? ctx.cuenta().cuenta : ''
        }) : null,

        (!attrs.disabled) ?  m('input.si[type=text]', {
            style:(attrs.readonly ? 'display:none': ''),
            config:ctx.config
        }) : null,

        m('div.small', ctx.cuenta() ? ctx.cuenta().nombre : '')
    ]);
}

CuentaSelector.configGlobal = function (el, isInitialized) {
    if(isInitialized) return;

    el.addEventListener('click', function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        var input = el.querySelector('input.si');
        input && input.focus();
    })
}

CuentaSelector.config = function (ctx) {
    var selector;
    return function (el, isInitialized) {

        if(isInitialized) {
            if(ctx.model() != selector.value()) {

                ctx.onchange.mute(true);
                selector.selectId(ctx.model());
                ctx.onchange.mute(false);

                //Actualizo el caption
                d3.select(el.parentNode).select('div.small')
                    .html( (selector.selected()) ? selector.selected().nombre : '');
            }

            return;
        }

        var data =  CuentaSelector.cuentas().map(function (d) {
            var c = {};
            c.data         = d;
            c.id           = ctx.id(d);
            c.text         = d.cuenta;
            c.cuenta       = d.cuenta;
            c.nombre       = d.nombre;
            c.search       = d.search;
            c.searchCuenta = d.searchCuenta;
            return c;
        });



        selector = ASSelector(el, {
            onchange : ctx.onchange,
            data :data
        });

        selector.afterUpdate = function (selection,items) {
            items.classed('disabled', function (d) { return d.$selectable === false });
            items.style('padding-left',  function (d) {
                return String(10 * (d.depth - 2)).concat('px')
            })
        }

        selector.computer(function (sel, done) {
            var open         = sel.searching() === false;
            var searchString = sel.searchString() && sel.searchString().toLowerCase().latinize();
            var ctas         = sel.getData();
            var searchIsNumber = searchString == Number(searchString);
            var show = {};

            if((open && !sel.value()) || sel.searching() && searchString.length < 2) {
                sel.message().html('<h6>Escribe al menos 2 caracteres para buscar una cuenta</h6>');
            } else {
                sel.message().html('');
            }

            ctas.filter(ctx.FILTER).forEach(BUSQUEDA);


            if(ctx.DETALLE) {
                d3.range(1, CuentaSelector.maxDepth()).forEach(function (iDepth) {
                    var depth    = CuentaSelector.maxDepth() - iDepth;
                    var lCuentas = ctas.filter(function (d) {return (d.data && d.data.tacum) === 'A' && d.data.depth === depth; });

                    lCuentas.forEach(function (d) {
                        d.$show = hasVisibleChildren(d);
                        d.$selectable = false;
                    });
                });
            }


            function BUSQUEDA (cta) {
                if(!cta.search) return;

                cta.$selectable = true;
                if(open) {
                    cta.$show = cta.id === sel.value()
                } else {
                    if(searchString && searchString.length >= 2) {
                        cta.$show = Boolean(cta.search.indexOf(searchString) + 1);
                        if(!cta.$show && searchIsNumber) {
                            cta.$show = cta.searchCuenta.indexOf(searchString) === 0;
                        }
                    } else {
                        cta.$show = false;
                    }
                }

                if(cta.data && cta.data.cuenta) {
                    show[cta.data.cuenta] = cta.$show;
                }
            }

            function hasVisibleChildren (d) {
                if(d.data.children && d.data.children.length) {
                    for(var i=0; i< d.data.children.length; i++) {
                        if(show[d.data.children[i].cuenta]) {
                            return true;
                        }
                    }
                }
                return false;
            }




            if(!ctas[AGREGAR_CUENTA]) {
                ctas[AGREGAR_CUENTA] = true;
                ctas.push({
                    id : AGREGAR_CUENTA,
                    text : '',
                    cuenta : '',
                    nombre : 'Crear Nueva Cuenta...',
                    $selectable : true,
                    $show : true
                });
            }

            done(ctas);
        });


        selector.onchange(ctx.onchange);

        selector.html(function (d) {
            var style= "padding:5px 0px;line-height:1.1em; margin:0px;";
            
            if(d.$selectable == false) {
                style = style + 'white-space:nowrap; overflow:hidden; text-overflow:ellipsis;';
            }
            return '<h6 style="' + style + '">' + d.cuenta + ' <span style="font-size:12px">' + d.nombre + '</span>' +'</div>';
        });


        if(ctx.model()) {
            ctx.onchange.mute(true);

            selector.selectId(ctx.model());
            //Actualizo el caption
            d3.select(el.parentNode).select('div.small').html(selector.selected() ? selector.selected().nombre : '');
            ctx.onchange.mute(false);
            selector.updateInput();
        }
    }
}





CuentaSelector.openModal = function (parentCtx, oncancel, onsave) {
    MTModal.open({
        controller : CrearCuenta.controller,
        content : CrearCuenta.view,
        top : function (ctx) {
            return [
                m('.close', {onclick:ctx.cancel}, m.trust('&times;')),
                m('h5', 'Crear Cuenta')
            ];
        },
        bottom : function (ctx) {
            return [
                ctx.infoEsValida() ? oor.stripedButton('button.btn-success.pull-right', 'Guardar', {
                    onclick : ctx.guardarCuenta
                }) : null
            ]
        },
        args : {
            oncancel : oncancel,
            onsave : onsave
        },
        modalAttrs : {
            'class' : 'modal-medium'
        }
    });
}

var endebug = m.prop(false);

CuentaSelector.onsave = function (parentCtx, selector) {
    return function (cuenta) {
        var modal = this.$modal;
        CuentaSelector.cuentas(null);
        CuentaSelector.cargar().then(function (d) {
            parentCtx.model( parentCtx.id(cuenta) );
            selector.setData( CuentaSelector.cuentas().map(_.identity) );
            selector.value(null);
            toastr.success('¡Cuenta Creada!');
            modal.close();
        });
    }
}

CuentaSelector.oncancel = function (parentCtx, selector) {
    return function () {
        this.$modal.close();
    }
}
