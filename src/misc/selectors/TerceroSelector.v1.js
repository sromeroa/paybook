var TerceroSelector = module.exports = {};
var ASSelector  = require('../../misc/ASSelector');

TerceroSelector.oninit = function (vnode) {
    this.disabled = m.prop( Boolean(vnode.attrs.tercero) );
    this.setFocus = m.prop(false);

    this.setNull = function () {
        vnode.state.disabled(false);
        vnode.state.setFocus(true);
    };
};

TerceroSelector.getData = function (runCallback) {
    if(! TerceroSelector.data ) {
        TerceroSelector.data = oor.request('/apiv2?modelo=terceros')
    }
    if(TerceroSelector.data() == undefined)  {
        TerceroSelector.data.run(runCallback);
    } else {
        runCallback( TerceroSelector.data() );
    }
}

TerceroSelector.oncreate = function (vnode) {
    vnode.state.selector = CreateSelector(vnode);
};


TerceroSelector.onupdate = function (vnode) {
    if(vnode.state.setFocus() == true) {
        vnode.state.setFocus(false);
        requestAnimationFrame(function () { vnode.dom.querySelector('input.tsa').focus()});
    }
};



TerceroSelector.view = function (vnode) {
    return m('div.oor-selector-tercero', [
        m('.mt-inputer', [
            m('label', 'Cliente: '),

            m('input[type=text].tsa', {
                disabled : this.disabled()
            }),

            m('button.btn-flat.btn-xs.pull-left',{
                style : {
                    'margin-right':'10px',
                    'display' : this.disabled() ? 'inherit' : 'none'
                },
                onclick : this.setNull
            },'×'),
        ]),
        vnode.attrs.model() ? vnode.state.terceroView(vnode) : null
    ]);
};


TerceroSelector.terceroView = function (vnode) {
    return m('[style=padding:10px]', [
        m('h5', {style:'margin:0px'}, [
            m('strong', vnode.attrs.tercero.codigo),
            ' — ',
            vnode.attrs.tercero.nombre
        ]),
        m('div', {style:'padding-top:4px'},[
            m('span.text-grey.small', 'RFC: '),
            m('span', vnode.attrs.tercero.clave_fiscal),
        ])
    ])
}


function CreateSelector (vnode) {
    var tSelector = ASSelector(vnode.dom.querySelector('input.tsa'), {});

    tSelector.computer(SelectorComputer);

    tSelector.onchange(function () {
        vnode.state.disabled(true)
        vnode.attrs.onchange.call(tSelector);
        m.redraw();
    });

    tSelector.html(function (d) {
        var str = '<div><h5 style="margin:0"><strong>'  + d.data.codigo + '</strong>';
        str += ' — ' + d.data.nombre;
        str += '</h5>';
        str += '<h6>' + d.data.clave_fiscal ? d.data.clave_fiscal : '' + '</h6>'
        return  str;
    });

    tSelector.value(vnode.attrs.model(), vnode.attrs.tercero ? vnode.attrs.tercero.codigo : '');
    tSelector.updateInput();
}


function SelectorComputer (selector, done) {
    selector.message().html('cargando datos...');

    TerceroSelector.getData(function (iData) {
        selector.message().html('')

        var open  = selector.searching() === false;
        var searchString = selector.searchString();

        var data = iData.map(function (d) {
            return {data:d, id:d.tercero_id, text:d.codigo, $selectable:true, $show:true, search: String(d.codigo + ' ' + d.nombre + ' ' + d.clave_fiscal).toLowerCase() };
        });

        if(open && selector.value()) {
            data.forEach(function (d){ d.$show = selector.value() == d.id });
        } else {
            searchString = searchString.toLowerCase().latinize();
            data.forEach(function (d) { d.$show = d.search.indexOf(searchString) > -1; });
        }

        selector.setData(data);
        done(data);
    });


}
