var FechasMultiSelector = module.exports = {};

FechasMultiSelector.controller = function (args) {
    this.tipo         = args.tipo
    this.mes_contable = args.mes_contable;
    this.ano_contable = args.ano_contable;
    this.desde        = args.desde;
    this.hasta        = args.hasta;
    this.mostrarFiscal = m.prop();

    this.mostrarFiscal(typeof args.mostrarFiscal == 'undefined' ? true : false);

    this.options = m.prop([
        {name:'P. Fiscal', val:'fiscal'},
        {name:'Rango de Fechas', val:'fechas'}
    ]);

    this.change =function (key, val) {
        this[key](this.cast(val, key));
        if(typeof args.onchange == 'function') {
            args.onchange();
        }
    }


    this.cast = function (val, key) {
        if(val && typeof this['cast_' + key] == 'function') {
            return this['cast_' + key](val);
        }
        return val;
    }
    
    this.cast_mes_contable = function(mes) {
        return Math.min(14,Math.max(1,Number(mes)));
    };


}

FechasMultiSelector.view = function (ctrl) {
    return m('div', [

         m('div.row', {style:'padding:12px 0;font-size:14px'}, [
            m('div.col-xs-12.text-left', [
                m('span', 'Filtrar por: '),
                ctrl.mostrarFiscal() ? m('span.label[style="cursor:pointer;margin:4px"]', {
                    onclick: ctrl.change.bind(ctrl,'tipo','fiscal'),
                    'class' : ctrl.tipo() == 'fiscal' ? 'label-primary' : 'label-default'
                }, 'P. Fiscal'): '',
                m('span.label[style="cursor:pointer;margin:4px"]', {
                    onclick: ctrl.change.bind(ctrl,'tipo','fechas'),
                    'class' : ctrl.tipo() == 'fechas' ? 'label-primary' : 'label-default'
                },'Rango Fechas'),
                //m('span.text-blue-grey[style="padding:0 12px;cursor:pointer"]', 'Este Año'),
                //m('span.text-blue-grey[style="padding:0 12px;cursor:pointer"]', 'Este Mes')
                //m('span.text-blue-grey[style="padding:0 12px;cursor:pointer"]', m('i.ion-android-arrow-back')),
                //m('span.text-blue-grey[style="padding:0 12px;cursor:pointer"]', m('i.ion-android-arrow-forward')),
            ])
        ]),

        ctrl.tipo() === 'fiscal' ? m('div.flex-row', {style:'margin:0'},[
                m('.mt-inputer', [
                    m('label', 'Mes:'),
                    m('input.dextras[type="number"][name=am]',  {
                        style:'width:40px',
                        onchange : m.withAttr('value',ctrl.change.bind(ctrl, 'mes_contable')),
                        value: ctrl.mes_contable(),
                        id: 'mesCont'
                    }),
                ]),

                m('.mt-inputer', [
                    m('label', 'Año:'),
                    m('input.dextras[type="number"][size=4][name=ac]', {
                        style:'width:80px',
                        onchange : m.withAttr('value',ctrl.change.bind(ctrl, 'ano_contable')),
                        value: ctrl.ano_contable(),
                        id: 'anoCont'
                    })
                ])
        ]) : '',


        ctrl.tipo() === 'fechas' ? m('div.flex-row', {style:'margin:0'},[
            m('.mt-inputer', [
                m('label', 'Desde'),
                m('input.dextras[type="date"][name=fi]', {
                    style:'width:150px',
                    onchange : m.withAttr('value', ctrl.change.bind(ctrl, 'desde')),
                    value : ctrl.desde()
                })
            ]),

            m('.mt-inputer', [
                m('label', 'Hasta'),
                m('input.dextras[type="date"][name=ff]', {
                    style:'width:150px',
                    onchange : m.withAttr('value', ctrl.change.bind(ctrl, 'hasta')),
                    value : ctrl.hasta()
                })
            ])
        ]) : ''

    ]);
};
