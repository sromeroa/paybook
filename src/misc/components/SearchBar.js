
var SearchBar = module.exports = {};

SearchBar.controller  = function (args) {
    var ctx = this;

    ctx.internalSearch = m.prop();
    ctx.search = args.search;
    ctx.internalSearch( ctx.search() );

    ctx.placeholder = m.prop(args.placeholder || 'Buscar')

    var searchDebounced = _.debounce(function () {
        ctx.performSearch( ctx.internalSearch() );
    }, 500);

    ctx.performSearch = function (val) {
        ctx.internalSearch(val)
        ctx.search(val)

        if(args.onsearch) {
            args.onsearch();
        }
    };

    ctx.bindEvent = function (element, isInitialized) {
        if(args.autofocus) oor.autofocus(element, isInitialized);
        if(isInitialized) return;

        element.addEventListener('input', function () {
            var value = this.value;
            ctx.internalSearch(value);
            searchDebounced();
        });
    }
}


SearchBar.view = function (ctx) {
    return m('.mt-inputer', {style:'flex:1 2'}, [
        m('label', {style:'font-size:15px'}, m('i.ion-ios-search')),
        m('input[type=text]', {
            style : 'height:23px',
            placeholder : ctx.placeholder(),
            value : ctx.internalSearch(),
            config : ctx.bindEvent,
            id : 'search',
            onchange : m.withAttr('value', ctx.performSearch)
        })
    ])
}
