var FechasBar = require('./FechasBar2');
var CuentasSelector = require('../selectors/CuentaSelector');

module.exports = {
    controller: FiltrosReporteCtrl,
    view:FiltrosReporteView,
    aplicar : aplicar,
    queryParams : queryParams,
    savedParams : savedParams,
    getKey : getKey
};


function getKey (propiedades, valores) {
    var obj = {};

    propiedades.forEach(function (key) {
        var val = valores[key];
        val = nh.isFunction(val) ? val() : null;
        if(val) obj[key] = val;

    });

    return m.route.buildQueryString(obj);
}


function savedParams () {
    var d = localStorage.getItem('OORDEN/filtrosReporte');
    d = d ? JSON.parse(d) : {};
    var savedParams = {};

    Object.keys(d).forEach(function (key) {
        savedParams[key] = m.prop(d[key]);
    });

    return savedParams;
}

function queryParams () {
    var qParams = {};
    var parsed = m.route.parseQueryString(window.location.search);

    Object.keys(parsed).forEach(function (key) {
        if(!key) return;
        qParams[key] = m.prop( parsed[key] );
    });

    console.log('qParams', qParams);
    return qParams;
}


function aplicar (propiedades, aplicarA, datos, datosDefault) {
    propiedades.forEach(function (nombrePropiedad) {
        var prop = aplicarA[nombrePropiedad];
        var origen = datos[nombrePropiedad];
        if(nh.isFunction(origen) == false && datosDefault) {
            origen = datosDefault[nombrePropiedad];
        }

        if(nh.isFunction(origen)) {
            prop( origen() );
        }
    });
}

function FiltrosReporteCtrl (params) {
    var ctx = this;

    ctx.fechasBar = new FechasBar.controller({});

    ctx.fechasBar.fechaDesde( params.value.fechaDesde() )
    ctx.fechasBar.fechaHasta( params.value.fechaHasta() )

    ctx.cuenta = m.prop()

    ctx.aplicar = function () {
        params.aplicarA.fechaDesde(ctx.fechasBar.fechaDesde());
        params.aplicarA.fechaHasta(ctx.fechasBar.fechaHasta());
        params.close()
    }
}

function FiltrosReporteView (ctx,params) {
    return m('.highlight.grey', [

        m('.close', { onclick: params.close }, m.trust('&times;')),

        m('.row', [
            m('.col-sm-12', [
                FechasBar.view(ctx.fechasBar)
            ])
        ]),

        params.seleccionarCuenta ? m('.row', [
            m('.col-sm-6', [
                m('h6', 'Mostrar Cuentas'),

                m.component(CuentasSelector, {
                    seleccionar : '*',
                    model : ctx.cuenta,
                    id : f('cuenta')
                })
            ])
        ]) : null,


        ctx.cuenta(),

        oor.stripedButton('button.btn-success.pull-right' ,'Aplicar Filtros', {
            onclick : ctx.aplicar
        }),

        m('.clear')

    ]);
}
