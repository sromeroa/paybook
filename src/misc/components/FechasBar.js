var FechasBar = module.exports = {};

FechasBar.controller = function (args) {
    var ctx = this;

    function nullify () {
        var prop = m.prop(arguments[0]);
        prop.toJSON = function () {
            if(prop() == null || prop() == '') return undefined;
            return prop();
        }
        return prop;
    }

    args.fechaKey = 'fecha'

    var keysMap = {
        rango : ['$lte', '$gte'],
        fecha : ['$equals']
    };

    ctx.consulta = m.prop('rango');

    ctx.$lte = nullify(null);
    ctx.$gte = nullify(null);
    ctx.$equals = nullify(null);

    ctx.change = function (k, v) {
        ctx[k](v);

        var s = {};

        s[args.fechaKey] = {};

        keysMap[ctx.consulta()].forEach(function (f){
            s[args.fechaKey][f] = ctx[f];
        });

        var data =  JSON.parse(JSON.stringify(s));

        args.onchange(data);
    }
};

FechasBar.view = function (ctx) {
    return m('div', [
        m('.btn-group.dropdown.pull-left', [
            m('button.btn.btn-flat.btn-xs[data-toggle="dropdown"]', {
                style : 'font-size:14px'
            }, [
                'Fecha por: rango ',
                 m('i.ion-chevron-down')
            ]),
            m('ul.dropdown-menu')
        ]),

        m('.mt-inputer.pull-left', [
            m('label', 'Desde'),
            m('input[type=date]', {
                onchange : m.withAttr('value', ctx.change.bind(ctx, '$gte')),
                value : ctx.$gte(),
                id : "fechai"
            })
        ]),

        m('.mt-inputer.pull-left', [
            m('label', 'Hasta'),
            m('input[type=date]', {
                onchange : m.withAttr('value', ctx.change.bind(ctx, '$lte')),
                value : ctx.$lte(),
                id : "fechaf"
            })
        ])
    ]);
};
