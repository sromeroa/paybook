var NotasComponent = module.exports = {};


NotasComponent.controller = function (args) {
    var ctx = this;

    ctx.pertenece_a_id = args.pertenece_a_id;
    ctx.contexto = args.contexto ? args.contexto : m.prop();

    ctx.nuevaNota = m.prop();
    ctx.notas = m.prop([]);

    ctx.crearNota = function () {
        var nota = new Nota({
            pertenece_a_id : ctx.pertenece_a_id(),
            usuario_id : oorden.usuario().usuario_id,
            contexto : ctx.contexto()
        })

        nota.$isNew(true)
        ctx.nuevaNota(nota)
    }

    ctx.guardar = function () {
        return Nota.save(ctx.nuevaNota())
                .then(cargarNotas)
                .then(ctx.nuevaNota.bind(null,null))
    }

    cargarNotas();

    function cargarNotas () {
        obtenerNotas(ctx).then(ctx.notas)
    }
};


NotasComponent.view = function (ctx) {
    return m('.panel', [
        m('.panel-heading', [
            m('.panel-title', [
                !ctx.nuevaNota() ? m('button.btn.btn-sm.pull-right.btn-primary',{
                    onclick:ctx.crearNota
                },'+') : null,
                m('h4', 'Notas')
            ])
        ]),
        m('.panel-body.without-padding', [
            m('ul.list-material', [

                ctx.nuevaNota() ? m('li.has-action-right', [
                    m('.list-content',NotasComponent.nuevaNotaForm(ctx))
                ]) : null,

                ctx.notas().slice(0,3).map(function (nota) {
                    return m('li.has-action-right.nota', {key:nota.nota_id(),config:oor.time.update}, [
                        m('a.visible[href="javascript:;"]',[
                            m('.list-content', [
                                m('.title', [
                                    nota.usuario().nombre(),
                                    nota.accion() ? [
                                        m.trust(' &mdash; '),
                                        m('span.text-indigo', nota.accion(), ' (' , nota.fecha_accion() + ')')
                                    ] : null
                                ]),
                                m('.caption', nota.nota()),
                            ]),
                            m('.list-action-right', [
                                m('span.top', {'x-time-till-now' : nota.created_at()})
                            ])
                        ])
                    ]);
                })

            ])
        ])
    ]);
};


NotasComponent.nuevaNotaForm = function (ctx) {
    var nuevaNota = ctx.nuevaNota();

    return m('form', {
        onsubmit : function () { ctx.guardar(); return false;}   
    }, [

        m('input', {
            config : function(el, isInited) {isInited || el.focus()},
            placeholder:'Nota...',
            oninput : m.withAttr('value', nuevaNota.nota)
        }),

        m('br'),

        m('select', {value:nuevaNota.accion(),onchange:m.withAttr('value',setAccion)}, [
            m('option', {value : ''}, '(Sin Acción)'),
            m('option', {value : 'llamada'}, 'Llamada'),
            m('option', {value : 'cita'}, 'Cita')
        ]),

        nuevaNota.accion() ? m('input[type=date]', {
            value : nuevaNota.fecha_accion(),
            onchange : m.withAttr('value', nuevaNota.fecha_accion)
        }) : null,

        m('button[type="submit"].btn.btn-sm.btn-success.btn-sm.pull-right', 'Guardar')
    ]);


    function setAccion (value) {
        nuevaNota.accion(value)
        nuevaNota.fecha_accion(value ? oor.fecha.toSQL(new Date) : null)
    }
}


Nota.save = function (nota) {
    var method, url;

    if(nota.$isNew && nota.$isNew()) {
        method = 'POST';
        url = '/apiv2/agregar?modelo=notas';
    } else {
        url = '/apiv2/editar/' + nota.nota_id() + '?modelo=notas';
        method = 'PUT';
    }

    return m.request({
        method : method,
        url : url,
        data : nota
    });
}


function obtenerNotas(busqueda) {
    var contexto = f('contexto')(busqueda);

    var params = _.extend({}, {
        pertenece_a_id : f('pertenece_a_id')(busqueda),
        contexto : contexto ? contexto : undefined,  
        modelo : 'notas',
        include : 'notas.usuario'
    });

    return m.request({
        url : '/apiv2',
        data : params,
        unwrapSuccess : function (r) {
            return r.data.map(function(nota) {
                return new Nota(nota);
            });
        }
    });
} 


/**
 * Modelo de Notas
 */

function Nota (d) {
    d || (d = {});

    this.nota_id = m.prop(d.nota_id);
    this.nota = m.prop(d.nota);
    this.pertenece_a_id = m.prop(d.pertenece_a_id);
    this.contexto = m.prop(d.contexto);
    this.accion = m.prop(d.accion);
    this.fecha_accion = m.prop(d.fecha_accion);
    this.created_at = m.prop(d.created_at);
    this.usuario_id = m.prop(d.usuario_id);

    var usuario = d.usuario || {};

    this.usuario = m.prop({ nombre : m.prop(usuario.nombre) });


    this.$isNew = m.prop(false);
}
