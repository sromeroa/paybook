/**
 *
 */

module.exports = ASSelector;

function ASSelector (inputNode, properties) {
    if((this instanceof ASSelector) === false) return new ASSelector(inputNode, properties);
    var $this   = this;
    var value   = m.prop(null);
    var textVal = m.prop('');
    var text    = f('text');
    var popUp   = null;
    var data    = m.prop()
    var html    = defaultHTML;
    var update  = defaultUpdate;
    var searchString = m.prop('');
    var computer = computerDefault;
    var onchange;

    $this.computedData = m.prop();
    $this.searchString = searchString;
    $this.highlightID  = m.prop(null);
    $this.selected     = m.prop(null);
    $this.searching    = m.prop(false)


    $this.select = function (item) {
        var oldVal = $this.value();
        $this.selected(item);

        if(item != null && (typeof item != 'undefined')) {
            $this.value(item.id, text(item));
        } else {
            $this.value(null,'');
        }

        if(oldVal != $this.value()) {
            nh.isFunction(onchange) && onchange.call($this);
        }

        $this.updateInput();
        $this.close();
    }


    $this.selectId = function (id) {
        if(id != null && (typeof id != 'undefined')) {
            var item = $this.findById(id);
            $this.select(item);
        }
    }


    $this.findById = function (id) {
        if(id != null && (typeof id != 'undefined')) {
            return $this.getData().filter( f('id').is(id) )[0];
        }
    }

    $this.message = function () {
        return popUp.select('.message');
    }


    $this.compute = function (callback) {
        $this.computedData([]);

        $this.computer().call($this,$this, function (computedData) {
            $this.computedData(computedData);
            $this.redraw();
            nh.isFunction(callback) && callback();
        });
    }


    $this.computer = function () {
        if(arguments.length) {
            computer = arguments[0]
        }

        return computer;
    }


    $this.html =  function () {
        if(arguments.length) {
            html = arguments[0]
        }
        return html;
    }


    $this.update =  function () {
        if(arguments.length) {
            update = arguments[0]
        }
        return update;
    }


    $this.onchange = function ()  {
        if(arguments.length) {
            onchange = arguments[0];
        }
        return onchange;
    }

    $this.popUp = function () {
        return popUp;
    }


    $this.setData = function (aData) {
        if(arguments.length) {
            if(nh.isFunction(aData)) {
                data = aData;
            } else {
                data(aData);
            }
        }
    }


    $this.getData = function () {
        return data();
    };

    $this.isOpen = function () {
        return Boolean(popUp);
    };

    $this.updatePopUp = function (callback) {
        if(this.isOpen()) {
            $this.compute(callback);
        } else {
            $this.open();
        }
    };

    $this.redraw = function () {
        popUp.call(asSelector, $this, {});
    }

    $this.value = function () {
        if(arguments.length) {
            value(arguments[0]);
            textVal(arguments.length > 1 ? arguments[1] : "" );
        }

        return value();
    };


    $this.text = function () {
        if(arguments.length) {
            text = arguments[0];
        }
        return text;
    };

    $this.updateInput = function () {
        inputNode.value = textVal();
    };

    $this.highlight = function (attrs) {
        if(!attrs)attrs = {};
        var highlighted = $this.highlightID();

        var available = $this.computedData().filter(function (d) { return d.$show && d.$selectable;});
        var current = available.indexOf(available.filter( f('id').is(highlighted) )[0]);
        var nIdex;

        if(typeof attrs.byPosition === 'number') {
            nIndex = attrs.byPosition;
        } else if(typeof attrs.move === 'number') {
            nIndex = attrs.move + (current || 0);
        }

        if(typeof nIndex != 'undefined') {
            nIndex = Math.min(available.length-1, nIndex);
            nIndex = Math.max(0, nIndex);
            $this.highlightID(available[nIndex] ? available[nIndex].id : null);
        } else if (typeof attrs.id !== 'undefined') {
            $this.highlightID(attrs.id)
        }

        if(popUp) popUp.call(updateHighlight, $this.highlightID())
    };

    $this.close = function () {
        if($this.isOpen()) {
            popUp.remove();
            popUp = null;

            d3.select(inputNode)
                .style('position', undefined)
                .style('z-index', undefined)
        }

        $this.updateInput();
    }

    $this.selectHighlighted = function () {
        var d = popUp.selectAll('.ctitem').filter(function (d) {
            return $this.highlightID() === d.id;
        });
        $this.select( d3.select(d.node()).datum() );
    }

    $this.open = function () {
        if(popUp) return;
        if(value()) {
            $this.highlightID( value() );
        }

        var el = inputNode;
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();
        var top = pos.top - 10 //+ height + 5;
        captureFocus = false;

        d3.select(el)
            .style('position', 'relative')
            .style('z-index', 5001)

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        opacity : 0,
                        position:'absolute',
                        padding : '60px 10px 10px 10px',
                        height:'450px',
                        width: String(width + 128).concat('px'),
                        background:'white',
                        top  : String(top).concat('px'),
                        left : String(pos.left - 10).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0',
                        'box-shadow' : '3px 3px 6px rgba(0,0,0,0.3)',
                        'border-radius' : '8px'
                    });

        popUp.append('div').attr('class', 'message');
        popUp.append('div').attr('class','ct');
        popUp.transition().style({ opacity : 1 });

        $this.updatePopUp(function () {
            if($this.value()) {
                $this.highlight({ id:$this.value() })
            }
        });



        return popUp;
    }

    $this.performSearch = function (search) {
        $this.searching(true);
        $this.searchString(search);

        $this.updatePopUp(function () {
            $this.highlight({ byPosition:0 });
        });

        $this.searching(false);
    }

    if(properties.data) {
        $this.setData(properties.data);
    }

    if(properties.onchange) {
        $this.onchange(properties.onchange)
    }

    mountSelector(inputNode, $this);
    return $this;
}

function asSelector (selection, selector, args) {
    var allData = selector.getData();
    var data, filter, ct, items;

    ct     = selection.select('.ct');
    data   = selector.computedData().filter( f('$show').is(true) )
    items  = ct.selectAll('.ctitem').data(data);

    items.exit().remove();
    //items.style('display', function (d) { return d.$show ? undefined : 'none' });
    items.style('cursor', function(d) {  return d.$selectable ? 'pointer' : undefined });
    items.enter().append('div').attr('class', 'ctitem');
    items.exit().remove();
    items.call(selector.update(), selector);
    items.on('click', selector.select);


    if(selector.afterUpdate) {
        selection.call(selector.afterUpdate, items, selector);
    }

    //items.classed('Highlight', f('id').is(selector.highlightID()) )
}

function updateHighlight(selection, highlightID) {
    var items  = selection.selectAll('.ctitem')
    var hItems = items.filter(function (d) { return d.id == highlightID; });
    items.classed('Highlight', false);
    hItems.classed('Highlight', true);
    var node = hItems.node();
    if(node) {
        node.parentNode.scrollTop = -100 + node.offsetTop - node.parentNode.offsetTop;
    }
}

/**
 * Default Functioning config
 */

function fnFrom(value) {
    return function () { return value };
}

function computerDefault (selector) {
    var data = selector.getData();
    var searchString = selector.searchString().toLowerCase().latinize();
    var searching = selector.searching();
    var value = selector.value();


    data.forEach(function (d) {
        if(searching) {
            d.$show = searchString ? Boolean(d.search.indexOf(searchString) +1) : true;
        } else {
            d.$show = value ? d.id === value : true;
        }
        d.$selectable = 1;
    });
    return data;
}

function defaultUpdate (items, selector, args) {
    items.html( selector.html() );
}

function defaultHTML (d) {
    return d.text;
}


/**
 * Mount the selector on the input element
 */
function mountSelector (el, selector) {
    el.addEventListener('focus', function () {
        if(el.value) {
            el.setSelectionRange(0,el.value.length);
        }
        selector.searchString('');
        selector.open();
    });

    el.addEventListener('input', function () {
        selector.performSearch(el.value)
    });

    el.addEventListener('keydown', function (ev) {
        if( ev.keyCode === 40 || ev.keyCode === 38 ) {
            ev.stopPropagation();
            ev.preventDefault();
            selector.highlight({ move : ev.keyCode == 40 ? 1 : -1 });

        } else if( ev.keyCode === 13 || ev.keyCode === 9) {
            if(ev.keyCode === 13) {
                ev.stopPropagation();
                ev.preventDefault();
            }
            if( selector.highlightID() ) {
                //selector.select( TerceroComponent.byId[ selector.highlightID() ] );
                selector.selectHighlighted();
            } else {
                selector.select( selector.selected() );
            }
        } else if(ev.keyCode === 27) {
            //selector.select( selector.isOpen() ? selector.selected() : null);

            if(selector.isOpen()) {
                selector.close()
            }
        }
    });




    var captureFocus = false;

    document.body.addEventListener('click', function (ev) {
        if(ev.target!= el && selector.isOpen() && captureFocus) {
            var p = nodeIsParentOf(selector.popUp().node(), ev.target);
            if(!p) selector.close()
        }
    },false);

    document.body.addEventListener('focus', function (ev) {
        if(ev.target!= el && selector.isOpen() && captureFocus) {
            var p = nodeIsParentOf(selector.popUp().node(), ev.target);
            if(!p) selector.close()
        }
    },true);

    el.addEventListener('blur', function (ev) {
        captureFocus = true;
    });

    function nodeIsParentOf(parent, child){
        var children = [];
        var curr = child;

        while(curr){
            children.push(curr);
            if(curr == parent) {
                return true;
            }
            curr = curr.offsetParent;
        }

        return false;
    }


}
