
module.exports = AdvSelector;

window.xtSelector = AdvSelector;


AdvSelector.current = m.prop(null);

function AdvSelector (settings) {
    var triggerTime = settings.triggerTime || 200;
    var limitResults = settings.limitResults || 5;
    var search = settings.search || searchFn;
    var text = settings.text || f('text');
    var value = settings.value || f('id');
    var enterFunction = settings.enter || enter;
    var data = nh.isFunction(settings.data) ?  settings.data : m.prop(settings.data || []);


    advSelector.model = settings.model;
    advSelector.drawedValue = m.prop();
    advSelector.highlighted = m.prop(0);
    advSelector.results = m.prop([]);
    advSelector.selection = m.prop();
    advSelector.selected = m.prop();

    advSelector.select = function (d) {
        advSelector.selected(d);
        advSelector.model(value(d));
        advSelector.update();
        nh.isFunction(settings.onchange) && settings.onchange();
        advSelector.hide();

        console.log('Select');
        m.redraw();
    }

    advSelector.show = function () {
        advSelector.selection().classed({ opened : true })

        if(advSelector.selected()) {
            advSelector.selection().select('input').property('value', text( advSelector.selected() ));
            advSelector.update([advSelector.selected()])
        }
    }

    advSelector.hide = function () {
        advSelector.selection().classed({ opened : false })
    }

    advSelector.byID = function (id) {
        return data().filter(function (d) { return value(d) == id; })[0]
    }



    advSelector.update = function (results) {
        var resultNodes = advSelector.selection().select('.results-container').selectAll('.result');

        advSelector.drawedValue( advSelector.model() );

        advSelector.selection()
            .select('.xt-view-template')
            .datum(advSelector.selected() || {})
            .call(enterFunction)

        if(results) {
            advSelector.results(results);
            advSelector.highlighted(0);

            resultNodes = resultNodes.data(results, value);

            resultNodes.enter()
                .append('div')
                .attr('class','result')
                .attr('x-value', value)
                .call(enterFunction)
                .on('keyup', function (d) {  d3.event.keyCode == 13 && select(d) } )

            resultNodes.exit().remove();
        }

        resultNodes.style('background-color', function (d,i) {
            return advSelector.highlighted() == i ? 'lightblue' : 'white'
        });
    }

    return advSelector;


    function advSelector (node, isInitialized) {
        var selection, list, container;


        if(advSelector.drawedValue() != advSelector.model()) {
            advSelector.selected(advSelector.byID(advSelector.model()));
            if(isInitialized === true) {
                 advSelector.update()
            }
        }



        if(arguments.length > 1 && isInitialized === true) {
            return;
        }



        node.innerHTML = '';

        selection = d3.select(node);

        selection.classed({'xt-selector':true}).attr('tabindex',0)

        selection.append('div').attr('class', 'xt-view-template');

        selection.append('div').attr('class', 'xt-list');

        list = selection.select('.xt-list');

        list.append('input').attr('type', 'text').attr('placeholder', 'Buscar Cuenta...')

        container = list.append('div').attr('class', 'results-container');

        advSelector.selection(selection)


        var notShow = m.prop(false);

        $(node).on('focus', function () {
            if(notShow()) {
                notShow(false)
                return;
            }

            advSelector.show();
            $('input', node).focus();
        });



        $('input', node).on('blur', function (ev) {
            setTimeout(function () {
                advSelector.hide();
                notShow(true)
            },1000);
        })



        $(node).on('click', '[x-value]', function (ev) {
            var d = advSelector.byID( $(this).attr('x-value') );
            advSelector.select(d);
            ev.stopPropagation();
            ev.preventDefault()
        });

        $(node).on('click', function () {
            advSelector.show();
            $('input', node).focus();
        });



        $(node).on('keypress', function (ev) {
            if(ev.target == this && ev.keyCode == 13) {
                advSelector.show();
                $('input', node).focus();
            }
        });


        $('input', node).on('keydown', function (ev) {
            var h = advSelector.highlighted();

            if(ev.keyCode == 38 || ev.keyCode == 40) {
                ev.stopPropagation()
                ev.preventDefault()

                if(ev.keyCode == 38) {
                    advSelector.highlighted(Math.max(0,h-1));
                    advSelector.update()
                }

                if(ev.keyCode == 40 ){
                    advSelector.highlighted( Math.min(advSelector.results().length-1, h+1));
                    advSelector.update();
                }
            }

            //ENTER
            if(ev.keyCode == 13) {
                advSelector.select(advSelector.results()[advSelector.highlighted()]);
            }

            if(ev.keyCode == 9) {
                advSelector.select(advSelector.results()[advSelector.highlighted()])
            }
        })

        $('input',node).on('input', _.debounce(function () {
            var val = $(this).val();
            var results;

            if(!val) {
                results = [];
                advSelector.highlighted(0);
            } else {
                results = data().filter(search(val, text));
                results = results.slice(0,limitResults);
                advSelector.highlighted(0);
            }
            
            advSelector.update(results);
        },triggerTime));

        advSelector.update();
    }

    function enter (selection) {
        selection.text(text)
    }
}

function searchFn (str, textFn) {
    str = format(str);
    return function (d) {
        return format(textFn(d)).indexOf(str) > -1;
    }
}

function format (d) {
    return String(d).latinise().toLowerCase();
}
