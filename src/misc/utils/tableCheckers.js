
module.exports = tableCheckers;


function tableCheckers (params) {

    var tableHelper;


    console.log(params.id)

    return {
        name : params.name,
        value : params.value,
        filter : function () {},
        update : update,
        enter : enter,
        updateHeading : updateHeading,
        enterHeading : enterHeading,
        tableHelper : function (iTHelper) {
            tableHelper = iTHelper;
        }
    };

    function enter (selection) {
        selection.append('input')
            .attr('type', 'checkbox')
            .attr('value', function (d) { return idFn(d.row) })
            .on('change', function (i) {

                params.change(i.row, this.checked)
                if(tableHelper) tableHelper.draw()
            })
    }

    function update (selection) {
        selection.select('input')
            .property('checked', function (d) {
                return d.value ? true : false;
            })
    }

    function updateHeading (selection) {
        
    }

    function enterHeading (selection) {

    }

    function idFn (item) {
        if(nh.isFunction(params.id)) {
            return params.id(item);
        }
        return f('id')(item);
    }
}