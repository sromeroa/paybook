/** 
 * Modelo de Poliza
 */
function Partida (data) {
    var Partida = oor.mm('Partida');

    data || (data = {});
    Partida.initializeInstance.call(this,data);

    this.$errors = m.prop([]);

    this.$cuenta = m.prop( oorden.cuenta(data.cuenta_id) );
    this.$ccto1  = m.prop( oorden.ccto(data.ccto_1_id) );
    this.$ccto2  = m.prop( oorden.ccto(data.ccto_2_id) );

    this.debe_o_haber( Number(this.debe_o_haber()) );
    this.debe     = Modelo.numberProp(m.prop(), 2);
    this.haber    = Modelo.numberProp(m.prop(), 2);
    this.importe  = Modelo.numberProp(this.importe,2);

    if(Number(this.debe_o_haber()) == 1) {
        this.debe.$int(this.importe.$int());
    } else {
        this.haber.$int(this.importe.$int());
    }

    this.debe.fix();
    this.haber.fix();

}

/*
Operacion.estatus = function (op, estatus) {
    return m.request({
        method : 'POST',
        url : '/api/operacion/estado_operacion/' + op.operacion_id() + '/' + estatus
    });
}
*/

oor.mm('Partida', Partida)
    .idKey('poliza_partida_id')

    .prop('poliza_partida_id',{})
    .prop('poliza_id', {})
    .prop('organizacion_id', {})
    .prop('cuenta_id', {})
    .prop('concepto', {})
    .prop('debe_o_haber', {})
    .prop('importe', {})

    .prop('tipo_de_cambio', {})
    .prop('tasa_de_cambio', {})
    .prop('moneda', {})

    .prop('ccto_1_id', {})
    .prop('ccto_2_id', {})

    .prop('fecha', {})
    .prop('ano_contable',{})
    .prop('mes_contable',{})
    
    .prop('referencia', {})
    .prop('total', {})
    
    .prop('operacion_id', {})
    .prop('estatus', {})  
    .prop('notas', {}) 
    .method('addError', function (err) {
        if(this.$errors().indexOf(err) == -1) {
            this.$errors().push(err);
        }
    })
    .method('remError', function (err) {
        var idx = this.$errors().indexOf(err);
        if(idx > -1) {
            this.$errors().splice(idx,1);
        }
    })
    .method('hasError', function (err) {
        return this.$errors().indexOf(err) > -1;
    })

