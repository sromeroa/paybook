/**
 * Modelo de OperacionItem
 */
function OperacionItem (data) {
    OperacionItem.initializeInstance.call(this,data);

    data || (data = {});

    var self = this;

    this.importe = Modelo.numberProp(this.importe, 2);
    this.descuento = Modelo.numberProp(this.descuento, 2);
    this.precio_unitario = Modelo.numberProp(this.precio_unitario, 2);
    this.cantidad = Modelo.numberProp(this.cantidad, 4, true);
    this.cantidad_devuelta = Modelo.numberProp(this.cantidad_devuelta, 4, true);

    this.importe.fix();
    this.precio_unitario.fix();
    this.cantidad.fix();
    this.cantidad_devuelta.fix();
    this.descuento.fix();

    this.$integrable = m.prop(false);
    this.$impuestosIncluidos = m.prop(false);

    this.$impuesto = m.prop(oorden.impuesto(this.impuesto_conjunto_id()) || null);
    this.$retencion = m.prop(oorden.retencion(this.retencion_conjunto_id()));

    this.$tasaRetencion = m.prop(null);
    this.$tasaImpuesto = m.prop(0);

    this.$descuento = Modelo.numberProp(m.prop(0),2);
    this.monto_retencion = Modelo.numberProp(this.monto_retencion,2);

    this.$sinImpuestos = Modelo.numberProp(m.prop(0), 2);
    this.$montoImpuestos = Modelo.numberProp(m.prop(0), 2);

    this.$cuenta = m.prop(data.cuenta|| null);

    this.$operacionItemAnterior = m.prop();
    this.$operacionItemAnterior(data.operacionItemAnterior);

    this.asignarImpuesto(this.$impuesto());
    this.asignarRetencion(this.$retencion());

    this.$errors= m.prop ([]);
}

oor.mm('OperacionItem', OperacionItem)
    .idKey('operacion_item_id')

    .prop('operacion_item_id',{})
    .prop('operacion_id',{})
    .prop('operacion_anterior_item_id', {})
    .prop('posicion_item', {})
    .prop('producto_id',{})
    .prop('producto_variante_id',{})
    .prop('producto_nombre', {})
    .prop('cantidad', {})
    .prop('cantidad_devuelta', {})
    .prop('unidad', {})
    .prop('precio_unitario',{})
    .prop('descuento',{})
    .prop('importe',{})
    .prop('cuenta_id',{})

    .prop('retencion_conjunto_id', {})
    .prop('impuesto_conjunto_id', {})
    .prop('ccto_1_id', {})
    .prop('ccto_2_id', {})
    .prop('monto_impuestos', {})
    .prop('monto_retencion', {})

    .prop('$producto', {})
    .prop('$cuenta', {})
    .prop('$impuestosIncluidos', {})
    .prop('$sinImpuestos', {})
    .prop('$impuestos',{})
    .prop('$retenciones',{})

    .prop('$tasaImpuesto',{})
    .prop('$tasaRetencion',{})
    .prop('$sinImpuestos', {})

    .method('asignarCuenta', function (cta) {
        this.cuenta_id(cta ? cta.cuenta_contable_id : null);
        this.$cuenta(cta);
    })
    .method('asignarImpuesto', function (impuesto) {
        var tasa = 0;
        this.$impuesto(impuesto);
        this.impuesto_conjunto_id(impuesto ? impuesto.impuesto_conjunto_id : null);

        if(this.$impuesto() && this.$impuesto().componentes) {

            this.$impuesto().componentes
                .filter(function (componente) {
                    return !Number( f('compuesto')(componente) )
                })
                .forEach(function (c) {
                    var t = Number(c.tasa);
                    tasa += t;
                    c.porcentajeTasa = t/100;
                });

            this.$impuesto().componentes
                .filter(function (componente) {
                    return Number( f('compuesto')(componente) )
                })
                .forEach(function (c) {
                    var t = Number(c.tasa);
                    c.porcentajeTasa = t/100;

                    tasa = (1+c.porcentajeTasa) * (1+tasa/100) - 1;
                    tasa = tasa * 100;
                });

            tasa = Math.round(tasa * 100000) / 100000;
        }

        this.$tasaImpuesto(tasa / 100);
    })


    .method('asignarRetencion',  function (retencion) {
        var tasa = 0;
        this.$retencion(retencion);

        if( this.$retencion() && this.$retencion().componentes) {
            this.$retencion().componentes.forEach(function (c) {
                var t = Number(c.tasa);
                tasa += t;
                c.porcentajeTasa = t / 100;
            });
        }

        this.$tasaRetencion(tasa / 100);
    })


    .method('actualizar',  function () {
        var precioUnit   = Number(this.precio_unitario.getString());
        var cantidad     = Number(this.cantidad.getString());
        var descuento    = Number(this.descuento.getString()) / 100;
        var factorDscto  = 1 - descuento;
        var importe      = precioUnit * cantidad * factorDscto;

        this.importe(importe);
        this.importe.fix();

        if(this.$integrable() == false) {
            this.$integrable(  (this.producto_nombre() && this.producto_nombre() != '') || (this.importe.$int() != 0) );
        }

        if( this.$impuestosIncluidos()  ) {
            this.$sinImpuestos.$int( this.importe.$int() / (1 + (this.$tasaImpuesto()))  );
        } else {
            this.$sinImpuestos(importe);
        }

        this.$descuento((precioUnit*cantidad) - importe);

        this.$impuestos   = this.calcularImpuestos();
        this.$retenciones = this.calcularRetenciones();

        var monto = 0;

        Object.keys(this.$retenciones).forEach(function (ret) {
            monto += this.$retenciones[ret];
        }.bind(this));

        this.monto_retencion.$int(monto);

    })

    /**
     * Calcular Impuestos
     */
    .method('calcularImpuestos', function () {
        var impuestos = {};
        var sinImpuestos = this.$sinImpuestos.$int();
        var valor = sinImpuestos;


         if(this.$impuesto() && this.$impuesto().componentes) {

            this.$impuesto().componentes
                .filter(function (componente) {
                    return ! Number(f('compuesto')(componente))
                })
                .forEach(function (c) {
                    var valorImpuesto = Math.round(sinImpuestos * c.porcentajeTasa);
                    valor += valorImpuesto;
                    impuestos[nombreComponente(c)]  = valorImpuesto;
                });


            this.$impuesto().componentes
                .filter(function (componente) {
                    return Number(f('compuesto')(componente))
                })
                .forEach(function (c) {
                    var valorImpuesto = Math.round(valor * c.porcentajeTasa);
                    impuestos[nombreComponente(c)]  = valorImpuesto;
                });
        }

        return impuestos;
    })

    /**
     * Calcular Retenciones
     */
    .method('calcularRetenciones', function () {
        var retenciones  = {};
        var sinImpuestos = this.$sinImpuestos.$int();

        if(this.$retencion() && this.$retencion().componentes) {
            this.$retencion().componentes.forEach(function (c) {
                var monto = Math.round( sinImpuestos * c.porcentajeTasa);
                retenciones[ c.componente + ' ' + (c.porcentajeTasa*100).toFixed(2) ] = monto
            });
        }

        return retenciones;
    })
    /*
     * Calcula la cantidad restante por devolver
     */
    .method('cantidadRestante', function (){
        return Number(this.cantidad()) - Number(this.cantidad_devuelta());
    })

    .method('addError', function (err) {
        if(this.$errors().indexOf(err) == -1) {
            this.$errors().push(err);
        }
    })
    .method('remError', function (err) {
        var idx = this.$errors().indexOf(err);
        if(idx > -1) {
            this.$errors().splice(idx,1);
        }
    })
    .method('hasError', function (err) {
        return this.$errors().indexOf(err) > -1;
    })
    ;

function nombreComponente (c) {
    return (c.componente || '').toUpperCase() + ' ' + (c.porcentajeTasa*100).toFixed(2) + ' %';
}
