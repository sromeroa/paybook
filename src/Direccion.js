
/**
 * Modelo de Operacion
 */
function Direccion (data) {
  data = data || {};
  Direccion.initializeInstance.call(this,data);
}


oor.mm('Direccion', Direccion)
    .serviceName('direcciones')
    .idKey('direccion_id')

    .prop('direccion_id', {})
    .prop('pertenece_a_id', {})
    .prop('calle_y_numero', {})
    .prop('no_exterior', {})
    .prop('no_interior', {})
    .prop('estado_o_region', {})
    .prop('ciudad_o_municipio', {})
    .prop('codigo_postal', {})
    .prop('organizacion_id', {})
    .prop('tipo_id', {})
    .prop('codigo_pais',{})

    .prop('actual', {})
    .prop('colonia', {})
    .prop('referencia', {})
    .prop('ya_utilizada', {})
