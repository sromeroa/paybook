

module.exports = D3Date;



function D3Date () {
    var value     = m.prop();
    var formatted = m.prop();
    var format    = oor.fecha.prettyFormat.year;
    var element;
    var popUp;

    dateFn.onchange = m.prop();

    dateFn.popUp = function () {
        return popUp;
    }

    dateFn.value = function () {
        if(arguments.length)  {
            var v = value();
            value(arguments[0]);
            formatted( format(value()) );
            element && (element.value = formatted());

            if(v != value() ) {
                dateFn.onchange && dateFn.onchange(value());
            }
        }
        return value();
    }

    dateFn.formatted = function () {
        return formatted();
    }

    dateFn.element = function (el) {
        mount(el, dateFn);
        element = el;
    }

    dateFn.close = function () {
        if(popUp) popUp.transition().style('opacity', 0).remove();
        popUp = null;
    }

    dateFn.update = function () {
        popUp.call(dateFn)
    }

    dateFn.open = function () {
        if(!element) return;

        var el = element;
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();
        var top = pos.top + 10 + height;
        captureFocus = false;
        if(popUp) return;

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        opacity : 0,
                        position:'absolute',
                        'padding' : '20px',
                        width:'250px',
                        height:'280px',
                        background:'white',
                        'text-align' : 'center',
                        top  : String(pos.top).concat('px'),
                        left : String(pos.left).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0',
                        'box-shadow' : '3px 3px 6px rgba(0,0,0,0.3)',
                        'border-radius' : '8px',
                    });


        popUp.transition()
            .style({opacity : 1, top : String(top).concat('px') })

        popUp.call(dateFn);

        return popUp;
    }


    var weekStart = 1;
    var weekLen   = 7;
    var dias = ['L', 'M', 'X', 'J', 'V', 'S', 'D']

    function dateFn (selection, dateCalendar) {
        var valSQL = value();
        var val;
        var calendar = {};

        if(dateCalendar) {
            val = dateCalendar
        } else {
            if(!valSQL) {
                val = oor.fecha.toSQL(new Date);
            } else {
                val = oor.fecha.fromSQL(valSQL)
            }
        }

        if( selection.select('table.calendar').empty() ) {
            selection.append('button')
                .attr('class', 'pull-left btn btn-xs btn-flat btn-default')
                .html('&laquo;');

            selection.append('button')
                .attr('class', 'pull-right btn btn-xs btn-flat btn-default')
                .html('&raquo;');


            selection.append('h6')


            selection.append('table').attr('class', 'calendar')
            selection.select('table.calendar').append('thead').append('tr');
            selection.select('table.calendar').append('tbody');

            for(var h= 0; h<7; h++){
                selection.select('table.calendar thead tr').append('th').text(dias[h])
            }

            selection.append('button')
                .attr('class', 'btn btn-xs btn-flat btn-primary')
                .text('Seleccionar Hoy')
                .on('click', function () {
                    dateFn.value( oor.fecha.toSQL(new Date) );
                    selection.call(dateFn);
                    dateFn.close()
                })
        }


        calendar.startMonth = oor.fecha.inicialMes(val);
        calendar.endMonth   = oor.fecha.finalMes(val);
        calendar.startDay   = calendar.startMonth.getDay();
        calendar.startDiff  = calendar.startMonth.getDay() - weekStart ;

        if(calendar.startDiff < 0) {
            calendar.startDiff = 7  + calendar.startDiff;
        }

        calendar.startDate  = new Date(calendar.startMonth);
        calendar.startDate.setDate( calendar.startDate.getDate() - calendar.startDiff );

        calendar.weeks = [];
        var week;
        var currDate = new Date(calendar.startDate);

        for(var i = 0; i<6; i++)
        {
            calendar.weeks.push([])
            for(var j= 0; j<7; j++)
            {
                calendar.weeks[i].push({
                    value : oor.fecha.toSQL(currDate),
                    date  : currDate.getDate(),
                    month : currDate.getMonth() + 1
                })
                currDate.setDate( currDate.getDate() + 1);
            }
        }


        selection.select('h6').text( oor.fecha.fullLabelMes(calendar.startMonth.getMonth() + 1) + ' ' + calendar.startMonth.getFullYear() )

        var rows = selection.select('table.calendar tbody').selectAll('tr').data(calendar.weeks)

        rows.enter().append('tr')

        var cells = rows.selectAll('td').data(function (d) { return d })

        cells.enter().append('td')

        cells.text( f('date') );

        cells.attr('class','text-grey');

        cells.filter( f('month').is( calendar.startMonth.getMonth() + 1 ) ).attr('class', '')

        cells.filter( f('value').is(valSQL) ).attr('class', 'selected');

        cells.on('click', function (d) {
            dateFn.value(d.value);
            selection.call(dateFn);
            dateFn.close()
        });


        selection.select('button.pull-right')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() + 1);
                selection.call(dateFn, nDate);
            });

        selection.select('button.pull-left')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() - 1);
                selection.call(dateFn, nDate);
            });
    }


    var cadenas = {
        dia: 'date',   d:'date',  dias:'date',
        mes: 'month',  m:'month', meses:'month'
    }

    var metodos =  {
        date  : {set : 'setDate',  get : 'getDate'},
        month : {set : 'setMonth', get : 'getMonth'}
    }

    var factores = {'-' : -1, '+' : 1};

    dateFn.parse = function (val) {
        val = val.split(' ').join('');
        val = val.split('/').join('-')

        if(oor.fecha.validateSQL(val)) {
            return val;
        }

        var hoy = new Date, match;
        if(val == 'h' || val == 'hoy') {
            return oor.fecha.toSQL(hoy)
        }

        if(match = val.match( /([\+|\-])(\s+)?(\d{1,})(\s+)?([A-Za-z]+)/) ) {
            var que    = match[5].latinize().toLowerCase();
            var date   = new Date(hoy);
            var factor = match[1] == '-' ? -1 : 1;
            var monto  = Number(match[3]);

            if(!cadenas[que]) return false;
            que = metodos[ cadenas[que] ];

            if(!que) return false;

            date[que.set].call(date, date[que.get]() + (factor * monto) );
            return oor.fecha.toSQL(date);
        }

        if(match = val.match( /(\d{1,2})([A-Za-z]{3,15})(\d{1,4})?/) ) {
            var date = new Date;
            var nMes = match[2].toLowerCase();
            var nAno = match[3] ? Number(match[3]) : null

            var mes = oor.fecha.meses().filter( function (s) {
                return s.toLowerCase().indexOf(nMes) > -1;
            })[0];

            if(!mes) return false;
            date.setMonth(oor.fecha.meses().indexOf(mes) - 1);
            date.setDate(match[1]);

            if(nAno !== null) {
                if(nAno < 1000) {
                    nAno += 2000;
                }
                date.setFullYear(nAno);
            }

            return oor.fecha.toSQL(date);
        }
    }

    return dateFn;
}



function mount (el, date) {
    $(el).val( date.formatted() );

    el.addEventListener('focus', function () {
        if(el.value) el.setSelectionRange(0,el.value.length)
        date.open();
    });
    el.addEventListener('change', function () {
        var d = date.parse(el.value);
        if(d) {
            date.value(d);
        }
        date.update();
    });


    var captureFocus = false;

    document.body.addEventListener('click', function (ev) {
        if(ev.target!= el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },false);

    document.body.addEventListener('focus', function (ev) {
        if(ev.target != el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },true);

    el.addEventListener('blur', function (ev) { captureFocus = true; });

    function nodeIsParentOf(parent, child){
        var children = [];
        var curr = child;

        while(curr){
            children.push(curr);
            if(curr == parent) {
                return true;
            }
            curr = curr.offsetParent;
        }

        return false;
    }
}




D3Date.mounter = function (dateFn, attrs) {
    if(attrs.model()) { dateFn.value( attrs.model() ); }

    return function (element, isInit) {
        if(isInit == false) {
            if(attrs.model()) { dateFn.value( attrs.model() ); }


            dateFn.onchange = function (v) {
                attrs.model(v);
                attrs.onchange && attrs.onchange(v);
                m.redraw();
            };
        }

        if(attrs.model() != dateFn.value()) {
            dateFn.value(attrs.model());
        }
    }
}



D3Date.component = {
    oncreate : function (vnode) {
        vnode.state.dateFn = new D3Date();

        if(vnode.attrs.model()) {
            vnode.state.dateFn.value( vnode.attrs.model() );
        }
        vnode.state.dateFn.element(vnode.dom);

        vnode.state.dateFn.onchange = function (v) {
            vnode.attrs.model(v);
            nh.isFunction(vnode.attrs.onchange) && attrs.onchange(v);
            m.redraw();
        };
    },
    onupdate : function (vnode) {
        if(vnode.attrs.model() != vnode.state.dateFn.value()) {
            vnode.state.dateFn.value( vnode.attrs.model() );
        }
    },
    view : function () {
        return m('input[type=text]');
    }
}
