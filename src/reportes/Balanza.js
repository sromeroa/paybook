var BalanzaContable = module.exports = {};
var FechasMultiSelector = require('../misc/components/FechasMultiSelector');

BalanzaContable.controller = function()  {
    var cuentasSelector;
    var ctrl = this;
    var search  = m.route.parseQueryString(location.search);;
    var now = new Date;

    this.tipo = m.prop();
    this.mes_contable = m.prop();
    this.ano_contable = m.prop();
    this.fecha_inicial = m.prop()
    this.fecha_final = m.prop();
    this.cuentas = m.prop();
    this.mostrarCuentas = m.prop();
    this.busqueda = m.prop();
    this.nivelMaximo = m.prop(0);

    if (search) {
        if (search.mes_contable) {
            this.tipo('fiscal')
            this.mes_contable(search.mes_contable);
            this.ano_contable(search.ano_contable);
            this.fecha_inicial(oor.fecha.toSQL(oor.fecha.inicialMes(now)))
            this.fecha_final(oor.fecha.toSQL(oor.fecha.finalMes(now)))

        } else if(search.fecha_inicial) {
            this.tipo('fechas');
            this.fecha_inicial(search.fecha_inicial);
            this.fecha_final(search.fecha_final);
            this.ano_contable(now.getFullYear());
            this.mes_contable(now.getMonth() + 1)
        } else {
            this.tipo('fiscal')
            this.ano_contable(now.getFullYear());
            this.mes_contable(now.getMonth() + 1);
            this.fecha_inicial(oor.fecha.toSQL(oor.fecha.inicialMes(now)))
            this.fecha_final(oor.fecha.toSQL(oor.fecha.finalMes(now)))
        }
    }

    this.fecha = {
        tipo: this.tipo,
        mes_contable: this.mes_contable,
        ano_contable: this.ano_contable,
        hasta: this.fecha_final,
        desde: this.fecha_inicial,
        onchange: update
    }


    ctrl.actualizarBusqueda = function () {
        if(arguments.length) ctrl.busqueda(arguments[0]);

        ctrl.mostrarCuentas( 
            ctrl.cuentas().filter(function (cta) {
                if(!ctrl.busqueda()) return true;
                var str = ctrl.busqueda().latinize().toLowerCase();
                var search = (cta.cuenta + ' ' + cta.nombre).latinize().toLowerCase();
                return search.indexOf(str) > -1 ;
            })
        );
    }

    var colors = ['#222', '#444', '#666', '#888', '#AAA', '#CCC'];

    ctrl.cuentaNombre = function (cuenta) {
        var tag= "span", params = {}, options;
        params.style = 'color:'.concat(colors[ Number(cuenta.nivel) - 1],';');

        if(cuenta.acumulativa == '0') {
            params.style += 'text-decoration:underline';
            options = getOptions();
            tag = 'a';
            params.href = '/reportes/auxiliar/'.concat(cuenta.cuenta_contable_id, '?',m.route.buildQueryString(options));
        }

        return m(tag, params, cuenta.nombre);
    }

    oorden().then(function() { update(); });

    var saldosKeys = ['saldo_inicial','saldo_final','multiplicador','debe','haber'];

    function getOptions () {
        var options = {};
        if (ctrl.tipo() == 'fiscal') {
            options.mes_contable = ctrl.mes_contable();
            options.ano_contable = ctrl.ano_contable();
        } else {
            options.fecha_inicial = ctrl.fecha_inicial();
            options.fecha_final = ctrl.fecha_final();
        }
        return options;
    }

    function update() {
        var options = getOptions();
        m.request({
            url: '/reportes/balanzadatos',
            method: 'GET',
            data: options
        }).then(function(r) {
        
            ctrl.cuentas(
                r.cuentas.map(function (cta) {
                    var raw_saldos = cta.saldos || {};
                    var new_saldos = { $calculated : Boolean(cta.saldos) };

                    saldosKeys.forEach(function (k) {
                        new_saldos[k] = Modelo.numberProp(m.prop(raw_saldos[k]||0), 2);
                    });

                    cta.saldos = new_saldos;
                    return cta;
                })
            );

            calcularSaldos(ctrl.cuentas());
            ctrl.actualizarBusqueda();
        });
    }

    function calcularSaldos(cuentas) {
        var cuentasByParent = {};

        cuentas.forEach(function (cta) {
            var parentArr = cuentasByParent[cta.subcuenta_de || '0'];
            if(! parentArr) {
                parentArr = cuentasByParent[cta.subcuenta_de || '0'] = [];
            }
            parentArr.push(cta);

            if(Number(cta.nivel) > ctrl.nivelMaximo()) {
                ctrl.nivelMaximo(Number(cta.nivel));
            }
        });

        cuentas.forEach(function (cta) {
            saldoCuenta(cta);
        });

        function saldoCuenta (cta) {
            if (cta.saldos.$calculated) return;
            var subcuentas = cuentasByParent[cta.cuenta_contable_id];
            var saldos     = cta.saldos;
            if(Object.prototype.toString.call( saldosKeys ) === '[object Array]'){
                saldosKeys.forEach(function (key) {
                    var suma = 0;
                    if(Object.prototype.toString.call( subcuentas ) === '[object Array]'){
                        subcuentas.forEach(function(subcuenta) {
                            saldoCuenta(subcuenta);
                            suma += subcuenta.saldos[key].$int();
                        });
                    }

                    saldos[key].$int(suma);
                });
            }
        }
    }
};

BalanzaContable.view = function(ctrl) {
    var Poliza = oor.mm('Poliza');
    
    return m('div', [
        m('h3.column-title', 'Balanza'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', m.component(FechasMultiSelector, ctrl.fecha))
            ])
        ]),
        m('table.table.bill', [
            m('thead', [
                m('tr', [
                    m('th[colspan=2]', 'Cuenta'),
                    m('th', 'Saldo Inicial'),
                    m('th', 'Debe'),
                    m('th', 'Haber'),
                    m('th', 'Saldo Final')
                ]),
                m('tr', [
                    m('th[colspan=2]',
                        m('div.input-group', {style:'width:100%'}, [
                            m('span.input-group-addon', [
                                m('i.ion-ios-search')
                            ]),
                            m('div.input-wrappper', [
                                m('input.form-control[type="text"]', {
                                    placeholder:'Buscar Cuenta',
                                    oninput : m.withAttr('value',ctrl.actualizarBusqueda)
                                })
                            ])
                        ]) 
                    ),
                    m('th', ''),
                    m('th', ''),
                    m('th', ''),
                    m('th', '')
                ])
            ]),
            m('tbody', [
                ctrl.mostrarCuentas().map(function (cuenta) {
                    return m('tr', [
                        m('td', cuenta.cuenta),
                        m('td',[ 
                            m('div', {
                                style:'padding-left:'.concat(cuenta.nivel * 20, 'px') 
                            }, ctrl.cuentaNombre(cuenta))
                        ]),
                        m('td.text-right', cuenta.saldos.saldo_inicial.number()),
                        m('td.text-right', cuenta.saldos.debe.number()),
                        m('td.text-right', cuenta.saldos.haber.number()),
                        m('td.text-right', cuenta.saldos.saldo_final.number())
                    ]);
                })  
            ])
        ])  
    ]);
};