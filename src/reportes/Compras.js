/**
 *  Creamos nuestro Modelo
 */
var Ventas = module.exports = {};

var FechasBar = require('../misc/components/FechasBar2.js');

/**
 * Creamos una lista vacia de la lista de ventas
 * @type Array
 */
Ventas.lista = Array;
Ventas.tercero = Array;
Ventas.sucursal = Array;
Ventas.vendedor = Array;
Ventas.productos = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 * @param  array data Informacion de la venta
 * @return void
 */

var sum = 0;
Ventas.setinfo = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    var venta = data.venta_compra * 1;
    var notas_credito = data.notas_credito * 1;
    var saldo = data.saldo * 1;
    this.nombre = m.prop(data.nombre);
    this.ventas = m.prop(venta);
    this.notas_credito = m.prop(notas_credito);
    this.total = m.prop(venta - notas_credito);
    this.saldo = m.prop(saldo);
    this.cantidad_venta = m.prop('('+data.cantidad_venta_compra+')');
    this.cantidad_nota = m.prop('('+data.cantidad_nota+')');
};

Ventas.setTotales = function(list){
    var tventas = 0;
    var tnotas = 0;
    var tsaldo = 0;
    list.map(function(data) {
        tventas = tventas + (data.ventas() * 1);
        tnotas = tnotas + (data.notas_credito() * 1);
        tsaldo = tsaldo + (data.saldo() * 1);
    });
    var total = tventas - tnotas;
    Ventas.tventas = tventas;
    Ventas.tnotas = tnotas;
    Ventas.tsaldo = tsaldo;
    Ventas.total = total;
    console.log(Ventas);
}

/**
 * View-Model interaccion
 * @return objeto
 */
Ventas.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function(fecha_inicial, fecha_final) {
        //a running list of todos
        vm.tercero = new Ventas.tercero();
        vm.sucursal = new Ventas.sucursal();
        vm.vendedor = new Ventas.vendedor();
        vm.productos = new Ventas.productos();
        this.getVentas(fecha_inicial, fecha_final);
        vm.lista = vm.tercero;
    };

    vm.getVentas = function(fecha_inicial,fecha_final){
        /*oorden().then(function () {
            return  m.request({
              url : '/api/reportes/compras',
              method : 'POST',
              data : {
                  'fechai':fecha_inicial,
                  'fechaf':fecha_final
              }
          });
        })
        .then(function (r) {
           // console.log('Daniel:', oorden.organizacion.format(1256.93));

            if(r.data != 'No hay registros para mostrar.'){
                Object.keys(r.data).map(function (k) {
                    Object.keys(r.data[k]).map(function (ke) {
                        if(k == 'sucursal'){
                            vm.sucursal.push(new Ventas.setinfo(r.data[k][ke]));
                        }
                        if(k == 'tercero'){
                            vm.tercero.push(new Ventas.setinfo(r.data[k][ke]));
                        }
                        if(k == 'vendedor'){
                            vm.vendedor.push(new Ventas.setinfo(r.data[k][ke]));
                        }
                        if(k == 'productos'){
                            vm.productos.push(new Ventas.setinfo(r.data[k][ke]));
                        }
                    });
                });
            }

            var timeoutId = null;
            function clickTab() {
                if (!!$("#ventas").find("> div > ul > li.active > a").length) {
                    document.querySelector("#ventas > div > ul > li.active > a").click();
                } else {
                    clearTimeout(timeoutId);
                    timeoutId = setTimeout(function () {
                        clickTab();
                    }, 1000);
                    // console.log(timeoutId);
                }
            }
            // console.log(timeoutId);
            clickTab();
        });*/
    };

    return vm;
}())

Ventas.controller = function()  {
    Ventas.vm.init(null, null);
    Ventas.vm.tab = 1;
};


Ventas.fechasBar = new FechasBar.controller({
    onchange : function () {
        var val = Ventas.fechasBar.value;
        var fecha_inicial = val.fechaDesde();
        var fecha_final = val.fechaHasta();
        
        if(fecha_inicial && fecha_final){
            Ventas.vm.init(fecha_inicial, fecha_final);
        }
    }
});

Ventas.fechasBar.esteAno();

Ventas.view = function() {
    return m('div');
    /*return m('div', [
        m('h3.column-title', 'Compras'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', FechasBar.view(Ventas.fechasBar))
            ])
        ]),
        m('ul.nav.nav-tabs.with-panel',[
            m(Ventas.vm.tab == 1 ? 'li.active' : 'li',{id: "tercero"},[
                m('a',
                    {
                        href:"javascript:void(0)",
                        onclick:function(){
                            Ventas.vm.lista = Ventas.vm.tercero;
                            Ventas.vm.tab = 1;
                            Ventas.setTotales(Ventas.vm.tercero);
                        }
                    },
                    'Proveedores ',
                    m('span.t.small.gray-circle', Ventas.vm.tercero.length)
                )
            ]),
            m(Ventas.vm.tab == 2 ? 'li.active' : 'li',{id: "vendedor"},[
                m('a',
                    {
                        href:"javascript:void(0)",
                        onclick:function(){
                            Ventas.vm.lista = Ventas.vm.vendedor;
                            Ventas.vm.tab = 2;
                            Ventas.setTotales(Ventas.vm.vendedor);
                        }
                    },
                    'Vendedor ',
                    m('span.t.small.gray-circle', Ventas.vm.vendedor.length)
                )
            ]),
            m(Ventas.vm.tab == 3 ? 'li.active' : 'li',{id: "sucursal"},[
                m('a',
                    {
                        href:"javascript:void(0)",
                        onclick:function(){
                            Ventas.vm.lista = Ventas.vm.sucursal;
                            Ventas.vm.tab = 3;
                            Ventas.setTotales(Ventas.vm.sucursal);
                        }
                    },
                    'Sucursal ',
                    m('span.t.small.gray-circle', Ventas.vm.sucursal.length)
                )
            ]),
            m(Ventas.vm.tab == 4 ? 'li.active' : 'li',{id: "productos"},[
                m('a',
                    {
                        href:"javascript:void(0)",
                        onclick:function(){
                            Ventas.vm.lista = Ventas.vm.productos;
                            Ventas.vm.tab = 4;
                            Ventas.setTotales(Ventas.vm.productos);
                        }
                    },
                    'Productos ',
                    m('span.t.small.gray-circle', Ventas.vm.productos.length)
                )
            ])
        ]),
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead',
                    m('tr',[
                        m('th','Nombre'),
                        m('th','Compras'),
                        m('th','Notas de crédito'),
                        m('th','Total'),
                        m('th','Saldo'),
                    ])
                ),
                m('tbody',[
                    Ventas.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.nombre()),
                            m('td',oorden.organizacion.format(data.ventas()) + ' ' + data.cantidad_venta()),
                            m('td',oorden.organizacion.format(data.notas_credito()) + ' ' + data.cantidad_nota()),
                            m('td',oorden.organizacion.format(data.total())),
                            m('td',oorden.organizacion.format(data.saldo())),
                        ]);
                    }),
                    m('tr',[
                        m('td',""),
                        m('td',oorden.organizacion.format(Ventas.tventas)),
                        m('td',oorden.organizacion.format(Ventas.tnotas)),
                        m('td',oorden.organizacion.format(Ventas.total)),
                        m('td',oorden.organizacion.format(Ventas.tsaldo)),
                    ])
                ])
            ])
        ])
    ]);*/
};
