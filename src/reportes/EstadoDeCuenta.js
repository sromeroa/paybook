var EdoDeCta = module.exports = {};
var Recordatorio = require('./components/Recordatorio');
var NotasComponent = require('../misc/components/NotasComponent');

var reportes = {};

/**
 * 
 * Definición de reportes
 *
 **/
reportes.cliente = {
    factores : {VTA:1, VNC:-1, IDC:-1},
    cuentasPor : 'cobrar',
    documento : 'Factura',
    tipo_documento : 'vta',
    pago : 'Ingreso',
    tipo_pago : 'idc'
};

reportes.proveedor = {
    factores : {COM:1, CNC:-1, EPP:-1},
    cuentasPor : 'pagar',
    documento : 'Compra',
    tipo_documento : 'com',
    pago : 'Egreso',
    tipo_pago : 'epp'
};


var tabs = m.prop([
    {
        title : '',
        name : '',
        query : ''
    }
]);

EdoDeCta.controller = function (args) {
    var ctx = this;
    var reporte = reportes[args.reporte];
    var factores  = reporte.factores;

    ctx.tercero = m.prop({});
    ctx.contacto = m.prop();
    ctx.reporte = m.prop(args.reporte);
    ctx.settings = m.prop(reporte);
    ctx.operaciones = m.prop();
    ctx.cuentasPor = m.prop(reporte.cuentasPor);
    ctx.dias = m.prop(30);

    ctx.totalAnual = Modelo.numberProp(m.prop(0),2);

    ctx.saldoTotal = Modelo.numberProp(m.prop(0),2);
    ctx.saldoVencido = Modelo.numberProp(m.prop(0),2);
    ctx.saldoDias = Modelo.numberProp(m.prop(0),2);
    ctx.incluir_pasados = m.prop(true);

    ctx.tabla = Tabla(ctx);
    ctx.tabs = Tabs();
    ctx.tab = m.prop(ctx.tabs.tabs[0]);

    console.log( ctx.reporte().cuentasPor )
    ctx.contexto = m.prop( ctx.reporte().cuentasPor );

    ctx.selectTab = function (tab) {
        ctx.tab(tab);
        filtrarOperaciones();
    }

    ctx.abrirRecordatorio = function () {
        Recordatorio.openModal.call(this);
    }

    ctx.incluirPasados = function (val) {
        ctx.incluir_pasados(val);
        ctx.actualizarDias();
    }

    ctx.actualizarDias = function (dias) {
        if(arguments.length == 0) {
            dias = ctx.dias();
        }

        var limitDate = new Date;
        var now = new Date;
        var incluir_pasados = ctx.incluir_pasados();

        ctx.dias(Math.max(Number(dias),0));


        limitDate.setDate(limitDate.getDate() + ctx.dias());

        var limitDateSql = oor.fecha.toSQL(limitDate);
        var nowSQL = oor.fecha.toSQL(now);

        var saldoDias = 0;

        ctx.operaciones().filter(function (d) {
            console.log(d.tipo_operacion())
            return d.tipo_operacion() == reporte.tipo_documento.toUpperCase();
        }).forEach(function (op) {
            var incluir = incluir_pasados || (op.fecha_esperada() >= nowSQL);
            if(incluir && op.fecha_esperada() <= limitDateSql) {
                saldoDias += op.saldo.$int();
            }
        });


        console.log(saldoDias);

        ctx.saldoDias.$int(saldoDias);

    }

    ctx.cuentasBancarias = m.prop();

    ctx.cargarCuentasBancarias = function () {
        if(ctx.cuentasBancarias()) return;

        m.request({
            url : '/apiv2?modelo=cuentas&es_bancos=1&acumulativa=0',
            method : 'GET'
        }).then(function (r) {
            ctx.cuentasBancarias(r.data);
        })
    }

    cargar();

    function cargar () {
        var Tercero = oor.mm('Tercero');

        Tercero.include = m.prop('');

        Tercero.get(args.tercero_id)
            .then(ctx.tercero)
            .then(cargarContacto)
            .then(cargarDatosReporte)
            .then(function (r) {
                var Operacion = oor.mm('Operacion');
                var saldo = 0;
                var vencido = 0;
                var now = new Date;
                var factorDias = 1000 * 60 * 60 * 24;
                var now_sql = oor.fecha.toSQL(now);

                if(r.data.anual) {
                    ctx.totalAnual(r.data.anual['+'] - r.data.anual['-'] );
                }

                var operaciones = r.data.documentos.map(function (op) {
                    var operacion = new Operacion(op);
                    var factor = factores[operacion.tipo_operacion()];

                    operacion.feHover = m.prop(false);
                    operacion.feEdit  = m.prop(false);
                    operacion.selected = m.prop(false);
                    operacion.vencido = m.prop(false);
                    operacion.proxDias = m.prop(false);

                    operacion.dias_vencimiento = m.prop();
                    operacion.dias_esperada = m.prop();

                    if(factor === 1) {
                        var vence = oor.fecha.fromSQL(operacion.fecha_vencimiento());
                        var esp = oor.fecha.fromSQL( operacion.fecha_esperada() );

                        var dias_vencimiento = Math.floor((now-vence)/factorDias);
                        var dias_esperada = Math.floor((now-esp)/factorDias);

                        operacion.dias_vencimiento( dias_vencimiento );
                        operacion.dias_esperada( dias_esperada );
                    }

                    operacion.guardarFE = function (fecha_esperada) {
                        operacion.fecha_esperada(fecha_esperada);

                        m.request({
                            url:'/api/operacion/agregar',
                            method : 'POST',
                            data :operacion
                        })
                    }

                    saldo += operacion.saldo.$int() * factor;

                    if(factor === 1) {
                        if(now_sql > operacion.fecha_vencimiento()) {
                            vencido += operacion.saldo.$int();
                            operacion.vencido(true);
                        }
                    }

                    return operacion;
                });

                
                ctx.saldoTotal.$int(saldo);
                ctx.saldoVencido.$int(vencido);
                ctx.operaciones(operaciones);
                ctx.actualizarDias();
                filtrarOperaciones();
            })
    }


    function cargarContacto () {
        var ctoId = ctx.tercero().contacto_principal_id();
        if(!ctoId) return;
        return oor.mm('Tercero').get(ctoId).then(ctx.contacto);
    }


    function cargarDatosReporte () {
        return m.request({
            url : '/reportes/tercero',
            data : {
                tercero_id : ctx.tercero().tercero_id(),
                reporte : ctx.reporte()
            }
        });
    }

    function filtrarOperaciones () {
        var operaciones = ctx.operaciones().filter(ctx.tab().filter);
        ctx.tabla.items(operaciones);
    }

};

EdoDeCta.view = function (ctx) {
    var tercero_id = ctx.tercero().tercero_id();

    var notasCmp = m.component(NotasComponent, {
        pertenece_a_id : ctx.tercero().tercero_id,
        contexto : ctx.contexto
    });

    return m('div.row', [

        m('.col-md-6', [
            m('h2', {style:'margin-bottom:4px'}, ctx.tercero().nombre()),
            ctx.contacto() ? [
                m('span', ctx.contacto().nombre()), ' ',
                m('span', ctx.contacto().email())
            ] : '',
            m('h5', 'Total del Año: ', ctx.totalAnual.number()),

            m('h3', {style:'margin:0px'}, 'Por ', ctx.settings().cuentasPor, ' : ', ctx.saldoTotal.number()),
            m('h5.text-red', 'Vencido: ', ctx.saldoVencido.number()),
            m('h5', [
                'Por ' + ctx.cuentasPor() + ' en ',
                m('input[type="number"]', {
                    style:"width:4em",
                    value : ctx.dias(),
                    onchange : m.withAttr('value', ctx.actualizarDias)
                }),
                ' días: ',
                ctx.saldoDias.number()
            ]),
        ]),

        m('.col-md-6', notasCmp),

        m('.row', [
            m('.col-md-12', [
                m('button.btn.btn-default.pull-right', {
                    onclick : ctx.abrirRecordatorio
                }, 'Recordatorio'),

                m('a.btn.btn-default.pull-right', {
                    href : '/operaciones/' + ctx.settings().tipo_documento + '/crear?tercero_id='+ tercero_id 
                }, ctx.settings().documento),

                m('.btn-group.dropdown.pull-right', [
                    m('btn.btn.btn-default.pull-right.dropdown-toggle[data-toggle="dropdown"]', {
                        onclick : ctx.cargarCuentasBancarias
                    }, ctx.settings().pago, ' ', m('span.caret')),

                    m('ul.dropdown-menu.dropdown-menu-right', [

                        ctx.cuentasBancarias() ? ctx.cuentasBancarias().map(function (cta) {
                            
                            var href = '/operaciones/' + ctx.settings().tipo_pago + '/crear';
                            href += '?tercero_id=' + tercero_id;
                            href += '&cuenta=' + cta.cuenta_contable_id; 

                            return m('li', [
                                m('a', {href : href}, cta.nombre)
                            ])
                        }) : m('li', m('i.fa.fa-spin.fa-spinner'))

                    ])
    
                ])
                
            ])
        ]),

        m('br'),
        m('br'),

       

        MTModal.view()
    ])
};


/** 
 *
 * Tabs 
 *
 */

function Tabs (args) {
    var tabs = [];

    tabs.push({
        title : 'Con Saldo',
        name : 'S',
        filter : function (item) { return item.saldo.$int() }
    });

    tabs.push({
        title : 'Con Saldo Vencido',
        name : 'V',
        filter : function (item) {
            return item.saldo.$int() && item.vencido();
        }
    });

    tabs.push({
        title : 'Con Anticipo',
        name : 'A',
        filter : function (item) {
            return item.saldo.$int() && (item.seccion() == 'E' || item.seccion() == 'I')
        }
    });

    tabs.push({
        title : 'Todos',
        name : 'T',
        filter : function () { return true }
    });

    return {tabs : tabs};
}


Tabs.view = function (ctx) {
    return m('.row', [
        m('.simpleTabs.col-md-10.col-md-offset-1', [
            m('ul.nav.nav-tabs.nav-justified.simple', {style:'margin-bottom:10px'}, [
                ctx.tabs.tabs.map(function (tab) {
                    return m('li', {'class':tab.name === ctx.tab().name ? 'active' : ''}, [
                        m('a[href="javascript:;"]', {onclick: ctx.selectTab.bind(ctx, tab)}, tab.title)
                    ]);
                })
            ])
        ])
    ]);
}

/** 
 *
 * Columnas para la tabla
 *
 *
 **/
function Tabla (ctx) {
    var defCol = {};

    defCol.check = {
        caption : '',
        value : function (item) {
            return m('div.checkboxer.checkboxer-indigo.form-inline', [
                m('input[type="checkbox"]', {
                    id:'check-' + item.tercero_id(),
                    checked : item.selected(),
                    onchange:m.withAttr('checked', item.selected)
                }),
                m('label', {
                    'for': 'check-' + item.tercero_id()
                }, '')
            ]);
        },
        sortable:false
    };

    defCol.fecha = {
        caption : 'Fecha'
    }

    defCol.label = {
        caption : 'Documento',
        value : function (item) {
            return m('a.text-indigo', {
                href : '/operaciones/' + item.tipo_operacion().toLowerCase() + '/' + item.operacion_id(),
                target : '_blank'
            }, item.label())
        }
    };

    defCol.fecha_vencimiento = {
        caption : 'Vence'
    };

    defCol.fecha_esperada = {
        caption : 'Plan',
        value : function (item) {
            return m('div', {
                onmouseenter : function () { 
                    item.feHover(true) 
                },
                onmouseleave : function () { 
                    item.feHover(false) 
                }
            },
            item.feEdit() ? [

                m('input[type="date"]', {
                    config : function (element, isInited) {
                        !isInited && element.focus();
                    },
                    value : item.fe(),
                    onchange : m.withAttr('value', item.fe)
                }),

                item.fe() != item.fecha_esperada() ? m('button.btn.btn-sm.btn-default', {
                    onclick : function () {
                        item.guardarFE(item.fe())
                    }
                }, 'Guardar') : ''

            ] : [
                item.fecha_esperada(),
                item.feHover() ? m('a[href="javascript:;"]', {
                    onclick : function () {
                        item.feEdit(true);
                        item.fe = m.prop(item.fecha_esperada());
                    }
                }, m('i.fa.fa-pencil')) : ''
            ]
            );
        }
    };

    defCol.total = {
        caption : 'Importe',
        value : function (item) {
            return item.total.number();
        }
    };

    defCol.saldo = {
        caption : 'Saldo',
        value : function (item) {
            return item.saldo.number();
        }
    };

    defCol.dias_vencimiento = {
        caption : 'Vence',
        value : function (item) {
            return m('div.text-right', [
                m('div', item.fecha_vencimiento()),
                item.dias_vencimiento() === 0 ? m('small.text-indigo', 'hoy') : '',
                item.dias_vencimiento() > 0 ? m('small.text-red', 'hace ' , item.dias_vencimiento() , ' dias') : '',
                item.dias_vencimiento() < 0 ? m('small', 'en ' , Math.abs(item.dias_vencimiento()) , ' dias') : ''
            ])
        }
    }

    defCol.dias_esperada = {
        caption : 'Esperada',
        value : function (item) {
            return m('div.text-right', [
                m('div', item.fecha_esperada()),
                item.dias_esperada() === 0 ? m('small.text-indigo', 'hoy') : '',
                item.dias_esperada() > 0 ? m('small.text-red', 'hace ' , item.dias_esperada() , ' dias') : '',
                item.dias_esperada() < 0 ? m('small', 'en ' , Math.abs(item.dias_esperada()) , ' dias') : ''
            ])
        }
    }

    defCol.fecha = {
        caption : 'Fecha',
        value : function (item) {
            var f = oor.fecha.fromSQL(item.fecha());
            return f.toLocaleDateString();
        }
    }
    var table = MtTable({
        columnas : ['check','fecha','label', 'dias_vencimiento', 'dias_esperada', 'total', 'saldo'],
        defCol : defCol
    });

    table.itemTrParams = function (item) {
        return {'class' : item.selected() ? 'warning' : ''};
    }

    return table;
}
