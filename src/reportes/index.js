module.exports = {
    AuxiliarContable : require('./AuxiliarContable.js'),
    Balanza : require('./Balanza.js'),
    CuentasPorPagar: require('./CuentasPorPagar'),
    EstadoDeCuenta : require('./EstadoDeCuenta'),
    Compras : require('./Compras.js'),
    Ventas : require('./Ventas.js'),
    Operaciones : require('./Operaciones.js'),
    MovimientoEfectivo : require('./MovimientoEfectivo.js'),
    cxp : require('./cxp.js'),
    cxc : require('./cxc.js'),
    Bancos : require('./Bancos.js'),
    BancosEstado : require('./BancosEstado.js'),
    estadoDeCuenta : require('./estado-de-cuenta'),
    VentasVue: require('./ventas-vue'),
    events: require('./events'),
    Plantilla: require("../plantillas/index")
}