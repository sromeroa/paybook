module.exports = seleccionarTipoFecha

seleccionarTipoFecha.tipos = m.prop([
    {caption:'Vencimiento', key:'fecha_vencimiento'},
    {caption:'Esperada', key:'fecha_esperada'}
]);


seleccionarTipoFecha.tipoDefault = 'fecha_vencimiento';

function seleccionarTipoFecha (ctx) {
    return [
        m('span', 'Considerar Fecha: '),
        m('ul', [
            seleccionarTipoFecha.tipos().map(function (tf) {
                return m('li', [
                    m('label',[
                        m('input[type=radio][name=tipo-fecha]',{
                            checked :  ctx.tipoFecha() == tf.key,
                            onchange : function () { ctx.setTipoFecha(tf.key) }
                        }),
                        ' ',
                        tf.caption,
                        ' '
                    ])
                ]);
            })
        ])
    ];
}