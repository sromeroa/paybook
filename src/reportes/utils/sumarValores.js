module.exports = sumarValores;

 
function sumarValores (items, signature) {

    var result = {};
    var keys = Object.keys(signature);


    keys.forEach(function (key) {
        var value = 0;
        var getter = signature[key];

        items.forEach(function (item) {
            var val = getter(item);
            if(!val) val = 0;
            value += val;
        });


        result[key] = value;
    });


    return result;
}