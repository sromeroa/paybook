module.exports = revaluarMonedas;

function revaluarMonedas (items, monedas, signature, decimales) {
    var fields = Object.keys(signature);

    if(!monedas) {
        throw 'No hay monedas';
    }

    if(typeof decimales == 'undefined') {
        decimales = 2;
    }


    items.forEach(function (item) {
        var moneda = f(item.moneda)(monedas);

        if(!moneda || !moneda.valor) {
            throw 'No hay valor para ' + item.moneda;
        }

        fields.forEach(function (field) {
            var valor;
            var prop;

            valor = f(signature[field])(item);
            valor = Modelo.numberProp(m.prop(valor), decimales);
            

            prop = Modelo.numberProp(m.prop(0), decimales);

            prop.$int( valor.$int() * moneda.valor);

            item[field] = prop;
        });

    });
}