/**
 *	Creamos nuestro Modelo
 */
var MovimientoEfectivo = module.exports = {};

/**
 * Creamos una lista vacia de la lista de ventas
 * @type Array
 */
MovimientoEfectivo.lista = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 * @param  array data Informacion de los movimientos
 * @return void
 */
MovimientoEfectivo.movimiento = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    this.cuenta = m.prop(data.cuenta);
    this.nombre = m.prop(data.nombre);
    this.descripcion = m.prop(data.descripcion);
    this.naturaleza = m.prop(data.naturaleza == "D" ? "Debito" : "Credito");
    this.Anterior = m.prop(data.Anterior);
    this.Enero = m.prop(data.Enero);
    this.Febrero = m.prop(data.Febrero);
    this.Marzo = m.prop(data.Marzo);
    this.Abril = m.prop(data.Abril);
    this.Mayo = m.prop(data.Mayo);
    this.Junio = m.prop(data.Junio);
    this.Julio = m.prop(data.Julio);
    this.Agosto = m.prop(data.Agosto);
    this.Septiembre = m.prop(data.Septiembre);
    this.Octubre = m.prop(data.Octubre);
    this.Noviembre = m.prop(data.Noviembre);
    this.Diciembre = m.prop(data.Diciembre);
};

/**
 * View-Model interaccion
 * @return objeto
 */
MovimientoEfectivo.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function(year) {
        //a running list of todos
        vm.lista = new MovimientoEfectivo.lista();
        this.getMovimientoEfectivo(year);
    };

    vm.getMovimientoEfectivo = function(year){
       m.request({
            url : '/api/reportes/flujo-de-efectivo/'+year,
            method : 'GET'
        }).then(function (r) {
            r.data.map(function (data) {
                vm.lista.push(new MovimientoEfectivo.movimiento(data));
            });
        });
    };
    return vm;
}())

MovimientoEfectivo.controller = function()  {
    MovimientoEfectivo.year = m.prop(2016);
    MovimientoEfectivo.vm.init(MovimientoEfectivo.year());
};

MovimientoEfectivo.view = function(ctrl) {
    return m('div', [
        m('h3.column-title', 'Movimiento de Efectivo'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', [
                    m('.mt-inputer', [
                        m('label', 'Año'),
                        m('input.dextras[type="number"][name=am]',  {
                            style:'width:40px',
                            onchange: update,
                            value: MovimientoEfectivo.year(),
                            oninput : m.withAttr('value',MovimientoEfectivo.year)
                        }),
                    ])
                ])
            ])
        ]),
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead', 
                    m('tr',[
                        m('th','Cuenta'),
                        m('th','Nombre'),
                        m('th','Anterior'),
                        m('th','Enero'),
                        m('th','Febrero'),
                        m('th','Marzo'),
                        m('th','Abril'),
                        m('th','Mayo'),
                        m('th','Junio'),
                        m('th','Julio'),
                        m('th','Agosto'),
                        m('th','Septiembre'),
                        m('th','Octubre'),
                        m('th','Noviembre'),
                        m('th','Diciembre'),
                    ])
                ),
                m('tbody',[
                    MovimientoEfectivo.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.cuenta()),
                            m('td',data.nombre()),
                            m('td',data.Anterior()),
                            m('td',data.Enero()),
                            m('td',data.Febrero()),
                            m('td',data.Marzo()),
                            m('td',data.Abril()),
                            m('td',data.Mayo()),
                            m('td',data.Junio()),
                            m('td',data.Julio()),
                            m('td',data.Agosto()),
                            m('td',data.Septiembre()),
                            m('td',data.Octubre()),
                            m('td',data.Noviembre()),
                            m('td',data.Diciembre()),
                        ]);
                    })
                ])
            ])
        ])
    ]);
};

function update() {
    MovimientoEfectivo.vm.init(MovimientoEfectivo.year());
}