var AuxiliarContable = module.exports = {};
var FechasMultiSelector = require('../misc/components/FechasMultiSelector');

AuxiliarContable.controller = function () {
    var cuentasSelector;
    var ctrl = this;
    var search = m.route.parseQueryString(location.search);
    var now = new Date;

    search.cuenta = location.pathname.split('/reportes/auxiliar/');

    this.tipo         = m.prop();

    this.mes_contable = m.prop();
    this.ano_contable = m.prop();
    this.fecha_inicial= m.prop()
    this.fecha_final  = m.prop();
    this.cuenta_id    = m.prop();

    if(search) {
        if(search.mes_contable) {
            this.tipo('fiscal')
            this.mes_contable( search.mes_contable );
            this.ano_contable( search.ano_contable );
            this.fecha_inicial( oor.fecha.toSQL(oor.fecha.inicialMes(now)) )
            this.fecha_final( oor.fecha.toSQL(oor.fecha.finalMes(now)) )

        } else {
            this.tipo('fechas');
            this.fecha_inicial(search.fecha_inicial);
            this.fecha_final(search.fecha_final);
            this.ano_contable(now.getFullYear());
            this.mes_contable(now.getMonth() + 1)
        }

        if(search.cuenta) {
            this.cuenta_id(search.cuenta);
        }
    }

    this.cuenta = m.prop();
    this.movimientos = m.prop();
    this.saldoInicial = Modelo.numberProp(m.prop(),2);
    this.saldoFinal = Modelo.numberProp(m.prop(),2);


    this.fecha = {
        tipo : this.tipo,
        mes_contable : this.mes_contable,
        ano_contable : this.ano_contable,
        hasta : this.fecha_final,
        desde : this.fecha_inicial,
        onchange : update
    }

    oorden().then(function () {
          cuentasSelector = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctrl.cuenta_id,
            onchange : update
        });

        ctrl.cuentasSelector = cuentasSelector;
        update();

    })
  
    function update() {
        var options = { cuenta : ctrl.cuenta_id()}

        if(!options.cuenta) return;

        if(ctrl.tipo() == 'fiscal') {
            options.mes_contable = ctrl.mes_contable();
            options.ano_contable = ctrl.ano_contable();
        } else {
            options.fecha_inicial = ctrl.fecha_inicial();
            options.fecha_final   = ctrl.fecha_final();
        }


        m.request({
            url : '/reportes/auxiliardatos',
            method : 'GET',
            data : options
        }).then(function (r) {
            ctrl.cuenta(r.cuenta);
            ctrl.saldoInicial(r.saldo_inicial);
            ctrl.saldoFinal(r.saldo_inicial);

            ctrl.movimientos(
                r.movimientos.map(function (mov) {
                    mov.importe = Modelo.numberProp(m.prop(mov.importe),2);
                    mov.saldo   = Modelo.numberProp(m.prop(),2);

                    var saldo = ctrl.saldoFinal.$int() + (mov.importe.$int() * mov.debe_o_haber * r.cuenta.multiplicador);
                    mov.saldo.$int(saldo);
                    ctrl.saldoFinal.$int(saldo);
                    return mov;
                })
            );
        })
    }
}

AuxiliarContable.view = function (ctrl) {
    var Poliza = oor.mm('Poliza');

    return m('div', [
        m('h3.column-title', 'Auxiliar Contable'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', m.component(FechasMultiSelector, ctrl.fecha)),
                m('div.col-sm-6', [
                    m('label', 'Cuenta'),
                    m('div.cuentas-selector', [
                        ctrl.cuentasSelector ? ctrl.cuentasSelector.view() :''
                    ])
                ])
                
            ])
        ]),

        m('table.table.bill', [
            m('thead', [
                m('tr', [
                    m('th','Fecha'),
                    m('th','Póliza'),
                    m('th','Concepto'),
                    m('th.text-right','Debe'),
                    m('th.text-right','Haber'),
                    m('th.text-right','Saldo')
                ])
            ]),
            m('tbody', [
                m('tr', [
                    m('th.text-right[colspan=5]', 'Saldo Inicial'),
                    m('td.text-right', ctrl.saldoInicial.number())
                ]),
                ctrl.movimientos() ? ctrl.movimientos().map(function (mov) {
                    var tipo = Poliza.tipos[mov.tipo];
                    tipo = tipo ? tipo() : {};

                    var urlPoliza = '/polizas/poliza/' + mov.poliza_id;

                    return m('tr', [
                        m('td', mov.fecha_p),
                        m('td', 
                            m('strong',m('a', {href:urlPoliza}, tipo.text, ' #' , mov.numero))
                        ),
                        m('td', mov.concepto),
                        m('td.text-right', mov.debe_o_haber == 1 ? mov.importe.number() : ''),
                        m('td.text-right', mov.debe_o_haber == -1 ? mov.importe.number() : ''),
                        m('td.text-right', mov.saldo.number())
                    ]) 
                }) : '',
                 m('tr', [
                    m('th.text-right[colspan=5]', 'Saldo Final'),
                    m('td.text-right', ctrl.saldoFinal.number())
                ]),
            ])
        ])
    ])  
}