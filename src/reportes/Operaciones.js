/**
 * /api/reportes/operaciones
 */

/**
 *	Creamos nuestro Modelo
 */
var Operaciones = module.exports = {};

var FechasBar = require('../misc/components/FechasBar.js');

/**
 * Creamos una lista vacia de la lista de ventas
 * @type Array
 */
Operaciones.lista = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 * @param  array data Informacion de la operacion
 * @return void
 */
Operaciones.operacion = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    this.cuenta = m.prop(data.cuenta);
    this.nombre = m.prop(data.nombre);
    this.descripcion = m.prop(data.descripcion);
    this.tipo_operacion = m.prop(data.tipo_operacion);
    this.producto_nombre = m.prop(data.producto_nombre);
    this.importe_m_base = m.prop(data.importe_m_base);
    this.seccion = m.prop(data.seccion);
};

/**
 * View-Model interaccion
 * @return objeto
 */
Operaciones.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function(fecha_inicial, fecha_final) {
        //a running list of todos
        vm.lista = new Operaciones.lista();
        this.getOperaciones(fecha_inicial, fecha_final);
    };

    vm.getOperaciones = function(fecha_inicial, fecha_final){
       m.request({
            url : '/api/reportes/ventas',
            method : 'POST',
            data : {
                'fechai':fecha_inicial,
                'fechaf':fecha_final
            }
        }).then(function (r) {
            if(r.data != 'No hay registros para mostrar.'){
                r.data.map(function (data) {
                    vm.lista.push(new Operaciones.operacion(data));
                });
            }
        });
    };
    return vm;
}())

Operaciones.controller = function()  {
    Operaciones.vm.init(null, null);
};

Operaciones.fechasBar = new FechasBar.controller({
    onchange : function (val) {
        var fecha_inicial = val.fecha.$gte;
        var fecha_final = val.fecha.$lte;
        if(fecha_inicial && fecha_final){
            Operaciones.vm.init(fecha_inicial, fecha_final);
        }
    }
});

Operaciones.view = function() {
    return m('div', [
        m('h3.column-title', 'Operaciones'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', FechasBar.view(Operaciones.fechasBar))
            ])
        ]),
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead', 
                    m('tr',[
                        m('th','Cuenta'),
                        m('th','Nombre'),
                        m('th','Descripcion'),
                        m('th','Nombre Producto'),
                        m('th','Seccion'),
                        m('th','Importe'),
                    ])
                ),
                m('tbody',[
                    Operaciones.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.cuenta()),
                            m('td',data.nombre()),
                            m('td',data.descripcion()),
                            m('td',data.producto_nombre()),
                            m('td',data.seccion()),
                            m('td',data.importe_m_base()),
                        ]);
                    })
                ])
            ])
        ])
    ]);
};