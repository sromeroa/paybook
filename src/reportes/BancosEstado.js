/**
 *	Creamos nuestro Modelo
 */
var ctrl = module.exports = {};

var FechasBar = require('../misc/components/FechasBar.js');

/**
 * Creamos una lista vacia de la lista de ventas
 * @type Array
 */
ctrl.lista = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 */
ctrl.datos = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    this.cuenta = m.prop(data.cuenta);
    this.nombre = m.prop(data.nombre);
    this.descripcion = m.prop(data.descripcion);
    this.tipo_operacion = m.prop(data.tipo_operacion);
    this.producto_nombre = m.prop(data.producto_nombre);
    this.naturaleza = m.prop(data.naturaleza == "D" ? "Debito" : "Credito");
    this.importe_m_base = m.prop(data.importe_m_base);
};

/**
 * View-Model interaccion
 * @return objeto
 */
ctrl.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function(fecha_inicial, fecha_final) {
        //a running list of todos
        vm.lista = new ctrl.lista();
        this.getLista(fecha_inicial, fecha_final);
    };

    vm.getLista = function(fecha_inicial, fecha_final){
       m.request({
            url : '/api/reportes/banco-estado',
            method : 'POST',
            data : {
                'fechai':fecha_inicial,
                'fechaf':fecha_final
            }
        }).then(function (r) {
            if(r.data != 'No hay registros para mostrar.'){
                r.data.map(function (data) {
                    vm.lista.push(new ctrl.datos(data));
                });
            }
        });
    };
    return vm;
}())

ctrl.controller = function()  {
    ctrl.vm.init(null, null);
};

ctrl.fechasBar = new FechasBar.controller({
    onchange : function (val) {
        var fecha_inicial = val.fecha.$gte;
        var fecha_final = val.fecha.$lte;
        if(fecha_inicial && fecha_final){
            ctrl.vm.init(fecha_inicial, fecha_final);
        }
    }
});

ctrl.view = function() {
    return m('div', [
        m('h3.column-title', 'Estado Bancario'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', FechasBar.view(ctrl.fechasBar))
            ])
        ]),
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead', 
                    m('tr',[
                        m('th','Cuenta'),
                        m('th','Nombre'),
                        m('th','Descripcion'),
                        m('th','Naturaleza'),
                        m('th','Nombre Producto'),
                        m('th','Importe'),
                        m('th','Tipo de Operacion'),
                    ])
                ),
                m('tbody',[
                    ctrl.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.cuenta()),
                            m('td',data.nombre()),
                            m('td',data.descripcion()),
                            m('td',data.naturaleza()),
                            m('td',data.producto_nombre()),
                            m('td',data.importe_m_base()),
                            m('td',data.tipo_operacion()),
                        ]);
                    })
                ])
            ])
        ])
    ]);
};