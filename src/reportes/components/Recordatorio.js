
var Recordatorio = module.exports = {};

Recordatorio.controller = function (args) {

    var ctx = this;

    ctx.recordatorios = [];
    ctx.medios = [];
    ctx.recordatorio = m.prop();
    ctx.medio = m.prop();

    ctx.recordatorios.push({value:'pendientes', label:'Adeudos Pendientes'});
    ctx.recordatorios.push({value:'vencidos', label:'Adeudos Vencidos'});
    ctx.recordatorios.push({value:'completo', label:'Estado de Cuenta Completo'});

    ctx.recordatorio(ctx.recordatorios[0].value);

    ctx.medios.push({value:'email', label:'Enviar Email',icon : 'fa.fa-envelope-o'})
    ctx.medios.push({value:'pdf', label:'Crear PDF', icon:'fa.fa-file-pdf-o'})
    ctx.medios.push({value:'word', label:'Crear Word', icon:'fa.fa-file-word-o'});

    ctx.medio(ctx.medios[0].value);
}

Recordatorio.view  = function (ctx) {
    return m('div.recordatorio', [
        m('div', [
            m('ul',[
                m('li', ctx.recordatorios.map(function (r) {
                    var id = 'recordatorio-'+r.value;
                    return m('.radioer', [
                        m('input[type="radio"]', {
                            name : 'r-recordatorio',
                            id:id,
                            value : r.value,
                            checked : ctx.recordatorio() === r.value,
                            onchange : function () {
                                this.checked && ctx.recordatorio(r.value);
                            }
                        }),
                        m('label', {'for' : id}, r.label)
                    ])
                }))
            ])
        ]),

        m('div', [
            m('ul.medios', [
                ctx.medios.map(function (r) {
                    return m('li.medio', {
                        'class' : ctx.medio() === r.value ? 'active' : '',
                        onclick : ctx.medio.bind(null, r.value)
                    },[
                        m('i.'.concat(r.icon)),
                        m('div', r.label)
                    ])
                })
            ])
        ])

    ])
};


Recordatorio.openModal = function () {
    
    MTModal.open({
        controller : Recordatorio.controller,
        args : {},
        content : Recordatorio.view,
        top : function () {
            return m('h4', 'Generar Recordatorio con')
        },
        bottom : function  (ctx) {
            return [
                m('button.pull-left.btn-flat.btn-sm.btn-default', {
                    onclick:closeModal.bind(ctx)
                }, 'Cancelar'),
                m('button.pull-right.btn-flat.btn-sm.btn-primary', {
                    onclick:closeModal.bind(ctx)
                }, 'Generar'),
            ]
        },
        modalAttrs : {
            'class' : 'modal-small'
        },
        el : this
    });


    function closeModal () {
        this.$modal.close()
    }
}
