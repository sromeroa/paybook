var CXP = module.exports = {};
var Recordatorio = require('./components/Recordatorio');

var dynTable = require('../nahui/dtable');
var TableHelper = require('../nahui/dtableHelper');
var nahuiCollection = require('../nahui/nahui.collection');
var Field = require('../nahui/nahui.field');
var SearchBar = require('../misc/components/SearchBar');
var seleccionarTipoFecha = require('./utils/seleccionarTipoFecha');

var settings = {};

settings.cobrar = {
    tercero : 'Cliente',
    reporte : 'cobrar',
    link : link('cliente'),
    cuentasPor : 'Cobrar',
    documentos : {VTA:1, VNC:-1, IDC:-1},
    pagosEn : 'IDC',
    tableLabels : {anual : 'Ventas del Año'},
    dias : 30
};

settings.pagar = {
    tercero : 'Proveedor',
    reporte : 'pagar',
    link : link('proveedor'),
    cuentasPor : 'Pagar',
    documentos : {COM:1, CNC:-1, EPP:-1},
    pagosEn : 'VTA',
    tableLabels : {anual : 'Compras del Año' },
    dias : 30
};

function link (reporte) {
    var terceroId = f('tercero_id');
    return function (t) {
        return '/reportes/' + reporte + '?tercero_id=' + terceroId(t);
    }
}

function captionDias (dias) {
    return 'Prox. ' + dias + ' días'
}

var tabs = m.prop([
    {
        caption : 'Con Saldo', 
        name:'S', 
        query : {saldo : {$gt:0}, saldoVencido:undefined, anticipo:undefined}
    },
    {
        caption : 'Con Saldo Vencido', 
        name:'V', 
        query :{saldo : undefined, saldoVencido:{$gt:0}, anticipo:undefined}
    },
    {
        caption : 'Con Anticipo', 
        name : 'A',
        query :{saldo : undefined, saldoVencido:undefined, anticipo:{$gt:0}}
    },
    {
        caption : 'Todos',
        name:'T',
        query :{saldo : undefined, saldoVencido:undefined, anticipo:undefined}
    }
]);

CXP.controller = function (args) {
    var ctx = this;
    var table, collection, query, thelper, columnas, fields, selectedTab;

    ctx.settings = m.prop(settings[args.reporte]);

    ctx.search = m.prop('');

    fields = TableFields(ctx.settings());

    columnas = [
        fields.nombre,
        fields.anual,
        fields.anticipos,
        fields.saldo,
        fields.saldoVencido,
        fields.saldoFecha
    ];

    table = dynTable().key( f('tercero_id') );

    collection = nahuiCollection('terceros', {
        identifier : 'tercero_id'
    });

    query = collection.query();

    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(columnas)
                .set({ pageSize : 10});


    ctx.searchBar = new SearchBar.controller({
        search : ctx.search,
        onsearch : function () {
            var search = ctx.search() ? {$contains:ctx.search()} : undefined;
            query.add({nombre:search}).exec();
        }
    });


    ctx.setDias = function (dias) {
        ctx.dias(dias);
        fields.saldoFecha.caption = captionDias(dias);
        actualizarDatosTerceros();
        thelper.draw();
    }

    ctx.setTipoFecha = function (tf) {
        ctx.tipoFecha(tf)
        actualizarDatosTerceros();
        thelper.draw();
    }

    selectedTab = m.prop(tabs()[0]);

    ctx.tab = function () {
        if(arguments.length) {
            selectedTab(arguments[0]);
            query.add(selectedTab().query).exec();
        }
        return selectedTab();
    }
     
    this.busy = m.prop(false);

    this.saldoTotal = Modelo.numberProp(m.prop(0),2);
    this.saldoVencido = Modelo.numberProp(m.prop(0),2);
    this.saldoDias = Modelo.numberProp(m.prop(0),2);

    this.tipoFecha = m.prop( seleccionarTipoFecha.tipoDefault );
    this.dias = m.prop(ctx.settings().dias);
    this.incluir_pasados = m.prop(true);


    this.terceros = m.prop();
    this.monedas  = m.prop();
    this.documentos = m.prop();

    this.conversionesMoneda = m.prop();

    



    ctx.abrirRecordatorio = function () {
        Recordatorio.openModal.call(this);
    }

    ctx.actualizarDatos = function (documentos) {
        ctx.saldoTotal(0);
        var importe = Modelo.numberProp(m.prop(0), 2);
        var vDocs = ctx.settings().documentos;

        var operaciones =  documentos
            .filter(function (d) {
                var tipoOp = f('tipo_operacion')(d);

                if(vDocs[tipoOp]) {
                    d.saldo = d.saldo * vDocs[tipoOp];
                    return true;
                }

                return false;
            })
            .map(function (doc) {
                importe(f('saldo')(doc));
                ctx.saldoTotal.$int( ctx.saldoTotal.$int()  + importe.$int() );
                return doc;
            });

        return operaciones;
    }


    ctx.setupTable = function (element, isInitialized) {
        if(isInitialized)  return;

        thelper.element(element);
        ctx.tab(selectedTab());
    }



    cargarReporte();


    function cargarReporte () {
        ctx.busy(true);

        cargarTerceros()
            .then(cargarMonedas)
            .then(cargarDocumentos)
            .then(ctx.busy.bind(null,false))
    }


    function cargarMonedas () {
        return m.request({
            method : 'GET',
            url : '/reportes/monedas',
            unwrapSuccess : f('data')
        }).then(ctx.monedas)
    }

    function cargarTerceros () {
        return m.request({
            method : 'GET',
            url : '/reportes/listadoterceros',
            unwrapSuccess : f('data')
        })
        .then(ctx.terceros)
        .then(function () {
            ctx.terceros().forEach(collection.insert);
        });
    }

    function cargarDocumentos () {
        return m.request({
            method : 'GET',
            url : '/reportes/documentos',
            unwrapSuccess : f('data')
        })
        .then(ctx.actualizarDatos)
        .then(ctx.documentos)
        .then(actualizarDatosTerceros)
    }


    function actualizarDatosTerceros () {
        var tercerosMap  = {};
        var documentos = ctx.documentos();
        var terceros = ctx.terceros();

        var getFecha = f( ctx.tipoFecha() );
        var settings  =ctx.settings();
        var saldo = Modelo.numberProp(m.prop(0),2);
        var nowDate = new Date;
        var now = oor.fecha.toSQL(nowDate);
        nowDate.setDate(nowDate.getDate() + ctx.dias());
        var fecha = oor.fecha.toSQL(nowDate);

        ctx.saldoTotal(0);
        ctx.saldoVencido(0); 
        ctx.saldoDias(0);

        terceros.forEach(function (t) {
            tercerosMap[t.tercero_id] = t;
            ajustarTercero(t);
        });

        documentos
            .forEach(function (doc) {
                var terceroId = f('tercero_id')(doc);
                var tercero = tercerosMap[terceroId];
                var tipoOperacion = f('tipo_operacion')(doc);

                saldo( f('saldo')(doc) );

                ctx.saldoTotal.$int( saldo.$int() + ctx.saldoTotal.$int() );
                tercero.saldo.$int( saldo.$int() + tercero.saldo.$int() );

                if(settings.documentos[tipoOperacion] == 1) {
                    if(now > f('fecha_vencimiento')(doc) ) {
                        ctx.saldoVencido.$int( saldo.$int() + ctx.saldoVencido.$int() );
                        tercero.saldoVencido.$int( saldo.$int() + tercero.saldoVencido.$int() );
                    }

                    if(fecha > getFecha(doc) ) {
                        ctx.saldoDias.$int( saldo.$int() + ctx.saldoDias.$int() );
                        tercero.saldoFecha.$int( saldo.$int() + tercero.saldoFecha.$int() );
                    }
                }
            });
    }



    function ajustarTercero (t) {
        t.saldo = Modelo.numberProp(m.prop(0),2);
        t.saldoVencido = Modelo.numberProp(m.prop(0),2);
        t.saldoFecha = Modelo.numberProp(m.prop(0), 2);
        t.anticipos = Modelo.numberProp(m.prop(0), 2);

        t.maxDias = m.prop(null);
    }
};


CXP.reporte = function (ctx) {
    var settings = ctx.settings();

    return m('div', [
        m('div.row', [
            m('div.col-sm-6', [
                m('h3', 'Total Por ', settings.cuentasPor, ' : ' ,ctx.saldoTotal.number()),
                m('h5.text-red', 'Saldo Vencido: ', ctx.saldoVencido.number()),
                m('h5', [
                    'Por ' + settings.cuentasPor + ' en ',
                    m('input[type="number"]', {
                        style:"width:3em",
                        value : ctx.dias(),
                        onchange : m.withAttr('value', ctx.setDias)
                    }),
                    ' días: ',
                    ctx.saldoDias.number()
                ]),

                seleccionarTipoFecha(ctx)
            ])
        ]),


        m('.row', [
            m('.col-sm-6', [
                SearchBar.view(ctx.searchBar)
            ]),
            m('.col-sm-6', [
                m('button.btn.btn-sm.btn-default.pull-right', {
                    onclick:ctx.abrirRecordatorio
                },'Recordatorio')
            ])
        ]),

        m('br'),
        displayTabs(tabs, ctx.tab),
        m('.table-responsive', [
            m('table.table', {config:ctx.setupTable})
        ]),
        
        MTModal.view()
    ])
}

CXP.cargando = function (ctx) {
    return m('div.text-center',[
        m('i.fa.fa-refresh.fa-spin')
    ]);
}

CXP.view = function (ctx) {
    return ctx.busy() ? CXP.cargando(ctx) : CXP.reporte(ctx);
}

function TableFields (settings)  {
    var fields = {};

    fields.nombre = Field.linkToResource('nombre', {
        caption : 'Nombre',
        url : settings.link,
        'class' : 'text-indigo'
    })

    fields.anual = Field('anual', {
        caption : settings.tableLabels.anual,
        'class' : 'text-right'
    })

    fields.anticipos = Field('anticipos', {
        caption : 'Anticipos', 
        'class' : 'text-right'
    })

    fields.saldo = Field('saldo',{
        caption : 'Saldo',
        'class' : 'text-right'
    })

    fields.saldoVencido = Field('saldoVencido', {
        caption : 'Vencido',
        'class' : 'text-right'
    })

    fields.saldoFecha = Field('saldoFecha', {
        caption : captionDias(settings.dias),
        'class' : 'text-right'
    })

    return fields;
}

function displayTabs (tabs, tab) {
    return m('ul.nav.nav-tabs.with-panel', [
        tabs().map(function (t) {
            return m('li', {
                'class' : tab().name == t.name ? 'active' : null,
                onclick : function () {
                    tab(t);
                    (onclick || nh.noop)(t);
                }
            }, [
                m('a', {href:'javascript:;'}, [
                    t.caption,
                    ' '
                ])
            ])
        })
    ]);
}
