module.exports = TablaEstadoDeCuenta;

var dynTable = require('../../nahui/dtable');
var TableHelper = require('../../nahui/dtableHelper');
var nahuiCollection = require('../../nahui/nahui.collection');
var Field = require('../../nahui/nahui.field');

function TablaEstadoDeCuenta (settings) {
    var Operacion = oor.mm('Operacion');
    var thelper, tabla, collection, query, fields, columnas;

    fields = FieldsTabla(settings);

    columnas = [
        fields.doc_ref,
        fields.fecha_vencimiento,
        fields.fecha_esperada,
        fields.total_moneda,
        fields.total_m_base,
        fields.saldoRelativo
    ];

    tabla = dynTable().key( f('operacion_id') );

    collection = nahuiCollection('operaciones', {
        identifier : 'operacion_id'
    });

    query = collection.query();

    thelper = TableHelper()
                .table(tabla)
                .query(query)
                .columns(columnas)
                .set({pageSize : 10})

    thelper.collection = collection;

    return thelper;
}


function FieldsTabla (settings) {
    var Operacion = oor.mm('Operacion');
    var fields = {};

    nh.extend(fields, Operacion.fields());




    fields.fecha_esperada = Field('fecha_esperada', {
        caption : 'Esperada',
        enter : function (selection) {
            selection.call(Field.enter);

            selection.append('span')
                .attr('class', 'hide-on-edit text-blue')
                .attr('style', 'cursor:pointer')
                .attr('x-edit-id', function (d) { return f('operacion_id')(d.row) })
                .attr('x-edit-prop', 'fecha_esperada')
                .attr('x-edit-enable', '')


            var div = selection.append('div').attr('class', 'show-on-edit')
                
            div.append('input')
                .attr('class','pull-left')
                .attr('type','date')
                .attr('x-edit-id', function (d) { return f('operacion_id')(d.row) } )
                .attr('x-edit-prop', 'fecha_esperada')
                .attr('x-edit-trigger', '')

            div.append('span')
                .attr('class', 'close pull-left')
                .html('&times')

        },
        update : function (selection) {
            selection.select('span')
                .attr('x-edit-value', f('value'))
                .call(Field.update);
        }
    });

    fields.total_m_base = Field('total_m_base', {
        caption : 'Total MXN',
        'class' :'text-right'
    });

    fields.saldoRelativo = Field('saldoRelativo', {
        caption : 'Saldo MXN',
        'class' :'text-right',
        update : function (sel) {
            sel.classed({
                'text-red' : function (d) { return d.value < 0}
            }).call(Field.update)
        }
    });

    return fields;
}