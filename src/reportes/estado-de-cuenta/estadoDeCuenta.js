module.exports = EstadoDeCuenta;

var Recordatorio = require('../components/Recordatorio');
var NotasComponent = require('../../misc/components/NotasComponent');
var EdoCtaTabla = require('./tabla');
var revaluarMonedas = require('../utils/revaluarMonedas');
var sumarValores = require('../utils/sumarValores');
var seleccionarTipoFecha = require('../utils/seleccionarTipoFecha');

var tabs = m.prop([
    {
        title : 'Con Saldo',
        name : 'S',
        query : {saldoVencido:undefined, saldo:{$gt:0}, seccion:{$in:['C','V']}}
    },
    {
        title : 'Con Saldo Vencido',
        name : 'V',
        query : {saldoVencido:{$gt:0}, saldo:undefined, tipo_operacion:undefined}
    },
    {
        title : 'Con Anticipo',
        name : 'A',
        query : {saldoVencido:undefined, saldo:{$gt:0}, seccion:{$in:['E','I']}}
    },
    {
        title : 'Todos',
        name : 'T',
        query : {saldo:undefined, saldoVencido:undefined, tipo_operacion:undefined}
    }
]);


function EstadoDeCuenta (settings) {
    return {
        view : View,
        controller : Controller(settings)
    }
}

function Controller (settings) {
    return function EstadoDeCuentaController (args) {
        var ctx = this;
        var thelper = EdoCtaTabla(settings);
        var Tercero = oor.mm('Tercero');


        ctx.tercero = m.prop({});
        ctx.contacto = m.prop({});
        ctx.monedas = m.prop();
        ctx.operaciones = m.prop();
        ctx.settings = m.prop(settings);
        ctx.cuentasBancarias = m.prop();
        ctx.dias = m.prop(30);

        ctx.totalAnual = Modelo.numberProp(m.prop(0),2);

        ctx.saldoTotal = Modelo.numberProp(m.prop(0),2);
        ctx.saldoVencido = Modelo.numberProp(m.prop(0),2);
        ctx.saldoDias = Modelo.numberProp(m.prop(0),2);

        ctx.tipoFecha = m.prop( seleccionarTipoFecha.tipoDefault );

        ctx.contexto = m.prop(settings.cuentasPor);


        console.log( ctx.contexto() )
    
        var selectedTab = m.prop( tabs()[0] );


        ctx.tab = function () {
            if(arguments.length) {
                selectedTab(arguments[0])
                thelper.query().add(selectedTab().query).exec()
            }

            return selectedTab();
        }


        ctx.setTipoFecha = function (tf) {
            ctx.tipoFecha(tf)
            actualizarValores();
        }


        ctx.cargarCuentasBancarias = function () {
            if(ctx.cuentasBancarias()) return;

            m.request({
                url : '/apiv2?modelo=cuentas&es_bancos=1&acumulativa=0',
                method : 'GET'
            }).then(function (r) {
                ctx.cuentasBancarias(r.data);
            })
        }


        ctx.setupTable = function (element, isInited) {
            if(isInited) return;

            thelper.element(element);
            ctx.tab(selectedTab());

            if(!settings.vms){
                settings.vms = {}
            }


            $(element).on('click', '[x-edit-enable]', function () {
                var el = $(this);
                var id = el.attr('x-edit-id');
                var prop = el.attr('x-edit-prop');
                var val = el.attr('x-edit-value');
                var selector = 'input[x-edit-prop="' +prop+ '"][x-edit-id="' + id +'"]';
                var input = $(selector);

                var cell = $('[x-row="' + id + '"] [x-cell="' + prop +'"]');

                cell.addClass('edit-mode');

                if(! settings.vms[id] ) {
                    settings.vms[id] = {};
                }

                settings.vms[id][prop] = m.prop(val);
                input.val(val);
            });

            $(element).on('change', 'input[x-edit-trigger]', function () {
                var dataSingnature = {};
                var el = $(this);
                var id = el.attr('x-edit-id');
                var prop =el.attr('x-edit-prop');
                var cell = $('[x-row="' + id + '"] [x-cell="' + prop +'"]');

                console.log(cell);

                cell.removeClass('edit-mode');

                if(! el.val()) return;

                dataSingnature.operacion_id = id;

                dataSingnature[prop] = el.val();

                m.request({
                    url : '/operaciones/fechaesperada', 
                    method : 'PUT', 
                    data : {
                        operaciones : [dataSingnature]
                    },
                    unwrapSuccess : f('data')
                })
                .then(actualizarOperaciones)
                
            })
        } 


        ctx.actualizarDias = function (dias) {
            ctx.dias(Number(dias));
            actualizarValores();
        }


        cargarDatos();


        function actualizarOperaciones (data) {

            data.operaciones.forEach(function (d) {
                    var operacionID = f('operacion_id')(d);

                    var operacion = ctx.operaciones().filter(function (d) {
                        return f('operacion_id')(d) == operacionID
                    })[0];

                    if(operacion) {
                        operacion.fecha_esperada = f('fecha_esperada')(d);
                    } else {
                        console.log('nop')
                    }
                });
            
            toastr.success('Cambios Efectuados')
            actualizarValores()
            thelper.draw()
        }


        function cargarDatos () {
            Tercero.include = m.prop('');

            Tercero.get(args.tercero_id)
                .then(ctx.tercero)
                .then(cargarContacto)
                .then(cargarMonedas)
                .then(cargarReporte)
        }


        function cargarContacto () {
            var idContacto = ctx.tercero().contacto_principal_id();
            
            if(idContacto) {
                return Tercero.get(ctx.tercero().contacto_principal_id())
                    .then(ctx.contacto)
            } else {
                ctx.contacto( new Tercero );
            }
        }

        function cargarMonedas () {
            return m.request({
                method : 'GET',
                url : '/reportes/monedas',
                unwrapSuccess : f('data')
            }).then(ctx.monedas)
        }


        function cargarReporte () {
            return m.request({
                url : '/reportes/tercero',
                data : {
                    tercero_id : ctx.tercero().tercero_id(),
                    reporte : settings.reporte
                },
                unwrapSuccess : f('data')
            })
            .then(setDataReporte)
        }


        function setDataReporte (data) {
            ctx.operaciones(data.documentos)
            ctx.operaciones().forEach(thelper.collection.insert)
            actualizarValores()    
        }

        function actualizarValores () {
            var decimales = 2;

            revaluarMonedas(ctx.operaciones(), ctx.monedas() , {
                'saldo_m_base' : 'saldo',
                'total_m_base' : 'total'
            }, decimales);

            var now = new Date;
            var today = oor.fecha.toSQL(now);
            now.setDate( now.getDate() + ctx.dias() );
            var future = oor.fecha.toSQL(now);

            ctx.operaciones().forEach(function (item) {
                var tipoOp = f('tipo_operacion')(item);
                item.saldoRelativo = Modelo.numberProp(m.prop(), decimales);
                item.saldoRelativo.$int( item.saldo_m_base.$int() *  settings.factores[tipoOp]);
            })


            var valores = sumarValores(ctx.operaciones(), {
                
                sumatoria : function (item) {
                    var tipoOp = f('tipo_operacion')(item);
                    return settings.factores[tipoOp] * item.saldo_m_base.$int()
                },

                anticipos : function (item) {
                    if(f('tipo_operacion')(item) == settings.tipo_pago) {
                        return item.saldo_m_base.$int();
                    }
                },

                devoluciones : function (item) {
                    if(f('tipo_operacion')(item) == settings.tipo_devolucion) {
                        return item.saldo_m_base.$int();
                    }
                },

                saldoVencido : function (item) {
                    var tipoOp = f('tipo_operacion')(item);
                    var getter = f('fecha_vencimiento');

                    if(tipoOp == settings.tipo_documento) {
                        if(getter(item) < today) {
                            return item.saldo_m_base.$int();
                        }
                    }

                    return 0;
                },

                saldoDias : function (item) {
                    var tipoOp = f('tipo_operacion')(item);
                    var getter = f(ctx.tipoFecha());

                    if(tipoOp == settings.tipo_documento) {
                        if(getter(item) < future) {
                            return item.saldo_m_base.$int();
                        }
                    }
                    return 0;
                }
            });
    
            ctx.saldoTotal.$int( valores.sumatoria );
            ctx.saldoVencido.$int( valores.saldoVencido - valores.anticipos - valores.devoluciones);
            ctx.saldoDias.$int( valores.saldoDias - valores.anticipos - valores.devoluciones);
        }
    }
}



function View (ctx) {
    var tercero_id = ctx.tercero().tercero_id();

    var notas = m.component(NotasComponent, {
        pertenece_a_id : ctx.tercero().tercero_id,
        contexto : ctx.contexto
    });

    return m('div.row', [
        m('.col-md-6', [
            m('h2', {style:'margin-bottom:4px'}, ctx.tercero().nombre()),
            
            ctx.contacto() ? [
                m('span', ctx.contacto().nombre()), 
                ' ',
                m('span', ctx.contacto().email())
            ] : '',

            m('h5', 'Total del Año: ', ctx.totalAnual.number()),

            m('h4', {style:'margin:0px'}, 'Por ', ctx.settings().cuentasPor, ' : ', ctx.saldoTotal.number()),
            m('h5.text-red', 'Vencido: ', ctx.saldoVencido.number()),
            m('h5', {style:'margin:0px'}, [
                'Por ' + ctx.settings().cuentasPor + ' en ',
                m('input[type="number"]', {
                    style:"width:4em",
                    value : ctx.dias(),
                    onchange : m.withAttr('value', ctx.actualizarDias)
                }),
                ' días: ',
                ctx.saldoDias.number()
            ]),

            seleccionarTipoFecha(ctx)
        ]),

        m('.col-md-6', [
            notas,

            m('.btn-group.dropdown.pull-right', [
                m('btn.btn.btn-default.pull-right.dropdown-toggle[data-toggle="dropdown"]', {
                    onclick : ctx.cargarCuentasBancarias
                }, ctx.settings().pago, ' ', m('span.caret')),

                m('ul.dropdown-menu.dropdown-menu-right', [

                    ctx.cuentasBancarias() ? ctx.cuentasBancarias().map(function (cta) {
                        var href = '/operaciones/' + ctx.settings().tipo_pago + '/crear';
                        href += '?tercero_id=' + tercero_id;
                        href += '&cuenta=' + cta.cuenta_contable_id; 

                        return m('li', [
                            m('a', {href : href}, cta.nombre)
                        ])
                    }) : m('li', m('i.fa.fa-spin.fa-spinner'))

                ])

            ]),

            m('button.btn.btn-default.pull-right', {
                onclick : ctx.abrirRecordatorio
            }, 'Recordatorio'),

            m('a.btn.btn-default.pull-right', {
                href : '/operaciones/' + ctx.settings().tipo_documento + '/crear?tercero_id='+ tercero_id 
            }, ctx.settings().documento) 

        ]),

        m('.row', []),

        m('br'),
        m('br'),
        m('.table-responsive', [
            m('table.table', {config : ctx.setupTable})
        ]),
        MTModal.view()
    ])
}