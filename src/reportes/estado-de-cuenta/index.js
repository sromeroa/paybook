var EstadoDeCuenta = require('./estadoDeCuenta');

module.exports = {
    cliente : EstadoDeCuenta({
        reporte : 'cliente',
        factores : {VTA:1, VNC:-1, IDC:-1},
        cuentasPor : 'cobrar',
        documento : 'Factura',
        tipo_documento : 'VTA',
        pago : 'Ingreso',

        tipo_pago : 'IDC',
        tipo_devolucion : 'VNC'
    }),

    proveedor : EstadoDeCuenta({
        reporte : 'proveedor',
        factores : {COM:1, CNC:-1, EPP:-1},
        cuentasPor : 'pagar',
        documento : 'Compra',
        tipo_documento : 'COM',
        pago : 'Egreso',

        tipo_pago : 'EPP',
        tipo_devolucion : 'CNC'
    })
} 