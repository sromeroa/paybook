/**
 *	Creamos nuestro Modelo
 */
var cxc = module.exports = {};

var FechasBar = require('../misc/components/FechasBar.js');

/**
 * Creamos una lista vacia de la lista de ventas
 * @type Array
 */
cxc.lista = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 * @param  array data Informacion de los movimientos
 * @return void
 */
cxc.movimiento = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    this.cuenta = m.prop(data.cuenta);
    this.nombre = m.prop(data.nombre);
    this.descripcion = m.prop(data.descripcion);
    this.tipo_operacion = m.prop(data.tipo_operacion);
    this.fecha = m.prop(data.fecha);
    this.fecha_vencimiento = m.prop(data.fecha_vencimiento);
    this.referencia = m.prop(data.referencia);
    this.producto_nombre = m.prop(data.producto_nombre);
    this.importe_m_base = m.prop(data.importe_m_base);
    this.dias = m.prop(data.dias);
    this.proveedor = m.prop(data.proveedor);
};

/**
 * View-Model interaccion
 * @return objeto
 */
cxc.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function(fecha_inicial, fecha_final) {
        //a running list of todos
        vm.lista = new cxc.lista();
        this.getMovimiento(fecha_inicial, fecha_final);
    };

    vm.getMovimiento = function(fecha_inicial, fecha_final){
       m.request({
            url : '/api/reportes/cxp',
            method : 'POST',
            data : {
                'fechai':fecha_inicial,
                'fechaf':fecha_final
            }
        }).then(function (r) {
            if(r.data != 'No hay registros para mostrar.'){
                r.data.map(function (data) {
                    vm.lista.push(new cxc.movimiento(data));
                });
            }
        });
    };
    return vm;
}())

cxc.controller = function()  {
    cxc.vm.init(null, null);
};

cxc.fechasBar = new FechasBar.controller({
    onchange : function (val) {
        var fecha_inicial = val.fecha.$gte;
        var fecha_final = val.fecha.$lte;
        if(fecha_inicial && fecha_final){
            cxc.vm.init(fecha_inicial, fecha_final);
        }
    }
})

cxc.view = function(ctrl) {
    return m('div', [
        m('h3.column-title', 'Cuentas por Pagar'),
        m('div.highlight.grey', [
            m('div.row', [
                m('div.col-sm-6', FechasBar.view( cxc.fechasBar))
            ])
        ]),
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead', 
                    m('tr',[
                        m('th','Cuenta'),
                        m('th','Nombre'),
                        m('th','Tipo de Operacion'),
                        m('th','Fecha'),
                        m('th','Fecha de Vencimiento'),
                        m('th','Referencia'),
                        m('th','Nombre Producto'),
                        m('th','Importe'),
                        m('th','Dias'),
                        m('th','Proveedor'),
                    ])
                ),
                m('tbody',[
                    cxc.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.cuenta()),
                            m('td',data.nombre()),
                            m('td',data.tipo_operacion()),
                            m('td',oor.fecha.format(data.fecha())),
                            m('td',oor.fecha.format(data.fecha_vencimiento())),
                            m('td',data.referencia()),
                            m('td',data.producto_nombre()),
                            m('td',data.importe_m_base()),
                            m('td',data.dias()),
                            m('td',data.proveedor()),
                        ]);
                    })
                ])
            ])
        ])
    ]);
};
