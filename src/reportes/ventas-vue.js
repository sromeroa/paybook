$(document).ready(function () {
    var selector = "";
    if (window.location.pathname.indexOf("reportes/ventas") > -1) selector = ".reporte-ventas";
    if (window.location.pathname.indexOf("reportes/compras") > -1) selector = ".reporte-compras";

    if (selector) {
        // Necesitamos cambiar los delimitadores a corchetes ( '[' y ']' ) porque hay conflitos con angular usando llaves ('{' '}')
        Vue.config.delimiters = ['[[', ']]'];
        var vm = new Vue({
            // We're defining the app as the entire body
            el: 'body',

            // This is the data we're using in our app
            data: {
                fechai: null,
                fechaf: null,
                filtro_fecha: "personalizado",

                datos: [],
                clientes: [],
                vendedores: [],
                sucursal: [],
                productos: [],

                cliente_total_ventas: 0,
                cliente_total_cantidad_ventas: 0,
                cliente_total_nota_credito: 0,
                cliente_total_cantidad_nota_credito: 0,
                cliente_total_totales: 0,
                cliente_total_saldo: 0,

                vendedor_total_ventas: 0,
                vendedor_total_cantidad_ventas: 0,
                vendedor_total_nota_credito: 0,
                vendedor_total_cantidad_nota_credito: 0,
                vendedor_total_totales: 0,
                vendedor_total_saldo: 0,

                sucursal_total_ventas: 0,
                sucursal_total_cantidad_ventas: 0,
                sucursal_total_nota_credito: 0,
                sucursal_total_cantidad_nota_credito: 0,
                sucursal_total_totales: 0,
                sucursal_total_saldo: 0,

                producto_total_ventas: 0,
                producto_total_cantidad_ventas: 0,
                producto_total_nota_credito: 0,
                producto_total_cantidad_nota_credito: 0,
                producto_total_totales: 0,
                producto_total_saldo: 0,

                clientes_cantidad: 0,
                vendedor_cantidad: 0,
                sucursal_cantidad: 0,
                productos_cantidad: 0,

                buscador: '',

                tab_cliente: true,
                tab_vendedor: false,
                tab_sucursal: false,
                tab_productos: false,
                current_tab: 'tercero',

                tipo_reporte: '',

                sucursales: '',
                sucursales_options: [],

                monedas: '',
                monedas_options: [],

                vendedor_seleccionado: '',
                vendedores_options: [],

                categorias_clientes: '',
                categorias_clientes_options: [],

                categorias_productos: '',
                categorias_productos_options: [],

                sortKey: 'nombre',
                reverse: 1,

                arrow_direction: ''
            },
            created: function () {
                var url = window.location.pathname;
                if (url.indexOf("reportes/ventas") !== -1) {
                    this.tipo_reporte = 'ventas'
                } else if (url.indexOf("reportes/compras") !== -1) {
                    this.tipo_reporte = 'compras'
                }

            },
            computed: {
                urlExportar: function() {
                    return "/exportar/ventasCompras?tipo=" + this.tipo_reporte +"&filtro=" + this.current_tab +"&"+ this.crearParametros() +"";
                }
            },
            watch: {
                'fechai': function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                'fechaf': function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "sucursales": function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "monedas": function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "vendedor_seleccionado": function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "categorias_clientes": function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "categorias_productos": function(valor) {
                    this.cambiarURL(this.crearParametros());
                },
                "buscador": function(valor) {
                    this.cambiarURL(this.crearParametros());
                }
            },
            filters: {
                money: function (valor) {
                    return oorden.organizacion.format(valor);
                },

                link: function (valor, id, esNotaCredito, esSaldo) {
                    var parametro = 'tTerceroId';

                    if (this.current_tab == 'vendedor') {
                        parametro = 'opVendedorId';
                    }

                    if (this.current_tab == 'sucursal') {
                        parametro = 'opSucursalId';
                    }

                    if (!id) {
                        id = 'ISNULL'
                    }

                    var url = "/operaciones/ventas?" + parametro + '=' + id + '&fechaDesde=' + this.fechai + '&fechaHasta=' + this.fechaf + "&opMoneda=" + this.monedas;

                    if (this.tipo_reporte == "ventas") {
                        if (esNotaCredito) {
                            url += '&opTipoOperacion=VNC';
                        } else {
                            url += '&opTipoOperacion=VTA'
                        }
                    } else if (this.tipo_reporte == "compras") {
                        if (esNotaCredito) {
                            url += '&opTipoOperacion=CNC';
                        } else {
                            url += '&opTipoOperacion=COM'
                        }
                    }

                    if (esSaldo) {
                        url = url.replace(/&opTipoOperacion[^&]+/, "");
                    }

                    return url;
                },

                saldo: function (valor, id) {
                    if (!id) {
                        id = 'ISNULL'
                    }

                    return "/reportes/cliente?tercero_id=" + id + '&fechaDesde=' + this.fechai + '&fechaHasta=' + this.fechaf + "&opMoneda=" + this.monedas;
                }
            },
            ready: function () {


                // Necesitamos verificar que el objecto oorden ha sido creado
                var interval = setInterval(function () {
                    if (typeof oorden !== typeof undefined && typeof oorden.organizacion !== typeof undefined) {
                        oorden();
                        clearInterval(interval);

                        var parametros = this.filtrosParametros();
                        if (Object.keys(parametros).length) {
                            this.filtros(parametros);
                        } else {
                            this.ultimoUso();
                        }
                        this.sortBy();
                        this.cargarSucursales();
                        this.cargarMonedas();
                        this.cargarVendedores();
                        this.cargarCategoriasClientes();
                        this.cargarCategoriasProductos();
                    }
                }.bind(this), 500);

            },
            methods: {
                toggleArrow: function() {
                    this.arrow_direction = this.reverse == -1 ? "down" : "up";
                }
                ,
                sortBy: function(key) {
                    if (this.sortKey == key) {
                        this.reverse = this.reverse == -1 ? 1 : -1;
                    }

                    this.sortKey = key;
                    this.toggleArrow();
                },
                cambiarURL: function(parametros){
                    window.history.replaceState("", "", "?" + parametros);
                },
                filtrosParametros: function() {
                    var parametros = this.obtenerParametros();

                    $.each(parametros, function(key, value){
                        if (value === "" || value === null){
                            delete parametros[key];
                        }
                    });

                    return parametros;

                },
                filtros: function(listaFiltros) {
                    if (listaFiltros.fechai) {
                        this.fechai = listaFiltros.fechai;
                    }

                    if (listaFiltros.fechaf) {
                        this.fechaf = listaFiltros.fechaf;
                    }

                    if (listaFiltros.sucursal_id) {
                        this.sucursales = listaFiltros.sucursal_id;
                    }

                    if (listaFiltros.vendedor_id) {
                        this.vendedor_seleccionado = listaFiltros.vendedor_id;
                    }

                    if (listaFiltros.tercero_categoria_id) {
                        this.categorias_clientes = listaFiltros.tercero_categoria_id;
                    }

                    if (listaFiltros.categoria_id) {
                        this.categorias_productos = listaFiltros.tercero_categoria_id;
                    }

                    if (listaFiltros.moneda) {
                        this.monedas = listaFiltros.moneda;
                    }

                    if (listaFiltros.buscador) {
                        this.buscador = decodeURIComponent(listaFiltros.buscador).replace(/\+/g, " ");
                    }

                  /*  if (listaFiltros.filtro_fecha) {
                        this.filtro_fecha = listaFiltros.filtro_fecha;
                    }

                    if (listaFiltros.fechai == null && listaFiltros.fechaf == null) {
                        this.filtro_fecha = "personalizado";
                    }*/

                    this.cargarReporte();

                },
                ultimoUso : function() {
                    oorden();
                    oorden.organizacion();

                    var interval = setInterval(function () {
                        if (typeof oorden.organizacion().organizacion_id !== typeof undefined && typeof oorden.organizacion().usuario_id !== typeof undefined) {
                            clearInterval(interval);
                            var url = "C_" +oorden.organizacion().organizacion_id + "_" + oorden.organizacion().usuario_id;

                            var local = this;
                            $.ajax({
                                "method": "GET",
                                'processData': false,
                                'crossDomain': true,
                                'contentType': 'application/json',
                                "url": "/api/last-used/" + url,
                                "success": function (filtros) {
                                    filtros = filtros.data;
                                    local.filtros(filtros);
                                }
                            })
                        }
                    }.bind(this), 500);
                },
                cargarReporte: function () {
                    // console.info(this.tipo_reporte);
                    var local = this;
                    $.ajax({
                        "method": "POST",
                        "data": this.crearFiltros(),
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/api/reportes/" + this.tipo_reporte,
                        "beforeSend": function () {
                            $(".tabla-partidas").hide();
                            $(".resultados-reportes").append("<div class='text-center loading-icon'><li class='fa fa-spin fa-spinner'></li></div>");
                        },
                        "success": function (respuesta) {
                            local.clientes = respuesta.data.tercero;
                            local.vendedor = respuesta.data.vendedor;
                            local.sucursal = respuesta.data.sucursal;
                            local.productos = respuesta.data.productos;

                            local.clientes_cantidad = respuesta.data.tercero ? Object.keys(respuesta.data.tercero).length : 0;
                            local.vendedor_cantidad = respuesta.data.vendedor ? Object.keys(respuesta.data.vendedor).length : 0;
                            local.sucursal_cantidad = respuesta.data.sucursal ? Object.keys(respuesta.data.sucursal).length : 0;
                            local.productos_cantidad = respuesta.data.productos ? Object.keys(respuesta.data.productos).length : 0;


                            // $("#tercero").click();
                            $(".loading-icon").remove();

                            $(".tabla-partidas").show();
                            local.calcularTotales(respuesta.data);

                            // Actualiza el tab que se está visualizando cuando los datos cambian
                            $("#" + this.current_tab).click();
                        }
                    }).done(function(data){
                        local.sortKey = 'nombre';

                    });
                },
                calcularTotales: function (respuesta) {

                    this.cliente_total_ventas = 0;
                    this.cliente_total_cantidad_ventas = 0;
                    this.cliente_total_nota_credito = 0;
                    this.cliente_total_cantidad_nota_credito = 0;
                    this.cliente_total_totales = 0;
                    this.cliente_total_saldo = 0;

                    this.vendedor_total_ventas = 0;
                    this.vendedor_total_cantidad_ventas = 0;
                    this.vendedor_total_nota_credito = 0;
                    this.vendedor_total_cantidad_nota_credito = 0;
                    this.vendedor_total_totales = 0;
                    this.vendedor_total_saldo = 0;

                    this.sucursal_total_ventas = 0;
                    this.sucursal_total_cantidad_ventas = 0;
                    this.sucursal_total_nota_credito = 0;
                    this.sucursal_total_cantidad_nota_credito = 0;
                    this.sucursal_total_totales = 0;
                    this.sucursal_total_saldo = 0;

                    this.producto_total_ventas = 0;
                    this.producto_total_cantidad_ventas = 0;
                    this.producto_total_nota_credito = 0;
                    this.producto_total_cantidad_nota_credito = 0;
                    this.producto_total_totales = 0;
                    this.producto_total_saldo = 0;


                    for (var tercero in respuesta.tercero) {
                        this.cliente_total_ventas += respuesta.tercero[tercero].venta_compra;
                        this.cliente_total_cantidad_ventas += respuesta.tercero[tercero].cantidad_venta_compra;
                        this.cliente_total_nota_credito += respuesta.tercero[tercero].notas_credito;
                        this.cliente_total_cantidad_nota_credito += respuesta.tercero[tercero].cantidad_nota;
                        this.cliente_total_totales += respuesta.tercero[tercero].venta_compra - respuesta.tercero[tercero].notas_credito;
                        this.cliente_total_saldo += respuesta.tercero[tercero].saldo;
                        this.clientes[tercero].total = this.clientes[tercero].venta_compra - this.clientes[tercero].notas_credito;
                        this.clientes[tercero].nombre = this.clientes[tercero].nombre.trim();
                    }

                    for (var vendedorIndex in respuesta.vendedor) {
                        this.vendedor_total_ventas += respuesta.vendedor[vendedorIndex].venta_compra;
                        this.vendedor_total_cantidad_ventas += respuesta.vendedor[vendedorIndex].cantidad_venta_compra;
                        this.vendedor_total_nota_credito += respuesta.vendedor[vendedorIndex].notas_credito;
                        this.vendedor_total_cantidad_nota_credito += respuesta.vendedor[vendedorIndex].cantidad_nota;
                        this.vendedor_total_totales += respuesta.vendedor[vendedorIndex].venta_compra - respuesta.vendedor[vendedorIndex].notas_credito;
                        this.vendedor_total_saldo += respuesta.vendedor[vendedorIndex].saldo;
                        this.vendedor[vendedorIndex].total = this.vendedor[vendedorIndex].venta_compra - this.vendedor[vendedorIndex].notas_credito;
                        this.vendedor[vendedorIndex].nombre = this.vendedor[vendedorIndex].nombre.trim();
                    }

                    for (var sucursalIndex in respuesta.sucursal) {
                        this.sucursal_total_ventas += respuesta.sucursal[sucursalIndex].venta_compra;
                        this.sucursal_total_cantidad_ventas += respuesta.sucursal[sucursalIndex].cantidad_venta_compra;
                        this.sucursal_total_nota_credito += respuesta.sucursal[sucursalIndex].notas_credito;
                        this.sucursal_total_cantidad_nota_credito += respuesta.sucursal[sucursalIndex].cantidad_nota;
                        this.sucursal_total_totales += respuesta.sucursal[sucursalIndex].venta_compra - respuesta.sucursal[sucursalIndex].notas_credito;
                        this.sucursal_total_saldo += respuesta.sucursal[sucursalIndex].saldo;
                        this.sucursal[sucursalIndex].total = this.sucursal[sucursalIndex].venta_compra - this.sucursal[sucursalIndex].notas_credito;
                        this.sucursal[sucursalIndex].nombre = this.sucursal[sucursalIndex].nombre.trim();
                    }

                    for (var productoIndex in respuesta.productos) {
                        this.producto_total_ventas += respuesta.productos[productoIndex].venta_compra;
                        this.producto_total_cantidad_ventas += respuesta.productos[productoIndex].cantidad_venta_compra;
                        this.producto_total_nota_credito += respuesta.productos[productoIndex].notas_credito;
                        this.producto_total_cantidad_nota_credito += respuesta.productos[productoIndex].cantidad_nota;
                        this.producto_total_totales += respuesta.productos[productoIndex].venta_compra - respuesta.productos[productoIndex].notas_credito;
                        this.producto_total_saldo += respuesta.productos[productoIndex].saldo;
                        this.productos[productoIndex].total = this.productos[productoIndex].venta_compra - this.productos[productoIndex].notas_credito;
                        this.productos[productoIndex].nombre = this.productos[productoIndex].nombre.trim();
                    }


                    this.cliente_total_ventas = oorden.organizacion.format(this.cliente_total_ventas);
                    this.cliente_total_nota_credito = oorden.organizacion.format(this.cliente_total_nota_credito);
                    this.cliente_total_totales = oorden.organizacion.format(this.cliente_total_totales);
                    this.cliente_total_saldo = oorden.organizacion.format(this.cliente_total_saldo);

                    this.vendedor_total_ventas = oorden.organizacion.format(this.vendedor_total_ventas);
                    this.vendedor_total_nota_credito = oorden.organizacion.format(this.vendedor_total_nota_credito);
                    this.vendedor_total_totales = oorden.organizacion.format(this.vendedor_total_totales);
                    this.vendedor_total_saldo = oorden.organizacion.format(this.vendedor_total_saldo);

                    this.sucursal_total_ventas = oorden.organizacion.format(this.sucursal_total_ventas);
                    this.sucursal_total_nota_credito = oorden.organizacion.format(this.sucursal_total_nota_credito);
                    this.sucursal_total_totales = oorden.organizacion.format(this.sucursal_total_totales);
                    this.sucursal_total_saldo = oorden.organizacion.format(this.sucursal_total_saldo);

                    this.producto_total_ventas = oorden.organizacion.format(this.producto_total_ventas);
                    this.producto_total_nota_credito = oorden.organizacion.format(this.producto_total_nota_credito);
                    this.producto_total_totales = oorden.organizacion.format(this.producto_total_totales);
                    this.producto_total_saldo = oorden.organizacion.format(this.producto_total_saldo);
                    $("#" + this.current_tab).click();

                },
                cambiarRangoFecha: function () {
                    switch (this.filtro_fecha) {

                        case "esteAno":
                            fechaInicio = moment().format('YYYY') + "-01-01";
                            fechaFinal = moment().format('YYYY') + "-12-31";
                            break;

                        case "esteMes":
                            fechaInicio = moment().format('YYYY-MM') + "-01";
                            fechaFinal = moment().endOf('month').format('YYYY-MM-DD');
                            break;

                        case "personalizado":
                            fechaInicio = this.fechai;
                            fechaFinal = this.fechaf;
                            break;

                        case "ultimos15":
                            fechaInicio = moment().subtract(15, "days").format('YYYY-MM-DD');
                            fechaFinal = moment().format('YYYY-MM-DD');
                            break;

                        case "ultimos30":
                            fechaInicio = moment().subtract(30, "days").format('YYYY-MM-DD');
                            fechaFinal = moment().format('YYYY-MM-DD');
                            break;

                        case "mesAnterior":
                            fechaFinal = moment().subtract(1, "months").endOf('month');
                            fechaInicio = fechaFinal.format("YYYY-MM") + "-01";

                            fechaFinal = fechaFinal.format("YYYY-MM-DD");
                            break;

                        case "todo":
                            fechaFinal = null;
                            fechaInicio = null;
                            break;
                    }

                    this.fechai = fechaInicio;
                    this.fechaf = fechaFinal;
                    this.cargarReporte();
                },
                mostrarData: function (tab) {

                    switch (tab) {
                        case "tercero":
                            this.tab_cliente = true;
                            this.datos = this.clientes;

                            this.tab_vendedor = false;
                            this.tab_sucursal = false;
                            this.tab_productos = false;
                            break;

                        case "vendedor":
                            this.tab_vendedor = true;
                            this.datos = this.vendedor;

                            this.tab_cliente = false;
                            this.tab_sucursal = false;
                            this.tab_productos = false;
                            break;

                        case "sucursal":
                            this.tab_sucursal = true;
                            this.datos = this.sucursal;


                            this.tab_vendedor = false;
                            this.tab_cliente = false;
                            this.tab_productos = false;

                            break;

                        case "productos":
                            this.tab_productos = true;
                            this.datos = this.productos;

                            this.tab_sucursal = false;
                            this.tab_vendedor = false;
                            this.tab_cliente = false;
                            break;
                    }

                    this.current_tab = tab;
                },

                cargarSucursales: function() {
                    var local = this;
                    $.ajax({
                        "method": "GET",
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/api/sucursales",
                        "success": function (sucursales) {
                            if (sucursales) {
                                sucursales = JSON.parse(sucursales);
                                for(var sucursal in sucursales.data) {
                                    local.sucursales_options.push({
                                       key:  sucursales.data[sucursal].sucursal_id,
                                       value: sucursales.data[sucursal].nombre
                                    });
                                }
                            }
                        }
                    })
                },

                cargarMonedas: function () {
                    var local = this;
                    $.ajax({
                        "method": "GET",
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/monedas/monedas",
                        "success": function (monedas) {
                            if (monedas) {
                                monedas = JSON.parse(monedas);
                                monedas = _.uniq(_.pluck(monedas.data, "codigo_moneda"));
                                for (var moneda in monedas) {
                                    local.monedas_options.push({key: monedas[moneda]});
                                }
                            }
                        }
                    })
                },

                cargarVendedores: function () {
                    var local = this;
                    $.ajax({
                        "method": "GET",
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/api/vendedores",
                        "success": function (vendedores) {
                            if (vendedores) {
                                vendedores = JSON.parse(vendedores);
                                for (var vendedor in vendedores.data) {
                                    local.vendedores_options.push({
                                        key: vendedores.data[vendedor].usuario_id,
                                        value: vendedores.data[vendedor].nombre
                                    });
                                }
                            }
                        }
                    })
                },

                cargarCategoriasClientes: function () {
                    var local = this;
                    $.ajax({
                        "method": "GET",
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/apiv2?modelo=terceros-categorias",
                        "success": function (categoriasCliente) {
                            if (categoriasCliente) {
                                for (var categoria in categoriasCliente.data) {
                                    local.categorias_clientes_options.push({
                                        key: categoriasCliente.data[categoria].tercero_categoria_id,
                                        value: categoriasCliente.data[categoria].categoria
                                    });
                                }
                            }
                        }
                    })
                },


                cargarCategoriasProductos: function () {
                    var local = this;
                    $.ajax({
                        "method": "GET",
                        'processData': false,
                        'crossDomain': true,
                        'contentType': 'application/json',
                        "url": "/apiv2?modelo=categorias",
                        "success": function (categoriasProducto) {
                            if (categoriasProducto) {
                                for (var categoria in categoriasProducto.data) {
                                    local.categorias_productos_options.push({
                                        key: categoriasProducto.data[categoria].categoria_id,
                                        value: categoriasProducto.data[categoria].categoria
                                    });
                                }
                            }
                        }
                    })
                },


                crearFiltros: function() {
                    return  JSON.stringify({
                        fechai: this.fechai,
                        fechaf: this.fechaf,
                        vendedor_id: this.vendedor_seleccionado,
                        sucursal_id: this.sucursales,
                        moneda: this.monedas,
                        tercero_categoria_id: this.categorias_clientes,
                        categoria_id: this.categorias_productos,
                        buscador: this.buscador,
                        filtro_fecha: this.filtro_fecha
                    });
                },

                crearParametros: function() {
                    return $.param(JSON.parse(this.crearFiltros()));
                },

                obtenerParametros: function (str) {
                    str = str || document.location.search;
                    return (!str && {}) || str.replace(/(^\?)/, '').split("&").map(function (n) {
                            return n = n.split("="), this[n[0]] = n[1], this
                        }.bind({}))[0];
                }
            }
        })
    }
});
