

    module.exports = Modelo.crear('operaciones', Operacion, {
        idKey:'operacion_id',
        keys: keys()
    }, proto());

    function Operacion (data) {
        data || (data = {});
        Modelo.registerKeys(this,Operacion.keys,data);

        this.$new = m.prop(false);

        this.total = Modelo.numberProp(this.total, 2);

        //Se convierten a Booleanos
        ['precios_con_impuestos','mostrar_descuento','mostrar_cctos','mostrar_cuentas','mostrar_retenciones','mostrar_impuestos'].forEach(
            function (k) {
                this[k]( Boolean(Number( this[k]() )) );
            }.bind(this)
        );

        this.$impuestosIncluidos = m.prop(0);

        this.$sinImpuestos       = Modelo.numberProp(m.prop(0) , 2);
        this.$totalImpuestos     = Modelo.numberProp(m.prop(0) , 2);
        this.$totalRetenciones   = Modelo.numberProp(m.prop(0) , 2);
        this.$sumaImportes       = Modelo.numberProp(m.prop(0) , 2);
        this.$residuo            = Modelo.numberProp(m.prop(0) , 2);

        this.$cuentaBanco        = m.prop(data.cuentaBanco || null);
        this.$tercero            = m.prop(null);

        this.$metodoDePago       = m.prop(null);

        this.$tipoDeCambio       = m.prop(OORDEN.tipoDeCambio[this.tipo_de_cambio()] || null );
        this.$tipoDeDocumento    = m.prop(null);

        this.$detalleImpuestos  = m.prop([]);

        this.items = (this.items() || []).map(function (item) {
            return new modelo.OperacionItem(item);
        });

    }

    function keys () {
        return [
            'operacion_id',
            'organizacion_id',
            'operacion_anterior_id',
            'sucursal_id',
            'seccion',
            'tipo_operacion',
            'tipo_documento',
            'serie',
            'numero',
            'fecha',
            'tercero_id',
            'tercero_direccion_id',
            'contacto_id',
            'direccion_envio',
            'cuenta_banco_mov',
            'fecha_vencimiento',
            'vendedor_id',
            'referencia',
            'tipo_de_cambio',
            'moneda',
            'tasa_de_cambio',

            'precios_con_impuestos',

            'mostrar_descuento',
            'mostrar_cctos',
            'mostrar_cuentas',
            'mostrar_retenciones',
            'mostrar_impuestos',

            'descuentos',
            'subtotal',
            'impuestos',
            'retenciones',
            'total',
            'estatus',
            'saldo',
            'forma_pago_id',
            'num_cheque',
            'tipo_ncf_id',
            'ncf',
            'fecha_cheque',
            'clave_banco_destino',
            'nombre_banco_destino',
            'cuenta_banco_destino',

            'created_at',
            'updated_at',

            'items'
        ];
    }

    function proto () {
        return {
            actualizar : function () {
                var self            = this;
                var subtotal        = 0; //Suma de los sin impuestos
                var tImpuestos      = 0;
                var tRetenciones    = 0;
                var total           = 0; //subtotal + impuestos - retenciones
                var sumaImportes    = 0;
                var residuo         = 0;

                var impuestos       = {};
                var retenciones     = {};


                self.items.forEach(function (item) {
                    item.$impuestosIncluidos( self.$impuestosIncluidos() );
                    item.actualizar();

                    subtotal      += item.$sinImpuestos.$int();
                    sumaImportes  += item.importe.$int();

                    Object.keys(item.$impuestos).forEach(function (impKey) {
                        if(! impuestos[impKey] ) {
                            impuestos[impKey] = 0;
                        }
                        impuestos[impKey] += item.$impuestos[impKey];
                        tImpuestos        += item.$impuestos[impKey];
                    });
                });


                self.$detalleImpuestos(
                    Object.keys(impuestos).map(function (impKey) {
                        var monto = Modelo.numberProp(m.prop(), 2);
                        monto.$int(impuestos[impKey]);
                        return {nombre:impKey , monto : monto};
                    })
                );


                self.$totalImpuestos.$int(tImpuestos);
                self.$sumaImportes.$int(sumaImportes);
                total = subtotal + tImpuestos - tRetenciones;

                if( self.$impuestosIncluidos() ) {
                    residuo =   sumaImportes - (total + tRetenciones);
                    total   = (sumaImportes - tRetenciones);
                }

                self.$residuo.$int(residuo);
                self.total.$int(total);

            },

            asignarTercero  : function (tercero) {
                this.$tercero(tercero);
                this.configurarDestinatario();
            },

            configurarDestinatario : function () {
                console.log( this.$tercero() );
            },

            asignarTipoDeDocumento : function (tipoDoc) {
                console.log(tipoDoc);
                this.tipo_documento(tipoDoc.tipo_documento_id);
                this.$tipoDeDocumento(tipoDoc);
                this.serie(tipoDoc.serie);
            },

            asignarTipoDeCambio : function (tc) {
                var self = this;

                this.$tipoDeCambio( tc );
                this.tipo_de_cambio( tc.tipo_de_cambio );
                this.moneda(tc.codigo_moneda);

                if(tc.base == 1) {
                    this.tasa_de_cambio('1.0000')
                } else {
                    m.request({
                        method:'GET',
                        url : '/api/tasa/'.concat(tc.tipo_de_cambio)
                    }).then(function (tasa) {
                        self.tasa_de_cambio( tasa.ultimaTasa.tasa_de_cambio );
                    });
                }
            },

            asignarMetodoDePago : function (mt) {
                this.$metodoDePago(mt);
                this.forma_pago_id(mt.id);
            }
        };

    }



    Operacion.form = function (params) {
        var form = {};
        var operacion = params.operacion;

        form.enabled = m.prop(true);

        form.tiposDeDocumento = params.tiposDeDocumento;

        form.settings = {
            usuario  : m.prop(params.settings.usuario || {}),
            sucursal : m.prop(params.settings.sucursal || {}),
            tercero  : m.prop({}),
        };

        form.tipoDocSelector = oor.tipoDocSelect2({
            tiposDoc : form.tiposDeDocumento,
            model : operacion.tipo_documento,
            onchange : function () {
                operacion.asignarTipoDeDocumento( form.tipoDocSelector.selected() );
            }
        });

        /**
         * Se asigna el tercero
         */
        form.tercerosSelector = oor.tercerosSelect2({
            onchange : function (tercero_id) {
                operacion.asignarTercero( form.tercerosSelector.selected() );
                oor.uSettings([tercero_id, operacion.tipo_operacion()]).then(function (s) {
                    form.settings.tercero( s[0] );
                    console.log( form.settings.tercero() );
                });
            },
            model : params.operacion.tercero_id
        });


        /**
         *
         */
        form.metodosDePago = params.oorden.metodosDePago.filter(function (mt) {
            return ['Cheque','Transferencia'].indexOf(mt.metodo) > -1;
        });


        form.metodoDePagoSelector = oor.select2({
            model : operacion.forma_pago_id,
            data : form.metodosDePago.map(function (m) {
                return {id:m.id, text:m.metodo};
            }),
            find : function (id) {
                return form.metodosDePago.filter(function (mt) {
                   return mt.id == id;
                })[0];
            },
            onchange : function (id) {
                var cta = operacion.$cuentaBanco();
                var mt = form.metodoDePagoSelector.selected();
                operacion.asignarMetodoDePago( mt );

                if(angular.lowercase(mt.metodo) == 'cheque') {
                   operacion.num_cheque(cta.numero_cheque);
                   operacion.fecha_cheque(operacion.fecha());
                   operacion.cuenta_banco_destino(null);
                   operacion.clave_banco_destino(null);
                   operacion.nombre_banco_destino(null);
                } else if (angular.lowercase(mt.metodo) === 'transferencia') {
                   operacion.num_cheque(null);
                   operacion.fecha_cheque(null);
                   operacion.cuenta_banco_destino(form.settings.tercero().cuenta_banco_destino || null );
                   operacion.clave_banco_destino( form.settings.tercero().clave_banco_destino || null  );
                   operacion.nombre_banco_destino( form.settings.tercero().nombre_banco_destino || null );
                }

            }
        });

        form.tipoDeCambioSelector = oor.select2({
            model : operacion.tipo_de_cambio,
            data : params.oorden.tiposDeCambio.map(function (t) {
                return {
                    id : t.tipo_de_cambio,
                    text : t.tipo_de_cambio + ' ' + t.nombre
                };
            }),
            find : function (tc) {
                return params.oorden.tipoDeCambio[tc];
            },
            onchange : function () {
                operacion.asignarTipoDeCambio(form.tipoDeCambioSelector.selected());
            }
        });

        form.impORet = function () {
            return operacion.mostrar_impuestos() || operacion.mostrar_retenciones();
        }

        form.celdasActivas = function () {
            var d = Number(operacion.mostrar_cctos() || 0) + Number(operacion.mostrar_descuento() || 0) + Number(operacion.mostrar_cuentas() || 0) + Number(form.impORet() || 0);
            return d;
        };

        return form;
    }

    /**
     * Agregar un item en una operación
     */
    Operacion.agregarItem = function (operacion, id) {
        var item = new modelo.OperacionItem({ operacion_item_id : id });

        item.operacion_id( operacion.operacion_id() );
        item.cantidad(1);

        operacion.items.push(item);
        return item;
    };


    Operacion.include = function () {
        return 'operaciones.tercero,operaciones.items,operaciones.cuentaBanco,operaciones.tipoDeCambio';
    };


    
