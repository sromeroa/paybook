

    module.exports = Modelo.crear('operaciones', OperacionItem, {idKey:'operacion_id', keys: keys()}, proto());

    function OperacionItem (data) {
        Modelo.registerKeys(this,OperacionItem.keys,data);

        this.importe         = Modelo.numberProp(this.importe, 2);
        this.precio_unitario = Modelo.numberProp(this.precio_unitario, 2); 
        this.cantidad        = Modelo.numberProp(this.cantidad, 4, true); 

        this.importe.fix();
        this.precio_unitario.fix();
        this.cantidad.fix();

        this.$integrable         = m.prop(false);
        this.$impuestosIncluidos = m.prop(false);
        
        this.$impuesto           = m.prop( OORDEN.impuesto(this.impuesto_conjunto_id()) || null);
        this.$retencion          = m.prop( OORDEN.retencion(this.impuesto_conjunto_id()) );
        
        this.$tasaRetencion      = m.prop(null);
        this.$tasaImpuesto       = m.prop(0);

        this.$sinImpuestos   = Modelo.numberProp(m.prop(0), 2);
        this.$montoImpuestos = Modelo.numberProp(m.prop(0), 2);

        this.$cuenta = m.prop( OORDEN.cuentaContable(this.cuenta_id()) );
        
        this.asignarImpuesto( this.$impuesto()   );
        this.asignarRetencion( this.$retencion() );
    }

    function keys () {
        return [
                'operacion_item_id',
                'operacion_id',
                'organizacion_id',
                'operacion_anterior_item_id',
                'posicion_item',
                'producto_id',
                'producto_variante_id',
                'producto_nombre',
                'cantidad',
                'cantidad_devuelta',
                'unidad',
                'precio_unitario',
                'descuento',
                'importe',
                'cuenta_id',
                'retencion_conjunto_id',
                'impuesto_conjunto_id',
                'ccto_1_id',
                'ccto_2_id',
                'monto_impuestos',
                'monto_retencion',

                'created_at',
                'updated_at',
            ];
    }
      
    function proto () {
        return {

            actualizar : function () {
                var precioUnit   = Number(this.precio_unitario.getString());
                var cantidad     = Number(this.cantidad.getString());
                var importe      = precioUnit * cantidad;

                this.importe(importe);
                this.importe.fix();

                if( this.$impuestosIncluidos()  ) {
                    this.$sinImpuestos.$int( this.importe.$int() / (1 + (this.$tasaImpuesto()))  );
                } else {
                    this.$sinImpuestos(importe);
                }

                this.$impuestos   = this.calcularImpuestos();
                this.$retenciones = this.calcularRetenciones();
            },

            calcularImpuestos : function () {
                var impuestos = {};
                var sinImpuestos = this.$sinImpuestos.$int();

                 if(this.$impuesto() && this.$impuesto().componentes) {
                    this.$impuesto().componentes.forEach(function (c) {
                        impuestos[ c.componente + ' ' + c.porcentajeTasa.toFixed(2) ]  = Math.round( sinImpuestos * c.porcentajeTasa);
                    });
                }

                return impuestos;
            },

            calcularRetenciones : function () {
                var retenciones  = {};
                var sinImpuestos = this.$sinImpuestos.$int();

                 if(this.$retencion() && this.$retencion().componentes) {
                    this.$retencion().componentes.forEach(function (c) {
                        retenciones[ c.componente + ' ' + c.porcentajeTasa.toFixed(2) ]  = Math.round( sinImpuestos * c.porcentajeTasa);
                    });
                }

                return retenciones;
            },


            asignarImpuesto : function (impuesto) {
                var tasa = 0;

                this.$impuesto(impuesto);

                this.impuesto_conjunto_id(impuesto ? impuesto.impuesto_conjunto_id : null);

                if( this.$impuesto() && this.$impuesto().componentes) {
                    this.$impuesto().componentes.forEach(function (c) {
                        tasa += Number(c.tasa);
                        c.porcentajeTasa = tasa / 100;
                    });
                }

                this.$tasaImpuesto(tasa / 100);
            },

            asignarRetencion : function (retencion) {
                var tasa = 0;
                this.$retencion(retencion);

                if( this.$retencion() && this.$retencion().componentes) {
                    this.$retencion().componentes.forEach(function (c) {
                        tasa += Number(c.tasa);
                        c.porcentajeTasa = tasa / 100;
                    });
                }

                this.$tasaRetencion(tasa / 100);
            },


            asignarCuenta : function (cuenta) {
                this.$cuenta( cuenta ); 
            }
        }
    };


    /** 
     * Este es el constructor del formulario  de OperacionItem
     * recibe en params
     *      item : instancia de OperacionItem
     *      operacion : instancia de Operacion
     *
     */

    OperacionItem.form = function (params) {
        var form    = {};
        var oorden  = params.oorden;
        var cuentas = oorden.cuentasContables.map(function (s) { return s; });
        var updateTimeout = null;
        var startedComputations = 0;
        var item = params.item;

        form.enabled = m.prop(true);
    
        form.cuentasSelector = oor.cuentasSelect2({
            cuentas : cuentas,
            model : params.item.cuenta_id,
            onchange : function (v) {
                item.asignarCuenta( form.cuentasSelector.selected() );
            }
        });

        form.impuestosSelector = oor.impuestosSelect2({
            impuestos : oorden.impuestos,
            model : params.item.impuesto_conjunto_id,
            onchange : function (v) {
                params.item.asignarImpuesto( form.impuestosSelector.selected() );
                console.log('impuestos');
                actualizarOperacion();
            }
        });

        form.retencionesSelector = oor.retencionesSelect2({
            retenciones :oorden.retenciones,
            model : params.item.retencion_conjunto_id,
            onchange : function (v) {
                params.item.asignarRetencion( form.retencionesSelector.selected() );
                actualizarOperacion();
            }
        });


        form.cctosSelectors = params.centrosDeCosto.map(function (ccto, idx) {
            var name = 'ccto_'.concat(idx + 1);
            return oor.cctoSelect2({
                centroDeCosto : ccto,
                model : params.item[name.concat('_id')]
            });
        });

        form.inputImpuestos = function () {
             return [
                m('div', [
                    m('div', {style:form.enabled() ? 'display:none' : ''}, params.item.$impuesto() ? params.item.$impuesto().impuesto_conjunto : '(ninguno)'),
                    m('div', {style:form.enabled() ? '' : 'display:none'} , form.impuestosSelector.view())
                ])
            ];
        }

        form.inputCuentas = function () {
            return [
                m('div', [
                    m('div.cuenta-display.detalle', {style:form.enabled() ? 'display:none' : ''}, [
                        m('div.cuenta-cuenta', item.$cuenta() ? item.$cuenta().cuenta : '---'),
                        m('div.cuenta-nombre', item.$cuenta() ? item.$cuenta().nombre : '(no hay cuenta)')
                    ]),
                    m('div', {style:form.enabled() ? '' : 'display:none'} , form.cuentasSelector.view())
                ])
            ];
        };

        form.inputRetenciones = function () {
            return [
                m('div', [
                    m('div', {style:form.enabled() ? 'display:none' : ''}, params.item.$retencion() ? params.item.$retencion().retencion_conjunto : '(ninguna)'),
                    m('div', {style:form.enabled() ? '' : 'display:none'} , form.retencionesSelector.view())
                ])
            ];
        };


        form.inputConcepto = function () {
            return m('input[type="text"]'.concat(form.enabled() ? '' : '[disabled]') , {
                placeholder : 'Concepto', 
                value : params.item.producto_nombre(), 
                onchange : m.withAttr('value', function (v) {
                    params.item.producto_nombre(v);
                    params.operacion.actualizar();
                }),
                size : 25
            });
        };

        form.inputCantidad = function () {
            return m('input.text-right[type="text"]'.concat(form.enabled() ? '' : '[disabled]'), {
                placeholder : 'Cant.', 
                value : params.item.cantidad(), 
                onchange : m.withAttr('value', function (v) {
                    params.item.cantidad.fix(v);
                    params.operacion.actualizar();
                }),
                size : 12
            });
        };

        form.inputUnidad = function () {
            return m('input.text-left[type="text"]'.concat(form.enabled() ? '' : '[disabled]'), {
                placeholder :'Unid.', 
                value : params.item.unidad(), 
                onkeyup : m.withAttr('value', params.item.unidad),
                size : 12
            });
        };

        form.inputPrecioUnitario = function () {
            return m('input.text-right[type="text"]'.concat(form.enabled() ? '' : '[disabled]'), {
                placeholder : 'Precio unit.', 
                value : params.item.precio_unitario(), 
                onchange:m.withAttr('value', function (v) {
                    console.log(v);
                    params.item.precio_unitario.fix(v);
                    params.operacion.actualizar();
                }),
                size : 14
            });
        };

        form.inputCcto1  = function () {
            return form.cctosSelectors[0] ? form.cctosSelectors[0].view() : '';
        };

        form.inputCcto2  = function () {
            return form.cctosSelectors[1] ? form.cctosSelectors[1].view() : '';
        };

        function actualizarOperacion () {
            clearTimeout(updateTimeout);
            startedComputations++;
            m.startComputation();
            
            updateTimeout = setTimeout(function () {
                params.operacion.actualizar();
                for(; startedComputations>0; startedComputations--) m.endComputation();
            }, 250);  
        }

        return form;
    }
