
function AplicacionDePago (data) {
    AplicacionDePago.initializeInstance.call(this,data);

    this.monto_pago     = Modelo.numberProp(this.monto_pago,2);
    this.monto_pagado   = Modelo.numberProp(this.monto_pagado,2);
    this.tasa_de_cambio = Modelo.numberProp(this.tasa_de_cambio,6);
};

oor.mm('AplicacionDePago', AplicacionDePago)
    .prop('aplicacion_id',{})
    .prop('aplicacion_a_doc_id', {})
    .prop('operacion_pago_id', {}) 
    .prop('monto_pago', {})
    .prop('tipo_de_cambio', {})
    .prop('tasa_de_cambio',{})
    .prop('documento_pagado_id', {})
    .prop('monto_pagado', {})