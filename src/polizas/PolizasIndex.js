var PolizasIndex = module.exports = {};
var FechasMultiSelector = require('../misc/components/FechasMultiSelector.js');
var dynTable = require('../nahui/dtable');
var TableHelper = require('../nahui/dtableHelper');
var nahuiCollection = require('../nahui/nahui.collection');

var SearchBar = require('../misc/components/SearchBar');
var FechasBar = require('../misc/components/FechasBar');
var tableCheckers = require('../misc/utils/tableCheckers');

PolizasIndex.tabs = m.prop([
    {
        caption : 'Todas',
        badge : false,
        query : {estatus : undefined}
    },
    {
        caption : 'En Preparacion',
        badge : 'P',
        query : {estatus : 'P'}
    },
    {
        caption : 'Por Autorizar',
        badge : 'T',
        query : {estatus : 'T'}
    },
    {
        caption :'Aplicadas',
        badge : 'A',
        query : {estatus : 'A'}
    },
    {
        caption : 'Canceladas',
        badge : 'X',
        query : {estatus : 'X'}
    }
]);

PolizasIndex.controller = function (args) {
    var ctx = this;
    var Poliza = oor.mm('Poliza');
    var fields =  Poliza.fields();
    var thelper, table, coll, query, searchBar, fechasBar, params;

    ctx.seccion = m.prop(args.seccion);

    table = dynTable().key( f('poliza_id') );

    coll = nahuiCollection('polizas', {identifier : 'poliza_id'} );

    query = coll.query({});


    var seleccionados = {};
    
    checkers = tableCheckers({
        name : 'checker',
        value : function (d) {
            return seleccionados[f('poliza_id')(d)];
        },
        change : function (item, value) {
            if(value) {
                seleccionados[f('poliza_id')(item)] = true;
            } else {
                seleccionados[f('poliza_id')(item)] = false;
            }
        },
        id : f('poliza_id')
    });

    ctx.columnas =  m.prop([
        checkers,
        fields.fechaYPeriodo,
        fields.Poliza,
        fields.totalDebe,
        fields.totalHaber,
        fields.estatus
    ]);


    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 10 })


    ctx.search = m.prop('');

    searchBar = new SearchBar.controller({
        search : ctx.search,
        onsearch : function () {
            var search = ctx.search() ? {$contains:ctx.search()} : undefined;
            query.add({ search: search }).exec();
        }
    });


    fechasBar = new FechasBar.controller({
        onchange : function (val) {
            query.add({ fecha : val.fecha }).exec();
        }
    })

    ctx.searchBar = searchBar;
    ctx.fechasBar = fechasBar;

    ctx.setupTable = function (element, isInitialized) {
        if(!isInitialized) thelper.element(element);
    }

    ctx.tab = m.prop();

    ctx.selectTab = function (tab, update) {
        ctx.tab(tab);
        query.add(tab.query);

        if(update === false) return;
        query.exec();
    }


    ctx.selectTab(PolizasIndex.tabs()[0], false);

    loadItems();

    function loadItems () {
        oorden().then(function () {
            m.request({
                url:'/apiv2',
                method:'GET',
                data : {
                    modelo : 'polizas'
                }
            }).then(function (r) {
                
                r.data.forEach(function (item) {
                    item.search = Poliza.search(item);
                    coll.insert(item);
                });

                query.exec();
            });
        });
    }

};


PolizasIndex.view = function (ctx) {
    return m('div', [
        m('div.row', [
            m('div.col-sm-4', SearchBar.view(ctx.searchBar) ),
            m('div.col-sm-8', FechasBar.view(ctx.fechasBar) )
        ]),
        m("div", {style:'margin-top:10px'}, 
            m('ul.nav.nav-tabs.with-panel', [
                PolizasIndex.tabs().map(function (tab) {
                    return m('li', {
                        'class' : tab == ctx.tab() ? 'active' : '',
                        onclick : ctx.selectTab.bind(ctx,tab)
                    }, [
                        m('a', {href:'javascript:;'}, [
                            tab.caption,
                            ' ',
                            tab.badge ? m('.badge.badge-sm', '') : null
                        ])
                    ])
                })
            ])
        ),
        m('.table-responsive', [
            m('table.table', {config:ctx.setupTable})
        ])
    ]);
};

