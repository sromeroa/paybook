var tablaPartidas = module.exports = {};

tablaPartidas.controller = function (args) {
    this.__proto__ = args.polizaCtrl;
};

tablaPartidas.view = function (ctrl){
    return m('table.tabla-partidas.table.bill', [
        m('thead', tablaPartidas.thead(ctrl)),
        m('tbody', tablaPartidas.tbody(ctrl)),
        m('tfoot', {style:'font-size:16px;font-weight:bold'}, tablaPartidas.tfoot(ctrl))
    ])
};

tablaPartidas.tbody = function (ctrl) {
    return ctrl.poliza().partidas().map(function (partida) {
        return m.component(PartidaComponent, {
            key : partida.poliza_partida_id(),
            partida:partida,
            partidaCtrl:ctrl
        })
    })
};


tablaPartidas.thead = function (ctrl) {
    return m('tr', [
        m('th'),
        m('th','Concepto'),
        m('th', 'Cuenta'),
        ctrl.poliza().ccto1() ? m('th', ctrl.poliza().ccto1().ccto) : '',
        ctrl.poliza().ccto2() ? m('th', ctrl.poliza().ccto2().ccto) : '',
        m('th', 'Debe'),
        m('th', 'Haber'),
        m('th')
    ])
};


tablaPartidas.tfoot = function (ctrl) {
    var mostrarDiferencia = ctrl.poliza().diferencia.$int() > 0;
    var dif = ctrl.poliza().total_debe.$int() - ctrl.poliza().total_haber.$int();
    var colspan = 3;
    if(ctrl.poliza().ccto1()) colspan++;
    if(ctrl.poliza().ccto2()) colspan++;

    return [
        m('tr.bottom-bill', [
            m('td.text-right.colored.light', {colspan:colspan}, 'Totales :'),
            m('td.text-right.colored.light', ctrl.poliza().total_debe.number()),
            m('td.text-right.colored.light', ctrl.poliza().total_haber.number()),
            m('td.colored.light')
        ]),

        m('tr.bottom-bill.text-red', [
            m('td.text-right.text-red.colored', {colspan:colspan}, mostrarDiferencia ? 'Diferencia:' : ' '),
            m('td.text-right.text-red.colored', dif > 0 ? ctrl.poliza().diferencia.number() :' ' ),
            m('td.text-right.text-red.colored', dif < 0 ? ctrl.poliza().diferencia.number() : ' '),
            m('td.colored')
        ])

    ];
}



var PartidaComponent = {};

PartidaComponent.controller = function (args) {
    var ctrl = this;

    this.__proto__ = args.partidaCtrl;

    this.partida = args.partida;

    /**
     * Cuentas
     */
    this.cuentaSelector = oor.cuentasSelect2({
        cuentas : oorden.cuentas().map(function (s) { return s}),
        model : this.partida.cuenta_id,
        onchange : this.actualizar
    });

    this.cuentaSelector.enabled = this.enabled;

    /**
     * Centros de Costo
     */
    if(this.poliza().ccto1()) {
        this.ccto1Selector = oor.cctoSelect2({
            centroDeCosto : this.poliza().ccto1(),
            model : this.partida.ccto_1_id
        });

        this.ccto1Selector.enabled = ctrl.enabled;
    }

    if(this.poliza().ccto2()) {
        this.ccto2Selector = oor.cctoSelect2({
            centroDeCosto : this.poliza().ccto2(),
            model : this.partida.ccto_2_id
        });
         this.ccto2Selector.enabled = ctrl.enabled;
    }



    this.debe = function (val) {
        if(arguments.length === 0) {
            return ctrl.partida.debe_o_haber() > 0 ? ctrl.partida.debe() : '';
        }

        ctrl.partida.debe(val);
        ctrl.partida.debe.fix();
        ctrl.partida.importe.$int( ctrl.partida.debe.$int() )

        ctrl.partida.haber(0);
        ctrl.partida.debe_o_haber(1);
        ctrl.actualizar(true);
    };

    this.haber = function (val) {
        if(arguments.length === 0) {
            return ctrl.partida.debe_o_haber() < 0 ? ctrl.partida.haber() : '';
        }

        ctrl.partida.haber(val);
        ctrl.partida.haber.fix();
        ctrl.partida.importe.$int( ctrl.partida.haber.$int() )

        ctrl.partida.debe(0);
        ctrl.partida.debe_o_haber(-1);
        ctrl.actualizar(true);
    };

    this.concepto = function (val) {
        if(arguments.length == 0) return;

        ctrl.partida.concepto(val);
        ctrl.actualizar();
    };

    this.conceptoFocus = function () {
        if(ctrl.partida.concepto()) return;
        ctrl.concepto( ctrl.poliza().concepto() );
        var idxPartida = ctrl.poliza().partidas().indexOf(ctrl.partida);

        if(idxPartida > 0) {
            var partidaAnterior = ctrl.poliza().partidas()[idxPartida -1];
            ctrl.partida.ccto_1_id( partidaAnterior.ccto_1_id() );
            ctrl.partida.ccto_2_id( partidaAnterior.ccto_2_id() );
        }
    };

    this.debeHasFocus = m.prop(false);

    this.haberHasFocus = m.prop(false);

    this.debeFocus = function () {
        ctrl.debeHasFocus(true);

        if(ctrl.poliza().diferencia.$int()) {
            if(!ctrl.partida.importe.$int()) {
                if(ctrl.poliza().total_haber.$int() > ctrl.poliza().total_debe.$int()) {
                    ctrl.debe( ctrl.poliza().diferencia() );
                }
            }
        }
    }

    this.haberFocus = function () {
        ctrl.haberHasFocus(true);

        if(ctrl.poliza().diferencia.$int()) {
            if(!ctrl.partida.importe.$int()) {
                if(ctrl.poliza().total_debe.$int() > ctrl.poliza().total_haber.$int()) {
                    ctrl.haber( ctrl.poliza().diferencia() );
                }
            }
        }
    }

    var numFormat = d3.format(',.2f');

    this.debeBlur = function () {
        ctrl.debeHasFocus(false);
    }

    this.haberBlur = function () {
        ctrl.haberHasFocus(false);
    }


    this.printDebe = function () {
        if(ctrl.partida.debe_o_haber() != 1) return '';
        if(ctrl.debeHasFocus() == false) return numFormat(ctrl.partida.debe());
        return ctrl.partida.debe();
    }

    this.printHaber = function () {
        if(ctrl.partida.debe_o_haber() != -1) return '';
        if(ctrl.haberHasFocus() == false) return numFormat(ctrl.partida.haber());
        return ctrl.partida.haber();
    }


    this.removePartida = function () {
        var poliza = ctrl.poliza();
        var partida = ctrl.partida;

        poliza.eliminar().push(partida.poliza_partida_id());

        var idx = poliza.partidas().indexOf(partida);
        poliza.partidas().splice(idx,1);

        ctrl.actualizar(true);
    }
}

PartidaComponent.view = function (ctrl, args) {
    var partida = ctrl.partida;
    var enEdicion = ctrl.enabled() && partida.$integrable();
    var poliza = ctrl.poliza();

    return m('tr', {id : 'partida-'.concat(args.key) , config:config(ctrl), 'data-partida-id' : ctrl.partida.poliza_partida_id() },[
        m('td.options', enEdicion ? m('i.ion-drag') : ''),

        m('td.concepto', {'class':partida.hasError('noConcepto') ? 'error' : ''}, [
            ctrl.enabled() ? PartidaComponent.inputConcepto(ctrl) : partida.concepto()
        ]),

        m('td.cuentas', m('div.cuentas-selector', {
            'class' : partida.hasError('noCuenta') ? 'error' : ''
        }, ctrl.cuentaSelector.view()) ),

        poliza.ccto1() ? m('td.ccto', [
            ctrl.ccto1Selector.view({style:'width:100px'})
        ]) : '',

        poliza.ccto2() ? m('td.ccto', [
            ctrl.ccto2Selector.view({style:'width:100px'})
        ]) : '',

        m('td.importe.debe.text-right',
            ctrl.enabled() ? PartidaComponent.inputDebe(ctrl) :  (partida.debe.$int() ? partida.debe.number() : '--')
        ),
        m('td.importe.haber.text-right',
            ctrl.enabled() ? PartidaComponent.inputHaber(ctrl) : (partida.haber.$int() ? partida.haber.number() : '--')
        ),
        m('td.options',
            enEdicion ? m('i.ion-trash-b', {onclick:ctrl.removePartida}) : ''
        )
    ])
};

PartidaComponent.inputConcepto = function (ctrl) {
    return m('input[type="text"]', {
        value : ctrl.partida.concepto(),
        onchange : m.withAttr('value', ctrl.concepto),
        onfocus : ctrl.conceptoFocus,
        style : 'width:100%',
        disabled : !ctrl.enabled(),
    })
};


PartidaComponent.inputDebe = function (ctrl) {
    return m('input[type="text"].text-right', {
        value : ctrl.printDebe(),
        onchange : m.withAttr('value',ctrl.debe),
        onfocus : ctrl.debeFocus,
        onblur : ctrl.debeBlur,
        disabled : !ctrl.enabled(),
        size:12
    });
};

PartidaComponent.inputHaber = function(ctrl) {
    return m('input[type="text"].text-right', {
        value : ctrl.printHaber(),
        onchange : m.withAttr('value',ctrl.haber),
        onfocus : ctrl.haberFocus,
        onblur : ctrl.haberBlur,
        disabled : !ctrl.enabled(),
        size:12
    });
};



var dragging = null;
var dropping = null;

function config(ctrl){

    return function (element, initialized) {
        if(initialized) return;


        element.addEventListener('mouseover', function(ev) {
            if(!ev.target.matches('.ion-drag')) return;
            element.setAttribute('draggable','true');
        },true);

        element.addEventListener('mouseout', function(ev) {
            if(!ev.target.matches('.ion-drag')) return;
            element.removeAttribute('draggable');
        },true);

        element.addEventListener('dragstart', function (ev) {
            ev.stopPropagation();
            ev.dataTransfer.effectAllowed = 'move';

            dragging = element.getAttribute('data-partida-id');
            element.classList.add('dragging');

            var draggingItem;

            ctrl.poliza().partidas().forEach(function (partida) {
                if(dragging == partida.poliza_partida_id()) draggingItem = partida;
            });

            var crt = document.createElement('div');
            crt.style.display = 'none';
            crt.innerHTML = draggingItem.concepto();
            document.body.appendChild(crt);
            ev.dataTransfer.setDragImage(crt,0,0);

        },false);

        element.addEventListener('dragend', function (evt) {
            dragging = null;
            dropping = null;
            element.classList.remove('dragging');
        },false);

        element.addEventListener('dragenter', function (ev) {
            var _dropping = element.getAttribute('data-partida-id');
            var draggingIndex, droppingIndex, partida;

            if(_dropping != dropping){
                dropping = _dropping;

                if(dropping != dragging) {

                    ctrl.poliza().partidas().forEach(function (partida, index) {
                        if(partida.poliza_partida_id() == dragging) draggingIndex = index;
                        if(partida.poliza_partida_id() == dropping) droppingIndex = index;
                    });

                    partida = ctrl.poliza().partidas()[draggingIndex];
                    ctrl.poliza().partidas().splice(draggingIndex, 1);
                    ctrl.poliza().partidas().splice(droppingIndex, 0, partida);
                    m.redraw();
                }
            }
        },false);

    }
}
