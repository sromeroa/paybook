
var ActionBar = module.exports = {};
/**
 * ACTION BAR
 */

ActionBar.controller = function (args) {
    var ctrl = this;

    ctrl.copiar = function () {
        toastr.warning('No Disponible');
    }

    ctrl.imprimir = function () {
        var uri = "/exportar/docsPolizas/" + ctrl.poliza().poliza_id();
        window.open(uri, '_blank');
    }

    ctrl.cancelar = function () {
        location.pathname = '/polizas'
    }

    ctrl.puedeEliminar = function () {
        if(ctrl.poliza().$new()) return false;
        if(!ctrl.poliza().manual()) return false;

        return ctrl.poliza().estatus() != 'X';
    }

    ctrl.puedeGuardar = function () {
        return ctrl.enabled();
    }

    ctrl.puedeAplicar = function () {
        if (ctrl.enabled() == false) return false;
        return ['A','X'].indexOf(ctrl.poliza().estatus()) == -1;
    }

    ctrl.puedeFinalizar = function () {
        if (ctrl.enabled() == false) return false;
        return ['A','X','T'].indexOf(ctrl.poliza().estatus()) == -1;
    }


    ctrl.menuOptions  = m.prop([
        {
            icon:'i.ion-ios-copy-outline',
            action:'copiar', 
            caption:'Copiar Póliza',
            show : function () {
                return true
            }
        },
        {
            icon:'i.glyphicon.glyphicon-print',
            action:'imprimir', 
            caption:'Imprimir',
            show : function () {
                return true
            }
        },
        {
            icon:'i.ion-edit', 
            action:'activarEdicion', 
            caption:'Editar', 
            show : function () {
                return (ctrl.poliza().manual() == 1) && (ctrl.poliza().estatus() != 'X'); 
            }
        },
        {
            icon:'i.ion-ios-trash', 
            action:'eliminar', 
            caption:'Eliminar',
            show : function () {
                return (ctrl.poliza().manual() == 1) && (ctrl.poliza().estatus() != 'X'); 
            }
        }
    ])
}

ActionBar.view = function (ctrl) {
    return m('div.action-panel', [
        m('button.btn.btn-default.btn-sm', {
            onclick: ctrl.cancelar
        }, m('i.ion-ios-arrow-back'),' Cancelar'),

        /*
        ctrl.puedeEliminar() ? m('button.btn.btn-danger.btn-sm', {
            onclick : ctrl.eliminar
        }, m('i.ion-ios-trash'), ' Eliminar') : '',
        */
        
        ctrl.puedeGuardar () ? m('button.btn.btn-primary.btn-sm', {
            'class' : ctrl.guardable() ? '' : 'disabled',
            onclick : ctrl.guardar
        }, m('i.ion-archive'),' Guardar') : '',

        m('div.pull-right', [
            ctrl.puedeAplicar() ? m('button.btn.btn-success.btn-sm', {
                'class' : ctrl.finalizable() ? '' : 'disabled',
                onclick:ctrl.aplicar
            }, m('i.ion-checkmark-round'), ' Aplicar') : ''
        ])
    ]);
}
