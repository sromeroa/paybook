var TabsComponent = module.exports = {};

TabsComponent.controller = function (args) {

    var polizaCtrl = args.polizaCtrl;
    var componenteActivo = m.prop();


    this.componentes = m.prop([]);

    this.componentes().push({
        nombre : function () {
            var qNotas = polizaCtrl.notas.notas().length;
            return 'Notas' + ( qNotas ? ' (' + ( qNotas > 10 ? '10+' : qNotas) + ')' : '' )
        },
        componente : m.component(NotasComponent, {
            enabled:polizaCtrl.enabled,
            model : polizaCtrl.notas
        })
    });

    this.componentes().push({
        nombre : m.prop('Archivos'),
        componente : m.component(ArchivosComponent, {
            nombre_recurso : 'poliza',
            id_recurso : polizaCtrl.poliza().poliza_id(),
            id_organizacion : oorden.organizacion().organizacion_id
        })
    });

    this.componentes().push({
        nombre : m.prop('Bitácora'),
        componente : m.component(ComponenteNoDisponible)
    });


    this.componenteActivo = componenteActivo;

    this.selectComponente = function (cmp,ev) {
        ev.stopPropagation();
        ev.preventDefault();
        componenteActivo(cmp);
    }
};

TabsComponent.view = function (ctrl) {
    return m('div.poliza-extras', {'class' : ctrl.componenteActivo() ? '' : 'cerrada'}, [
        m('ul.nav.nav-tabs[role="tablist"]',
            ctrl.componenteActivo() ? m('li.pull-right', [
                m('a[href]', {
                    onclick:ctrl.selectComponente.bind(null,null)
                }, m.trust('&times;'))
            ]) : '',
            ctrl.componentes().map(function (cmp) {
                return m('li', {
                    'class':ctrl.componenteActivo() === cmp ? 'active' : ''
                }, [
                    m('a[href]', {
                        onclick:ctrl.selectComponente.bind(null,cmp)
                    }, cmp.nombre())
                ]);
            })
        ),
        ctrl.componenteActivo() ? m('div.tab-content', [
            m('div.tab-pane.active', [
                ctrl.componenteActivo().componente
            ])
        ]) : ''
    ])
};


var NotasComponent = {};

NotasComponent.view = function (ctrl, args) {
    return args.model.view()
};

var ComponenteNoDisponible = {};

ComponenteNoDisponible.view = function () {
    return m('p', '(no disponible)')
};






var ArchivosComponent = {};

ArchivosComponent.view = function (ctrl, args) {
    /*return m('div',{config:ArchivosComponent.init},[
        m('div[adjuntar-documentos=""]', {
            'nombre-recurso' : args.nombre_recurso,
            'id-recurso' : args.id_recurso,
            'id_organizacion' : args.id_organizacion
        })
    ])*/

    return m('div', {
        'nombre-recurso' : args.nombre_recurso,
        'id-recurso' : args.id_recurso,
        'id_organizacion' : args.id_organizacion
    },[
        m('input[file]', {

        })
    ])
}

ArchivosComponent.init = function (el, isInit) {
    /*if(isInit) return;
    var $scope = $(el).scope();
    var $compile = $scope.$root.__compiler__;
    $compile($(el).contents())($scope);
    */
}
