var PolizaIndex = module.exports = {};
var FechasMultiSelector = require('../misc/components/FechasMultiSelector.js');


PolizaIndex.controller = function () {
    var ctrl = this;
    var Poliza = oor.mm('Poliza');

    ctrl.items         = m.prop([]);
    ctrl.filteredItems = m.prop([]);
    ctrl.criterios     = Criterios(ctrl);

    ctrl.filterItems = function () {
        var filteredItems = ctrl.items().filter(ctrl.criterios.filter);
        ctrl.filteredItems(filteredItems);
        ctrl.table.items(ctrl.filteredItems());
    };


    ctrl.table = MtTable({
        columnas : ['check','fecha','poliza','concepto','debe','haber','estatus'],
        defCol : Columnas(ctrl)
    });

    ctrl.table.itemTrParams = function (item) {
        return {
            key : item.operacion_id(),
            class : item.checked && item.checked() ? 'info' : ''
        };
    }


    ctrl.changeTab = function (tab,evt) {
        evt.stopPropagation();
        evt.preventDefault();
        ctrl.criterios().tab.selected(tab);
        ctrl.filterItems();
    };


    loadItems();

    function loadItems () {
        oorden().then(function () {
            m.request({url:'/apiv2?modelo=polizas',method:'GET'}).then(function (r) {
                var data = r.data.map(function (p) { return new Poliza(p); });
               ctrl.items(data);
               ctrl.filterItems();
            });
        });
    }
};


PolizaIndex.view = function (ctrl) {
    var tab = ctrl.criterios().tab;

    return m('div.polizas-index.search-index', [
        m('div.simpleTabs', [
            m('ul.nav.nav-tabs.nav-justified.simple', {style:'margin-bottom:15px'}, [
                tab.tabs().map(function (tb) {
                    return m('li', {'class' : tab.selected() == tb ? 'active' : '', onclick: ctrl.changeTab.bind(null,tb) }, [
                        m('a[href=""]', tb.nombre)
                    ]);
                })
            ])
        ]),
        m('div.row.highlight.grey', {style:'margin:0 0 10px 0'}, [
            m('div.col-xs-12.col-md-5', [
                m.component(FechasMultiSelector, ctrl.criterios().fechas)
            ]),
            m('div.col-xs-12.col-md-4', {style:'margin-top:16px'}, [
                m('label', 'Búsqueda'),

                m('input.form-control[type="text"]', {
                    config:criterios(ctrl),
                    placeholder : 'Concepto o referencia'
                })

            ])
        ]),
        ctrl.table()
    ]);
};



function criterios (ctrl) {
    var actualizar = _.throttle(function () {
        ctrl.filterItems();
        m.redraw();
    },500);

    return function (element,isInitialized) {
        if(isInitialized) return;

        var el = $(element);
        el.on('input', function () {
            var input = $(this);
            var criterio = ctrl.criterios().search;

            criterio.value(input.val());
            actualizar();
        });
    }
}




function Criterios (ctrl) {
    var criterios = {}
    var criteriosProp = m.prop(criterios);


    criterios.tab = function (item) {
        if(criterios.tab.selected()) {
            return criterios.tab.selected().filter(item);
        }
    }


    criterios.tab.tabs = m.prop([]);

    criterios.tab.tabs().push({
        nombre : 'Todas',
        filter : function (item) { return true }
    });


    oor.mm('Poliza').estatuses().forEach(function (est) {
        criterios.tab.tabs().push({
            nombre : est.nombre,
            filter : function (item) { return item.estatus() == est.estatus }
        });
    });


    criterios.tab.selected = m.prop( criterios.tab.tabs()[0] );


    /**
     * Búsqueda Rápida
     */

    criterios.search = function (item) {
        var val;
        var concepto;
        var referencia;

        val = criterios.search.value() || '';
        val = val.latinize().toLowerCase();
        if(val) {
            concepto = item.concepto() || '';
            referencia = item.referencia() || '';
            if(concepto.latinize().toLowerCase().indexOf(val) >= 0)  return true;
            if(referencia.latinize().toLowerCase().indexOf(val) >= 0)  return true;
            return false;
        }
        return true;
    }

    criterios.search.value = m.prop();

    /**
     * Por Fechas
     */
    criterios.fechas = function (item) {
        if(criterios.fechas.tipo() == 'fiscal') {
            var mes = Number(item.mes_contable());
            var ano = Number(item.ano_contable());

            if(mes != Number(criterios.fechas.mes_contable())) return false;
            if(ano != Number(criterios.fechas.ano_contable())) return false;
            return true;
        } else if( criterios.fechas.tipo() == 'fechas') {
            var f = item.fecha();
            var desde = criterios.fechas.desde();
            var hasta = criterios.fechas.hasta();

            if(desde && (f < desde)) return false;
            if(hasta && (f > hasta)) return false;
            return true;
        }

        return true;
    }

    var now   = new Date;
    var fecha = oor.fecha.toSQL(now).split('-');

    criterios.fechas.tipo = m.prop('fiscal');
    criterios.fechas.desde = m.prop( oor.fecha.toSQL(oor.fecha.inicialMes(now)) );
    criterios.fechas.hasta = m.prop( oor.fecha.toSQL(oor.fecha.finalMes(now)) );
    criterios.fechas.mes_contable = m.prop(fecha[1]);
    criterios.fechas.ano_contable = m.prop(fecha[0]);

    criterios.fechas.onchange = function () {
        ctrl.filterItems();
    };

    criteriosProp.filter = function (item,pos) {
        var k;
        for(k in criterios) {
            if(! criterios[k].call(null,item,pos)) return false;
        }
        return true;
    };

    return criteriosProp;
}

function Columnas (ctrl) {
     var columnas = {};

    columnas.check = {
        caption : function () {
             return m('input[type="checkbox"]')
        },
        value : function (item) {
            if(!item.checked) item.checked = m.prop(false);
            return m('input[type="checkbox"]', {
                checked : item.checked(),
                value : item.poliza_id(),
                onchange : m.withAttr('checked', item.checked)
            });
        },
        sortable:false
    };

    columnas.concepto = {
        caption : 'Concepto',
        value : function (item) {
            return m('div', [
                m('a.text-indigo', {href:'/polizas/poliza/' + item.poliza_id()}, [
                    item.manual() == 1 ? m('i.ion-android-hand[style="padding-right:6px"]') : '',
                    item.concepto()
                ]),
                m('div', m('small.text-grey', item.referencia()))
            ]);
        }
    };

    columnas.poliza = {
        caption : 'Póliza',
        value : function (item) {
            var tipo = oor.mm('Poliza').tipos[item.tipo()];
            tipo = tipo ? tipo() : {};
            return tipo.text + ' #' + item.numero();
        }
    };

    columnas.fecha = {
        caption : 'Fecha',
        value : function (item) {
            return m('div', [
                m('span',  item.fecha()),
                m('div',[
                    m('small.text-grey', item.mes_contable() + '-' + item.ano_contable())
                ])
            ]);
        }
    };

    columnas.debe = {
        caption : 'Debe',
        value : function (item) {
            return m('div.text-right', item.total_m_base_debe.number());
        }
    };

    columnas.haber = {
        caption : 'Haber',
        value : function (item) {
            return m('div.text-right', item.total_m_base_haber.number());
        }
    };

    columnas.estatus = {
        caption : 'Estatus',
        value : function (item) {
             var estatus = oor.mm('Poliza').estatuses[item.estatus()];
            estatus = estatus ? estatus() : {};
            return m('div.text-center', [
                m('small.text-' + estatus.color, estatus.nombre_corto)
            ])
        }
    };

    return columnas;
}
