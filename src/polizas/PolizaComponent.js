var PolizaComponent = module.exports = {};
var TablaPartidas = require('./TablaPartidas.js');
var ActionBar = require('./components/ActionBar.js');
var TabsComponent = require('./components/TabsComponent.js');

var NotasComponent = require('../misc/components/NotasComponent');

PolizaComponent.controller = function (attrs) {
    var ctrl = this;
    var Poliza = oor.mm('Poliza');

    this.poliza     = m.prop();
    this.actualizar = actualizar;

    this.enabled    = m.prop(false);
    this.hardEnabled = m.prop(false);

    this.guardar    = guardar;
    this.aplicar    = aplicar;

    this.finalizable = finalizable;
    this.guardable   = guardable;
    this.eliminar    = eliminar;

    this.settings = m.prop();

    
    ActionBar.controller.call(this);

    this.caption = function () {
        var poliza = ctrl.poliza();
        var tipo    = Poliza.tipos[poliza.tipo()];
        tipo = tipo ? tipo() : {};

        var caption = 'Póliza de ' + tipo.text + ' #';
        caption += ctrl.poliza().numero() ? ctrl.poliza().numero() : '<auto>';

        return caption;
    }

    this.estatus = function () {
        var estatus = Poliza.estatuses[ctrl.poliza().estatus()];
        return estatus ? estatus() : {};
    }

    this.activarEdicion = function () {
        if(ctrl.poliza().estatus() == 'A') {
            ctrl.enabled(true);
        }
    }

    this.obtenerPoliza = function () {

        obtenerPoliza(attrs.polizaId).then(function (poliza) {
            if(poliza.$new()) {
                poliza.poliza_id(oor.uuid());
                poliza.estatus('P');
                poliza.tipo(ctrl.settings().tipo_poliza || 'D');
                poliza.manual(true);

                poliza.asignarFecha( ctrl.settings().fecha || oor.fecha.toSQL(new Date) );

                var cctosActivos =  oorden.cctos.activos();
                if(cctosActivos[0]) poliza.ccto1(cctosActivos[0]);
                if(cctosActivos[1]) poliza.ccto2(cctosActivos[1]);

                var tcBase = oorden.tipoDeCambio.base();
                poliza.moneda( tcBase.codigo_moneda );
                poliza.tipo_de_cambio( tcBase.tipo_de_cambio );
                poliza.tasa_de_cambio(1);
            }

            ctrl.poliza(poliza);

            ctrl.enabled(estatusEsModificable(poliza.estatus()));
            ctrl.hardEnabled( ctrl.enabled() );

            ctrl.actualizar(true);
            crearControl();
            crearNotas();
        });
    }

    this.obtenerPoliza();

    // Empieza en edicion si es dif a Aplicado y dif a Eliminado
    function estatusEsModificable(estatus) {
        return ['A','X'].indexOf(estatus) == -1;
    }


    function crearControl () {
        ctrl.tipoSelector = oor.select2({
            data : Poliza.tipos(),  
            model : ctrl.poliza().tipo
        });

        ctrl.tipoSelector.enabled = ctrl.hardEnabled;
    }


    function crearNotas () {
        ctrl.notas = new NotasComponent.controller({
            pertenece_a_id : ctrl.poliza().poliza_id
        }) 

        ctrl.notas.view  = function () {
            return NotasComponent.view(ctrl.notas);
        }
    }

    function finalizable () {
        var poliza = ctrl.poliza();
        if(!guardable()) return false;
        if(poliza.diferencia.$int() > 0) return false;
        return true;
    }

    function guardable () {
        return ctrl.poliza().$errors().length == 0;
    }

    function actualizar (actualizarPoliza) {
        if(actualizarPoliza) ctrl.poliza().actualizar();

        if(!ctrl.poliza().concepto()) {
            ctrl.poliza().addError('noConcepto');
        } else {
            ctrl.poliza().remError('noConcepto');
        }

        ctrl.poliza().$partidasErroneas([]);

        ctrl.poliza().partidas().forEach(function (partida) {
            if(!partida.$integrable) {
                partida.$integrable = m.prop(false);
            }
            if(partida.$integrable() == false) {
                partida.$integrable( Boolean(partida.concepto() || partida.importe.$int()) );
            }

            if(partida.$integrable()) {
                if(!partida.concepto()) {
                    partida.addError('noConcepto')
                } else {
                    partida.remError('noConcepto');
                }
                if(partida.importe.$int() && !partida.cuenta_id()) {
                    partida.addError('noCuenta');
                } else {
                    partida.remError('noCuenta');
                }
                if(partida.$errors().length) {
                    ctrl.poliza().$partidasErroneas().push(partida);
                }
            }
        });

        if(ctrl.poliza().$partidasErroneas().length) {
            ctrl.poliza().addError('partidas');
        } else {
            ctrl.poliza().remError('partidas');
        }

        autoLineas();
    }

    function apiError(r) {
        toastr.error(r.status.message);
    }

    function guardar () {
        return Poliza.save(ctrl.poliza())
                .then(afterSave(null,'Cambios Guardados'), apiError)
    }

    function aplicar () {
            Poliza.save(ctrl.poliza())
                .then(function () {
                    return  Poliza.estatus(ctrl.poliza(), 'A')
                }, apiError)
                .then(afterSave(null, 'Póliza Aplicada'),apiError)
    }


    function eliminar () {
        if(!confirm('¿Desea Eliminar la Póliza?')) return;

        Poliza.eliminar(ctrl.poliza())
            .then(afterSave(null, 'Póliza Eliminada'));
    }

    function afterSave (redirect, message) {
        message || (message = 'Póliza Guardada');
        return function () {
            lsMessage({text:message})
            location.pathname = '/polizas';
        }
    }


    function autoLineas () {
        if(ctrl.hardEnabled() == false) return;

        var integrables = 0;

        ctrl.poliza().partidas().forEach(function (partida) {
            if(partida.$integrable()) integrables++;
        });

        if(ctrl.poliza().partidas().length == integrables) {
            agregarLinea();
        }
    }


    function agregarLinea() {
        var Partida = oor.mm('Partida');
        var partida =  new Partida;

        partida.poliza_partida_id(oor.uuid());

        ctrl.poliza().partidas().push(partida);
        ctrl.poliza().extend(partida);

        actualizar();
    }

    function obtenerPoliza (id) {
        var defer = m.deferred();

        oorden().then(function () {
            if(id == 'crear') {
                cargarLastUsed().then(function (used) {
                    var poliza = new Poliza;
                    ctrl.settings(used || {});
                    poliza.$new(true);
                    defer.resolve(poliza);
                });
            } else {
                Poliza.get(id).then(function (poliza) {
                    poliza.$new(false);
                    defer.resolve(poliza);
                });
            }
        });

        return defer.promise;
    }


    function cargarLastUsed () {
        return m.request({
            method : 'GET',
            url : '/api/last-used/poliza_' + oorden.usuario().usuario_id,
            unwrapSuccess : f('data')
        })
    }

};



PolizaComponent.view = function (ctrl) {
    return m('div.poliza', [
        m('div.btn-group.dropdown.pull-right', [
            m('button.btn.btn-default.btn-sm.dropdown-toggle.btn-flat[data-toggle="dropdown"]', [
                m('i.ion-more')
            ]),


            m('ul.dropdown-menu.dropdown-menu-right', [
                ctrl.menuOptions().map(function (opt) {
                    console.log(opt)
                    if(!opt.show()) return null;

                    return m('li', [
                        m('a[href="javascript:;"]', {
                            onclick: ctrl[opt.action]
                        }, [
                            m(opt.icon), ' ', opt.caption
                        ])
                    ])
                })
            ])
        ]),
        m('strong.pull-right.text-' + ctrl.estatus().color, {
            style:'padding-right:20px'
        }, [
            m('small',ctrl.estatus().nombre)
        ]),
        m('h3.column-title', ctrl.caption()),

        m('div.poliza-form', [
            m('div.flex-row.flex-end',[
                PolizaComponent.inputConcepto(ctrl),
                PolizaComponent.inputTipo(ctrl),
                PolizaComponent.inputNumero(ctrl)
            ]),

            m('div.flex-row.flex-end', [
                PolizaComponent.inputReferencia(ctrl),
                PolizaComponent.inputFecha(ctrl),
                PolizaComponent.fiscal(ctrl)
            ])
        ]),

        m('br'),

        m('div.row', [
            m('div.col-md-12', m.component(TabsComponent, {polizaCtrl : ctrl}))
        ]),

        m('br'),

        m('.table-responsive', [
            m.component(TablaPartidas, {polizaCtrl : ctrl})
        ]),

        ActionBar.view(ctrl),

        m('br')
    ]);
}

PolizaComponent.inputTipo = function  (ctrl) {
    return m('div.mt-inputer.tipo', [
        m('label', 'Póliza de'),
        ctrl.tipoSelector ? ctrl.tipoSelector.view({style:'min-width:80px'}) : ''
    ]);
};


PolizaComponent.inputNumero = function  (ctrl) {
    return oor.inputer({
        label : '#',
        model : ctrl.poliza().numero,
        input: 'input.text-center[type="text"]',
        placeholder : '<auto>'
    }, {'class' :'numero'}, {size:7, disabled : !ctrl.hardEnabled()})
};

PolizaComponent.inputConcepto = function  (ctrl) {
    return oor.inputer({
        label : 'Concepto',
        model : ctrl.poliza().concepto
    },{'class':'concepto'},{disabled : !ctrl.enabled(), config : focus(ctrl)})
};

function focus (ctrl) {
    if(!ctrl.enabled()) return _.noop;

    return function (el, isInitialized) {
        if(isInitialized) return;

        el.focus();
        el.parentElement.classList.add('focus');

    }
}

PolizaComponent.inputFecha = function (ctrl) {
    return m('div.mt-inputer.fecha',[
        m('label', 'Fecha'),
        m('input.text-right[type="date"]', {
            value : ctrl.poliza().fecha(),
            onchange : m.withAttr('value',function (v) {
                ctrl.poliza().asignarFecha(v);
            }),
            disabled : !ctrl.enabled()
        })
    ])
}


PolizaComponent.inputReferencia = function (ctrl) {
    return oor.inputer({
        label : 'Referencia',
        model : ctrl.poliza().referencia
    },{'class' : 'referencia'},{disabled : !ctrl.enabled()});
}

PolizaComponent.fiscal = function (ctrl)  {
    var mesEnabled = ctrl.enabled() && (12 <= ctrl.poliza().mes_contable()) && (14 >= ctrl.poliza().mes_contable());

    return m('.mt-inputer.fiscal', [
        m('label', 'P. Fiscal'),
        m('input[type="number"].mes.text-right', {
            value : mesContable(),
            onchange : m.withAttr('value', mesContable),
            disabled : !mesEnabled
        }),
        m('input[type="text"].ano.text-right', {
            value : ctrl.poliza().ano_contable(),
            disabled : true
        })
    ]);

    function mesContable (mes) {
        if(!arguments.length) return ctrl.poliza().mes_contable();

        if(mes > 11 && mes < 15) {
            ctrl.poliza().mes_contable(mes);
        }
    }
}

PolizaComponent.mesFiscal = function (ctrl) {
    var mesEnabled = ctrl.enabled() && (12 <= ctrl.poliza().mes_contable()) && (14 >= ctrl.poliza().mes_contable());

    return oor.inputer({
        label : 'M.F.',
        input : 'input.text-right[type="number"]',
        model : mesContable
    },{style:'flex:80px  1 1'},{disabled:!mesEnabled, style :'width:2em'});
}


PolizaComponent.anoFiscal = function (ctrl) {
    return oor.inputer({
        label : 'A.F',
        input : 'input.text-right[type="text"]',
        model : ctrl.poliza().ano_contable
    },{style:'flex:80px 1 1'},{disabled:true, style :'width:3em'});
}
