require('./Latinise');
require('../misc/advSelector');

var timeManager = require('./TimeManager');
var fecha = require('./fecha');
var cuentasSelect2 = require('./selectors/cuentasSelect2');
var cuentasSelector = require('./selectors/cuentasSelector');
window.Papa = require('../papaparse/papaparse');

var oor = {
    select2 : select2,
    tercerosSelect2 : tercerosSelect2,

    loading : function (inline) {
        return m(inline ? 'span' : 'div.text-center', {key : '$loading'}, m('i.fa.fa-spin.fa-spinner'));
    },

    fondoGris : function () {
        $('html').css('background',"#f3f3f3");
        $('body').css('background',"#f3f3f3");
    },

    autofocus : function (el, inited) {
        if(!inited) { el.focus(); }
    },

    cuentasSelect2 : cuentasSelect2,
    cuentasSelector : cuentasSelector,
    impuestosSelect2 : impuestosSelect2,
    retencionesSelect2 : retencionesSelect2,

    cctoSelect2 : cctoSelect2,
    tipoDocSelect2 : tipoDocSelect2,

    inputer : inputer,
    uSettings : userSettings,

    uuid : guid,
    mm : OorModels(),
    fecha : fecha(),

    time : timeManager(),
    xPropConfig : xPropConfig,
    displayMessages : displayMessages,
    lsMessage : lsMessageInitializer(),
    performHighlight : performHighlight,
    tooltip : tooltip,

    request :request,
    autoGrow : autoGrow
};

templateFunctions(oor);

module.exports = oor;


function autoGrow (el, isInitialized, data) {
    if(!isInitialized) {
        data.grow = function () {
            el.style.height = '10px';
            el.scrollTop = 20000;
            if(el.scrollTop < 20000) {
                el.style.height = String(Math.max(50,10 + el.scrollTop)).concat('px')
            }
        };

        data.grow.debounced = _.debounce(data.grow, 100);
        el.addEventListener('input', data.grow.debounced);
        el.style['min-width'] = '300px';
        data.grow();
    }
}




function request (url, method, extraParams) {

    var data = {
        method : method || 'GET',
        url : url,
        unwrapSuccess : f('data')
    };

    if(oor.v1 == true) {
        data.type = function (r) { return r.data }
    }

    var params = nh.extend(data, extraParams);

    return m.request(params);
}


function performHighlight (selector, color, startColor) {
    if(!startColor){
        startColor = '#fff'
    }

    return function () {
        d3.selectAll(selector)
            .style('background', startColor)
            .transition().duration(500)
            .style('background', '#79e078')
            .transition().delay(500).duration(500)
            .style('background', startColor)
            .each('end', function () {
                d3.select(this).style('background', undefined);
            })

    }
}



/**
 * Función que muestra los mensajes almacenados en localStorage
 */
function lsMessageInitializer () {
    var MESSAGE_LOCAL_STORAGE = 'MESSAGE_LOCAL_STORAGE';

    setTimeout(function () {
        var message;

        if(message = localStorage.getItem(MESSAGE_LOCAL_STORAGE)) {
            localStorage.removeItem(MESSAGE_LOCAL_STORAGE);

            message = JSON.parse(message);
            message.type = message.type || 'success';
            toastr[message.type](message.text);
        }

    },200);


    function lsMessage (msg) {
        localStorage.setItem(MESSAGE_LOCAL_STORAGE, JSON.stringify(msg));
    }

    window.lsMessage = lsMessage;
    return lsMessage;

}



m.redraw.debounced = _.debounce(m.redraw, 500);

function displayMessages () {
    var msgNode = document.querySelector('#messages-toastr');
    if(!msgNode) return;

    var messages = JSON.parse(msgNode.innerHTML);
    if(!messages) return;

    Object.keys(messages).forEach(function (kind) {
        messages[kind].forEach(function (msg) {
            toastr[kind](msg);
        })
    });
}

function xPropConfig(ctx) {
    return function (element, isInitialized) {
        if(isInitialized) return;

        $(element).on('input', '[x-prop]', function () {
            var input = $(this);
            var model = f(input.attr('x-model'))(ctx);

            model[input.attr('x-prop')](input.val())

            m.redraw.debounced();
        })
    }
}

$(function () {
    $(document.body).on('focus', '.mt-inputer > input, .mt-inputer > textarea', function () {
        $(this).parent().addClass('focus');
    });

    $(document.body).on('blur', '.mt-inputer > input, .mt-inputer > textarea', function () {
        $(this).parent().removeClass('focus');
    })
})


/**
 * Select2 Adapter
 *
 */

select2.NULL_VALUE = '::NULL_VALUE::';

function select2 (params) {
    var model    = m.prop( params.model );

    var data = ( typeof params.data === 'function') ? params.data : m.prop(params.data);
    var isInitialized = m.prop( false);
    var options       = m.prop( null );
    var selected      = m.prop( null );
    var el            = m.prop( null );
    var currVal       = m.prop( null );

    var enabled = true;

    var self = {
        view  : view,
        model : model,
        data  : data,
        selected : selected,
        element : el,
        rebuild : rebuild,
        config : config,
        key : params.key,
        onchange : params.onchange,
        enabled : params.enabled,
        el : el
    };

    return self;


    function rebuild () {
        if(isInitialized() == false) return;
        config(el()[0], false);
    }


    function notify () {
        var val = model();
        //Se actualiza el valor del modelo
        val(el().select2('val'));

        if(typeof params.find == 'function') {
            selected( params.find(val()) );
        }

        if(typeof self.onchange == 'function') {
            self.onchange( val() );
        }

        if(typeof val.onchange == 'function') {
            val.onchange( val() );
        }
    }

    function setValue(force) {
        if(force === true || (model())() != currVal()) {
            currVal( (model())() );
            if(! el() ) return;
            el().select2('val', currVal());
        }
    }

    function config (node, initialized) {
        var element   = $(node);
        var initValue = (model())();

        if(initialized === false) {
            el(element);
            var select2Params = {
                data : data(),
                templateSelection : params.templateSelection,
                templateResult    : params.templateResult,
                placeholder       : params.placeholder
            };

            if(params.url) {
                select2Params.ajax = {
                    url : params.url,
                    dataType : 'json',
                    delay : 250,
                    data :  params.ajaxData,
                    processResults : params.ajaxProcessResults,
                    cache : true
                }
                delete(select2Params.data);
            }


            function onChange() {
                var val = model();
                if(element.select2('val') == val()) return;

                if(element.select2('val') == select2.NULL_VALUE){
                    element.select2('val', null);
                }

                m.startComputation();
                notify();
                m.endComputation();
            }


            element.select2(select2Params)
                .on('change', function (e) {
                    isInitialized() && onChange()
                })


            element.parent().on('focus', '.select2-selection', function () {
                element.select2('open');
            });



            if((typeof self.enabled == 'function')) {
                element.select2('enable', self.enabled());
                enabled = self.enabled();
            }


            setValue(true);
            isInitialized(true);
        }

        if((typeof self.enabled == 'function') && self.enabled() != enabled) {
            element.select2('enable', self.enabled());
            enabled = self.enabled();
        }

        setValue();
    }

    function view (vParams) {
        vParams || (vParams = {});
        vParams.config = config;

        return [
            params.quitBtn ? m('div.pull-right', {
                style:'cursor:pointer',
                onclick:function () { el().select2('val',null) }
            }, '×') : '',
            m('select', vParams)

        ]
    }
}

/**
 * Return TercerosSelector
 */
function tercerosSelect2 (iParams) {

    var data = m.prop();

    getOptions();

    var selector = select2(_.extend({
        data : data,
        debug : true,
        find : function (id) {
            var f = (data() || []).filter(function (t) { return t.id == id })[0];
            return f && f.data;
        }
    }, iParams));

    selector.getOptions = getOptions;

    return selector;

    function getOptions () {
        var query = {modelo:'terceros'};



        if(iParams.tipo == 'cliente') {
            console.log('EsCliente')
            query.es_cliente = '1';
        } else if (iParams.tipo == 'proveedor') {
            console.log('EsProveedor')
            query.es_proveedor = '1';
        }

        return m.request({method:'GET', url:'/apiv2', data : query}).then(function (t) {
            data(
                t.data.map(function (t3) {
                    return {id:t3.tercero_id, text:t3.nombre + ' - ' + t3.clave_fiscal + ' - (' + t3.codigo + ')', data:t3};
                })
            );
            selector.rebuild();
        });
    }
}


/**
 * Return TercerosSelector
 */
function productosSelect2 (iParams) {

    var data = m.prop();

    getOptions();

    var selector = select2(_.extend({
        data : data,
        debug : true,
        find : function (id) {
            var f = (data() || []).filter(function (t) { return t.id == id })[0];
            return f && f.data;
        }
    }, iParams));

    selector.getOptions = getOptions;

    return selector;

    function getOptions () {
        return m.request({method:'GET', url:'/apiv2?modelo=productos'}).then(function (t) {
            data(
                t.data.map(function (t3) {
                    return {id:t3.tercero_id, text:t3.codigo +  ' - ' + t3.nombre + ' - ' + t3.clave_fiscal, data:t3};
                })
            );
            selector.rebuild();
        });
    }
}

/**
 * @return retencionesSelector
 */


function retencionesSelect2 (iParams) {

    var retenciones = iParams.retenciones
        .filter(function (c) { return c.estatus == 1})
        .map(function (c) {
            return {
                id:c.retencion_conjunto_id,
                text:c.retencion_conjunto
            };
        });

    retenciones.unshift({id:select2.NULL_VALUE, text:'(ninguna)'});

    return select2(
        _.extend({
            placeholder : '(ninguna)',
            data : retenciones,
            find : function (id) {
                return iParams.retenciones.filter(function (c) {
                    return c.retencion_conjunto_id == id;
                })[0];
            }
        }, iParams)
    );
}

/**
 * @return impuestosSelector
 */
function impuestosSelect2 (iParams) {
    return select2(
        _.extend({
            data : iParams.impuestos
            .filter(function (c) { return c.estatus == 1})
            .map(function (c) {
                return {
                    id:c.impuesto_conjunto_id,
                    text:c.impuesto_conjunto + ' (' + Number(c.tasa) + '%)'
                };
            }),
            find : function (id) {
                return iParams.impuestos.filter(function (c) {
                    return c.impuesto_conjunto_id == id;
                })[0];
            }
        }, iParams)
    );
}


function cctoSelect2  (iParams) {
    return select2(
        _.extend({
            data : iParams.centroDeCosto.elementos.map(function (e) {
                return {id:e.elemento_ccosto_id, text:e.elemento}
            })
        },iParams)
    );
}


/**
 * @return Selector de Tipos de Documento
 */

function tipoDocSelect2 (iParams) {
    return select2(
        _.extend({
            data : iParams.tiposDoc.map(function (t) {
                return {id:t.tipo_documento_id, text:t.nombre};
            }),
            find : function (id) {
                return iParams.tiposDoc.filter(function (t) {
                    return t.tipo_documento_id == id;
                })[0];
            }

        }, iParams)
    );
}

/**
 * @return
 */

function inputer (attrs, attrsInputer, attrsInput) {
    attrsInputer = _.extend({}, attrsInputer, {config: config});

    attrsInput = _.extend({}, {
        placeholder : attrs.placeholder || attrs.label,
        onchange : m.withAttr('value', function (v) {
            attrs.model(v);
            if(typeof attrs.model.onchange === 'function') attrs.model.onchange(v);
        }),
        onkeyup : m.withAttr('value',attrs.model),
        value : attrs.model()
    }, attrsInput);

    return m('div.mt-inputer', attrsInputer, [
        m('label', attrs.label),
        m((attrs.input || 'input[type="text"]').concat( attrs.readonly ? '[disabled]' : ''), attrsInput)
    ]);

    function config (node, initialized) {
        if(!initialized) {
            var el = $(node);

            el.delegate('input', 'focus', function () {
                el.addClass('focus');
            });

            el.delegate('input', 'blur', function () {
                el.removeClass('focus');
            });
        }
    }
}




/** USER SETTINGS  **/

function userSettings () {

    var settings = [].map.call(arguments, function (item) {
            return item.join(':');
        }).join(',');

    return m.request({
        method : 'GET',
        url : '/u-settings/settings?' + $.param({'settings' : settings}),
        unwrapSuccess : function (r) { return r.data; }
    });
}


userSettings.save = function (data) {
    return m.request({
        method : 'PUT',
        url : '/u-settings/settings',
        data  : data
    });
};

userSettings.choose = function (key, defaultValue) {
    return function () {
        for(var i = 0; i<arguments.length; i++) {
            if(arguments[i] && (typeof arguments[i][key]) != 'undefined') {
                return arguments[i][key];
            }
        }
        return defaultValue;
    }
};

/**
 *
 * UUID
 **/

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}




function tooltip (tooltipKey) {
    return tooltipKey;
}

/**
 * Modelos
 */

function OorModels () {
    if(!window.mtk) return;
    var mm = mtk.ModelManager();

    var map = {
        polizas : 'poliza',
        terceros : 'terceros',
        operaciones : 'operacion'
    };

    mm.api.prefix('/apiv2');

    mm.api.get = function (id) {

        var qParams = {modelo : this.serviceName()};
        qParams[this.idKey()] =  id;

        if(this.include()) {
            qParams.include = this.include();
        }

        return {
            method : 'GET',
            url : mm.api.prefix().concat('?', m.route.buildQueryString(qParams)),
        }
    };


    mm.api.create = function (resource) {
        var url;
        console.log( map[this.serviceName()]);

        if(map[this.serviceName()]) {
            if(resource.$new && resource.$new() == false && resource.editUrl) {
                url = resource.editUrl()
            } else {
                url = '/api/' + map[this.serviceName()] + '/agregar';
            }
        } else {
            url = mm.api.prefix().concat('?modelo=' + this.serviceName() );
        }


        return  {
            method : 'POST',
            url : url,
            data : resource.toJSON()
        };
    };

    return mm;
}


function templateFunctions (oor) {

    oor.panel = function (settings, body) {
        var title = settings.title;
        if(typeof title == 'string'){
            title = m('h3', title)
        }

        return m('div', [
            m('#title-head', [
                m('.col-xs-12.col-md-10.title-bar', title),
                m('.col-xs-12.col-md-6.action-bar', [ settings.buttons ? m('.panel-buttons', settings.buttons) : null ]),
                m('.clear'),
            ]),

            settings.inter ? settings.inter : null,

            m('.panel', [
                m('.panel-body',body)
            ]),
        ]);
    };

    oor.stripedButton = function (tag,children,attrs) {
        return m((tag || 'button').concat('.btn.button-striped.button-full-striped.btn-ripple.btn-xs'),attrs,children)
    }

}
