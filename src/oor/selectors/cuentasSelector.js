
module.exports = cuentasSelector;

function cuentasSelector (params) {
    var cuentasMap = {};

    var cuentas = params.cuentas
        .filter( f('estatus').is('1') )
        .filter(function (d) {
            cuentasMap[d.cuenta_contable_id] = d;
            return d.acumulativa == 0;
        });

    cuentas.forEach(function (d) {
        if(d.subcuenta_de) {
            d.padre = cuentasMap[d.subcuenta_de]
        }
    });

    return xtSelector(
        nh.extend({
            data : cuentas,
            text : function (d) { 
                return String(f('cuenta')(d)).concat(' ',f('nombre')(d)); 
            },
            enter : function (selection) {
                selection.html('');
                selection.append('div').text( f('cuenta') );

                selection.append('div').attr('class','opt-nombre-cuenta')
                    .html(function (d) {
                        var s = '';
                        //if(d.padre) s = s.concat('<small class="text-grey">', d.padre.nombre ,'</small> &raquo; ')
                        return  s.concat('<small>', d.nombre || '', '</small>');
                    });
            },
            value : f('cuenta_contable_id')
        }, params)
    );
}