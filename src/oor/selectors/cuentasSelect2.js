module.exports = cuentasSelect2;

var CuentaForm = require('../../ajustes/cuentas-contables/CuentaForm');
 var add = '::ADD::'
/**
 * return CuentasSelector
 */
function cuentasSelect2(iParams) {


   
    var cuentas = m.prop();
    var lCuentas = buildData(iParams);
    var cuentasMap = lCuentas.map;

    cuentas(lCuentas.cuentas);

    var selector = oor.select2(
        _.extend({
            data : cuentas,
            find : function (id) {
                return iParams.cuentas.filter(function (c) {
                    return c.cuenta_contable_id == id;
                })[0];
            },
            templateResult : formatCuenta(true),
            templateSelection : formatCuenta()
        }, iParams, {
            onchange : function (d) {
                if(d == add) {

                    CuentaForm.openModal.call(selector.el()[0],{
                        nombre : 'Crear Cuenta',
                        onsave : function (rCuenta) {
                            var cuenta = JSON.parse(JSON.stringify(rCuenta));
                            cuenta.acumulativa = '0';

                            iParams.cuentas.push(cuenta);

                            var lCuentas = buildData(iParams);
                            cuentasMap = lCuentas.map;

                            cuentas(lCuentas.cuentas);

                            selector.rebuild();
                            iParams.model(cuenta.cuenta_contable_id);
                            m.redraw();
                        }
                    });

                    return;

                } else if(nh.isFunction (iParams.onchange)) {
                    iParams.onchange(d);
                }
            }
        })
    );

    return selector;


    function formatCuenta (detalle) {
        return function  (item) {
            var cuenta = cuentasMap[item.id || item.cta_id];
            var cuentaPadre, nombreCuenta;
            cuenta || ( cuenta = {} );

            if(cuenta.subcuenta_de) {
                cuentaPadre = cuentasMap[cuenta.subcuenta_de];
            }

            nombreCuenta = '<div class="cuenta-nombre">';

            if(cuentaPadre) {
                nombreCuenta += '<small>' +cuentaPadre.nombre  + '</small>' + ' &raquo; '
            }

            nombreCuenta += '<strong>' + cuenta.nombre + '</strong>';
            nombreCuenta += '</div>';

            return $('<div class="cuenta-display ' + (item.id && detalle ? 'detalle' : 'acumulativa') + '"><div class="cuenta-cuenta">' + cuenta.cuenta + '</div>' + nombreCuenta + '</div>');
        }
    }
}




function buildData (iParams) {
    var cuentasMap = {};
    var incluirCta = {};
    var agregarCta = (typeof iParams.agregarCta  == 'undefined') ? true : iParams.agregarCta;

    
    iParams.cuentas.filter(function (cta) {
        return cta.acumulativa == '0';
    }).forEach(function (cta) {
        incluirCta[cta.cuenta_contable_id] = true;
        incluirCta[cta.subcuenta_de] = true;
    });

    iParams.cuentas.forEach(function (c) {
        cuentasMap[c.cuenta_contable_id] = c;
    });

    var lCuentas = iParams.cuentas.filter(function (cta) {
            return incluirCta[cta.cuenta_contable_id];
        })
        .map(function (cta) {
            var id = cta.cuenta_contable_id;

            if(cta.acumulativa == '0'){
                return {id:id, text:cta.cuenta + ' ' + cta.nombre};
            } else {
                return {cta_id :id, text:cta.cuenta + ' ' + cta.nombre};
            }
        });

    if(agregarCta) {

        lCuentas.unshift({
           text : 'Agregar Nueva Cuenta',
           id : add
        });

        cuentasMap[add] = {
            nombre:'', 
            cuenta: 'Agregar Cuenta...'
        };
    }


    return {
        cuentas : lCuentas,
        map : cuentasMap
    };

}
