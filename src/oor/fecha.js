module.exports = fecha;

function fecha () {
    var labelMeses = ['0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov','Dic'];
    var fullLabelMeses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    var now = m.prop(new Date);

    prettyFormat.year = function (str) {
        return prettyFormat(str, true);
    }

    return {
        fromSQL : desdeSQL,
        desdeSQL : desdeSQL,
        toSQL : toSQL,
        inicialMes : inicialMes,
        finalMes : finalMes,
        inicialAno : inicialAno,
        finalAno : finalAno,
        str : str,
        format : nh.memoize(format),
        prettyFormat : prettyFormat,

        to10 : to10,
        labelMes : labelMes,
        fullLabelMes : fullLabelMes,
        validate : validate,
        validateSQL : validateSQL,

        meses : function () { return fullLabelMeses }
    };


    function inicialMes (fecha) {
        var inicial = new Date(fecha);
        inicial.setDate(1);
        return inicial;
    }

    function finalMes (fecha) {
        var ffinal = new Date(fecha);
        ffinal.setMonth(ffinal.getMonth() + 1);
        ffinal.setDate(0);
        return ffinal;
    }

    function inicialAno (fecha) {
        var i = new Date(fecha);
        i.setMonth(0);
        i.setDate(1);
        return i;
    }

    function finalAno (fecha) {
        var f = new Date(fecha);
        f.setMonth(11);
        f.setDate(31);
        return f;
    }

    function desdeSQL (str) {
        var d = str.split('-');
        return new Date(to10(d[1]) + '/' + to10(d[2]) + '/' + d[0]);
    }

    function format (str) {
        if(!str){
            return '-';
        }
        var d = new Date(str);
        return d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
    }

    function prettyFormat (str, forceYear) {
        var info = str.split('-');
        var year = ' ' + info[0];

        if(year == now().getFullYear() && !forceYear) {
            year = '';
        }
        return to10(Number(info[2])).concat(' ', labelMes(info[1]),  year);
    }


    function toSQL (date) {
        return date.getFullYear() + '-' + to10(date.getMonth() + 1) + '-' + to10(date.getDate());
    }

    function to10 (n) {
        if(n < 10) return '0'.concat(n);
        return String(n);
    }

    function str (d) {
        return toSQL(d) + ' ' + to10(d.getHours()) + ':' + to10(d.getMinutes()) + ':' + to10(d.getSeconds());
    }

    function labelMes (d) {
        return labelMeses[Number(d)];
    }

    function fullLabelMes (d) {
        return fullLabelMeses[Number(d)];
    }

    function validate (d) {
        var data = String(d).match(/^\d{2}-\d{2}-\d{4}$/);
        return (data && data.length) ? true: false;
    }

    function validateSQL (d) {
        var data = String(d).match(/^\d{4}-\d{2}-\d{2}$/);
        return (data && data.length) ? true: false;
    }
};
