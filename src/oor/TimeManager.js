module.exports = timeManager;


function timeManager () {
    var attr = 'x-time-till-now';

    var getNow =  _.throttle(function () { return new Date }, 5000);

    var getTime = _.memoize(createTime);

    function getTextDiff(ref, time) {
        var diff = Math.floor((ref - time)/ 1000);
        var k = 1;
        var l = 60;

        if(diff < 10) {
            return 'ahora';
        } else if(diff < 60) {
            return '1 min.';
        } else if(diff < 60 * 60) {
            return Math.floor(diff/60) + ' mins';
        } else if(diff < 60 * 60 * 24) {
            return Math.floor(diff/(60 * 60)) + ' hrs';
        }

        return Math.floor(diff/(60 * 60 * 24)) + ' días';
    }

    setInterval(function () { update(document.body); },10000);

    function update (node, isInited) {
        if(arguments.length > 1 && isInited == true) return;
        var nodes = node.querySelectorAll('[' + attr + ']');
        [].forEach.call(nodes, updateNode);
    }

    function updateNode (node) {
        var time = getTime(node.getAttribute(attr));
        node.innerHTML = getTextDiff(getNow(),time);
    }

    function createTime (t) {
        return new Date(t);
    }

    return  {update : update}
}
