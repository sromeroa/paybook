module.exports = oorden;
/*
 * Este módulo maneja datos de OORden de la organizacion
 */
var promise;
var metodosSalvables = ['cheque','transferencia', 'tarjeta de credito'];

oorden.initialize = initialize;
oorden.organizacion  = m.prop();
oorden.org = oorden.organizacion;

oorden.organizacion.unformat = function (v) {
    v = Number(v.split(',').join(''))
    return oorden.organizacion.round(v);
}

oorden.organizacion.factor = function () {
    return Math.pow(10, oorden.organizacion().numero_decimales);
}

oorden.organizacion.format = function (aNumber) {
    return oorden.organizacion.numberFormat()(aNumber)
}

oorden.organizacion.numberFormat = function () {
    if(!oorden.organizacion() ) {
        throw 'OORDEN data no cargada'
    }
    return d3.format(',.'.concat(oorden.organizacion().numero_decimales || 2, 'f'));
}



oorden.organizacion.round = function (number) {
    if(!oorden.organizacion() ) {
        throw 'OORDEN data no cargada'
    }
    var factor = oorden.organizacion.factor();
    return Math.round(number * factor) / factor;
}


oorden.ready = function (callback) {
    oorden.ready.callbacks.push(callback)
}
oorden.ready.callbacks = [];

function ready () {
    oorden.ready.callbacks.forEach(function (callback) {
        callback();
    })
}

oorden.sucursal      = m.prop();
oorden.usuario       = m.prop();

oorden.cuentas       = m.prop();
oorden.cctos         = m.prop();
oorden.tiposDeCambio = m.prop();
oorden.impuestos     = m.prop();
oorden.retenciones   = m.prop();
oorden.metodosDePago = m.prop();
oorden.tiposDeDocumento = m.prop();

oorden.paises = m.prop();
oorden.monedas = m.prop();
oorden.vendedores = m.prop();

var paisesYMonedas;
var vendedores;

oorden.monedasDisponibles = function () {
    var codigosMoneda = {};

    oorden.tiposDeCambio().forEach(function (tc) {
        codigosMoneda[tc.codigo_moneda] = true;
    });

    return oorden.monedas().filter(function (mon) {
        return codigosMoneda[mon.codigo_moneda] || false;
    }).map(function (mon) {
        return {
            id: mon.codigo_moneda ,
            text : mon.codigo_moneda + ' ' + mon.nombre
        }
    });
}


oorden.paisesYMonedas = function () {
    if(paisesYMonedas) return paisesYMonedas;

    paisesYMonedas = m.request({
        method : 'GET',
        url : '/api/paises'
    }).then(function (d) {
        oorden.paises(d.data.paises);
        oorden.monedas(d.data.monedas);
    });

    return paisesYMonedas
}


oorden.getVendedores = function () {
    if(vendedores) return vendedores;

    vendedores = m.request({
        method : 'GET',
        url : '/organizacion/vendedores',
        unwrapSuccess : f('data')
    }).then(oorden.vendedores);

    return vendedores;
}

function oorden () {
    return oorden.initialize();
}

function getPromise () {
    return promise;
}

/**
 * Obtiene la ayuda para los tooltips
 * @param string ayuda
 */
oorden.obtenerAyuda = function (ayuda) {
    /*
    return m.request({
        method : 'GET',
        url : '/api/ayuda/' + ayuda,
        unwrapSuccess : function (resultado) {
            var valor = resultado.data[Object.keys(resultado.data)[0]];
            if (valor == false) return "Ayuda no encontrada";
            return resultado.data[Object.keys(resultado.data)[0]];
        }
    });
    */
};



/**
 * La vista crea un node con <div data-oorden-org=""> y los datos de la organizacion incrustados
 */

function fastInit () {
    var data = document.querySelector('[data-oorden-org]');
    data = JSON.parse(data.getAttribute('data-oorden-org'));
    oorden.organizacion(data);
    ready();
}


/**
 * Obtiene los datos cacheados del localstorage, cada 5 min se consultan
 * Con el fin de reducir las llamadas a esta función
 */

function getData () {
    var id = document.querySelector('html').getAttribute('x-oorden-org');
    var metakey = 'OORDEN/data/' + id, time, key, datos;
    var def = m.deferred();

    now = new Date();
    now = Math.floor(now.getTime() / 1000);

    if(time = localStorage.getItem(metakey)) {
        //Si la key es vieja elimino todos los caches
        if((now - time) > 300) {
            Object.keys(localStorage).forEach(function (k) {
                if(k.indexOf('OORDEN/data/') == 0 ) {
                    localStorage.removeItem(k)
                }
            });
            time = false
        }
    }

    if(time && (datos = localStorage.getItem('OORDEN/data/' + id + '/' + time))) {
        datos = JSON.parse(datos);
        def.resolve(datos);
    }

    if(!datos) {
        m.request({method:'GET', url:'/organizacion/config'})
            .then(function (d) {
                localStorage.setItem(metakey, now);
                localStorage.setItem('OORDEN/data/' + id + '/' + now, JSON.stringify(d));
                def.resolve(d);
            });
    }

    return def.promise;
}



function initialize () {
    promise = getData().then(setData);
    oorden.initialize = getPromise;
    return promise;
}


function setData(data) {

   oorden.organizacion(data.organizacion);
   oorden.sucursal(data.sucursal);
   oorden.usuario(data.usuario);

    //CUENTAS
    oorden.cuentas( data.cuentasContables );
    oorden.cuenta = makeGetter(oorden.cuentas(), 'cuenta_contable_id');



    //CENTROS DE COSTO
    oorden.cctos(data.centrosDeCosto);
    oorden.cctos.activos = m.prop(
        data.centrosDeCosto.filter(function (ccto) {
            return Boolean(Number(ccto.activo));
        })
    );
    oorden.ccto = makeGetter(oorden.cctos(), 'centro_de_costo_id');

    oorden.buscarCcto = (function () {
        var ccto_elementos = {};
        oorden.cctos().forEach(function (ccto) {
            ccto.elementos.forEach(function (elem) {
                ccto_elementos[elem.elemento_ccosto_id] = ccto;
            });
        });
        return function (elementoId) {
            return ccto_elementos[elementoId];
        }
    }());


    oorden.obtenerParametroPorNombre = function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    //Tipos De Cambio
    oorden.tiposDeCambio(data.tiposDeCambio);
    oorden.tipoDeCambio = makeGetter(oorden.tiposDeCambio(), 'tipo_de_cambio');
    group(oorden.tiposDeCambio, oorden.tiposDeCambio(), 'codigo_moneda');
    oorden.tiposDeCambio().forEach(function (tdc) {
        if(tdc.base == "1") {
            oorden.tipoDeCambio.base = m.prop(tdc);
        }
    });


    //Tipos De Documento
    oorden.tiposDeDocumento(data.tiposDeDocumento);
    oorden.tipoDeDocumento = makeGetter(data.tiposDeDocumento, 'tipo_documento_id');
    oorden.tipodeDocumento = oorden.tipoDeDocumento;
    group(oorden.tiposDeDocumento, oorden.tiposDeDocumento(), 'tipo_operacion');


    //Impuestos
    oorden.impuestos(data.impuestos);
    var impuestoGetter = makeGetter(oorden.impuestos(), 'impuesto_conjunto_id');
    oorden.impuesto = impuestoGetter;

    //Retenciones
    oorden.retenciones(data.retenciones);
    oorden.retencion = makeGetter(oorden.retenciones(),'retencion_conjunto_id');


    //Metodos de Pago
    oorden.metodosDePago(data.metodosDePago)
    oorden.metodoDePago = makeGetter(oorden.metodosDePago(), 'id');
    delete(oorden.metodosDePago.toJSON);

    oorden.metodosDePago().forEach(function (mp) {
        var metodo = mp.metodo.toLowerCase();
        if(metodosSalvables.indexOf(metodo) >= 0) {
            oorden.metodosDePago[metodo] = m.prop(mp);
        }
    });
}

function makeGetter(data, key) {
    var map = {};
    data.forEach(function (d) { map[d[key]] = d; });
    return function (val) {
        return map[val];
    }
}

function group (holder, data, key) {
    data.forEach(function (d) {
        var keyVal = d[key];
        var groupProp = holder[keyVal];

        if(!groupProp) {
            holder[keyVal] = groupProp = m.prop([]);
        }

        groupProp().push(d);
    });
}

document.addEventListener('DOMContentLoaded', fastInit);
