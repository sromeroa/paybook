var Field = require('./nahui/nahui.field');

/**
 * Modelo de Poliza
 */
function Poliza (data) {
    var Partida = oor.mm('Partida');

    data || (data = {});
    Poliza.initializeInstance.call(this,data);

    var poliza = this;

    this.$errors = m.prop([]);
    this.$partidasErroneas = m.prop([]);
    this.eliminar([]);

    this.manual( Boolean(Number(this.manual())) );

    this.total_debe  = Modelo.numberProp(m.prop(data.total_m_base_debe),2);
    this.total_haber = Modelo.numberProp(m.prop(data.total_m_base_haber),2);

    this.total_m_base_debe = this.total_debe;
    this.total_m_base_haber= this.total_haber;

    this.total       = this.total_debe;
    this.diferencia  = Modelo.numberProp(m.prop(0), 2);


    this.ccto1 = m.prop();
    this.ccto2 = m.prop();

    var ccto1, ccto2;

    var partidas = (data.partidas || [])
        .map(function (partida) {
            ccto1 = ccto1 || partida.ccto_1_id;
            ccto2 = ccto2 || partida.ccto_2_id;

            return partida;
        })
        .sort(function (p1, p2) {
            return Number(p1.posicion_partida) - Number(p2.posicion_partida)
        })
        .map(function (p) {
            var partida = new Partida(p);
            poliza.extend(partida);
            return partida;
        });


    this.ccto1( oorden.buscarCcto(ccto1) );
    this.ccto2( oorden.buscarCcto(ccto2) );


    this.partidas(partidas);

    this.partidas.toJSON = function () {
        return this.partidas().filter(function (partida) {
            return partida.$integrable();
        });
    }.bind(this);

    this.$new = m.prop();


    poliza.poliza = function () {
        var tipo = Poliza.tipos[poliza.tipo()];
        tipo = tipo ? tipo() :{};
        return tipo.text + ' #' +poliza.numero();
    }
}


Poliza.include = function () {
    return 'poliza.partidas';
};

//ESTATUSES
Poliza.estatuses = m.prop([
    { estatus : 'P', nombre : 'En Preparacion', color : 'blue', nombre_corto: 'PREP'},
    { estatus : 'T', nombre : 'Por Aplicar', color : 'amber', nombre_corto: 'PDTE'},
    { estatus : 'A', nombre : 'Aplicada', color : 'green',  nombre_corto : 'APLI'},
    { estatus : 'X', nombre : 'Eliminada', color : 'red', nombre_corto : 'ELIM'},
]);

Poliza.estatuses().forEach(function (t) {
    Poliza.estatuses[t.estatus] = m.prop(t);
});


Poliza.url = function (item) {
    return '/polizas/poliza/' + f('poliza_id')(item); 
}

Poliza.estatus = function (poliza, estatus) {
    var url = '/api/poliza/estado_poliza/' + poliza.poliza_id() + '/' + estatus;
    return m.request({
        method : 'POST',
        url : url
    });
};

Poliza.eliminar = function (poliza) {
    return m.request({
        method : 'POST',
        url : '/api/poliza/eliminar/' + poliza.poliza_id()
    });
};

//TIPOS
Poliza.tipos = m.prop([
    {id:'D', text:'Diario'},
    {id:'E', text:'Egreso'},
    {id:'I', text:'Ingreso'}
]);

Poliza.tipos().forEach(function (t) {
    Poliza.tipos[t.id] = m.prop(t);
});


Poliza.search = function (p) {
    return f('poliza')(p) + ' ' + f('concepto')(p);
}



Poliza.fields = function () {
    var nFormat = d3.format(",.2f");
    var fields = {};

    var currency = d3.format(',.2f');

    function numberFormat (d) {
        return nFormat(d || 0);
    }

    numberFormat.hideZero = function (val) {
        if(val == 0) return '--';
        return numberFormat(val);
    }

    fields.fecha = Field('fecha',{
        caption:'Fecha'
    });

    fields.periodoContable = Field('periodoContable', {
        caption : 'Periodo Contable',
        value : function (item) {
            return f('ano_contable')(item) + ' \u2014 ' + f('mes_contable')(item);
        },
        'class' : 'text-grey'
    });

    fields.fechaYPeriodo = Field.twice('fechaYPeriodo', {
        subfields : [fields.fecha, fields.periodoContable]
    });

    fields.manual = Field('manual', {
        caption : ' ',
        sortable : false,
        enter : function (selection) {
            selection
                .classed({'pull-left' : true})
                    .append('i')
                    .attr('class', 'ion-android-hand text-indigo')
                    .style('margin-right', '4px')
        },
        update : function (selection) {
            selection.style('display', function (d) {
                return f('value')(d) == '1' ? 'block' : 'none'
            })

        }
    });

    fields.poliza = Field.linkToResource('poliza', {
        caption : 'Póliza',
        url : Poliza.url,
        'class' :'text-indigo'
    });

    fields.concepto = Field('concepto',{
        caption:'Concepto',
        'class' : 'text-grey'
    });

    fields.fecha_esperada = Field('fecha_esperada', {
        caption:'Esp.'
    });

    fields.Poliza = Field.twice('Poliza',{
        subfields : [fields.manual, fields.poliza, fields.concepto]
    });

    fields.totalDebe = Field('totalDebe',{
        caption : 'Total Debe',
        value : f('total_m_base_debe'),
        filter: numberFormat,
        'class' : 'text-right'
    })

    fields.totalHaber = Field('totalHaber',{
        caption : 'Total Haber',
        value : f('total_m_base_haber'),
        filter: numberFormat,
        'class' : 'text-right'
    })

    fields.estatus = Field('estatus', {
        'class':'text-center',
        caption : 'Estatus',
        filter :  function (st) {
            return Poliza.estatuses[st]().nombre_corto;
        },
        update : function (selection) {
            selection.call(Field.update);

            Poliza.estatuses().forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) {
                    return d.value == st.estatus;
                });
            });
        }
    });



            
    return fields;
}

oor.mm('Poliza', Poliza)
    .serviceName('polizas')
    .idKey('poliza_id')

    .prop('poliza_id', {})
    .prop('organizacion_id', {})
    .prop('sucursal_id', {})

    .prop('fecha', {})
    .prop('ano_contable',{})
    .prop('mes_contable',{})

    .prop('tipo', {})
    .prop('numero', {})
    .prop('manual', {})
    .prop('concepto', {})
    .prop('referencia', {})
    .prop('total', {})
    .prop('total_debe', {})
    .prop('total_haber', {})
    .prop('total_m_base_debe', {})
    .prop('total_m_base_haber', {})

    .prop('tipo_de_cambio', {})
    .prop('tasa_de_cambio', {})
    .prop('moneda', {})

    .prop('operacion_id', {})
    .prop('estatus', {})
    .prop('notas', {})

    .prop('partidas', {})
    .prop('eliminar', {})
    .prop('created_at', {})
    .prop('updated_at', {})


    .method('actualizar', function () {
        var totales = {'1' : 0, '-1' : 0};

        this.partidas().forEach(function (partida) {
            totales[partida.debe_o_haber()] += partida.importe.$int();
        });

        this.total_debe.$int( totales['1'] );
        this.total_haber.$int( totales['-1']);
        this.diferencia.$int( Math.abs(totales['1'] - totales['-1']) );
    })
    .method('asignarFecha', function (d) {
        if(!d) d = oor.fecha.toSQL(new Date);
        var elements = d.split('-');


        this.fecha(d);
        this.ano_contable(elements[0]);
        this.mes_contable(elements[1]);
    })

    .method('extend', function (partida) {
        partida.estatus = this.estatus;

        partida.fecha   = this.fecha;
        partida.ano_contable = this.ano_contable;
        partida.mes_contable = this.mes_contable;

        partida.tasa_de_cambio = this.tasa_de_cambio;
        partida.tipo_de_cambio = this.tipo_de_cambio;
        partida.moneda = this.moneda;
    })
    .method('editUrl', function () {
        return '/v1/api/polizas/editar/' + this.poliza_id();
    })
    .method('addError', function (err) {
        if(this.$errors().indexOf(err) == -1) {
            this.$errors().push(err);
        }
    })
    .method('remError', function (err) {
        var idx = this.$errors().indexOf(err);
        if(idx > -1) {
            this.$errors().splice(idx,1);
        }
    })
    .method('hasError', function (err) {
        return this.$errors().indexOf(err) > -1;
    })
    .method('periodoContable', function () {
        return this.ano_contable() + ' ' + this.mes_contable()
    })
    .method('url', function () {
        return '/polizas/poliza/' + this.poliza_id();
    })
