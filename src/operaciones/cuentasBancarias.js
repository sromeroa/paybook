var cuentasBancarias = module.exports = {};

var dynTable = require('../nahui/dtable');
var Field = require('../nahui/nahui.field');

cuentasBancarias.controller = function (params) {
    var bancos = m.prop(params.bancos);
    var ctx = this;
    var table = dynTable().key(f('cuenta_contable_id'));

    var actualizarTabla = m.prop(false);

    var fields = {};
    var format = d3.format(',.2f');

    fields.banco = Field('banco');
    fields.cuentaBancaria = Field('cuentaBancaria', {
        caption : 'Cta. Bancaria',
        value : function (item) {
            return f('banco')(item) + ' - ' + f('cuenta_bancaria')(item);
        }
    });

    fields.nombre = Field.linkToResource('nombre', {
        caption : 'Nombre',
        url : function (d) {
            return '/bancos/ver/' + f('cuenta_contable_id')(d);
        },
        'class' : 'text-indigo'
    });


    fields.ctaBanco = Field.twice('ctaBanco', {
        subfields : [fields.nombre,fields.cuentaBancaria]
    });


    fields.moneda = Field('moneda', {

        'class' : 'text-right text-grey'
    });

    fields.nombreCta = Field.twice('nombreCta', {
        subfields : [fields.nombre, fields.moneda]
    })

    fields.saldoInicial = Field('saldo_inicial', {
        caption : 'Inicial mes',
        'class' : 'text-right',
        filter : format
    });
    fields.saldo_final = Field('saldo_final', {
        caption : 'Saldo Actual',
        'class' : 'text-right',
        filter : format
    });

    fields.saldoFinal = Field.twice('saldoFinal', {
        subfields : [fields.saldo_final, fields.moneda],

    })

    fields.ingresos = Field('ingresos', {
        caption : 'Ingresos',
        'class' : 'text-right',
        filter : format
    });

    fields.egresos = Field('egresos', {
        caption : 'Egresos',
        filter:format,
        'class' : 'text-right'
    });


    fields.saldoBanco = Field('saldo_banco', {
        caption : 'Saldo según Banco',
        filter : format,
        value : function () {return 0},
        'class' : 'text-right'
    });

    fields.ultima_conciliacion = Field('ultima_conciliacion', {
        caption :' Última Conciliación',
        filter :function () {
            var d = new Date()
            return oor.fecha.toSQL(d);
        },
        'class' : 'text-right'
    });

    fields.conciliacion = Field.twice('conciliacion', {
        subfields : [fields.saldoBanco, fields.ultima_conciliacion]
    })

    ctx.columnas = m.prop([
        fields.ctaBanco,
        fields.saldoInicial,
        fields.ingresos,
        fields.egresos,
        fields.saldoFinal,
        fields.conciliacion
    ]);

    ctx.setupTable = function (element, isInitialized) {
        if(isInitialized && (actualizarTabla() === false)) return;

        d3.select(element).call(table, ctx.columnas(), bancos());
        actualizarTabla(false);
    }

}

cuentasBancarias.view = function (ctx) {
    return m('div',[
        m('table.table', {config : ctx.setupTable})
    ])
}
