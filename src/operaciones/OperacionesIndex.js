var OperacionIndex = module.exports = {};
var FechasMultiSelector = require('../misc/components/FechasMultiSelector.js');
var dynTable = require('../nahui/dtable');
var TableHelper = require('../nahui/dtableHelper');
var nahuiCollection = require('../nahui/nahui.collection');

var SearchBar = require('../misc/components/SearchBar');
var FechasBar = require('../misc/components/FechasBar');
var tableCheckers = require('../misc/utils/tableCheckers');

var gParams = {
    V : { nombreTercero : 'Cliente' },
    C : { nombreTercero : 'Proveedor'}
};

OperacionIndex.tabs = m.prop([
    {
        caption : 'Todas',
        badge : false,
        query : {estatus : {$in : ['A', 'S', 'P', 'T']}}
    },
    {
        caption : 'En Preparación',
        badge : 'P',
        query : {estatus : 'P'}
    },
    {
        caption : 'Por Autorizar',
        badge : 'T',
        query : {estatus : 'T'}
    },
    {
        caption : 'Aplicadas',
        badge : false,
        query : {estatus : {$in : ['A','S']}}
    },
    {
        caption :'Con Saldo',
        badge : 'A',
        query : {estatus : 'A'}
    },
    {
        caption : 'Canceladas',
        badge : 'X',
        query : {estatus : 'X'}
    }
]);


function AplicarOperacionesController (params) {
    var ctx = this;

    ctx.seleccion = m.prop();
    ctx.operacionesCorrectas = m.prop();
    ctx.operacionesErroneas = m.prop();

    ctx.ready = m.prop(false)
    ctx.ready.inited = m.prop(false);



    ctx.cargarDatos = function () {
        if(ctx.ready.inited()) return;

        ctx.ready.inited(true);
        ctx.ready(false);

        oor.request('/operaciones/accion/aplicar', 'POST', {data:{operaciones:params.operaciones}})
            .then(asignarOperaciones)
            .then(ctx.ready.bind(null, true))
            .then(ctx.seleccion.bind(null, {}))
    }


    ctx.ejecutarAccion = function () {
        var operaciones;
        if(ctx.ejecutarAccion.busy()) return;

        operaciones = Object.keys(ctx.seleccion());
        ctx.ejecutarAccion.busy(true);

        oor.request('/operaciones/accion/aplicar/ejecutar', 'POST', {data:{operaciones:operaciones}})
            .then(asignarResultados)
    }


    function asignarResultados(resultados) {
        resultados.forEach(function (res) {
            console.log(res);

            var info = ctx.operacionesCorrectas().filter(function (i) {
                return i.operacion.operacion_id == res.operacion.operacion_id;
            })[0];

            info.resultado = res.accion;
        })
    }

    ctx.ejecutarAccion.busy = m.prop(false);

    function asignarOperaciones (operaciones) {
        var operacionesCorrectas = operaciones.filter(function (info) { return info.accion.puede });
        var operacionesError = operaciones.filter(function (info) { return !info.accion.puede });

        ctx.operacionesCorrectas(operacionesCorrectas);
        ctx.operacionesErroneas(operacionesError);
    }
}

function AplicarOperacionesView (ctx) {
    ctx.cargarDatos();
    return ctx.ready() ? AplicarOperacionesOperaciones(ctx) : oor.loading()
}
 

function AplicarOperacionesOperaciones (ctx) {
    var seleccion = ctx.seleccion();

    return m('div',[
        ctx.operacionesErroneas().length ? m('.alert.alert-warning', [
            ctx.operacionesErroneas().length, ' Operaciones Seleccionadas No se podrán aplicar Aplicar'
        ]) : '',

        m('h5', 'Operaciones Para Aplicar'),
        
        ctx.operacionesCorrectas().map(function (info) {
            var selProp;

            if(!seleccion[info.operacion.operacion_id]){
                seleccion[info.operacion.operacion_id] = m.prop(true);
            }

            selProp = seleccion[info.operacion.operacion_id];

            return m('div.thmb-operacion', [
                m('label', [

                    m('input[type=checkbox]', {
                        checked : selProp(),
                        onchange : m.withAttr('checked', selProp)
                    }), ' ',

                ]),

                info.resultado ? m('div.pull-right', [
                    info.resultado.result ? m('i.ion-checkmark-round.text-green') : m('i'),
                    info.resultado.message ? m('div.small', info.resultado.message) : ''
                ]) : null,

                info.operacion.documento , ' de ', info.operacion.nombre_tercero
            ])
        })
    ]);
}

function AplicarOperaciones (ctx, ids) {
    MTModal.open({
        args : {
            operaciones : ids
        },
        controller : AplicarOperacionesController,
        content : AplicarOperacionesView,
        top : m('h4', 'Aplicar Operaciones'),
        bottom : function (ctx) {
            return ctx.ready() ? [
                m('button.btn.btn-flat.btn-sm.btn-success.pull-right', {
                    onclick : ctx.ejecutarAccion
                }, 'Aplicar ', ctx.operacionesCorrectas().length , ' Operaciones'),

                m('button.btn.btn-flat.btn-sm.btn-default.pull-left', {
                    onclick : ctx.$modal.close
                }, 'Cancelar')
            ] : null
        },
        modalAttrs : {
            'class' : 'modal-medium'
        },
        el:this
    })
}



function MountActions (ctx){
    ctx.actions = {
        pagar : pagar,
        eliminar : eliminar,
        finalizar : finalizar,
        aplicar : aplicar,
    };

    function aplicar () {
        var seleccionados = ctx.seleccionados();
        var ids = Object.keys(seleccionados).filter(function (key) {
            return seleccionados[key];
        });

        if(!ids.length) return toastr.warning('¡Seleccione Al menos una Operación!');

        AplicarOperaciones.call(this,ctx,ids);
    }

    function finalizar () {
        toastr.warning('Finalizar')
    }

    function pagar () {
        var seleccionados = ctx.seleccionados();
        var ids = Object.keys(seleccionados).filter(function (key) {
            return seleccionados[key];
        });

        console.log(ctx);

        if(!ids.length) return toastr.warning('¡Seleccione Al menos una Operación!');

        var data = {
            redirect_url : location.pathname,
            movimientos : ids.join(',')
        }

        var url = '/movimientos/'.concat( ctx.pago() , '?', m.route.buildQueryString(data) );
        
        window.location = url;
    }

    function eliminar () {
        toastr.warning('No Disponible')
    }
}


OperacionIndex.controller = function (args) {
    var ctx = this;
    MountActions(ctx);
    var Operacion = oor.mm('Operacion');
    var fields =  Operacion.fields();
    var thelper, table, coll, query, searchBar, fechasBar, params, checkers;

    ctx.seccion = m.prop(args.seccion);
    ctx.pago = m.prop(ctx.seccion() == 'V' ? 'ingreso' : 'egreso');

    table = dynTable()
                .key( f('operacion_id') )
                .rowUpdate(function (selection) {
                    selection.style('background-color', function (r) { 
                        return seleccionados[f('operacion_id')(r)] ? '#cef' : undefined;
                    });
                })

    coll = nahuiCollection('operaciones', {
        identifier : 'operacion_id'
    });

    query = coll.query({});

    params = gParams[ ctx.seccion() ];

    fields.nombre_tercero.caption = params.nombreTercero;

    var seleccionados = {};

    ctx.seleccionados = m.prop(seleccionados);
    
    checkers = tableCheckers({
        name : 'checker',
        id : f('operacion_id'),
        value : function (d) {
            return seleccionados[f('operacion_id')(d)];
        },
        change : function (item, value) {
            if(value) {
                seleccionados[f('operacion_id')(item)] = true;
            } else {
                seleccionados[f('operacion_id')(item)] = false;
            }
        }
    })

    

    ctx.columnas =  m.prop([
        checkers,
        fields.fecha,
        fields.doc_ref,
        fields.infoVta,
        fields.total_moneda,
        fields.saldoYEstatus,
        //fields.estatus
    ]);


    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 25 })


    checkers.tableHelper(thelper)

    ctx.search = m.prop('');

    searchBar = new SearchBar.controller({
        search : ctx.search,
        onsearch : function () {
            var search = ctx.search() ? {$contains:ctx.search()} : undefined;
            query.add({ search: search }).exec();
        }
    });


    fechasBar = new FechasBar.controller({
        onchange : function (val) {
            query.add({ fecha : val.fecha }).exec();
        }
    })

    ctx.searchBar = searchBar;
    ctx.fechasBar = fechasBar;

    ctx.setupTable = function (element, isInitialized) {
        if(!isInitialized) thelper.element(element);
    }

    ctx.tab = m.prop();

    ctx.selectTab = function (tab, update) {
        ctx.tab(tab);
        query.add(tab.query);

        if(update === false) return;
        query.exec();
    }


    ctx.selectTab( OperacionIndex.tabs()[0], false );
    loadItems();

    function loadItems () {
        oorden().then(function () {
            m.request({
                url:'/apiv2',
                method:'GET',
                data : {
                    modelo : 'operaciones',
                    seccion : ctx.seccion(),
                    'include' : 'operaciones.vendedor,operaciones.sucursal'
                }
            }).then(function (r) {

                r.data.forEach(function (item) {
                    item.search = Operacion.search(item);
                    coll.insert(item);
                });

                query.exec();
            });
        });
    }
};




OperacionIndex.view = function (ctx) {
    return m('div');
    return m('div', [

        m('div.row', [
            m('div.col-sm-4', SearchBar.view(ctx.searchBar)),
            m('div.col-sm-8', FechasBar.view(ctx.fechasBar))
        ]),

        m('.clear'),
        m('br'),

        m('.row', [
            m('.col-sm-12', [
                m('button.btn.btn-default.btn-xs', {onclick:ctx.actions.aplicar}, m('i.ion-checkmark-round'), ' Aplicar'),
                m('button.btn.btn-default.btn-xs', {onclick:ctx.actions.finalizar}, m('i.ion-checkmark-round'), ' Finalizar'),
                m('button.btn.btn-default.btn-xs', {onclick:ctx.actions.eliminar}, m('i.ion-ios-trash'), ' Eliminar'),

                m('.btn-group', [
                    m('button.btn.btn-default.btn-xs', {
                        onclick:ctx.actions.pagar
                    }, m('i.ion-cash'), ' Pagar'),
                ])
                
            ])
        ]),

        m('.clear'),

        m('.table-responsive', {style:'margin-top:10px'}, [
            m('ul.nav.nav-tabs.with-panel', [
                OperacionIndex.tabs().map(function (tab) {
                    return m('li', {
                            'class' : tab == ctx.tab() ? 'active' : '',
                            onclick : ctx.selectTab.bind(ctx,tab)
                        }, [
                        m('a', {href:'javascript:;'}, tab.caption, ' ', tab.badge ? m('.badge.badge-sm', '') : null)
                    ]);
                })
            ]),


            m('table.table', {config:ctx.setupTable})
        ]),
        
        MTModal.view()
    ]);
};

