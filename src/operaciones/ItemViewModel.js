module.exports = OperacionItemViewModel;

function OperacionItemViewModel (item) {
    var opCtrl = this;
    var vm  = {
        enabled : this.vm.enabled
    };

    
    
    if(typeof opCtrl.cuentasSelector == 'function') {
        cuentas = opCtrl.cuentasSelector();
    } else {
        cuentas = oorden.cuentas().map(_.identity);
    }


    vm.cuentasSelector = oor.cuentasSelect2({
        cuentas : cuentas,
        model : item.cuenta_id,
        onchange : function (v) {
            item.asignarCuenta( vm.cuentasSelector.selected() );
        }
    });

    vm.impuestosSelector = oor.impuestosSelect2({
        impuestos : oorden.impuestos(),
        model : item.impuesto_conjunto_id,
        onchange : function (v) {
            item.asignarImpuesto( vm.impuestosSelector.selected() );
            opCtrl.operacion().actualizar();
        }
    });

    vm.retencionesSelector = oor.retencionesSelect2({
        retenciones : oorden.retenciones(),
        model : item.retencion_conjunto_id,
        onchange : function (v) {
            item.asignarRetencion( vm.retencionesSelector.selected() );
            opCtrl.operacion().actualizar();
        }
    });

    
    vm.cctosSelectors = opCtrl.centrosDeCosto().map(function (ccto, idx) {
        var name = 'ccto_'.concat(idx + 1);
        return oor.cctoSelect2({
            centroDeCosto : ccto,
            model : item[name.concat('_id')]
        });
    });



    vm.inputConcepto = function () {
        return m('input[type="text"]'.concat(vm.enabled() ? '' : '[disabled]') , {
            placeholder : 'Concepto', 
            value : item.producto_nombre(), 
            onchange : m.withAttr('value', function (v) {
                item.producto_nombre(v);
                opCtrl.operacion().actualizar();
            }),
            size : 25
        });
    };

    vm.inputCantidad = function () {
        return m('input.text-right[type="text"]'.concat(vm.enabled() ? '' : '[disabled]'), {
            placeholder : 'Cant.', 
            value : item.cantidad(), 
            onchange : m.withAttr('value', function (v) {
                item.cantidad.fix(v);
                opCtrl.operacion().actualizar();
            }),
            size : 12
        });
    };

    vm.inputUnidad = function () {
        return m('input.text-left[type="text"]'.concat(vm.enabled() ? '' : '[disabled]'), {
            placeholder :'Unid.', 
            value : item.unidad(), 
            onkeyup : m.withAttr('value', item.unidad),
            size : 12
        });
    };


    vm.inputPrecioUnitario = function () {
        return m('input.text-right[type="text"]'.concat(vm.enabled() ? '' : '[disabled]'), {
            placeholder : 'Precio unit.', 
            value : item.precio_unitario(), 
            onchange:m.withAttr('value', function (v) {
                console.log(v);
                
                item.precio_unitario.fix(v);
                opCtrl.operacion().actualizar();
            }),
            size : 14
        });
    };

    vm.inputCuentas = function () {
        return [
            m('div', [
                m('div.cuenta-display.detalle', {style:vm.enabled() ? 'display:none' : ''}, [
                    m('div.cuenta-cuenta', item.$cuenta() ? item.$cuenta().cuenta : '---'),
                    m('div.cuenta-nombre', item.$cuenta() ? item.$cuenta().nombre : '(no hay cuenta)')
                ]),
                m('div', {style:vm.enabled() ? '' : 'display:none'} , vm.cuentasSelector.view())
            ])
        ];
    };

    vm.inputImpuestos = function () {
         return [
            m('div', [
                m('div', {style:vm.enabled() ? 'display:none' : ''}, item.$impuesto() ? item.$impuesto().impuesto_conjunto : '(ninguno)'),
                m('div', {style:vm.enabled() ? '' : 'display:none'} , vm.impuestosSelector.view())
            ])
        ];
    }

    vm.inputRetenciones = function () {
        return [
            m('div', [
                m('div', {style:vm.enabled() ? 'display:none' : ''}, item.$retencion() ? item.$retencion().retencion_conjunto : '(ninguna)'),
                m('div', {style:vm.enabled() ? '' : 'display:none'}, vm.retencionesSelector.view())
            ])
        ];
    };


    vm.inputCcto1  = function () {
        return vm.cctosSelectors[0] ? vm.cctosSelectors[0].view() : '';
    };

    vm.inputCcto2  = function () {
        return vm.cctosSelectors[1] ? vm.cctosSelectors[1].view() : '';
    };


    return vm;
}

