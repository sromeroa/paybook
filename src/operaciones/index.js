var operaciones = module.exports = {
    OperacionComponent : require('./Componente.js'),
    PolizaComponent : require('../polizas/PolizaComponent.js'),
    PolizasIndex : require('../polizas/PolizasIndex.js'),
    index : require('./OperacionesIndex.js'),
    banco : require('./OperacionesBanco.js'),
    cuentasBancarias : require('./cuentasBancarias.js'),
    MainComponent : require('./MainComponent'),

    importarCSV : require('./importarCSV'),
    panelFacturacion : require('./facturacion/panelFacturacion')
};

operaciones.tercerosSelector = function (operacion) {
    var t3Selector = oor.tercerosSelect2({
        onchange : function (tercero_id) {
            console.log( t3Selector.selected() );
            operacion.asignarTercero( t3Selector.selected() );
        },
        model : operacion.tercero_id
    });
    return t3Selector;
};

operaciones.tipoDocSelector = function (operacion, tipos, ctrl) {
    tipos = tipos[operacion.tipo_operacion()]();

    var tdSelector = oor.tipoDocSelect2({
        tiposDoc : tipos,
        model : operacion.tipo_documento,
        onchange : function () {
            operacion.asignarTipoDeDocumento(tdSelector.selected());
            ctrl.vm.hasChanges(true);
            (ctrl.terceroOperacionSettings || angular.noop)();
        }
    });

    return tdSelector;
};

operaciones.metodoDePagoSelector = function (operacion,metodosDePago) {
    var mtSelector = oor.select2({
        model : operacion.forma_pago_id,
        data : metodosDePago.map(function (m) { return {id:m.id, text:m.metodo}; }),
        find : function (id) {
            return  metodosDePago.filter(function (mt) {
                       return mt.id == id;
                    })[0];
        },
        onchange : function (id) {
            operacion.asignarMetodoDePago( mtSelector.selected() );
        }
    });
    return mtSelector;
};

operaciones.tipoDeCambioSelector = function (operacion, tiposDeCambio) {
    var tcSelector = oor.select2({
        model : operacion.tipo_de_cambio,
        data : tiposDeCambio.map(function (t) {
            return {
                id : t.tipo_de_cambio,
                text : t.tipo_de_cambio + ' ' + t.nombre
            };
        }),
        find : function (tc) {
            return OORDEN.tipoDeCambio[tc];
        },
        onchange : function () {
            operacion.asignarTipoDeCambio(tcSelector.selected());
        }
    });

    return tcSelector;
};
