
module.exports = TablaItems;

/**
 * Los ítems de las tablas de operaciones, usado en
 *       - Ventas
 *       - NC Ventas
 *       - Compras
 *       - NC Compras
 *       - EOT
 */
function TablaItems (ctrl) {
    return  m('div.table-responsive', [
        m('table.table.bill.op-table', [
            m('thead', TablaItems.head(ctrl)),
            m('tbody', TablaItems.body(ctrl)),
            m('tfoot', TablaItems.foot(ctrl))
        ])
    ]);
};

/**
 * Construye un header para la tabla
 */
TablaItems.head = function (ctrl) {
    var impORet = ctrl.operacion().mostrar_impuestos() || ctrl.operacion().mostrar_retenciones();
    return m('tr', [
        m('th.options',''),
        m('th.concepto', 'Concepto'),
        m('th.cantidad', 'Cant.'),
        m('th.unidad', 'Unid.'),
        m('th.precio-unitario', 'Pr. Unit.'),
        ctrl.operacion().mostrar_descuento() ? m('th.descuento', '% Dcto' ) : '',
        ctrl.operacion().mostrar_cuentas()   ? m('th.cuenta', 'Cuenta')     : '',
        impORet ? m('th.imp-ret', 'Imp/Ret') : '',
        ctrl.operacion().mostrar_cctos() ? m('th.cctos', 'CCTOS') : '',
        m('th.importe', 'Importe'),
        m('th.options','')
    ]);
};

/**
 *
 */
TablaItems.body = function (ctrl) {
    return ctrl.operacion().items.map(function (item) {
        var itemVM  = ctrl.itemViewModel(item);
        var impORet = ctrl.operacion().mostrar_impuestos() || ctrl.operacion().mostrar_retenciones();
        var impYRet = ctrl.operacion().mostrar_impuestos() && ctrl.operacion().mostrar_retenciones();
        var id = 'operacion-item-'.concat(item.operacion_item_id());
        var enabled = itemVM.enabled() && item.$integrable();

        return m('tr',  {id:id, key:item.operacion_item_id(), config:config(ctrl), 'data-item-id' : item.operacion_item_id()},  [
            m('td', enabled ? m('i.ion-drag.drag') : ''),
            m('td.concepto', itemVM.inputConcepto()),
            m('td.text-right.cantidad', itemVM.inputCantidad()),
            m('td.text-right.unidad', itemVM.inputUnidad()),
            m('td.text-right.precio-unitario', itemVM.inputPrecioUnitario()),

            ctrl.operacion().mostrar_descuento() ? m('td.text-right.descuento', '' ) : '',
            ctrl.operacion().mostrar_cuentas() ? m('td.cuenta',  m('div.pull-left.cuentas-selector', itemVM.inputCuentas())) : '',

            impORet ? m('td.imp-ret', [
                ctrl.operacion().mostrar_impuestos()   ? m('div.impuesto', itemVM.inputImpuestos())   : '',
                ctrl.operacion().mostrar_retenciones() ? m('div.retencion',itemVM.inputRetenciones()) : ''
            ]) : '',


            ctrl.operacion().mostrar_cctos() ? m('td.cctos',
                m('div.ccto1', itemVM.inputCcto1()),
                m('div.ccto2', itemVM.inputCcto2())
            ) : '',

            m('td.text-right.importe[data-item-importe=' + item.operacion_item_id() + ']', item.importe.$int() ? item.importe() : '--'),

            m('td', [
                enabled ? m('i.ion-trash-a') : ''
            ])
        ]);
    });
};

/**
 * Footer
 */
TablaItems.foot = function (ctrl) {
    var rowspan = 2;
    rowspan += ctrl.operacion().$detalleImpuestos().length;
    rowspan += ctrl.operacion().$detalleRetenciones().length;

    return [
        m('tr', [
            m('th', {colspan: 3 + ctrl.vm.celdasActivas(), rowspan:rowspan} , [
                m('div.checkboxer.checkboxer-blue.form-inline', [
                    m('input[type="checkbox"]'.concat(ctrl.vm.enabled() ? '' : '[disabled]'), {
                        id: 'impuestos-incluidos',
                        checked : ctrl.operacion().precios_con_impuestos(),
                        onclick : m.withAttr('checked', function (v) {
                            ctrl.operacion().precios_con_impuestos(v);
                            ctrl.operacion().$impuestosIncluidos(Boolean(v));
                            ctrl.operacion().actualizar();
                        })
                    }),
                    m('label', {'for':'impuestos-incluidos'}, [
                        'Precios Incluyen Impuestos: ',
                        m('strong',ctrl.operacion().precios_con_impuestos() ? 'SÍ' : 'NO')
                    ]),
                ]),

                m('br'),

                m('ul.mostrar-checkers', {style:'font-size:0.9em'}, [
                    m('li', {style:'display:inline-block;padding:0 12px'} , m('strong', 'Mostrar:')),

                    m('li',{style:'display:inline-block;padding:0 12px'} ,
                        m('div.checkboxer.checkboxer-blue.form-inline', [
                            m('input[type="checkbox"][disabled]', {
                                id:'check-cuentas',
                                checked : ctrl.operacion().mostrar_cuentas(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_cuentas)
                            }),
                            m('label', {'for':'check-cuentas'}, 'Cuentas')
                        ])
                    ),

                    m('li',{style:'display:inline-block;padding:0 12px'} ,
                        m('div.checkboxer.checkboxer-blue.form-inline', [
                            m('input[type="checkbox"]'.concat(ctrl.vm.enabled() ? '' : '[disabled]'), {
                                id:'check-impuestos',
                                checked : ctrl.operacion().mostrar_impuestos(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_impuestos)
                            }),
                            m('label', {'for':'check-impuestos'}, 'Impuestos')
                        ])
                    ),

                    m('li',{style:'display:inline-block;padding:0 12px'} ,
                        m('div.checkboxer.checkboxer-blue.form-inline', [
                            m('input[type="checkbox"]'.concat(ctrl.vm.enabled() ? '' : '[disabled]'), {
                                id:'check-retenciones',
                                checked : ctrl.operacion().mostrar_retenciones(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_retenciones)
                            }),
                            m('label', {'for':'check-retenciones'}, 'Retenciones')
                        ])
                    ),

                    m('li',{style:'display:inline-block;padding:0 12px'} ,
                        m('div.checkboxer.checkboxer-blue.form-inline', [
                            m('input[type="checkbox"]'.concat(ctrl.vm.enabled() ? '' : '[disabled]'), {
                                id:'check-cctos',
                                checked : ctrl.operacion().mostrar_cctos(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_cctos)
                            }),
                            m('label', {'for':'check-cctos'}, 'Centros de Costo')
                        ])
                    )
                ])
            ]),
            m('th[colspan="2"]', 'Subtotal'),
            m('td.text-right', ctrl.operacion().$sumaImportes()),
            m('td','')
        ]),

        ctrl.operacion().$detalleImpuestos().map(function (impuesto) {
            return m('tr', [
                m('td[colspan="2"]', 'Impuesto: ' + impuesto.nombre),
                m('td.text-right', impuesto.monto()),
                m('td','')
            ]);
        }),

        ctrl.operacion().$detalleRetenciones().map(function (retencion) {
            return m('tr', [
                m('td[colspan="2"]', 'Retención: ' + retencion.nombre),
                m('td.text-right', retencion.monto()),
                m('td','')
            ]);
        }),

        m('tr', [
            m('th[colspan="2"]', 'Total'),
            m('td.text-right', ctrl.operacion().total.$int() ? ctrl.operacion().total() : ''),
            m('td','')
        ])
    ]
};


var dragging = null;
var dropping = null;

function config(ctrl){
    return function (element, initialized) {
        if(initialized) return;

        var dragger = element.querySelector('.ion-drag');

        element.addEventListener('mouseover', function(ev) {
            if(!ev.target.matches('.ion-drag.drag')) return;
            element.setAttribute('draggable','true');
        },true);

        element.addEventListener('mouseout', function(ev) {
            if(!ev.target.matches('.ion-drag.drag')) return;
            element.removeAttribute('draggable');
        },true);



        element.addEventListener('dragstart', function (ev) {
            ev.stopPropagation();
            ev.dataTransfer.effectAllowed = 'move';

            dragging = element.getAttribute('data-item-id');
            element.classList.add('dragging');

            var draggingItem;

            ctrl.operacion().items.forEach(function (item) {
                if(dragging == item.operacion_item_id()) draggingItem = item;
            });

            var crt = document.createElement('div');
            crt.style.display = 'none';
            crt.innerHTML = draggingItem.producto_nombre();
            document.body.appendChild(crt);
            ev.dataTransfer.setDragImage(crt,0,0);

        },false);

        element.addEventListener('dragend', function (evt) {
            dragging = null;
            dropping = null;
            element.classList.remove('dragging');
        },false);

        element.addEventListener('dragenter', function (ev) {
            var _dropping = element.getAttribute('data-item-id');
            var draggingIndex, droppingIndex, item;

            if(_dropping != dropping){
                dropping = _dropping;
                if(dropping != dragging) {
                    ctrl.operacion().items.forEach(function (item, index) {
                        if(item.operacion_item_id() == dragging) draggingIndex = index;
                        if(item.operacion_item_id() == dropping) droppingIndex = index;
                    });

                    item = ctrl.operacion().items[draggingIndex];
                    ctrl.operacion().items.splice(draggingIndex, 1);
                    ctrl.operacion().items.splice(droppingIndex, 0, item);
                    m.redraw();
                }
            }
        },false);

    }
}
