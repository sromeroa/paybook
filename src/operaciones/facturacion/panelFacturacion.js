module.exports = {
    controller : PanelFacturacionController,
    view : PanelFacturacionView
};

var tabs = [
    {caption : 'Sin Asociar', value : 0},
    {caption : 'Asociados',   value : 1},
    {caption : 'Todas',       value : null}
];


function PanelFacturacionController () {
    var ctx = this;

    this.cfdis    = m.prop();
    this.cargando = m.prop(false);
    this.origen   = m.prop('R');
    this.asociado = m.prop(0);

    this.initialize = function () {
        if(this.cfdis() || this.cargando()) return;

        this.cargando(true);

        oorden()
            .then(function () {
                return oor.request('/facturacion/cfdis', 'GET',{
                    data : {
                        origen   : ctx.origen(),
                        asociado : ctx.asociado() === null ? undefined : ctx.asociado()
                    }
                })
            })
            .then(function (r) {
                return r.map(function (d) {
                    d.fecha_hora = d.fecha;
                    d.fecha = d.fecha_hora.split(' ')[0];
                    return d;
                })
            })
            .then(ctx.cfdis)
            .then(this.cargando.bind(null, false))

    }

    ctx.xmlFile = m.prop();

    ctx.setxml = function () {
        var file = this.files[0];
        ctx.xmlFile(file ? file.name : null);
    }
}


function actualizar(ctx, prop) {
    return function (val) {
        prop(val);
        ctx.cfdis(null);
    }
}

function PanelFacturacionView (ctx) {
    ctx.initialize();

    return oor.panel({
        title : 'Panel CFDIS y XML´s'
    }, [

        m('.row', [
            m('.col-sm-6', [
                m('label', [
                    'Mostrar CFDIs',
                    m('select', {value:ctx.origen(), onchange: m.withAttr('value',actualizar(ctx,ctx.origen)) }, [
                        m('option', {value : 'R'}, 'Recibidos'),
                        m('option', {value : 'E'}, 'Emitidos')
                    ])
                ])
            ]),

            m('form.col-sm-6', {method:'POST', action:'/facturacion/subir', enctype:'multipart/form-data'}, [
                m('label', [
                     oor.stripedButton(
                         'span.btn-'.concat(ctx.xmlFile() ? 'success' : 'primary' ),
                         ctx.xmlFile() ? ctx.xmlFile() : 'Seleccionar XML ...' , {
                             style : 'zoom:0.8'
                         }
                    ),
                    m('input[type=file][name=xml]', {onchange : ctx.setxml, style:'display:none', accept:'.xml'}),

                ]),

                m('br'),

                ctx.xmlFile() ? m('label', [
                    oor.stripedButton('span.btn-primary', 'Seleccionar PDF (opcional)', {style : 'zoom:0.8'}),
                    m('input[type=file][name=pdf]', {style:'display:none', accept:'.pdf'}) 
                ]) : null,

                ctx.xmlFile() ? m('input.pull-right[type=submit][value="Subir CFDI"]') : null
            ]),

        ]),

        m('ul.nav.nav-tabs', [
            tabs.map(function (d) {
                return m('li', {'class' : ctx.asociado() == d.value ? 'active' : ''}, [
                    m('a[href=javascript:;]', {onclick: actualizar(ctx,ctx.asociado).bind(null, d.value) }, d.caption)
                ])
            })
        ]),

        tablaCfdis(ctx),
        MTModal.view() 
    ])
}


function tablaCfdis (ctx) {
    return ctx.cargando() ? oor.loading() : [
        ctx.cfdis().length == 0 ? m('h6',{style:'margin-top:20px'},'No hay CFDIs') : m('table.table.megatable', [
            m('thead', [
                m('tr', [
                    m('th', 'Fecha'),
                    m('th.text-center', 'Archivos'),

                    m('th.text-right', 'Folio Fiscal'),
                    m('th', 'Tipo'),
                    m('th', 'RFC Emisor'),
                    m('th', 'RFC Receptor'),
                    m('th.text-right', 'Total'),

                    m('th.text-right', 'Operación')
                ])
            ]),
            m('tbody', [
                ctx.cfdis().map(function (cfdi) {
                    return m('tr', [
                        m('td', oor.fecha.prettyFormat.year(cfdi.fecha)),

                        m('td.text-right',[
                            m('a.text-indigo.pull-right', {
                                href : '/facturacion/getXML/' + cfdi.folio_fiscal
                            }, [
                                m('i.fa.fa-file-code-o'), ' XML'
                            ]),

                            cfdi.archivo_pdf ? m('a.text-indigo.pull-left', {
                                href : '/facturacion/getPDF/' + cfdi.folio_fiscal
                            },[
                                m('i.fa.fa-file-code-o'), ' PDF'
                            ]) : null
                        ]),

                        m('td.text-right', [
                            m('a.text-indigo', cfdi.folio_fiscal)
                        ]),

                        m('td', cfdi.tipo_comprobante),
                        m('td', cfdi.rfc_emisor),
                        m('td', cfdi.rfc_receptor),

                        m('td.text-right', [
                            m('span.pull-left.text-grey', cfdi.moneda),
                            oorden.org.format(cfdi.total)
                        ]),

                        m('td.text-right', [
                            cfdi.operacion_asociada_id ? m('a.text-indigo', {
                                href : '/operaciones/' + cfdi.tipoOperacion + '/' + cfdi.operacion_asociada_id
                            }, cfdi.operacionAsociada) : null
                        ])
                    ])
                })
            ])
        ])
    ];
}




function IniciarAsociacion(ctx, cfdi) {
    return function () {
        MTModal.open({
            controller : AsociarCFDIController,
            args : {
                cfdi:cfdi,
                afterAsociar : function () { ctx.cfdis(null); }
            },
            content : AsociarCFDIView,
            top : function () {
                return m('h4','Asociar XML')
            }
        })
    }
}



function AsociarCFDIController(params) {
    var ctx = this;
    var tipos   = {ingreso : ['VTA','CNC'], egreso : ['VNC', 'COM'] };
    var esEmisor = Number(params.cfdi.rfc_emisor === oorden.org().clave_fiscal);

    var parametrosBusqueda = {
        opTipoOperacion : tipos[params.cfdi.tipo_comprobante][ 1  - esEmisor ],
        opTotal : params.cfdi.total,
        opFecha : params.cfdi.fecha,
        opFolioFiscal : '-IS-NULL-'
    };

    ctx.operaciones = m.prop();
    ctx.cargando = m.prop();


    ctx.initialize = function () {
        if(ctx.operaciones() || ctx.cargando()) return;

        ctx.cargando(true);

        oor.request('/operaciones/listado', 'GET', {data:parametrosBusqueda})
            .then(log)
            .then(f('operaciones'))
            .then(ctx.operaciones)
            .then(ctx.cargando.bind(null,false))
    }


    ctx.puedeAsociar = function (op) {
        return op.opEstatus === 'S' || op.opEstatus === 'A';
    }


    ctx.asociar = function (operacion) {
        oor.request('/facturacion/asociar', 'POST', {
            data : {
                cfdi_id      : params.cfdi.cfdi_id,
                folio_fiscal : params.cfdi.folio_fiscal,
                operacion_id : operacion.opOperacionId
            }
        })
        .then(function (r) {
            toastr.success('CFDI correctamente asociado con la operación');
            ctx.$modal && ctx.$modal.close();
            _.isFunction(params.afterAsociar) && params.afterAsociar();
        })
    }
}


function log (d) {
    console.log(d);
    return d;
}

function AsociarCFDIView (ctx) {
    ctx.initialize();
    if(ctx.cargando()) return oor.loading();

    return m('div', [
        m('table.table.megatable', [
            m('tbody', [
                ctx.operaciones().map(function (op) {
                    return m('tr', [
                        m('td', oor.fecha.prettyFormat.year(op.opFecha)),
                        m('td', op.opTitulo),
                        m('td.text-grey', op.opReferencia),
                        m('td', op.opMoneda),
                        m('td', oorden.org.format(op.opTotal)),
                        m('td', op.opEstatus),
                        m('td.text-right', [
                            ctx.puedeAsociar(op) ? oor.stripedButton('button.btn-success', 'Asociar', {
                                style : 'zoom:0.7; margin:4px',
                                onclick : ctx.asociar.bind(ctx, op)
                            }) : '---'
                        ])
                    ])
                })
            ])
        ])
    ]);
}
