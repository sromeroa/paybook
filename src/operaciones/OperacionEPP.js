var EPPComponent    = module.exports  = {};

var tablaItems      = require('./TablaItems.js');
var OpDetail        = require('./mixins/OperacionBase.js');
var OperacionPago   = require('./mixins/OperacionPago.js');
var OperacionEgreso = require('./mixins/OperacionEgreso.js');


EPPComponent.controller = function (params) {
    var ctrl = this;

    ctrl.documentosAPagar = ['COM','VNC','IOT'];

    OperacionEgreso.controller.call(ctrl, params);
    OperacionPago.controller.call(ctrl, params);

    /** 
     * Se modifica el tFoot de la tabla de documentos
     */
    ctrl.documentos.tFoot = EPPComponent.tFoot.bind(null, ctrl);

};

EPPComponent.view = function (ctrl) {
    var cuentaBanco = ctrl.operacion().$cuentaBanco();
    

    return m('div',
        m('div.transaccion-form.flex-row', {}, [
            m('div.transaccion-left', [
                m('h3.column-title', 'Destinatario'),
                ctrl.vm.terceroComponent,

                m('h5', 'Cuenta: ', m('strong', cuentaBanco.banco + '(' + cuentaBanco.cuenta_bancaria +')')),
                OpDetail.inputMetodoDePago(ctrl), 
                m('div.flex-row.flex-end', OpDetail.metodoDePagoFormulario(ctrl))
                //ctrl.metodoDePagoComponente
            ]),
            m('div.transaccion-right', [
                m('h3.column-title', 'Documento'),
                m('div.flex-row.flex-end', [
                    OpDetail.inputTipoDoc(ctrl),
                    OpDetail.inputSerie(ctrl),
                    OpDetail.inputNumero(ctrl)
                ]),
                m('div.flex-row.flex-end',OpDetail.inputFecha(ctrl)),
                OpDetail.inputReferencia(ctrl),
                
                m('br'),

                ctrl.monedaEsBase() ? m('div',
                    ctrl.operacion().$monedas().length ? m('h5', 'Tipos de Cambio') : '', 
                    ctrl.operacion().$monedas().map(function (moneda) {
                        return ctrl.monedaComponenteMap(moneda);
                    })
                ) : ctrl.monedaComponente
            ])
        ]),

        m('div.tabla-documentos', [
            m('h3.column-title', 'Documentos'),
            ctrl.documentos.items().length ? ctrl.documentos() : 'No hay documentos a pagar'
        ]),

        m.component(OpDetail.polizaComponent, {
            poliza : ctrl.operacion().$poliza 
        })
        //OpDetail.actionBar(ctrl)
    );
};



EPPComponent.tFoot = function (ctrl) {
    return [
        m('tr', {style:"border-top:3px solid #ccc"}, [
            m('th[colspan=4][rowspan=2]'),
            m('th.text-right[colspan=2]', 'Anticipo'),
            m('td.text-right', 
                m('input.text-right'.concat(ctrl.vm.enabled() ? '' : '[disabled="disabled"]'), {
                    value : ctrl.operacion().saldo(),
                    onchange : m.withAttr('value',function (v) {
                        ctrl.operacion().saldo(v);
                        ctrl.operacion().saldo.fix();
                        ctrl.operacion().actualizarPagos();
                    })
                })
            )
        ]),

        m('tr', [
            m('th.text-right[colspan=2]', 'Total'),
            m('td.text-right', ctrl.operacion().total())
           
        ])
    ];
};


