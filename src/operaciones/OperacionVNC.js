var VNCComponent    = module.exports  = {};

var OpDetail        = require('./mixins/OperacionBase.js');
var OpCompraVenta = require('./mixins/OperacionCompraVenta.js');
var Devolucion = require('./components/Devolucion.js');


VNCComponent.controller = function (params) {
    var ctrl = this;

    OpDetail.controller.call(this, params);
    OpCompraVenta.controller.call(this, params);

    ctrl.keyPrecioUnitario('precio_unitario_venta');
    ctrl.keyCuentaProducto('cuenta_nc_venta');
    ctrl.keyImpuestoProducto('impuesto_venta');
    ctrl.keyImpuestoTercero('impuesto_venta_id');

    ctrl.opcionesTercero = function () {
        var tercero = ctrl.operacion().$tercero();
        if(!tercero) return;
    };

    ctrl.devolucion = _.cached(function () {
        return new Devolucion.controller(ctrl);
    });

    ctrl.opcionesTercero();
    ctrl.actualizar(true);
    ctrl.loadProductos();
};

VNCComponent.view = OpCompraVenta.view;
