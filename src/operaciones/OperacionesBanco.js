module.exports = OperacionesBanco = {};

var title = OperacionesBanco.title = {};
var page = OperacionesBanco.page = {};
var dynTable = require('../nahui/dtable');
var TableHelper = require('../nahui/dtableHelper');
var nahuiCollection = require('../nahui/nahui.collection')

page.controller = function (settings) {
    var ctx = this;
    var Operacion = oor.mm('Operacion');
    var fields = Operacion.fields();
    var thelper
    var table = dynTable().key(f('operacion_id'));

    ctx.A = m.prop(0);
    ctx.S = m.prop(0);
    ctx.P = m.prop(0);
    ctx.T = m.prop(0);
    ctx.X = m.prop(0);

    var coll  = nahuiCollection('operaciones', { identifier : 'operacion_id' });
    var query = coll.query({});

    ctx.columnas = m.prop([
        fields.fecha,
        fields.doc_ref,
        fields.nombre_tercero,
        fields.monto_pagado,
        fields.monto_recibido,
        fields.estatus
    ]);

    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 9 })

    var saldo = {};

    ctx.saldo = m.prop(saldo);
    ctx.search = m.prop('');
    ctx.cuenta = m.prop(settings.cuenta);
    ctx.cuenta_id = m.prop(ctx.cuenta().cuenta_contable_id);
    ctx.transacciones = m.prop();



    Object.keys(settings.saldo).forEach(function (k) {
        var n = f(k)(settings.saldo);
        var prop  = Modelo.numberProp(m.prop(),2);

        prop(n);
        saldo[k] = prop;
    })


    var changeSearch = _.debounce(function (v) {
        v = v ? {$contains:v} : undefined;
        query.add({search :v}).exec();
    }, 500);

    ctx.changeSearch = function (v) {
        ctx.search(v);
        changeSearch(v)
    }

    actualizar();


    function actualizar () {
        obtenerTransacciones(ctx.cuenta_id(), ctx.search())
        .then(function (transacciones) {
            ctx.transacciones(transacciones);

            transacciones.forEach(function (item) {
                item.search = Operacion.search(item);
                ctx[item.estatus]( ctx[item.estatus]() + 1 );
                coll.insert(item)
            });

            setTimeout(function () {
                query.exec();
                m.redraw();
            },100);
        });
    }

    ctx.setupTable = function (element, isInitialized) {
        if(!isInitialized) {
             thelper.element(element);
             thelper.draw();
         }
    }

    ctx.selectTab = function (tab, update) {
        ctx.tab(tab);
        query.add(tab.query);

        if(update ===  false) return;
        query.exec();
    }

    ctx.tab = m.prop(null);
    ctx.selectTab( OperacionesBanco.tabs()[2], false)
}

page.view = function (ctx) {
    function tooltip (element, isInitialized) {
        if(isInitialized) return;
        $('[data-toggle="tooltip"]',element).tooltip();
    }

   var saldoFinal = ctx.saldo().saldo_final;

    return m('div', {config:tooltip},[
        m('.row', [
            m('.col-sm-4', [
                m('h3', ctx.cuenta().nombre),
                m('h5', [
                    ctx.cuenta().banco,
                    m.trust(' &mdash; '),
                    ctx.cuenta().cuenta_bancaria
                ]),
                m('h6', 'Cuenta: ' , m('strong',ctx.cuenta().cuenta))
            ]),
            m('.col-sm-8', [
                m('h5', [
                    m('div.pull-right', [
                        'Saldo OORDEN: ', m('strong', saldoFinal ? saldoFinal.number() : '' , ctx.cuenta().moneda)
                    ])
                ]),
                m('br'),
                m('h5', [
                    m('div.pull-right', [
                        'Saldo CONCILIADO: ', m('strong', '0.00' , ' ', ctx.cuenta().moneda)
                    ])
                ]),

                m('br'),

                
                m('.btn-group.pull-right', {style:'margin:0 10px'},[
                    m('a.btn.btn-sm.btn-default', {
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Transferir entre mis cuentas'
                    }, [
                       'Transferir'
                    ]),
                    m('a.btn.btn-sm.btn-default', {
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Conciliar con banco'
                    }, [
                        'Conciliar'
                    ])
                ]),

                m('.btn-group.pull-right', {style:'margin:0 10px'},[
                    m('a.btn.btn-sm.btn-default', {
                        'href' : '/operaciones/eot/crear?cuenta=' + ctx.cuenta_id(),
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Egreso'
                    }, [
                        'Egreso'
                    ]),
                    m('a.btn.btn-sm.btn-default', {
                        'href' : '/operaciones/epp/crear?cuenta=' + ctx.cuenta_id(),
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Pago a Proveedores'
                    }, [
                        'Pago'
                    ])
                ]),

                m('.btn-group.pull-right', {style:'margin:0 10px'},[
                    m('a.btn.btn-sm.btn-default', {
                        'href' : '/operaciones/iot/crear?cuenta=' + ctx.cuenta_id(),
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Ingreso'
                    }, [
                        'Ingreso'
                    ]),
                    m('a.btn.btn-sm.btn-default', {
                        'href' : '/operaciones/idc/crear?cuenta=' + ctx.cuenta_id(),
                        'data-toggle' : 'tooltip',
                        'data-placement' : 'bottom',
                        'title' : 'Cobro a Clientes'
                    }, [
                        'Cobro'
                    ])
                ])
            
            ])
        ]),


        m('.row', [
            m('.inputer.floating-label.inputer-indigo.col-sm-6', [
                m('.input-wrapper', [
                    m('input#buscar[type="text"].form-control',  {
                        value : ctx.search(),
                        oninput : m.withAttr('value',ctx.changeSearch)
                    }),
                    m('label', {'for' : 'buscar'}, [
                        m('i.ion-ios-search'), ' Buscar'
                    ])
                ])
            ]),
            m('.col-sm-6', [

            ])
        ]),

        /**
        en Preparación (XX),
        por Autorizar (XX),
        Aplicadas,
        con Saldo (XX),
        Canceladas  (XX),
        Todas
        **/

        m('ul.nav.nav-tabs.with-panel', [
            OperacionesBanco.tabs().map(function (tab) {
                return m('li', {
                    'class' : tab == ctx.tab() ? 'active' : '',
                    onclick : ctx.selectTab.bind(ctx,tab)
                }, [
                    m('a', {href:'javascript:;'}, [
                        tab.caption,
                        ' ',
                        tab.badge ? m('.badge.badge-sm', ctx[tab.badge]()) : null
                    ])
                ])
            })
        ]),

        m('table.table', {
            config : ctx.setupTable
        })
    ])
}


OperacionesBanco.tabs = m.prop([
    {
        caption : 'En Preparacion',
        badge : 'P',
        query : {estatus : 'P'}
    },
    {
        caption : 'Por Autorizar',
        badge : 'T',
        query : {estatus : 'T'}
    },
    {
        caption : 'Aplicadas',
        badge : false,
        query : {estatus : {$in : ['A','S']}}
    },
    {
        caption :'Con Saldo',
        badge : 'A',
        query : {estatus : 'A'}
    },
    {
        caption : 'Canceladas',
        badge : 'X',
        query : {estatus : 'X'}
    },
    {
        caption : 'Todas',
        badge : false,
        query : {estatus : undefined}
    }
]);


OperacionesBanco.obtenerCuenta = obtenerCuenta;


function obtenerCuenta (cuentaId) {
    return m.request({
        url : '/apiv2',
        method : 'GET',
        data : {
            modelo : 'cuentas',
            cuenta_contable_id : cuentaId
        },
        unwrapSuccess : function (r) {
            return r.data[0]
        }
    })
}

function obtenerTransacciones (cuentaId, search) {
    return m.request({
        url : '/apiv2',
        method : 'GET',
        data : {
            search : search ? search : undefined,
            modelo : 'operaciones',
            cuenta_banco_mov : cuentaId
        },
        unwrapSuccess : function (r) { return r.data }
    })
}

function setupTable (ctx) {
    return function (element, initialized) {
        if(initialized) return;
        var container  = d3.select(element);
        container.call(ctx.mTable);
    }
}
