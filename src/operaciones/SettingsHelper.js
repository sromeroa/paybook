module.exports = SettingsHelper;


function SettingsHelper (params) {
    var tipoOperacion = params.tipoOperacion;
    var settings = this;

    settings.usuario = m.prop({});
    settings.sucursal = m.prop({});
    settings.tercero = m.prop({});


    var defaultOrder = ['tercero', 'usuario', 'sucursal'];

    settings.obtener = function () {

        var signatureUsuario = [
            oorden.usuario().usuario_id,
            tipoOperacion()
        ];

        var signatureSucursal = [
            oorden.sucursal().sucursal_id,
            tipoOperacion(),
        ];

        return oor.uSettings(signatureUsuario, signatureSucursal)
            .then(function (response) {
                var dUsuario = response[0];
                var dSucursal = response[1];

                settings.usuario(dUsuario)
                settings.sucursal(dSucursal);
            });
    }

    settings.getKey = function (key, defaultValue, order) {
        if(!order) order = defaultOrder;
        var sVal;
        for(var k in order) {
            sVal = settings[order[k]]();
            if(typeof sVal[key] != 'undefined') {
                return sVal[key];
            }
        }
        return defaultValue;
    }

}
