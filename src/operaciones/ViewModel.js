module.exports = OperacionViewModel;

var current = m.prop();

function OperacionViewModel (context) {
    var memory = {};

    getVm.id = m.prop(0);

    getVm.clear = function () {
        getVm.id( getVm.id() + 1 );
    }

    return function getVm () {
        ViewModel(context, getVm.id());
    }
}


function ViewModel (context, id) {
    var vm = this;
    var operacion = context.operacion();

    vm.id = m.prop(String(id).concat(':', operacion.operacion_id()));
    vm.operacion = operacion;
    vm.enabled = m.prop(false);


    /*
    vm.tipoDocumento = m.prop();
    vm.tipoDeCambio = m.prop();


    vm.tipoDocSelector = creator(function () {
        var sel = operaciones.tipoDocSelector(operacion, oorden.tiposDeDocumento)
    });

    vm.metodoDePagoSelector = creator(function () {
        return operaciones.metodoDePagoSelector(operacion, oorden.metodosDePago())
    });
    */

}
