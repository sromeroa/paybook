var MainComponent = module.exports = {};
var tiposValidos = ['VTA','VNC','EPP','IOT','COM','CNC'];
var OperacionModel = require('./models/OperacionModel');


MainComponent.controller = function (args) {
    var ctx = this;
    var search = m.route.parseQueryString(location.search);

    var TiposOperacion = {
        EPP : {
            tipoOperacion : 'EPP',
            seccion : 'E',
            tipos  : oorden.tiposDeDocumento.EPP()
        }
    }

    ctx.idOperacion = m.prop(args.operacionId);
    ctx.tipoOperacion = m.prop(String(args.tipoOperacion).toUpperCase());
    ctx.$tipoOperacion = m.prop(TiposOperacion[ctx.tipoOperacion()]);

    ctx.ready = m.prop(false);
    ctx.operacion = m.prop(null);
    ctx.vm = m.prop(null);


    ctx.guardar = function () {
        guardarOperacion(ctx.operacion())
    }

    ctx.generateViewModel = function () {
        var vm = new ViewModel(ctx.operacion());
        ctx.vm(vm);
    }

    if (tiposValidos.indexOf(ctx.tipoOperacion()) == - 1) {
        throw ctx.tipoOperacion().concat(' No es un tipo váido de Operación');
    }

    obtenerOperacion(ctx.idOperacion(), ctx.tipoOperacion())
        .then(dataWrapper, errorOperacion)
        .then(obtenerTercero, errorOperacion)
        .then(obtenerCuenta, errorOperacion)
        .then(operacionLista, errorOperacion)


    function dataWrapper (operacion) {
        return {operacion : operacion};
    }

    function obtenerTercero (data){
        var tercero_id = (data && data.tercero_id) || search.tercero_id;

        if(tercero_id){
            console.log('buscarTercero', tercero_id );
        }

        data.tercero = null;

        return data;
    }


    function obtenerCuenta (data) {
        var cuenta_id = data.operacion.cuenta_banco_mov || search.cuenta;
        var deferred = m.deferred();

        if(cuenta_id) {
            m.request({
                url : '/apiv2/',
                data : {
                    modelo : 'cuentas',
                    cuenta_contable_id : cuenta_id
                },
                unwrapSuccess : function (r) { return r.data[0] }
            }).then(function (cta) {
                data.cuentaBancaria = cta;
                deferred.resolve(data)
            })

            return deferred.promise;
        }

        data.cuentaBancaria = null;
        deferred.resolve(data);
        return deferred.promise;
    }


    function operacionLista (data) {
        ctx.operacion( OperacionModel(data) );
        ctx.generateViewModel();



        if(ctx.operacion().$isNew() == false) return;


        /**
         * Se asigna el tipo de documento primero
         *  por default.
         */
        var tDoc = ctx.$tipoOperacion().tipos[0]
        ctx.vm().asignarTipoDeDocumento(tDoc);

        /**
         *
         */
        if(data.tercero) {
            ctx.vm().asignarTercero(data.tercero);
        }

        /**
         *
         */
        if(data.cuentaBancaria) {
            ctx.vm().asignarCuentaBancaria(data.cuentaBancaria)
        }
    }


    function errorOperacion (err) {
        console.log('error: ' + err);
    }
};


function config (ctx) {
    var redraw = _.debounce(m.redraw, 500);
    return function (element, isInitialized) {
        if(isInitialized) return;

        $(element).on('input', '[x-prop]', function () {
            var scope = ctx;
            var input = $(this);
            var model = input.attr('x-model');

            if(model) {
                scope = f(model)(scope);
            }

            scope[input.attr('x-prop')]( input.val() );
            redraw();
        });
    }
}


MainComponent.view = function (ctx) {
    var vm = ctx.vm();
    var oper = ctx.operacion();
    return oper ? m('div', [

        m('div.row', {config:config(ctx)}, [
            m('.col-md-5', [

                m('h3.column-title', 'Destinatario'),

                /*
                vm ? MonedaComponent.view(vm.moneda) : null,
                */

                vm ? T3Component.view(vm.selectors.tercero) : null,

                vm ? m('.flex-row', [
                    m('.mt-inputer',[
                        m('label', 'Forma de Pago'),
                        m('div', vm.selectors.formaPago.view())
                    ]),
                    FormaDePagoForm(ctx)
                ]) : null,

            ]),

            m('.col-md-7', [

                m('h3.column-title', [
                    vm ? TDocComponent.view(vm.selectors.tipoDoc) : null,
                ]),

                m('.flex-row', [
                    m('.mt-inputer', [
                        m('label', oper.serie(), ' # '),
                        m('input[type="text"]', {
                            'x-prop' : 'numero',
                            'x-model' : 'operacion',
                            value : oper.numero()
                        })
                    ]),

                    m('.mt-inputer', [
                        m('label', 'fecha'),
                        m('input[type="date"]', {
                            'x-prop' : 'fecha',
                            'x-model' : 'operacion',
                            value : oper.fecha()
                        })
                    ]),
                ]),


                m('.mt-inputer', [
                    m('label', 'Referencia'),
                    m('input[type=text]', {
                        value : oper.referencia(),
                        'x-prop' : 'referencia',
                        'x-model' : 'operacion'
                    })
                ]),





                Object.keys(ctx.operacion().conversiones()).map(function (codigo) {
                    var moneda = ctx.operacion().conversiones()[codigo];
                    var display = true

                    if(moneda.moneda() == oorden.organizacion().codigo_moneda_base) {
                        display = false;
                    }
                    return m('div',  {
                        style : 'display:'.concat(display ? 'block': 'none')
                    }, [
                        m.component(MonedaComponent, moneda)
                    ]);
                }),


            ])



        ]),



        TablaDeDocumentos(ctx),


        m('button.btn.btn-sm.btn-success', {
            onclick : ctx.guardar
        }, 'Guardar'),

        /*
        m('table.table', [
            Object.keys( ctx.operacion() ).map(function (k) {
                var value = ctx.operacion()[k]();
                if(value === null) {
                    value = m('small', 'null')
                } else if (typeof value === 'object') {
                    value = '{ ... }';
                }

                return m('tr', [
                    m('th', k),
                    m('td', value)
                ])
            })
        ]),
        */

    ]) : m('div', 0)
};


function FormaDePagoForm (ctx) {
    var metodo = ctx.vm().nombreFormaPago();

    return [
         FormaDePagoForm[metodo] ? FormaDePagoForm[metodo](ctx) : null
     ];
};


FormaDePagoForm.cheque = function (ctx) {
    return  [
        m('.mt-inputer', [
            m('label', 'Cheque #'),
            m('input[type="text"]', {
                value : ctx.operacion().num_cheque(),
                'x-prop' : 'num_cheque',
                'x-model' : 'operacion'
            })
        ]),
        m('.mt-inputer', [
            m('label', 'Fecha Cheque'),
            m('input[type="date"]', {
                value : ctx.operacion().fecha_cheque(),
                'x-prop' : 'fecha_cheque',
                'x-model' : 'operacion'
            })
        ])
    ]
};

FormaDePagoForm.transferencia = function (ctx) {
    return [
        m('.mt-inputer', [
            m('label', 'Nombre Banco'),
            m('input[type="text"]', {
                value : ctx.operacion().nombre_banco_destino(),
                'x-model' : 'operacion',
                'x-prop' : 'nombre_banco_destino'
            })
        ]),
        m('.mt-inputer', [
            m('label', 'Cuenta Banco'),
            m('input[type="text"]', {
                value : ctx.operacion().cuenta_banco_destino(),
                'x-model' : 'operacion',
                'x-prop' : 'cuenta_banco_destino'
            })
        ])
    ]
};


function obtenerOperacion (idOperacion, tipo) {
    var deferred = m.deferred();

    if(idOperacion === null) {
        deferred.resolve({ 
            operacion_id: oor.uuid(),
            sucursal_id : oorden.sucursal().sucursal_id,
            tipo_operacion : tipo,
            seccion : tipo.charAt(0),

            tasa_de_cambio : '1.000000',
            tipo_de_cambio : 'MXN',
            moneda : 'MXN',

            fecha : oor.fecha.toSQL(new Date),
            isNew : true
        })
    } else {
        m.request({
            url : '/apiv2',
            data : {
                modelo : 'operaciones',
                operacion_id : idOperacion,
                include : 'operaciones.tercero'
            },
            unwrapSuccess : function (r) {
                return r.data[0];
            }
        }).then(deferred.resolve)
    }

    return deferred.promise;
}



var MonedaComponent = require('./components/Moneda');
var TDocComponent = require('./components/TipoDocumento');
var T3Component = require('./components/Tercero')

function ViewModel (operacion) {
    var vm = this;
    var selectors = {};

    vm.selectors = selectors;

    vm.tercero = m.prop();

    vm.cuentaBancaria = m.prop();

    vm.tipoDocumento = m.prop();

    vm.formaDePago = m.prop();

    vm.nombreFormaPago = m.prop();

    /**
     *
     *
    **/
    vm.asignarTipoDeDocumento = function (tDoc) {
        vm.tipoDocumento(tDoc);
        operacion.tipo_documento(tDoc ? tDoc.tipo_documento_id : null);
        operacion.serie(tDoc.serie ? tDoc.serie : '');
    }

    /**
     *
     *
    **/
    vm.asignarFormaDePago = function (formaDePago) {
        var metodo = formaDePago.metodo.toLowerCase();
        vm.nombreFormaPago(metodo);
    };

    /**
     *
     *
    **/
    vm.asignarTercero = function (tercero) {
        console.log( tercero )
    };

    /**
     *
     *
    **/
    vm.asignarCuentaBancaria = function (ctaBanco) {
        operacion.cuenta_banco_mov(ctaBanco ? ctaBanco.cuenta_contable_id : null)
    };

    //Se asigna la forma de pago para que el formulario funcione
    var fPago = oorden.metodoDePago( f('forma_pago_id')(operacion) );
    if(fPago) {
        vm.asignarFormaDePago(fPago);
    }

    selectors.tipoDoc = new TDocComponent.controller({
        tipo_documento : operacion.tipo_documento,
        tipo_operacion : operacion.tipo_operacion(),
        tipoDocumento : vm.tipoDocumento
    });

    selectors.tercero = new T3Component.controller({
        tercero : operacion.tercero,
        tercero_id : operacion.tercero_id,
        enabled : m.prop(true),
        placeholder : 'Seleccionar...'
    });


    /**
     * Métodos de Pago
     */
    var metodosDePago = Object.keys(oorden.metodosDePago)
                            .filter(function (d) {
                                return d != 'toJSON';
                            }).map(function (d) {
                                return oorden.metodosDePago[d]();
                            }).map(function (m) {
                                return {id:m.id, text:m.metodo};
                            });

    selectors.formaPago = oor.select2({
        data : metodosDePago,
        model : operacion.forma_pago_id,
        find : oorden.metodoDePago,
        onchange : function () {
            vm.asignarFormaDePago( selectors.formaPago.selected() );
        }
    })
}





function guardarOperacion (operacion) {
    var data = operacion;

    m.request({
        url : '/api/operacion/agregar',
        method : 'POST',
        data : data
    })
}










function TablaDeDocumentos (ctx) {
    if(!ctx.tablaDocumentos){
        ctx.tablaDocumentos = new TablaDeDocumentos.controller(ctx);
    }
    return TablaDeDocumentos.view(ctx);
}


TablaDeDocumentos.controller = function (ctx) {
    var tabla = this;

    var documentosEnlistados = ['VTA', 'CNC'];

    tabla.terceroId = m.prop();

    tabla.documentos = m.prop(null);

    tabla.obtenerDocumentos  = function () {
        var terceroId = null;

        if(ctx.operacion()) {
            terceroId = ctx.operacion().tercero_id() || null;
        }

        if(terceroId != tabla.terceroId()) {
            tabla.documentos(null);
            tabla.terceroId(terceroId);
            if(tabla.terceroId() == null) return tabla.documentos();

            m.request({
                url : '/apiv2',
                data : {
                    modelo : 'operaciones',
                    tercero_id : terceroId
                },
                unwrapSuccess : function (r) {
                    return r.data;
                }
            })
            .then(asignarDocumentos)
        }

        return tabla.documentos();
    };


    function asignarDocumentos (rDocumentos) {
        var documentos = rDocumentos.filter(function (d) {
                if(documentosEnlistados.indexOf(d.tipo_operacion) > -1) {
                    return Number(d.saldo);
                }
                return false;
            }).map(function (d) {
                ctx.operacion().infoDocumento(d);
                return d;
            })

        tabla.documentos(documentos);
    }
}

TablaDeDocumentos.view = function (ctx) {
    var tabla = ctx.tablaDocumentos;
    var documentos = tabla.obtenerDocumentos();

    function config (element, isInitialized) {
        if(isInitialized)return;

        $(element).on('focus', '[x-doc-a-pagar]', function () {
            var input = $(this);
            var docAPagarId = input.attr('x-doc-a-pagar');
            var pago = ctx.operacion().pagoDeDocumento(docAPagarId);

            if(pago) return;

            var doc = tabla.documentos().filter(function (d) {
                return f('operacion_id')(d) == docAPagarId;
            })[0];

            ctx.operacion().crearPago(doc);
            ctx.operacion().actualizar();
            m.redraw();
        });


        $(element).on('input', '[x-doc-a-pagar]', function () {
            var input = $(this);
            var docAPagarId = input.attr('x-doc-a-pagar');
            var pago = ctx.operacion().pagoDeDocumento(docAPagarId);

            if(!pago) return;
            pago.monto_pago( input.val() );
        });

    }

    return m('div', {style:'padding-top:10px'}, [

        documentos ? m('.table-responsive', {config:config}, [
            m('table.table', [
                m('thead', [
                    m('tr', [
                        m('th', 'Fecha'),
                        m('th', 'Documento'),
                        m('th', 'Total'),
                        m('th', 'Saldo'),
                        m('th', 'A Pagar')
                    ])
                ]),
                m('tbody', [
                    documentos.map(function (d) {
                        var moneda = ctx.operacion().getConversion( f('moneda')(d) );
                        var info = ctx.operacion().infoDocumento(d);
                        var pago = ctx.operacion().pagoDeDocumento(d);

                        return m('tr', [
                            m('td', f('fecha')(d) ),
                            m('td', f('documento')(d) ),
                            m('td', f('total')(d) ),
                            m('td.text-right', info.saldoMonedaCta.number()),
                            m('td.text-right', [
                                m('input[type="text"]', {
                                    'class' : 'text-right',
                                    'x-doc-a-pagar' : f('operacion_id')(d),
                                    'value' : pago ? pago.monto_pago() : ''
                                })
                            ])
                        ]);
                    })
                ]),
                m('thead',[
                    m('tr', [
                        m('th[colspan=4].text-right', 'Anticipo: '),
                        m('td.text-right', [
                            m('input[type="text"]', {
                                'class' : 'text-right',
                                onchange : m.withAttr('value', function(val) {
                                    ctx.operacion().saldo.fix(val);
                                    ctx.operacion().actualizar();
                                }),
                                value : ctx.operacion().saldo()
                            })
                        ])
                    ]),
                    m('tr', [
                        m('th[colspan=4].text-right', 'Total: '),
                        m('td.text-right',ctx.operacion().total.number())
                    ])
                ])
            ])
        ]) : TablaDeDocumentos.mensaje(ctx)
    ])
}


TablaDeDocumentos.mensaje = function (ctx) {
    return m('.text-center', [
        ctx.operacion().tercero_id() ? 'cargando' : 'Seleccione un Destinatario para comenzar'
    ])
}
