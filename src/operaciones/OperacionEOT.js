var EOTComponent = module.exports = {};
var tablaItems = require('./TablaItems.js');
var OpDetail = require('./mixins/OperacionBase.js');
var OpEgreso = require('./mixins/OperacionEgreso.js');

EOTComponent.controller = function(params) {
    var ctrl = this;
    OpEgreso.controller.call(ctrl, params);
}


EOTComponent.view = function(ctrl) {
    var cuentaBanco = ctrl.operacion().$cuentaBanco();

    return m('div.opv-view.transaccion-view', [
        m('div.transaccion-form.flex-row', [
            m('div.transaccion-left', [
                m('h3.column-title', 'Destinatario'),
                ctrl.vm.terceroComponent,

                m('h5', 'Cuenta: ', m('strong', cuentaBanco.banco + '(' + cuentaBanco.cuenta_bancaria + ')')),
                OpDetail.inputMetodoDePago(ctrl),
                m('div.flex-row.flex-end', OpDetail.metodoDePagoFormulario(ctrl))
            ]),

            m('div.transaccion-right', [
                m('h3.column-title', 'Documento'),

                m('div.flex-row.flex-end', [
                    OpDetail.inputTipoDoc(ctrl),
                    OpDetail.inputSerie(ctrl),
                    OpDetail.inputNumero(ctrl)
                ]),

                m('div.flex-row.flex-end', OpDetail.inputFecha(ctrl)),

                OpDetail.inputReferencia(ctrl),
                m('br'),
                ctrl.monedaComponente
            ])
        ]),

        m('br'),

        m('div', [
            ctrl.vm.enabled() ? m('div.pull-right.btn.btn-primary.btn-sm', {
                onclick: ctrl.agregarLinea
            }, '+ Línea') : '',
            m('h3.column-title', 'Detalle'),
            tablaItems(ctrl)
        ]),

        m.component(OpDetail.polizaComponent, {
            poliza: ctrl.operacion().$poliza
        })

    ]);
}