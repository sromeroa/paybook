var OpDetail  = module.exports = {};


OpDetail.VTA = require('./OperacionVTA.js');
OpDetail.VNC = require('./OperacionVNC.js');

OpDetail.COM = require('./OperacionCOM.js');
OpDetail.CNC = require('./OperacionCNC.js');

/* EGRESOS */
OpDetail.EOT = require('./OperacionEOT.js');
OpDetail.EPP = require('./OperacionEPP.js');

/* INGRESOS */
OpDetail.IOT = require('./OperacionIOT.js');
OpDetail.IDC = require('./OperacionIDC.js');
