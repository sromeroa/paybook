module.exports = TransaccionModel;

var PagoModel = require('./PagoModel');

/**
 *
 * Modelo para Pago y Cobro de Documentos
 *
 */
function TransaccionModel (params) {
    var oper = this;
    var iOper = params.operacion;

    /**
     * Se definen las propiedades
     * exclusivas para estos modelos
    **/
    //Banco Local
    oper.cuenta_banco_mov = m.prop()


    //Formas de Pago
    oper.forma_pago_id = m.prop(iOper.forma_pago_id);
    oper.num_cheque = m.prop(iOper.num_cheque || null);
    oper.fecha_cheque = m.prop(iOper.fecha_cheque || null);

    // Info banco externo
    oper.nombre_banco_destino = m.prop(iOper.nombre_banco_destino || null);
    oper.cuenta_banco_destino = m.prop(iOper.cuenta_banco_destino || null);


    //Listado de Pagos
    oper.pagos = m.prop([]);

    /**
     * Método de actualización de Totales
     * Se asigna el total sumando el saldo (anticipo)
     * con todos los montos de los pagos
     */
    Object.defineProperty(oper,'actualizar',{
        value : function () {
            var total = oper.saldo.$int();

            oper.pagos().forEach(function (p) {
                total += p.monto_pago.$int();
            });

            oper.total.$int(total);
        }
    });



    //Conversiones de Moneda
    var cMonedas = {};



    Object.defineProperty(oper,'conversiones', {
        enumerable : false,
        value : m.prop(cMonedas)
    })


    /**
     * Método que dado un codigo de moneda
     * devuelve un objeto preparado para ser un
     * selector de moneda solo uno por tipo de moneda
     */

    Object.defineProperty(oper, 'getConversion', {
        enumerable : false,
        value : function (codigoMoneda) {
            if(! cMonedas[codigoMoneda]) {
                cMonedas[codigoMoneda] = createConversion(codigoMoneda);
            }
            return cMonedas[codigoMoneda];
        }
    });

    /**
     * Crea el objeto preparado para ser un selector d eMoenda
     */
    function createConversion (codigoMoneda) {
        return {
            moneda : m.prop(codigoMoneda),
            key : codigoMoneda,
            tipo_de_cambio : m.prop(),
            tasa_de_cambio : m.prop(1),
            tipoDeCambio : m.prop(),
            onchange : update
        }
    }


    var  infoDocumentos = {};



    /**
     * Info documento
     * Función que devuelve la información relacionada al
     * documento que se pide
    **/
    Object.defineProperty(oper, 'infoDocumento', {
        enumerable : false,
        value : function (doc) {
            var opId = f('operacion_id')(doc);
            if(!infoDocumentos[opId]) {
                infoDocumentos[opId] = createDocInfo(doc);
            }
            return infoDocumentos[opId];
        }
    });

    /**
     * Crea la info del documento con lo necesario
     */
    function createDocInfo (doc) {
        var moneda = oper.getConversion(doc.moneda);
        var tasaCambio = null, saldoMonedaCta = null;

        var info = {
            tipo_de_cambio : moneda.tipo_de_cambio,
            tasa_de_cambio : moneda.tasa_de_cambio,
            saldoMonedaDoc : Modelo.numberProp(m.prop(doc.saldo),2),
            saldoMonedaCta : Modelo.numberProp(m.prop(0),2),
            update : function () {
                var saldo = info.tasa_de_cambio() * info.saldoMonedaDoc();
                saldo = Math.round(saldo * 100) / 100;
                info.saldoMonedaCta(saldo)
            }
        }

        info.update();
         return info;
    }



    /**
     * Busca un pago por medio del el documento que se paga
     */
    Object.defineProperty(oper, 'pagoDeDocumento', {
        value : function (doc) {
            if(typeof doc === 'object') {
                doc = f('operacion_id')(doc);
            }

            return oper.pagos().filter(function (p) {
                return f('documento_pagado_id')(p) == doc;
            })[0];
        }
    });


    /**
     * Crea un pago a través del documento que se paga
     */
    Object.defineProperty(oper, 'crearPago', {
        enumerable : false,
        value : function (doc) {
            var aplicacion_id;

            if(oper.pagos().length) {
                aplicacion_id = oper.pagos()[0].aplicacion_id();
            } else {
                aplicacion_id = oor.uuid();
            }

            var pago = new PagoModel({
                aplicacion_a_doc_id : oor.uuid(),
                aplicacion_id : aplicacion_id
            });

            var info = oper.infoDocumento(doc);
            var moneda = oper.getConversion(doc.moneda);


            pago.operacion_pago_id = oper.operacion_id;
            pago.fecha = oper.fecha;
            pago.sucursal_id = oper.sucursal_id;
            pago.documento_pagado_id( f('operacion_id')(doc) );

            pago.tasa_de_cambio = moneda.tasa_de_cambio;
            pago.tipo_de_cambio = moneda.tipo_de_cambio;

            /**
             * Se asignan los montos por default del pago
             */
            pago.monto_pago.$int( info.saldoMonedaCta.$int() );
            pago.monto_pagado.$int( info.saldoMonedaDoc.$int() );


            oper.pagos().push(pago);
        }
    });



    function update () {
        Object.keys(infoDocumentos).forEach(function (idDoc) {
            var info = infoDocumentos[idDoc];
            var saldoMonedaCtaAnterior = info.saldoMonedaCta.$int();
            var pago = oper.pagoDeDocumento(idDoc);

            info.update();

            // Si ya hay un pago registrado y este paga la totalidad
            // entonces se actualiza el monto para que siga pagando la totalidad
            if(pago && pago.monto_pago.$int() === saldoMonedaCtaAnterior) {
                pago.monto_pago.$int( info.saldoMonedaCta.$int() );
            }
        });

        oper.actualizar();
    }
}
