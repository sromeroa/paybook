module.exports = PagoModel;
/**
 *
 */


function PagoModel (data) {
    var pago = this;

    data || (data = {});

    pago.aplicacion_id = m.prop(data.aplicacion_id || null);

    pago.aplicacion_a_doc_id = m.prop(data.aplicacion_a_doc_id || null);

    pago.operacion_pago_id = m.prop(data.operacion_pago_id || null);

    pago.fecha = m.prop(data.fecha || null);


    //Monto en la moneda del documento
    pago.monto_pago = Modelo.numberProp(m.prop(0),2);

    //Monto en la moneda de la cuenta
    pago.monto_pagado = Modelo.numberProp(m.prop(0),2);

    //Documento que se paga
    pago.documento_pagado_id = m.prop(0);

}
