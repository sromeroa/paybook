module.exports = OperacionModel;

var EPPModel = require('./EPPModel');

var models = {
    EPP : EPPModel
};

function OperacionModel (params) {
    if(!(this instanceof OperacionModel)) return new OperacionModel(params);

    var oper = this;
    var iOper = params.operacion;

    oper.sucursal_id = m.prop(iOper.sucursal_id);
    oper.operacion_id = m.prop( iOper.operacion_id );
    oper.tipo_operacion = m.prop( iOper.tipo_operacion );
    oper.seccion = m.prop(iOper.seccion);
    oper.estatus = m.prop(iOper.estatus || 'P');


    oper.tipo_documento = m.prop(iOper.tipo_documento || null);
    oper.serie = m.prop(iOper.serie || '');
    oper.numero = m.prop(iOper.numero || null);



    oper.tercero_id = m.prop(iOper.tercero_id || null);
    oper.tercero = m.prop(iOper.tercero || null)

    oper.fecha = m.prop(iOper.fecha || null);
    oper.referencia = m.prop(iOper.referencia || null);





    //Tipos DeCambio y eso
    oper.tipo_de_cambio = m.prop(iOper.tipo_de_cambio);
    oper.moneda = m.prop(iOper.moneda);
    oper.tasa_de_cambio = m.prop(iOper.tasa_de_cambio);

    oper.$isNew = m.prop(iOper.isNew ? true : false);
    models[oper.tipo_operacion()].call(oper, params);


    oper.total = Modelo.numberProp(m.prop(iOper.total || 0), 2);
    oper.saldo = Modelo.numberProp(m.prop(iOper.saldo || 0), 2);
}
