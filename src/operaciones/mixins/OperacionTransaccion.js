var OpTransaccion = module.exports = {};
var OpBase = require('./OperacionBase.js');

/**
 * Transaccion es el código común a todos los
 *  Egresos e Ingresos
 */
OpTransaccion.controller = function () {
    var ctrl = this;

    ctrl.monedaCuenta  = m.prop();
    ctrl.monedaEsBase  = m.prop();

    ctrl.monedaCuenta( ctrl.operacion().$cuentaBanco().moneda );
    ctrl.monedaEsBase(true);

    OpBase.controller.call(this);

    /**
     * Si la transacción es nueva
     *  - se asigna el tipo de cambio
     *  - se asigna transferencia como metodo de pago por default (documentación)
     *
     */

    ctrl.terceroAsignado = function () {
        var t3Config = ctrl.$lastUsed.tercero();

        if(t3Config.banco) {
            ctrl.operacion().nombre_banco_destino(t3Config.banco);
        }

        if(t3Config.cuenta) {
            ctrl.operacion().cuenta_banco_destino(t3Config.cuenta);
        }
    }


    

    if(ctrl.operacion().$new()) {

        var tiposDeCambioMoneda = oorden.tiposDeCambio().filter(function (tc) {
            return tc.codigo_moneda == ctrl.monedaCuenta();
        });

        //console.log(  oorden.tipoDeCambio(tiposDeCambioMoneda[0].tipo_de_cambio_id) );

        ctrl.operacion().asignarTipoDeCambio( tiposDeCambioMoneda[0] );
        ctrl.operacion().asignarMetodoDePago( oorden.metodosDePago.transferencia() );
        ctrl.operacion().mostrar_cuentas(true);
    }
}
