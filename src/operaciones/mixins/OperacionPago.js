var OperacionPago       = module.exports = {};
var TablaDocumentos     = require('./TablaDocumentos.js');
var MonedaComponent     = require('../components/Moneda.js');

/** 
 * Código Común para
 *   EPP e IDC
 */
OperacionPago.controller = function () {
    var ctrl = this;
    var Aplicacion = oor.mm('AplicacionDePago');


    
    if( ctrl.monedaEsBase() == false) {
        alert('Pagos desahbilitados con moneda extranjera Temporalmente');
        history.back();
        return;
    }

    OperacionPago.prepararOperacion(ctrl.operacion());

    
    /** 
     * La tabla de los documentos
     */
    ctrl.documentos = TablaDocumentos.call(ctrl);

    var componentesDeMonedas = {};

    ctrl.monedaComponenteMap = function (moneda) {
        if(!componentesDeMonedas[moneda]) {
            var tiposDeCambio = ctrl.operacion().$tiposDeCambio();
            componentesDeMonedas[moneda] = m.component(MonedaComponent, tiposDeCambio[moneda]);
        }
        return componentesDeMonedas[moneda];
    };


    /** 
     * Seleccionar los documentos que se muestran
     */
    ctrl.filtrarDocumentos = function (item) {

        var pagos_doc_id = ctrl.operacion().$pagos_doc_id();
        var pago = pagos_doc_id[item.operacion_id];

        if(pago && pago.monto_pagado.$int()) return true;

        if(ctrl.monedaEsBase() === false)  {
            var monedaBase  = OORDEN.organizacion.moneda_base_id;
            var monedaCta   = ctrl.operacion().$cuentaBanco().moneda;
            //si la moneda no es base solo pueden pagarse en monedaBase y moneda de la cuenta
            if( [monedaBase, monedaCta].indexOf(item.moneda) == -1 ) return false;
        }

        console.log(item.saldo, ctrl.documentosAPagar, item.tipo_operacion);

       return ctrl.vm.enabled() && Number(item.saldo) != 0 && item.estatus == 'A' && ctrl.documentosAPagar.indexOf(item.tipo_operacion) > -1;
    };


    /** 
     * Al cargar un tercero, se cargan los documentos
     */
    ctrl.terceroAsignado = function () {
        ctrl.documentos.cargar();
    };


    if(ctrl.operacion().tercero_id()) {
        ctrl.documentos.cargar().then();
    }
}


OperacionPago.prepararOperacion = function (operacion) {
    var pagos_doc_id    = {};
    var tipos_de_cambio = {};
    var Aplicacion      = oor.mm('AplicacionDePago');

    operacion.$pagos_doc_id  = m.prop(pagos_doc_id);
    operacion.$tiposDeCambio = m.prop(tipos_de_cambio);
    operacion.$documentos    = m.prop([]);
    operacion.$monedas       = m.prop([]);

    operacion.total.fix();
    operacion.saldo.fix();

    operacion.crearPago = function (documento) {
        if(documento.pago()) return;
        
        var pago = new Aplicacion({
            aplicacion_id : operacion.operacion_id(),
            aplicacion_pago_id : oor.uuid()
        });

        var moneda = tipos_de_cambio[documento.moneda];

        operacion.pagos().push(pago);

        pago.tipo_de_cambio = moneda.tipo_de_cambio;
        pago.tasa_de_cambio = moneda.tasa_de_cambio;
        pago.$inicializar  = true;

        pago.documento_pagado_id( documento.operacion_id );
        pago.operacion_pago_id( operacion.operacion_id() )

        pago.monto_pago.err = m.prop(false);
        documento.pago(pago);

        if(operacion.$monedas().indexOf(documento.moneda) == -1) {
            agregarMoneda(documento.moneda);
            operacion.actualizarPagos();
        } else {
            operacion.actualizarPagos();
        }
    };

    /** 
     * Se asignan los documentos y se las asigna un pago
     */
    operacion.asignarDocumentos = function (documentos) {
        documentos.forEach(function (documento) {
            var moneda = documento.tipo_de_cambio.substring(0,3);

            documento.pago  = m.prop(pagos_doc_id[documento.operacion_id]);
            documento.total = Modelo.numberProp(m.prop(documento.total),2);
            documento.saldo = Modelo.numberProp(m.prop(documento.saldo),2);
            documento.saldoMonedaCta  = Modelo.numberProp(m.prop(0),2);

            
            //MONEDA DEL DOCUMENTO
            if(!tipos_de_cambio[moneda]) {
                tipos_de_cambio[moneda] = crearMoneda(operacion, moneda, documento.tipo_de_cambio, documento.tasa_de_cambio);
            }

            documento.factorMonedaCta = tipos_de_cambio[moneda].factorMonedaCta;
            calcularSaldoMonedaCuenta(documento);

            documento.total.fix();
            documento.saldo.fix();

            if(documento.pago()) {
                documento.pago().monto_pagado.fix();
            }

            operacion.$documentos().push(documento);
        });
    };


    operacion.actualizarPagos = function (config) {
        var total = 0;
        var err = false;
        
        operacion.$documentos().forEach(function (documento) {
            var pago = documento.pago();
            var saldoMonedaCtaAnterior = documento.saldoMonedaCta.$int();
            
            calcularSaldoMonedaCuenta(documento);

            var montoPagado, montoPago;

            if(pago) {
                if(pago.$inicializar || saldoMonedaCtaAnterior == pago.monto_pagado.$int()) {
                    delete(pago.$inicializar);
                    montoPagado = documento.saldoMonedaCta.$int();
                    montoPago   = documento.saldo.$int();
                } else {
                    montoPagado = pago.monto_pagado.$int();
                    montoPago   = montoPagado / documento.factorMonedaCta();
                }

                pago.monto_pagado.$int( montoPagado );
                pago.monto_pago.$int( montoPago );
                total += montoPagado;

                if(pago.monto_pagado.$int() > documento.saldoMonedaCta.$int()) {
                    toastr.error('No se puede pagar un monto mayor al saldo');
                    pago.monto_pagado.$int( documento.saldoMonedaCta.$int());
                    err = true;
                }
            }
        });

        operacion.saldo.fix();

        total += operacion.saldo.$int();
        operacion.total.$int(total);

        if(err) {
            operacion.actualizarPagos();
        }
    }

    /** 
     * 
     */
    operacion.pagos().forEach(function (pago) {
        pagos_doc_id[pago.documento_pagado_id()] = pago;
        var moneda = pago.tipo_de_cambio().substring(0,3);

        
        if(!tipos_de_cambio[moneda]) {
            tipos_de_cambio[moneda] = crearMoneda(operacion, moneda, pago.tipo_de_cambio() , pago.tasa_de_cambio());
            agregarMoneda(moneda);
        }

        //Se igualan los ítems
        pago.tipo_de_cambio = tipos_de_cambio[moneda].tipo_de_cambio;
        pago.tasa_de_cambio = tipos_de_cambio[moneda].tasa_de_cambio;
    });



    function agregarMoneda (moneda) {
        if(moneda == operacion.moneda()) return;
        if(operacion.$monedas().indexOf(moneda) >= 0) return;
        operacion.$monedas().push(moneda);
    }


    function calcularSaldoMonedaCuenta(documento) {
        var saldoMonedaCta = documento.factorMonedaCta() * documento.saldo.$int();
        documento.saldoMonedaCta.$int(saldoMonedaCta);
        documento.saldoMonedaCta.fix();
    };
}

function crearMoneda (operacion, moneda, tipo_de_cambio, tasa_de_cambio) {
    var moneda = {
        moneda : moneda,
        tipoDeCambio : m.prop(),
        tipo_de_cambio : m.prop( tipo_de_cambio ),
        tipoDeCambio_label : moneda,
        tasa_de_cambio : m.prop( tasa_de_cambio ),
        factorMonedaCta : m.prop(),
        onchange : onchange,
        key : moneda,
        actualizarTasa : function () {

        }
    };

    moneda.factorMonedaCta( calcularFactor() );

    function calcularFactor() { 
        return moneda.tasa_de_cambio() / operacion.tasa_de_cambio() 
    };

    function onchange () { 
        moneda.factorMonedaCta(calcularFactor()); 
        operacion.actualizarPagos();
    }

    return moneda;
};