
var CompraVenta = module.exports = {};

var OpDetail = require('./OperacionBase.js');
var TablaItems = require('../components/TablaItemsCompraVenta.js');
var Devolucion = require('../components/Devolucion.js');
var DireccionComponent = require('../components/Direccion.js');
var LastUsed = require('../helpers/lastUsed');
var NotasComponent = require('../../misc/components/NotasComponent');
var TabsComponent = require('../components/TabsComponent.js');
var PagarOperacionComponent = require('../components/PagarOperacionComponent');
var NCFSelector = require('../components/NCFSelector');
var ncf = require('../components/ncf.js');

var cuentaSelector = {};

cuentaSelector.controller = function (parentCtrl) {
    var ctrl = this;

    this.operacion = parentCtrl.operacion;

    this.enabled = m.prop(false);

    var selector = oor.cuentasSelect2({
        cuentas : oorden.cuentas(),
        model : function () {
            if(arguments.length) {
                parentCtrl.operacion().cuenta_tercero_id(arguments[0])
            }
            return parentCtrl.operacion().cuenta_tercero_id()
        },
        onchange : function () {
            ctrl.enabled(false);
        }
    });

    this.selector = selector;

    this.enable = function (i) {
        this.enabled(i)
    }
}



cuentaSelector.view =  function (ctrl) {
    var idCuenta = ctrl.operacion().cuenta_tercero_id();
    var cuenta = oorden.cuenta(idCuenta);

    return  m('div', {style:'margin:10px 0'}, [
        true ? [
            ctrl.enabled() ? m('div.cuentas-selector', [
                ctrl.selector.view(),
                ' ',
                m('span', {
                    onclick:ctrl.enable.bind(ctrl, false),
                    style:'cursor:pointer'
                }, m.trust('&times;'))

            ]) : m('div.text-indigo', {
                    onclick:ctrl.enable.bind(ctrl,true),
                    style:'font-size:0.8em;cursor:pointer'
                },cuenta ?  [
                    m('strong', f('cuenta')(cuenta)),
                    ' ',
                    m('span', f('nombre')(cuenta))
            ]: 'Sin Cuenta')
        ] : null
    ])
}


CompraVenta.controller = function () {
    var ctrl = this;

    ctrl.conVencimiento = m.prop(true);
    ctrl.diasCredito    = m.prop(0);
    ctrl.descuento      = m.prop(0);
    ctrl.productos      = m.prop();

    ctrl.keyPrecioUnitario = m.prop();
    ctrl.keyCuentaProducto = m.prop();
    ctrl.keyImpuestoTercero = m.prop();
    ctrl.keyImpuestoProducto = m.prop();

    ctrl.compraVenta(true);

    ctrl.dirActiva = m.prop('fiscal');

    function codigo_pais() {
        var direccion = ctrl.operacion().$direccion();
        console.log(direccion && direccion.codigo_pais());
        if(direccion) return direccion.codigo_pais();
    }

    ctrl.vm.direccion = {
        fiscal : new DireccionComponent.controller({
            direccion_id : ctrl.operacion().tercero_direccion_id,
            codigo_pais : codigo_pais,
            pertenece_a_id : ctrl.operacion().tercero_id,
            enabled : ctrl.vm.enabled
        }),

        envio : new DireccionComponent.controller({
            direccion_id : ctrl.operacion().direccion_envio,
            pertenece_a_id : ctrl.operacion().tercero_id,
            codigo_pais : codigo_pais,
            enabled : ctrl.vm.softEnabled
        })
    };

    //El precio unitario de
    ctrl.precioUnitarioProducto = function (producto) {
        return producto[ctrl.keyPrecioUnitario()];
    };

    ctrl.cuentaProducto = function (producto) {
        return oorden.cuenta(producto[ctrl.keyCuentaProducto()]);
    };

    ctrl.impuestoProducto = function (producto, item) {
        var operacion = ctrl.operacion();
        var impuestoVenta;
        var tercero = operacion.$tercero();

        if(tercero && tercero[ctrl.keyImpuestoTercero()]) {
            //el impuesto definindo del cliente (tercero)
            impuestoVenta = tercero[ctrl.keyImpuestoTercero()];
        } else if(producto[ctrl.keyImpuestoProducto()]){
            // el impuesto definido de la categoría de producto
            impuestoVenta = producto[ctrl.keyImpuestoProducto()];
        }
         return oorden.impuesto(impuestoVenta);
     };

     ctrl.itemRetencion = function (item) {
        return null;
     }

     /*
     *
     */

    ctrl.actualizaFechaVencimiento = function () {
        var fecha = oor.fecha.fromSQL(ctrl.operacion().fecha());
        fecha.setDate( fecha.getDate() + ctrl.diasCredito() );
        ctrl.operacion().fecha_vencimiento( oor.fecha.toSQL(fecha) );
    }

    ctrl.terceroAsignado = function () {
        var tercero = ctrl.operacion().$tercero();

        if(!tercero) {
            ctrl.operacion().$direccion(null);
            return;
        }

        ctrl.terceroOperacionSettings();

        ctrl.opcionesTercero();
        ctrl.actualizaFechaVencimiento();
        ctrl.actualizarDescuentos();
        ctrl.actualizar(true);


        if(ctrl.descuento()) {
            ctrl.operacion().mostrar_descuento(true);
        }

        asignarCuentaOperacion();

        obtenerDirecciones(tercero.tercero_id).then(function (direcciones) {
            var dirFiscal = direcciones.filter(f('direccion_id').is(tercero.ultima_direccion_fiscal));
            if(dirFiscal[0]) {
                ctrl.operacion().tercero_direccion_id(dirFiscal[0].direccion_id());
            }

            var dirEnvio = direcciones.filter(f('direccion_id')
                                .is(tercero.ultima_direccion_envio));

            if(dirEnvio[0]) {
                ctrl.operacion().direccion_envio(dirEnvio[0].direccion_id());
            }
        });
    };



    function asignarCuentaOperacion () {
        ctrl.operacion().cuenta_tercero_id(null);
        console.log(ctrl.operacion().cuenta_tercero_id(null));

        /*
        var url = '/api/terceros/';
        url += ctrl.operacion().$tercero().tercero_id;
        url += '/generar_cuenta/';
        url += ctrl.operacion().seccion() == 'V' ? 'cliente' : 'proveedor';

        m.request({
            method:'POST',
            url:url,
            unwrapSuccess:f('data')
        }).then(function (data) {
            ctrl.operacion().cuenta_tercero_id(data.cuenta_contable_id);
            console.log( ctrl.operacion().cuenta_tercero_id() );
        })
        */
    }


    ctrl.terceroOperacionSettings = function () {
        var op = ctrl.operacion();

        var options = LastUsed.get({
            prefix : op.tipo_documento(),
            on : ctrl.$lastUsed.tercero()
        });

        Object.keys(options).forEach(function (k) {
            if(typeof op[k] == 'function') {
                op[k]( options[k] );
            }
        });

        ctrl.lastUsedTercero = m.prop(options);

        /**
         * Se asigna el tipo de cambio de acuerdo
         *  al last used del tercero
         *  a la clave moneda del tercero
        **/
        var lastUsed = ctrl.lastUsedTercero();
        var tercero = ctrl.operacion().$tercero();
        var claveMoneda = lastUsed.tipo_de_cambio || (tercero && tercero.clave_moneda);

        if(claveMoneda) {
            claveMoneda = oorden.tipoDeCambio(claveMoneda);
            if(claveMoneda) {
                ctrl.operacion().asignarTipoDeCambio( claveMoneda ) 
            }
        }
    }

    ctrl.actualizarDescuentos = function () {
        ctrl.operacion().items.forEach(function (item){
            item.descuento(ctrl.descuento());
        });
    };

    ctrl.itemAsignarProducto = function (item, producto) {
        item.$producto(producto);

        item.unidad( producto.unidad );
        item.producto_nombre( producto.nombre );


        if(!item.cantidad.$int()) {
            item.cantidad(1);
        }


        console.log('asignarProductoCategoria: ', f('categoria_id')(producto));

        var options = LastUsed.get({
            prefix:f('categoria_id')(producto),
            on : ctrl.$lastUsed.tercero()
        });

        options.retencion_id = ctrl.$lastUsed.tercero().retencion_id;


        //Lo trae del producto
        item.precio_unitario( ctrl.precioUnitarioProducto(producto) );


        if(options.impuesto_id) {
            item.asignarImpuesto( oorden.impuesto(options.impuesto_id) );
        } else {
            item.asignarImpuesto( ctrl.impuestoProducto(producto) );
        }

        if(options.retencion_id) {
            item.asignarRetencion( oorden.retencion(options.retencion_id) )
        } else {
            item.asignarRetencion( ctrl.itemRetencion(item) );
        }

        //cuenta del producto o cuenta default
        console.log(options.cuenta_id);

        if(options.cuenta_id) {
            item.asignarCuenta( oorden.cuenta(options.cuenta_id) )
        } else {
            item.asignarCuenta( ctrl.cuentaProducto(producto) );
        }

        if(options.ccto1) {
            item.ccto_1_id( options.ccto1 );
        }

        if(options.ccto2) {
            item.ccto2_id( options.ccto2 );
        }

        ctrl.actualizar(true);
    };


    /**
     * Asigna las cuentas al item, si es que no tiene producto
     */

    function asignarCuentaAlItem(item) {
        if(!item.cuenta_id() && !item.producto_id() ) {
            var options = LastUsed.get({prefix: '', on:ctrl.$lastUsed.tercero()});

            if(options.impuesto_id) {
                item.asignarImpuesto( oorden.impuesto(options.impuesto_id) );
            }

            if(options.retencion_id) {
                item.asignarRetencion( oorden.retencion(options.retencion_id) )
            }

            if(options.cuenta_id) {
                item.asignarCuenta( oorden.cuenta(options.cuenta_id) )
            }

            if(options.ccto1) {
                item.ccto_1_id( options.ccto1 );
            }

            if(options.ccto2) {
                item.ccto2_id( options.ccto2 );
            }
        }
    }


    ctrl.inicializarItem = function (item) {
        item.descuento(ctrl.descuento());
    }

    ctrl.actualizar = function (actualizarOperacion) {
        var operacion = ctrl.operacion();
        if(actualizarOperacion) operacion.actualizar();

        operacion.$errors([]);

        if(!operacion.tercero_id()) {
            operacion.addError('noTercero');
        }

        operacion.items.forEach(function (item) {
            item.$errors([]);

            if(!item.$integrable) {
                item.$integrable = m.prop(false);
            }

            if(item.$integrable() == false) {
                item.$integrable( Boolean(item.producto_nombre()) );
            }

            if(item.importe.$int()) {
                asignarCuentaAlItem(item);
                if(!item.cuenta_id()) {
                    item.addError('noCuenta');
                }

                if(!item.impuesto_conjunto_id()){
                    item.addError('noImpuesto');
                }

                if(!item.producto_nombre()) {
                    item.addError('noConcepto');
                }
            }

            if(item.$errors().length) {
                operacion.addError('item');
            }
        });

        autoLineas();
    }



    ctrl.eliminarItem = function (item) {
        var idx = ctrl.operacion().items.indexOf(item);

        if(idx >= 0) {
            ctrl.operacion().items.splice(idx,1);
            ctrl.operacion().eliminar().push(item.operacion_item_id());
        }

        if(ctrl.eliminarItem.noActualizar) return;
        ctrl.actualizar(true);
    };


    ctrl.eliminarTodosLosItems = function (actualizar) {
        var items = ctrl.operacion().items.map(_.identity);
        var actualizar = (typeof actualizar == 'undefined') ? true : Boolean(actualizar);

        ctrl.eliminarItem.noActualizar = true;

        items.forEach(ctrl.eliminarItem);

        ctrl.eliminarItem.noActualizar = false;

        if(actualizar) {
            ctrl.actualizar(true);
        }
    }


    function autoLineas () {
        if( ctrl.vm.enabled() == false) return;

        var lineasTotales  = ctrl.operacion().items.length;

        if(lineasTotales == 0) {
            agregarLinea();
            agregarLinea();
            agregarLinea();
            agregarLinea();
            return;
        }

        var lineasAIncluir = 0;

        ctrl.operacion().items.forEach(function (linea) {
            if(linea.$integrable()) {
                lineasAIncluir++;
            }
        });

        if(lineasAIncluir == lineasTotales) {
            agregarLinea();
        }
    }


    function agregarLinea () {
        var OperacionItem = oor.mm('OperacionItem');

        var item = new OperacionItem({
            operacion_id : ctrl.operacion().operacion_id(),
            operacion_item_id : oor.uuid()
        });

        ctrl.operacion().items.push(item);

        ctrl.inicializarItem(item);

        ctrl.actualizar();
    }


    function obtenerDirecciones (perteneceA) {
        var Direccion = oor.mm('Direccion');
        return m.request({
            url : '/api/direcciones/'.concat(perteneceA),
            method : 'GET',
            unwrapSuccess : function (r) {
                return r.data.map(function (d) {
                    return new Direccion(d);
                });
            }
        });
    }


    ctrl.loadProductos  = function () {
        m.request({
            url : '/api/productos',
            method : 'GET'
        }).then(function (r) {
            ctrl.productos(r.data);
        })
    }

    ctrl.imprimir = function(){
        var operacion = ctrl.operacion();
        var url = '/exportar/docsOperacion/'+operacion.operacion_id();
        window.open(url, '_blank');
    }

    ctrl.SePuedeImprimir = function(){
        var operacion = ctrl.operacion();
        return !operacion.$new();
    }

    if(ctrl.operacion().$new()){
        if(ctrl.conVencimiento()) {
            ctrl.operacion().fecha_vencimiento( ctrl.operacion().fecha() );
        }
        ctrl.operacion().asignarTipoDeCambio(oorden.tipoDeCambio.base());
    }

}

CompraVenta.moreOptions = function (ctrl) {
    var menuOptions = [
        {icon:'i.glyphicon.glyphicon-print', action:ctrl.imprimir, caption:'Imprimir', show : ctrl.SePuedeImprimir()},
        {icon:'i.ion-ios-copy-outline', action:ctrl.copiar, caption:'Copiar', show : true},
        {caption:'Editar', icon:'i.ion-edit', action :ctrl.habilitarEdicion, show: true},
        {icon:'i.ion-ios-trash', action:ctrl.eliminar, caption:'Eliminar', show : ctrl.eliminable()}
    ];

    return m('div.dropdown.pull-right', [

        m('.btn btn-primary button-striped button-full-striped button-striped btn-default btn-xs dropdown-toggle btn-ripple ', {'data-toggle':'dropdown'},
            ['Acciones  ',m("span.caret"),{style:'width:30px'}]
        ),

        m('ul.dropdown-menu.dropdown-menu-right', [
            menuOptions
                .filter(f('show'))
                .map(function(opt) {
                    return m('li', [
                        m('a[href="javascript:;"]', {onclick:opt.action}, m(opt.icon), ' ', opt.caption)
                    ])
                })
        ])
    ]);
}

CompraVenta.formularioRight = function (ctrl) {
    var tDoc = ctrl.operacion().$tipoDeDocumento();
     var seccion = ctrl.operacion().seccion();

    return [
        NCFSelector(ctrl),

        m('div.flex-row.flex-end', [
            OpDetail.inputReferencia(ctrl),
        ]),

        m('div.flex-row.flex-end', [

            m('.mt-inputer.col-xs-12.col-md-6', [
                m('label', seccion == 'C' ? 'Comprador' : 'Vendedor'),
                ctrl.vm.vendedoresSelector.view()
            ]),

            OpDetail.inputFechaVencimiento(ctrl)

        ]),
        m('.flex-row.flex-end', [
            ctrl.devolucion ? Devolucion.view(ctrl.devolucion()) : null,
            ctrl.monedaComponente
        ])
    ];
}



CompraVenta.view = function (ctrl) {
    var seccion = ctrl.operacion().seccion();

    if(!ctrl.notas) {
        ctrl.notas = new NotasComponent.controller({ pertenece_a_id : ctrl.operacion().operacion_id });
        ctrl.notas.view  = function () { return NotasComponent.view(ctrl.notas); }
    }

    if(!ctrl.pagos && ctrl.operacion().$isNew() == false && (ctrl.operacion().estatus() == 'A') ) {
        ctrl.pagos = m.component(PagarOperacionComponent, {operacion : ctrl.operacion()});
    }

    /*
    m('span.small.pull-right.text-'.concat(ctrl.operacion().$estatus().color), [
        ctrl.operacion().$estatus().nombre
    ]),*/

    return m('div.opv-view', [

        m('.opv-encabezado',[
            m('h6', seccion == 'V' ? 'Cliente' :'Proveedor'),
            m('.opv-tercero', [
                ctrl.vm.terceroComponent,
                m.component(cuentaSelector, ctrl),
                ctrl.operacion().$tercero() ? CompraVenta.direcciones(ctrl) : null
            ]),

            m('.opv-info', [
                CompraVenta.formularioRight(ctrl)
            ])
        ]),

        ctrl.productos() ? m.component(TablaItems, {opCtrl:ctrl}) : '',

        m('div', {style:'margin:10px 0px'}, [
            m.component(TabsComponent, {
                opCtrl : ctrl,
                pagos : ctrl.pagos
            })
        ]),
        m.component(OpDetail.polizaComponent, {
            poliza:ctrl.operacion().$poliza
        }),
    ]);
};



CompraVenta.direcciones = function (ctrl) {
    return m('div', {style:'font-size:11px'},[
        m('span', 'Dirección: '),

        m('span.label.label-sm.cursor-pointer', {
            class : 'label-'.concat(ctrl.dirActiva() == 'fiscal' ? 'amber' : 'default' ),
            onclick : function () { ctrl.dirActiva('fiscal') }
        },'Fiscal'),

        ' ',

        m('span.label.label-sm.cursor-pointer', {
            class : 'label-'.concat(ctrl.dirActiva() == 'envio' ? 'amber' : 'default' ),
            onclick : function () { ctrl.dirActiva('envio') }
        }, 'Entrega'),

        DireccionComponent.view( ctrl.vm.direccion[ctrl.dirActiva()] )
    ]);
}
