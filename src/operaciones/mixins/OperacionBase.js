var itemViewModel   = require('../ItemViewModel.js');
var T3Component     = require('../components/Tercero.js');
var MonedaComponent = require('../components/Moneda.js');
var MonedaComponent = require('../components/Moneda.js');
var FechasBar2      = require('../../misc/components/FechasBar2');
var OperacionBase   = module.exports = {};
var D3Date = FechasBar2.D3Date;


/**
 * El Controlador crea el VM de la Operación con los selectores que se necesitan
 */
OperacionBase.controller = function (params) {
    var OperacionItem = oor.mm('OperacionItem');
    var Operacion     = oor.mm('Operacion');

    var ctrl = this;
    var vm = this.vm = {};



    vm.watcher = function (prop) {
        return function () {
            if(arguments.length) {
                vm.hasChanges(true);
                prop(arguments[0]);
            }
            return prop();
        }
    }

    vm.hasChanges = m.prop(false);

    if( ctrl.operacion().$isPendiente ) {
        vm.hasChanges(true);
    }

    window.addEventListener('beforeunload', function (e) {
        if(vm.hasChanges() == false) return;

        //Si la operación es nueva, no mostrar el mensaje, simplemente guardarla como pendiente para recuperarla
        if(ctrl.operacion().$isNew()) {
            localStorage.setItem('OORDEN/operacion/' + ctrl.operacion().tipo_operacion(),  JSON.stringify(ctrl.operacion()));
            return;
        }

        var confirmationMessage = "Si sales de esta página sin guardar, perderás los cambios realizados";
        e.returnValue = confirmationMessage;
        return confirmationMessage;
    })


    ctrl.compraVenta = m.prop(false);

    vm.enabled = m.prop(false);
    vm.softEnabled = m.prop(false);

    //Aprobada Saldada y Eliminada no empiezan a editarse
    var editarse = ['A','S','X'].indexOf(ctrl.operacion().estatus()) == -1
    if(editarse) vm.enabled(true);
    vm.softEnabled(vm.enabled());

    function tcEnabled () {
        if(ctrl.operacion().$operacionAnterior && ctrl.operacion().$operacionAnterior()) {
            return false;
        }

        if(ctrl.operacion().estatus() == 'S') {
            return false;
        }

        return vm.softEnabled();
    }

    vm.celdasActivas = function () {
        var op = ctrl.operacion();
        var celdas = 0;

        celdas += Number(op.mostrar_cuentas());
        celdas += Number(op.mostrar_impuestos() || op.mostrar_retenciones());
        celdas += Number(op.mostrar_descuento());
        celdas += Number(op.mostrar_cctos());

        return celdas;
    };




    function onterceroChange () {
        var tercero = ctrl.operacion().$tercero();
        ctrl.lastUsedTercero = m.prop(null);
        ctrl.operacion().asignarTercero(ctrl.operacion().$tercero());

        if(tercero) {
            ctrl.$lastUsed.cargarTercero(tercero)
                .then(ctrl.terceroAsignado || angular.noop);
        } else {
            (ctrl.terceroAsignado || angular.noop)();
        }
    };





    var secciones = {V:'cliente', C:'proveedor'};

    ctrl.vm.terceroComponent = m.component(T3Component, {
        tercero    : ctrl.operacion().$tercero,
        tercero_id : ctrl.vm.watcher(ctrl.operacion().tercero_id),
        onchange : onterceroChange,
        enabled    : ctrl.vm.enabled,
        tipo : secciones[ctrl.operacion().seccion()],
        placeholder : 'Seleccionar...'
    });


    ctrl.monedaComponente = m.component(MonedaComponent, {
        moneda : ctrl.monedaCuenta,
        tipo_de_cambio : ctrl.operacion().tipo_de_cambio,
        tasa_de_cambio : ctrl.operacion().tasa_de_cambio,
        codigo_moneda : ctrl.operacion().moneda,
        fecha : ctrl.operacion().fecha,
        tipoDeCambio : ctrl.operacion().$tipoDeCambio,
        tipoDeCambio_label : 'Tipo De Cambio',
        onchange : function () { ctrl.vm.hasChanges(true) },
        key : 'tipo-de-cambio',
        enabled : tcEnabled
    });

    //Tipo de Documento A COMPONENT
    vm.tipoDocSelector = operaciones.tipoDocSelector(ctrl.operacion(), oorden.tiposDeDocumento, ctrl);
    vm.tipoDocSelector.enabled = ctrl.vm.enabled;

    vm.vendedoresSelector = oor.select2({
        data : oorden.vendedores().map(function (v) {
            return {
                id : f('usuario_id')(v),
                text : f('nombre')(v)
            }
        }),
        model : function () {
            if(arguments.length) {
                ctrl.operacion().vendedor_id( arguments[0] );
                ctrl.vm.hasChanges(true);
            }
            return ctrl.operacion().vendedor_id();
        }
    });

    vm.vendedoresSelector.enabled = vm.softEnabled;

    //Metodos de Pago A COMPONENT
    var metodosAUsar = oorden.metodosDePago().filter(function (mp) {
        var metodo = (f('metodo')(mp) || '').toLowerCase();
        var index = ['transferencia', 'cheque', 'tarjeta de credito'].indexOf(metodo);
        return Boolean(index + 1);
    });

    vm.metodoDePagoSelector = operaciones.metodoDePagoSelector(ctrl.operacion(), metodosAUsar);
    vm.metodoDePagoSelector.enabled = ctrl.vm.enabled;


    //Agregar Línea (ítem) Operación
    ctrl.agregarLinea = function () {
        Operacion.agregarLinea( ctrl.operacion() );
    };

    /**
     * View Model de Ítems
     */
    ctrl.itemViewModel = mtk.vmFactory({
        create:itemViewModel.bind(ctrl),
        key:OperacionItem.id
    });
};

/**
 * Dibuja el input de Numero
 */
 OperacionBase.inputNumero = function  (ctrl) {
    var label = (ctrl.operacion().serie() || '') + ' #';
    var numero =  ctrl.operacion().numero();

    return m('span.mt-inputer.numero.line', [
        m('label', label),
        m('input[type="text"].text-left.pull-left', {
            placeholder : '<auto>',
            value : numero ? numero : '',
            disabled : !ctrl.vm.softEnabled(),
            onchange : m.withAttr('value', function (val) {
                var numero = Number(val);
                if(_.isNaN(numero)){
                    toastr.error('Especificar un número o dejar en blanco')
                }
                ctrl.vm.watcher(ctrl.operacion().numero)(numero || '')
            }),
            size : Math.max(6, String(numero).length)
        })
    ]);
};



OperacionBase.inputNumero = function (ctrl) {
    var numero =  ctrl.operacion().numero();
    return [
        m('.mt-inputer.serie', [
            m('label', 'Serie:'),
            m('input[type="text"].text-left.pull-left', {
                value : ctrl.operacion().serie(),
                disabled : 'disabled',
                size : Math.max(2, String(ctrl.operacion().serie()).length)
            })
        ]),

        m('span.mt-inputer.numero', [
            m('label', 'Núm:'),
            m('input[type="text"].text-left.pull-left', {
                placeholder : '<auto>',
                value : numero ? numero : '',
                disabled : !ctrl.vm.softEnabled(),
                onchange : m.withAttr('value', function (val) {
                    var numero = Number(val);
                    if(_.isNaN(numero)){
                        toastr.error('Especificar un número o dejar en blanco')
                    }
                    ctrl.vm.watcher(ctrl.operacion().numero)(numero || '')
                }),
                size : Math.max(6, String(numero).length)
            })
        ])

    ];
}






/**
 * Dibuja el Input de Serie
 */
OperacionBase.inputSerie = function (ctrl) {
    return ctrl.operacion().serie() ? oor.inputer({
        label :'Serie',
        input : 'input[type="text"]"][style="width:40px"].text-right',
        model : ctrl.operacion().serie,
        readonly : true
    }, {style:'flex:1 2 80px'}, {size:4}) : '';
};

/**
 * Dibuja el input de Fecha
 */
OperacionBase.inputFecha = function (ctrl) {
    /*
    return oor.inputer({
        input:'input.text-left[type="date"][style="width:130px"]',
        label:'Fecha',
        model: ctrl.vm.watcher(ctrl.operacion().fecha),
        readonly: !ctrl.vm.enabled()
    }, {
        class : 'fecha'
    });
    */

    if(!ctrl.inputFecha) {
        ctrl.inputFecha = D3Date.mounter(D3Date(), {
            model : ctrl.vm.watcher(ctrl.operacion().fecha)
        });
    }

    return m('.mt-inputer', [
        m('label', 'Fecha'),
        m('input[type=text]', {config : ctrl.inputFecha})
    ])
};



/**
 * Dibuja el input de Fecha
 */
OperacionBase.inputFechaVencimiento = function (ctrl) {
    return oor.inputer({
        input:'input.fecha-vencimiento.text-left[type="date"]',
        label:'Vence',
        model: ctrl.vm.watcher(ctrl.operacion().fecha_vencimiento),
        readonly: !ctrl.vm.softEnabled()
    },{
        class:'fecha-vencimiento col-xs-12 col-md-6'
    });
};

/**
 * Dibuja el input de Referencia
 */
//var operacionTooltip = m.prop(oorden.obtenerAyuda('op-referencia').then(operacionTooltip));

OperacionBase.inputReferencia = function (ctrl) {
     return oor.inputer({
        placeholder: 'Referencia...',
        label: 'Referencia',
        model: ctrl.vm.watcher(ctrl.operacion().referencia),
        readonly : !ctrl.vm.softEnabled()
    }, {
        // 'data-tip' : operacionTooltip(),
        class:'col-xs-12'
    });
};




OperacionBase.inputTipoDeCambio = function (ctrl) {
    return m('div.mt-inputer', [
        m('label', 'Tipo De Cambio'),
        ctrl.vm.tipoDeCambioSelector.view({
            style:''
        })
    ]);
};


/**
 * Dibuja el input de Referencia
 */
OperacionBase.inputTasaDeCambio = function (ctrl) {
     return oor.inputer({
        placeholder: 'Tasa',
        label:'Tasa de Cambio',
        model:ctrl.operacion().tasa_de_cambio,
        readonly : !ctrl.vm.enabled() ,
        size : 10
    });
};

/**
 * Tipo De Documento Selector
 */
OperacionBase.inputTipoDoc = function (ctrl) {
    return m('div.mt-inputer', {style:'flex-grow:4'}, [
        ctrl.vm.tipoDocSelector.view({style:'width:120px'})
    ]);
}

/**
 * Método de Pago Selector
 */
OperacionBase.inputMetodoDePago = function (ctrl) {
    return m('div.mt-inputer', [
        m('label','Método de Pago'),
        ctrl.vm.metodoDePagoSelector.view({style:'min-width:120px'})
    ], {style:'flex:50% 2 2'});
}

/**
 * Método de Pago Formulario
 */
OperacionBase.metodoDePagoFormulario = function (ctrl, ingreso) {
    var metPago = ctrl.operacion().$metodoDePago();
    metPago = metPago ? metPago.metodo.toLowerCase() : null;
    metPago = metPago ? OperacionBase.metodoDePagoFormulario[metPago] : null;
    return metPago ? metPago(ctrl, ingreso) : null;
};

OperacionBase.metodoDePagoFormulario.cheque = function (ctrl) {
    return [
        oor.inputer({
            input : 'input.text-right[type="text"]',
            label : 'Num Cheque',
            placeholder : '',
            model : ctrl.operacion().num_cheque,
            readonly: !ctrl.vm.enabled()
        }, null, {size:10, 'class':'text-right'}),

        oor.inputer({
            input : 'input.text-right[type="date"]',
            label : 'Fecha Cheque',
            placeholder : '',
            model : ctrl.operacion().fecha_cheque,
            readonly: !ctrl.vm.enabled()
        }, null, {'class':'text-right'})
    ];
};

OperacionBase.metodoDePagoFormulario.transferencia = function (ctrl, ingreso) {
    return ingreso ? [
        oor.inputer({
            label : 'Banco Origen.',
            placeholder : 'Banco...',
            model : ctrl.operacion().nombre_banco_destino,
            readonly: !ctrl.vm.enabled()
        }),
        oor.inputer({
            label : 'Cuenta Origen',
            placeholder : '123456124',
            model : ctrl.operacion().cuenta_banco_destino,
            readonly: !ctrl.vm.enabled()
        })
    ] : [
        oor.inputer({
            label : 'Banco Destino',
            placeholder : 'Banco...',
            model : ctrl.operacion().nombre_banco_destino,
            readonly: !ctrl.vm.enabled()
        }),

        oor.inputer({
            label : 'Cuenta Destino',
            placeholder : '123456124',
            model : ctrl.operacion().cuenta_banco_destino,
            readonly: !ctrl.vm.enabled()
        })
    ];
};


OperacionBase.metodoDePagoFormulario['tarjeta de credito'] = function (ctrl, ingreso) {
    return [
        oor.inputer({
            label : 'Terminación Tarjeta',
            placeholder : '1299',
            model : ctrl.operacion().cuenta_banco_destino,
            readonly: !ctrl.vm.enabled()
        })
    ];
};


OperacionBase.actionBar = function (ctrl) {
     return m('div.action-panel', [
        m('div.btn.btn-sm.btn-default', {onclick: ctrl.cancelar}, 'Cancelar'),

        ctrl.operacion().$new() ? '' : m('div.btn.btn-sm.btn-danger', {
            onclick: ctrl.eliminar
        }, 'Eliminar'),

        ctrl.vm.enabled() ? m('div.btn.btn-sm.btn-primary', {
            onclick: ctrl.guardar
        }, 'Guardar') : '',

         ctrl.vm.enabled() ? m('div.btn.btn-sm.btn-success.pull-right', {
            onclick: ctrl.puedeAutorizar() ? ctrl.autorizar : ctrl.porAutorizar
        }, ctrl.puedeAutorizar() ? 'Autorizar' : 'Enviar a Autorización') : ''
    ])
}


OperacionBase.polizaComponent = {};

OperacionBase.polizaComponent.view = function (ctrl, args)  {
    var poliza = args.poliza();

    return m('div.highlight.grey', {
        style:'display:'.concat(poliza ? 'block' : 'none')
    }, [
        m('strong', 'Afectación Contable: '),
        poliza ? m('a', {
            href:'/polizas/poliza/'.concat(poliza.poliza_id)
        }, poliza.concepto) : ''
    ]);
}
