var OpEgreso = module.exports = {};
var OpDetail   = require('./OperacionBase.js');
var OpTransaccion = require('./OperacionTransaccion.js');

/** 
 * BASE DE EGRESOS PaRA
 *   EOT y EPP
 * NO SE USA REVISAR
 */
OpEgreso.controller = function (params) {
    var ctrl = this;

    console.log('egreso');

    OpTransaccion.controller.call(ctrl,params);

    ctrl.cuentasSelector = function () {
        return oorden.cuentas().sort(function (c1,c2) {
            //Se ponen al principio las cuentas con 5
            if(c1.cuenta[0] != c2.cuenta[0]) {
                if(c1.cuenta[0] == '5') return -1;
                else if(c2.cuenta[0] == '5') return 1;
            }

            if (c1.cuenta < c2.cuenta) return -1;
            if (c1.cuenta > c2.cuenta) return +1;
            return 0;
        })
    }

    ctrl.terceroAsignado = function () {
        /*
        var settings = ctrl.settings();

        if(settings.tercero.metodo === 'transferencia') {
            ctrl.operacion().asignarMetodoDePago( ctrl.metodosDePago.transferencia );  
            asignarConfig(ctrl.operacion(), ['cuenta_banco_destino','clave_banco_destino','nombre_banco_destino'], settings.tercero);
        } else if(settings.tercero.metodo == 'cheque') {
            ctr.operacion().asignarMetodoDePago( ctrl.metodosDePago.cheque );
        }
        */

        alert(0);
        console.log( ctrl.$lastUsed.tercero() )

    }

    function asignarConfig (operacion, keys, settings) {
        keys.forEach(function (k) {
            if(settings[k] && (typeof operacion[k] === 'function')) {
                operacion[k](settings[k]);
            }
        });
    }

    ctrl.operacion().forma_pago_id.onchange = function () {
        var op = ctrl.operacion();
        var mt = op.$metodoDePago();

        if(mt.metodo.toLowerCase() == 'cheque') {
            op.fecha_cheque( op.fecha() );
            op.num_cheque( op.$cuentaBanco().numero_cheque);
        }
    }

};