
module.exports = TablaDocumentos;

function TablaDocumentos () {
    var ctrl = this;
    var Aplicacion = oor.mm('AplicacionDePago');

    var documentos = MtTable({
        columnas : ['fecha','documento','fecha_vencimiento','fecha_esperada','total','saldo','aplicar'],
        defCol : defCol.call(this)
    });

    /** 
     * Método para actualizar los documentos
     */ 
    documentos.cargar = function () {
        return m.request({
            method:'GET',
            url:'/apiv2?' + m.route.buildQueryString({
                modelo:'operaciones', 
                tercero_id:ctrl.operacion().tercero_id(),
                include : 'operaciones.tercero,operaciones.tipoDocumento',
            })
        }).then(function (r) {

            var documentos = r.data.filter(ctrl.filtrarDocumentos).map(function (item) {
                item.checked = m.prop(false);
                return item;
            });


    
           
            //Asigno los documentos a la operacion
            ctrl.operacion().asignarDocumentos(documentos);

            //A la tabla
            ctrl.documentos.items(documentos);
        });
    };


     /** 
     * Cambia el valor del monto_pagado
     * y como consecuencoa de monto_pago
     * A la definicion de Columnas
     */
    ctrl.aplicacionSetValue = function (item, value) {
        var pago = item.pago();
        
        if(Number(value) < 0) {
            value = 0;
        }

        pago.monto_pagado(value);
        ctrl.operacion().actualizarPagos();        
    };


    /** 
     * Es lo que sucede al seleccionar un item o ducmuento de la tabla
     * A la definicion de Columnas
     */
    ctrl.aplicacionFocus = function (documento, force) {
        if(force != true && documento.pago()) return;
        ctrl.operacion().crearPago(documento);
    };



    return documentos;

}


function defCol () {
    var columns = {};

    columns.fecha = {
        caption : 'Fecha'
    }

    columns.fecha_vencimiento = {
        caption : 'Vence'
    }

     columns.fecha_esperada = {
        caption : 'F. Esperada',
        value : function (item) {
            return item.fecha_esperada ? item.fecha_esperada : item.fecha_vencimiento
        }
    }

    columns.referencia = {
        caption : 'Referencia'
    }

    columns.documento = {
        caption : 'Documento',
        value : function (item) {
            return item.tipoDocumento.nombre + ' # ' + item.serie + ' ' + item.numero;
        }
    };


    columns.total = {
        caption : 'Total',
        value : function (doc) {
            return [
                m('span',doc.total()),
                m('span',' ' + doc.moneda)
            ];
        }
    };

    columns.saldo = {
        caption : 'Saldo',
        value : function (doc) {
            return doc.saldoMonedaCta();
        }
    };

    columns.tasa_de_cambio = {
        value : function (doc) {
            return doc.pago() ? doc.pago().tasa_de_cambio() :'';
        }
    };

    columns.extra = {
        value : function (item) {
            return 'x'.concat( item.factorMonedaCta(), '(PAGO: ', item.pago() ? item.pago().monto_pago(): 'NO', ')');
        }
    };

    columns.aplicar = {
        caption : 'Aplicar',
        value : function (item) {
            var pago = item.pago();
            var disabled = this.vm.enabled() ? '' : '[disabled=disabled]';

            return m('input.text-right'.concat(disabled), {
                onfocus  : this.aplicacionFocus.bind(null, item),
                onchange : m.withAttr('value',this.aplicacionSetValue.bind(null,item)),
                ondblclick : function () {
                    this.aplicacionFocus(item,true);
                }.bind(this),
                value    : pago ? pago.monto_pagado() : '',
                config   : function (node) {
                    //node.setCustomValidity(pago && pago.monto_pago && pago.monto_pago.err() ? 'No puede pagarse una cantidad mayor al saldo' : (void 0))
                }
            });

        }.bind(this)
    };


    return columns;
};
