var IOTComponent = module.exports = {};
var tablaItems    = require('./TablaItems.js');
var OperacionTransaccion   = require('./mixins/OperacionTransaccion.js');
var OperacionBase   = require('./mixins/OperacionBase.js');


IOTComponent.controller = function (params) {
    var ctrl = this;
    OperacionTransaccion.controller.call(ctrl);
};

IOTComponent.view = function (ctrl) {
    var cuentaBanco = ctrl.operacion().$cuentaBanco();

    return m('div.opv-view.transaccion-view', [
        m('div.transaccion-form.flex-row', {}, [
            m('div.transaccion-left', [
                m('h3.column-title', 'Remitente'),
                ctrl.vm.terceroComponent,

                m('h5', 'Cuenta: ', m('strong', cuentaBanco.banco + '(' + cuentaBanco.cuenta_bancaria +')')),
                OperacionBase.inputMetodoDePago(ctrl), 
                m('div.flex-row.flex-end', OperacionBase.metodoDePagoFormulario(ctrl, true))
            ]),
            m('div.transaccion-right', [
                m('h3.column-title', 'Documento'),
                m('div.flex-row.flex-end', [
                    OperacionBase.inputTipoDoc(ctrl),
                    OperacionBase.inputSerie(ctrl),
                    OperacionBase.inputNumero(ctrl)
                ]),
                m('div.flex-row.flex-end',
                    OperacionBase.inputFecha(ctrl)
                ),

                OperacionBase.inputReferencia(ctrl),
                ctrl.monedaComponente
            ])
        ]),
    
    
        m('br'),
        
        m('div', [
            ctrl.vm.enabled() ? m('div.pull-right.btn.btn-primary.btn-sm', {onclick:ctrl.agregarLinea}, '+ Línea') : '',
            m('h3.column-title', 'Detalle'),
            tablaItems(ctrl)
        ]),


        m.component(OperacionBase.polizaComponent, {
            poliza:ctrl.operacion().$poliza
        })
    ]);
};

