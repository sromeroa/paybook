var COMComponent    = module.exports  = {};

var OpDetail        = require('./mixins/OperacionBase.js');
var OpCompraVenta  = require('./mixins/OperacionCompraVenta.js');

COMComponent.controller = function (params) {
    var ctrl = this;

    OpDetail.controller.call(this, params);
    OpCompraVenta.controller.call(this, params);

    ctrl.keyPrecioUnitario('precio_unitario_compra');
    ctrl.keyCuentaProducto('cuenta_compra');
    ctrl.keyImpuestoProducto('impuesto_compra');
    ctrl.keyImpuestoTercero('impuesto_compra_id');

    ctrl.opcionesTercero = function () {
        var tercero = ctrl.operacion().$tercero();
        if(!tercero) return;
        ctrl.diasCredito(Number(tercero.dias_credito_compra) || 0);
    };

    ctrl.opcionesTercero();
    ctrl.actualizar(true);
    ctrl.loadProductos();
};

COMComponent.view = OpCompraVenta.view;
