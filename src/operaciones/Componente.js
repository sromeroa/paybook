var OpComponent = module.exports = {};
var OpDetail = require('./OperacionCaptura.js');
var OpBase = require('./mixins/OperacionBase');
var ObtenerOperacion = require('./ObtenerOperacion');
var ActionBar = require('./ActionBar');
var SettingsHelper = require('./SettingsHelper');
var LastUsed = require('./helpers/lastUsed');
var CompraVenta = require('./mixins/OperacionCompraVenta');

OpComponent.controller = function (args) {
    var ctrl          = this;
    var tipoOperacion = String(args.tipoOperacion).toUpperCase();
    var Operacion     = oor.mm('Operacion');
    var search        = m.route.parseQueryString(location.search);

    oor.fondoGris();

    ctrl.settings       = m.prop({});
    ctrl.operacion      = m.prop();
    ctrl.tipoOperacion  = m.prop(tipoOperacion);
    ctrl.component      = m.prop();
    ctrl.loading        = m.prop();
    ctrl.guardando      = m.prop();
    ctrl.centrosDeCosto = m.prop([]);

    ctrl.loading(true);

    ctrl.$settings = new SettingsHelper({ tipoOperacion : ctrl.tipoOperacion });
    ctrl.$lastUsed = LastUsed({ tipoOperacion : ctrl.tipoOperacion() });

    initialize();

    function initialize () {
        oorden().then(function () {
            ObtenerOperacion(args.operacionId, tipoOperacion, args.cuentaId)
                .then(loadTercero, function(err) {
                    toastr.error(err);
                    return null;
                })
                .then(function (op) {
                    if(!op) return;

                    return ctrl.$lastUsed.cargar().then(function () {
                        return op;
                    });
                })
                .then(loadOperacion)
        });

        m.redraw();
    }

    function loadTercero (operacion) {
        if(!operacion) return;
        if(search.tercero_id) {
            return m.request({
                url : '/apiv2?modelo=terceros&tercero_id=' + search.tercero_id,
                unwrapSuccess : function (r) {
                    var tercero = r.data[0];
                    tercero && operacion.asignarTercero(tercero);
                    return operacion;
                }
            });
        }
        return operacion;
    }


    function loadOperacion (operacion) {
        if(!operacion) return;
        ctrl.operacion(operacion);
        var centrosDeCosto = [];
        var cctosActivos = oorden.cctos.activos();

        if( operacion.$new() ) {
            var tipoOp   = operacion.tipo_operacion();
            var tiposDoc = oorden.tiposDeDocumento[tipoOp]();
            var tipoDocUsed;


            if(cctosActivos[0]) operacion.ccto1(cctosActivos[0]);
            if(cctosActivos[1]) operacion.ccto2(cctosActivos[1]);


            if(ctrl.$lastUsed.user()) {
                tipoDocUsed = ctrl.$lastUsed.user().tipo_documento_id;

                //Buscar el tipo de documento usado
                if(tipoDocUsed) {
                    tipoDocUsed = oorden.tipoDeDocumento(tipoDocUsed)
                }
            }

            //Si no hay setting el primer tipo de cambio
            if(!tipoDocUsed) {
                tipoDocUsed = tiposDoc[0]
            }


            var options = LastUsed.get({
                prefix:tipoDocUsed.tipo_documento_id,
                on: ctrl.$lastUsed.user()
            });

            Object.keys(options).forEach(function (k) {
                if(typeof operacion[k] == 'function') {
                    operacion[k]( Boolean(Number(options[k])) );
                }
            })

            operacion.asignarTipoDeDocumento(tipoDocUsed);
            operacion.fecha( oor.fecha.toSQL(new Date) );
            operacion.sucursal_id( oorden.sucursal().sucursal_id );
        }

        if(centrosDeCosto.length == 0 && cctosActivos.length > 0){
            if(cctosActivos[0]) operacion.ccto1(cctosActivos[0]);
            if(cctosActivos[1]) operacion.ccto2(cctosActivos[1]);
        }

        if(operacion.ccto1()) {
            centrosDeCosto.push( operacion.ccto1() );
        }

        if(operacion.ccto2()) {
            centrosDeCosto.push( operacion.ccto2() );
        }

        if(!operacion.estatus()) {
            operacion.estatus('P');
        }

        ctrl.centrosDeCosto(centrosDeCosto);
        operacion.seccion(operacion.tipo_operacion()[0]);

        ctrl.component(OpDetail[ctrl.tipoOperacion()]);
        ctrl.component().controller.call(ctrl);

        //Settings iniciales
        var tipo_documento = ctrl.$settings.getKey('tipo_documento', null);

        if(tipo_documento) {
            ctrl.operacion().tipo_documento(tipo_documento);
        }


        setTimeout(function () {
            var tercero = ctrl.operacion().$tercero();
            if(tercero) {
                if(ctrl.operacion().$new()) {
                    ctrl.operacion().tercero_id.onchange();
                } else {
                    ctrl.$lastUsed.cargarTercero( ctrl.operacion().$tercero() );
                }
            }
        },100);

        ActionBar.controller.call(ctrl);
        ctrl.loading(false);
    }
};


OpComponent.view = function (ctrl) {
    if(ctrl.loading()) {
        return m('i.fa.fa-spin.fa-refresh');
    }

    var tDoc = ctrl.operacion().$tipoDeDocumento();

    return oor.panel({
        title : m('.flex-row', {style:'margin:0!important'},[
            ctrl.operacion().$new() ? m('span', {style:'padding:4px 12px 4px 0'}, 'Nueva ') : null,

            m('div', [
                ctrl.vm.enabled() ? ctrl.vm.tipoDocSelector.view({
                    style:'width:160px'
                }) : tDoc.nombre,
            ]),

            OpBase.inputNumero(ctrl),
            OpBase.inputFecha(ctrl),
        ]),
        buttons : [
            ActionBar.timbrarCFDI(ctrl),
            CompraVenta.moreOptions(ctrl),
        ]
    }, [
        ctrl.component().view(ctrl),
        ActionBar.view(ctrl),
        MTModal.view()
    ])
};
