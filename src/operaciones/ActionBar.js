
var ActionBar = module.exports = {};

/**
 * Esta es la barra de botones inferiores de la operación
 */
ActionBar.controller = function () {
    var ctrl = this;
    var Operacion = oor.mm('Operacion');

    /**
     * Puede eliminarse la operacion?
     */
    ctrl.puedeEliminar = function () {
        if( ctrl.operacion().$new() ) return false;
        if( ['A','S'].indexOf(ctrl.operacion().estatus()) >= 0) return false;
        return ctrl.operacion().estatus() != 'X' ? true : false;
    };

    /**
     * Puede Gardarse la operación?
     */
    ctrl.puedeGuardar = function () {
        return ctrl.vm && (ctrl.vm.enabled() || ctrl.vm.softEnabled());
    };

    /**
     * Puede aplicarse la operación?
     */
    ctrl.puedeAplicar = function () {
        if(ctrl.vm && ctrl.vm.enabled()) {
            if(['P','T'].indexOf(ctrl.operacion().estatus()) >= 0 ){
                return true
            }
        }
        return false;
    };


    ctrl.guardable = function () {
        return true;
    };

    ctrl.finalizable = function () {
        return true;
    };

    ctrl.eliminable = function () {
        return ['P','T'].indexOf(ctrl.operacion().estatus()) >= 0 ? true : false;
    };

    ctrl.eliminar = function () {
        m.request({
            url : '/apiv2/eliminar/' + ctrl.operacion().operacion_id() + '?modelo=operaciones',
            method : 'DELETE'
        })
        .then(afterSave(false,'Operacion Eliminada'), apiError)
    };


    ctrl.copiar = function () {
        window.location = '/operaciones/' + ctrl.operacion().tipo_operacion().toLowerCase() + '/crear?copiar=' + ctrl.operacion().operacion_id()
    }

    ctrl.habilitarEdicion = function () {
        var estatus = ctrl.operacion().estatus();

        if(estatus == 'X') {
            toastr.error('No se puede editar una operación cancelada')
            return;
        }

        /*
        if (ctrl.pagosComponent.pagos().length !== 0) {
            return toastr.error('No se pueden editar Operaciones con Pagos')
        }
        */
        ctrl.vm.softEnabled(true);
        //if(estatus == 'A') return;

        ctrl.vm.enabled(true);
    }

    /**
     * Proceso de autorizer,
     *  - emite una notificación si falla la validación
     */
    ctrl.autorizar = function (capturarNueva) {
        //Si se está guardando no hace nada
        if(ctrl.guardando()) return;

        //Mensajes de Error que se muestran
        var messages = {
            'item'      : 'Revisar los ítems',
            'pagos'     : 'Revisar los pagos',
            'noTercero' : 'Falta Especificar '.concat(ctrl.operacion().seccion() == 'V' ? 'Cliente' : 'Proveedor')
        };

        //Actualizo la operación
        ctrl.actualizar && ctrl.actualizar(true);
        var errors = ctrl.operacion().$errors();

        if(errors.length) {
            return toastr.error( messages[errors[0]] );
        }

        //Activar la bandera de guardando
        ctrl.guardando(true);

        ctrl.preGuardado()
            .then(function () {
                Operacion.save(ctrl.operacion, {background:true})
                    .then(function () {
                        return Operacion.estatus(ctrl.operacion(),'A').then(afterSave(capturarNueva), apiError)
                    } , apiError);

            }, apiError)



         m.redraw();
    };

    ctrl.preGuardado = function () {
        var deff = m.deferred();

        //No hay numero no comprobar duplicados
        if(! ctrl.operacion().numero() )
        {
            deff.resolve(true);
            return deff.promise;
        }

        var data = {
            modelo:'operaciones',
            tipo_operacion : ctrl.operacion().tipo_operacion(),
            numero:ctrl.operacion().numero(),
            'operacion_id!' : ctrl.operacion().operacion_id(),
            'estatus!' : 'X'
        };

        if(ctrl.operacion().serie())
        {
            data.serie = ctrl.operacion().serie()
        }

        oor.request('/apiv2', 'GET', {data : data}).then(function (d) {
            if(d.length) return deff.reject('¡Combinación Serie y Número duplicada!');
            return deff.resolve(true)
        });

        return deff.promise;
    }

    /**
     * Proceso de guardado
     */
    ctrl.guardar = function (capturarNueva) {
        //Si se está guardando no hace nada
        if(ctrl.guardando()) return;
        //Se actualiza la operacion
        ctrl.actualizar && ctrl.actualizar(true);

        //Marcar que está guardando
        ctrl.guardando(true);

        var d = JSON.parse(JSON.stringify(ctrl.operacion()));

        ctrl.preGuardado()
            .then(function () {
                return Operacion.save(ctrl.operacion(),{background:true})
                            .then(afterSave(capturarNueva), apiError)
            }, apiError)



        m.redraw();
    };


    /**
     * Cancelar regresa a la lista de operaciones
     */
    ctrl.cancelar = function () {
        var url;
        if(ctrl.operacion().seccion() == 'V' || ctrl.operacion().seccion() == 'C') {
            url = '/operaciones/' + ctrl.operacion().seccion().toLowerCase();
        } else {
            url ='/bancos/ver/' + ctrl.operacion().cuenta_banco_mov();
        }
        location.pathname = url;
    };


    function apiError(r) {
        ctrl.guardando(false);
        toastr.error(_.isString(r) ? r : r.status.message);
    }

    function afterSave (redirect, message) {
        var url;
        message || (message = 'Operación Guardada');

        ctrl.vm.hasChanges(false);

        if(ctrl.operacion().seccion() == 'V' || ctrl.operacion().seccion() == 'C') {
            url = '/operaciones/' + ctrl.operacion().seccion().toLowerCase();
        } else {
            url ='/bancos/ver/' + ctrl.operacion().cuenta_banco_mov();
        }

        if(redirect) {
            url = '/operaciones/' + ctrl.operacion().tipo_operacion().toLowerCase() + '/crear';
        }


        return function () {
            ctrl.guardando(false);
            lsMessage({text:message})
            location.pathname = url;
        }
    }
}


/**
 * Vista de la barra inferior de operación
 */
ActionBar.view = function (ctrl) {
    if(!ctrl.operacion()) return '';

    return m('div.action-panel#operaciones-actions', [
        m('div.pull-left', [
            m('button.btn.btn-danger.btn-xs.button-striped.button-full-striped.btn-ripple', {
                onclick: ctrl.cancelar
            }, m('i.ion-ios-arrow-back'),' Cancelar'),

            /*
            ctrl.puedeEliminar() ? m('button.btn.btn-danger.btn-sm', {
                onclick : ctrl.eliminar,
                'class' : ctrl.eliminable() ? '' : 'disabled',
            }, m('i.ion-ios-trash'), ' Eliminar') : ''
            */
        ]),

        m('div.pull-right', [
            ctrl.puedeGuardar() ? ActionBar.guardarDropdown(ctrl) : null,
            ctrl.puedeAplicar() ? ActionBar.aplicarDropdown(ctrl) : null
        ]),

        /*
        m('.clear'),
        m('div', {style:'padding:12px 12px 12px 12px'}, [
            m('label.pull-right', [
            m('input[type=checkbox]'),
                ' Capturar Nueva'
            ])
        ]),
        */

        m('.clear')
    ]);
};

/**
 * Dropdown de Guardar
 */
ActionBar.guardarDropdown = function (ctrl) {
    return m('div.btn-group.dropup', [
        ActionBar.guardarBtn(ctrl),

        m('button.btn.btn-primary.button-striped.button-full-striped.btn-ripple.btn-sm.dropdown-toggle[data-toggle="dropdown"]', {
            'class' : ctrl.guardable() ? '' : 'disabled',
        }, m('span.caret')),

        m('ul.dropdown-menu.dropdown-menu-right', [
            m('li', m('a[href="#"]', {
                onclick : ctrl.guardar.bind(ctrl, true)
            }, 'Guardar y Capturar Nueva'))
        ])
    ]);
};


ActionBar.guardarBtn = function (ctrl) {
    return m('button.btn.btn-primary.button-striped.button-full-striped.btn-ripple.btn-sm', {
        'class' : ctrl.guardable() ? '' : 'disabled',
        onclick : ctrl.guardar.bind(ctrl, false)
    }, m(ctrl.guardando() ? 'i.fa.fa-spinner.fa-spin' : 'i.ion-archive'),' Guardar');
}


/**
 * Dropdown de Aplicar
 */
ActionBar.aplicarDropdown = function(ctrl) {
    return m('div.btn-group.dropup', [
        ActionBar.aplicarBtn(ctrl),
        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-sm.dropdown-toggle[data-toggle="dropdown"]', {
            'class' : ctrl.finalizable() ? '' : 'disabled',
        }, m('span.caret')),
        m('ul.dropdown-menu.dropdown-menu-right', [
            m('li', [
                m('a[href="javascript:;"]', {onclick : ctrl.autorizar.bind(ctrl, true)}, 'Aplicar y Capturar Nueva')
            ])
        ])
    ]);
};

ActionBar.aplicarBtn = function (ctrl) {
    return  m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-sm', {
            'class' : ctrl.finalizable() ? '' : 'disabled',
            onclick : ctrl.autorizar.bind(ctrl,false)
        }, [
            m(ctrl.guardando() ? 'i.fa.fa-spinner.fa-spin' : 'i.ion-checkmark-round'),
            ' Aplicar'
        ]);
}


ActionBar.timbrarCFDI = function (ctrl) {
    return oor.stripedButton('button.btn-success', 'Timbrar CFDI', {
        style   : 'margin:0 10px',
        onclick : function() { timbrarCFDI.initialize.call(this, ctrl) }
    });
}


var  timbrarCFDI = {};

timbrarCFDI.initialize = function (ctrl) {
    MTModal.open({
        controller : timbrarCFDI.controller,
        args : {
            operacionId : ctrl.operacion().operacion_id()
        },
        top : m('h4', 'Timbrar CFDI'),
        bottom : function (ctx) {
            return [
                timbrarCFDI.btnTimbrar(ctx)
            ]
        },
        content : timbrarCFDI.view,
        el : this,
        modalAttrs : {
            'class' : 'modal-medium'
        }
    })
}





timbrarCFDI.controller = function (args) {
    var $this = this;

    this.metodo_de_pago = m.prop();
    this.loading = m.prop(false)
    this.operacion = m.prop();
    this.timbrando = m.prop(false);
    this.error = m.prop(false)

    this.datosCfdi = {
        fechaHora : m.prop(),
        lugarExpedicion : m.prop(''),
        condicionesDePago : m.prop(''),
        metodoDePago : m.prop(''),
        formaDePago : m.prop(''),
        numCtaPago : m.prop('')
    }


    this.metodosSelector = oor.select2({
        data : oorden.metodosDePago().sort(function (d, d2) { return Number(d.clave_oficial) - Number(d2.clave_oficial) }).map(function (d) {
            return {id : d.id, text : d.clave_oficial + ' ' + d.metodo }
        }),
        model : this.metodo_de_pago,
        find : oorden.metodoDePago,

        onchange : function () {
            var val = $this.metodosSelector.selected();
            if(val) {
                $this.datosCfdi.metodoDePago(val.clave_oficial + ' ' + val.metodo)
            } else {
                $this.datosCfdi.metodoDePago('')
            }
        }
    });



    this.initialize = function () {
        if(this.operacion() || this.loading()) return;
        this.loading(true);

        oor.request('/apiv2', 'GET', {
            data : {
                modelo:'operaciones',
                operacion_id : args.operacionId,
                include : 'operaciones.sucursal,operaciones_sucursal.direcciones'
            }
        })
        .then(f('0'))
        .then(this.operacion)
        .then(asignarValores)
        .then(this.loading.bind(null,false))
    }


    this.timbrar = function () {
        if($this.timbrando()) return;

        $this.timbrando(true);
        $this.error(false);

        oor.request('/facturacion/timbrar/' + args.operacionId, 'POST', {
            data : $this.datosCfdi
        })
        .then(successMsg, errorMsg)
        .then($this.timbrando.bind(false, false))
        .then(function () {
            if($this.error() == false && $this.$modal) {
                ctx.$modal.close();
            }
        });

        m.redraw();

    }

    function successMsg ()  { $this.error(false); toastr.success('CFDI Timbrado Correctamente') }
    function errorMsg (r)   { $this.error(r.status.message); toastr.error('Ha ocurrido un error al timbrar') }


    function asignarValores (operacion) {
        console.log(operacion)
        var hora = new Date();

        hora = oor.fecha.to10(hora.getHours()) + ':' + oor.fecha.to10(hora.getMinutes()) + ':' + oor.fecha.to10( hora.getSeconds() );
        $this.datosCfdi.fechaHora( operacion.fecha + 'T' + hora);

        if(operacion.sucursal && operacion.sucursal.direcciones && operacion.sucursal.direcciones[0]) {
            $this.datosCfdi.lugarExpedicion( operacion.sucursal.direcciones[0].ciudad_o_municipio + ' ' +operacion.sucursal.direcciones[0].estado_o_region )
        }
    }

}

timbrarCFDI.view = function (ctx) {
    ctx.initialize();

    return ctx.loading() ? oor.loading() : m('div', [
        m('table.table.megatable', [

            m('tr', [
                m('th', 'Fecha y Hora'),
                m('td',[
                    m('input[type=text]',{
                        value : ctx.datosCfdi.fechaHora(),
                        onchange : m.withAttr('value', ctx.datosCfdi.fechaHora)
                    })
                ])
            ]),

            m('tr', [
                m('th', 'Método de Pago'),
                m('td', ctx.metodosSelector.view())
            ]),

            m('tr', [
                m('th', 'Num Cta Pago'),
                m('td', [
                    m('input[type=text]',{
                        value : ctx.datosCfdi.numCtaPago(),
                        onchange : m.withAttr('value', ctx.datosCfdi.numCtaPago)
                    })
                ])
            ]),

            m('tr', [
                m('th', 'Forma de Pago'),
                m('td', [
                    m('input[type=text]',{
                        value : ctx.datosCfdi.formaDePago(),
                        onchange : m.withAttr('value', ctx.datosCfdi.formaDePago)
                    })
                ])
            ]),

            m('tr', [
                m('th', 'Condiciones de Pago'),
                m('td', [
                    m('input[type=text]', {
                        value : ctx.datosCfdi.condicionesDePago(),
                        onchange : m.withAttr('value', ctx.datosCfdi.condicionesDePago)
                    })
                ])
            ]),

            m('tr', [
                m('th', 'Lugar de Expedición'),
                m('td',[
                    m('input[type=text]', {
                        value : ctx.datosCfdi.lugarExpedicion(),
                        onchange : m.withAttr('value', ctx.datosCfdi.lugarExpedicion)
                    })
                ])
            ])
        ]),

        ctx.timbrando() ? oor.loading() : null,
        ctx.error() ? m('.alert.alert-danger', ctx.error()) : null
    ]);
}

timbrarCFDI.btnTimbrar = function (ctx) {
    return oor.stripedButton('button.btn-success.pull-right', 'Timbrar CFDI', {
        'class' : ctx.timbrando() ? 'disabled' : '',
        onclick : ctx.timbrar
    })
}
