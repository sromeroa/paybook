var CNCComponent    = module.exports  = {};

var OpDetail        = require('./mixins/OperacionBase.js');
var OpCompraVenta = require('./mixins/OperacionCompraVenta.js');
var Devolucion = require('./components/Devolucion.js');


CNCComponent.controller = function (params) {
    var ctrl = this;

    OpDetail.controller.call(this, params);
    OpCompraVenta.controller.call(this, params);

    ctrl.keyPrecioUnitario('precio_unitario_compra');
    ctrl.keyCuentaProducto('cuenta_nc_compra');
    ctrl.keyImpuestoProducto('impuesto_compra');
    ctrl.keyImpuestoTercero('impuesto_compra_id');

    ctrl.opcionesTercero = function () {
        var tercero = ctrl.operacion().$tercero();
        if(!tercero) return;
    };

    ctrl.devolucion = _.cached(function () {
        return new Devolucion.controller(ctrl);
    });

    ctrl.opcionesTercero();
    ctrl.actualizar(true);
    ctrl.loadProductos();
};

CNCComponent.view = OpCompraVenta.view;
