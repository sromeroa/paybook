
module.exports = obtenerOperacion;

function obtenerOperacion (operacionId, tipoOperacion, cuentaId) {
    var Operacion = oor.mm('Operacion');
    var deferred  = m.deferred();
    var search = m.route.parseQueryString(location.search);


    if(operacionId) {
        Operacion.get(operacionId).then(function (operacion) {
            operacion.$new(false);

            if( operacion.tipo_operacion() != tipoOperacion ) {
                var err = 'No coincide el tipo: ' + tipoOperacion + ' ' + operacion.tipo_operacion();
                console.error(err);
                deferred.reject(err);
                
                return deferred.resolve;
            }

            if(!operacion.operacion_anterior_id()) {
                return deferred.resolve(operacion);
            }

            Operacion.get(operacion.operacion_anterior_id()).then(function (ant) {
                operacion.asignarOperacionAnterior(ant, true);
                deferred.resolve(operacion);
            });

        });
        return deferred.promise;
    }


    setTimeout(function() {
        tipoOperacion = tipoOperacion.toUpperCase();

        if(search.copiar) {

            m.request({method:'GET', url:'/api/operaciones/copiar/' + search.copiar})
                .then(operacionNueva)
                .then(function (operacion) {
                    operacion.saldo(null);
                    deferred.resolve(operacion);
                })

            return;
        }


        var pendiente = localStorage.getItem('OORDEN/operacion/' + tipoOperacion);

        if(pendiente) {
            pendiente = JSON.parse(pendiente);
            pendiente = operacionNueva(pendiente);
            pendiente.$isPendiente = true;

            localStorage.removeItem('OORDEN/operacion/' + tipoOperacion);
            deferred.resolve(pendiente);
            toastr.warning('Operación Pendiente Recuperada');
            return;
        }



        deferred.resolve(
            operacionNueva({
                operacion_id:oor.uuid(),
                tipo_operacion:tipoOperacion
            })
        );

    },0);


    function operacionNueva(data) {
        var operacion = new Operacion(data);
        operacion.$new(true);

        if(data.tipo_operacion != tipoOperacion)
        {
            throw 'Tipo Incorrecto';
        }

        operacion.operacion_anterior_id(null);
        operacion.tipo_operacion(tipoOperacion);
        operacion.seccion(tipoOperacion.substring(0,1));

        operacion.items.forEach(function (item) {
            item.operacion_anterior_item_id(null)
        });

        return operacion;
    }

    return deferred.promise;
}
