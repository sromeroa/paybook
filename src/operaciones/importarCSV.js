module.exports = {view:ImportarCSVView, controller:ImportarCSVController};

var localStorageKey = 'OORDEN/IMPORTAR/CSV';

function ImportarCSVView (ctx) {

    ctx.inicializar();


    return oor.panel({title:'Importar Ventas a partir de un CSV'}, [
        m('h6', 'Seleccionar Archivo CSV'),
        m('div', [
            m('input[type=file]', {config : inicializarControl(ctx)})
        ]),
        m('table', [
            ctx.datos().map(function (d) {
                return m('tr', m('td', d))
            })
        ])

    ]);

}

function ImportarCSVController () {
    var ctx = this;
    ctx.datos = m.prop(null)

    ctx.inicializar = function () {
        var data = localStorage.getItem(localStorageKey);
        if(data) {
            data = JSON.parse(data);
            ctx.datos(data);
        }
    }
}



function inicializarControl (ctx) {
    return function (el, isInitialized) {
        if(isInitialized) return;

        el.addEventListener('change', function () {
            Papa.parse(el.files[0], {
                encoding : 'UTF-8',
                complete : function (d) {
                    //Filtro los datos para no incluir filas vacías
                    var data = d.data.filter(function (d) {
                        for(var i = 0; i<d.length; i++) {
                            if(Boolean(d[i])) return true;
                        }
                        return false;
                    });

                    localStorage.setItem(localStorageKey, JSON.stringify(data));
                    ctx.datos(data);
                    m.redraw();
                }
            });
        });
    }
}
