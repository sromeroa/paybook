var TabsComponent = module.exports = {};
var Bitacora = require('./BitacoraComponent');

TabsComponent.controller = function (args) {

    var opCtrl = args.opCtrl;
    var componenteActivo = m.prop();


    this.componentes = m.prop([]);

    this.componentes().push({

        nombre : function () {
            var qNotas = opCtrl.notas.notas().length;
            return 'Notas'  + ( qNotas ? ' (' + ( qNotas > 10 ? '10+' : qNotas) + ')' : '' )
        },

        componente : m.component(NotasComponent, {
            enabled : opCtrl.vm.enabled,
            model : opCtrl.notas
        })
    });


    this.componentes().push({
        nombre : m.prop('Archivos'),
        componente : m.component(ArchivosComponent, {
            nombre_recurso : 'operacion',
            id_recurso : opCtrl.operacion().operacion_id(),
            id_organizacion : oorden.organizacion().organizacion_id
        })
    });


    this.componentes().push({
        nombre : m.prop('Bitácora'),
        componente : m.component(Bitacora, {
            operacion : opCtrl.operacion
        })
    });


    if(args.pagos) {
        this.componentes().push({
            nombre : function () {
                var saldo = opCtrl.operacion().saldo;
                return 'Pagos '.concat(saldo.$int() ? ' (Saldo: ' + saldo() + ')' : '');
            },
            componente : args.pagos
        })
    }


    this.componenteActivo = componenteActivo;

    this.selectComponente = function (cmp,ev) {
        ev.stopPropagation();
        ev.preventDefault();
        componenteActivo(cmp);
    }
};

TabsComponent.view = function (ctrl) {
    return m('div.poliza-extras', {'class' : ctrl.componenteActivo() ? '' : 'cerrada'}, [
        m('ul.nav.nav-tabs[role="tablist"]',
            ctrl.componenteActivo() ? m('li.pull-right', [
                m('a[href]', {
                    onclick:ctrl.selectComponente.bind(null,null)
                }, m.trust('&times;'))
            ]) : '',
            ctrl.componentes().map(function (cmp) {
                return m('li', {
                    'class':ctrl.componenteActivo() === cmp ? 'active' : ''
                }, [
                    m('a[href]', {
                        onclick:ctrl.selectComponente.bind(null,cmp)
                    }, cmp.nombre())
                ]);
            })
        ),
        ctrl.componenteActivo() ? m('div.tab-content', [
            m('div.tab-pane.active', [
                ctrl.componenteActivo().componente
            ])
        ]) : ''
    ])
};


var NotasComponent = {};

NotasComponent.view = function (ctrl, args) {
    return args.model.view()
};

var ComponenteNoDisponible = {};

ComponenteNoDisponible.view = function () {
    return m('p', '(no disponible)')
};











var ArchivosComponent = {};

ArchivosComponent.controller  = function (args) {
    var ctrl = this;
    var ruta = ''.concat(args.id_organizacion, '/', args.nombre_recurso, 's/', args.id_recurso);

    ctrl.file = m.prop();
    ctrl.documentos = m.prop();
    ctrl.cargando = m.prop();

    ctrl.change = function (ev) {
        ctrl.submit( ev.target.files[0] );
    }

    ctrl.submit = function (file) {
        var url = '/api/documento/'.concat(ruta);
        var data = new FormData();
        data.append("documento", file)

        m.request({
            url : url,
            method : 'POST',
            serialize: function(data) {return data},
            data: data
        }).then(function (d) {
            ctrl.documentos(false)
        });
    }

    ctrl.cargarDocumentos = function () {
        if(ctrl.cargando() || ctrl.documentos()) return;
        ctrl.cargando(true);

        m.request({url:'/api/documentos/'.concat(ruta)})
            .then(function (d) { ctrl.documentos(d.data || []) }, function (d) { ctrl.documentos([]) })
            .then(ctrl.cargando.bind(ctrl, false))
    }
};

ArchivosComponent.view = function (ctrl, args) {
    ctrl.cargarDocumentos();

    if(ctrl.cargando()) {
        return oor.loading()
    }

    return m('div', [
        m('ul', [
            ctrl.documentos().map(function (doc) {
                return m('li', [
                    m('a', {
                        href:'//s3.amazonaws.com/amc.public/'.concat(doc.path)
                    }, doc.basename)
                ]);
            })
        ]),
        ArchivosComponent.formulario(ctrl)
    ]);
}

ArchivosComponent.formulario = function (ctrl) {
    return m('div.dragfile', {config : ArchivosComponent.init(ctrl)},[
        m('input[type=file]#fileInput', {
            style : 'width:0.1px; height:0.1px',
            onchange : ctrl.change
        }),

        m('label', {'for': 'fileInput', style:'width:100%'}, [
            m('small.btn.btn-big.btn-primary', 'Subir archivo...'),
            ctrl.file() ? 'FILE' : m('div.small.text-grey', 'Puedes soltar un archivo aquí para subir')
        ])
    ])
}

ArchivosComponent.init = function (ctx) {
    return function (filedrag, isInit) {
        if(isInit) return;

        filedrag.addEventListener("dragover", FileDragHover, false);
        filedrag.addEventListener("dragleave", FileDragHover, false);
        filedrag.addEventListener("drop", FileSelectHandler, false);

        function FileDragHover (e) {
           e.stopPropagation();
           e.preventDefault();

           if(e.type == 'dragover') {
               filedrag.classList.add('drag-over')
           } else {
               filedrag.classList.remove('drag-over')
           }
        }

        function FileSelectHandler (e) {
            var files;
            e.stopPropagation();
            e.preventDefault();
            files = e.target.files || e.dataTransfer.files;
            ctx.documentos(null);
            ctx.submit(files[0]);
            m.redraw();
        }

   }
}
