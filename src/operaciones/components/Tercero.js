
var TerceroComponent =  module.exports = {};
var Selector = require('../../misc/advSelector')
var TerceroForm = require('../../ajustes/contactos/TerceroForm');

TerceroComponent.controller = function (attrs) {
    var ctrl = this;

    if( _.isUndefined(attrs.btnAgregar) ) {
        attrs.btnAgregar = true;
    }

    this.tercero     = attrs.tercero;
    this.tercero_id  = attrs.tercero_id;
    this.enabled     = attrs.enabled;
    this.btnAgregar  = nh.isFunction(attrs.btnAgregar) ? attrs.btnAgregar : m.prop(attrs.btnAgregar);

    this.selector = oor.tercerosSelect2({
        model    : this.tercero_id,
        onchange : function () {
            ctrl.tercero(ctrl.selector.selected())
            if(nh.isFunction(attrs.onchange)) {
                attrs.onchange()
            }
        },
        placeholder : attrs.placeholder,
        tipo : attrs.tipo
    });

    this.selector.enabled = this.enabled;

    /*
    ctrl.selector = Selector({
        text : function (d) { return f('nombre')(d) + ' ' + f('codigo')(d) },
        model : this.tercero_id,
        value : f('tercero_id'),
        enter : function (selection) {
            selection.html('')
            selection.append('div').attr('class', 'text-indigo').text(f('nombre'));
            selection.append('small').attr('class','text-small text-grey').text(f('codigo'))
        },
        onchange : function () {
            console.log('onchange'),
            ctrl.tercero(ctrl.selector.selected());
        }
    });
    */


    this.remove = function () {
        this.tercero(null);
        this.tercero_id(null);

        if(typeof this.tercero_id.onchange === 'function') {
            this.tercero_id.onchange(null);
        }

        if(nh.isFunction(attrs.onchange)){
            attrs.onchange();
        }
    }.bind(this);


    this.agregar = function (evt) {

        MTModal.open({
            controller : TerceroForm.controller,

            args  : {
                onsave : function (tercero) {

                    tercero = JSON.parse(JSON.stringify(tercero));
                    ctrl.tercero(tercero);
                    ctrl.tercero_id(tercero.tercero_id);
                    if(nh.isFunction(attrs.onchange)) {
                        attrs.onchange(tercero);
                    }
                },

                onerror : function (r) {
                    toastr.error(r.status.message);
                },
                cliente : attrs.tipo == 'cliente' ? true : false,
                proveedor : attrs.tipo == 'proveedor' ? true : false
            },

            top : function (ctx){
                var title = 'Agregar Tercero';

                if(ctx.cliente()) {
                    title = 'Agregar Cliente';
                } else if(ctx.proveedor()){
                    title = 'Agregar Proveedor';
                }

                return m('h4', title);
            },

            bottom : function (ctx) {
                return [
                    m('button.pull-right.btn.btn-sm.btn-success.btn-flat', {
                        onclick : function () {
                            ctx.guardar();
                        }
                    }, 'Guardar'),

                    m('button.pull-left.btn.btn-sm.btn-default.btn-flat', {
                        onclick : ctx.$modal.close
                    }, 'Cancelar')
                ];
            },
            modalAttrs : {'class' : 'modal-medium'},
            content : TerceroForm.view,
            el : this
        });
    };
};


TerceroComponent.view = function (ctrl) {
    var tercero = ctrl.tercero();

    return m('div.tercero', {'class' : tercero ? 'con-tercero' : ''},
        ctrl.tercero() ? m('div', [
            ctrl.enabled() ? m('.close', {onclick:ctrl.remove}, '×') : '',
            m('h5', {style:'margin-bottom:0px'}, [
                ctrl.tercero().nombre
            ]),
            m('div', [
                m('small.text-grey', ctrl.tercero().codigo),
                ' ',
                m('small', ctrl.tercero().clave_fiscal)
            ])
        ]) : m('div', [
            ctrl.enabled() && ctrl.btnAgregar() ? m('button.btn.btn-gray.button-striped.button-full-striped.btn-ripple.pull-right', {
                onclick:ctrl.agregar
            },m('i.ion-person-add')) : '',

            m('div[style="padding:10px 60px 10px 0"]', [
                ctrl.selector.view({style:'width:100%'})
            ])
        ])
    );
};


var AgregarTercero = {};

AgregarTercero.controller = function (args) {
    var ctrl = this;
    var paisSelect, monedaSelect;
    var Tercero   = oor.mm('Tercero');
    var Direccion = oor.mm('Direccion');

    var tercero   = new Tercero;
    var direccion = new Direccion;

    ctrl.$error = m.prop(true);

    this.tercero = tercero;
    this.tercero.$new  = m.prop(true);
    this.direccion = direccion;

    if(tercero.$new()) {
        tercero.tercero_id(oor.uuid());
        tercero.clave_moneda( oorden.organizacion().moneda_base_id);
        tercero.ultima_direccion_fiscal = direccion.direccion_id;
        tercero.estatus = m.prop(1);

        direccion.pertenece_a_id = tercero.tercero_id;
        direccion.direccion_id( oor.uuid() );
        direccion.tipo_id(3);
        direccion.codigo_pais(oorden.organizacion().codigo_pais);
    }


    tercero.codigo_pais = direccion.codigo_pais;

    this.guardar = function () {
        ctrl.validate();

        if(ctrl.$error()) {
            toastr.error('El formulario contiene errores');
            return;
        }

        return guardarDireccion()
            .then(guardarTercero)
            .then(notify);
    }

    function notify () {
        if(typeof args.onsave === 'function') {
            args.onsave(ctrl.tercero);
        }
    }

    function guardarDireccion () {
        return m.request({
            method : 'POST',
            url : '/api/direcciones/agregar',
            data : direccion
        });
    }

    function guardarTercero() {
        return Tercero.save(tercero);
    }


    oorden.paisesYMonedas();

    this.paisSelect = _.cached(function () {
        return oor.select2({
            data : oorden.paises().map(function (p) {
                return {id: p.codigo_pais, text:p.nombre};
            }),
            model : ctrl.tercero.codigo_pais
        });
    });


    this.tipoDeCambio = _.cached(function () {
        return oor.select2({
            data : oorden.tiposDeCambio().map(tipoDeCambioOptions),
            model : ctrl.tercero.clave_moneda
        });
    });

    function tipoDeCambioOptions (t) {
        return {
            id : t.tipo_de_cambio,
            text : t.tipo_de_cambio + ' ' + t.nombre
        };
    }

    this.cuentaVentaSelector = _.cached(function () {
        return oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctrl.tercero.cuenta_venta_id
        });
    });

    this.impuestoVentaSelector = _.cached(function () {
        return oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : ctrl.tercero.impuesto_venta_id
        })
    });

    this.ccto1VentaSelector;
    this.ccto2VentaSelector;


    this.cuentaCompraSelector = _.cached(function () {
        return oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctrl.tercero.cuenta_compra_id
        });
    });

    this.impuestoVentaSelector = _.cached(function () {
        return oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : ctrl.tercero.impuesto_compra_id
        })
    });

    this.ccto1CompraSelector;
    this.ccto2CompraSelector;


    function validate(model, keys) {
        var errors = 0;

        keys.forEach(function (key) {
            if(!model[key]()) {
                err(model[key], true);
                errors++;
            } else {
                err(model[key], false);
            }
        });
        err(model, Boolean(errors));
    }


    this.validate = function () {

        validate(tercero, [
            'codigo',
            'nombre',
            'clave_fiscal',
            'tipo_id'
        ]);

        validate(direccion, [
            'calle_y_numero',
            'no_exterior',
            'colonia',
            'codigo_postal',
            'ciudad_o_municipio',
            'estado_o_region',
        ]);

        err(ctrl, tercero.$error() || direccion.$error());
    };


    function err(prop, error) {
        if(!prop.$error) {
            prop.$error = m.prop()
        }
        prop.$error(error);
    }
};

AgregarTercero.view = function (ctrl,args) {
    return m('div', {style:'font-size:14px', config :AgregarTercero.config(ctrl)},[
        m('.row', [
            m('.col-md-6', [
                m('div.flex-row.flex-end', [
                    AgregarTercero.inputCodigo(ctrl),
                    AgregarTercero.input(ctrl, 'nombre', 'Nombre',{size:20}, {style:'flex-grow:3'}),
                ]),
                m('div.flex-row',[
                    m('div.mt-inputer', {style:'flex-grow:4'}, [
                        m('label','País'),
                        ctrl.paisSelect().view({style:'width:150px'})
                    ])
                ]),
                m('div.flex-row.flex-end', [
                    AgregarTercero.input(ctrl, 'clave_fiscal', 'Clave Fiscal',{},{style:'flex-grow:3'})
                ]),
                m('div.flex-row.flex-end', [
                    AgregarTercero.input(ctrl, 'tipo_id', 'Tipo Org.',{},{style:'flex-grow:3'})
                ]),
                m('div.flex-row.flex-end', [
                    m('div.mt-inputer', {style:'flex-grow:4'},[
                       m('label','Tipo de Cambio'),
                       ctrl.tipoDeCambio().view({style:'width:150px'})
                   ])
                ])
            ]),
            m('.col-md-6', [
                AgregarTercero.direccionPanel(ctrl)
            ])
        ]),

        m('br'),

        m('.row', [
            m('.col-md-6', [
                m('h5', 'Cliente'),
                m('div.flex-row.flex-end', [
                    m('.mt-inputer.line', {style:'flex:1 2 50%'},[
                        m('label', 'Cuenta Venta'),
                        m('.cuentas-selector',[
                            ctrl.cuentaVentaSelector().view()
                        ])
                    ]),
                    m('.mt-inputer.line',  {style:'flex:1 2 50%'}, [
                        m('label', 'Impuesto Venta'),
                         ctrl.impuestoVentaSelector().view({style:'min-width:150px'})
                    ])

                ]),
                m('div.flex-row.flex-end', [
                    AgregarTercero.input(ctrl, 'descuento', 'Descuento (%)',{size:6}, {style:'flex-grow:1'}),
                    AgregarTercero.input(ctrl, 'dias_credito_venta', 'Diás de Crédito',{size:6}, {style:'flex-grow:1'})
                ]),
            ])
        ]),

        m('br')
    ]);
};

AgregarTercero.direccionPanel = function (ctrl, args) {
    args || (args = {});

    return m('div.direccion-panel', [
        m('h5', args.title || 'Dirección'),
        m('.flex-row.flex-end', [
            AgregarTercero.inputDir(ctrl, 'calle_y_numero', 'Calle', {size:15},{style:'flex-grow:3'}),
            AgregarTercero.inputDir(ctrl, 'no_exterior', '#', {size:8}),
            AgregarTercero.inputDir(ctrl, 'no_interior', 'Int.', {size:8})
        ]),
        m('.flex-end.flex-row', [
            AgregarTercero.inputDir(ctrl, 'colonia', 'Colonia', {size:18}, {style:'flex-grow:3'}),
            AgregarTercero.inputDir(ctrl, 'codigo_postal', 'CP',{size:7})
        ]),
        m('.flex-end.flex-row', [
            AgregarTercero.inputDir(ctrl, 'ciudad_o_municipio', 'Ciudad',{size:15},{style:'flex-grow:2'}),
            AgregarTercero.inputDir(ctrl, 'estado_o_region', 'Estado',{size:15},{style:'flex-grow:2'})
        ])
    ]);
}

AgregarTercero.inputCodigo = function (ctrl) {
    return AgregarTercero.input(ctrl,'codigo','Código', {size:10})
};

AgregarTercero.input = function (ctrl, key, label, inputAttrs, inputerAttrs) {
    return AgregarTercero.rawInput(ctrl, 'tercero', key, label, inputAttrs, inputerAttrs);
};

AgregarTercero.inputDir = function (ctrl, key, label, inputAttrs, inputerAttrs) {
    return AgregarTercero.rawInput(ctrl, 'direccion', key, label, inputAttrs, inputerAttrs);
};

AgregarTercero.rawInput = function(ctrl,holder, key, label, inputAttrs, inputerAttrs) {
    var prop = ctrl[holder][key];
    label = label || key;

    var error = prop.$error ? prop.$error() : false;

    inputerAttrs = _.extend({},inputerAttrs, {
        'class' : error ? 'error' : ''
    });

    return m('div.mt-inputer',inputerAttrs,[
        m('label', label),
        m('input[type="text"]', _.extend({
            'data-model' : holder,
            'data-model-prop' : key,
            value : prop(),
        },inputAttrs))
    ]);
}

AgregarTercero.config = function (ctrl) {
    var updateModel = _.throttle(function () {
        ctrl.validate();
        m.redraw();
    },500);

    return function (element, initialized) {
        if(initialized) return;

        $(element).on('input', 'input', function() {
            var input = $(this);
            var dataModel = input.attr('data-model');
            var dataModelProp = input.attr('data-model-prop');

            ctrl[dataModel][dataModelProp]( input.val() );
            updateModel()
        });
    }
}
TerceroComponent.AgregarTercero = AgregarTercero;
