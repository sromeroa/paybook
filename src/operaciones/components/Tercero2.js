
var TerceroComponent =  module.exports = {
    terceros : m.prop(),
    byId : {}
};

var ASSelector = require('../../misc/ASSelector');

/*
var Selector = require('../../misc/advSelector')
var TerceroForm = require('../../ajustes/contactos/TerceroForm');
*/

obtenerTerceros.cargando = m.prop(false);

function obtenerTerceros () {
    if(TerceroComponent.terceros() || obtenerTerceros.cargando()) {
        return;
    }

    obtenerTerceros.cargando = m.prop(true);

    oor.request('/terceros/list')
        .then(asignarTerceros)
        .then(obtenerTerceros.cargando.bind(null,false));
}


function asignarTerceros (data) {
    TerceroComponent.terceros(
        data.map(function (tdata) {
            var tercero = {
                id : tdata[0],
                nombre: tdata[1],
                codigo: tdata[2],
                clave_fiscal : tdata[3],
                es_cliente : Boolean(tdata[4]),
                es_proveedor : Boolean(tdata[5])
            };
            tercero.search = tercero.nombre.concat(' ', tercero.codigo , ' ', tercero.clave_fiscal).latinize().toLowerCase();
            TerceroComponent.byId[ tercero.id ] = tercero;
            return tercero;
        })
    );
}

TerceroComponent.controller = function (attrs) {
    var ctx = this;

    ctx.tercero_id     = m.prop();
    ctx.tercero_record = m.prop();

    ctx.onchange = function () {
        attrs.onchange(ctx);
    };
};


TerceroComponent.view = function (ctx, attrs) {
    obtenerTerceros();

    return m('div.us-wrap', {style:'width:300px;margin-bottom:40px; border:1px solid #f0f0f0;padding:6px'}, obtenerTerceros.cargando() ? oor.loading() : [
        m('input', {
            placeholder : attrs.placeholder,
            config : function (el, isInited) {
                if(isInited) return false;

                var selector = ASSelector(el, {
                    data     : TerceroComponent.terceros(),
                    onchange : function (d) {
                        ctx.tercero_record( this.selected() );
                        ctx.tercero_id( this.value() );
                        ctx.onchange();
                        m.redraw();
                    }
                });

                selector.html( function (d) {
                    return '<h5 style="margin-bottom:0px">' + d.nombre + '</h5>' + '<h6 class="small">' + '<strong>' +  d.codigo + '</strong> · ' +  d.clave_fiscal + '</h6>'
                });
            }
        }),

        ctx.tercero_record() ? [
            m('h6', {style:'margin:0 5px 5px 5px'},[
                m('strong',ctx.tercero_record().codigo),
                ' · ',
                m('span',ctx.tercero_record().clave_fiscal)
            ]),
        ] : null,
    ]);
};
