
var TablaItems = module.exports = {};
var Pagos      = require('./Pagos');
var ASSelector  = require('../../misc/ASSelector');
var CuentaSelector = require('../../misc/selectors/CuentaSelector');


TablaItems.controller = function (args) {
    this.__proto__ = args.opCtrl;
};

TablaItems.view = function (ctrl) {
    return m('div', [
        m('br'),
        m('div.table-responsive', [
            m('table.table.bill.op-table.tabla-items-compraventa', [
                m('thead', TablaItems.head(ctrl)),
                m('tbody', TablaItems.body(ctrl)),
            ])
        ]),
        m('div.pie-factura', TablaItemsFoot(ctrl))
    ]);
};


TablaItems.head = function (ctrl) {
    var impORet = ctrl.operacion().mostrar_impuestos() || ctrl.operacion().mostrar_retenciones();

    return m('tr', [
        m('th.options',''),
        m('th.producto', 'Producto'),
        m('th.concepto', 'Concepto'),
        m('th.cantidad', 'Cant.'),
        m('th.unidad', 'Unid.'),
        m('th.precio-unitario', 'P. Unit.'),

        ctrl.operacion().mostrar_descuento() ? m('th.descuento', '% Dcto' ) : null,
        impORet ? m('th.imp-ret', 'Imp/Ret') : null,
        ctrl.operacion().mostrar_cuentas()   ? m('th.cuenta', 'Cuenta') : null,
        ctrl.operacion().mostrar_cctos() ? m('th.cctos', 'CCTOS') : null,

        m('th.importe', ctrl.operacion().moneda()),
        m('th.options','')
    ]);
};


TablaItems.body = function (ctrl) {
    return ctrl.operacion().items.map(function (item) {
        return m.component(ItemRow, {
            opCtrl:ctrl,
            item:item,
            key:item.operacion_item_id()
        });
    });
};


function TablaItemsFoot (ctrl) {
    var rowspan = 2;
    rowspan += ctrl.operacion().$detalleImpuestos().length;
    rowspan += ctrl.operacion().$detalleRetenciones().length;
    var dif = Number(ctrl.operacion().mostrar_cuentas());


    if(!ctrl.__proto__.pagosComponent) {
        ctrl.__proto__.pagosComponent = new Pagos.controller({ operacion : ctrl.operacion });
    }

    return m('.row', [
        m('.col-sm-6.col-xs-12', [

            m('div.checkboxer.checkboxer-indigo.form-inline', {style:'font-size:0.90em'}, [
                m('input[type="checkbox"]'.concat(ctrl.vm.enabled() ? '' : '[disabled]'), {
                    id: 'impuestos-incluidos',
                    checked : ctrl.operacion().precios_con_impuestos(),
                    onclick : m.withAttr('checked', function (v) {
                        ctrl.operacion().precios_con_impuestos(v);
                        ctrl.operacion().$impuestosIncluidos(Boolean(v));
                        ctrl.vm.hasChanges(true);
                        ctrl.actualizar(true);
                    })
                }),
                m('label', {'for':'impuestos-incluidos'},[
                    'Precios incluyen Impuestos: ',
                    m('strong', ctrl.operacion().precios_con_impuestos() ? 'SÍ' : 'NO')
                ]),
            ]),

            m('.clear'),

            m('div', 'Mostrar:', [
                m('ul', {style:'font-size:0.80em'}, [
                    m('li', {style:'float:left'}, [
                        m('div.checkboxer.checkboxer-indigo.form-inline', [
                            m('input[type="checkbox"]', {
                                id:'check-cuentas',
                                checked : ctrl.operacion().mostrar_cuentas(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_cuentas)
                            }),
                            m('label', {'for':'check-cuentas'}, 'Cuentas')
                        ]),
                    ]),

                    m('li', {style:'float:left'}, [
                        m('div.checkboxer.checkboxer-indigo.form-inline', [
                            m('input[type="checkbox"]', {
                                id:'check-descuento',
                                checked : ctrl.operacion().mostrar_descuento(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_descuento)
                            }),
                            m('label', {'for':'check-descuento'}, 'Descuentos')
                        ])
                    ]),

                    m('li', {style:'float:left'}, [
                        m('div.checkboxer.checkboxer-indigo.form-inline', [
                            m('input[type="checkbox"]', {
                                id:'check-impuestos',
                                checked : ctrl.operacion().mostrar_impuestos(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_impuestos)
                            }),
                            m('label', {'for':'check-impuestos'}, 'Impuestos')
                        ]),
                    ]),

                    m('li', {style:'float:left'},  [
                        m('div.checkboxer.checkboxer-indigo.form-inline', [
                            m('input[type="checkbox"]', {
                                id:'check-retenciones',
                                checked : ctrl.operacion().mostrar_retenciones(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_retenciones)
                            }),
                            m('label', {'for':'check-retenciones'}, 'Retenciones')
                        ])
                    ]),

                    ctrl.operacion().ccto1() ? m('li', {style:'float:left'},[
                         m('div.checkboxer.checkboxer-indigo.form-inline', [
                            m('input[type="checkbox"]', {
                                id:'check-cctos',
                                checked : ctrl.operacion().mostrar_cctos(),
                                onchange:m.withAttr('checked', ctrl.operacion().mostrar_cctos)
                            }),
                            m('label', {'for':'check-cctos'}, 'Centros de Costo')
                        ])
                    ]) : ''

                ])
            ])
        ]),

        m('.col-sm-6.col-xs-12', {style:'padding-right:0'},[

            m('table.table', {style:'font-size:.90em; table-layout:fixed'}, [
                m('tr', [
                    m('td', {style:'padding-left:5px; padding-top:5px; padding-bottom:5px'}, [
                        m('div', m('span','Subtotal') ),
                        ctrl.operacion().$descuentos.$int() ? m('small.text-grey', ' \u2022 ','Incluye descuentos por ', m('strong', ctrl.operacion().$descuentos.number())) : '',
                        ctrl.operacion().precios_con_impuestos() ? m('small.text-grey', ' \u2022 ','Impuestos Incluidos') : null
                    ]),
                    m('td.text-right', {style:'padding-right:5px; padding-top:5px; padding-bottom:5px'}, ctrl.operacion().$sumaImportes.number())

                ]),

                ctrl.operacion().$detalleImpuestos().map(function (impuesto) {
                    return m('tr', [
                        m('td', {style:'padding-left:5px; padding-top:5px; padding-bottom:5px'}, m('span.text-grey','Impuesto'), ' ', impuesto.nombre),
                        m('td.text-right', {style:'padding-right:5px; padding-top:5px; padding-bottom:5px'}, impuesto.monto.number())
                    ]);
                }),

                ctrl.operacion().$detalleRetenciones().map(function (retencion) {
                    return m('tr', [
                        m('td', {
                            style:'padding-left:5px 0 5px 5px'
                        }, m('span.text-grey','Retención'), ' ', retencion.nombre),

                        m('td.text-right', {
                            style:'padding-right: 5px 0 5px 5px'
                        }, retencion.monto.number())

                    ]);
                }),

                m('tr', {style:'border-bottom:2px solid #666; border-top:2px solid #666'}, [
                    m('td', {style:'padding-left:5px; padding-top:12px; padding-bottom:8px'}, m('strong','TOTAL ' , ctrl.operacion().moneda())),
                    m('td.text-right', {style:'padding-right:5px; padding-top:12px; padding-bottom:8px'}, ctrl.operacion().total.number())
                ]),

                Pagos.view(ctrl.pagosComponent)
            ])
        ])
    ]);
};



var dragging = null;
var dropping = null;

function config(ctrl){
    return function (element, initialized) {
        if(initialized) return;
        var dragger = element.querySelector('.ion-drag');

        element.addEventListener('mouseover', function(ev) {
            if(!ev.target.matches('.ion-drag.drag')) return;
            element.setAttribute('draggable','true');
        },true);

        element.addEventListener('mouseout', function(ev) {
            if(!ev.target.matches('.ion-drag.drag')) return;
            element.removeAttribute('draggable');
        },true);

        element.addEventListener('dragstart', function (ev) {
            ev.stopPropagation();
            ev.dataTransfer.effectAllowed = 'move';


            dragging = element.getAttribute('data-item-id');
            element.classList.add('dragging');

            var draggingItem;

            ctrl.operacion().items.forEach(function (item) {
                if(dragging == item.operacion_item_id()) draggingItem = item;
            });

            var crt = document.createElement('div');
            crt.style.display = 'none';
            crt.innerHTML = draggingItem.producto_nombre();
            document.body.appendChild(crt);
            ev.dataTransfer.setDragImage(crt,0,0);

        },false);

        element.addEventListener('dragend', function (evt) {
            dragging = null;
            dropping = null;
            element.classList.remove('dragging');
        },false);

        element.addEventListener('dragenter', function (ev) {
            var _dropping = element.getAttribute('data-item-id');
            var draggingIndex, droppingIndex, item;

            if(_dropping != dropping){
                dropping = _dropping;

                if(dropping != dragging) {

                    ctrl.operacion().items.forEach(function (item, index) {
                        if(item.operacion_item_id() == dragging) draggingIndex = index;
                        if(item.operacion_item_id() == dropping) droppingIndex = index;
                    });

                    item = ctrl.operacion().items[draggingIndex];
                    ctrl.operacion().items.splice(draggingIndex, 1);
                    ctrl.operacion().items.splice(droppingIndex, 0, item);
                    m.redraw();
                }
            }
        },false);

    }
}




var ItemRow = {};

ItemRow.controller = function (args) {
    var item = args.item,
        ctrl = this,
        operacion,
        cuentas,
        cuentasSelector,
        impuestosSelector,
        retencionesSelector,
        cctoSelectors,
        productosSelector;

    this.__proto__ = args.opCtrl;
    this.item = item;


    //Operacion
    operacion = this.operacion;

    //Cuentas
    cuentas = oorden.cuentas().map(function (s) { return s; });

    cuentasSelector = oor.cuentasSelector({
        cuentas : cuentas,
        model : function () {
            if(arguments.length){
                item.cuenta_id(arguments[0])
            }
            return item.cuenta_id()
        },
        onchange : function (v) {
            item.asignarCuenta( cuentasSelector.selected() );
            ctrl.actualizar(true);
        }
    });


    impuestosSelector = oor.impuestosSelect2({
        impuestos : oorden.impuestos().filter(function (imp) {
            return !imp.seccion || imp.seccion == ctrl.operacion().seccion() || imp.seccion == 'A';
        }),
        model : item.impuesto_conjunto_id,
        onchange : function (v) {
            item.asignarImpuesto( impuestosSelector.selected() );
            ctrl.vm.hasChanges(true);
            ctrl.actualizar(true);
        }
    });

    retencionesSelector = oor.retencionesSelect2({
        retenciones : oorden.retenciones().filter(function (imp) {
            return !imp.seccion || imp.seccion == ctrl.operacion().seccion() || imp.seccion == 'A';
        }),
        model : item.retencion_conjunto_id,
        seccion : ctrl.operacion().seccion(),
        onchange : function (v) {
            item.asignarRetencion( retencionesSelector.selected() );
            ctrl.vm.hasChanges(true);
            ctrl.actualizar(true)
        }
    });

    cctoSelectors = this.centrosDeCosto().map(function (ccto, idx) {
        var name = 'ccto_'.concat(idx + 1);
        var selector = oor.cctoSelect2({
            centroDeCosto : ccto,
            model : item[name.concat('_id')],
            onchange : function () {
                ctrl.vm.hasChanges(true);
            }
        });
        selector.enabled = softSelectorsEnabled;
        return selector;
    });

    productosSelector = oor.select2({
        data : ctrl.productos().map(function (p) {
            return {id:p.producto_id, text:p.codigo + ' ' + p.nombre, data:p};
        }),

        find : function (id) {
            return ctrl.productos().filter(function(p) {
                return p.producto_id == id;
            })[0];
        },

        onchange : function () {
            ctrl.itemAsignarProducto(item,productosSelector.selected() );
            ctrl.vm.hasChanges(true);
        },

        model : item.producto_id,

        templateSelection : function  (item) {
            return $('<div>' + item.data.codigo + '</div>');
        }
    });


    ctrl.selectorsEnabled = selectorsEnabled;

    ctrl.mostrarOtros = function () {
        if(ctrl.operacion().$operacionAnterior()) {
            return Boolean(item.$operacionItemAnterior());
        }
        return true;
    }

    function selectorsEnabled () {
        if(ctrl.operacion().$operacionAnterior()) {
            return false;
        }
        return ctrl.vm.enabled();
    }

    function softSelectorsEnabled() {
        if(ctrl.operacion().$operacionAnterior()) {
            return false;
        }
        return ctrl.vm.softEnabled();
    }


    productosSelector.enabled   = selectorsEnabled
    retencionesSelector.enabled = softSelectorsEnabled;
    impuestosSelector.enabled   = softSelectorsEnabled;
    cuentasSelector.enabled     = softSelectorsEnabled;

    this.cuentasSelector     = cuentasSelector;
    this.impuestosSelector   = impuestosSelector;
    this.retencionesSelector = retencionesSelector;
    this.cctosSelectors      = cctoSelectors;
    this.productosSelector   = productosSelector;
};


/**
 * Inicializa los eventos de cada Row
 */
ItemRow.config = function (ctrl) {
    var update = _.throttle(function () {
        ctrl.actualizar(true);
        m.redraw()
    },500);

    function initialize(el) {
        //maneja el cambio de los inputs
        el.delegate('[data-prop]', 'input', function () {
            var input = $(this);
            var prop  =  input.attr('data-prop');
            ctrl.vm.hasChanges(true);
            ctrl.item[prop](input.val());
            update();
        });

        el.delegate('[data-prop]', 'change', function () {
            var input = $(this);
            var prop  =  input.attr('data-prop');
            ctrl.item[prop](input.val());

            if(ctrl.item[prop].fix) {
                ctrl.item[prop].fix();
            }

            ctrl.vm.hasChanges(true);
            ctrl.actualizar(true);
            m.redraw();
        });

    }

    return function (el, isInitialized) {
        //Inicializa el drag and drop
        config(ctrl).apply(null, arguments);
        if(isInitialized) return;
        initialize($(el));
    }
}



ItemRow.view  = function(ctrl,attrs) {
    var impRet =  ctrl.operacion().mostrar_impuestos() || ctrl.operacion().mostrar_retenciones();
    var enabled = ctrl.vm.enabled();
    var softEnabled = ctrl.vm.softEnabled();
    var integrableEditable = ctrl.item.$integrable() && ctrl.vm.enabled();
    var mostrarOtros = ctrl.mostrarOtros();

    return m('tr', {id:'operacion-item-'.concat(attrs.key), config:ItemRow.config(ctrl), 'data-item-id' : ctrl.item.operacion_item_id()}, [
        m('td', (integrableEditable) ? m('i.ion-drag.drag.icon-options') : ''),
        m('td', [
            mostrarOtros ? ctrl.productosSelector.view({style:'width:150px'}) : ''
        ]),

        m('td', [
            m('textarea[style="width:100%"][data-prop="producto_nombre"]', {
                disabled  : !ctrl.vm.enabled(),
                config : oor.autoGrow,
            }, ctrl.item.producto_nombre())
        ]),

        m('td', [
            mostrarOtros ? m('input.text-right[type="text"][data-prop="cantidad"]', {
                value: ctrl.item.cantidad(),
                size:6,
                disabled : !ctrl.vm.enabled()
            }) : ''
        ]),

        m('td', [
            mostrarOtros ? m('input.text-right[type="text"][data-prop="unidad"]', {
                value:ctrl.item.unidad(),
                size:8,
                disabled : !ctrl.selectorsEnabled()
            }) : ''
        ]),

        m('td', [
            mostrarOtros ? m('input.text-right[type="text"][data-prop="precio_unitario"]', {
                value: ctrl.item.precio_unitario(),
                size:8,
                disabled : !ctrl.vm.enabled()
            }) : ''
        ]),

        ctrl.operacion().mostrar_descuento() ? m('td.text-right',
            mostrarOtros ? m('input.text-right[type="text"][data-prop="descuento"]',{
                value: ctrl.item.descuento(),
                size:5,
                disabled : !ctrl.selectorsEnabled()
            }) : ''
        ) : '',


        impRet ? m('td', [
            mostrarOtros && ctrl.operacion().mostrar_impuestos()   ? ItemRow.impuestos(ctrl)  : '',
            mostrarOtros && ctrl.operacion().mostrar_retenciones() ? ItemRow.retenciones(ctrl) : ''
        ]) : '',


        ctrl.operacion().mostrar_cuentas() ? m('td', [
            mostrarOtros ? m('div.cuentas-selector', [
                /*
                softEnabled ? m('div', {config : ctrl.cuentasSelector}) : m('div.cuenta-display.detalle', [
                    m('div.cuenta-cuenta', ctrl.item.$cuenta() ? ctrl.item.$cuenta().cuenta : '---'),
                    m('div.cuenta-nombre', ctrl.item.$cuenta() ? ctrl.item.$cuenta().nombre : '(no hay cuenta)')
                ])
                */
                /*
                m('input[type=text]', {
                    config : function (el, isInited) {

                    }
                }),
                m('div', ctrl.item.$cuenta() ? ctrl.item.$cuenta().nombre : null)
                */

                m.component(CuentaSelector, {
                    cuenta   : ctrl.item.$cuenta(),
                    model    :  ctrl.item.cuenta_id,
                    onchange : function () {
                        ctrl.vm.hasChanges(true);
                        ctrl.actualizar(true);
                    },
                    disabled : softEnabled == false
                })

            ]) : null
        ]) : null,


        ctrl.operacion().mostrar_cctos() ? m('td', [
            mostrarOtros ? ItemRow.centrosDeCosto(ctrl) : ''
        ]) : '',

        m('td.text-right',[
            ctrl.item.importe.$int() ? ctrl.item.importe.number() : '--',

            ctrl.item.hasError('noConcepto') ? m('.text-red', {
                style : 'cursor:pointer;font-size:0.8em'
            },'- Descripción') : null,

            ctrl.item.hasError('noCuenta') ? m('.text-red', {
                style : 'cursor:pointer;font-size:0.8em',
                onclick : ctrl.operacion().mostrar_cuentas.bind(null,true)
            }, 'Cuenta') : null,

            ctrl.item.hasError('noImpuesto') ? m('.text-red', {
                 style : 'cursor:pointer;font-size:0.8em',
                onclick : ctrl.operacion().mostrar_impuestos.bind(null,true)
            },'Impuesto') : null
        ]),

        m('td', (integrableEditable)? m('a', {
            onclick : ctrl.eliminarItem.bind(null, ctrl.item)
        },m('i.ion-trash-a.icon-options')) : '')
    ])
}


ItemRow.impuestos = function (ctrl){
    var enabled = true;
    return m('div.impuesto', [
        m('div', {style:enabled ? 'display:none' : ''}, ctrl.item.$impuesto() ? ctrl.item.$impuesto().impuesto_conjunto : '(ninguno)'),
        m('div', {style:enabled ? '' : 'display:none'} , ctrl.impuestosSelector.view())
    ]);
};

ItemRow.retenciones = function (ctrl){
    var enabled = true;
    return m('div.impuesto', [
        m('div', {style:enabled ? 'display:none' : ''}, ctrl.item.$retencion() ? ctrl.item.$retencion().retencion_conjunto : '(ninguna)'),
        m('div', {style:enabled ? '' : 'display:none'} , ctrl.retencionesSelector.view())
    ]);
};

ItemRow.centrosDeCosto = function (ctrl) {
    return ctrl.cctosSelectors.map(function (selector, index) {
        return m('div', selector.view({style:'width:100%'}));
    });
};
