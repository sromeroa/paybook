module.exports = {
    controller : PagarOperacionController,
    view : PagarOperacionView,
    generarPago : GenerarPago,
    registrarPagoOperacion : registrarPagoOperacion
}


function PagarOperacionController (params) {
    var ctx = this, saldo;

    ctx.transacciones = m.prop();
    ctx.transacciones.cargando = m.prop(false);
    ctx.operacion = params.operacion;
    ctx.registroPago = m.prop();

    ctx.monedaOperacion = m.prop( ctx.operacion.moneda() );
    ctx.monedaCuenta = m.prop( ctx.monedaOperacion() );

    ctx.cuenta = m.prop();
    ctx.pagar = m.prop(false);

    saldo = ctx.operacion.saldo();

    function guardadoOperacion (d) {
        var operacionSaldada = false;

        if(ctx.operacion.estatus() != d.operacion.estatus) {
            ctx.operacion.estatus(d.operacion.estatus);
            if(ctx.operacion.estatus() == 'S') {
                operacionSaldada = true;
            }
        }

        ctx.operacion.saldo( Number(d.operacion.saldo) )
        toastr.success(operacionSaldada ? 'Se ha saldado la operación' : '¡Pago Registrado!')
        ctx.registroPago(null)
        ctx.transacciones(null)
        //ctx.obtenerTransacciones()
        oor.lsMessage({type:'success', text:'Pago Registrado'})
        history.back();
    }

    function onError (d) {
        toastr.error(d.status.message);
    }

    ctx.guardarTransaccion = function () {
        registrarPagoOperacion( ctx.registroPago() ).then(guardadoOperacion, onError)
    }


    ctx.ajustarPago = function () {
        var montoPago  = oorden.organizacion.round( ctx.registroPago().monto_pago() );
        var tcPago =  ctx.registroPago().tasa_de_cambio_pago();
        var tasa = ctx.registroPago().tasa_de_cambio();
        var monto = oorden.organizacion.round( (tcPago/tasa) * montoPago);

        ctx.registroPago().monto(monto);
    };


    ctx.notasDeCredito = m.prop();
    ctx.notasDeCredito.cargando = m.prop(false);
    ctx.notaDeCredito = m.prop(null);

    ctx.cargarNotasDeCredito = function () {
        if(ctx.notasDeCredito() || ctx.notasDeCredito.cargando() ){
            return;
        }

        ctx.notasDeCredito.cargando(true);

        oor.request('/apiv2', 'GET',  {
            data : {
                modelo:'operaciones',
                tipo_operacion: ctx.operacion.seccion() == 'V' ? 'VNC' : 'CNC',
                tercero_id : ctx.operacion.tercero_id(),
                moneda: ctx.operacion.moneda(),
                estatus : 'A'
            }
        })
        .then(ctx.notasDeCredito)
        .then(function (notasDeCredito) {

            ctx.notasDeCreditoSelector = oor.select2({
                data : notasDeCredito.map(function (nota) {
                    return {id:nota.operacion_id, text:nota.documento + '(' + nota.fecha + ')'}
                }),
                model : m.prop(),
                find : function (val) {
                    return notasDeCredito.filter( f('operacion_id').is(val) )[0]
                },
                onchange : function () {
                    ctx.notaDeCredito( ctx.notasDeCreditoSelector.selected() )
                    ctx.notaDeCredito().saldoAplicar = Math.min(ctx.operacion.saldo(), ctx.notaDeCredito().saldo)
                }
            });
        })
        .then(ctx.notasDeCredito.cargando.bind(null, false))
    }

    ctx.aplicarNotaDeCredito = function () {
        var data = {};

        data[ctx.notaDeCredito().tipo_operacion] = ctx.notaDeCredito().operacion_id;
        data[ctx.operacion.tipo_operacion()] = ctx.operacion.operacion_id();

        oor.request('/movimientos/pagoNC', 'POST', {
            data : data
        }).then(guardadoOperacion, onError)
    }



    if( ctx.operacion.saldo.$int() > 0) {
        ctx.pagar(true);

        ctx.cuentaSelector = oor.select2({
            agregarCta : false,

            data : oorden.cuentas()
                    .filter(f('estatus').is(1))
                    .filter(f('acumulativa').is(0))
                    .filter(function (r) {return r.pago_permitido == 1 || r.es_bancos == 1} )
                    .map(function (d){
                        return {id:d.cuenta_contable_id, text:d.cuenta + ' ' + d.nombre};
                    }),

            model : function () {
                if(arguments.length) {
                    ctx.cuenta(arguments[0])
                }
                return ctx.cuenta();
            },

            find : oorden.cuenta,

            onchange : function () {
                GenerarPago({
                    operacion_id : ctx.operacion.operacion_id(),
                    cuenta_id : ctx.cuenta()
                }).then(
                    function (pago) {

                        var monedaBase = oorden.organizacion().codigo_moneda_base;

                        ctx.registroPago({
                            tipo_movimiento : 'PAGAR_OPERACION',
                            factor :  m.prop(pago.factor),

                            fecha :  m.prop(oor.fecha.toSQL(new Date)),
                            monto_pago : m.prop(0),
                            monto : m.prop(0),
                            cuenta_id : m.prop(pago.cuenta_id),
                            operacion_id : m.prop(pago.operacion_id),


                            tipo_de_cambio_pago : m.prop(pago.tipo_de_cambio_pago),
                            tasa_de_cambio_pago : m.prop(pago.tasa_de_cambio_pago),
                            moneda_pago : m.prop(pago.moneda_pago),

                            tasa_de_cambio : m.prop(pago.tasa_de_cambio),
                            tipo_de_cambio : m.prop(pago.tipo_de_cambio),
                            moneda : m.prop(pago.moneda),

                            referencia : m.prop(''),

                        });

                        ctx.registroPago().importe_pago = ctx.registroPago().monto;

                        ctx.monedasExtranjeras = [];

                        if(ctx.registroPago().moneda() != monedaBase) {
                            ctx.monedasExtranjeras.push({
                                moneda : ctx.registroPago().moneda,
                                moneda_base : m.prop(oorden.organizacion().codigo_moneda_base),
                                tipo_de_cambio : ctx.registroPago().tipo_de_cambio,
                                tasa_de_cambio : ctx.registroPago().tasa_de_cambio
                            })
                        }

                        if(ctx.registroPago().moneda() != ctx.registroPago().moneda_pago()) {

                            if(ctx.registroPago().moneda_pago() != monedaBase) {
                                ctx.monedasExtranjeras.push({
                                    moneda : ctx.registroPago().moneda_pago,
                                    moneda_base : m.prop(oorden.organizacion().codigo_moneda_base),
                                    tipo_de_cambio : ctx.registroPago().tipo_de_cambio_pago,
                                    tasa_de_cambio : ctx.registroPago().tasa_de_cambio_pago
                                });
                            }

                        } else {
                            ctx.registroPago().tasa_de_cambio_pago = ctx.registroPago().tasa_de_cambio;
                            ctx.registroPago().tipo_de_cambio_pago = ctx.registroPago().tipo_de_cambio;
                        }

                        ctx.registroPago().monto_pago(ctx.operacion.saldo());
                        ctx.ajustarPago()
                    }
                )
            }
        });
    }
}


function GenerarPago(params) {
    return oor.request('/movimientos/generarpago', 'GET', {
        data: params
    })
}

function registrarPagoOperacion (data) {
    return oor.request('/movimientos/pagardocumento', 'POST', {
        data : data
    });
}

function PagarOperacionView (ctx){
    ctx.cargarNotasDeCredito();
    var tab, mostrarNC;

    if(!ctx.tab) ctx.tab = m.prop(PagarConCuenta)

    tab = ctx.tab();
    mostrarNC = ctx.notasDeCredito() && ctx.notasDeCredito().length;

    return ctx.pagar() ? m('div.styled-tabs.row', [

        m('.col-sm-3', [
            m('ul.nav.nav-tabs.tabs-left', [

            m('li', {'class' : tab == PagarConCuenta ? 'active' : ''}, [
                m('a[href=javascript:;]', {onclick:ctx.tab.bind(null, PagarConCuenta)}, 'Con Cuenta')
            ]),

            m('li', {'class' : tab == PagarConNotaDeCredito ? 'active' : ''}, [
                m('a[href=javascript:;]', {onclick:ctx.tab.bind(null, PagarConNotaDeCredito)}, 'Con Nota de Crédito')
            ]),
            /*
            m('li', [
                m('a[href=javascript:;]', 'Con Anticipo')
            ])
            */
            ])
        ]),

        m('.col-sm-9.tab-content', {style:'background:white'}, [
            m('div.tab-pane.active', {key : tab.name }, tab(ctx))
        ])


    ]) : null;
}

function PagarConCuenta (ctx) {
    var transaccion = ctx.registroPago();
    var formato = d3.format(',.2f');


    return [
        m('h5', 'Pagar con Cuenta:'),
        m('.row', [
            m('.col-sm-12', [
                m('.flex-row', [

                    m('.mt-inputer', [
                        m('label', 'Registrar en la cuenta:'),
                        m('div', ctx.cuentaSelector.view())
                    ]),

                    transaccion ? m('.mt-inputer', [
                        m('label', 'Fecha'),
                        m('input[type=date]', {
                            onchange : m.withAttr('value', transaccion.fecha),
                            value : transaccion.fecha()
                        })
                    ]) : null
                ]),

                transaccion ? [
                    m('.flex-row', [
                        m('.mt-inputer', [
                            m('label', 'Monto (' ,  ctx.monedaOperacion() , ')'),
                            m('input[type=text].text-left', {
                                //size: transaccion.monto_pago().length + 5,
                                value : transaccion.monto_pago(),
                                onchange : m.withAttr('value', function (v) {
                                    transaccion.monto_pago(v);
                                    ctx.ajustarPago();
                                })
                            })
                        ]),
                        TasaDeCambioView(ctx)
                    ]),

                    m('.flex-row', [
                          m('.mt-inputer',  {style:'flex: 4 1 '}, [
                            m('label', 'Referencia'),
                            m('input[type=text]', {
                                value : transaccion.referencia(),
                                onchange : m.withAttr('value', transaccion.referencia)
                            })
                        ])
                    ]),

                    m('.row', [
                        m('button.btn.btn-flat.pull-right.btn-sm.btn-success', {
                            onclick : ctx.guardarTransaccion
                        },'Registrar Pago')
                    ])

                ] : null
            ])
        ])
    ];
}





function PagarConNotaDeCredito (ctx) {
    if(ctx.operacion.tipo_operacion() == 'VNC') return;
    if(ctx.operacion.tipo_operacion() == 'CNC') return;
    var notaDeCredito = ctx.notaDeCredito();

    return m('div', [
        m('h5', 'Pagar con Nota de Crédito:'),

        ctx.notasDeCredito.cargando() ? oor.loading() : [
            ctx.notasDeCredito().length ? m('.flex-row', [
                m('.mt-inputer', [
                    m('label','Con Nota de Crédito'),
                    ctx.notasDeCreditoSelector ? ctx.notasDeCreditoSelector.view() : m('label','(No hay Notas de Crédito del tercero)')
                ])
            ]) : null
        ],

        notaDeCredito ? m('div', [
            'Aplicar ' +  notaDeCredito.moneda + ' ' +  notaDeCredito.saldoAplicar + ' de ' + notaDeCredito.documento,
            m('button.btn-sm.btn-success.btn-flat', {
                onclick : ctx.aplicarNotaDeCredito
            }, 'Confirmar Aplicación')
        ]) : null
    ]);
}





function TasaDeCambioView (ctx) {
    return [
        (ctx.monedasExtranjeras || []).map(function (me) {
            return m('.mt-inputer', [
                m('label', '1 ', me.moneda() , ' = '),
                m('input.text-left[type=text]', {
                    size:12,
                    onchange : m.withAttr('value', function (v) {
                        me.tasa_de_cambio(v);
                        ctx.ajustarPago();
                    }),
                    value : me.tasa_de_cambio()
                }),
                m('label', me.moneda_base())
            ]);
        }),


        ctx.registroPago().moneda() != ctx.monedaOperacion() ? m('.mt-inputer', [
            m('label', 'Monto: (' , ctx.registroPago().moneda() , ')'),
            m('input.text-left[type=text]', {
                size:12,
                readonly : 'readonly',
                value : ctx.registroPago().monto()
            })
        ]) : ''
    ];
}
