module.exports = NCFSelector;

function NCFSelector(ctrl) {

    if (oorden.organizacion().codigo_pais != 'DO') {
        return null;
    }

    if (oorden.organizacion().seccion == 'C') {
        return null;
    }

    initialize(ctrl);

    function changeCheck() {
        acciones();
        $(".cambiar-ncf").on("change", function () {
            acciones();
        });
    }

    function deshabilitarNcf() {
        // Toma este campo como referencia para saber si está en modo edición
        if ($(".fecha-vencimiento").is(":disabled")) {
            $(".ncf-input").prop("disabled", true);
            $(".cambiar-ncf").prop("disabled", true);
        } else {
            $(".ncf-input").prop("disabled", false);
            $(".cambiar-ncf").prop("disabled", false);
        }
    }

    function validarNCF(element, isInitialized) {
        if (!isInitialized) return;

        if (!$(".cambiar-ncf").is(":checked") && element.value.length > 1 && element.value.length != 19) {
            $(".ncf-message").removeClass("hide");
            $(".ncf-input-container").css("border-bottom-color", "red");
        } else {
            $(".ncf-message").addClass("hide");
            $(".ncf-input-container").css("border-bottom-color", "");
        }
    }

    function ncfAuto(element, isInitialized) {
        if (!isInitialized) return;
        $(".ncf-container select").select2({allowClear: true});
        changeCheck();
    }

    function acciones() {
        if (ctrl.$ncf.editar()) {
            cargarInformacion();
            deshabilitarNcf();
        }
        ncf();
    }

    function moverMesaje(element, isInitialized) {
        if (!isInitialized) return;
        $(".ncf-message").insertAfter(".ncf-parent");
    }

    function ncf() {
        if ($(".cambiar-ncf").is(":checked")) {
            ctrl.operacion().tipo_ncf_id(ctrl.$ncf.tipo());
            ctrl.operacion().ncf('');
            ctrl.$ncf.ncf("");

            $(".ncf-input-container").hide();
            $(".ncf-container").show();

            //Arregla el ancho del select
            $(".ncf-container select").select2({
                width: 'resolve'
            }).trigger('change');

        } else {

            ctrl.operacion().ncf(ctrl.$ncf.ncf());
            ctrl.operacion().tipo_ncf_id('');
            ctrl.$ncf.tipo("");
            $(".ncf-container select").select2().val("").trigger('change');

            $(".ncf-container").hide();
            $(".ncf-input-container").show();
        }
    }

    function cargarInformacion() {
        if (!ctrl.$ncf.cargada()) {
            var ncfGlobal = !!ctrl.operacion().ncf() ? ctrl.operacion().ncf().ncf : '';
            var tipoGlobal = !!ctrl.operacion().tipo_ncf_id() ? ctrl.operacion().ncf().tipo_ncf_id : '';
            ctrl.$ncf.ncf(ncfGlobal);
            ctrl.$ncf.tipo(tipoGlobal);
            ctrl.$ncf.cargada(true);
        }
    }

    return m('div.flex-row.flex-end.ncf-parent', [

        m('.ncf-switcher.mt-inputer.line', {
            style: {"border-bottom": 0, "margin-top": "-10px"}
        }, [
            m('label', 'NCF Auto'),
            m('input[type=checkbox]', {
                'class': 'switcher cambiar-ncf',
                config: ncfAuto,
                value: 1
            })
        ]),

        m('.mt-inputer.line.ncf-container', [
            m('label', {"class": "tipo-nfc-label"}, 'Tipo NCF:'),
            ctrl.$ncf.tipoSelector ? ctrl.$ncf.tipoSelector.view() : ''
        ]),

        m('.mt-inputer.line.ncf-input-container', [
            m('label', "NCF"),
            m('input[type=text]', {
                'class': 'ncf-input',
                config: validarNCF,
                onchange: m.withAttr("value", ctrl.$ncf.ncf), value: ctrl.$ncf.ncf()
            })
        ])
    ],
        m('div.ncf-message.text-right.hide', [
            m('label.text-red.line', {
                config: moverMesaje
            }, "NCF no válido")
        ])
    );
}

function initialize(ctrl) {
    if (ctrl.$ncf) {
        return;
    }

    ctrl.$ncf = {
        ncf: m.prop(''),
        tipo: m.prop(''),
        editar: m.prop(false),
        cargada: m.prop(false)
    };

    if (window.location.pathname == "/operaciones/vta/crear") {

    } else {
        ctrl.$ncf.editar(true);
    }

    oor.request('/apiv2?modelo=tipo-ncf').then(function (tipos) {
        var data = m.prop(
            tipos.map(function (tipo) {
                return {
                    id: tipo.tipo_ncf_id,
                    text: tipo.nombre
                }
            })
        );

        ctrl.$ncf.tipoSelector = oor.select2({
            model: ctrl.$ncf.tipo,
            data: data,
            onchange: function (value) {
                ctrl.operacion().tipo_ncf_id(value);
            }
        });
    })
}