var T3Component = require('./Tercero.js');
var AgregarTercero = T3Component.AgregarTercero;

var DireccionComponent = module.exports = {};

DireccionComponent.controller = function (args) {
    var ctx = this;

    var direccion = m.prop();
    var direccion_id = m.prop();


    ctx.loading = m.prop(false);
    ctx.direccion = args.direccion ? args.direccion : m.prop();
    ctx.direccion_id = args.direccion_id;
    ctx.pertenece_a_id = args.pertenece_a_id;

    ctx.config = function (el, isInited) {
        if(isInited == false) {
            ctx.el = el;
        }
    }

    ctx.enabled = args.enabled || m.prop(true);

    ctx.cambiarDireccion = function () {
        MTModal.open({
            controller : DireccionPicker.controller,
            top : m('h3','Dirección'),
            bottom : function (ctx) {
                return m('button.btn.btn-default.btn-flat.btn-sm', {
                    onclick : function () {
                        ctx.$modal.close();
                    }
                }, 'Cancelar')
            },
            content : DireccionPicker.view,
            args : {
                pertenece_a_id : ctx.pertenece_a_id(),
                codigo_pais : args.codigo_pais(),
                onchange : setDireccion
            },
            el : ctx.el,
            modalAttrs : {
                'class' : 'direccion-modal'
            },
        });
    }


    ctx.getDireccion = function () {
        if(ctx.loading() == false && ctx.direccion_id() != direccion_id()) {
            direccion_id( ctx.direccion_id() );
            ctx.loadDireccion();
        }
        return direccion();
    }

    ctx.loadDireccion = function () {
        var Direccion = oor.mm('Direccion');

        direccion(null);

        m.request({
            url : '/api/direcciones/' + ctx.pertenece_a_id(),
            unwrapSuccess : function (r) {
                var d = r.data.filter(function (d) {
                    return d.direccion_id === ctx.direccion_id();
                });

                return d[0] ? new Direccion(d[0]) : null;
            },
            background : true
        })
        .then(direccion)
        .then(ctx.loading.bind(null, false))
        .then(m.redraw)
    }


    function setDireccion (dir) {
        ctx.direccion_id( dir.direccion_id() );
        direccion_id( dir.direccion_id() );

        direccion(dir);
    }
}


DireccionComponent.direccion = function (dir)  {
    return dir ? m('.direccion', [
        m('.linea1', [
            m('span', dir.calle_y_numero()), '  #',
            m('span', dir.no_exterior()),
            dir.no_interior() ? ' int. '  : '',
            dir.no_interior() ? m('span', dir.no_interior()) : ''
        ]),

        m('.linea2', [
            m('span', dir.colonia()),
            ' CP. ', m('span', dir.codigo_postal())
        ]),

        m('.linea3', [
            m('span', dir.ciudad_o_municipio()) , ', ',
            m('span', dir.estado_o_region()) , ', ',
            m('span', dir.codigo_pais())
        ])
    ]) : '';
}

DireccionComponent.view = function (ctx) {
    var dir = ctx.getDireccion();


    return m('div.direccion-holder', {config:ctx.config}, [

        ctx.enabled() ? m('.option',[
            m('i.fa.fa-pencil', {
                onclick : ctx.cambiarDireccion
            })
        ]) : null,

        dir ? DireccionComponent.direccion(dir) : '(No hay dirección)'
    ])
}


var DireccionPicker = {};

DireccionPicker.controller = function (args) {
    var Direccion = oor.mm('Direccion');
    var ctx = this;

    this.direcciones = m.prop([]);

    m.request({
        url : '/api/direcciones/' + args.pertenece_a_id,
        unwrapSuccess : function (r) {
            return r.data.map(function (d) {
                return new Direccion(d);
            })
        },
        background : true
    })
    .then(this.direcciones)
    .then(m.redraw);


    ctx.direccion = new Direccion({
        organizacion_id : oor.uuid(),
        pertenece_a_id:args.pertenece_a_id,
        codigo_pais : args.codigo_pais,
        tipo_id : 3
    });


    function guardarDireccion (direccion) {
        return m.request({
            method : 'POST',
            url : '/api/direcciones/agregar',
            data : direccion
        });
    }


    ctx.guardar = function () {
        guardarDireccion(ctx.direccion)
            .then(function () { ctx.seleccionarDireccion(ctx.direccion) })
    }
    

    ctx.seleccionarDireccion = function (direccion) {
        args.onchange(direccion);
        ctx.$modal.close();
        ctx.$modal.close = _.noop;
    }


    ctx.config = function (element, isInited) {
        if(isInited) return;

        $(element).on('input', 'input', function () {
            var input = $(this);
            var dataModel = input.attr('data-model');
            var dataModelProp = input.attr('data-model-prop');
            ctx[dataModel][dataModelProp]( input.val() );
        })
    }

}

DireccionPicker.view = function (ctx) {
    return m('div.direccion-picker', [

        m('div.direcciones-list', [
            ctx.direcciones().map(function(d) {
                return m('div.direccion-option', {
                    onclick : ctx.seleccionarDireccion.bind(null, d)
                }, DireccionComponent.direccion(d))
            })
        ]),

        m('div.dir-cont', {config:ctx.config}, [
            AgregarTercero.direccionPanel(ctx, {title:'Agregar Dirección'}),
            m('br'),
            m('button.btn.btn-primary.btn-xs.pull-right', {onclick:ctx.guardar},'Guardar')
        ])
    ])
}
