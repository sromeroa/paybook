var TipoDoc = module.exports =  {};


TipoDoc.controller = function (attrs) {
    var selector;
    var tipoOperacion = attrs.tipo_operacion;
    var tiposDeDocumento = oorden.tiposDeDocumento;

    tiposDeDocumento = tiposDeDocumento[tipoOperacion]()

    selector = oor.select2({
        data : tiposDeDocumento.map(adapter),
        model : attrs.tipo_documento,
        find : oorden.tipoDeDocumento,
        onchange : function () {
            attrs.tipoDocumento( selector.selected() );
        }
    });

    function adapter (tDoc) {
        return {
            id : tDoc.tipo_documento_id,
            text : tDoc.nombre
        }
    }

    this.selector = selector;
    this.tiposDeDocumento = tiposDeDocumento;
}

TipoDoc.view = function (ctx) {
    return m('div', [
        ctx.selector.view()
    ])
}
