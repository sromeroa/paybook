module.exports = {
    controller : PagosController,
    view : PagosView
};



function PagosController (params) {
    var pagos = m.prop();
    pagos.cargando = m.prop(false);

    this.operacion = params.operacion;
    this.pagos = pagos;


    this.cargarPagos = function () {
        if(pagos()) return;

        pagos([]);
        pagos.cargando(true);

        oor.request('/movimientos/pagos?operacion_id=' + this.operacion().operacion_id())
            .then(pagos)
            .then(pagos.cargando.bind(null, false))
    }

}

function PagosView (ctx) {
    ctx.cargarPagos();

    return [
        ctx.pagos.cargando() ? m('tr', m('th[colspan=2]', oor.loading() )) : ctx.pagos().map(function (obj) {
            return m('tr', [
                m('td', {style:'padding:5px;'}, [
                    m('a', {
                        href:'/movimientos/movimiento/'.concat(obj.movimiento.movimiento_banco_id)
                    }, [
                        m('div.text-indigo', {style:'white-space:nowrap; text-overflow:ellipsis; overflow:hidden'}, obj.detalle.concepto),
                        m('div.text-grey.small', obj.movimiento.fecha)
                    ])
                ]),
                m('td.text-right', [
                    oorden.organizacion.numberFormat()(-1 * obj.detalle.monto_pago)
                ])
            ])
        }),

        ['A', 'S'].indexOf(ctx.operacion().estatus()) > -1 ? m('tr', [
            m('td', {'style':'padding:5px 0 5px 5px'}, 'Saldo'),

            m('td.text-right', {'style':'padding:5px 5px 5px 0'}, [
                oorden.organizacion.numberFormat()( ctx.operacion().saldo() )
            ])
        ]) : null
    ]
}
