var MonedaComponent =  module.exports = {};

/*
 * moneda || filtro
 *
 * codigo_moneda
 * tipo_de_cambio
 * tasa_de_cambio
 * TasaDeCambio_label
 * tasaDeCambio_label
 *
 */
MonedaComponent.controller = function (attrs) {
    var tiposDeCambio = oorden.tiposDeCambio();
    var selector, moneda, codigo_moneda;
    var tipoDeCambioEnabled = attrs.enabled ? attrs.enabled : m.prop(true);
    var fecha = attrs.fecha;

    this.tipoDeCambio_label = attrs.tipoDeCambio_label;

    this.fecha = fecha;
    this.fechaCargada = m.prop();

    codigo_moneda = (attrs.codigo_moneda) ? attrs.codigo_moneda : m.prop();

    if(typeof codigo_moneda != 'function') {
        throw Error('codigo_moneda debe ser una función');
    }

    if(!attrs.tipo_de_cambio || !attrs.tasa_de_cambio || !attrs.tipoDeCambio) {
        throw Error('Debe especificar tasa_de_cambio, tipo_de_cambio y tipoDeCambio en el Componente de Monedas');
    }

    if(!attrs.tipo_de_cambio()) {
        attrs.tipo_de_cambio(oorden.tipoDeCambio.base().tipo_de_cambio);
    }

    if(!attrs.tipoDeCambio()) {
        attrs.tipoDeCambio( oorden.tipoDeCambio(attrs.tipo_de_cambio()) );
    }

    codigo_moneda( attrs.tipoDeCambio().codigo_moneda );


    //Si hay moneda se filtra la moneda
    if(attrs.moneda) {
        moneda = attrs.moneda;
        (typeof attrs.moneda == 'function') && (moneda = attrs.moneda());
        moneda = oorden.tiposDeCambio[moneda];

        if(moneda) {
            tiposDeCambio = moneda();
        }
    }

    /**
     * 
     */
    this.tipoDeCambioChange = function () {
        attrs.tipoDeCambio( selector.selected() );
        codigo_moneda( attrs.tipoDeCambio().codigo_moneda );
        cargarTasa().then(tasaDeCambio);
    };


    this.tcEsBase = function () {
        return attrs.tipoDeCambio() && (attrs.tipoDeCambio().base == '1');
    };


    this.urlTasaFecha = m.prop();

    function urlTasaFecha () {
        return attrs.tipo_de_cambio() + '/' + fecha();
    }

    this.tasaFecha = m.prop();

    this.cargarTasaFecha = function () {
        if(this.tcEsBase()) return;
        if(!this.fecha || !this.fecha()) return;
        if(this.urlTasaFecha() == urlTasaFecha()) return;

        this.urlTasaFecha( urlTasaFecha() );

        oor.request('/monedas/tasafecha/' + this.urlTasaFecha())
            .then(this.tasaFecha)
    }


    /**
     * Al cambiar la tasa replica el cambio
     */
    function onchange () {
        if(typeof attrs.onchange == 'function') {
            attrs.onchange();
        }
    };

    /**
     * Pseudo modelo para la tasa de cambio
     */
    function tasaDeCambio () {
        if(arguments.length == 0) return attrs.tasa_de_cambio();

        attrs.tasa_de_cambio(arguments[0]);
        onchange();
    }

    /**
     * Carga la nueva tasa
     */
    function cargarTasa () {
        return m.request({
            method:'GET',
            url : '/api/tasa/'.concat( attrs.tipo_de_cambio() ),
            unwrapSuccess : function (d) {
                return d.ultimaTasa.tasa_de_cambio
            }
        });
    }


    function tcAdapter(tc) {
        return {
            id: tc.tipo_de_cambio,
            text : tc.tipo_de_cambio + ' ' + tc.nombre
        }
    }

    /**
     *
     */
    selector = oor.select2({
        data : tiposDeCambio.map(tcAdapter),
        onchange : this.tipoDeCambioChange,
        model : attrs.tipo_de_cambio,
        find : function (tc) {
            return oorden.tipoDeCambio(tc);
        },
        enabled : tipoDeCambioEnabled
    });

    this.selector = selector;

    this.onchange = onchange;

    this.tasaDeCambio = tasaDeCambio;

};


/**
 *
 */
MonedaComponent.view = function (ctrl) {
    ctrl.cargarTasaFecha();

    return m('div', [
        m('div.mt-inputer', [
            ctrl.tipoDeCambio_label ? m('label', ctrl.tipoDeCambio_label ) : '',

            m('div',ctrl.selector.view({style:'min-width:100px;max-width:220px'})),

            ctrl.tcEsBase() ? '' : m('label', {style:'padding-left:10px'}, 'Tasa'),

            ctrl.tcEsBase() ? '' : m('input[type="text"]', {
                placeholder : 'Tasa',
                value : ctrl.tasaDeCambio(),
                onchange : m.withAttr('value', ctrl.tasaDeCambio),
                size : 10
            })

            
        ]),

        FechaTasaMessage(ctrl)
    ])

};



function FechaTasaMessage (ctrl) {
    var tasaFecha, iguales;
    if(ctrl.tcEsBase()) return;
    if(!ctrl.fecha) return;

    tasaFecha = ctrl.tasaFecha();
    if(!tasaFecha) return oor.loading();
    iguales = tasaFecha.tasa == ctrl.tasaDeCambio();


    return m('.small.text-right', {
        'class' : iguales ? 'text-green' : 'text-red'
    }, [
       iguales ? 
            ['Tasa de Cambio Correspondiente a ' + tasaFecha.fecha , m('i.')] : 
            ['No corresponde a la tasa de fecha ', tasaFecha.fecha , ': ', 
                m('a[href=javascript:;]', {
                    onclick : function () { ctrl.tasaDeCambio(tasaFecha.tasa)  }
                } ,tasaFecha.tasa)
            ]
    ]);
}
