var Devolucion = module.exports = {};

Devolucion.controller = function (opCtrl) {
    var ctrl = this;
    ctrl.__proto__ = opCtrl;
    var OperacionItem = oor.mm('OperacionItem');
    var tipoDocumentos = opCtrl.operacion().seccion() === 'V' ? ['VTA'] : ['COM'];

    ctrl.tercero_id  = m.prop(null);
    ctrl.documentos = m.prop();
    ctrl.cargandoDocumentos = m.prop(false);

    /**
     * Obtiene los documentos del tercero, solo si cambió
     */
    ctrl.obtenerDocumentos = function () {
        if(ctrl.tercero_id() == ctrl.operacion().tercero_id()) {
            return ctrl.documentos();
        }
        obtenerDocumentos().then(m.redraw);
        return ctrl.documentos();
    };

    /**
     * obtiene los documentos del tercero
     * activando las banderas de carga, etc
     */
    function obtenerDocumentos () {
        ctrl.tercero_id( ctrl.operacion().tercero_id() );
        ctrl.documentos([]);
        ctrl.cargandoDocumentos(true);

        return obtenerDocumentosTercero(ctrl.tercero_id()).then(function (docs) {
            var documentos = docs.filter(function (doc) {
                if(tipoDocumentos.indexOf(doc.tipo_operacion) == -1){
                    return false;
                }
                return doc.estatus == 'A' || doc.estatus == 'S';
            });
            ctrl.documentos(documentos);
            ctrl.cargandoDocumentos(false);
        });
    }

    /**
     * 
     */
    ctrl.aplicarDocumentoOrigen = function (documento) {
        if(!aplicarDocumentoAOperacion(documento, ctrl.operacion(), ctrl)) {
            return false;
        }
        ctrl.actualizar(true);
        return true;
    }

    /**
     * Seleccionar el documento
     */
    ctrl.seleccionarDocumento = function () {
        if(!ctrl.documentos()) {
            return obtenerDocumentos().then(function () {
                openModal.call(this, ctrl);
                m.redraw();
            }.bind(this));
        }
        openModal.call(this, ctrl)
    };

    return ctrl;
};

Devolucion.view = function (ctrl) {
    var tercero = ctrl.operacion().$tercero();
    var opAnterior = ctrl.operacion().$operacionAnterior();
    opAnterior || ctrl.obtenerDocumentos();

    return m('div.mt-inputer', [
        m('label.pull-left', {
            style:'padding-right:12px'
        }, ctrl.operacion().$tipoDeDocumento().nombre , '  a partir de '),
        
        tercero ? [
            opAnterior ?  Devolucion.documentoCaption(ctrl ,opAnterior) :  Devolucion.seleccionarDocumento(ctrl)
        ] : m('small.text-grey','(Selecciona un tercero)')
    ]);
};

Devolucion.documentoCaption = function (ctrl, opAnterior) {
    var label = opAnterior.label() + ' del ' + opAnterior.fecha();
    var enabled = ctrl.vm.enabled();

    return [
        m('span.text-indigo', {
            style:enabled ? 'cursor:pointer' :'',
            onclick:function () {
                if(enabled == false) return;
                ctrl.seleccionarDocumento.call(this);
            }
        }, label)
    ];
}


Devolucion.seleccionarDocumento = function (ctrl) {
    return ctrl.vm.enabled() ? (ctrl.cargandoDocumentos() ? '...' : (ctrl.documentos().length && ctrl.vm.enabled() ? m('span.text-indigo',{
        onclick: ctrl.seleccionarDocumento,
        style : 'cursor:pointer'
    },'Seleccionar doc. de origen') : 'No hay facturas para el tercero')) : 'Ningún documento' ;
}

/**
 * Seleccionar Documento e Items
 */

var SeleccionarDocumento = {};

SeleccionarDocumento.controller = function (args) {
    var ctrl = this;
    var Operacion = oor.mm('Operacion');

    //Se convierten los documentos en operaciones
    var documentos = args.documentos.map(function (doc) {
        return new Operacion(doc);
    });

    //Crear la tabla de documentos
    var tablaDocumentos = MtTable({
        columnas : ['fecha','documento', 'saldo', 'total'],
        defCol : Columnas(ctrl)
    });

    //Pongo los ítems
    tablaDocumentos.items(documentos);


    ctrl.tablaDocumentos = tablaDocumentos;
    ctrl.documentos = m.prop(args.documentos);
    ctrl.documento = m.prop();
    ctrl.cargandoDocumento = m.prop();

    /**
     *
     */
    ctrl.abrirDocumento = function (documento) {
        ctrl.documento(documento);
        ctrl.cargandoDocumento(true);

        var selected = args.selected || [];
        
        //Cargamos el documento con todo e items
        obtenerItemsDocumento(documento.operacion_id()).then(function (doc) {
            var documento = new Operacion(doc);

            //Establecemos los ítems seleccionados
            documento.items.forEach(function (item) {
                var isSelected = selected.indexOf ? selected.indexOf(item.operacion_item_id()) > -1 : false;
                item.selected = m.prop(isSelected);
            });

            //Se visualiza el documento
            ctrl.cargandoDocumento(false);
            ctrl.documento(documento);
            m.redraw();
        });
    };

    if(args.documento) {
        ctrl.abrirDocumento(args.documento);
    }
};

SeleccionarDocumento.view = function (ctrl) {
    if(!ctrl.documento()) return m('div',ctrl.tablaDocumentos());
    var tercero = ctrl.documento().$tercero();

    return ctrl.cargandoDocumento() ? m('div.text-center',m('i.fa.fa-spin.fa-refresh')) : m('div', [
        m('h5',tercero.nombre),
        m('table.table.bill', [
            m('thead', [
                m('tr', [
                    m('th', ''),
                    m('th', 'Cantidad'),
                    m('th' ,'Devuelta'),
                    m('th', 'Producto'),
                    m('th', 'P. Unit.'),
                    m('th', 'Descuento'),
                    m('th', 'Importe')
                ])
            ]),
            m('tbody', [
                ctrl.documento().items.map(function (item) {
                    var rowClass = '';

                    if(item.selected()) {
                        rowClass = item.cantidadRestante() <= 0 ? 'danger' : 'info';
                    }

                    return m('tr', {class : rowClass },[
                        m('td', SeleccionarDocumento.check(ctrl,item)),
                        m('td', item.cantidad()),
                        m('td', item.cantidad_devuelta()),
                        m('td', item.producto_nombre()),
                        m('td', item.precio_unitario()),
                        m('td', item.descuento()),
                        m('td', item.importe()),
                    ]);
                })
            ])
        ])
    ]);
};


SeleccionarDocumento.check = function (ctrl, item) {
    var mostrarcheck = true; item.cantidad.$int() > item.cantidad_devuelta.$int();

    return item.selected() || mostrarcheck ? m('input[type="checkbox"]', {
        checked : item.selected(),
        onchange: m.withAttr('checked', item.selected)
    }) : '';
};


function Columnas (ctrl) {
    var col = {};

    col.documento  = {
        caption : 'Documento / Ref.',
        value : function (item) {
            return m('div',[
                m('div',[
                    m('strong[style="cursor:pointer"]',{
                        onclick: ctrl.abrirDocumento.bind(null, item)
                    }, item.label())
                ]),
                m('small.text-grey', item.referencia())
            ]);
        }
    };

    return col;
}

function obtenerDocumentosTercero (tercero_id) {
    return m.request({
        method : 'GET',
        url : '/apiv2',
        data : {
            modelo : 'operaciones',
            tercero_id : tercero_id,
            include : 'operaciones.tercero,operaciones.tipoDocumento'
        },
        unwrapSuccess : function (d) { return d.data},
        background : true
    });
}

function obtenerItemsDocumento (operacion_id) {
    return m.request({
        method : 'GET',
        url : '/apiv2',
        data : {
            modelo: 'operaciones',
            operacion_id :operacion_id,
            include : 'operaciones.tercero,operaciones.tipoDocumento,operaciones.items'
        },
        unwrapSuccess : function (d) { return d.data[0] },
        background : true
   })
 }


 /**
  * Abre un modal para seleccionar una operacion de origen
  */

 function openModal (ctrl) {
    
    MTModal.open({
        controller : SeleccionarDocumento.controller,
        args : {
            documentos : ctrl.documentos(),
            documento : ctrl.operacion().$operacionAnterior(),
            selected  : ctrl.operacion().items.map(function (item) {
                return item.operacion_anterior_item_id();
            }).filter(Boolean)
        },
        top : topOfModal ,
        content : SeleccionarDocumento.view,
        bottom : bottomOfModal,
        el : this
    });

    /**
     *
     */
    function topOfModal (mCtrl) {
        return m('h3', mCtrl.documento() ? mCtrl.documento().label() :'Seleccionar Documento de Origen')
    }

     /**
      *
      */
    function bottomOfModal (mCtrl) {
        return [
            m('button.pull-left.btn-flat.btn-sm.btn-default', {
                onclick : cancelar,
            }, mCtrl.documento() ? [m('i.fa.fa-chevron-left'), ' Atrás'] :'Cancelar'),
            mCtrl.documento() ? m('button.pull-right.btn-flat.btn-sm.btn-success', {
                onclick : aplicarDocumento,
            }, 'Seleccionar') : ''
        ];

        function aplicarDocumento () {
            if(ctrl.aplicarDocumentoOrigen(mCtrl.documento())) {
                mCtrl.$modal.close();
            } else {
                toastr.error(ctrl.erroresAplicacion())
            }
        }

        function cancelar () {
            if(!mCtrl.documento()) return mCtrl.$modal.close();
            mCtrl.documento(null);
        }
    };

}



function probarAplicacionDocumento(documento) {
    var itemsSeleccionados = 0;
    var itemsErroneos = 0;

    documento.items.forEach(function (item) {
        if(item.selected()) {
            itemsSeleccionados++;
            if(item.cantidadRestante() <= 0) {
                itemsErroneos++;
            }
        }
    });

    if(itemsSeleccionados == 0) {
        return 'No hay ítems seleccionados';
    }

    if(itemsErroneos > 0) {
        return 'Hay Items que ya han sido devueltos en su totalidad. Es necesario quitarlos para continuar';
    }

    return false;
}




function aplicarDocumentoAOperacion (documento, operacion, ctrl) {
    var OperacionItem = oor.mm('OperacionItem');
    var error = probarAplicacionDocumento(documento);

    //Si la aplicación no se puede efectuar
    //retorna false y manda el error
    if(error) {
        ctrl.erroresAplicacion = m.prop(error);
        return false;
    }

    ctrl.eliminarTodosLosItems(false);


    /*
    Dirección principal
    Contacto
    Vendedor
    Referencia
    Moneda
    Tasa de cambio
    Opciones de mostrar
    */

    operacion.$operacionAnterior( documento );
    operacion.operacion_anterior_id( documento.operacion_id() );

    operacion.referencia( documento.referencia() );

    operacion.tipo_de_cambio( documento.tipo_de_cambio() );
    operacion.tasa_de_cambio( documento.tasa_de_cambio() );

    operacion.$tipoDeCambio ( oorden.tipoDeCambio(documento.tipo_de_cambio()) )
    operacion.moneda(documento.moneda());

    operacion.mostrar_cctos( documento.mostrar_cctos() );
    operacion.mostrar_cuentas( documento.mostrar_cuentas() );
    operacion.mostrar_descuento( documento.mostrar_descuento() );
    operacion.mostrar_impuestos( documento.mostrar_impuestos() );
    operacion.mostrar_retenciones( documento.mostrar_retenciones() );

    documento.items.forEach(function (item) {
        if(!item.selected()) return;
        /*
        Código de producto
        Variantes
        Producto

        Cantidad
        Unidad
        Precio
        Descuento

        Impuesto
        Retención

        Centros de costo
        */

        var producto = {};
        var variante = {};

        var newItem = new OperacionItem({
            operacion_id : operacion.operacion_id(),
            operacion_item_id : oor.uuid(),
            operacion_anterior_item_id : item.operacion_item_id(),
            operacionItemAnterior : item,

            producto_nombre : item.producto_nombre(),
            producto_id : item.producto_id(),
            producto : producto,
            variante : variante,

            cantidad : item.cantidad() - item.cantidad_devuelta(),
            unidad : item.unidad(),
            precio_unitario : item.precio_unitario(),
            descuento : item.descuento(),

            impuesto_conjunto_id : item.impuesto_conjunto_id(),
            retencion_conjunto_id : item.retencion_conjunto_id(),

            //Centros de Costo
            ccto_1_id : item.ccto_1_id(),
            ccto_2_id : item.ccto_2_id(),

            //NO!
            cuenta_id : item.cuenta_id()
        });

        operacion.items.push(newItem);
    });

    return true;
};

