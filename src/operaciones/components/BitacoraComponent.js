module.exports = {
    view : BitacoraView, 
    controller : BitacoraController
}



function BitacoraController (args) {
    var ctx = this;
    ctx.operacion = args.operacion;
    ctx.operacion_id = m.prop(null);
    ctx.registros = m.prop()
    ctx.cargando = m.prop(true)


    ctx.init = function () {
        if(!ctx.operacion().operacion_id()) return;  
        if(ctx.operacion().operacion_id() == ctx.operacion_id()) return;

        ctx.operacion_id( ctx.operacion().operacion_id() );
        cargarRegistros();
    }


    function cargarRegistros () {
        ctx.cargando(true);

        oor.request('/apiv2', 'GET', {
            'data' : {
                modelo:'bitacoras', 
                page_size:100, 
                pertenece_a_id:ctx.operacion_id(), 
                include:'bitacoras.usuario'
            }
        })
        .then(ctx.registros)
        .then(ctx.cargando.bind(null, false))
    }
}


function BitacoraView (ctx) {
    ctx.init();
    if(ctx.cargando()) return oor.loading();


    return m('div', [
        m('h5', 'Bitacora'),
        m('.table-responsive', [
            m('table.table.megatable', {config : oor.time.update}, [
                m('thead', [
                    m('tr', [
                        m('th', 'Fecha'),
                        m('th', 'Usuario'),
                        m('th', 'Actividad')
                    ])
                ]),

                m('tbody', [
                    ctx.registros().map(function (registro) {
                        return m('tr', [
                            m('td', [
                                registro.fecha,
                                m('div.small.text-grey', {
                                    'x-time-till-now' : registro.created_at
                                })
                            ]),
                            m('td', registro.usuario.nombre),
                            m('td', registro.actividad)


                        ])
                    })
                ])
            ])
        ])
    ])
}