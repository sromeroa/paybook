var VTAComponent    = module.exports  = {};

var OpDetail        = require('./mixins/OperacionBase.js');
var OpCompraVenta  = require('./mixins/OperacionCompraVenta.js');


VTAComponent.controller = function (params) {
    var ctrl = this;

    OpDetail.controller.call(this, params);
    OpCompraVenta.controller.call(this, params);

    ctrl.keyPrecioUnitario('precio_unitario_venta');
    ctrl.keyCuentaProducto('cuenta_venta');
    ctrl.keyImpuestoProducto('impuesto_venta');
    ctrl.keyImpuestoTercero('impuesto_venta_id');


    ctrl.opcionesTercero = function () {
        var tercero = ctrl.operacion().$tercero();
        if(!tercero) return;
        ctrl.diasCredito(Number(tercero.dias_credito_venta) || 0);
        ctrl.descuento(Number(tercero.descuento) || 0);
   };

    ctrl.opcionesTercero();
    ctrl.actualizar(true);
    ctrl.loadProductos();
};

VTAComponent.view = OpCompraVenta.view;
