module.exports = LastUsed;

function LastUsed (settings) {

    var lastUsed = {
        user : m.prop({}),
        tercero : m.prop({})
    };

    lastUsed.cargar = function () {
        var deferred = m.deferred();

        var url = '/api/last-used/' + settings.tipoOperacion;
        url += '_' + oorden.organizacion().organizacion_id;
        url += '_' + oorden.usuario().usuario_id;

        m.request({
            method : 'GET',
            url : url,
            unwrapSuccess : f('data')
        })
        .then(lastUsed.user)
        .then(deferred.resolve);

        return deferred.promise;
    };

    lastUsed.cargarTercero = function (tercero) {
        var deferred = m.deferred();

        var url = '/api/last-used/' + settings.tipoOperacion;
        url += '_' + f('tercero_id')(tercero);


        m.request({
            method : 'GET',
            url : url,
            unwrapSuccess : f('data')
        })
        .then(lastUsed.tercero)
        .then(deferred.resolve)

        return deferred.promise;
    }


    return lastUsed;
}


LastUsed.get = function (params) {
   
    var output = {};
    var prefix = params.prefix + '_';

    Object.keys(params.on)
        .filter(function (key) {
            return key.indexOf(prefix) == 0;
        })
        .forEach(function (key) {
            var oKey = key.substr(prefix.length);
            output[oKey] = f(key)(params.on);
        })

    return output;

}