
/**
 * Modelo de Operacion
 */
function Tercero (data) {
  data = data || {};
  Tercero.initializeInstance.call(this,data);
}


oor.mm('Tercero', Tercero)
    .serviceName('terceros')
    .idKey('tercero_id')

    .prop('tercero_id', {})
    .prop('organizacion_id', {})
    .prop('nombre', {})
    .prop('codigo', {})
    .prop('contacto_principal_id', {})
    .prop('es_extranjero', {})
    .prop('clave_fiscal', {})
    .prop('clave_moneda', {})

    .prop('descuento', {})
    .prop('impuesto_venta_id', {})
    .prop('cuenta_venta_id', {})
    .prop('ccto1_venta_id', {})
    .prop('ccto2_venta_id', {})
    .prop('dias_credito_venta', {})


    .prop('impuesto_compra_id', {})
    .prop('cuenta_compra_id', {})
    .prop('ccto1_compra_id', {})
    .prop('ccto2_compra_id', {})
    .prop('dias_credito_venta', {})

    .prop('estatus', {})
    .prop('es_cliente', {})
    .prop('es_proveedor',{})
    .prop('tipo_id',{})
    .prop('telefonos',{})
    .prop('email',{})

    .prop('website',{})
    .prop('ultima_direccion_fiscal',{})
    .prop('ultima_direccion_envio',{})
