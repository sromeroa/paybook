var categoriasYProductos = require('./productos/categoriasYProductos');
var TiposDeCambio = require('./TiposDeCambio');
var contactos = require('./contactos/contactos');
var impuestos = require('./impuestos/impuestos');
var retenciones = require('./impuestos/retenciones');
var cuentasContables = require('./cuentas-contables/cuentasContables');
var pruebaCuentas = require('./cuentas-contables/prueba');
var presupuestos = require('./presupuestos/presupuestos');
var suscripciones = require('./suscripciones/Suscripciones');

module.exports = {
    monedas : TiposDeCambio,
    categoriasYProductos : categoriasYProductos,
    contactos : contactos,
    impuestos : impuestos,
    retenciones : retenciones,
    cuentasContables : cuentasContables,
    pruebaCuentas : pruebaCuentas,
    presupuestos : presupuestos,
    suscripciones: suscripciones
};
