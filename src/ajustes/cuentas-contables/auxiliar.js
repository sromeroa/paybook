var FechasBar2 = require('../../misc/components/FechasBar2');
var CuentaSelector = require('../../misc/selectors/CuentaSelector');
var FiltrosReporte = require('../../misc/components/filtrosReporte');

module.exports = {
    view : AuxiliarView,
    controller : AuxiliarController
};


function AuxiliarController () {
    oor.fondoGris();

    var ctx  = this;

    this.key = m.prop();
    this.updateTable = m.prop(false);
    this.numCuenta  = m.prop('');
    this.cuenta_id = m.prop();
    this.fechaDesde = m.prop('');
    this.fechaHasta = m.prop('');
    this.cargando   = m.prop(false);
    this.cta     = m.prop();
    this.cuenta  = m.prop();
    this.data = m.prop();

    this.mostrarFormulario = m.prop();

    var tree = d3.layout.tree().children(function (d) {
        if(d.partida) return null;
        return ctx.cuentasMap[d.cuenta] || ctx.partidasMap[d.cuenta];
    });

    this.initialize = function () {
        if(ctx.cargando()) return;

        if(!ctx.cuenta() || !ctx.fechaDesde() || !ctx.fechaHasta()) {
            return;
        }

        var nKey = FiltrosReporte.getKey(['cuenta','fechaDesde','fechaHasta'], ctx);
        if(this.key() === nKey) return;
        this.key(nKey);

        console.log(nKey)

        ctx.cargando(true);

        oor.request('/cuentas-contables/auxiliarData'.concat('?',this.key()), 'GET')
            .then(asignar)
            .then(oorden)
            .then(ctx.cargando.bind(null))

    }


    function asignar (data) {
        ctx.cta(data.cuenta);
        ctx.cuenta_id(data.cuenta.cuenta_contable_id);

        ctx.cuentasMap = data.cuentas;
        ctx.partidasMap = {};

        Object.keys(data.partidas).forEach(function (k) {
            data.partidas[k].map(function (p) { return p.partida = true });
            ctx.partidasMap[k] = data.partidas[k];
        });

        var nodes = tree.nodes( ctx.cta() );

        ctx.data(nodes);
        ctx.updateTable(true)

    }

    this.onchange = function (prop) {
        return function (value) {
            prop(value);
            actualizarRuta();
        }
    }

    this.asignarParametros = function (numCta, fechaDesde, fechaHasta) {
        var keyData = this.key().split('/');
        var numCta      = keyData[0];
        var fechaDesde  = keyData[2];
        var fechaHasta  = keyData[3];
        this.numCuenta(numCta);
        this.fechaDesde(fechaDesde);
        this.fechaHasta(fechaHasta);
    }

    function actualizarRuta () {
        var ruta = [ctx.numCuenta(), 'fechas', ctx.fechaDesde(), ctx.fechaHasta()].join('/');
        var uri = '/cuentas-contables/auxiliar/' + ruta;
        history.pushState({}, ruta, uri);
    }


    FiltrosReporte.aplicar(
        ['cuenta','fechaDesde', 'fechaHasta'],
        ctx,
        FiltrosReporte.queryParams(), // Parametros de la búsqueda
        FiltrosReporte.savedParams() // Parametros guardados
                                //Faltarian los parametros default absolutos
    );


    console.log(ctx.mostrarFormulario(true))
}

function AuxiliarView (ctx) {
    ctx.initialize();

    return oor.panel({
        title : 'Auxiliar Contable'
    }, [
        ctx.mostrarFormulario() ? m.component(FiltrosReporte, {
            close    : ctx.mostrarFormulario.bind(null,false),
            aplicarA : ctx,
            value    : ctx,
            seleccionarCuenta : true
        }) : null,

        ctx.cargando() ? oor.loading() : [
            ctx.cta() ? [
                m('h5', {style:'margin-bottom:0px'}, [
                    'Auxiliar Contable '  , ctx.cta().cuenta + ' - ' + ctx.cta().nombre,
                ]),
                m('h6.pull-left', [
                    ' del ' , oor.fecha.prettyFormat.year( ctx.fechaDesde()  ),
                    ' al ' ,  oor.fecha.prettyFormat.year( ctx.fechaHasta()  ),
                    ' '
                ]),
                ' ',
                ctx.mostrarFormulario() ? null : m('a.small',{
                    href:'javascript:;',
                    onclick: ctx.mostrarFormulario.bind(null, true)
                }, 'Modificar Filtros'),

            ] : null,

            ctx.data() ? m('table.tabla-cuentas.tabla-auxiliar', {style:'width:100%;', config : ConfigTable(ctx) }) : null
        ]

    ])
}


function ConfigTable (ctx) {
    return function (el, isInit) {
        if(ctx.updateTable()) {
            d3.select(el).call(Table, ctx.data());
        }
    }
}

function Table (selection, data) {
    selection.html('');

    if(selection.select('tbody').empty()) {
        var theadTr = selection.append('thead').append('tr')

        theadTr.append('th').text('Fecha / Cuenta')
        theadTr.append('th').text('Concepto')

        theadTr.append('th').text('S.In. / Póliza')
        theadTr.append('th').text('Debe').attr('class', 'text-right')
        theadTr.append('th').text('Haber').attr('class', 'text-right')
        theadTr.append('th').text('S Final').attr('class', 'text-right')


        selection.append('tbody');
    }

    data = data.filter(function (d) {
        return d.partida || (d.children && d.children.length);
    });

    var tbody    = selection.select('tbody');
    var rows     = tbody.selectAll('tr').data(data);
    var trEnter  = rows.enter().append('tr');

    trEnter.each(function (p) {
        var tr = d3.select(this);

        if(!p.partida) {

            tr.attr('class', 'cuenta')
                .style('background-color', '#f6f6f6')



            tr.append('td').attr('class','cuenta').attr('colspan',2)
                .append('h5')

            //tr.append('td').attr('class','sinicial-label text-right')
            //            .append('h5').style('font-size', '12px')

            tr.append('td').attr('class','sinicial text-right')
                        .append('h5')

            tr.append('td').attr('class','sdebe text-right')
                        .append('h5')

            tr.append('td').attr('class','shaber text-right')
                        .append('h5')

            tr.append('td').attr('class','sfinal text-right')
                        .append('h5')


            /*
            tr.append('td').attr('class', 'concepto')
                .style('padding-left', function (d) { return String(d.depth * 10 + 10).concat('px') })
                .append('h5').style('margin-bottom', 0)

            tr.append('td').attr('class', 'fecha text-right small text-grey')
            tr.append('td').attr('class', 'poliza text-right').attr('colspan',2)
            tr.append('td').attr('class', 'debe text-right')
            tr.append('td').attr('class', 'haber text-right')
            tr.append('td').attr('class', 'sfinal text-right')

            */

            return;
        }


        tr.attr('class', 'partida')

        tr.append('td').attr('class', 'fecha text-right').style('padding-left','60px')
            .append('h6')



        tr.append('td').attr('class', 'concepto')

            .append('h6')

        //tr.append('td').attr('class', 'poliza-tipo text-right').append('h6')
        tr.append('td').attr('class', 'poliza text-right').append('a').append('h6').attr('class', 'text-indigo')
        tr.append('td').attr('class', 'debe text-right').append('h6')
        tr.append('td').attr('class', 'haber text-right').append('h6')
        tr.append('td').attr('class', 'sfinal text-right').append('h6')
    })


    var partidas = rows.filter( f('partida').is(true) );
    var labelPoliza = {D: 'Diario', E:'Egreso', I:'Ingreso'};

    partidas.attr('data-id', function (p) { return p.poliza_id })
    partidas.select('td.fecha h6').text(function(d) { return oor.fecha.prettyFormat.year(d.fecha) })
    partidas.select('td.concepto h6').text(function(d) { return d.concepto })

    //partidas.select('td.poliza-tipo h6').text(function (d) { return labelPoliza[d.tipo] })
    partidas.select('td.poliza h6').text(function (d) {
        return labelPoliza[d.tipo] + ' ' + d.numero;
    });

    partidas.select('td.poliza a').attr('target','_blank').attr('href',function (d) {
        return '/polizas/poliza/' + d.poliza_id + '?poliza_partida_id=' + d.poliza_partida_id
    });

    partidas.select('td.debe h6').text(function(d) { return  d.debe_o_haber == 1 ? oorden.org.format(d.importe)  : '' })
    partidas.select('td.haber h6').text(function(d) { return d.debe_o_haber ==-1 ? oorden.org.format(d.importe)  : ''})
    partidas.select('td.sfinal h6').text(function(d) { return oorden.org.format(d.saldoFinal) })


    var cuentas = rows.filter( function (d) { return !d.partida } );

    cuentas.select('td.nivel h5').text(f('nivel'))
    cuentas.select('td.cuenta h5')
        .text( function (d) { return d.cuenta + ' - ' + d.nombre })
        .style('padding-left', function (d) { return (d.depth * 15) + 'px' })


    cuentas.select('td.sinicial-label h5')
        .text('Saldo Inicial:')

    cuentas.select('td.sinicial h5').text(function (d) {
        return  oorden.org.format(d.saldoInicial) ;
    })


    cuentas.select('td.sdebe h5').text(function(d) { return oorden.org.format(d.debe) })
    cuentas.select('td.shaber h5').text(function(d) { return oorden.org.format(d.haber) })
    cuentas.select('td.sfinal h5').text(function(d) { return oorden.org.format(d.saldoFinal) })
}
