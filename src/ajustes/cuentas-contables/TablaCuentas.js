module.exports = TablaCuentas;

var dynTable = require('../../nahui/dtable');
var Field = require('../../nahui/nahui.field');


function TablaCuentas (ctx) {
    var table = dynTable()
                    .key( f('cuenta_contable_id') )
                    .rowUpdate( function (sel) {
                        sel.attr('class', function (r) {
                            return r.estatus == 2 ? 'cuenta-oculta' : ''
                        })
                    })


    var fields = {};
    var nFormat = d3.format(",.2f");

    table.TIPOS = {};

    fields.info = Field('info', {
        caption : ' ',
        enter : function (sel) { 
            sel.call(Field.enter)
                .append('button')
                    .attr('class',' btn btn-xs btn-flat')
                    .append('i')
                    .attr('class','fa')
                    .style('cursor','pointer') 

        },
        update : function(sel) { 

            sel
                .attr('x-action', function (d) {
                    if(d.row.acumulativa == 1) {
                        return ctx.opened()[d.row.cuenta_contable_id] ? 'close' : 'open'
                    }
                    return '';
                })
                .style('display', function (d) {  
                    if(!ctx.esArbol()) return 'none';
                    return d.row.acumulativa == 1 ? 'inherit' : 'none'  
                })
                .select('.fa').classed({
                    'fa-minus-square-o' : function (d) { return ctx.opened()[d.row.cuenta_contable_id] },
                    'fa-plus-square-o' : function (d) {  return !ctx.opened()[d.row.cuenta_contable_id] }
                }) 
        }
    })

    fields.cuenta = Field('cuenta', {
        caption : 'Cuenta',

        enterEdit : function (selection) {
            selection.call(Field.enterEdit)
                .append('div').attr('class', 'cuenta-padre')
                
            selection.select('.cuenta-padre')
                .append('div').attr('class', 'cuenta-padre-cuenta')

            selection.select('.cuenta-padre')
                .append('div').attr('class', 'cuenta-padre-nombre')

            selection.select('.cuenta-padre')
                .append('div').attr('class', 'cuenta-padre-error')



            var formatoCuentas = f('formato_cuentas')( oorden.organizacion() );
            formatoCuentas = formatoCuentas.replace(/\d/g, '*');
           
            selection.call(function () {
                    $('input', this.node())
                        .mask(formatoCuentas)
                        .focus()
                })

        },
        enter : function (sel) {
            sel.call(Field.enter).append('span')
        },
        update : function (sel) {
            sel.call(Field.update)
                .classed({
                    //'text-bold' : function (d) { return d.row.depth == 1}
                })
        }
    });


    fields.tipo_id = Field('tipo_id', {
        caption : ' ',
        enterEdit : function (sel) {
            var dOptions = Object.keys(table.TIPOS)
                .map(function (t) { return table.TIPOS[t] });
               
            var gOptions = dOptions.filter(function (t) { return ! f('padre')(t) })
                .map(function (padre) {
                    padre.children = dOptions.filter(function (t) { return f('tipo_id')(padre) == f('padre')(t) });
                    return padre;
                });
    
            var optGroups = sel.append('select').attr('x-prop', 'tipo_id').selectAll('optgroup').data(gOptions)
            optGroups.enter().append('optgroup').attr('label',f('nombre'))

            var options = optGroups.selectAll('option').data(function (t) { return t.children })
            options.enter().append('option').text(f('nombre')).attr('value', f('tipo_id'))


        },
        filter : function (t) { 
            var tipo = table.TIPOS[t];
            
            if(!tipo) {
                console.log('no Existe' + t);
            }

            return tipo ? tipo.nombre : '(NO ESPECIFICADO)'
        }, 


        update: function (sel) {
            sel.call(Field.update)
                .style('display', function (d) { return d.row.depth == 1 ? 'block' : 'none' })
                
        }
    });

    fields.infoCuenta = Field.twice('infoCuenta', {
        caption :' ',
        subfields : [fields.cuenta, fields.tipo_id]
    });

    fields.nombre = Field.linkToResource('nombre', {
        caption : 'Nombre',
        enterEdit : true,
        url : function (d) {
            return '/cuentas/editar/' + f('cuenta_contable_id')(d);
        },
        enter : function (sel) {
            sel.call(Field.linkToResource.enter).classed({'pull-left':true})
        },
        update : function (sel) {
            sel.call(Field.linkToResource.update).select('a')
                .classed({
                    'text-indigo' : function (d) { return d.row.depth == 1}
                })
                .style('color', function (d) { 
                    return [undefined, '#444', '#666', '#888', '#888'][d.row.depth -1] 
                })
        }
    });

    fields.descripcion = Field('descripcion', {
        caption : 'Descripción',
        class :'text-grey'
    });


    fields.titulo = Field.twice('titulo',{
        subfields : [fields.nombre, fields.info, fields.tipo_id, fields.descripcion],
        update : function (sel) {
            sel.style('padding-left', function (d) {
                return String(8 + (d.row.depth-1) * 15).concat('px');
            })
            .call(Field.twice.update)
        } 
    });


    fields.buttons = Field('buttons',{
        caption : ' ',
        enter : function (sel) {
            sel.call(Field.enter);

            var div = sel.append('div')
                    .attr('class', 'btn-group btn-group-xs dropdown');

            div.append('button')
                .attr('class', 'btn btn-flat btn-sm btn-default dropdown-toggle')
                .attr('data-toggle', 'dropdown')
                .style('font-size', '12px')
                .append('i').attr('class', 'fa fa-ellipsis-h')


            var deSistema = f('de_sistema')( sel.datum().row )
            var list = div.append('ul').attr('class','dropdown-menu dropdown-menu-right')

            
            /*
            list.append('li')
                .append('a').attr('href', 'javascript:;').text('Agregar Subcuenta')
                .attr('x-action', 'agregarSubcuenta')
                .attr('x-id', function (d) { return d.row.cuenta_contable_id })
            */


            list.append('li')
                .classed('disabled', deSistema == '1')
                .append('a').attr('href', 'javascript:;')
                .attr('x-action', 'ocultar')
                .attr('x-id', function (d) { return d.row.cuenta_contable_id })

            list.append('li')
                .classed('disabled', deSistema == '1')
                .append('a').attr('href', 'javascript:;').text('Eliminar')
                .attr('x-action', 'eliminar')
                .attr('x-id', function (d) { return d.row.cuenta_contable_id })


            /*

            list.append('li').attr('class','divider')
            list.append('li')
                .classed('disabled', deSistema == '1' ? 'none' : 'block')
                .append('a')
                .attr('x-id', function (d) { return d.row.cuenta_contable_id })
                .attr('x-action', 'eliminar')
                .attr('href', 'javascript:;')
                .text('Eliminar')

            */
        },
        update : function (sel) {
            sel.select('[x-action=ocultar]').text(function (d) {
                return d.row.estatus == '2' ? 'Desarchivar' : 'Archivar'
            })
        },

        'class' : 'text-right'
    });

    fields.saldo = Field.linkToResource('saldo',{
        caption :'S. Final',
        value : ctx.saldos.getter('saldo_final'),
        enterEdit : function (sele) {
            sele.append('button')
                .attr('class', 'btn btn-sm btn-xs btn-success pull-left')
                .attr('x-action', 'commit')
                .html('<i class="fa fa-save"></i>')

            sele.append('button')
                .attr('class', 'btn btn-sm btn-xs btn-default pull-left')
                .attr('x-action', 'cancel')
                .html('&times;')
        },
        url : function (d) {
            if(d.acumulativa == 1) {
                return 'javascript:;'
            }
            return '/reportes/auxiliar/' + f('cuenta_contable_id')(d) + '?' + ctx.saldos.search()
        },
        headingClass : 'text-right',
        class : 'pull-right',
        filter : ctx.saldos.formato(nFormat)
    });


    fields.cargos = Field('cargos', {
        url : function (d) {
            return '/cuentas/' + d.cuenta_contable_id;
        },
        value : ctx.saldos.getter('debe'),
        filter : ctx.saldos.formato(nFormat),
        caption :'Cargos',
        'class' : 'text-right',
        headingClass : 'text-right'
    });

    fields.abonos = Field('abonos', {
        caption :'Abonos',
        url : function (d) {
            return '/cuentas/' + d.cuenta_contable_id;
        },
        value : ctx.saldos.getter('haber'),
        filter : ctx.saldos.formato(nFormat),
        'class' : 'text-right',
        headingClass : 'text-right'
    });

    fields.saldoInicial = Field('saldoInicial', {
        caption :'S. Inicial',
        url : function (d) {
            return '/cuentas/' + d.cuenta_contable_id;
        },
        value : ctx.saldos.getter('saldo_inicial'),
        filter : ctx.saldos.formato(nFormat),
        'class' : 'pull-right',
        headingClass : 'text-right'
    });




    table.columns = [
        //fields.info,
        fields.cuenta, 
        fields.titulo, 
        fields.saldoInicial,
        fields.cargos,
        fields.abonos,
        fields.saldo,
        fields.buttons
    ];

    return table;   
}