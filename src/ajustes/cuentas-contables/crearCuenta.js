module.exports = {
    controller     : CrearCuentaController,
    view           : CrearCuentaView,
    initializaMask : initializeMask
};

function CrearCuentaController (params) {
    var ctx = this;


    ctx.ready = m.prop(false);
    ctx.numDeCta = m.prop();
    ctx.numDeCta.isValid = m.prop(false);
    ctx.numDeCta.message = m.prop()

    ctx.cuentaPadre = m.prop();
    ctx.cuentaPadre.cargando = m.prop(false);

    ctx.cuenta = {};
    ctx.cuenta.subcuenta_de = m.prop(null);
    ctx.cuenta.cuenta = ctx.numDeCta;
    ctx.cuenta.nombre = m.prop('');
    ctx.cuenta.tipo_id = m.prop();
    ctx.cuenta.naturaleza = m.prop();

    ctx.guardando = m.prop(false);

    ctx.cancel = function () {
        if(_.isFunction(params.oncancel)) {
            params.oncancel.call(ctx);
        }
    }

    ctx.initialize = function () {
        oorden().then(ctx.ready.bind(null,true));
    };

    ctx.buscarCuentaPadre = function () {
        if(ctx.cuentaPadre.cargando()) return;
        if(!ctx.numDeCta())  {
            ctx.cuentaPadre(null)
            ctx.numDeCta.message('Número de cuenta no válido');
            return;
        }

        var url = '/api/buscarCuentaPadre/'.concat(ctx.numDeCta());

        ctx.cuentaPadre.cargando(true);
        ctx.cuentaPadre(null)

        console.log('ctaparde', ctx.cuentaPadre.cargando(true));

        m.request({url:url, method:'GET'}).then(asignarCuentaPadre);
        m.redraw();
    };

    ctx.asignarCuenta = function (cta) {
        if(ctx.numDeCta() == cta) {
            return;
        }

        ctx.cuentaPadre(null);

        ctx.numDeCta(cta);
        ctx.numDeCta.isValid(false);

        ctx.buscarCuentaPadre();
    };


    ctx.guardarCuenta = function () {
        if(ctx.guardando()) {
            return;
        }

        ctx.guardando(true);

        oor.request('/apiv2/agregar?modelo=cuentas', 'POST', {
            data : ctx.cuenta
        })
        .then(function (d) {
            nh.isFunction(params.onsave) && params.onsave.call(ctx, d);
            ctx.guardando(false);
        });
    };

    ctx.infoEsValida = function () {
        return ctx.numDeCta.isValid() && ctx.cuenta.nombre();
    };

    function asignarCuentaPadre (p) {
        ctx.cuenta.subcuenta_de(null);
        ctx.numDeCta.message(null);

        if(!p) {
            ctx.numDeCta.message('Error: num de cuenta no tiene padre')
        }


        if(p.error) {
            ctx.numDeCta.isValid(false)
            ctx.numDeCta.message(p.error)
            p = null
        }

        ctx.cuentaPadre(p);
        ctx.cuentaPadre.cargando(false);

        if(!p) return;

        ctx.cuenta.subcuenta_de(p.cuenta_contable_id);
        ctx.cuenta.tipo_id( p.tipo_id );
        ctx.cuenta.naturaleza(p.naturaleza);
        ctx.numDeCta.isValid(true);
    }
}

function CrearCuentaView (ctx) {
    ctx.initialize();

    return m('div', ctx.ready() ? [
        ctx.$modal ? null : [
            m('.close', {onclick:ctx.cancel}, m.trust('&times;')),
            m('h5', 'Crear Cuenta')
        ],

        m('table.megatable.table', [
            m('tbody', [
                m('tr', [
                    m('td.text-grey.text-right','Cuenta: '),
                    m('td[colspan=3]', [
                        m('input[type=text]', {
                            config:initializeMask(ctx),
                            disabled : ctx.cuentaPadre.cargando(),
                            onchange: m.withAttr('value', ctx.asignarCuenta)
                        }),

                        ctx.cuentaPadre.cargando() ? oor.loading(true) : null,
                        ctx.numDeCta.message ? m('span.text-red',ctx.numDeCta.message()) : null
                    ])
                ]),

                ctx.numDeCta.isValid() ? [
                    ctx.cuentaPadre() ? m('tr', [
                        m('td.text-grey.text-right', 'Subcuenta de: '),
                        m('td[colspan=3].text-indigo', [
                             ctx.cuentaPadre().cuenta, ' - ', ctx.cuentaPadre().nombre
                        ])
                    ]) : null,

                    m('tr', [
                        m('td.text-grey.text-right','Nombre: '),
                        m('td[colspan=3]', [
                            m('input[type=text]', {
                                style:'width:100%',
                                value : ctx.cuenta.nombre(),
                                oninput : m.withAttr('value', ctx.cuenta.nombre),
                                config : function (el, i) { if(!i) el.focus() }
                            })
                        ])
                    ]),

                    m('tr', [
                        m('td.text-grey.text-right','Tipo: '),
                        m('td', [
                            ctx.cuentaPadre() ? ctx.cuenta.tipo_id() : m('input[type=text]', {
                                onchange : m.withAttr('value', ctx.cuenta.tipo_id)
                            })
                        ]),
                        m('td.text-grey.text-right','Naturaleza: '),
                        m('td', NaturalezaLabel(ctx.cuenta.naturaleza()))
                    ])
                ] : null

            ])
        ]),

        ctx.$modal ? null : [
            ctx.infoEsValida() ?  oor.stripedButton('button.btn-success.pull-right', 'Guardar', {
                onclick : ctx.guardarCuenta
            }) : null
        ]

    ] : oor.loading());
}

function NaturalezaLabel (n) {
    return n == 'D' ? 'Deudora' : 'Acreedora';
}


function initializeMask (ctx) {
    return function (element, isInited) {
        if(isInited) return;
        var formatoCuentas = f('formato_cuentas')( oorden.organizacion() );
        formatoCuentas = formatoCuentas.replace(/\d/g, '9');
        $(element).mask(formatoCuentas, {
            completed : function () {
                ctx.numDeCta.message(null)
                ctx.asignarCuenta(this.val())
            }
        });
        element.focus();
    }
}
