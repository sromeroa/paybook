module.exports = CuentaViewModel;

var Cuenta = require('./CuentaModel');

function CuentaViewModel (params) {
    this.cuenta = m.prop();

    this.cuentaPadre = m.prop();

    this.buscandoPadre = m.prop();

    this.errorCuenta = m.prop();

    this.guardando = m.prop();

    this.buscarCuentaPadre = function () {
        this.buscandoPadre(true);

        return m.request({
            method : 'GET',
            url : '/api/buscarCuentaPadre/' + this.cuenta().cuenta()
        }).then(function (d) {
           this.cuentaPadre(null);
           this.errorCuenta(null);

            if(d.error) {
                this.errorCuenta(d.error)
                return;
            }

            this.cuentaPadre(d);

            this.cuenta().tipo_id( d.tipo_id );
            this.cuenta().subcuenta_de( d.cuenta_contable_id )
            this.cuenta().naturaleza( d.naturaleza)

        }.bind(this));

    }.bind(this);



    this.guardar = function () {
        var errors = Cuenta.validar(this.cuenta());

        if(this.errorCuenta()) {
            errors.push(this.errorCuenta())
        }

        if(errors.length) {
            if(nh.isFunction(params.onsaveError)) {
                params.onsaveError(errors)
            }
            return;
        }

        this.guardando(true);
        update();

        Cuenta.guardar(this.cuenta())
            .then(function (rCuenta) {
                this.guardando(false);
                update();
                toastr.success('¡Cuenta Guardada!');

                if(nh.isFunction(params.onsave)) {
                    params.onsave( rCuenta )
                }
            }.bind(this));

    }.bind(this);



    var update = function () {
        if(!update.element()) {
            return;
        }
        d3.select(update.element()).call(updateForm, this)
    }.bind(this);

    update.element = m.prop();


    this.bridge = function (element) {

        var events = {};

        events.change = function (message) {
            var prop = this.cuenta()[message.prop];

            prop(message.value);
            
            if(message.prop == 'cuenta') {
                update();
                if(!message.value) {
                    toastr.error('¡Número de cuenta inválido!')
                }

                this.buscarCuentaPadre()
                    .then(update)
            }

        }.bind(this);


        events.commit = this.guardar;

        events.cancel = function () {
            nh.isFunction(params.oncancel) && params.oncancel();
        }


        update.element(element)

        $(element).on('change', '[x-prop]', function () {
            var input = $(this);
            var msg = {
                type : 'change',
                prop : input.attr('x-prop'),
                value : input.val()
            };
            emit(msg);
        });


        $(element).on('input', '[x-prop]', function () {
            var input = $(this);
            var msg = {
                type : 'input',
                prop : input.attr('x-prop'),
                value : input.val()
            };
            emit(msg);
        });


        $(element).on('click', '[x-action]', function () {
            var button = $(this);
            var msg = {
                type : button.attr('x-action')
            }

            emit(msg);
        })


        function emit (message) {
            console.log(message)
            if(nh.isFunction(events[message.type])){
                events[message.type](message)
            }
        }

    }.bind(this);

}



function updateForm (selection, context) {
    var cuentaPadre = context.cuentaPadre();
    var errCtaPadre = context.errorCuenta();

    selection.select('.cuenta-padre-cuenta')
        .text(cuentaPadre ? f('cuenta')(cuentaPadre) : '')

    selection.select('.cuenta-padre-nombre')
        .text(cuentaPadre ? f('nombre')(cuentaPadre) : '')

    selection.select('.cuenta-padre-error')
        .text(errCtaPadre ? errCtaPadre : '')

    selection.select('[x-prop=tipo_id]')
        .attr('disabled', cuentaPadre ? 'disabled' : undefined)
        .property('value', context.cuenta().tipo_id())

    selection.select('[x-action=commit]')
        .classed('disabled', context.guardando() ? true : false)
        .select('i')
        .classed({
            'fa-save' : !context.guardando(),
            'fa-spinner' : context.guardando(),
            'fa-spin' : context.guardando()
        })

}