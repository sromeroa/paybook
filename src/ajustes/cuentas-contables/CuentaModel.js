module.exports = Cuenta;


var fields = {
    cuenta_contable_id : {},
    nombre : {},
    cuenta : {},
    subcuenta_de : {},
    tipo_id:{},
    naturaleza : {},
    descripcion : {},
    codigo_oficial : {},
    pago_permitido : { filter :function (d) { return Boolean(Number(d))}},
    es_bancos : { filter :function (d) { return Boolean(Number(d))}},

    banco : {},
    cuenta_bancaria : {},
    moneda : {},
    numero_cheque : {},

    impuesto_conjunto_id : {}
};


var obtenerTipos = null;


function Cuenta (data) {
    var obj = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined')
        {
            fieldVal = null;
        }

        obj[k] = m.prop(fieldVal);
    });

    obj.$isNew = m.prop(false);
    obj.$isNew.toJSON = function () { return undefined }
}

Cuenta.id = f('cuenta_contable_id');

Cuenta.tipos = m.prop({});
Cuenta.tiposAgrupados = m.prop([]);

Cuenta.obtenerTipos = function () {

    if(!obtenerTipos) {
        obtenerTipos = m.request({
            url : '/cuentas-contables/tipos',
            method : 'GET',
            unwrapSuccess : f('data')
        })
        .then(function (tipos) {
            var glbTipos = Cuenta.tipos();

            tipos.forEach(function (tipo) { glbTipos[tipo.tipo_id] = tipo; });

            var oGroups = Object.keys(tipos)
                .filter(function (t) {
                    return !tipos[t].padre
                })
                .map(function (t) {
                    var mTipo = tipos[t];
                    var children =  tipos.filter(function (tipo) { 
                        return tipo.padre == mTipo.tipo_id 
                    });

                    return nh.extend(mTipo, {children:children});
                });

            Cuenta.tiposAgrupados(oGroups)
        });
    }

    return obtenerTipos;
}

Cuenta.validar = function (cuenta) {
    var errores = [];

    if(!cuenta.nombre()) {
        errores.push({ prop : 'nombre', message : 'Se necesita una cuenta'})
    }

    if(! cuenta.cuenta()) {
        errores.push({ prop : 'cuenta', message : 'Especifique una número de cuenta válido'})
    }


    return errores;
}


Cuenta.guardar = function (cuenta) {

    var isNew = cuenta.$isNew();
    var method = isNew ? 'POST' : 'PUT';
    var url = '/apiv2/';
    url = url + (isNew ? 'agregar' : 'editar/' + Cuenta.id(cuenta)) + '?modelo=cuentas';

    return m.request({
        method : method,
        url : url,
        data : cuenta,
        unwrapSuccess : f('data')
    })
}