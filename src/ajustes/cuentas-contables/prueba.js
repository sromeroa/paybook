module.exports = {
    controller : PruebaController,
    view : PruebaView,
    ejecutar: ejecutarPrueba
}


var ddui;

function PruebaController () {
    
}


function PruebaView () {
    
}


function ejecutarPrueba () {
    var rootNode = document.querySelector('#pruebas');
    var scope = new Scope;

    window.scope = scope;

    window.update = function () {
        update(rootNode, scope);
    }


    var $UPDATE = _.debounce(function () { update(rootNode, scope); }, 100);


    initialize(rootNode, scope);
    update(rootNode, scope);

    $(rootNode).on('input', '[x-prop]', function () {
        var input = $(this);
        var prop =  scope[input.attr('x-prop')];
        prop( input.val() );
        scope.change( prop() );
        $UPDATE();
    });


    $(rootNode).on('change', '[x-prop]', function () {
        var input = $(this);
        var prop =  scope[input.attr('x-prop')];
        prop( input.val() );
        scope.change( prop() );
        $UPDATE();
    });
}


function initialize (rootNode, scope) {

    var node = d3.select(rootNode)
        
    node.append('input')
        .attr('type', 'text')
        .attr('x-model', 'cuenta')
        .attr('x-prop', 'cuenta')
        .attr('placeholder', 'charmander')


    var cuentaPadre = node.append('div')
                        .attr('class', 'cuenta-padre')


    cuentaPadre.append('h3')
    cuentaPadre.append('h4')
    
}


function update (rootNode, scope) {
    var node = d3.select(rootNode);

    node.select('input[x-prop=cuenta]')
        .property('value',  f('cuenta')(scope))

    node.select('.cuenta-padre')
        .attr('display', f('cuentaPadre')(scope) ? 'block' : 'none')

    node.select('.cuenta-padre h3').text( f('cuentaPadre')(scope) );

}

function Scope () {
    this.cuenta = m.prop('__');

    this.cuentaPadre = m.prop();

    this.change = (function ($this) {
        return function (val) {
            $this.cuentaPadre('length: ' + this.cuenta().length)
        }

    })(this);
}
/*
var cForm = xui();

cForm
    .on('input', '[x-prop]', function () {
        var input = $(this);
        var prop = scope[input.attr('x-prop')]
    })
    .on('change', '[x-prop]', function () {

    })
    .update(function () {

    })


function cForm (ctx) {
    return xui()

}

xui.mount(element, cForm, Scope);
*/

