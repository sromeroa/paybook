module.exports = {
    controller : EstadoResultadosController,
    view : EstadoResultadosView
};

var Catalogo = require('./catalogo')
var FechasBar = require('../../misc/components/FechasBar2');

function EstadoResultadosController (){
    oor.fondoGris()
    var ctx = this;

    ctx.datos = m.prop();
    ctx.datos.cargando = m.prop(false)
    ctx.updateTable = m.prop(false);

    ctx.fechaDesde = m.prop(null);
    ctx.fechaHasta = m.prop(null);

    ctx.formatFecha = oor.fecha.prettyFormat.year;
    ctx.mostrarFormulario = m.prop(true);

    ctx.fechasBar = new FechasBar.controller({ onchange : _.noop() })
    ctx.fechasBar.esteMes();

    ctx.key = m.prop(null);

    ctx.aplicar = function () {
        if(ctx.fechasBar.fechaDesde() && ctx.fechasBar.fechaHasta()) {
            ctx.fechaDesde(  ctx.fechasBar.fechaDesde() )
            ctx.fechaHasta(  ctx.fechasBar.fechaHasta() )
            ctx.mostrarFormulario(false)
        } else {
            toastr.warning('Las fechas no son correctas');
        }
    }

    ctx.buildKey = function () {
        var keys = [];

        if(ctx.fechaDesde() && ctx.fechaHasta()) {
            keys.push('desde:'.concat(ctx.fechaDesde()))
            keys.push('hasta:'.concat(ctx.fechaHasta()))
        }

        return keys.join('#');
    }


    ctx.layout = d3.layout.tree().children(function (d) {
        return ctx.datos()[ d.id ];
    });


    ctx.cargarDatos = function () {
        var key = ctx.buildKey();
        console.log('key', key);

        if(!key || ctx.datos.cargando() || key == ctx.key()) {
            return
        }

        ctx.datos.cargando(true);
        ctx.key( ctx.buildKey() );

        oorden().then(function () {
            return oor.request('/cuentas-contables/datosEstadoResultados', 'GET', {
                data : {
                    fechaDesde : ctx.fechaDesde(),
                    fechaHasta : ctx.fechaHasta()
                }
            })
        })
        .then(ctx.datos)
        .then(ctx.datos.cargando.bind(null,false))
        .then(ctx.updateTable.bind(null,true))

    }
}



function EstadoResultadosView (ctx) {
   // ctx.cargarCuentas();
    ctx.cargarDatos();

    return oor.panel({
        title : 'Estado de Resultados 2',
        buttons : [
            ctx.mostrarFormulario() ? null : oor.stripedButton('button.btn-primary', 'Seleccionar Fechas', {
                onclick : ctx.mostrarFormulario.bind(null, true)
            })
        ]
    },[

        ctx.mostrarFormulario() ? m('div.row', [
            m('.col-sm-10', FechasBar.view(ctx.fechasBar) ),
            m('.col-sm-2.text-right',[
                oor.stripedButton('button.btn-primary', 'Aplicar', {
                    onclick: ctx.aplicar,
                    'style' : 'margin-top:20px'
                })
            ])
        ]) : [

            ctx.datos() ? m('h5', [
                'Estado de Resultados del ',
                m('span', ctx.formatFecha( ctx.fechaDesde() )),
                ' al ',
                m('span', ctx.formatFecha( ctx.fechaHasta() )),
            ]) : null,

            ctx.datos.cargando() ? oor.loading() :m('table', {
                style:'width:100%',
                config : EstadoResultadosTable(ctx)
            })
        ]
    ])
}




function EstadoResultadosTable(ctx) {
    return function (element, isInited) {
        if(ctx.updateTable() == false) return false;


        var data = ctx.layout.nodes(ctx.datos().root);
        console.log(data);
        d3.select(element).call(EstadoResultados, data, ctx)
    }
}

function EstadoResultados(selection, data, ctx) {
    if( selection.select('thead').empty() ) {

        selection.append('thead').append('tr')
            //.style('background-color', 'steelblue');

        selection.select('thead tr').append('td').text('')

        selection.select('thead tr').append('td')
            .style('padding', '12px')
            .append('h5')
            .attr('class', 'text-right').text('Periodo')

        selection.select('thead tr').append('td')
            .style('padding', '12px')
            .append('h5')
            .attr('class', 'text-right').text('Acumulado')

        selection.append('tbody');
    }

    selection = selection.select('tbody');

    var rows = selection.selectAll('tr').data(data, f('id'));
    var enterRow = rows.enter().append('tr');


    enterRow.each(function (d) {
        var self = d3.select(this);

        self.append('td').attr('class','cuenta')
            .append('h6')
            .style('padding-left', function (d) { return String(d.depth * 20).concat('px') }).style('margin', '0px')


        if(d.clase === 'P') {
            self.select('td.cuenta').attr('colspan', 3);
            return;
        }

        self.append('td')
                .attr('class', 'saldo-periodo').style('padding', '0 12px')
            .append('h6')
                .style('margin', '0px').attr('class', 'text-right')


        self.append('td')
                .attr('class', 'saldo-total').style('padding', '0 12px')
            .append('h6')
                .style('margin', '0px').attr('class', 'text-right')
    });

    enterRow.style('border', '1px solid #eee')

    rows.selectAll('.cuenta h6')
        .text(f('nombre'))
        .style('padding-left', function (d) { return String(d.depth * 20).concat('px') })

    rows.selectAll('h6').style('font-weight', function (d) {
        return d.clase == 'G' ? 'bold' : undefined
    });

    rows.selectAll('td h6')
            .classed({ 'text-center' : function (d) {  return d.clase == 'P'} })
            .filter( f('clase').is('P') )
            .style('font-size', '1.2em')
            .style('margin','12px')
            .style('text-transform', 'uppercase')

    rows.filter( f('clase').is('T') )
        .style('background-color', '#eee')
        .selectAll('td h6')
            .style({ color : '#666' , 'padding' : '5px 0 5px 5px'})

    rows.filter( f('clase').is('TT') )
        .style('background-color', '#ccc')
        .selectAll('td h6')
            .style({ color : '#666' , 'padding' : '5px 0 5px 5px'})


    rows.selectAll('.saldo-total h6').text(function (d) {
        if(d.id != 'UP' && d.depth < 2) return '';
        if(ctx.datos.cargando()) return '...';
        return oorden.org.format( d.saldoFinal || 0);
    });

    rows.selectAll('.saldo-periodo h6').text(function (d) {
        if(d.id != 'UP' && d.depth < 2) return '';
        if(ctx.datos.cargando()) return '...';
        return oorden.org.format( d.saldoPeriodo || 0);
    });


}
