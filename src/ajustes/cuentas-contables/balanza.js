var catalogo = require('./catalogo');
var SearchBar = require('../../misc/components/SearchBar');
var FechasBar = require('../../misc/components/FechasBar2');
var FiltrosReporte = require('../../misc/components/filtrosReporte');

module.exports = {
    controller : BalanzaController,
    view : BalanzaView
};


function BalanzaController () {
    catalogo.controller.call(this);
    var ctx = this;


    /**
     * Modificadores para la tabla
     */
    this.UpdateRow = UpdateRow;
    this.EnterRow  = EnterRow;
    this.afterDraw = afterDraw;


    /**
     * Saldos
     */
    this.saldos = m.prop()
    this.saldos.cargando = m.prop();
    this.saldos.key = m.prop();


    this.cargarCuenta = m.prop();

    this.mostrarFormulario = m.prop(false);

    this.mostrarBusqueda = m.prop(false);


    ctx.fechaDesde = m.prop();
    ctx.fechaHasta = m.prop();

    ctx.modificarFiltros = function (datos) {
        ctx.mostrarFormulario(true);
        //ctx.fechasBar.fechaHasta( ctx.fechaHasta() );
        //ctx.fechasBar.fechaDesde( ctx.fechaDesde() );
    }

    ctx.cargarSaldos = function () {
        if(!ctx.dCuentas()) return;

        //Genero la llave de la consulta
        var key = FiltrosReporte.getKey(['fechaDesde', 'fechaHasta'], ctx);
        if(ctx.saldos.cargando() || key === ctx.saldos.key()) {
            return
        }

        //Si la llave de la consulta es distinta entonces cambió
        ctx.saldos.cargando(true);
        ctx.saldos.key(key);

        //Faltaria guardar la consulta y actualizar la ruta

        oorden().then(function () {
            return oor.request('/cuentas-contables/datosBalanza', 'GET', {
                data : {
                    fechaDesde : ctx.fechaDesde(),
                    fechaHasta : ctx.fechaHasta()
                }
            });
        })
        .then(function (d) {
            var saldos = {};

            d.forEach(function (c) {
                saldos[c.ctav] =  {
                    inicial  : oorden.org.format(c.saldoInicial),
                    final  : oorden.org.format(c.saldoFinal),
                    debe   : oorden.org.format(c.debe),
                    haber  : oorden.org.format(c.haber),
                }
            });

            ctx.saldos(saldos);
            ctx.saldos.cargando(null);
            ctx.updateTable(true);
        });
    }

    FiltrosReporte.aplicar(
        ['fechaDesde', 'fechaHasta'],
        ctx,
        FiltrosReporte.queryParams(), // Parametros de la búsqueda
        FiltrosReporte.savedParams() // Parametros guardados
                                    //Faltarian los parametros default absolutos
    );

}



function BalanzaView (ctx) {
    ctx.cargarCuentas();
    ctx.cargarSaldos();
    var fFormat = oor.fecha.prettyFormat.year;
    var fechaDesde = ctx.fechaDesde() ? fFormat(ctx.fechaDesde()) : '';
    var fechaHasta = ctx.fechaHasta() ? fFormat(ctx.fechaHasta()) : '';


    return oor.panel({
        title:'Balanza',
        buttons : [
            m('.btn-group', [
                m('.btn btn-primary button-striped button-full-striped button-striped btn-default dropdown-toggle btn-ripple btn-xs', {'data-toggle':'dropdown'},
                    ['Exportar  ',m("span.caret"),{style:'width:30px'}]
                ),

                m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                    m("li", m('a',{href:'javascript:void(0);',id:'balanza-pdf'},  [
                            m('i.fa fa-file-pdf-o')," PDF"
                        ])
                    ),
                    m("li", m('a',{href:'javascript:void(0);',id:'balanza-csv'},  [
                            m('i.fa fa-file-excel-o')," Excel"
                        ])
                    )
                ])
            ])
        ]
    }, ctx.dCuentas.cargando() ? oor.loading() : [

        ctx.mostrarFormulario() ? m.component(FiltrosReporte, {
            close : ctx.mostrarFormulario.bind(null,false),
            aplicarA : ctx,
            value : ctx
        }) : null,

        m('br'),
        m('h4.pull-left', 'Balanza desde ', fechaDesde , ' hasta ', fechaHasta, ' ' ),

        ctx.mostrarFormulario() ? null : m('a.small.text-indigo.pull-right', {
            href:'javascript:;', onclick:ctx.modificarFiltros
        }, 'Modificar Filtros'),

        m('.row', [
            m('.col-sm-12', [
                m('table.cuentas-titles',  [
                    m('thead', [
                        m('tr', {style:'height:36px; background-color:#3F51B5;color:white'},[
                            m('td.cuenta', 'Cuenta Contable', ' ', m('a[href=javascript:;]', {onclick:ctx.mostrarBusqueda.bind(null, true), style:'color:white'}, m('i.fa.fa-search') ) ),
                            m('td.saldo-inicial', 'Inicial'),
                            m('td.saldo-debe', 'Debe'),
                            m('td.saldo-haber', 'Haber'),
                            m('td.saldo-final', 'Final')
                        ]),

                        ctx.mostrarBusqueda() ? m('tr', {style:'height:36px'},[
                            m('td.cuenta', {style:'padding:4px'}, SearchBar.view(ctx.searchBar)),
                            m('td.saldo-inicial', ''),
                            m('td.saldo-debe', ''),
                            m('td.saldo-haber', ''),
                            m('td.saldo-final', '')
                        ]) : null
                    ])
                ])
            ]),

            m('.col-sm-12', {style:'overflow-y:scroll; height:700px'}, [
                m('table.tabla-cuentas', {config : catalogo.D3Table(ctx), style:'width:100%'})
            ])
        ])
    ])
}

function afterDraw (selection) {
    var hTabla = d3.select('.cuentas-titles thead');

    ['cuenta', 'saldo-inicial', 'saldo-debe', 'saldo-final', 'saldo-haber'].forEach(function (k, i) {
        var node  = selection.select('tr.isCuenta td.'.concat(k)).node();
        var width = node ? node.offsetWidth : 120


        hTabla.select('td.'.concat(k))
            .classed({'text-right' : Boolean(i) })
            .style('max-width', String(width).concat('px'))
            .style('min-width', String(width).concat('px'))
            .style('padding', '0 12px')
    })

}



function EnterRow (selection) {
    var labels = {
        inicial : 'S. Inicial',
        final : 'S. Final',
        debe : 'Debe',
        haber : 'Haber'
    };


    selection.filter(function (d) { return d.depth < 3 })
        .select('td.cuenta')
        .attr('colspan', 5);

    ['inicial', 'debe', 'haber', 'final'].forEach(function (saldo) {
        selection.filter(function (d) { return d.depth > 2 })
            .append('td')
            .attr('class', 'text-right saldo-'.concat(saldo))
            .style('padding', '0 12px')
            .style('font-size', '14px')
            .style('font-family', 'monospace')
            .append('a')
    });
}

function getHref(ctx) {
    return function (d) {
        //if(d.tacum == 'A') return  undefined;
        return '/cuentas-contables/auxiliar'.concat('?cuenta=',d.id, '&fechaDesde=',ctx.fechaDesde(), '&fechaHasta=', ctx.fechaHasta());
    }
}

function getHtml(ctx, key) {
    var saldos = ctx.saldos();
    return function (d) {
        return saldos[d.id] ? saldos[d.id][key] : '0.00';
    }
}

function UpdateRow (selection, ctx) {
    var saldos = ctx.saldos();

    selection = selection.filter( function (d) { return d.depth > 0 })

    if(ctx.saldos.cargando() ) {
        selection.select('.saldo-final a').html('...');
        selection.select('.saldo-inicial a').html('...');
        selection.select('.saldo-debe a').html('...');
        selection.select('.saldo-haber a').html('...');

        return;
    }

    if(!saldos) return;

    selection.selectAll('a')
        .attr('target', '_blank')
        .classed({ 'text-indigo' : function (d) { return true } })
        .attr('href', getHref(ctx))
        

    selection.select('.saldo-final a').html( getHtml(ctx, 'final') );
    selection.select('.saldo-debe a').html( getHtml(ctx, 'debe') )
    selection.select('.saldo-haber a').html( getHtml(ctx, 'haber') );
    selection.select('.saldo-inicial a').html( getHtml(ctx, 'inicial') );
}
