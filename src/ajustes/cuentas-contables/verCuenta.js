module.exports = {
    controller : VerCuentaController,
    view : VerCuentaView
}

var EditableComponent = {
    controller : EdCompController,
    view : EdCompView
};


function createVM (ctx) {
    var cuenta = ctx.cuenta();
    var id = cuenta.cuenta_contable_id;

    return {
        applyCuenta : applier('cuenta'),
        applyNombre : applier('nombre'),
        applyEsBancos : applier('es_bancos'),
        applyPagoPermitido : applier('pago_permitido'),
        applyDescripcion : applier('descripcion'),
        applyCuentaBancaria : applier('cuenta_bancaria')
    };

    function applier (keyName) {
        return function (model) {
            var oldValue = cuenta[keyName];
            cuenta[keyName] = model();

            return oor.request('/apiv2/editar/'.concat(id, '?modelo=cuentas'), 'PUT', {data : cuenta})
                    .then(
                        function (d) { ctx.onsave(d); return true; },
                        function (r) { cuenta[keyName] = oldValue; toastr.error(r.status.message); return false;}
                    );
        }
    }
}


function VM (ctx) {
    if(!VM.map[ ctx.cuenta().cuenta_contable_id ]) {
        VM.map[ ctx.cuenta().cuenta_contable_id ] = createVM(ctx);
    }
    return VM.map[ ctx.cuenta().cuenta_contable_id ];
}

VM.map = {};



function VerCuentaController (params) {
    var ctx = this;

    ctx.cuenta = m.prop(params.cuenta)
    ctx.cuenta.cargando = m.prop(false);
    ctx.tipos = params.tipos;
    ctx.onsave = params.onsave;

    ctx.cargarCuenta = function () {
        if(ctx.cuenta() || ctx.cuenta.cargando()) return false;
        ctx.cuenta.cargando(true);

        oor.request('/apiv2', 'GET', {
            data : {modelo:'cuentas', cuenta_contable_id : params.cuenta_id}
        })
        .then(f(0))
        .then(ctx.cuenta)
        .then(ctx.cuenta.cargando.bind(null,false))
    }
}

function NaturalezaLabel (n) {
    return n == 'D' ? 'Deudora' : 'Acreedora';
}




function VerCuentaView (ctx, params) {
    ctx.cargarCuenta();

    var tipo          = ctx.cuenta().tipo_id ? ctx.tipos[ctx.cuenta().tipo_id] : null;
    var nombreTipo    = tipo ? tipo.nombre : '(inválido)';
    var codigoOficial = ctx.cuenta().cuenta.split('-').slice(0,2).join('.');
    var vm = VM(ctx)

    return m('div', [

        m.component(EditableComponent, {
            initializeEdit : function (model) {
                model(ctx.cuenta().cuenta)
            },
            apply : vm.applyCuenta,
        }, [
            m('h4.pull-left', {style:'margin-bottom:0px'}, ctx.cuenta().cuenta)
        ]),

        m('.clear'),

        m.component(EditableComponent, {
            initializeEdit : function (model) {
                model(ctx.cuenta().nombre)
            },
            apply : vm.applyNombre
        }, [
            m('h5.pull-left',  {style:'margin-bottom:10px; margin-top:0px'}, ctx.cuenta().nombre),
        ]),

        m('.clear'),


        m('table.table-bordered.table.tabla-cuenta.megatable',{style:'margin-bottom:10px; margin-top:10px'}, [
            ctx.cuenta().subcuentaDe  ? m('tr', [
                m('td.text-right.text-grey.small.cells-title', 'Subcuenta de: '),
                m('td[colspan=3].text-indigo', ctx.cuenta().subcuentaDe.cuenta, ' - ', ctx.cuenta().subcuentaDe.nombre)
            ]) : null,
            m('tr', [
                m('td.text-right.text-grey.small.cells-title', 'Tipo:'),
                m('td[colspan=3]', nombreTipo),
            ]),

            m('tr', [
                m('td.text-right.text-grey.small.cells-title', 'Naturaleza:'),
                m('td[colspan=3]',[
                    NaturalezaLabel(ctx.cuenta().naturaleza)
                ])
            ]),

            m('tr', {style:'min-height:3em'}, [
                m('td.text-right.text-grey.small.cells-title', 'Descripción:'),
                m('td[colspan="3"]',
                    m.component(EditableComponent, {
                        initializeEdit : function (model) {
                            console.log(ctx.cuenta(), ctx.cuenta().descripcion)
                            model(ctx.cuenta().descripcion);
                            console.log( model() )
                        },
                        apply : vm.applyDescripcion,
                        textarea : true
                    }, [
                        ctx.cuenta().descripcion || m('.text-grey.small', '(agregar descripción)')
                    ])
                )
            ]),


            m('tr', {style:'background-color:#f6f6f9 !important'}, [
                m('td.text-right.text-grey.small.cells-title', '¿Es Banco o caja?:'),
                m('td',
                    m.component(EditableComponent, {
                        initializeEdit : function (model) {
                            model(ctx.cuenta().es_bancos)
                        },
                        apply : vm.applyEsBancos,
                        options : [
                            {caption:'Sí', value:'1'},
                            {caption:'No', value:'0'}
                        ]
                    }, [
                        ctx.cuenta().es_bancos == 1 ? 'Sí' : 'No'
                    ])
                ),

                m('td.text-right.text-grey.small.cells-title', ctx.cuenta().es_bancos == 1 ? 'Banco' : ''),
                m('td', ctx.cuenta().es_bancos == 1 ? ctx.cuenta().banco : ''),
            ]),


            ctx.cuenta().es_bancos == 1 ? [
                m('tr', {style:'background-color:#f6f6f9 !important'}, [
                    m('td.text-right.text-grey.small.cells-title', 'Num Cta Bancaria'),
                    m('td',
                        m.component(EditableComponent, {
                            initializeEdit : function (model) {
                                model(ctx.cuenta().cuenta_bancaria  || '')
                            },
                            apply : vm.applyCuentaBancaria,
                        }, [
                            ctx.cuenta().cuenta_bancaria ||  m('.text-grey.small', '(agregar num de cta)')
                        ])
                    ),
                    m('td.text-right.text-grey.small.cells-title', '# Cheque'),
                    m('td', ctx.cuenta().numero_cheque || '--')
                ])
            ]: null,


            m('tr', [
                m('td.text-right.text-grey.small.cells-title', '¿Permite Pagos?:'),
                m('td',
                    m.component(EditableComponent, {
                        initializeEdit : function (model) {
                            model(ctx.cuenta().pago_permitido)
                        },
                        apply : vm.applyPagoPermitido,
                        options : [
                            {caption:'Sí', value:'1'},
                            {caption:'No', value:'0'}
                        ]
                    }, [
                        ctx.cuenta().pago_permitido == 1 ? 'Sí' : 'No'
                    ])
                ),
                m('td.text-right.text-grey.small.cells-title', 'Código Oficial:'),
                m('td', codigoOficial)
            ]),

        ])
    ]);
}



function EdCompController (params) {
    var ctx = this;

    ctx.model = m.prop();
    ctx.estatus = m.prop('show');

    ctx.initializeEdit = function () {
        params.initializeEdit.call(this,ctx.model);
        ctx.estatus('edit');
    }

    ctx.submit = function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        ctx.apply();
    }

    ctx.cancelEdit = function () {
        ctx.estatus('show')
    }

    ctx.apply = function () {
        var estatus = params.apply.call(this, ctx.model);
        if(estatus && estatus.then) {
            estatus.then(end);
        } else {
            end(estatus);
        }

    }
    function end (estatus) {
        if(estatus) ctx.estatus('show');
    }
}

function ShowInput (ctx, params) {
    if(params.options) {
        return  [
            m('select', {
                value:ctx.model(),
                onchange: m.withAttr('value', function (val) {
                    ctx.model( val );
                    ctx.apply();
                })
            }, [
                params.options.map(function (d) {
                    return m('option', {value : d.value}, d.caption)
                })
            ]),
            m('a.btn.btn-xs.btn-default', {onclick : ctx.cancelEdit}, m.trust('&times;'))
        ]
    }

    return  [
        params.textarea ? m('textarea', {
            onchange : m.withAttr('value', ctx.model),
            config   : function (el,isInit) { if(!isInit) el.focus() },
            style : 'width:100%'
        }, ctx.model()) : m('input[type=text]', {
            size : ctx.model().length ? Math.min(ctx.model().length + 2, 40) : 5,
            value    : ctx.model(),
            oninput : m.withAttr('value',ctx.model),
            config   : function (el,isInit) { if(!isInit) el.focus() }
        }),
        m('button.btn.btn-xs.btn-primary', {type:'submit'}, m('i.fa.fa-floppy-o')),
        m('a.btn.btn-xs.btn-default', {onclick : ctx.cancelEdit}, m.trust('&times;'))
    ]
}

function EdCompView (ctx, params, show) {
    return m('div.ed-comp', [
        ctx.estatus() == 'show' ? m('.show', { onclick : ctx.initializeEdit }, show) : null,
        ctx.estatus() == 'edit' ? m('form.edit' , {onsubmit : ctx.submit}, ShowInput(ctx, params)) : null
    ]);
}
