var FechasBar2 = require('../../misc/components/FechasBar2');
var CuentaSelector = require('../../misc/selectors/CuentaSelector');

module.exports = {
    view : AuxiliarView,
    controller : AuxiliarController
}

function AuxiliarController () {
    oor.fondoGris();

    var ctx = this;
    var init = false;

    this.key = m.prop();
    this.updateTable = m.prop(false);
    this.numCuenta  = m.prop('');
    this.cuenta_id = m.prop();
    this.fechaDesde = m.prop('');
    this.fechaHasta = m.prop('');
    this.cargando   = m.prop(false);
    this.cuenta     = m.prop();
    this.data = m.prop()

    this.fechasBar = new FechasBar2.controller({});

    var tree = d3.layout.tree().children(function (d) {
        return ctx.childrenMap[d.id];
    });

    this.initialize = function () {
        if(ctx.cargando()) return;

        var nKey = location.pathname.split('/cuentas-contables/balanceGeneral/').join('');
        var fecha = nKey;

        if(this.key() == nKey) return
        this.key(nKey);

        ctx.cargando(true);

        oor.request('/datos-reportes/balanceGeneral/'.concat('fecha/', this.key()), 'GET')
            .then(asignar)
            .then(oorden)
            .then(ctx.cargando.bind(null))
    }

    var parse = {
        root : _.identity,
        titulos : function (d) {
            d.id = d.tipo_id;
            d.$titulo = true;
            return d;
        },
        tipos : function (d) {
            d.id = d.tipo_id;
            d.$tipo = true;
            d.nombre = '- '.concat(d.nombre);
            return d;
        },
        cuentas : function (d) {
            d.id = d.cuenta;
            d.$cuenta = true;
            d.nombreCompleto = d.cuenta + ' ' + d.nombre
            return d;
        }
    }


    function asignar (datos) {
        ctx.childrenMap = {};

        Object.keys(datos).forEach(function (key) {
            var parser = parse[key];

            if(!_.isFunction(parser)){
                console.error('No parser fn:' + key)
            }

            if(key === 'root') {
                ctx.root = parser( datos[key] );
                return;
            }

            Object.keys(datos[key]).forEach(function (k) {
                ctx.childrenMap[k] = [];
                datos[key][k].forEach(function (dato) {
                    ctx.childrenMap[k].push( parser(dato) );
                });
            });
        });


        ctx.data(tree.nodes(ctx.root));
        ctx.updateTable(true);
    }

    /*
    function asignar (data) {
        ctx.cuenta(data.cuenta);
        ctx.cuenta_id(data.cuenta.cuenta_contable_id);

        ctx.cuentasMap = data.cuentas;
        ctx.partidasMap = {};

        Object.keys(data.partidas).forEach(function (k) {
            data.partidas[k].map(function (p) { return p.partida = true });
            ctx.partidasMap[k] = data.partidas[k];
        });

        var nodes = tree.nodes( ctx.cuenta() );

        ctx.data(nodes);
        ctx.updateTable(true)

    }

    this.onchange = function (prop) {
        return function (value) {
            prop(value);
            actualizarRuta();
        }
    }

    this.asignarParametros = function (numCta, fechaDesde, fechaHasta) {
        var keyData = this.key().split('/');
        var numCta      = keyData[0];
        var fechaDesde  = keyData[2];
        var fechaHasta  = keyData[3];
        this.numCuenta(numCta);
        this.fechaDesde(fechaDesde);
        this.fechaHasta(fechaHasta);
    }

    function actualizarRuta () {
        var ruta = [ctx.numCuenta(), 'fechas', ctx.fechaDesde(), ctx.fechaHasta()].join('/');
        var uri = '/cuentas-contables/auxiliar/' + ruta;
        history.pushState({}, ruta, uri);
    }
    */
}

function AuxiliarView (ctx) {
    ctx.initialize();
    return oor.panel({
        title : 'Auxiliar Contable'
    }, [
        /*
        m.component(CuentaSelector, {
            cuenta   :  ctx.cuenta(),
            model    :  ctx.cuenta_id,
            seleccionar : '*',
            onchange : function (idCuenta,sCuenta) {
                ctx.onchange(ctx.numCuenta)(sCuenta.cuenta);
            }
        }),

        FechasBar2.view(ctx.fechasBar),
        */

        ctx.cargando() ? oor.loading() : [
            [
                m('h5', {style:'margin-bottom:0px'}, [
                    'Balance General '
                ]),

            ],
            ctx.data() ? m('table.tabla-cuentas.tabla-auxiliar', {style:'width:100%;', config : ConfigTable(ctx) }) : null
        ]

    ])
}


function ConfigTable (ctx) {
    return function (el, isInit) {
        if(ctx.updateTable()) {
            d3.select(el).call(Table, ctx.data().filter(function (d) { return d.depth > 0}) );
        }
    }
}

function Table (selection, data) {
    selection.html('');

    if( selection.select('tbody').empty() )  {
        selection.append('tbody')
    }

    selection = selection.append('tbody');

    var rows = selection.selectAll('tr').data(data);
    var enterRow = rows.enter().append('tr');


    var titulos = enterRow.filter( f('$titulo') );
    titulos.append('td').attr('class', 'rNombre').append('h5').text(f('nombre'))
    titulos.append('td').attr('class','rSaldo text-right').append('h5').text( function (d) { return oorden.org.format(d.sInicial) } )
    titulos.style('background-color', '#e0e0e0')

    var tipos = enterRow.filter( f('$tipo') )
    tipos.append('td').attr('class', 'rNombre').append('h5').text( f('nombre') )
    tipos.append('td').attr('class','rSaldo text-right').append('h5').text( function (d) { return oorden.org.format(d.sInicial) } )
    tipos.style('background-color', '#f6f6f6')


    var cuentas = enterRow.filter( f('$cuenta') ).filter( function (d) { return Number(d.sInicial)  } );
    cuentas.append('td').attr('class', 'rNombre').append('h6').text( f('nombreCompleto') )
    cuentas.append('td').attr('class','rSaldo text-right').append('h6').text( function (d) { return oorden.org.format(d.sInicial) } )

    rows.select('.rNombre').style('padding-left', function (d) { return String(10 + (d.depth - 1) * 20).concat('px') })
}
