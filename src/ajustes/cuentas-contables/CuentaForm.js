module.exports = {
    controller : CuentaFormController,
    view : CuentaFormView,
    openModal : openModal
}

var Cuenta = require('./CuentaModel');
var CuentaViewModel = require('./CuentaViewModel');

function CuentaFormController (params) {
    var ctx = this;

    ctx.cuenta = m.prop();
    ctx.guardar = guardar;

    ctx.vm = new CuentaViewModel({
        onsave : afterSave,
        onsaveError : function (errors) {
            toastr.error(errors[0].message);
        }
    });

    ctx.vm.cuenta = ctx.cuenta;

    ctx.impuestosSelector = oor.impuestosSelect2({
        impuestos : oorden.impuestos(),
        model : function () {
            if(arguments.length) {
                ctx.cuenta().impuesto_conjunto_id(arguments[0]);
            }
            return ctx.cuenta().impuesto_conjunto_id();
        }
    });


    ctx.initializeMask = function (element, isInited) {
        if(isInited) return;
        var formatoCuentas = f('formato_cuentas')( oorden.organizacion() );
        formatoCuentas = formatoCuentas.replace(/\d/g, '*');
        $(element).mask(formatoCuentas);

    }

    function guardar () {
        ctx.vm.guardar()
    }

    function afterSave (cuenta) {
        ctx.$modal && ctx.$modal.close();
        nh.isFunction(params.onsave) && params.onsave(cuenta);
    }


    ctx.cargarCuenta = function (argument) {
        Cuenta.obtenerTipos()
            .then(oorden.paisesYMonedas)
            .then(selectorMoneda)
            .then(cargarCuenta);
    }



    function selectorMoneda () {
        ctx.selectorMoneda = oor.select2({
            data : oorden.monedasDisponibles(),
            model : function () {
                if(arguments.length) {
                    ctx.cuenta().moneda( arguments[0] )
                }
                return ctx.cuenta().moneda();
            }
        })
    }

    function cargarCuenta () {
        if( ctx.cuenta() ) return;

        if(!params.cuenta_contable_id) {
            ctx.cuenta( new Cuenta );
            ctx.cuenta().$isNew(true);
            return;
        }

        return m.request({
            method : 'GET',
            url : '/apiv2',
            background : true,
            data : {
                modelo : 'cuentas',
                cuenta_contable_id : params.cuenta_contable_id
            },
            unwrapSuccess : function (r) {
                return new Cuenta(r.data[0]);
            }
        })
        .then(function (data) {
            ctx.cuenta(data);
        })
        .then(m.redraw);
    }
}

function CuentaFormView (ctx) {
    ctx.cargarCuenta();

    return m('div', [
        ctx.cuenta() ? CuentaFormulario(ctx) : oor.loading()
    ]);
}

function radioer (label, id, fn) {
    return [
        m('div', label),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'si' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(checked)
                }),
                checked : fn()
            }),
            m('label', {'for':id + 'si'}, 'Sí')
        ]),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'no' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(!checked)
                }),
                checked : !fn()
            }),
            m('label', {'for': id + 'no'}, 'No')
        ])
    ];
}

/**
 * El formulario de la cuenta
 */
function CuentaFormulario (ctx) {
    var cuenta = ctx.cuenta();
    var ctaPadre = ctx.vm.cuentaPadre();

    return [

        m('.row', [
            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'No. de cuenta'),
                    m('input[type=text]', {
                        config : ctx.initializeMask,
                        placeholder :'Número de Cuenta',
                        value : cuenta.cuenta(),
                        onchange : m.withAttr('value', function (val) {
                            cuenta.cuenta(val);
                            ctx.vm.buscarCuentaPadre()
                        })
                    })
                ])
            ]),

            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'Nombre'),
                    m('input[type=text]', {
                        value : cuenta.nombre(),
                        placeholder : 'Nombre de la cuenta',
                        onchange : m.withAttr('value', cuenta.nombre)
                    })
                ])
            ])
        ]),

        m('.row', [
            m('.col-sm-12', [
                ctaPadre ? m('div', [
                    'Subcuenta de: ',
                    f('nombre')(ctaPadre),
                    '(' + f('cuenta')(ctaPadre) + ')'
                ]) : null
            ])
        ]),

        m('.row', [
            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'Tipo'),
                    SelectTipo(cuenta.tipo_id)
                ])
            ])
        ]),

        m('.row', [
            m('.col-sm-12', [
                m('.mt-inputer.line', [
                    m('label', 'Descripción'),
                    m('textarea', {
                        placeholder : 'Descripción',
                        onchange : m.withAttr('value', cuenta.descripcion)
                    },cuenta.descripcion())
                ])

            ])
        ]),

        m('br'),

        m('.row', [


            m('.col-sm-6', [
                radioer('¿Es banco o Caja?', 'es-banco-o-caja', cuenta.es_bancos),
            ], cuenta.es_bancos() ? [

                m('.mt-inputer.line', [
                    m('label', 'Banco'),
                    m('input[type=text]', {
                        value : cuenta.banco(),
                        onchange : m.withAttr('value', cuenta.banco)
                    })
                ]),

                m('.flex-row.flex-end', [

                    m('.mt-inputer.line', {style : 'flex:2 2'}, [
                        m('label', 'Cuenta Bancaria'),
                        m('input[type=text]', {
                            size : 16,
                            value : cuenta.cuenta_bancaria(),
                            onchange : m.withAttr('value', cuenta.cuenta_bancaria)
                        })
                    ]),

                    m('.mt-inputer.line', {style:'flex:1 2'}, [
                        m('label', '# Cheque'),
                        m('input[type=text]', {
                            size: 5,
                            value : cuenta.numero_cheque(),
                            onchange : m.withAttr('value', cuenta.numero_cheque)
                        })
                    ])
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Moneda'),
                    ctx.selectorMoneda.view()
                ])

            ] : null),

            m('.col-sm-6', [
                m('div', {style:'margin:10px'},[
                    radioer('¿Pagos Permitidos?', 'pagos-permitidos', cuenta.pago_permitido)
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Código Agrupador SAT'),
                    m('input[type=text]', {
                        value : cuenta.codigo_oficial(),
                        readonly : 'readonly'
                    })
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Conjunto de Impuestos'),
                    ctx.impuestosSelector.view()
                ])
            ])
        ])

    ]
}

function SelectNaturaleza (fn) {
    return m('select', {
        value : fn(),
        onchange : m.withAttr('value', fn)
    }, [
        m('option[value=A]', 'Acredora'),
        m('option[value=D]', 'Deudora')
    ])
}


function SelectTipo (fn) {
    var tipos = Cuenta.tipos();

    return m('select', {
        value : fn(),
        onchange : m.withAttr('value', fn)
    }, [
        Cuenta.tiposAgrupados().map(function (group) {
            return m('optgroup', {label : group.nombre}, [
                group.children.map(function (tipo) {
                    return m('option', {value : tipo.tipo_id}, tipo.nombre);
                })
            ]);
        })
    ])
}

function openModal (settings) {
     MTModal.open({
        controller :CuentaFormController,
        content : CuentaFormView,
        args : settings,
        top : function () {
            return m('h4', settings.nombre )
        },
        bottom : function (ctx) {
            return [
                m('button.pull-left.btn-flat.btn-sm.btn-default', {
                    onclick : ctx.$modal.close
                }, 'Cancelar'),

                m('button.pull-right.btn-flat.btn-sm.btn-success', {
                    onclick : ctx.guardar
                }, 'Guardar')
            ];
        },
        el : this,
        modalAttrs : {
            'class' : 'modal-medium'
        }
    });

}
