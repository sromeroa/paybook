var Cuenta = require('./CuentaModel');
var CuentaViewModel = require('./CuentaViewModel');
var ccTable = require('./TablaCuentas');
var CuentaForm = require('./CuentaForm');
var SearchBar = require('../../misc/components/SearchBar');
var catalogo = require('./catalogo');
var balanza = require('./balanza');
var estadoResultados = require('./estadoResultados');
var auxiliar = require('./auxiliar');
var balanceGeneral = require('./balanceGeneral');

var tabs = m.prop([
    {caption:'Árbol', name:'arbol'},
    {caption:'Ocultas', name:'ocultas'},
    {caption:'Todas', name:'todas'}
]);


var menuActions = {};

menuActions.ocultar = function (ctx, message) {
    var url = '/cuentas-contables/archivar/' + message.id;
    m.request({
        method : 'POST',
        url : url
    }).then(function (d) {
        toastr.success('Cuenta Archivada')
        ctx.obtenerCuentas()
    });
};


menuActions.eliminar = function (ctx, message) {
    var url = '/cuentas-contables/eliminar/' + message.id;

    m.request({
        method : 'POST',
        url : url
    }).then(function (d) {
        toastr.success('¡Cuenta Eliminada!')
        ctx.obtenerCuentas()
    });
};



menuActions.agregarSubcuenta = function (ctx, message) {
    console.log('agregarSubcuenta')
};



module.exports = {
    balanza : balanza,
    balanceGeneral : balanceGeneral,
    catalogo : catalogo,
    estadoResultados : estadoResultados,
    auxiliar : auxiliar,
    controller : ccController,
    view : function (ctx) {
        return m('div', [
            m('a.pull-right.btn.btn-flat.btn-success', {
                onclick : ctx.agregarCuenta
            }, '+'),
            m('h3.column-title', 'Cuentas Contables'),
            ccView(ctx),
            MTModal.view()
        ]);
    }
};

function ccController () {
    var ctx = this;


    ctx.ready = m.prop(false);
    ctx.cuentas = m.prop();
    ctx.byParentId = m.prop({});
    ctx.opened = m.prop({});
    ctx.nodes = m.prop();

    ctx.saldos = Saldos(ctx.cuentas);
    ctx.saldos.ready = (function (originalReady) {
        ready.cargando = originalReady.cargando;

        return ready;

        function ready () {


            if(arguments.length) {
                originalReady(arguments[0])
                table.redraw()
            }

            return originalReady();
        }
    })(ctx.saldos.ready)



    ctx.tab = m.prop( tabs()[0] );

    ctx.vm = new CuentaViewModel({
        oncancel : function () {
            ctx.nodes().shift();
            ctx.nuevaCuenta = null;
            table.redraw();
        },
        onsave : afterSave,
        onsaveError : function (e) {
            console.log(e)
        }
    });

    ctx.obtenerCuentas = obtenerCuentas;

    ctx.search = m.prop('')

    ctx.esArbol = m.prop(true)
    ctx.mostrarOcultas = m.prop(false)
    ctx.updateNodes = updateNodes

    ctx.searchBar = new SearchBar.controller({
        placeholder : 'Buscar por nombre o número de cuenta',
        search : ctx.search,
        onsearch : updateNodes
    });

    function updateNodes () {
        var search = ctx.search() || '';
        search = search.toLowerCase().latinize();
        var nodes;

        if(search.length == 0) {
            ctx.esArbol(true);

            nodes = ctx.cuentas()
                        .filter(ocultasFilter)
                        .filter(arbolFilter);

        } else if ( search.length < 3) {
            nodes = []
        } else {
            nodes = ctx.cuentas()
                        .filter(ocultasFilter)
                        .filter(function (d) { return d.search.indexOf(search) + 1 });

            ctx.esArbol(false)
        }

        ctx.nodes(nodes);
        m.redraw()
        table.redraw();
    }

    function arbolFilter(d) {
        return (!d.subcuenta_de) || ctx.opened()[d.subcuenta_de];
    }

    function ocultasFilter (d) {
        return d.estatus == 1 || (ctx.mostrarOcultas() && d.estatus == 2)
    }

    function buscarCuentas (search) {
        var results = [];
        return results;
    }

    var hierarchy = d3.layout.hierarchy();
    var table = ccTable(ctx);

    table.mounted = m.prop(false);
    table.element = m.prop();
    table.redraw = function () {
        d3.select(table.element()).call(table, table.columns, ctx.nodes());
    }

    hierarchy.children(function (d) {
        var idCta =  d.cuenta_contable_id ? d.cuenta_contable_id : '_';
        return ctx.byParentId()[idCta];
    });

    var tabActions = {};
    tabActions.arbol = obtenerCuentas;
    tabActions.ocultas = obtenerCuentas;
    tabActions.todas = obtenerCuentas;

    ctx.intialize = function () {
        if(ctx.cuentas()) return;

        ctx.selectTab(ctx.tab(), true);
        ctx.ready(false);
    }


    ctx.expandirTodas = function () {
        ctx.cuentas()
            .forEach(function (node) {
                ctx.openNode(node.cuenta_contable_id,false)
            })

        recalculateNodes();
    }


     ctx.colapsarTodas = function () {
        ctx.cuentas()
            .forEach(function (node) {
                ctx.closeNode(node.cuenta_contable_id,false)
            })

        recalculateNodes();
    }


    ctx.agregarCuenta = function () {
        if(ctx.nuevaCuenta) return;

        ctx.nuevaCuenta = new Cuenta({
            nombre : '',
            cuenta_contable_id : oor.uuid(),
            tipo_id : Object.keys(table.TIPOS)[0]
        });

        ctx.nuevaCuenta.$isNew(true);
        ctx.nuevaCuenta.$edit = m.prop(true);

        ctx.nodes().unshift(ctx.nuevaCuenta);

        ctx.vm.cuenta(ctx.nuevaCuenta);

        table.redraw();
    }

    ctx.selectTab  = function (tab, force) {
        if(force !== true && tab == ctx.tab()) return;
        ctx.ready(false);
        ctx.tab(tab);
        tabActions[tab.name]();
    }

    ctx.openNode = function (idNode, recalculate) {
        ctx.opened()[idNode] = true;
        if(recalculate === false) return;
        recalculateNodes();
    }


    ctx.closeNode = function (idNode, recalculate) {
        ctx.opened()[idNode] = false;
        if(recalculate === false) return;
        recalculateNodes();recalculateNodes();
    }


    ctx.setupTable = function (element, isInited) {
        if(table.mounted()) {
            return
        }

        if(ctx.nodes()) {
            table.element(element);
            table.redraw();
            table.mounted(true)
        }

        if(isInited) return;

        $(element).on('click', '[x-cell=info]', function (ev) {
            var elem = $(this);
            var action = elem.attr('x-action');
            var parent = elem.parent();
            var id = parent.attr('x-row') || parent.parent().attr('x-row');

            if(action == 'open') {
                ctx.openNode( id )
            } else if(action == 'close') {
                ctx.closeNode( id );
            }
        });

        $(element).on('click', '[x-action]', function (ev) {
            var elem = $(this);
            var xRow = elem.parentsUntil('tbody', '[x-row]');
            var idRow = xRow.attr('x-row');
            var message = {action : elem.attr('x-action'), id:idRow};

            if(elem.parent().hasClass('disabled')){
                console.log('disabled');
                return;
            }

            var action = menuActions[message.action];
            if(nh.isFunction(action)) {
                action(ctx, message);
            }
        });

        $(element).on('click', 'a.celda-nombre' , function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            var anchor = $(this)
            var id = anchor.attr('href').split('/cuentas/editar/').join('');

            CuentaForm.openModal.call(this, {
                onsave : afterSave,
                cuenta_contable_id : id,
                nombre : anchor.text()
            });

            m.redraw();
        })

        ctx.vm.bridge(element);
    }




    function obtenerCuentas (settings) {
        return oorden()
            .then(Cuenta.obtenerTipos)
            .then(function (tipos) {
                //Asignar los tipos de cuentas
                table.TIPOS = Cuenta.tipos();
            })
            .then(function () {
                return m.request({
                    method : 'GET',
                    url : '/cuentas-contables/cuentas/' + ctx.tab().name,
                }).then(asignarCuentasYArbol);
            });
    }


    function asignarCuentasYArbol (d) {
        var cuentas, byParentId = {};
        var mostrarCuenta;

        cuentas = d.data.filter(function (cta) {
            return table.TIPOS[cta.tipo_id];
        });

        cuentas = cuentas.sort(function (c1, c2) {
                return f('cuenta')(c2) > f('cuenta')(c1) ? -1 : 1
            })


        ctx.cuentas(cuentas);


        if(ctx.mostrarCuenta) {
            abrirRutaACuenta(ctx.mostrarCuenta)
            ctx.mostrarCuenta = false;
        }

        ctx.cuentas().forEach(function (d) {
            var subcuentaDe = d.subcuenta_de ? d.subcuenta_de : '_';
            if(!byParentId[subcuentaDe]) {
                byParentId[subcuentaDe] = [];
            }

            d.search = String(f('cuenta')(d) + ' ' + f('nombre')(d)).toLowerCase().latinize();
            byParentId[subcuentaDe].push(d)
        });

        ctx.byParentId(byParentId);

        recalculateNodes();

        ctx.ready(true);
    }


    function abrirRutaACuenta(idCuenta) {
        var cuenta = ctx.cuentas().filter(function (cta) {
            return f('cuenta_contable_id')(cta) == idCuenta;
        })[0];

        var subcuenta_de = f('subcuenta_de')(cuenta);

        if(subcuenta_de) {
            ctx.openNode(subcuenta_de, false);
            abrirRutaACuenta(subcuenta_de);
        }
    }

    function recalculateNodes (settings) {
        var nodes = hierarchy({
                cuenta_contable_id : null
            }).filter(function (n) {
                return n.cuenta_contable_id;
            }).sort(function (c1, c2) {
                return f('cuenta')(c2) > f('cuenta')(c1) ? -1 : 1
            }).filter(arbolFilter)




        if(ctx.nuevaCuenta) {
            ctx.nodes().unshift(ctx.nuevaCuenta);
        }

        //table.redraw();

        updateNodes();
    }


    function openRouteToNode(node) {
        var parent = node;
        for(var i = 0; i<100; i++) {
            console.log(parent);
            if(!parent) break;

            ctx.opened()[ f('cuenta_contable_id')(parent) ] = true;
            parent = node.parent;
        }
    }



    function afterSave (cuenta) {
        var selector;

        ctx.nuevaCuenta = null
        selector = '[x-row="' + Cuenta.id(cuenta) + '"] td'
        ctx.mostrarCuenta = f('cuenta_contable_id')(cuenta);

        obtenerCuentas()
            .then(oor.performHighlight(selector, '#79e078'))
    }



}


function Saldos (cuentas) {
    var saldos = {};
    var now = new Date();

    saldos.ready = m.prop(false);
    saldos.ready.cargando = m.prop(false);
    saldos.saldos = m.prop()
    saldos.mes_contable = m.prop(now.getMonth() + 1)
    saldos.ano_contable = m.prop(now.getFullYear());

    saldos.periodo = m.prop(true);
    saldos.fechaInicial = m.prop( oor.fecha.toSQL(now) );
    saldos.fechaFinal = m.prop( oor.fecha.toSQL(now) )

    saldos.esteMes = function () {
        var inicial = oor.fecha.inicialMes(now)
        var final = oor.fecha.finalMes(now)

        saldos.periodo(false)
        saldos.fechaInicial( oor.fecha.toSQL(inicial) )
        saldos.fechaFinal( oor.fecha.toSQL(final) )
        saldos.actualizar();
    }

    saldos.esteAno = function () {
        var inicial = oor.fecha.inicialAno(now)
        var final = oor.fecha.finalAno(now)

        saldos.periodo(false)
        saldos.fechaInicial( oor.fecha.toSQL(inicial) )
        saldos.fechaFinal( oor.fecha.toSQL(final) )
        saldos.actualizar();
    }

    saldos.periodoFiscal = function () {
        saldos.periodo(true)
        saldos.mes_contable = m.prop(now.getMonth() + 1)
        saldos.ano_contable = m.prop(now.getFullYear());
        saldos.actualizar();
    }

    saldos.actualizar = function () {
        saldos.ready(0);
        saldos.ready.cargando(0);
    }

    saldos.get = function (id) {
        var lSaldos = saldos.saldos();

        if(!lSaldos[id]) {
           lSaldos[id] = calcularSaldos(id);
        }

        return lSaldos[id];
    }

    saldos.setMes = function (mes) {
        var mes, ano = saldos.ano_contable();
        if(mes <= 0) {
            mes = 12;
            ano--;
        }
        else if(mes >= 15) {
            mes = 1;
            ano++;
        }

        saldos.mes_contable(mes);
        saldos.ano_contable(ano);
    }

    saldos.getter = function (key) {
        return function (cta) {
            return saldos.obtener(cta.cuenta_contable_id, key);
        }
    }


    function calcularSaldos (id) {
        var lCuentas = cuentas().filter(f('subcuenta_de').is(id))
                            .map(f('cuenta_contable_id'))
                            .map(saldos.get);



        var calcSaldo = {
            cuenta_contable_id : id,
            saldo_inicial : 0,
            saldo_final : 0,
            debe : 0,
            haber : 0
        }


        lCuentas.forEach(function (lSaldo) {
            calcSaldo.saldo_inicial += lSaldo.saldo_inicial || 0;
            calcSaldo.saldo_final += lSaldo.saldo_final || 0;
            calcSaldo.debe += lSaldo.debe || 0;
            calcSaldo.haber += lSaldo.haber || 0;
        });

        return calcSaldo;
    }


    saldos.obtener = function (idCuenta, key) {
        if(saldos.ready() == false) return;
        var s = saldos.get(idCuenta);
        return s[key];
    }

    saldos.formato = function (formatFn) {
        return function (s) {
            if(saldos.ready() == false) return '...';
            return formatFn(s);
        }
    }

    saldos.search = function () {

        var dataR;

        if(saldos.periodo()){
            dataR = {mes_contable : saldos.mes_contable(), ano_contable : saldos.ano_contable()};
        } else {
            dataR = {fecha_inicial : saldos.fechaInicial(), fecha_final: saldos.fechaFinal()};
        }

        return m.route.buildQueryString(dataR);
    }

    saldos.cargar = function () {
        if(saldos.ready() || saldos.ready.cargando()) return;
        saldos.ready.cargando(true);

        var dataR;

        if(saldos.periodo()){
            dataR = {mes_contable : saldos.mes_contable(), ano_contable : saldos.ano_contable()};
        } else {
            dataR = {fecha_inicial : saldos.fechaInicial(), fecha_final: saldos.fechaFinal()};
        }

        oor.request('/reportes/balanzadatos', 'GET', {data:dataR, unwrapSuccess:f('cuentas')})
            .then(function (r) {

                var rSaldos = r.map(function (ss) {
                        if(ss.acumulativa == 1) return false;

                        if(ss.saldos) {
                            ss.saldos.cuenta_contable_id = ss.cuenta_contable_id;
                        }

                        return ss.saldos;
                    })
                    .filter(function (s) {
                        return Boolean(s)
                    })
                    .reduce(function (saldos,saldo) {
                        saldos[saldo.cuenta_contable_id] = saldo;
                        return saldos;
                    }, {})


                console.log(rSaldos);
                saldos.saldos(rSaldos);
                saldos.ready(true);

            });
    }




    return saldos;
}


/**
 * Vista de Cuentas Contables
 */
function ccView (ctx) {
    ctx.intialize();
    var search = ctx.search();

    ctx.ready() && ctx.saldos.cargar();


    return m('div', [
        m('.row', [
            m('.col-sm-4', [
                m('div', [
                    SearchBar.view(ctx.searchBar),
                    (search && search.length < 3) ? m('small.text-grey', 'Escriba al menos 3 caracteres para buscar') : ''
                ])
            ]),

            m('.col-sm-4', [

            ])
        ]),

        m('br'),

        m('.row', [
            m('.col-sm-8', [
                m('h6', 'Mostrar Saldos De:'),
                m('.btn-group.dropdown', [
                    m('button.btn-sm.btn-default', {'data-toggle':'dropdown'}, m('i.fa.fa-chevron-down')),

                    ctx.saldos.periodo() ? m('span', {}, [
                        'Periodo Contable: ',

                        m('input[type=number]', {
                            value : ctx.saldos.mes_contable(),
                            onchange : m.withAttr('value', function (v) {
                                console.log(v);
                                ctx.saldos.setMes(v);
                                ctx.saldos.actualizar();
                            }),
                            style : 'width:3em',
                            id : 'mesCont'
                        }),

                        m.trust(' &mdash; '),

                        m('input[type=number]', {
                            value : ctx.saldos.ano_contable(),
                            onchange : m.withAttr('value', function (v) {
                                ctx.saldos.ano_contable(v);
                                ctx.saldos.actualizar();
                            }),
                            style : 'width:5em',
                            id : 'anoCont'
                        }),
                    ]) : m('span', {}, [
                        m('label', {style:'margin-right:12px'}, 'Desde: ',
                            m('input[type=date]', {
                                style : 'height:1.4em; width:9.5em',
                                value : ctx.saldos.fechaInicial()
                            })
                        ),
                        m('label',' Hasta: ',
                            m('input[type=date]', {
                                style : 'height:1.4em; width:9.5em',
                                value : ctx.saldos.fechaFinal()
                            })
                        ),
                    ]),


                    m('ul.dropdown-menu.dropdown-menu-left', [
                        m('li', [
                            m('a', {href:'javascript:;', onclick : ctx.saldos.esteAno }, 'Este Año')
                        ]),

                        m('li', [
                            m('a', {href:'javascript:;', onclick : ctx.saldos.esteMes }, 'Este Mes')
                        ]),

                        m('li.separator'),

                        m('li', [
                            m('a', {href:'javascript:;', onclick : ctx.saldos.periodoFiscal }, 'Periodo Contable')
                        ]),

                    ])
                ])
            ])
        ]),


        m('.row', {style:'margin-bottom:12px'}, [
            m('.col-sm-12', [
                m('.pull-right', [
                    m('button.btn.btn-sm', {
                        'class' : ctx.mostrarOcultas() ? 'active btn-primary' : 'btn-default',
                        onclick : function () {
                            ctx.mostrarOcultas( !ctx.mostrarOcultas() );
                            ctx.updateNodes()
                        }
                    },
                    ctx.mostrarOcultas() ? m('i.ion-ios-checkmark-outline' ): m('i.ion-ios-circle-outline'),
                    ' Mostrar Archivadas'
                    ),

                    m('.btn-group', [
                        m('button.btn.btn-sm.btn-default', {
                            onclick : ctx.expandirTodas
                        }, 'Expandir'),
                        m('button.btn.btn-sm.btn-default', {
                            onclick : ctx.colapsarTodas
                        }, 'Colapsar')
                    ])

                ])
            ])
        ]),

        ctx.ready() ? m('.table-responsive', [
            m('table.table.arbol-cuentas', {config:ctx.setupTable})
        ]) : oor.loading()

    ]);
}
