var VerCuenta = require('./verCuenta');
var CrearCuenta = require('./crearCuenta');
var SearchBar = require('../../misc/components/SearchBar');

module.exports = {
    controller : CatalogoController,
    view : CatalogoView,
    D3Table : D3Table
};

function CatalogoController () {
    var ctx = this;
    oor.fondoGris();

    ctx.search = m.prop('')
    ctx.updateTable = m.prop();
    ctx.dCuentas = m.prop();
    ctx.dCuentas.cargando = m.prop(false);
    ctx.idCuentaSeleccionada = m.prop();
    ctx.cuentaSeleccionada = m.prop();
    ctx.cuentaSeleccionada.cargando = m.prop(false);
    ctx.crearCtaModule = m.prop();
    ctx.expandido = m.prop();
    ctx.opened = {};
    ctx.mostrarArchivadas = m.prop(false);


    ctx.UpdateRow = UpdateRow;
    ctx.beforeDraw = beforeDraw;


    ctx.saldos = m.prop();
    ctx.saldos.cargando = m.prop();

    ctx.searchBar = new SearchBar.controller({
        search : ctx.search,
        autofocus : true,
        onsearch : function (d) {
            ctx.updateTable(true)
            m.redraw()
        }
    })

    ctx.cargarCuentas = function () {
        if(ctx.dCuentas()  || ctx.dCuentas.cargando()) {
            return
        }

        ctx.dCuentas.cargando(true);

        oor.request('/cuentas-contables/datos')
            .then(function (r) {
                ctx.childrenMap = {};

                Object.keys(r).forEach(function (name) {
                    if(name == 'root') {
                         ctx.root = r[name];
                         ctx.root.search = '';
                         return;
                    }

                    ctx.childrenMap[name] = r[name].map(buildRecord)

                    if(name.length <= 5) {
                        ctx.opened[name] = true;
                    } else {
                        ctx.childrenMap[name] = ctx.childrenMap[name].sort(function (d1,d2) {
                            return d2.id > d1.id ? -1 : 1
                        });
                    }
                });

                ctx.dCuentas(true);
            })
            .then(function () {
                return oor.request('/cuentas-contables/tipos')
            })
            .then(function (tipos) {
                ctx.tipos = {};
                tipos.forEach(function (t) {
                    ctx.tipos[t.tipo_id] = t;
                });
            })
            .then(ctx.dCuentas.cargando.bind(null,false))
    }

    function buildRecord (d) {
        var nombre = d[0].length > 3 ? d[0].concat(' - ', d[1]) : (d[1] || '');
        return {
            id:d[0],
            nombre:nombre,
            estatus : d[2],
            tacum : d[3],
            search : nombre.toLowerCase().latinize()
        };
    }


    ctx.cargarCuenta = function (cta) {
        var id = cta.id;
        var seleccionada = id == ctx.idCuentaSeleccionada();

        if(seleccionada) {
            ctx.cuentaSeleccionada(null);
            ctx.cuentaSeleccionada.cargando(true);
        }

        oor.request('/cuentas-contables/cuenta/'.concat(id))
            .then(function (r) {
                if(seleccionada) {
                    ctx.cuentaSeleccionada(r.cuenta);
                    ctx.cuentaSeleccionada.cargando(false);
                }
                ctx.updateTable(true);
            });

        ctx.updateTable(true);
    }

    ctx.crearCuenta = function () {
        var module = m.component(CrearCuenta, {
            oncancel : ctx.crearCtaModule.bind(null,false),
            onsave : function (d) {
                ctx.cuentaSeleccionada(d);
                ctx.idCuentaSeleccionada(d.cuenta);
                ctx.updateTable(true);

                toastr.success('Cuenta '.concat(d.cuenta, '-', d.nombre, ' creada correctamente'));

                ctx.crearCtaModule(false);
                cargarRamaCompleta(d.cuenta);
            }
        });

        ctx.crearCtaModule(module);
    }

    ctx.cargarRamaCompleta = cargarRamaCompleta;

    function cargarRamaCompleta (nCuenta) {
        oor.request('/cuentas-contables/rama/'.concat(nCuenta))
            .then(function (d) {
                var tipo_id = Object.keys(d.rama)[0];
                var ramaMadre = ctx.childrenMap[tipo_id];
                var rama = buildRecord(d.rama[tipo_id]);
                var ramaIdx = ramaMadre.filter( f('id').is(rama.id) )[0];

                ramaMadre[ramaIdx] = rama;
                ctx.opened[rama.id] = true;

                delete(d.rama);

                Object.keys(d).forEach(function (key) {
                    ctx.childrenMap[key] = d[key].map(buildRecord);
                });

                ctx.updateTable(true);
            });
    }
}

function CatalogoView (ctx) {
    ctx.cargarCuentas();


    return oor.panel({
        title:'Catálogo de Cuentas',
        buttons : [
            ctx.crearCtaModule() ? null : oor.stripedButton('button.btn-success', 'Nueva Cuenta', {
                style : 'margin-right:18px',
                onclick : ctx.crearCuenta
            }),
            m('.btn-group', [
                m('.btn btn-primary button-striped button-full-striped button-striped btn-default dropdown-toggle btn-ripple btn-xs', {'data-toggle':'dropdown'},
                    ['Exportar  ',m("span.caret"),{style:'width:30px'}]
                ),

                m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                    m("li", m('a',{href:'/exportar/exportarCatalogo'},  [
                            m('i.fa fa-file-excel-o')," Excel"
                        ])
                    )
                ])
            ])
        ]
    }, ctx.dCuentas.cargando() ? oor.loading() : [

        m('.row', [
            m('.col-sm-6', [
                m('table.tabla-cuentas', {style:'width:100%'},
                    m('thead', [
                        m('tr', [
                            m('td', {'style':'background-color:steelblue;'}, [
                                m('.pull-right', [
                                    m('a[href=javascript:;]', {
                                        style:'margin:20px 6px;'.concat( ctx.expandido() ? 'color:white;' : 'color:#ddd;'),
                                        'title' : 'Expandir Todas',
                                        onclick : function () {
                                            ctx.expandido( !ctx.expandido() );
                                            ctx.updateTable(true)
                                        }
                                    }, m(ctx.expandido() ? 'i.fa.fa-folder-open' : 'i.fa.fa-folder')),

                                    m('a[href=javascript:;]', {
                                        style:'margin:20px 6px;'.concat( ctx.mostrarArchivadas() ? 'color:white;' : 'color:#ddd;'),
                                        'title' : 'Mostrar Archivadas',
                                        onclick : function () {
                                            ctx.mostrarArchivadas( !ctx.mostrarArchivadas() );
                                            ctx.updateTable(true)
                                        }
                                    }, m(ctx.mostrarArchivadas() ? 'i.fa.fa-eye' : 'i.fa.fa-eye-slash'))
                                ]),

                                m('h5', {style:'color:white;margin:4px'}, [
                                    'Cuentas Contables ',
                                    m('a[href=javascript:;][style="color:white;font-weight:bold"]', {}, m('i.ion-ios-search'))
                                ]),
                            ])
                        ]),
                        m('tr', [
                            SearchBar.view(ctx.searchBar)
                        ])
                    ])
                ),

                m('table.tabla-cuentas', {config : D3Table(ctx) })
            ]),

            m('.col-sm-6', ctx.crearCtaModule() || [
                ctx.cuentaSeleccionada.cargando() ? oor.loading() : null,
                ctx.cuentaSeleccionada() ? m.component(VerCuenta, {
                    tipos : ctx.tipos,
                    key : ctx.idCuentaSeleccionada(),
                    cuenta: ctx.cuentaSeleccionada(),
                    onsave : function (d) {
                        ctx.cuentaSeleccionada(d);
                        ctx.idCuentaSeleccionada(d.cuenta);
                        ctx.updateTable(true);

                        toastr.success('Datos Guardados');
                        ctx.dCuentas(null);
                    }

                }) : null
            ])

        ])

    ])
}


function D3Table (ctx) {
    if(!ctx.d3Table) {
        ctx.d3Table = createD3Table(ctx);


        window.addEventListener('resize', _.debounce(function () {
            ctx.updateTable(true);
            m.redraw()
        },200));
    }

    return function (element, isInited) {
        if(ctx.updateTable() == false && isInited) return;
        var getIdCta = f('cuenta_contable_id');

        ctx.updateTable(false);
        d3.select(element).call(ctx.d3Table);
    }
}


function makeSearchFilter(s) {
    var search = s.toLowerCase().latinize();
    return function (d) {
        d.idxSearch = d.search.indexOf(search);
        if(d.id.length < 5) return true;
        return d.idxSearch > -1;
    }
}

function fTrue () {
    return true;
}


function UpdateRow (rows, ctx) {
    var width = ctx.width;

    rows.filter(function (d) { return d.depth > 2 }).select('td.cuenta')
        .style('max-width', String(width).concat('px'))
        .style('min-width', String(width).concat('px'))
}


function beforeDraw (selection, ctx) {
    var width = selection.node().offsetParent.getBoundingClientRect().width;
    width -=20;
    ctx.width = width;
}

function createD3Table(ctx) {

    d3Table.tree = d3.layout.tree().children(function (d) {
        if(ctx.expandido()) return ctx.childrenMap[d.id];
        return (ctx.search() || ctx.opened[d.id]) && ctx.childrenMap[d.id]
    });


    return d3Table;

    function d3Table (selection) {
        d3Table.data = d3Table.tree.nodes(ctx.root);

        ctx.beforeDraw && selection.call(ctx.beforeDraw, ctx);


        var idSel = ctx.idCuentaSeleccionada();
        var search = ctx.search();

        // si hay búsqueda se aplica el filtro
        var data = d3Table.data.filter(search ? makeSearchFilter(search) : fTrue);

        // relaciona los nodos con los datos
        var rows    = selection.selectAll('tr').data(data, f('id'));

        //Remueve los que ya no estan visibles
        rows.exit().remove();

        //Agrega una fila
        var enterRow = rows.enter().append('tr').style('background-color', '#fff').classed({
            isCuenta : function (d) { return d.depth > 2 }
        })

        //Agrega la celda a la fila
        var tdEnter = enterRow.append('td').attr('class', 'cuenta')


        //Organiza las filas de acuerdo a los datos computados
        rows.sort(function (d1, d2) {
            return d3Table.data.indexOf(d1) - d3Table.data.indexOf(d2)
        });

        //Agrega el ícono y el texto
        tdEnter.append('span').style('position','absolute').attr('class', 'icon')
        tdEnter.append('h6').attr('class', 'cta-n-title');

        ctx.EnterRow && enterRow.call(ctx.EnterRow, ctx);

        /**
         * Actualiza el texto y las clases
         */
        rows.select('.cta-n-title')
            .text(f('nombre'))
            .attr('class', function(d) { return  'cta-n-title depth-'.concat(d.depth) })


        ctx.UpdateRow && rows.call(ctx.UpdateRow, ctx)

        /**
         * Actualiza los íconos
         */
        var icons = rows.select('span.icon');

        icons.style('display','none')
            .style('left', function (d) {
                return String(12 + Math.max(d.depth-2, 0) * 15).concat('px');
            });

        //Muestra los ícnos que deben mostrarse
        icons.filter(function (d) {return d.depth > 1})
            .attr('class', 'icon small text-grey fa fa-folder-o')
            .style('display', 'block')

        //Los que tienen hijos, le abre la carpeta
        icons.filter(function (d) { return ctx.expandido() || ctx.opened[d.id] })
            .attr('class', 'icon small text-grey fa fa-folder-open-o');

        //Los que no son acumulativa le pone el icono de archivo
        icons.filter( f('tacum').is('D') ).attr('class', 'icon small text-grey fa');

        icons.on('click', function (d) {
            if(ctx.expandido()){
                return toastr.warning('No disponible en modo expandido');
            }
            var id = d.id;
            ctx.opened[id] = !Boolean(ctx.opened[id]);
            ctx.updateTable(true);
            m.redraw();
        });



        rows.style('display', function (d) {
            if(ctx.mostrarArchivadas() || d.estatus == undefined) return undefined;
            return d.estatus == "1" ? undefined : 'none'
        });


        rows.select('h6').style('color', function (d) {
            if(d.depth == 1) return '#fbfbfb';
            if(d.estatus == undefined) return '#666';

            return d.estatus == "1" ? '#666' : '#bbb'
        });


        rows.select('h6').on('click', function (d) {
            var id = d.id;

            if(!id || id.length < 5) return;
            if(id == ctx.idCuentaSeleccionada()) return;

            ctx.idCuentaSeleccionada(id);
            ctx.cargarCuenta(d);
            ctx.updateTable(true);
            m.redraw();
        });

        rows.style('background-color', '#fff');


        if(idSel) {
            //Marca de amarillo la selección
            rows.filter( f('id').is(idSel) ).style('background-color', '#FFE57F')
        }


        if(search) {
            //Marca con los resultados de la búsqueda
            rows.filter(function (d) { return d.idxSearch > -1 })
                .select('h6')
                .html(function (d) {
                    return [
                        d.nombre.substring(0, d.idxSearch),
                        '<strong class="text-indigo">',
                        d.nombre.substring(d.idxSearch, d.idxSearch + search.length),
                        '</strong>',
                        d.nombre.substring(d.idxSearch + search.length)
                    ].join('')
                });

            //Esconde las carpetas
            icons.style('display', 'none')
        }


        ctx.afterDraw && selection.call(ctx.afterDraw, ctx);

    }
}
