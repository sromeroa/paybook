



function CuentasFormController () {

    var ctx = {};
    var form = ddui();



    ddui.initialize(function (selection, context) {
        var node = selection.node();
        var bridge = BridgeForm({ withContext : context});

        bridge('change', this,context);
        bridge('input', this,context);
    });



    //form.implement(updater());
    /*

    form.on('change', '[x-prop]', function () {
        var input = $(this);
        var prop =  ctx[input.attr('x-prop')];
        prop( input.val() );
    });


    form.on('input', '[x-prop]', function () {

    })

    */


    //Mount on the DOM
    function config (element) {
        form(element);
    }
}



function updater (context) {
    return function (form) {
        form.on('change', '[x-prop]', function () {
            var input, prop, scope, model;
            
            input = $(this);
            model = input.attr('x-model');
            scope = model ? f(model)(context) : context;
            prop = model[input.attr('x-prop')];
            prop(input.val())
        });



    }
}




function Bridge (settings) {
    var context = settings.context;
    function bridge (element) {
        $(element).on('change', '[x-prop]', function () {
            var input, prop, scope, model;
            
            input = $(this);
            model = input.attr('x-model');
            scope = model ? f(model)(context) : context;
            prop = model[input.attr('x-prop')];
            prop(input.val());
        })
    }
}








function ddui () {
    return dduiMounter;

    function dduiMounter (selection, context) {
        if(!selection.attr('x-ddui-id')) {
            initialize(selection, context)
        }
        update(selection, context);
    }   
}


ddui.update = function () {
    d3.selectAll('[x-ddui-id]').each(function (){
        d3.select(this)
    })
}




ddui.mount = function (selector, module, context) {
    if(nh.isFunction(context)) {
        console.log('Not supported! Functions as context')
        return;
    } else {
        if(!context) {
            context = {};
        }

        rawMount(selector,module, context);
    }
}


function rawMount (selector, module, context) {
    d3.select(selector, module, context)
}



