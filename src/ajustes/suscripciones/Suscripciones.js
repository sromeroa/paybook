$(document).ready(function () {
    if (window.location.pathname.indexOf("/suscripciones") > -1) {
        var interval = setInterval(function () {
            if (typeof oorden() !== 'undefined' && typeof oorden.organizacion() !== 'undefined' && oorden.organizacion().hasOwnProperty("usuario_id")) {
                clearInterval(interval);
                ejecutarVue();
            }
        }, 100);


        function ejecutarVue() {
            Vue.config.delimiters = ['[[', ']]'];
            var vm = new Vue({
                el: "#suscripciones-index",
                ready: function () {
                    this.planes();
                    oorden();
                    oorden.organizacion();

                    this.obtenerTarjetaUsuario();


                    this.obtenerModalidadUsuario();
                    // this.obtenerModalidades();

                    // $("#modal-modalid-pago").modal();
                },
                data: {
                    suscripcionData: [],
                    planesData: [],
                    fechaActual: moment().format("YYYY-MM-DD"),
                    fechaVencimiento: null,
                    tarjetaUsuario: null,

                    numero_tarjeta: null,
                    mes_expiracion: null,
                    ano_expiracion: null,
                    cvc: null,
                    ciudad: null,
                    pais: null,
                    zip: null,
                    direccion: null,
                    nombre: null,


                    listaModalidades: [],
                    usuarioModalidadPago: {},
                    modalidadSeleccionada: "",
                    tieneModalidadPago: false,

                    permitirActualizarSuscripcion: false
                },
                filters: {
                    fechaHuman: function (fecha) {
                        return moment(fecha).fromNow();
                    },
                    fechaFormateada: function (fecha) {
                        return moment(fecha).format("DD/MM/YYYY");
                    },
                    rango: function (max, tipo) {
                        var min = 0;
                        if (tipo == "a") {
                            min = new Date().getFullYear();
                        } else if (tipo == "m") {
                            min = 1;
                        }
                        var resultado = [];
                        for (var i = 0; i < max; i++) {
                            resultado.push(min + i);
                        }
                        return resultado;
                    }
                },

                methods: {
                    suscripcionUsuario: function (usuarioId) {
                        var local = this;
                        $.ajax({
                            url: "api/suscripcionUsuario/" + usuarioId,
                            method: "GET",
                            success: function (suscripcion) {
                                if (Object.keys(suscripcion.data).length) {
                                    local.suscripcionData = suscripcion.data;
                                } else {
                                    toastr.warning("Aún no estás suscrito a ningún plan.");
                                }
                                local.planes();
                            }
                        })
                    },
                    planes: function () {
                        var local = this;
                        $.ajax({
                            url: "api/planes",
                            method: "GET",
                            success: function (planes) {
                                if (planes.length) {
                                    local.planesData = planes;
                                }
                            }
                        })
                    },
                    actualizarPlan: function (plan, evento) {

                        // Si el usuario tiene una tarjeta asignada es porque ya rellenó el formulario de billing
                        if (!vm.tarjetaUsuario) {
                            $('#modal-pagos-tarjeta').modal("show");
                        } else {
                            var respuesta = confirm("¿Estás seguro que deseas actualizar tu plan?");
                            // toastr.success("Suscripción", "Suscripción actualizada");
                        }

                        if (respuesta && vm.permitirActualizarSuscripcion) {
                            var $elemento = $("#" + evento.target.id);
                            $.ajax({
                                url: "api/actualizarSuscripcion/" + oorden.organizacion().usuario_id,
                                method: "POST",
                                data: {"plan": plan},
                                beforeSend: function () {
                                    $(".btn-actualizar").prop("disabled", true);
                                    $elemento.text("Actualizando plan ");
                                    $elemento.append('<li class="fa fa-spin fa-spinner"></li>');
                                },
                                success: function (respuesta) {
                                    toastr.success("Tu suscripción ha sido actualizada exitosamente", "Suscripción actualizada");
                                    $(".btn-actualizar").prop("disabled", false);
                                    $elemento.find('li').remove();
                                    $elemento.text("Actualizar plan");
                                    vm.suscripcionUsuario(oorden.organizacion().usuario_id);
                                }
                            })
                        }
                    },
                    obtenerTarjetaUsuario: function () {
                        $.ajax({
                            url: "api/obtenerMarcaTarjeta/" + oorden.organizacion().usuario_id,
                            method: "GET",
                            success: function (respuesta) {
                                vm.tarjetaUsuario = respuesta.data.tarjeta;
                                vm.permitirActualizarSuscripcion = !!vm.tarjetaUsuario;
                                vm.suscripcionUsuario(oorden.organizacion().usuario_id);

                            }
                        })
                    },
                    crearSuscripcion: function () {
                        toastr.success("Tu suscripción ha sido actualizada exitosamente", "Suscripción actualizada");
                        $('#informacion-tarjeta-modal').modal("hide");
                        $('#formulario-datos-tarjeta')[0].reset();
                    },
                    obtenerModalidadUsuario: function () {
                        $.ajax({
                            url: "api/usuariosModalidadPago?usuario_id=" + oorden.organizacion().usuario_id,
                            method: "GET",
                            success: function (respuesta) {
                                if (Object.keys(respuesta.data).length) {
                                    vm.usuarioModalidadPago = respuesta.data;
                                    vm.tieneModalidadPago = !!Object.keys(respuesta.data).length;
                                }
                                vm.obtenerModalidades();

                            }
                        })
                    },
                    obtenerModalidades: function () {
                        $.ajax({
                            url: "api/modalidadesPago",
                            method: "GET",
                            success: function (respuesta) {
                                if (respuesta.data.length) {
                                    vm.listaModalidades = respuesta.data;
                                } else {
                                    toastr.success(respuesta.status.message.charAt(0).toUpperCase() + respuesta.status.message.slice(1));
                                }
                            }
                        })
                    },
                    verificarObjeto: function(obj) {
                      return Object.keys(obj).length;
                    },
                    actualizarModalidadPago: function () {
                        $.ajax({
                            url: "api/modalidadesPago/" + vm.usuarioModalidadPago.id,
                            method: "PUT",
                            data: {
                                "modalidad": vm.modalidadSeleccionada
                            },
                            beforeSend: function () {
                                // $elemento.text("Actualizando plan");
                                // $(".btn-actualizar").prop("disabled", true);
                                $(".btn-accion-modalid-pago").prop("disabled", true);
                                $(".btn-accion-modalid-pago").append('<li class="fa fa-spin fa-spinner"></li>');
                            },
                            success: function (respuesta) {
                                $(".btn-accion-modalid-pago").prop("disabled", false);
                                $(".btn-accion-modalid-pago li").remove();

                                toastr.success("Modalidad de pago actualizada");

                            }
                        })
                    },
                    crearModalidadPago: function () {

                        if (!vm.modalidadSeleccionada) {
                            toastr.error("Debes seleccionar una modalidad de pago", "Error");
                            return;
                        }

                        if (vm.modalidadSeleccionada == "transferencia_bancaria" ||
                            vm.modalidadSeleccionada == "domiciliacion_bancaria") {
                            toastr.warning("Esta modalidad de pago aún no está disponible");
                            return;
                        }

                        $.ajax({
                            url: "api/crearUsuarioModalidadPago",
                            method: "POST",
                            data: {
                                "modalidad": vm.modalidadSeleccionada,
                                "usuario_id": oorden.organizacion().usuario_id
                            },
                            beforeSend: function () {
                                // $elemento.text("Actualizando plan");
                                // $(".btn-actualizar").prop("disabled", true);
                                $(".btn-accion-modalid-pago").prop("disabled", true);
                                $(".btn-accion-modalid-pago").append('<li class="fa fa-spin fa-spinner"></li>');
                            },
                            success: function (respuesta) {

                                if (Object.keys(respuesta.data).length) {
                                    vm.usuarioModalidadPago = respuesta.data;

                                    if (respuesta.data.modalidad == "transferencia_bancaria" ||
                                        respuesta.data.modalidad == "domiciliacion_bancaria") {
                                        vm.permitirActualizarSuscripcion = true;

                                    } else if (respuesta.data.modalidad == "tarjeta_credito"){
                                        toastr.success("Modalidad de pago creada");
                                        $("#modal-pagos-tarjeta").modal("show");
                                        $("#modal-modalid-pago").modal("hide");
                                        vm.tieneModalidadPago = !!Object.keys(respuesta.data).length;
                                    }


                                }

                                $(".btn-accion-modalid-pago").prop("disabled", false);
                                $(".btn-accion-modalid-pago li").remove();

                                // $("#btn-actualizar-modalidad").remove();
                            }
                        })
                    },
                    asignarTarjeta: function () {
                        $.ajax({
                            url: "api/asignarTarjeta/" + oorden.organizacion().usuario_id,
                            method: "POST",
                            data: {
                                "numero_tarjeta": vm.numero_tarjeta,
                                "cvc": vm.cvc,
                                "mes_expiracion": vm.mes_expiracion,
                                "ano_expiracion": vm.ano_expiracion,
                                "zip": vm.zip,
                                "direccion": vm.direccion,
                                "pais": vm.pais,
                                "ciudad": vm.ciudad,
                                "nombre": vm.nombre
                            },
                            beforeSend: function () {
                                // $(".btn-actualizar").prop("disabled", true);
                                $(".tarjeta-btn").prop("disabled", true);
                                $(".tarjeta-btn").append('<li class="fa fa-spin fa-spinner"></li>');
                            },
                            success: function (respuesta) {

                                vm.obtenerTarjetaUsuario();
                                vm.permitirActualizarSuscripcion = true;
                                toastr.success("Datos de tarjeta guardados");
                                setTimeout(function(){
                                    $("#modal-pagos-tarjeta").modal("hide");
                                }, 1000);
                            }
                        }).done(function(){
                            $(".tarjeta-btn").prop("disabled", false);
                            $(".tarjeta-btn").find("li").remove();
                        })


                    },
                    actualizarTarjeta: function () {
                        vm.permitirActualizarSuscripcion = false;

                    }
                }
            });
        }
    }
});