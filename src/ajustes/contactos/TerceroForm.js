module.exports = {
    view : TerceroFormView,
    controller : TerceroFormController
};

var Tercero   = require('./TerceroModel');
var Direccion = require('./DireccionModel');
var Persona   = require('./PersonaModel');

var tabs = m.prop([
    {
        name : 'tercero',
        caption : 'Organización',
        view : TabTerceroView,
    },
    {
        name : 'direccion',
        caption : 'Direcciones',
        view : TabDireccionView
    },
    {
        name : 'personas',
        caption : 'Personas',
        view : TabPersonasView
    }
]);


function TerceroFormController (params) {
    var ctx = this;
    var selectores = {};
    ctx.selectores = selectores;
    ctx.ready = m.prop(false);
    ctx.cliente = m.prop(params.cliente? true : false);
    ctx.proveedor = m.prop(params.proveedor ? true : false);


    oorden().then(oorden.paisesYMonedas)
    .then(function () {
        //Si las categorias estan especificadas las usa
       if(params.categorias) return params.categorias;

       //Si no, las consulta
       return oor.request('/apiv2?modelo=terceros-categorias');
    })
    .then(function (categorias) { params.categorias = categorias; })
    .then(function () {
        var categoriaEnabled = m.prop();

        ctx.direcciones = m.prop();
        ctx.tercero = m.prop(new Tercero(params.tercero));
        ctx.direccion = m.prop();
        ctx.ready(true);


        ctx.tab = m.prop(tabs()[0]);

        if(!params.tercero) {
            ctx.tercero().$isNew(true);
        }

        if(ctx.cliente()) {
            ctx.tercero().es_cliente(true);
        }

        if(ctx.proveedor()) {
            ctx.tercero().es_proveedor(true);
        }

        if(!ctx.tercero().codigo_pais()){
            ctx.tercero().codigo_pais( oorden.organizacion().codigo_pais )
        }

        if(!ctx.tercero().tercero_categoria_id()) {
            if(params.categoria) {
                ctx.tercero().tercero_categoria_id(params.categoria.tercero_categoria_id)
            }
        }

        if(!ctx.tercero().clave_moneda()) {
            ctx.tercero().clave_moneda(oorden.organizacion().codigo_moneda_base)
        }

        ctx.setup = oor.xPropConfig(ctx);

        categoriaEnabled(
            ctx.tercero().$isNew() ||  !ctx.tercero().tercero_categoria_id()
        );

        ctx.guardar = function () {
            if(!ctx.tercero().es_cliente() && !ctx.tercero().es_proveedor()) {
                return toastr.error('Seleccione si es cliente o proveedor');
            }

            return Tercero.guardar(ctx.tercero()).then(function (t) {
                //Se Actualiza el id
                ctx.tercero().tercero_id(t.tercero_id);
                ctx.tercero().ultima_direccion_fiscal(t.ultima_direccion_fiscal);

                ctx.$modal && ctx.$modal.close();
                nh.isFunction(params.onsave) && params.onsave(ctx.tercero());
            }, function (r) {
                nh.isFunction(params.onerror) && params.onerror(r);
            });
        }

        ctx.eliminarTercero = function () {
            var confirmar = confirm('¿Eliminar Tercero?');

            if(!confirmar) return;

            Tercero.eliminar( Tercero.id(ctx.tercero()) )
                .then(function () {
                    toastr.warning('Tercero Eliminado');
                    nh.isFunction(params.ondelete) && params.ondelete();
                    ctx.$modal && ctx.$modal.close();
                }, function (r) {
                    toastr.error(r.status.message);
                });
        };

        ctx.cargarDirecciones = function () {
            if(ctx.direcciones()) return;

            ctx.direcciones([]);

            oor.request('/api/direcciones/' + ctx.tercero().tercero_id())
                .then(ctx.direcciones)
                .then(function (direcciones) {
                    var direccion = direcciones.filter(function (dir) {
                        return dir.direccion_id == ctx.tercero().ultima_direccion_fiscal()
                    })[0];

                    return new Direccion(direccion);
                })
                .then(ctx.direccion)

        }

        ctx.asignarActual = function (d) {
            var direccion = new Direccion(d);
            ctx.direccion(direccion);

            Tercero.guardartercero({
                tercero_id : ctx.tercero().tercero_id,
                ultima_direccion_fiscal : direccion.direccion_id
            }).then(function (d) {
                toastr.success('Direccion Actual Modificada')
            }, function () {
                toastr.error('Ha ocurrido un error al cambiar la dirección actual')
            });

        }

        
        selectores.categoria = oor.select2({
            data : params.categorias.map(function (c) {
                return {
                    id : f('tercero_categoria_id')(c),
                    text : f('categoria')(c)
                }
            }).filter(f('id')),
            model : model(ctx, 'tercero', 'tercero_categoria_id')
            //enabled : categoriaEnabled
        });


        selectores.pais = oor.select2({
            data : oorden.paises().map(function (p) {
                return {
                    id : f('codigo_pais')(p),
                    text : f('nombre')(p)
                }
            }),
            model : model(ctx, 'tercero', 'codigo_pais')
        });


        selectores.moneda = oor.select2({
            data : oorden.tiposDeCambio().map(function (m) {
                return {
                    id : f('tipo_de_cambio')(m),
                    text : f('tipo_de_cambio')(m) + ' ' + f('nombre')(m)
                }
            }),
            model : model(ctx, 'tercero', 'clave_moneda')
        });

    });

    m.redraw();
}

function model (ctx, holderName, prop) {
    return function () {
        var holder = f(holderName)(ctx);
        if(arguments.length) {
            holder[prop](arguments[0]);
        }
        return holder[prop]();
    }
}

function TerceroFormView (ctx) {
    var cat = ctx.tercero();
    if(!ctx.ready()) return oor.loading();

    return m('div', {config:ctx.setup}, [
        m('button.btn.btn-sm.btn-flat.pull-right', {
            style:'margin-top:10px',
            onclick : ctx.eliminarTercero
        }, m('i.fa.fa-trash')),

        m('ul.nav.nav-tabs', [
            tabs().map(function (tab) {
                return m('li', {'class' : tab == ctx.tab() ? 'active' : ''}, [
                    m('a[href=javascript:;]', {
                        onclick : function () { ctx.tab(tab); }
                    }, tab.caption)
                ]);
            })
        ]),

        m('div', {
            style:'padding:10px; border:1px solid #f0f0f0',
            key : ctx.tab() ? ctx.tab().name : null
        }, [
            ctx.tab() ? ctx.tab().view(ctx) : null
        ])
    ])
}


function input (ctx, model, prop, label, params) {
    var holder = f(model)(ctx);

    return m('.mt-inputer.line', {style:'flex:1 15em'},[
        m('label', label),
        m('input[type="text"]',
            nh.extend({
                'x-prop' : prop,
                'x-model' : model,
                'value' : f(prop)(holder)
            }, params)
        )
    ])
}



function TabDireccionView (ctx) {
    ctx.cargarDirecciones();
    var tercero = ctx.tercero();
    var direccion = ctx.direccion();
    if(!direccion) return oor.loading();

    function config (element, isInitialized) {
        if(isInitialized) return;

        $(element).on('change', '[propname]', function () {
            var input = $(this);
            var propname = input.attr('propname');
            var prop = ctx.direccion()[propname];
            prop( input.val() );

            if(!ctx.tercero().direccion) {
                ctx.tercero().direccion = m.prop()
            }

            ctx.tercero().direccion(ctx.direccion());
        })
    }

    return m('div', {config : config}, [
        m('h5', 'Dirección Fiscal'),

        m('.flex-row.flex-end', [
            m('.mt-inputer',  {style:'flex:8'}, [
                m('label','Calle'),
                m('input[type=text]', {
                    value : direccion.calle_y_numero(),
                    propname : 'calle_y_numero'
                })
            ]),

            m('.mt-inputer', [
                m('label','#'),
                m('input[type=text]', {
                    size : 10,
                    value : direccion.no_exterior(),
                    propname : 'no_exterior'
                })
            ]),

            m('.mt-inputer', [
                m('label','Int.'),
                m('input[type=text]', {
                    size : 10,
                    value : direccion.no_interior(),
                    propname : 'no_interior'
                })
            ])
        ]),

        m('.flex-row.flex-end', [
            m('.mt-inputer', {style:'flex:8'}, [
                m('label','Colonia'),
                m('input[type=text]',{
                    value : direccion.colonia(),
                    propname : 'colonia'
                })
            ]),

            m('.mt-inputer', [
                m('label','CP'),
                m('input[type=text]', {
                    size : 8,
                    value : direccion.codigo_postal(),
                    propname : 'codigo_postal'
                })
            ])
        ]),

        m('.flex-row.flex-end', [
            m('.mt-inputer', {style:'flex:3'}, [
                m('label','Ciudad'),
                m('input[type=text]', {
                    value :   direccion.ciudad_o_municipio(),
                    propname : 'ciudad_o_municipio'
                })
            ]),

            m('.mt-inputer', {style:'flex:3'}, [
                m('label','Estado'),
                m('input[type=text]', {
                    value : direccion.estado_o_region(),
                    propname : 'estado_o_region'
                })
            ]),

            m('.mt-inputer', [
                m('label','País'),
                m('input[type=text]', {
                    size : 10,
                    value : direccion.codigo_pais(),
                    propname : 'codigo_pais'
                })
            ])
        ]),

        m('br'),
        m('h5', 'Otras Direcciones'),

        ctx.direcciones()
            //.filter(function (d) { return d.direccion_id != direccion.direccion_id() })
            .map(function (d) {
                var direccionActual = d.direccion_id == direccion.direccion_id();
                return m('div',  {style:'border:1px solid #ddd; padding:4px; border-radius:4px; margin:4px'}, [
                    m('.pull-right',  [

                        direccionActual == false ? [
                            m('a[href=javascript:;]', {style:'margin:4px'}, [
                                m('i.ion-ios-trash.text-grey')
                            ])
                        ] : null,

                        m('a[href=javascript:;]', {style:'margin:4px', onclick:ctx.asignarActual.bind(ctx, d)}, [
                            m('i.ion-checkmark', {
                                'class' :  direccionActual ? 'text-green' : 'text-grey'
                            })
                        ])
                    ]),
                    m('div', [
                        m('span', d.calle_y_numero),
                        ' #' ,
                        m('span', d.no_exterior),
                        d.no_interior ? [
                            ' Int. ',
                            m('span', d.no_interior)
                        ] : null
                    ]),

                    m('div', [
                        m('span.text-grey', 'Colonia '),
                        m('span', d.colonia),
                        m('span.text-grey', ' CP '),
                        m('span', d.codigo_postal)
                    ]),

                    m('div', [
                        m('span', d.ciudad_o_municipio),
                        ', ',
                        m('span', d.estado_o_region),
                        ', ',
                        m('span', d.codigo_pais)
                    ])
                ])
            })
    ])
}


function TabTerceroView (ctx) {
    return  [
        m('.flex-row', [
            m('.mt-inputer.line', [
                m('label', 'Categoría'),
                m('div', ctx.selectores.categoria.view())
            ]),
            input(ctx, 'tercero','codigo', 'Código', {size:10}),
            input(ctx, 'tercero','nombre', 'Nombre')
        ]),

        m('.flex-row', [
            m('.mt-inputer.line', [
                m('label', 'País'),
                m('div.pais-selector', ctx.selectores.pais.view())
            ]),
            input(ctx, 'tercero','clave_fiscal', 'Clave Fiscal', {size:'15'})
        ]),

        m('.row', [
            m('.col-sm-6', [
                m('label', [
                    m('input[type=checkbox]', {
                        checked : ctx.tercero().usar_cuentas_detalle(),
                        disabled : ctx.tercero().cuentas_asignadas(),
                        onchange : m.withAttr('checked', ctx.tercero().usar_cuentas_detalle)
                    }),
                    ' Usar Cuentas Individualizadas'
                ])
            ])
        ]),

        m('br'),

        m('.row', [
            m('.col-sm-6', [
                m('label', [
                    m('input[type=checkbox]', {
                        checked : ctx.tercero().es_cliente(),
                        disabled : ctx.cliente(),
                        onchange : m.withAttr('checked', ctx.tercero().es_cliente)
                    }),
                    ' Cliente'
                ])
            ])
        ]),

        ctx.tercero().es_cliente() ? m('.row', [
            m('.col-sm-6', [
                input(ctx, 'tercero', 'dias_credito_venta', 'Días de Crédito que se le dan')
            ]),
            m('.col-sm-6', [
                input(ctx, 'tercero', 'descuento', '% Descuento')
            ]),
        ]) : null,

        m('br'),

        m('.row', [
            m('.col-sm-6', [
                m('label', [
                    m('input[type=checkbox]', {
                        checked : ctx.tercero().es_proveedor(),
                        disabled : ctx.proveedor(),
                        onchange : m.withAttr('checked', ctx.tercero().es_proveedor)
                    }),
                    ' Proveedor'
                ])
            ])
        ]),

        ctx.tercero().es_proveedor() ? m('.row', [
            m('.col-sm-6', [
                input(ctx, 'tercero', 'dias_credito_compra', 'Días de Crédito que otorga')
            ])
        ]) : null
    ];
}

function initializeTabPersonas (ctx) {
    if(ctx.nuevaPersona) return;

    ctx.personas = m.prop();
    ctx.nuevaPersona = m.prop();
    ctx.personasEditando = {};
    ctx.cargandoPersonas = m.prop(false)

    ctx.agregarPersona = function () {
        var persona = new Persona;
        ctx.nuevaPersona(persona);
        persona.$isNew(true);
    }

    ctx.cancelarPersona = function (persona){
        if(persona == ctx.nuevaPersona()) {
            return ctx.nuevaPersona(null);
        }
        ctx.personasEditando[ f('persona_id')(persona) ] = false;
    }

    ctx.guardarPersona = function (persona) {
        persona.tercero_id = f('tercero_id')(ctx.tercero());

        Persona.guardar(persona).then(
            function () {
                toastr.success('¡Contacto Guardado!');
                ctx.personas(null)
                ctx.nuevaPersona(null)
            }
        )

    }


    ctx.cargarPersonas = function () {
        if(ctx.personas() || ctx.cargandoPersonas() ) {
            return
        }

        ctx.cargandoPersonas(true);

        oor.request('/apiv2', 'GET', {
            data : {modelo:'personas', tercero_id:ctx.tercero().tercero_id()}
        })
        .then(ctx.personas)
        .then(ctx.cargandoPersonas.bind(null,false))
    }
}



function TabPersonasView (ctx)  {

    initializeTabPersonas(ctx);
    ctx.cargarPersonas();

    return m('div', [
        ctx.nuevaPersona() ? null : m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs.pull-right', {onclick:ctx.agregarPersona}, [
            'Agregar'
        ]),
        m('h5', 'Personas:'),

        ctx.personas() ? m('table.table.megatable', [
            m('thead', [
                m('tr', [
                    m('td', 'Persona'),
                    m('td', '')
                ])
            ]),
            m('tbody', [
                ctx.personas().map(function (persona) {
                    return m('tr.btn-toggler', {style:'border-bottom:1px solid  #ccc; padding:4px 8px'}, [
                        m('td', [
                            m('div', [
                                m('span.small.text-grey', {}, persona.puesto), ' ',
                                m('span', {}, persona.nombre), ' ',
                                m('span', {}, persona.apellido)
                            ]),
                            m('div', [
                                m('span.small.text-grey', 'Email: '),
                                m('span', persona.email),
                                ' '
                            ]),
                            m('div', [
                                m('span.small.text-grey', 'Tel. Fijo: '),
                                m('span', persona.telefono_fijo),
                                ' ',
                                m('span.small.text-grey', 'Tel. Móvil: '),
                                m('span', persona.telefono_movil)
                            ])
                        ]),

                        m('td', [
                            m('a.edit-btn[href=javascript:;]', [
                                m('i.fa.fa-pencil')
                            ])
                        ])
                    ])
                })
            ])
        ]) : oor.loading(),

        m('.row', [
            ctx.nuevaPersona() ? FormularioPersona(ctx, ctx.nuevaPersona()) : null
        ]),

        m('.clear')

    ]);
}



function FormularioPersona (ctx, persona) {
    return m('div', {style:'border: 1px solid #eee; padding:4px; border-radius:4px'}, [
        m('.flex-row', [
            m('.mt-inputer.line', {style:'flex:1 3'}, [
                m('label', 'Puesto'),
                m('input[type=text]', {
                    value : persona.puesto(),
                    onchange : m.withAttr('value', persona.puesto),
                    config : function(el, isInitialized) {
                        if(!isInitialized) el.focus();
                    },
                    size: 12
                })
            ]),

            m('.mt-inputer.line', [
                m('label', 'Nombre'),
                m('input[type=text]', {
                    value : persona.nombre(),
                    onchange : m.withAttr('value', persona.nombre),
                    size: 20
                })
            ]),

            m('.mt-inputer.line', [
                m('label', 'Apellido'),
                m('input[type=text]', {
                    value : persona.apellido(),
                    onchange : m.withAttr('value', persona.apellido),
                    size: 20
                })
            ])
        ]),

        m('.flex-row', [
            m('.mt-inputer.line',  {style:'flex:1 2'}, [
                m('label', 'Email'),
                m('input[type=text]', {
                    value : persona.email(),
                    onchange : m.withAttr('value', persona.email),
                })
            ]),

            m('.mt-inputer.line', {style:'flex:1 3'}, [
                m('label', 'Tel Fijo'),
                m('input[type=text]', {
                    value : persona.telefono_fijo(),
                    onchange : m.withAttr('value', persona.telefono_fijo),
                })
            ]),

            m('.mt-inputer.line', {style:'flex:1 3'}, [
                m('label', 'Tel. Móvil'),
                m('input[type=text]', {
                    value : persona.telefono_movil(),
                    onchange : m.withAttr('value', persona.telefono_movil),
                })
            ])
        ]),

        m('.row', [
            m('col-sm-12', [
                m('button.btn.btn-sm.btn-primary.pull-right', {onclick:ctx.guardarPersona.bind(ctx, persona)}, 'Guardar'),
                m('button.btn.btn-sm.btn-default.pull-left',  {onclick:ctx.cancelarPersona.bind(ctx, persona)}, 'Cancelar')
            ])
        ])
    ]);
}
