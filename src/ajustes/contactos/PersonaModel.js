module.exports = Persona;


var fields =  {
    puesto : {},
    titulo : {},
    nombre : {},
    apellido : {},
    email : {},
    telefono_fijo : {},
    telefono_movil : {},
    email : {}
}


function Persona (data) {
    var cat = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(nh.isFunction(fieldDef.filter)){
            fieldVal = fieldDef.filter(fieldVal);
        } 

        if(typeof fieldVal == 'undefined') {
            fieldVal = null;
        }

        cat[k] = m.prop(fieldVal || null);
    });

    cat.$isNew = m.prop(false);
}


Persona.guardar = function (persona) {
    var url, method;

    method = persona.$isNew() ? 'POST' : 'PUT';
    url = '/apiv2/'.concat( persona.$isNew() ? 'agregar' : 'editar/' + Persona.id(persona) );
    search = '?modelo=personas';

    return oor.request(url + search, method, {data : persona})
}

Persona.id = f('persona_id')