module.exports = Tercero;


var fields =  {
    tercero_id : {},
    tercero_categoria_id : {},
    nombre : {},
    codigo : {},
    codigo_pais : {},
    es_extranjero : {},
    clave_fiscal : {},
    clave_moneda : {},

    descuento : {},
    impuesto_venta : {},
    cuenta_venta_id : {},
    ccto1_venta_id : {},
    ccto2_venta_id : {},
    dias_credito_venta : {},

    cuentas_asignadas : {filter :BooleanFilter},
    usar_cuentas_detalle : {filter :BooleanFilter},


    impuesto_compra_id : {},
    cuenta_compra_id : {},
    ccto1_compra_id : {},
    ccto2_compra_id : {},
    dias_credito_compra : {},

    es_cliente   : { filter : BooleanFilter },
    es_proveedor : { filter : BooleanFilter },

    ultima_direccion_fiscal : {},
    ultima_direccion_envio : {},
}




function Tercero (data) {
    var cat = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(nh.isFunction(fieldDef.filter)){
            fieldVal = fieldDef.filter(fieldVal);
        }
        if(typeof fieldVal == 'undefined') {
            fieldVal = null;
        }

        cat[k] = m.prop(fieldVal || null);
    });

    cat.$isNew = m.prop(false);
}


Tercero.guardar = function (tercero) {
    var method = tercero.$isNew() ? 'POST' : 'PUT';
    var url = '/apiv2/'.concat(tercero.$isNew() ? 'agregar' : 'editar/' + tercero.tercero_id());
    var search = '?modelo=terceros';
    var direccion = tercero.direccion;
    delete( tercero.direccion);


    return oor.request(url + search, method, {data:tercero}).then(function (t) {
        tercero.direccion = direccion;
        tercero.tercero_id(t.tercero_id)
        return guardartercero(tercero)
    });
}

Tercero.guardartercero = guardartercero;

function guardartercero (tercero) {
    return oor.request('/apiv2/editar/' + tercero.tercero_id() + '?modelo=terceros', 'PUT', {data:tercero});
}


Tercero.findOne = function (id) {
    return m.request({
        method : 'GET',
        url : 'apiv2',
        data : {
            modelo : 'terceros',
            tercero_id : id
        },
        unwrapSuccess : function (d) { return d.data[0]; }
    });
}

Tercero.eliminar = function (idTercero) {
    return m.request({
        method : 'DELETE',
        url : '/apiv2/eliminar/' + idTercero + '?modelo=terceros'
    });
}

Tercero.search = function (t) {
    return f('nombre')(t) + ' ' + f('codigo')(t) + ' ' + f('clave_fiscal')(t);
};

Tercero.id = f('tercero_id');

function BooleanFilter (i) {
    return Boolean(Number(i))
}
