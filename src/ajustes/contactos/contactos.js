module.exports = {
    view : ContactoView,
    controller : ContactoController
}

var dynTable = require('../../nahui/dtable');
var TableHelper = require('../../nahui/dtableHelper');
var nahuiCollection = require('../../nahui/nahui.collection');
var Field = require('../../nahui/nahui.field');
var CategoriaForm = require('./CategoriaForm');
var TerceroForm = require('./TerceroForm');
var Tercero = require('./TerceroModel');
var SearchBar = require('../../misc/components/SearchBar');

var tabs = m.prop([
    {
        caption : 'Todos',
        query : {es_cliente : undefined, es_proveedor :undefined}
    },
    {
        caption : 'Clientes',
        query : {es_cliente :'1', es_proveedor :undefined}
    },
    {
        caption : 'Proveedores',
        query : {es_cliente :undefined, es_proveedor:'1'}
    }
]);

function ContactoController () {
    var ctx = this;

    var table, coll, query, thelper;
    var fields = ContactosFields(ctx);
    var searchBar;

    ctx.contactos = m.prop([]);
    ctx.categorias = m.prop([]);
    ctx.categoria = m.prop();

    ctx.tab = m.prop( tabs()[0] );

    ctx.columnas = m.prop([
        fields.nombreYCodigo,
        fields.clave_fiscal,
        fields.categoria,
        //fields.tipo
    ]);

    table = dynTable().key( f('tercero_id') );

    coll = nahuiCollection('productos', {
        identifier : 'tercero_id'
    });

    query = coll.query({});


    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 10 });



    ctx.search = m.prop('');

    searchBar = new SearchBar.controller({
        search : ctx.search,
        onsearch : function () {
            var search = ctx.search() ? {$contains:ctx.search()} : undefined;
            query.add({search : search}).exec()
        }
    });

    ctx.searchBar = searchBar;


    ctx.selectTab = function (t) {
        ctx.tab(t);
        query.add(t.query).exec();
    }

    ctx.agregarCategoria = function () {
        abrirModalCategoria.call(this, ctx,{})
    };

    ctx.agregarContacto = function () {

        abrirModalTercero.call(this, ctx, {
            categoria : ctx.categoria(),
            categorias : ctx.categorias()
        });
    }

    ctx.abrirCategoria = function (categoria) {
        abrirModalCategoria.call(this, ctx, {
            categoria : categoria,
        })
    };

    ctx.actualizarCategorias = function () {
        return m.request({
            url :'/apiv2',
            data : {
                modelo : 'terceros-categorias'
            },
            unwrapSuccess : f('data')
        })
        .then(ctx.categorias)
        .then(agregarCategoriaTodas)
    }


    ctx.actualizarContactos = function (){
        return m.request({
            url : '/apiv2',
            data : {
                modelo : 'terceros'
            },
            unwrapSuccess : f('data')
        })
        .then(ctx.contactos)
        .then(function () {
            ctx.contactos().map(function (item) {
                item.search = Tercero.search(item);
                coll.insert(item)
            });

            query.exec();
        });
    }

    ctx.actualizarTodo = function () {
        coll.removeAll();

        return ctx.actualizarCategorias()
                    .then(ctx.actualizarContactos)
    }

    ctx.buscarCategoria = function (tCatId) {
        return ctx.categorias().filter(function (cat) {
            return f('tercero_categoria_id')(cat) == tCatId
        })[0];
    }

    ctx.seleccionarCategoria = function (tCatId) {
        var cat = ctx.buscarCategoria(tCatId);
        ctx.categoria(cat);

        query.add({
            tercero_categoria_id : cat.tercero_categoria_id || undefined
        }).exec()

    }


    ctx.setupTable = function (element, isInitialized) {
        if(isInitialized)return;

        thelper.element(element);

        $(element).on('click', 'a[data-id]', function (ev) {
            var id = $(this).attr('data-id');
            ctx.abrirTercero.call(this,id);
            return false;

        });
    };


    ctx.abrirTercero = function (id) {
        var $this = this;

        Tercero.findOne(id).then(function (tercero) {
            var catId = f('tercero_categoria_id')(tercero);
            var categoria = ctx.buscarCategoria(catId);

            abrirModalTercero.call($this, ctx, {
                categorias : ctx.categorias(),
                categoria : categoria,
                tercero : tercero
            })
        })
    }

    function agregarCategoriaTodas () {
        ctx.categorias().unshift({
            categoria  : 'Todas',
            title : 'Todas las categorías',
            descripcion : '..',
            categoria_id : null
        });

        if(!ctx.categoria()) {
            ctx.categoria( ctx.categorias()[0] )
        }
    }

    ctx.actualizarTodo();
}

function ContactoView (ctx) {
    return m('.row', [
        m('.col-md-3', [
            ContactoCategoriasView(ctx)
        ]),
        m('.col-md-9', [
            ContactoTablaView(ctx)
        ])
    ])
}

function ContactoCategoriasView (ctx) {
    return [
        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs.pull-right', {
            onclick:ctx.agregarCategoria
        }, '+ Categoría'),
        m('h3.column-title', 'Categorías'),
        m('div', [
            ctx.categorias().map(function (cat) {
                var get = f('tercero_categoria_id');

                return m('.categoria-elem', {
                    'class' : get(cat) == get(ctx.categoria()) ? 'active' : '',
                    onclick : function () {
                        ctx.seleccionarCategoria(cat.tercero_categoria_id)
                    }
                }, [
                    m('div',f('categoria')(cat)),
                    m('small.text-grey', f('descripcion')(cat))
                ])
            })
        ]),
        MTModal.view()
    ];
}

function ContactoTablaView (ctx) {
    var categoria = ctx.categoria();

    return [
        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs.pull-right', {
            onclick:ctx.agregarContacto
        }, '+ Contacto'),

        m('h3.column-title', 'Contactos'),

        m('div.row', [
            m('div.col-sm-8', [
                 m('h5', categoria.title || [ 'Categoría: ',
                    m('a.text-indigo', {
                        href : 'javascript:;',
                        onclick : function () {
                            ctx.abrirCategoria.call(this, ctx.categoria())
                        }
                    }, '"' + categoria.categoria + '"')
                ])
            ]),
            m('div.col-sm-4', [
                SearchBar.view(ctx.searchBar)
            ])
        ]),

        m('br'),

        m('ul.nav.nav-tabs.with-panel', [
            tabs().map(function (tab) {
                return m('li', {
                    'class' : tab == ctx.tab() ? 'active' : '',
                    onclick : ctx.selectTab.bind(ctx,tab)
                }, [
                    m('a', {href:'javascript:;'}, tab.caption)
                ])
            })
        ]),
        m('.table-responsive', [
            m('table.table', {config:ctx.setupTable})
        ]),
    ]
}


function ContactosFields (ctx) {
    var fields = {};

    fields.producto_id = Field('producto_id');

    fields.nombre = Field.linkToResource('nombre', {
        caption : 'Nombre',
        class : 'text-indigo',
        url : function (d) { return '/terceros/ver/' + f('tercero_id')(d); },
        id : f('tercero_id')
    });

    fields.codigo = Field('codigo', {
        caption : 'Código',
        'class' : 'text-grey'
    });

    fields.clave_fiscal = Field('clave_fiscal', {
        caption : 'Clave Fiscal'
    });

    fields.nombreYCodigo = Field.twice('nombreYCodigo', {
        subfields : [fields.nombre, fields.codigo]
    });


    fields.categoria = Field('categoria', {
        caption : 'Categoría',
        value : f('tercero_categoria_id'),
        filter : function (d) {
            var categ = ctx.categorias().filter(function (cat) {
                return f('tercero_categoria_id')(cat) == d;
            })[0];

            return categ ? f('categoria')(categ) : '(sin categoria)';
        }
    })

    fields.tipo = Field('tipoTercero', {
        caption : 'Tipo',
        value : function (d) { return d },
        filter : function (d) {
            var s = '';

            if(f('es_cliente')(d) == 1) {
                s = 'CTE';
            }

            if(f('es_proveedor')(d) == 1) {
                s = s.concat(s ? ' y ' : '', 'PRV')
            }

            return s ? s : '--';
        },
        'class' : 'text-right'
    })

    return fields;

}


function abrirModalCategoria (ctx, params) {
    MTModal.open({
        controller : CategoriaForm.controller,
        args : {
            categoria : params.categoria,
            onsave : function (d) {
                toastr.success('Categoría Guardada');
                ctx.actualizarCategorias().then(function () {
                    ctx.seleccionarCategoria( d.tercero_categoria_id() );
                });
            },
        },
        top : m('h4', params.categoria ? 'Editar Categoría' : 'Agregar Categoria'),
        content : CategoriaForm.view,
        bottom : function (ctx) {
            return [
                m('button.pull-right.btn.btn-sm.btn-success.btn-flat', {
                    onclick : function () {
                        ctx.guardar()
                    }
                }, 'Guardar'),

                m('button.pull-left.btn.btn-sm.btn-default.btn-flat', {
                    onclick : ctx.$modal.close
                }, 'Cancelar')
            ];
        },
        modalAttrs : {
            'class' : 'modal-medium'
        },
        el : this
    });
}

function abrirModalTercero (ctx, params) {
    MTModal.open({
        controller : TerceroForm.controller,
        args : {
            tercero : params.tercero,
            categoria : params.categoria,
            categorias : params.categorias,
            onsave : function (tercero) {
                toastr.success('Contacto Guardado');

                ctx.actualizarTodo().then(function () {
                    var categoriaId = f('tercero_categoria_id')(tercero);
                    ctx.seleccionarCategoria(categoriaId);
                });
            },
            onerror : function (r) {
                toastr.error(r.status.message)
            },
            ondelete : function (tercero) {
                ctx.actualizarTodo();
            }
        },
        top : function (ctx) {
            return m('h4', ctx.tercero().$isNew() ? 'Agregar Contacto' : params.tercero.nombre);

        },
        content : TerceroForm.view,
        bottom : function (ctx) {
            return [
                
                m('button.pull-left.btn.btn-sm.btn-default.btn-flat', {
                    onclick : ctx.$modal.close
                }, 'Cancelar'),

                m('button.pull-right.btn.btn-sm.btn-success.btn-flat', {
                    onclick : function () { ctx.guardar(); }
                }, 'Guardar')
            ]
        },
        modalAttrs : {
            'class' : 'modal-medium'
        },
        el : this
    });
}
