module.exports = {
    view : CategoriaFormView,
    controller : CategoriaFormController
};

var Categoria = require('./CategoriaModel');

var tabs = m.prop([
    {
        name : 'categoria',
        caption : 'Categoría',
        view : TabCategoriaView
    },
    {
        name : 'venta',
        caption : 'Venta',
        view : TabVentaView
    },
    {
        name : 'compra',
        caption : 'Compra',
        view : TabCompraView
    }
])

function CategoriaFormController (params) {
    var ctx = this;

    ctx.categoria = m.prop(new Categoria(params.categoria));

    ctx.tab = m.prop(tabs()[0]);

    if(!params.categoria) {
        ctx.categoria().$isNew(true);
    }

    ctx.guardar = function () {
        
        return Categoria.guardar(ctx.categoria()).then(function (r) {
            //Se Actualiza el id
            ctx.categoria().tercero_categoria_id(r.data.tercero_categoria_id);

            if(nh.isFunction(params.onsave)) {
                params.onsave(ctx.categoria())
            }

            if(ctx.$modal) {
                ctx.$modal.close()
            }
            
        }, function (d) {
            toastr.error(d.status.message);
        })
    };

    function ventaEnabled () {
        return ctx.categoria().mostrar_venta();
    }

    function compraEnabled () {
        return ctx.categoria().mostrar_compra();
    }

    function getSet (m,v) {
        return function () {
            var model = f(m)(ctx);
            var fn = model[v];

            if(arguments.length) {
                fn(arguments[0]);
            }

            return fn();
        }
    }

    oorden().then(function () {
        var selectors = {};
        
        /**
         * Venta 
         **/
        selectors.cuentaCliente = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_cliente'),
            enabled : ventaEnabled
        });

        selectors.cuentaAnticipoCliente = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_anticipo_cliente'),
            enabled : ventaEnabled
        });

        selectors.cuentaVenta = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model :getSet('categoria', 'cuenta_venta'),
            enabled : ventaEnabled
        });

        selectors.cuentaNCVenta = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_nc_venta'),
            enabled : ventaEnabled
        });

        selectors.impuestoVenta = oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : getSet('categoria', 'impuesto_venta'),
            enabled : ventaEnabled
        });


        /**
         * Compra 
         **/
        selectors.cuentaProveedor = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model :getSet('categoria', 'cuenta_proveedor'),
             enabled : compraEnabled
        });

        selectors.cuentaAnticipoProveedor = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_anticipo_proveedor'),
            enabled : compraEnabled
        });

        selectors.cuentaCompra = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_compra'),
            enabled : compraEnabled
        });

        selectors.cuentaNCCompra = oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : getSet('categoria', 'cuenta_nc_compra'),
             enabled : compraEnabled
        });

        selectors.impuestoCompra = oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : getSet('categoria', 'impuesto_compra'),
            enabled : compraEnabled
        });

        ctx.selectors = selectors;

    });
}

function CategoriaFormView (ctx) {
    var cat = ctx.categoria();

    console.log('CategoriaFormView')

    return m('div', [
        m('ul.nav.nav-tabs', [
            tabs().map(function (tab) {
                return m('li', {'class' : tab == ctx.tab() ? 'active' : ''}, [
                    m('a[href=javascript:;]', {
                        onclick : function () { ctx.tab(tab); }
                    }, tab.caption)
                ]);
            })
        ]),

        m('div', {
            style:'padding:10px; border:1px solid #f0f0f0', 
            key : ctx.tab() ? ctx.tab().name : null
        }, [
            ctx.tab() ? ctx.tab().view(ctx) : null
        ])
    ]);
}


function radioer (label, id, fn) {
    return [
        m('div', label),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'si' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(checked)
                }),
                checked : fn()
            }),
            m('label', {'for':id + 'si'}, 'Sí')
        ]),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'no' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(!checked)
                }),
                checked : !fn()
            }),
            m('label', {'for': id + 'no'}, 'No')
        ])
    ];
}


function TabCategoriaView (ctx) {
    var cat = ctx.categoria();

    return [
        m('.flex-row', [
            m('.mt-inputer', [
                m('label', 'Categoría'),
                m('input[type="text"]', {
                    value : cat.categoria(),
                    oninput : m.withAttr('value', cat.categoria)
                })
            ])
        ]),
        m('.flex-row', [
            m('.mt-inputer', [
                m('label', 'Descripción'),
                m('input[type="text"]', {
                    value : cat.descripcion(),
                    oninput : m.withAttr('value', cat.descripcion)
                })
            ])
        ]),

        m('.flex-row', [
            radioer('Mostrar Venta', 'mostrar-venta', cat.mostrar_venta)
        ]),
        m('.flex-row', [
            radioer('Mostrar Compra', 'mostrar-compra', cat.mostrar_compra)
        ])
    ];
}



function TabVentaView (ctx) {
    var cat = ctx.categoria();

    return [
        m('.flex-row', [
            radioer('Mostrar Venta', 'mostrar-venta2', cat.mostrar_venta)
        ]),

        m('.flex-row', [
            radioer('Cuenta Cliente Común', 'cta-cliente-comun', cat.ctas_cliente_comun)
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Cliente'),
                m('div.cuentas-selector', ctx.selectors.cuentaCliente.view())
            ]),

            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Anticipo Cliente'),
                m('div.cuentas-selector', ctx.selectors.cuentaAnticipoCliente.view())
            ])
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Venta'),
                m('div.cuentas-selector', ctx.selectors.cuentaVenta.view())
            ]),

            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta NC Venta'),
                m('div.cuentas-selector', ctx.selectors.cuentaNCVenta.view())
            ])
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Impuesto Venta'),
                m('div', ctx.selectors.impuestoVenta.view())
            ])
        ])
        

    ]
}

function TabCompraView (ctx) {
    var cat = ctx.categoria();

    return [
        m('.flex-row', [
            radioer('Mostrar Compra', 'mostrar-compra2', cat.mostrar_compra)
        ]),

        m('.flex-row', [
            radioer('Cuenta Proveedor Común', 'cta-prov-comun', cat.ctas_proveedor_comun)
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Proveedor'),
                m('div.cuentas-selector', ctx.selectors.cuentaProveedor.view())
            ]),

            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Anticipo Proveedor'),
                m('div.cuentas-selector', ctx.selectors.cuentaAnticipoProveedor.view())
            ])
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta Compra'),
                m('div.cuentas-selector', ctx.selectors.cuentaCompra.view())
            ]),

            m('.mt-inputer.line.cuentas', [
                m('label', 'Cuenta NC Compra'),
                m('div.cuentas-selector', ctx.selectors.cuentaNCCompra.view())
            ])
        ]),

        m('.flex-row', [
            m('.mt-inputer.line.cuentas', [
                m('label', 'Impuesto Compra'),
                m('div', ctx.selectors.impuestoCompra.view())
            ])
        ])
    ]
}