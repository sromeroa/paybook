module.exports = Direccion;


var fields =  {
    direccion_id : {},
    no_exterior : {},
    no_interior : {},
    colonia : {},
    codigo_postal : {},
    calle_y_numero : {},
    ciudad_o_municipio : {},
    estado_o_region : {},
    codigo_pais : {}
}



function Direccion (data) {
    var cat = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(nh.isFunction(fieldDef.filter)){
            fieldVal = fieldDef.filter(fieldVal);
        } 

        if(typeof fieldVal == 'undefined') {
            fieldVal = null;
        }

        cat[k] = m.prop(fieldVal || null);
    });

    cat.$isNew = m.prop(false);
}
