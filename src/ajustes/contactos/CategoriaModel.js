module.exports = Categoria;


var fields =  {
    tercero_categoria_id : {},
    categoria : {},
    descripcion : {},
    subcategoria_de : {},

    mostrar_venta : {filter: BooleanFilter},
    ctas_cliente_comun : {filter: BooleanFilter},
    cuenta_cliente : {},
    cuenta_anticipo_cliente : {},
    cuenta_venta : {},
    cuenta_nc_venta : {},
    impuesto_venta : {},

    mostrar_compra : {filter: BooleanFilter},
    ctas_proveedor_comun : {filter: BooleanFilter},
    cuenta_proveedor :{},
    cuenta_anticipo_proveedor : {},
    cuenta_compra : {},
    cuenta_nc_compra : {},
    impuesto_compra : {}
}


function BooleanFilter (d) {
    return Boolean(Number(d));
}


function Categoria (data) {
    var cat = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal === 'undefined') {
            fieldVal = null;
        }

        cat[k] = m.prop(fieldVal);
    });

    cat.$isNew = m.prop(false);
}


Categoria.id = f('tercero_categoria_id');


Categoria.guardar = function (cat) {
    var method = cat.$isNew() ? 'POST' : 'PUT';
    var url = 'apiv2/'.concat(cat.$isNew() ? 'agregar' : 'editar/' + cat.tercero_categoria_id());
    var search = '?modelo=terceros-categorias';

    return m.request({
        method : method,
        url : url + search,
        data : cat
    });
}


Categoria.eliminar = function (categoriaId) {

    return m.request({
        method : 'DELETE',
        url : '/apiv2/eliminar/' + categoriaId + '?modelo=terceros-categorias'
    })
}