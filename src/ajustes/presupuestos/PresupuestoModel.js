
module.exports = Presupuesto;

function Presupuesto (d) {
    d || (d = {});

    this.ano = m.prop( f('ano')(d) || (new Date).getFullYear() );
    this.nombre = m.prop( f('nombre')(d) || '');
    this.presupuesto_id = m.prop( f('presupuesto_id')(d) )
    this.$isNew = m.prop(false);
}

Presupuesto.id = f('presupuesto_id')

Presupuesto.guardar = function (presupuesto) {
    var url, method;

    if(presupuesto.$isNew()) {
        url = '/apiv2/agregar?modelo=presupuestos'
        method = 'POST'
    } else {
        url = '/apiv2/editar/' + Presupuesto.id(presupuesto) + '?modelo=presupuestos'
        method = 'PUT'
    }

    return m.request({
        method : method, 
        url : url,
        data: presupuesto,
        unwrapSuccess : f('data')
    })
}

Presupuesto.obtener = function (id) {



    return m.request({
        method : 'GET',
        url : '/apiv2', 
        data : {
            modelo : 'presupuestos',
            presupuesto_id : id,
            include : 'presupuestos.partidas'
        },
        unwrapSuccess : function (d) {
            return d.data[0] 
        }
    })
}
