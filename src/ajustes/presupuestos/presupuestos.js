var Presupuesto = require('./PresupuestoModel');
var TablaMeses = require('./TablaMeses');
var PresupuestoForm = require('./PresupuestoForm');

module.exports = {
    controller : PresupController,
    view : PresupView
};

function PresupController () {
    var ctx = this;
    var ano = m.prop(null);
    var presupuesto_id = m.prop(null);

    ctx.ano = m.prop( (new Date).getFullYear() );
    ctx.presupuesto_id = m.prop();   
    ctx.presupuestos = m.prop();
    ctx.editPresupuesto = m.prop();
    ctx.presupuesto = m.prop();

    ctx.crearPresupuesto = function () {
        /*
        PresupuestoForm.abrirModal.call(this, {
            onsave : afterSave,
            onsaveError : saveError,
            ano : ctx.ano()
        });
        */

        var nPresupuesto = new Presupuesto({
            ano : ctx.ano()
        });

        nPresupuesto.$isNew(true);

        ctx.editPresupuesto(nPresupuesto);
    };

    ctx.cancelarEdicion = function () {
        ctx.editPresupuesto(null);
    };

    ctx.cargarPresupuestos = function (force, presupuesto_id) {
        if((!force) && ctx.ano() == ano() ) return;

        ctx.presupuestos(null);
        ano(ctx.ano());
        ctx.presupuesto_id(null);

        m.request({
            method : 'GET',
            url : '/apiv2',
            data : {modelo:'presupuestos', ano:ctx.ano()},
            unwrapSuccess : f('data')
        })
        .then(ctx.presupuestos)
        .then(function (presupuestos) {
            if(presupuesto_id) {
                ctx.presupuesto_id(presupuesto_id);
            } else if(presupuestos && presupuestos.length) {
                ctx.presupuesto_id(presupuestos[0].presupuesto_id)
            }
        })
    }   


    ctx.cargarPresupuestoSeleccionado = function () {
        // Si ya fue cargado regresamos
        if(ctx.presupuesto_id() == presupuesto_id()) return;

        presupuesto_id( ctx.presupuesto_id() );
        ctx.presupuesto(null)

        //Si no hay id de presupuesto regrear
        if(!presupuesto_id()) return;

        cuentasContables.obtener()
            .then(obtenerPresupuesto)
            //.then(inicializarTabla)
    }


    ctx.getValue = function (key) {
        var partida;
        key = key.split(':')
        if(partida = ctx.partidas()[key[0]]) {
            return partida[key[1]] ? partida[key[1]] : zero;
        }
    }

    ctx.guardarPresupuesto = function () {
        Presupuesto.guardar(ctx.editPresupuesto())
            .then(afterSave)
    }

    function obtenerPresupuesto () {
        return Presupuesto.obtener(ctx.presupuesto_id())
                    .then(ctx.presupuesto)
    }


    function afterSave (p) {
        ctx.editPresupuesto(null);
        toastr.success('Presupuesto ' + f('nombre')(p) + ' Creado')
        ctx.cargarPresupuestos(true, f('presupuesto_id')(p));
    }

    function saveError(r) {
       toastr.error( r.status.message );
    }
    
}


function PresupView (ctx) {
    ctx.cargarPresupuestos();
    ctx.cargarPresupuestoSeleccionado();

    return m('div', [

        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.pull-right.btn-xs', {
            onclick : ctx.crearPresupuesto
        } ,'Agregar'),

        m('h3.column-title', 'Presupuestos'),

        SeleccionarPresupuesto(ctx),

        m('br'),

        MostrarTabla(ctx),

        MTModal.view()
    ]);
}





function MostrarTabla (ctx) {
    if( ! ctx.presupuesto_id() ) return '(Seleccionar Presupuesto para ver)';
    if( ! ctx.presupuesto() ) return oor.loading();

    return m.component(TablaMeses, {
        key : ctx.presupuesto_id(),
        presupuesto : ctx.presupuesto(),
        cuentasContables :cuentasContables 
    });
}



function SeleccionarPresupuesto (ctx) {
    if(ctx.editPresupuesto()) {
        return EditarPresupuesto(ctx);
    }

    return m('.row',[
        m('.col-xs-3', [
            m('.mt-inputer', [
                m('label', 'Año'),
                m('input[type=number]', {
                    value : ctx.ano(),
                    onchange : m.withAttr('value', ctx.ano)
                })
            ])
        ]),
        m('.col-xs-6', [
            m('.mt-inputer', [
                m('label', 'Presupuesto'),
                Selector(ctx)
            ])
        ])
    ]);
}


function Selector(ctx) {
    if(!ctx.presupuestos()) return oor.loading();

    return m('select', {
        value : ctx.presupuesto_id(),
        onchange : m.withAttr('value', ctx.presupuesto_id)
    },[
        ctx.presupuestos().map(function (pres) {
            return m('option', {
                value : f('presupuesto_id')(pres)
            }, f('nombre')(pres));
        })
    ]);
}

function EditarPresupuesto (ctx) {
    var presupuesto = ctx.editPresupuesto();

    return m('form.row', {onsubmit:submitForm(ctx)},[
        m('.col-xs-3', [
            m('.mt-inputer', [
                m('label', 'Año'),
                m('input[type=number]', {
                    value : presupuesto.ano()
                })
            ])
        ]),

        m('.col-xs-6', [
            m('.mt-inputer', [
                m('label', 'Presupuesto'),
                m('input[type=text]', {
                    config : function (e,i) { i || e.focus() },
                    value : presupuesto.nombre(),
                    onchange : m.withAttr('value', presupuesto.nombre)
                })
            ])
        ]),

        m('.col-xs-3', [
            m('button.btn.btn-sm.btn-success', 'Guardar'),
            m('button.btn.btn-sm.btn-default', {
                onclick : ctx.cancelarEdicion
            },'Cancelar')
        ])
    ]);
}


function submitForm (ctx) {
    return function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        ctx.guardarPresupuesto();
    }
}



var cuentasContables = m.prop();
cuentasContables.obtener = function () {
    if(!cuentasContables.req) {

        cuentasContables.req = m.request({
            method : 'GET',
            url : '/cuentas-contables/presupuestos',
            unwrapSuccess : f('data')
        })
        .then(cuentasContables)
    }
    return cuentasContables.req;
}