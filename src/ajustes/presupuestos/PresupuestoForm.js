    module.exports = {
    controller : Controller, 
    view : View,
    abrirModal : abrirModal
};

var Presupuesto = require('./PresupuestoModel');

function Controller (params) {
    
    var ctx = this;

    ctx.presupuesto = m.prop();


    ctx.guardar = function () {
        Presupuesto
            .guardar(ctx.presupuesto())
            .then(function (r) {
                if(ctx.$modal) {
                    ctx.$modal.close();
                }

                if(nh.isFunction(params.onsave)) {
                    params.onsave(r);
                }
            }, function (r) {
                if(nh.isFunction(params.onsaveError)) {
                    params.onsaveError(r)
                };
            })
    }

    inicializarPresupuesto();

    function inicializarPresupuesto () {
        var presup = new Presupuesto({ano : params.ano});
        presup.$isNew(true);
        ctx.presupuesto(presup);
    }

}


function View (ctx) {
    return m('div', [
        m('.mt-inputer.line', [
            m('label', 'Año Presupuesto'),
            m('input[type=number]', {
                oninput : m.withAttr('value', ctx.presupuesto().ano),
                value : ctx.presupuesto().ano()
            })
        ]),
        m('.mt-inputer.line', [
            m('label', 'Nombre Presupuesto'),
            m('input[type=text]', {
                oninput : m.withAttr('value', ctx.presupuesto().nombre),
                value : ctx.presupuesto().nombre()
            })
        ])
    ]);
}

function abrirModal (params) {

    MTModal.open({
        controller : Controller,
        args : params,
        content : View,
        top : function () {
            return m('h4', 'Crear Presupuesto')
        },

        bottom : function (ctx) {
            return [
                m('button.pull-right.btn.btn-flat.btn-sm.btn-success', {
                    onclick:ctx.guardar
                },'Guardar'),

                m('button.pull-left.btn.btn-flat.btn-sm.btn-default', {
                    onclick:ctx.$modal.close
                },'Cancelar')
            ];
        },
        modalAttrs : {
            'class' : 'modal-small'
        },
        el : this
    })
}