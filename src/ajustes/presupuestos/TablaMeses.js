

var Presupuesto = require('./PresupuestoModel')
var meses = m.prop([]);

meses.generar = function () {
    var nombreMes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    for(var i = 1; i<13 ; i++) {
        meses().push({ 
            nombre : 'mes_' + oor.fecha.to10(i),
            caption : nombreMes[i - 1]
        })
    }
    meses.generar = nh.noop;
}



module.exports = {
    controller : TMesesController,
    view : TMesesView
}


function TMesesController (args) {
    var ctx = this;
    var zero = '0.00';
    var nFormat = d3.format(",.2f");

    ctx.zero = m.prop(zero);
    ctx.presupuesto = m.prop(args.presupuesto);
    ctx.cuentasContables = args.cuentasContables;


    ctx.modificados = {};
    ctx.values = {};

    ctx.toUpdate = {};

    meses.generar();


    if(ctx.presupuesto() && ctx.presupuesto().partidas) {
        ctx.presupuesto().partidas.forEach(crearValores)
    }


    function crearValores (partida) {
        var cuentaId = partida.cuenta_contable_id;
        
        meses().forEach(function (mes) {
            var key = cuentaId + ':' + mes.nombre;
            ctx.values[key] = createValue(key, partida[mes.nombre] || zero);
        });
    }


    ctx.getValue = function (key) {
        if(ctx.values[key]) {
            return ctx.values[key]();
        }
    }


    ctx.setValue = function (key, val) {
        if(!ctx.values[key]) {
            ctx.values[key] = createValue(key,zero);
        }
    
        ctx.values[key](val);
    }

    ctx.guardarCambios = function () {
        var nPartidas = calcularPartidas(ctx.presupuesto().partidas, ctx.values, ctx.modificados);
        var presupuesto = new Presupuesto( ctx.presupuesto() );
        presupuesto.partidas = nPartidas;


        Presupuesto.guardar(presupuesto);
    }


    ctx.copiarValores = function (key, settings) {
        var keys = key.split(':');
        var val = ctx.values[key]();
        var mesIndex = Number( /\d+$/.exec(key)[0] );
        var mes;

    
        settings || (settings = {})

        function nextVal(v) {
            if(settings.aplicar == '%') {
                
            } else if(settings.aplicar = '+') {

            }
            return v;
        }
    
        for(var i=mesIndex; i<12; i++) {
            mes = meses()[i];
            val = nextVal(val);
            ctx.setValue(keys[0] + ':' + mes.nombre, val);
        }
    }

    

    function createValue (key, valor) {
        var prop = m.prop(validate(valor));
        val.key = m.prop(key);
        val.str = m.prop(unformatNumber(valor));
        ctx.toUpdate[val.key()] = true;

        return val;

        function val () {
            if(arguments.length) {
                ctx.modificados[val.key()] = true;
                ctx.toUpdate[val.key()] = true;
                console.log(ctx.toUpdate)
                prop(validate(arguments[0]));
                val.str( unformatNumber(prop()) );
            }

            return prop();
        }
    }


    function validate (aNumber) {
        var n = Number( unformatNumber(aNumber) );
        return nFormat(n);
    }

    function unformatNumber (aNumber) {
        return String(aNumber).split(',').join('')
    }
}


function TMesesView (ctx) {
    var modificados = Object.keys(ctx.modificados).length;

    return m('div', [
        m('button.pull-right.btn-success.btn.btn-sm', {
            'class' : modificados ? '' : 'disabled',
            onclick:ctx.guardarCambios
        }, 'Guardar Cambios'),

        m('.clear'),
        m('br'),
        m('.table-responsive', [ 
            m('table.table.megatable.arbol-cuentas', {config : tableConfig(ctx)}, [
                m('thead', [
                    m('tr', [
                        m('th', 'Cuenta'),
                        meses().map(function (d) {
                            return m('th.text-right', f('caption')(d) )
                        })
                    ])
                ]),
                CachedTable(ctx)
            ])
        ])
    ]);
}



function CachedTable (ctx) {
    if(!ctx.count) {
        ctx.count = m.prop(false)
    }

    if(ctx.count()) {
        console.log('subtree')
        return {subtree:'retain'}
    }

    ctx.count(true);

    return m('tbody', [
        ctx.cuentasContables().map(function (cuenta) {


            return m('tr', [
                m('td', {
                    style : 'max-width:180px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; padding-left: ' + f('nivel')(cuenta) * 12 + 'px'
                },[
                    m('div', f('cuenta')(cuenta)),
                    m('small.text-grey', f('nombre')(cuenta))
                ]),

                meses().map(function (mes) {
                    var key = bindAttr(cuenta,mes);

                    return m('td', [
                        cuenta.acumulativa == 0 ? m('input[type=text].text-right', {
                            'x-bind' : key,
                            'placeholder' : ctx.zero(),
                            size:10
                        }) : m('div.text-right', {
                            //'x-bind' : key
                        }),
                        cuenta.acumulativa == 0 ? m('a.pull-right.text-indigo.copiar-valores', {
                            style:'font-size:10px',
                            href : 'javascript:;',
                            tabindex : '-1',
                            'x-copiar-valores' : key
                        }, 'Copiar Valores >>') : null
                    ])
                })
            ])
        })
    ]);
}


function calcularPartidas(iPartidas, values, modificados) {
    var partidas = [];
    var partidasMap = {};

    (iPartidas || []).forEach(function (partida) {
        partidas.push(partida);
        partidasMap[partida.cuenta_contable_id] = partida;
    });


    Object.keys(modificados).forEach(function (mKey) {
        var keys = mKey.split(':');
        var mes = keys[1];
        var cuentaId = keys[0];
        var partida;

        if(!partidasMap[cuentaId]) {
            partidasMap[cuentaId] = {cuenta_contable_id : cuentaId}
            partidas.push(partidasMap[cuentaId])
        } 

        partida = partidasMap[cuentaId]
        partida[mes] = values[mKey].str()

    });

    return partidas;
}


function bindAttr(cuenta, mes) {
    return f('cuenta_contable_id')(cuenta) + ':' + f('nombre')(mes);
}


function tableConfig(ctx) {

    return function (el, isInited) {
        Object.keys(ctx.toUpdate).forEach(function (k) {
            var selector = '[x-bind="' + k + '"]';
            var input = $(selector, el);

           if(input.prop('tagName') == 'INPUT') {
                input.val(ctx.getValue(k))
            } else {
                input.text(ctx.getValue(k))
            }
            delete ctx.toUpdate[k];
        })
        
        if(isInited) return;

        $(el).on('change', '[x-bind]', function () {
            var input = $(this);
            ctx.setValue(input.attr('x-bind'), input.val());
            m.redraw()
        })

        $(el).on('click', '[x-copiar-valores]', function () {
            ctx.copiarValores($(this).attr('x-copiar-valores'));
            m.redraw();
        })
    }
}