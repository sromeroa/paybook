
module.exports = Categoria;

function Categoria (d) {
    d || (d = {});

    function bool (d) { return Boolean(Number(d)) }

    this.categoria_id = m.prop(d.categoria_id);

    this.categoria = m.prop(d.categoria || '');
    this.descripcion = m.prop(d.descripcion || '');


    // Venta
    this.mostrar_venta = m.prop(bool(d.mostrar_venta));
    this.impuesto_venta = m.prop(d.impuesto_venta || null);
    this.cuenta_venta = m.prop(d.cuenta_venta || null);
    this.cuenta_nc_venta = m.prop(d.cuenta_nc_venta || null);
    this.cuenta_costo_venta = m.prop(d.cuenta_costo_venta || null);

    // Compra
    this.mostrar_compra = m.prop(bool(d.mostrar_compra));
    this.impuesto_compra = m.prop(d.impuesto_compra || null);
    this.cuenta_compra = m.prop(d.cuenta_compra || null);
    this.cuenta_nc_compra = m.prop(d.cuenta_nc_compra || null);
    this.cuenta_costo_compra = m.prop(d.cuenta_costo_compra || null);

    //Inventario
    this.inventariable = m.prop(bool(d.inventariable));
    this.cuenta_inventario = m.prop(d.cuenta_inventario);

    this.$isNew = m.prop(false);

}


Categoria.guardar = function (cat) {
    var method = cat.$isNew() ? 'POST' : 'PUT';
    var url = 'apiv2/'.concat(cat.$isNew() ? 'agregar' : 'editar/' + cat.categoria_id());
    var search = '?modelo=categorias';

    return m.request({
        method : method,
        url : url + search,
        data : cat
    });
}


Categoria.id = f('categoria_id');

Categoria.eliminar = function (categoriaId) {

    return m.request({
        method : 'DELETE',
        url : '/apiv2/eliminar/' + categoriaId + '?modelo=categorias'
    })
}