module.exports = {
    controller : VariantesController, 
    view : VariantesView
};

var Variante = require('./VarianteModel');

var VarianteComponent = {};

var NuevaVarianteComponent = {
    controller : VarianteController,
    view : NuevaVarianteView
};

function VariantesController (params) {
    var ctx = this;

    ctx.producto = m.prop(params.producto);
    ctx.productoId = m.prop( params.producto.producto_id() );
    ctx.variantes = m.prop([]);

    ctx.agregarVariante = m.prop(false);

    ctx.onsave = function () {
        cargarVariantes();
        ctx.agregarVariante(false)
    };

    cargarVariantes();

    function cargarVariantes () {
        m.request({
            method:'GET', url:'/productos/variantes/'+ ctx.productoId(),
            unwrapSuccess : function (r) {
                return r.data;
            }
        }).then(ctx.variantes)
    }
}

function VariantesView (ctx) {
    return m('div',  [
       
        ctx.agregarVariante() ? m.component(NuevaVarianteComponent, {
            key : 'nuevo',
            producto : ctx.producto(),
            onsave : ctx.onsave,
            oncancel : ctx.agregarVariante.bind(ctx, false)
        }) : m('div', [
            m('button.btn.btn-sm.btn-success.pull-right', {
                onclick : function () { ctx.agregarVariante(true) }
            },'Agregar Variante'),
            m('.clearfix'),
            m('br')
        ]),
       
        m('.clearfix'),
    
        m('.table-responsive', [
            m('table.table', [
                m('thead', [
                    m('tr', [
                        m('th', ctx.producto().nombre_variante_1() ),
                        m('th', ctx.producto().nombre_variante_2() ),
                        m('th', 'Descripción')
                    ])
                ]),

                m('tbody', [
                    ctx.variantes().map(function (variante) {
                        return m('tr', [
                            m('td', f('variante_1')(variante) || '--'),
                            m('td', f('variante_2')(variante) || '--'),
                            m('td', f('descripcion')(variante) || '...' )
                        ])
                    })
                ])
            ])
        ])
    ]);
}

function VarianteController (params) {
    var ctx = this;

    ctx.producto = m.prop(params.producto);

    ctx.variante = m.prop();

    if(!params.variante) {
        ctx.variante(new Variante)
        ctx.variante().$isNew(true);
        ctx.variante().producto_id( ctx.producto().producto_id() );
    } else {
        ctx.variante( new Variante(params.variante) )
    }

    ctx.submit = function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        ctx.guardar();
    }

    ctx.guardar = function () {
        Variante.guardar( ctx.variante() ).then( params.onsave )
    }

    ctx.cancelar = function () {
        if(typeof params.oncancel === 'function') {
            params.oncancel();
        }
    }

    ctx.config = oor.xPropConfig(ctx);
}

function NuevaVarianteView (ctx) {
    return m('form', {onsubmit:ctx.submit, config:ctx.config}, [ 
        m('.flex-row', [
            
            m('.mt-inputer', [
                m('label',ctx.producto().nombre_variante_1()),
                m('input[type=text]', {
                    config : function (e,isInit) { isInit || e.focus() },
                    'x-prop' : 'variante_1',
                    'x-model' : 'variante'
                })  
            ]),

            m('.mt-inputer', [
                m('label',ctx.producto().nombre_variante_2()  ),
                m('input[type=text]', {
                    'x-prop' : 'variante_2',
                    'x-model' : 'variante'
                })
            ]),

            m('.mt-inputer', [
                m('label', 'Descripción'),
                m('input[type=text]', {
                    'x-prop' : 'descripcion',
                    'x-model' : 'variante'
                })
            ]),

            m('button.btn.btn-sm.btn-success.pull-right', {
                type : 'submit'
            }, 'Guardar'),

            m('button.btn.btn-sm.btn-default.pull-right', {
                onclick : ctx.cancelar
            }, 'Cancelar')
        ]),

        m('.clearfix')
    ])
}
