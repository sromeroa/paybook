module.exports = Producto;

function Producto (d) {
    d || (d = {});

    var producto = this;

    var fields = {
        'producto_id' : {},
        'categoria_id' : {},
        'codigo' : {},
        'nombre' : {},
        'unidad' : {},
        'clave_alterna' : {},
        'descripcion' : {},
        'moneda_venta' : {},
        'precio_unitario_venta' :{},
        'moneda_compra' : {},
        'precio_unitario_compra' : {},
        'nombre_variante_1' : {},
        'nombre_variante_2' : {},
        'peso' : {},
        'dimension_alto' : {},
        'dimension_ancho' : {},
        'dimension_largo' : {}
    };

    Object.keys(fields).forEach(function (k) {
        producto[k] = m.prop(f(k)(d) || null);
    });

    producto.usarVariantes = m.prop(false);

    if(producto.nombre_variante_1() || producto.nombre_variante_2()) {
        producto.usarVariantes(true);
    }

    producto.$isNew = m.prop(false);
}


Producto.guardar = function (cat) {
    var method = cat.$isNew() ? 'POST' : 'PUT';
    var url = 'apiv2/'.concat(cat.$isNew() ? 'agregar' : 'editar/' + cat.producto_id());
    var search = '?modelo=productos';

    return m.request({
        method : method,
        url : url + search,
        data : cat
    });
}


Producto.findOne = function (id) {
    return m.request({
        url : '/apiv2',
        data : {
            modelo : 'productos',
            producto_id : id
        },
        unwrapSuccess : function (r) {
            return r.data[0]
        }
    });
}