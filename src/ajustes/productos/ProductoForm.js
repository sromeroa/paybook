/**
 * ProductoForm
 */

var ProductoForm = module.exports = {};
var Producto = require('./ProductoModel');
var ProductoVariantes = require('./ProductoVariantes');

ProductoForm.controller = function (params) {
    var ctx = this;

    ctx.tab = m.prop(ProductoForm.tabs()[0]);

    ctx.categoria = m.prop();
    ctx.categoria(params.categoria || {});

    ctx.categorias = m.prop();
    ctx.categorias(params.categorias);

    ctx.producto = m.prop();
    ctx.producto( new Producto(params.producto) );
    ctx.usarVariantes = m.prop( ctx.producto().usarVariantes() ); 

    if(!params.producto) {
        ctx.producto().categoria_id( f('categoria_id')(ctx.categoria()) );
        ctx.producto().producto_id(oor.uuid());
        ctx.producto().$isNew(true);
    }

    ctx.mostrarVenta = function () {
        var mostrar_venta = f('mostrar_venta')(ctx.categoria());
        if(mostrar_venta) {
            return Boolean(Number(mostrar_venta));
        }
        return false;
    }

    ctx.mostrarCompra = function () {
        var mostrar_venta = f('mostrar_compra')(ctx.categoria());
        if(mostrar_venta) {
            return Boolean(Number(mostrar_venta));
        }
        return false;
    }

    function categoriasAdapter (c) {
        return {
            id : f('categoria_id')(c),
            text : f('categoria')(c)
        }
    }


    ctx.guardar = function () {
        return Producto.guardar(ctx.producto())
                    .then(params.onsave)
    }

    ctx.catSelector = oor.select2({
        data : ctx.categorias().map(categoriasAdapter),
        model : function () {
            if(arguments.length) {
                ctx.producto().categoria_id(arguments[0]);
            }
            return ctx.producto().categoria_id();
        },
        find : function (d) {
            return ctx.categorias().filter(function (c) {
                return f('categoria_id')(c) == d;
            })[0]
        },
        onchange: function () {
            ctx.categoria(ctx.catSelector.selected());
        }
    });


    oorden().then(function () {
        var monedaBase = oorden.tipoDeCambio.base().codigo_moneda;

        console.log( ctx.producto().moneda_venta() )

        if(ctx.producto().$isNew()) {
            ctx.producto().moneda_compra(monedaBase);
            ctx.producto().moneda_venta(monedaBase);
        }

        oorden.paisesYMonedas().then(function () {
            var monedasDisponibles = oorden.monedasDisponibles();

            ctx.monedaVentaSelector = oor.select2({
                data : monedasDisponibles,
                model : function () {
                    if(arguments.length) {
                        ctx.producto().moneda_venta(arguments[0]);
                    }
                    return ctx.producto().moneda_venta()
                }
            });

            ctx.monedaCompraSelector = oor.select2({
                data : monedasDisponibles,
                model : function () {
                    if(arguments.length) {
                        ctx.producto().moneda_compra(arguments[0]);
                    }
                    return ctx.producto().moneda_compra()
                }
            });
        });
    });
        
    
    



}


ProductoForm.view = function (ctx) {
    return m('div', [
        /*m('button.btn.btn-sm.btn-flat.btn-warning.pull-right', {
            style: 'margin:5px',
            onclick : ctx.eliminar
        }, m('.ion-trash-a')),*/
        m('ul.nav.nav-tabs', [
            ProductoForm.tabs().map(function (tab) {
                if(!tab.show(ctx)) return null;

                return m('li', {'class' : tab == ctx.tab() ? 'active' : ''}, [
                    m('a[href=javascript:;]', {
                        onclick : function () {
                            ctx.tab(tab);
                        }
                    }, tab.caption)
                ]);
            })
        ]),
        
        m('div', {style:'padding:10px; border:1px solid #f0f0f0', key : ctx.tab() ? ctx.tab().name : null}, [
            ctx.tab() ? ctx.tab().view(ctx) : null
        ])
    ])
}

ProductoForm.tabs = m.prop([
    {
        name : 'producto',
        caption : 'Producto',
        view : TabProductoView,
        show : function (ctx) {
            return true;
        }
    }, 
    {
        name : 'variantes',
        caption : 'Variantes',
        view : TabVariantesView,
        show : function (ctx) {
           return ctx.usarVariantes()
        }
    }
]);

ProductoForm.inputer = function (ctx, label, model, prop, input) {
    input = input ? input : 'input[type="text"]';

    return m('.mt-inputer.line.fprod.prod-'.concat(prop), [
        m('label', label),
        m(input, {
            'x-model' : model,
            'x-prop' : prop,
            'value' : ctx[model]()[prop]()
        })
    ])
}

ProductoForm.config = function (ctx) {
    var redraw = _.debounce(m.redraw, 500);

    return function (element, isInitialized) {
        if(isInitialized) return;

        $(element).on('input', '[x-prop]', function () {
            var input = $(this);
            var model = f(input.attr('x-model'))(ctx);

            model[input.attr('x-prop')](input.val())

            redraw();
        })
    }
}


function TabProductoView (ctx) {
    return m('div.producto-form', {config:ProductoForm.config(ctx)}, [
        m('div', [
            m('.flex-row', [
                m('.mt-inputer.line', [
                    m('label', 'Categoría'),
                    m('div', ctx.catSelector.view())
                ]),
                ProductoForm.inputer(ctx, 'Producto', 'producto', 'nombre')
            ]),
            m('.flex-row', [
                ProductoForm.inputer(ctx, 'Código', 'producto', 'codigo'),
                ProductoForm.inputer(ctx, 'Clave Alterna', 'producto', 'clave_alterna'),
                ProductoForm.inputer(ctx, 'Unidad', 'producto', 'unidad')
            ]),
            m('.flex-row', [
                ProductoForm.inputer(ctx, 'Descripción', 'producto', 'descripcion')
            ]),

                
        ]),

        m('.row', [
            m('.col-sm-4', [
                m('h5', [ 
                    'Venta ', ctx.mostrarVenta() ? '' : '(no se muestra en la categoría)'
                ]),
                ctx.mostrarVenta() ? [
                    m('.flex-row', [
                        m('.mt-inputer.line', [
                            m('label', 'Moneda Venta'),
                            ctx.monedaVentaSelector.view()
                        ])
                    ]),
                    m('.flex-row', [
                        ProductoForm.inputer(ctx, 'Precio Unitario Venta', 'producto', 'precio_unitario_venta')
                    ])
                ] : null
            ]),

            m('.col-sm-4', [
                m('h5', [
                    'Compra ', ctx.mostrarCompra() ? '' : '(no se muestra en la categoría)'
                ]),

                ctx.mostrarCompra() ? [
                    m('.flex-row', [
                        m('.mt-inputer.line', [
                            m('label', 'Moneda Compra'),
                            ctx.monedaCompraSelector.view()
                        ])
                    ]),
                    m('.flex-row',[
                        ProductoForm.inputer(ctx, 'Precio Unitario Compra', 'producto', 'precio_unitario_compra')
                    ])
                ] : null
            ]),

            m('.col-sm-4', [
                m('div', [
                    checkboxer('Usar Variantes', 'prod-variantes', ctx.producto().usarVariantes)
                ]),
                m('div', [
                    ctx.producto().usarVariantes() ? m('.flex-row', [
                        ProductoForm.inputer(ctx, 'Nombre Variante 1', 'producto', 'nombre_variante_1'),
                        ProductoForm.inputer(ctx, 'Nombre Variante 2', 'producto', 'nombre_variante_2')
                    ]) : null
                ])
            ])
        ]),

        m('br')
    ]);
}

function TabVariantesView (ctx) {
    return m.component(ProductoVariantes, {
        producto : ctx.producto()
    })
}

function checkboxer (label, id, fn) {
    return m('.checkboxer.checkboxer-indigo', [
        m('input[type="checkbox"]', {
            id : id,
            checked : fn(),
            onchange : m.withAttr('checked', fn)
        }),
        m('label', {'for':id}, label)
    ])
}


