
var CategoriaForm = module.exports = {};
var Categoria = require('./CategoriaModel');

/**
 * CATEGORIAS
 *
 */
CategoriaForm.controller = function (params) {
    var ctx = this;

    ctx.categoria = m.prop();

    ctx.categoria( new Categoria(params.categoria) );

    ctx.tab = m.prop( CategoriaForm.tabs()[0] );

    if(!params.categoria) {
        ctx.categoria().categoria_id( oor.uuid() );
        ctx.categoria().$isNew(true);
    }

    function enabledCompra () {
        return ctx.categoria() && ctx.categoria().mostrar_compra()
    }

    function enabledVenta () {
        return ctx.categoria() && ctx.categoria().mostrar_venta()
    }

    function enabledInventario () {
        return ctx.categoria() && ctx.categoria().inventariable();
    }

    /**
     * View Model Part
     */
    ctx.compra = {
        impuesto : oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : ctx.categoria().impuesto_compra,
            enabled : enabledCompra
        }),

        cuenta: oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctx.categoria().cuenta_compra,
            enabled : enabledCompra
        }),

        cuentaNC : oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctx.categoria().cuenta_nc_compra,
            enabled : enabledCompra
        }),

        cuentaCosto : oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctx.categoria().cuenta_costo_compra,
            enabled : enabledCompra
        })
    };

    ctx.venta = {
        impuesto : oor.impuestosSelect2({
            impuestos : oorden.impuestos(),
            model : ctx.categoria().impuesto_venta,
            enabled : enabledVenta
        }),
        cuenta: oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctx.categoria().cuenta_venta,
            enabled : enabledVenta
        }),
        cuentaNC : oor.cuentasSelect2({
            cuentas : oorden.cuentas(),
            model : ctx.categoria().cuenta_nc_venta,
            enabled : enabledVenta
        })
    };


    ctx.cuentaInventario = oor.cuentasSelect2({
        cuentas : oorden.cuentas(),
        model : ctx.categoria().cuenta_inventario,
        enabled : enabledInventario
    });


    ctx.cuentaCostoVenta = oor.cuentasSelect2({
        cuentas : oorden.cuentas(),
        model : ctx.categoria().cuenta_costo_venta,
        enabled : enabledInventario
    });


    ctx.guardar = function () {
        return Categoria.guardar(ctx.categoria())
                    .then(function (r) {
                        ctx.categoria().categoria_id( r.data.categoria_id );
                        if(params.onsave) {
                            params.onsave(ctx.categoria())
                        }
                        return r;
                    })
    };


    ctx.eliminarCategoria = function () {
        var confirmar = confirm('¿Deseas Eliminar esta Categoría?');
        if(!confirmar) return;

        Categoria.eliminar( Categoria.id(ctx.categoria()) )
            .then(function () {
                toastr.success('Categoría Eliminada');
                if(nh.isFunction(params.ondelete)) params.ondelete(ctx.categoria());
                ctx.$modal && ctx.$modal.close();
            }, function (r) {
                toastr.error(r.status.message)
            })
    }


    ctx.validar = function () {
        var errores = [];

        if(ctx.categoria().mostrar_venta()) {
            if(!ctx.categoria().cuenta_venta()) {
                
            }
        }

        return errores;
    }
}

CategoriaForm.tabs = m.prop([
    {
        name : 'categoria',
        caption : 'Categoria',
        view : TabCategoriaView
    },
    {
        name : 'venta',
        caption : 'Venta',
        view : TabVentaView
    },
    {
        name : 'compra',
        caption : 'Compra',
        view : TabCompraView
    },
    {
        name : 'inventario',
        caption : 'Inventario',
        view : TabInventarioView
    }
]);

CategoriaForm.view = function (ctx) {
    var cat = ctx.categoria();

    return m('div', [
        m('button.btn.btn-flat.btn-sm.pull-right', {
            style:'margin-top:5px',
            onclick : ctx.eliminarCategoria
        }, m('i.fa.fa-trash.text-grey')),
        m('ul.nav.nav-tabs', [
            CategoriaForm.tabs().map(function (tab) {
                return m('li', {'class' : tab == ctx.tab() ? 'active' : ''}, [
                    m('a[href=javascript:;]', {
                        onclick : function () {
                            ctx.tab(tab);
                        }
                    }, tab.caption)
                ]);
            })
        ]),
        
        m('div', {style:'padding:10px; border:1px solid #f0f0f0', key : ctx.tab() ? ctx.tab().name : null}, [
            ctx.tab() ? ctx.tab().view(ctx) : null
        ])
    ])
}

function TabVentaView (ctx) {
    var cat = ctx.categoria();

    return [
        m('div', [
            radioer('¿Mostrar Venta?', 'mostrar-venta2', cat.mostrar_venta)
        ]),
         m('.row', [
            m('div.col-sm-6', [
                m('label', 'Cuenta Venta'),
                m('div.cuentas-selector', ctx.venta.cuenta.view() )
            ]),

            m('div.col-sm-6', [
                m('label', 'Cuenta NC Venta'),
                m('div.cuentas-selector', ctx.venta.cuentaNC.view() )
            ])
        ]),

        m('.row', [
            /*
            
            */
            m('div.col-sm-6',[
                m('label', 'Impuesto Venta:'),
                ctx.venta.impuesto.view()
            ])

        ])
    ];
}

function TabCompraView (ctx) {
    var cat = ctx.categoria();

    return [
        m('div', [
            radioer('¿Mostrar Compra?', 'mostrar-compra2', cat.mostrar_compra)
        ]),
        m('.row', [
            m('div.col-sm-6', [
                m('label', 'Cuenta Compra'),
                m('div.cuentas-selector', ctx.compra.cuenta.view() )
            ]),

            m('div.col-sm-6', [
                m('label', 'Cuenta NC Compra'),
                m('div.cuentas-selector', ctx.compra.cuentaNC.view() )
            ]),
        ]),

        m('.row', [
            /*
            m('div.col-sm-6', [
                m('label', 'Cuenta Costo Compra'),
                m('div.cuentas-selector', ctx.compra.cuentaCosto.view() )
            ]),
            */
            m('div.col-sm-6', [
                m('label', 'Impuesto Compra:'),
                m('br'),
                ctx.compra.impuesto.view()
            ])
        ])
    ];
}

function TabInventarioView (ctx) {
    var cat = ctx.categoria();
    return [
        m('div', [
            radioer('¿Inventario?', 'inventariable', cat.inventariable)
        ]),

        m('.row', [
            m('.col-sm-6', [
                m('label', 'Cuenta Inventario'),
                m('.cuentas-selector', ctx.cuentaInventario.view())
            ]),
            
            m('div.col-sm-6', [
                m('label', 'Cuenta Costo Venta'),
                m('div.cuentas-selector', ctx.cuentaCostoVenta.view() )
            ]),
        ])     
    ];
}


function TabCategoriaView (ctx) {
    var cat = ctx.categoria();

    return [
        m('.flex-row', [
            m('.mt-inputer.line', [
                m('label', 'Categoria'),
                m('input[type="text"]', {
                    value : cat.categoria(),
                    oninput : m.withAttr('value', cat.categoria)
                })
            ]),
            m('.mt-inputer.line', [
                m('label', 'Descripción'),
                m('input[type="text"]', {
                    value : cat.descripcion(),
                    oninput : m.withAttr('value', cat.descripcion)
                })
            ])
        ]),

        m('br'),

        m('.row', {style:'font-size:14px'}, [
            m('.col-sm-6', [
                radioer('¿Mostrar Venta?', 'mostrar-venta', cat.mostrar_venta)
            ]),

            m('.col-sm-6', [
                radioer('¿Mostrar Compra?', 'mostrar-compra', cat.mostrar_compra)
            ]),

            m('.col-sm-6', [
                radioer('¿Inventariable?', 'inventariable', cat.inventariable)
            ])
        ])
    ];
}

function checkboxer (label, id, fn) {
    return m('.checkboxer.checkboxer-indigo', [
        m('input[type="checkbox"]', {
            id : id,
            checked : fn(),
            onchange : m.withAttr('checked', fn)
        }),
        m('label', {'for':id}, label)
    ])
}

function radioer (label, id, fn) {
    return [
        m('div', label),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'si' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(checked)
                }),
                checked : fn()
            }),
            m('label', {'for':id + 'si'}, 'Sí')
        ]),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'no' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(!checked)
                }),
                checked : !fn()
            }),
            m('label', {'for': id + 'no'}, 'No')
        ])
    ];
}

