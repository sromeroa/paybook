module.exports = Variante;


function Variante (data) {
    data || (data = {})

    this.producto_variante_id  = m.prop( f('producto_variante_id')(data) );
    this.producto_id = m.prop( f('producto_id')(data) );
    this.variante_1 = m.prop( f('variante_1')(data) );
    this.variante_2 = m.prop( f('variante_2')(data) );
    this.descripcion = m.prop( f('descripcion')(data) );

    this.$isNew = m.prop(false);
}


Variante.guardar = function (variante) {
    var method, url;

    if(variante.$isNew()) {
        url = '/productos/crearvariante'
        method = 'POST'
    } else {

    }

    return m.request({
        url:url, method:method, data:variante
    });
}