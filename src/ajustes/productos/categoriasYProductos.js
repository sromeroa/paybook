module.exports = {
    controller : ProductosController,
    view : ProductosView
};

var dynTable = require('../../nahui/dtable');
var TableHelper = require('../../nahui/dtableHelper');
var nahuiCollection = require('../../nahui/nahui.collection');
var Field = require('../../nahui/nahui.field');
var CategoriaForm = require('./CategoriaForm');
var ProductoForm = require('./ProductoForm');
var Producto = require('./ProductoModel');
var SearchBar = require('../../misc/components/SearchBar');


function ProductosController (params) {
    var ctx = this;

    var table, coll, query, thelper;
    var fields = ProductosFields();
    var searchBar;

    ctx.categoria = m.prop();

    ctx.columnas = m.prop([
        fields.nombreYCodigo,
        fields.precioCompra,
        fields.precioVenta,
    ]);

    table = dynTable().key( f('producto_id') );

    coll = nahuiCollection('productos', { identifier : 'producto_id'});

    query = coll.query({});

    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 10 });


    ctx.categorias = m.prop(params.categorias);
    
    agregarCategoriaTodas();

    ctx.seleccionarCategoria = function (cat) {
        if(typeof cat == 'string' || String(cat) == 'null') {
            cat = ctx.categorias().filter(function (c) {
                console.log(c.categoria_id, cat);
                return c.categoria_id == cat;
            })[0];
        }

        ctx.categoria(cat);

        query.add({
            categoria_id : cat.categoria_id || undefined
        }).exec();
    };


    ctx.search = m.prop('');

    searchBar = new SearchBar.controller({
        search : ctx.search,
        onsearch : function () {
            var search = ctx.search() ? {$contains:ctx.search()} : undefined;
            query.add({search : search}).exec()
        }
    });

    ctx.searchBar = searchBar;

    ctx.seleccionarCategoria( ctx.categorias()[0] );

    ctx.setupTable = function (element, isInitialized) {
        if(isInitialized)return;

        thelper.element(element);

        $(element).on('click', '.celda-nombre', function (ev) {
            var id = $(this).attr('href').split('/productos/ver/').join('');
            ctx.abrirProducto.call(this, id);
            return false;
        });
    };


    ctx.agregarCategoria = function () {
        abrirModalCategoria.call(this, ctx, {});
    };


    ctx.abrirCategoria = function (categoria) {
        abrirModalCategoria.call(this, ctx, {
            categoria : categoria
        });
    };

    ctx.abrirProducto = function (producto_id) {
        var $this = this;

        Producto.findOne(producto_id).then(function (producto) {

            var categoria = ctx.categorias().filter(function (cat) {
                return f('categoria_id')(cat) == f('categoria_id')(producto);
            })[0];

            abrirModalProducto.call($this, ctx, {
                categorias : ctx.categorias(),
                categoria : categoria,
                producto : producto
            });
        });
    }

    ctx.agregarProducto = function (producto) {
        abrirModalProducto.call(this, ctx, {
            categorias : ctx.categorias(),
            categoria : ctx.categoria(),
        })
    }


    ctx.actualizarCategorias = function () {
        return m.request({
            url :'/apiv2',
            data : {
                modelo : 'categorias'
            },
            unwrapSuccess : function (d) {
                return d.data;
            }
        })
        .then(ctx.categorias)
        .then(agregarCategoriaTodas)
    };

    ctx.actualizarProductos = function () {
        coll.removeAll();
        m.request({
            url : '/apiv2',
            data : {
                modelo : 'productos'
            }
        }).then(function (r) {
            r.data.map(function (producto) {
                producto.search = producto.nombre + ' ' + producto.codigo;
                coll.insert(producto)
            });
            query.exec();
        })
    }

    function agregarCategoriaTodas () {
        ctx.categorias().unshift({
            categoria  : 'Todas',
            title : 'Todas las categorías',
            descripcion : 'Mostrar todas las categorías',
            categoria_id : null
        });
    }

    ctx.actualizarProductos();
};

function ProductosView (ctx) {
    return m('.row', [
        m('.col-md-3', ViewCategorias(ctx)),
        m('.col-md-9', ViewProductos(ctx))
    ])
}

function ViewCategorias (ctx) {
    return [
        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs.pull-right', {
            onclick : ctx.agregarCategoria
        }, 'Nueva Categoría'),

        m('h3.column-title', 'Categorías'),

        m('div', [
            ctx.categorias().map(function (cat) {
                return m('div..categoria-elem',  {
                    'class' : cat == ctx.categoria() ? 'active' : '',
                    'id': cat.categoria_id,
                    key : cat.categoria_id,
                    onclick:ctx.seleccionarCategoria.bind(ctx,cat)
                }, [
                    m('span.text-indigo', cat.categoria),
                    m('div', m('small', cat.descripcion))
                ])
            })
        ])

    ]
}

function ViewProductos (ctx) {
    var categoria = ctx.categoria();

    return [
        m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs.pull-right', {
            onclick : ctx.agregarProducto
        }, 'Nuevo Producto'),
        m('h3.column-title', 'Productos'),

        m('.row', [
            m('.col-sm-8', [
                m('h5', categoria.title || ['Categoría: ',
                    m('a.text-indigo', {
                        href : 'javascript:;',
                        onclick : function () { ctx.abrirCategoria.call(this, ctx.categoria()) }
                    },  categoria.categoria)
                ]),
            ]),

            m('.col-sm-4', [
                 SearchBar.view(ctx.searchBar)
            ])
        ]),
    
        m('table.table', {config:ctx.setupTable}),
        MTModal.view()
    ]
}


function ProductosFields () {
    var fields = {};

    fields.producto_id = Field('producto_id');

    fields.codigo = Field('codigo', {
        caption : 'Código'
    });

    fields.nombre = Field.linkToResource('nombre', {
        caption : 'Nombre',
        class : 'text-indigo',
        url : function (d) { return '/productos/ver/' + f('producto_id')(d); }
    });

    fields.compra = Field('precio_unitario_compra', {
        caption : 'Compra',
        class:'text-right'
    });

    fields.venta = Field('precio_unitario_venta', {
        caption : 'Venta',
        class : 'text-right'
    });

    fields.monedaCompra = Field('moneda_compra', {
        class : 'text-right text-grey',
        caption : 'Moneda'
    });

    fields.monedaVenta = Field('moneda_venta', {
        class : 'text-right text-grey',
        caption : 'Moneda'
    });

    fields.precioCompra = Field.twice('precioCompra', {
        subfields : [fields.compra, fields.monedaCompra]
    });

    fields.precioVenta = Field.twice('precioVenta', {
        subfields : [fields.venta, fields.monedaVenta]
    });

    fields.nombreYCodigo = Field.twice('nombreYCodigo', {
        subfields : [fields.nombre, fields.codigo]
    });

    return fields;

}


function abrirModalCategoria (ctx, params) {

    MTModal.open({
        controller : CategoriaForm.controller,
        args : {
            categoria : params.categoria,
            onsave : function (d) {
                toastr.success('Categoría Guardada');
                
                ctx.actualizarCategorias()
                    .then(ctx.seleccionarCategoria.bind(null, d.categoria_id() ))

            },
            ondelete : function (d) {
                 ctx.actualizarCategorias()
                    .then(ctx.seleccionarCategoria.bind(null,null));
            }
        },
        top : m('h4', params.categoria ? f('categoria')(params.categoria) : 'Agregar Categoria'),
        content : CategoriaForm.view,
        bottom : function (ctx) {
            return [
                m('button.pull-right.btn.btn-sm.btn-success.btn-flat', {
                    onclick : function () {
                        ctx.guardar().then(ctx.$modal.close)
                    }
                }, 'Guardar'),
                m('button.pull-left.btn.btn-sm.btn-default.btn-flat', {
                    onclick : function () {
                        ctx.$modal.close()
                    }
                }, 'Cancelar')
            ]
        },
         modalAttrs : {
            'class' : 'modal-small'
        },
        el : this
    });
}



function abrirModalProducto (ctx, params) {
    MTModal.open({
        controller : ProductoForm.controller,
        args : {
            categoria : params.categoria,
            categorias : params.categorias,
            producto : params.producto,
            onsave : function (d) {
                ctx.actualizarProductos()
            }
        },
        top : function (ctx) {
            return m('h3', 'Agregar Producto')
        },
        content : ProductoForm.view,
        bottom : function (ctx) {
            return [
                m('button.btn.btn-flat.btn-sm.btn-success.pull-right', {
                    onclick : function () {
                        ctx.guardar().then(ctx.$modal.close)
                    }
                },'Guardar'),

                m('button.btn.btn-flat.btn-sm.btn-default.pull-left',{
                    onclick : ctx.$modal.close
                }, 'Cancelar'),
            ]
        },
        el : this
    })
}