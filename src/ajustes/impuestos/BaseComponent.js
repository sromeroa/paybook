module.exports = {
    controller : ComponentController,
    view : ComponentView
};

var ConjuntoComponent = {
    controller : ConjuntoController,
    view : ConjuntoView
};

var Tabs = [
    {caption : 'Activos',   filter:f('estatus').is(1)   },
    {caption : 'Inactivos', filter:f('estatus').is(0)   }
];


var EditorComponent = {
    controller : EditorComponentController,
    view : EditorComponentView
};



function ComponentController (params) {
    var ctx = this;
    var ConjuntoModel = params.conjuntoModel;

    ctx.ready = m.prop(false);
    ctx.items = m.prop();
    ctx.items.cargando = m.prop();
    ctx.getNombreConjunto = params.getNombreConjunto;
    ctx.getIdConjunto = params.getIdConjunto;
    ctx.format = params.format;
    ctx.idSeleccionado = m.prop()
    ctx.verConjunto = m.prop(null);


    ctx.tab = m.prop(Tabs[0])

    ctx.crear = function () {
        toastr.warning('No Disponible');
    }

    ctx.abrirConjunto = function (item) {
        var id = params.getIdConjunto(item);
        var cparams, component;

        if( ctx.idSeleccionado() != id) {
            ctx.idSeleccionado(id);

            cparams = nh.extend({}, params, {
                key : ctx.idSeleccionado(),
                id : ctx.idSeleccionado(),
                afterSave : function () {
                    ctx.items(false);
                }
            });

            component = m.component(ConjuntoComponent,cparams);
            ctx.verConjunto(component)
        }
    }

    ctx.initialize = function () {
        if(ctx.items() || ctx.items.cargando()) {
            return;
        }

        ctx.items.cargando(true);

        ConjuntoModel.find()
            //.then(cargarUsos)
            .then(ctx.items)
            .then(calcularTasas)
            .then(ctx.items.cargando.bind(null,false))
            .then(ctx.ready.bind(null,true))
            //.then(ctx.selectTab)
    }


    function calcularTasas () {
        ctx.items().forEach(function (d) {
            d.tasa = ConjuntoModel.calcularTasa(d);
        });
    }


    function cargarUsos () {

    }

}

function selectTab (ctx,tab) {
    return function () {
        ctx.tab(tab)
    }
}

function ComponentView (ctx, params) {
    ctx.initialize();

    return m('div', [
        oor.stripedButton('button.btn-success.pull-right', params.nuevoLabel, {
            onclick : ctx.crear
        }),

        m('h4', params.titulo),

        ctx.ready() ? m('.row', [
            m('.col-sm-5', [

                ctx.items.cargando() ? oor.loading() : m('ul.impuestos-list', [
                    Tabs.map(function (tab) {
                        return [
                            m('li.impuestos-li', m('h5', {style:'margin:10px 0 5px 0'}, tab.caption)),

                            ctx.items().filter(tab.filter).map(function (item) {
                                var id = ctx.getIdConjunto(item);
                                var klass = id == ctx.idSeleccionado() ? 'Highlight' : '';

                                return m('li.impuestos-li',
                                    m('h6', {
                                        'class' : klass,
                                        style:'cursor:pointer; font-size:14px !important; margin:0 0 5px 10px',
                                        onclick : function () { ctx.abrirConjunto(item) }
                                    }, [
                                        ctx.getNombreConjunto(item),
                                        m('span.pull-right', ctx.format(item.tasa), '%')
                                    ])
                                );
                            })
                        ]
                    })
                ])

            ]),

            m('.col-sm-7', {style:'padding:0 20px'},[
                 ctx.verConjunto() || null
            ])

        ]) : oor.loading()
    ])
}



function TablaConjuntos (ctx) {
    return m('table.table.megatable', [
        m('tbody', [
            ctx.items().filter(ctx.tab().filter).map(function (item) {
                var id = ctx.getIdConjunto(item);
                var klass = id == ctx.idSeleccionado() ? 'Highlight' : '';

                return m('tr', {'class':klass},[
                    m('td', [
                        m('a.text-indigo[href=javascript:;]', {
                            onclick : function () { ctx.abrirConjunto(item) }
                        },ctx.getNombreConjunto(item))
                    ]),
                    m('td.text-right', ctx.format(item.tasa), '%')
                ])
            })
        ])
    ])
}


function CompuestoLabel (d) {
    return Boolean(Number(d)) ? 'Compuesto' : 'Simple';
}

function SeccionLabel (d) {
    if(d == 'C') return 'Compras';
    if(d == 'V') return 'Ventas';
    return 'Ambas'
}


function EstatusLabel (d) {
    return Boolean(Number(d)) ? 'Activo' : 'Inactivo';
}

function EstatusBtnLabel (d) {
    return Boolean(Number(d)) ? 'Descativar' : 'Activar';
}

function ConjuntoController (params) {
    var ctx = this;

    ctx.conjunto = m.prop();
    ctx.conjunto.cargando = m.prop();
    ctx.showAlert = m.prop(2);
    ctx.cambiandoEstatus = m.prop(false);
    ctx.getIdConjunto = params.getIdConjunto;
    ctx.afterSave = params.afterSave;
    ctx.endpoint = params.endpoint;

    ctx.cargarConjunto = function () {
        if(ctx.conjunto() || ctx.conjunto.cargando()) {
            return
        }

        ctx.conjunto.cargando(true)
            oorden().then(function () {
                return  params.conjuntoModel.get( params.id )
            })
            .then(function (c) {
                var comp = new params.conjuntoModel(c);
                comp.tasa = m.prop(params.conjuntoModel.calcularTasa(comp));
                return comp;
            })
            .then(ctx.conjunto)
            .then(ctx.conjunto.cargando.bind(null,false))
    }

    ctx.guardar = function () {

        var cjto = JSON.parse(JSON.stringify(ctx.conjunto()));
        delete cjto.componentes;

        return params.conjuntoModel.guardar(cjto)
                .then(
                    function () { toastr.success('Cambios Guardados'); },
                    function (r) { toastr.error(r.estatus.message);  }
                )
    }

    ctx.guardarComponente = function (componente) {
        return params.componenteModel.guardar(componente)
    }
}


function cambiarEstatus (ctx) {
    return function () {
        if(ctx.cambiandoEstatus()) return;
        if(!confirm('¿Desea Cambiar estatus?')) return;
        ctx.cambiandoEstatus(true);

        var action = Number(ctx.conjunto().estatus()) ? 'desactivar' : 'activar';
        var id = ctx.getIdConjunto(ctx.conjunto());

        oor.request(ctx.endpoint.concat('/', action, '/', id))
            .then(function () { toastr.success('Cambios Realizados') })
            .then(function () {
                ctx.conjunto(false);
                ctx.cambiandoEstatus(false);
            })
            .then(ctx.afterSave)
    }
}

function ConjuntoView (ctx, params) {
    ctx.cargarConjunto();

    var conjunto = ctx.conjunto();

    return ctx.conjunto.cargando() ? oor.loading() : m('div',[
        m('h5', params.getNombreConjunto(ctx.conjunto())),
        m('table.megatable.table', [
            m('tbody', [
                m('tr', [
                    m('td.text-right.text-grey', 'Nombre:'),
                    m('td[colspan=3]', [
                        params.getNombreConjunto(ctx.conjunto())
                    ])
                ]),

                m('tr', [
                    m('td.text-right.text-grey', 'Estatus:'),
                    m('td[colspan=2]', [
                        EstatusLabel(ctx.conjunto().estatus()), ' '
                    ]),
                    m('td', [
                        ctx.cambiandoEstatus() ? null : oor.stripedButton('.btn-primary.pull-right', EstatusBtnLabel(ctx.conjunto().estatus()), {
                            onclick : cambiarEstatus(ctx),
                            style : 'zoom:0.7'
                        })
                    ])
                ]),

                m('tr', [
                    m('td.text-right.text-grey', 'Mostrar En:'),
                    m('td',
                        m.component(EditorComponent, {
                            apply : function (v) {
                                ctx.conjunto().seccion(v);
                                this.busy(true);
                                return ctx.guardar()
                                        .then(this.busy.bind(this,false));
                            },

                            initialize : function (model) {
                                model( ctx.conjunto().seccion() )
                            },

                            options : [
                                {value:'A', caption:'Ambas'},
                                {value:'V', caption:'Ventas'},
                                {value:'C', caption:'Compras'}
                            ]
                        }, SeccionLabel(ctx.conjunto().seccion()))
                    )
                ]),

                m('tr', [
                    m('td.text-right.text-grey', 'Usos:'),
                    m('td', 0),
                    m('td.text-right.text-grey', 'Tasa Real:'),
                    m('td[colspan=3].text-right', [
                        params.format(ctx.conjunto().tasa()), '%',
                        ctx.showAlert() == 2 ? m('span.text-right.label.label-warning', {
                            style : 'cursor:pointer',
                            onclick : ctx.showAlert.bind(null,1)
                        }, '?') : null
                    ])
                ])
            ])
        ]),

        ctx.showAlert() == 1 ? m('div.text-small.alert.alert-warning', {style:'position:absolute'}, [
            m('.close', {onclick:ctx.showAlert.bind(null,2)} , m.trust('&times;')),
            params.alertaCompuestos
        ]) : null,

        m('h6', 'Componentes:'),



        conjunto.componentes().map(function (c) {
            var cta = c.cuenta() ? oorden.cuenta(c.cuenta()) : null;
            var ctaTraslado = c.cuenta_traslado() ? oorden.cuenta(c.cuenta_traslado()) : null;

            return m('table.table.table-bordered.megatable', {style:'margin-bottom:10px'}, [
                m('tbody', [
                    m('tr', [
                        m('td', [

                            m.component(EditorComponent, {
                                apply : function (v) {
                                    c.nombreComponente(v);

                                    return ctx.guardarComponente(c)
                                                .then(function () { toastr.success('Cambios Guardados') })
                                },
                                initialize : function (model) {
                                    model( c.nombreComponente() )
                                }
                            }, [
                                m('span', c.nombreComponente())
                            ], function (edCtx) {

                                return [
                                    m('input[type=text]', {
                                        oninput : m.withAttr('value', edCtx.model),
                                        value : edCtx.model(),
                                        onkeyup : function (ev) {
                                            if(ev.keyCode == 13) {
                                                edCtx.apply( edCtx.model() )
                                            }
                                        }
                                    })
                                ]
                            }),

                            m('div.small.text-grey', CompuestoLabel(c.compuesto()))
                        ]),
                        m('td.text-right', params.format(c.tasa()) ),

                    ]),

                    m('tr', [
                        m('td', m('span.text-grey.small','Cuenta :')),
                        m('td', [
                            m.component(EditorComponent, {
                                apply : function (v) {
                                    c.cuenta(v);
                                    return ctx.guardarComponente(c)
                                                .then(function () { toastr.success('Cambios Guardados') })
                                },
                                initialize : function (model) { model(c.cuenta()) }
                            }, [
                                m('span', [
                                    cta ? cta.cuenta.concat(' - ', cta.nombre) : m('span.small.text-grey','(sin cuenta)')
                                ])
                            ], function (edCtx) {
                                edCtasSelector(edCtx);
                                return m('.cuentas-selector', edCtx.cuentasSelector.view());
                            })
                        ])

                    ]),

                    m('tr', [
                        m('td', m('span.text-grey.small','Cuenta Traslado:')),
                        m('td', [
                            m.component(EditorComponent, {
                                apply : function (v) {
                                    c.cuenta_traslado(v);
                                    return ctx.guardarComponente(c)
                                                .then(function () { toastr.success('Cambios Guardados') })
                                },
                                initialize : function (model) { model(c.cuenta_traslado()) }
                            }, [
                                m('span', [
                                    ctaTraslado ? ctaTraslado.cuenta.concat(' - ', ctaTraslado.nombre) : m('span.small.text-grey','(sin cuenta)')
                                ])
                            ], function (edCtx) {
                                edCtasSelector(edCtx);
                                return m('.cuentas-selector', edCtx.cuentasSelector.view());
                            })
                        ])

                    ])
                ])
            ])
        })
    ])
}




function edCtasSelector (ctx) {
    if(ctx.cuentasSelector) return;
    ctx.cuenta = m.prop(null);
    ctx.cuentasSelector = oor.cuentasSelect2({
        cuentas : oorden.cuentas(),
        find : oorden.cuenta,
        model : ctx.model,
        onchange : function (v) {
            ctx.cuenta( ctx.cuentasSelector.selected() );
            ctx.apply(v);
        }
    })
}




function EditorComponentController (params) {
    var ctx = this;

    ctx.showPencil = m.prop(false);
    ctx.editando = m.prop(false);
    ctx.model = m.prop();
    ctx.busy = m.prop();



    ctx.apply = function (v) {
        var result = params.apply.call(ctx, v);
        function resolve () {
            ctx.model(v);
            ctx.deshabiltarEdicion()
        }

        if(result && result.then) {
            result.then(resolve)
        } else if(result) {
            resolve()
        }

        m.redraw();

    }

    ctx.editar = function () {
        params.initialize(ctx.model)
        ctx.editando(true);
    }

    ctx.deshabiltarEdicion = function () {
        ctx.editando(false)
    }
}


function EditorComponentSelector (ctx,args) {
    return m('select', {disabled: ctx.busy(), value : ctx.model(), onchange: m.withAttr('value',ctx.apply) }, [
        args.options.map(function (d) {
            return m('option', d, d.caption)
        })
    ]);
}

function EditorComponentView (ctx, args, mostrar, editar) {
    return m('div', {}, ctx.editando() ? [

        editar ? editar(ctx) : EditorComponentSelector(ctx,args),

        ctx.busy() ? oor.loading(true) :m('button.btn.btn-flat.btn-xs.btn-default', {
            onclick : ctx.deshabiltarEdicion
        }, m.trust('&times;'))

    ] : [
        m('div', {onmouseenter : ctx.showPencil.bind(null, true), onmouseleave : ctx.showPencil.bind(null, false) }, [
            mostrar,
            ctx.showPencil() ? m('a[href=javascript:;]', {onclick : ctx.editar}, [
                m('i.fa.fa-edit.text-grey')
            ]) : null
        ])
    ]);
}
