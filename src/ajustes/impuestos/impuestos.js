var BaseComponent = require('./BaseComponent');
var ImpuestoModel = require('./ImpuestoModel');
var ImpuestoComponenteModel = require('./ImpuestoComponenteModel');

module.exports = m.component(BaseComponent, {
    titulo : 'Control de Impuestos',
    nuevoLabel : 'Nuevo Impuesto',

    conjuntoModel : ImpuestoModel,
    componenteModel : ImpuestoComponenteModel,
    getNombreConjunto : f('impuesto_conjunto'),
    getIdConjunto : f('impuesto_conjunto_id'),
    format :  d3.format(',.2f'),

    endpoint : '/impuestos',


    getNombreComponente : f('componente'),
    alertaCompuestos : 'Los conjuntos compuestos se calculan al final, esto puede alterar el cálculo.'
});
