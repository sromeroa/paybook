module.exports = Impuesto;
var Componente = require('./ImpuestoComponenteModel');

var fields = {
    impuesto_conjunto_id : {},
    impuesto_conjunto : {},
    seccion : {
        filter : function (d) {
            if(d == 'C' || d == 'V') {
                return d;
            }
            return 'A'
        }
    },

    exento : {
        filter : function (d) {
            return Boolean(Number(d));
        }
    },

    estatus : {},

    componentes : {
        filter : function (componentes) {
            if(! componentes ) return [];
            return componentes.map(function (c) { return new Componente(c); });
        }
    }
};

function Impuesto (data) {
    var obj = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined')
        {
            fieldVal = null;
        }

        obj[k] = m.prop(fieldVal);
    });

    obj.$isNew = m.prop(false);
    obj.$isNew.toJSON = function () { return undefined }
}


Impuesto.asignarCompuesto = function (conjunto, componente) {
    var getCompId = f('impuesto_componente_id');


    conjunto.componentes().forEach(function (comp) {
        var origVal = comp.compuesto()
        var val = getCompId(comp) == getCompId(componente);

        if(origVal && val) {
            val = false;
        }

        comp.compuesto(val);
    })
}


Impuesto.agregarComponente = function (conjunto) {
    conjunto.componentes().push(
        new Componente({
            impuesto_componente_id : oor.uuid()
        })
    );
}



Impuesto.find = function (data) {
    return m.request({
        url : '/apiv2',
        data : nh.extend({
            modelo:'impuestos',
            include : 'impuestos.componentes'
        }, data),
        unwrapSuccess : f('data')
    });
}

Impuesto.get = function (id) {
    return Impuesto.find({ impuesto_conjunto_id : id}).then(f('0'))
}

Impuesto.calcularTasa = function(imp) {
    var tasa = 0;
    var componentes = f('componentes')(imp);

    if(!componentes || !componentes.length) return tasa;

    componentes.filter(Impuesto.conjuntoNoEsCompuesto)
        .forEach(function (d) {
            tasa += Number( f('tasa')(d) );
        });


    componentes.filter(Impuesto.conjuntoEsCompuesto)
        .forEach(function(d) {
            var lTasa = (1 + f('tasa')(d)/100) * (1 + tasa/100)
            tasa = Math.round(10000 * (lTasa - 1))/100;
        });

    return tasa;
}

Impuesto.guardar = function (d) {
    return m.request({
        method : 'POST',
        url : '/apiv2/agregar?modelo=impuestos',
        data : d
    })
}

Impuesto.conjuntoEsCompuesto = function (c) {
    return Boolean(Number( f('compuesto')(c) ));
}

Impuesto.conjuntoNoEsCompuesto = function (c) {
    return !Boolean(Number( f('compuesto')(c) ));
}
