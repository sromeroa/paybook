module.exports = Retencion;
var Componente = require('./RetencionComponenteModel');

var fields = {
    retencion_conjunto_id : {},
    retencion_conjunto : {},
    seccion : {
        filter : function (d) {
            if(d == 'C' || d == 'V') {
                return d;
            }
            return 'A'
        }
    },
    exento : {
        filter : function (d) {
            return Boolean(Number(d));
        }
    },
    estatus : {},
    componentes : { 
        filter : function (componentes) {
            if(! componentes ) return [];
            return componentes.map(function (c) { return new Componente(c); });
        }
    }
};


function Retencion (data) {
    var obj = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined')
        {
            fieldVal = null;
        }

        obj[k] = m.prop(fieldVal);
    });

    obj.$isNew = m.prop(false);
    obj.$isNew.toJSON = function () { return undefined }
}

Retencion.agregarComponente = function (conjunto) {
    conjunto.componentes().push( 
        new Componente({
            retencion_componente_id : oor.uuid()
        }) 
    );
}



Retencion.find = function (data) {
    return m.request({
        url : '/apiv2',
        data : nh.extend({
            modelo:'retenciones',
            include : 'retenciones.componentes'
        }, data),
        unwrapSuccess : f('data')
    });
}

Retencion.get = function (id) {
    return Retencion.find({ retencion_conjunto_id : id}).then(f('0'))
}


Retencion.calcularTasa = function(imp) {
    var tasa = 0;
    var componentes = f('componentes')(imp);
    if(!componentes || !componentes.length) return tasa;

    componentes.filter(Retencion.conjuntoNoEsCompuesto)
        .forEach(function (d) {
            tasa += Number( f('tasa')(d) );
        });

    return tasa;
}

Retencion.guardar = function (d) {
    return m.request({
        method : 'POST',
        url : '/apiv2/agregar?modelo=retenciones', 
        data : d
    })
}

Retencion.conjuntoEsCompuesto = function (c) {
    return Boolean(Number( f('compuesto')(c) ));
}

Retencion.conjuntoNoEsCompuesto = function (c) {
    return !Boolean(Number( f('compuesto')(c) ));
}