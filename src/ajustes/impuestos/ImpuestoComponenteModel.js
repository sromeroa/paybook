module.exports = ImpuestoComponente;

var fields = {
    impuesto_componente_id : {},
    impuesto_conjunto_id : {},
    componente : {},
    tasa : {
        filter : function (n) {
            n = Number(n);
            return n|| 0
        }
    },
    compuesto : {
        filter : function (d) {  return Boolean(Number(d)) }
    },
    exento : {
        filter : function (d) {  return Boolean(Number(d)) }
    },
    cuenta : {

    },
    cuenta_traslado : {

    }
}

function ImpuestoComponente (data) {
    var obj = this;
    data || (data = {});


    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined') {
            fieldVal = null;
        }

        obj[k] = m.prop(fieldVal);
    });


    obj.nombreComponente = obj.componente;


    obj.$isNew = m.prop(false);
    obj.$isNew.toJSON = function () { return undefined }
}



ImpuestoComponente.guardar = function (d) {
    return m.request({
        method : 'POST',
        url : '/apiv2/agregar?modelo=impuestos-componentes',
        data : d
    })
}
