var BaseComponent = require('./BaseComponent');
var RetencionModel = require('./RetencionModel');

module.exports = m.component(BaseComponent, {
    titulo : 'Control de Retenciones',
    nuevoLabel : 'Nueva Retencion',

    conjuntoModel : RetencionModel,
    getNombreConjunto : f('retencion_conjunto'),
    getIdConjunto : f('retencion_conjunto_id'),
    format :  d3.format(',.2f'),

    endpoint : '/retenciones',

    getNombreComponente : f('componente'),
    alertaCompuestos : 'Los conjuntos compuestos se calculan al final, esto puede alterar el cálculo.'
});
