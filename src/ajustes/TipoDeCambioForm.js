var FormTipoDeCambio = module.exports = {
    controller : FormTipoDeCambioController,
    view : FormTipoDeCambioView,
    TipoDeCambio : TipoDeCambio
};




function FormTipoDeCambioController (params) {
    var ctx = this;

    var opcionesMonedas = oorden.monedas().filter(function (d) {
            return d.codigo_moneda != oorden.organizacion().moneda_base_id
        })
        .map(function (moneda) {
            return  {
                id : moneda.codigo_moneda,
                text : moneda.codigo_moneda + ' - ' + moneda.nombre
            }
        });

    ctx.tipoDeCambio = m.prop();
    ctx.sufijo = m.prop('');

    if(!params.tipo_de_cambio_id) {
        ctx.tipoDeCambio( new TipoDeCambio );
        //Es nuevo
        ctx.tipoDeCambio().$isNew(true);
        //Le asigno el ID
        ctx.tipoDeCambio().tipo_de_cambio_id(oor.uuid());
        //Le asigno la Moneda
        ctx.tipoDeCambio().codigo_moneda(params.codigo_moneda);

        asignarSufijo('');
    } else {
        TipoDeCambio.obtener(params.tipo_de_cambio_id)
            .then(function (data) {
                ctx.tipoDeCambio( new TipoDeCambio(data) );
                prepararSufijo();
            })
    }


    var monedaSelector = oor.select2({
        data : opcionesMonedas,
        placeholder : 'Seleccionar Moneda',
        model : function () {
            if(arguments.length) {
                ctx.tipoDeCambio().codigo_moneda( arguments[0] );
            }
            return ctx.tipoDeCambio().codigo_moneda();
        },
        onchange : function (v) {
            asignarSufijo();
        },
        enabled : function () {
            return ctx.tipoDeCambio().$isNew()
        }
    });


    ctx.monedaSelector = monedaSelector;
    ctx.asignarSufijo = asignarSufijo;
    ctx.guardar = guardar;

    function prepararSufijo () {
        var tipoDeCambio = ctx.tipoDeCambio().tipo_de_cambio();
        var codigo_moneda = ctx.tipoDeCambio().codigo_moneda();

        console.log(tipoDeCambio, codigo_moneda);

        var sufijo = tipoDeCambio.replace(new RegExp('^' + codigo_moneda),'');
        console.log(sufijo);

        ctx.sufijo(sufijo || '');
    }

    function asignarSufijo () {
        if(arguments.length) {
            ctx.sufijo(arguments[0]);
        }

        var sufijo = ctx.sufijo();
        var tipoDeCambio = ctx.tipoDeCambio();
        var codigoMoneda = tipoDeCambio.codigo_moneda() || '';

        tipoDeCambio.tipo_de_cambio( codigoMoneda.concat(sufijo) );
    }

    function guardar () {
        validar();

        if(ctx.errores().length) {
            return toastr.error('Error al Guardar Revise los datos')
        }

        return TipoDeCambio.guardar(ctx.tipoDeCambio())
            .then(
                function () { toastr.success('Tipo De Cambio Guardado')},
                function () {}
            );
    }

    ctx.errores = m.prop([]);

    ctx.validar = validar;

    ctx.errores.has = function (key) {
        return Boolean(ctx.errores().indexOf(key) + 1)
    }

    function validar () {
        var errores = [];
        var tipoDeCambio = ctx.tipoDeCambio();

        if(!tipoDeCambio.codigo_moneda()) {
            errores.push('noMoneda');
        }

        if(!tipoDeCambio.tipo_de_cambio()) {
            errores.push('noTipoDeCambio');
        }

        if(!tipoDeCambio.nombre()) {
            errores.push('noNombre');
        }

        if(tipoDeCambio.tipo() == 'auto') {
            if(!tipoDeCambio.fuente_id()) {
                errores.push('noFuente');
            }
        }

        if(tipoDeCambio.$isNew() && tipoDeCambio.tipo() == 'manual') {
            if(!tipoDeCambio.tasa()){
                errores.push('noTasa');
            }
        }

        ctx.errores(errores);
    }

    ctx.eliminar = function () {
        var tipoDeCambio = ctx.tipoDeCambio();
        var result = confirm('¿Eliminar este Tipo de Cambio?');
        if(result == false) return;


        TipoDeCambio.eliminar(tipoDeCambio.tipo_de_cambio_id())
            .then(function () {
                if(ctx.$modal) {
                    ctx.$modal.close();
                }

                toastr.success('Tipo De Cambio Eliminado');
            })
            .then(params.ondelete)

    }
}

function  FormTipoDeCambioView (ctx) {
    var tipoDeCambio = ctx.tipoDeCambio();
    var keyHistorial = tipoDeCambio.tipo().concat('-', tipoDeCambio.fuente_id() || '');

    ctx.validar();

    return tipoDeCambio ? m('.row', [
        m('div.col-sm-6', [
            m('row', [
                m('button.pull-right.btn.btn-flat.btn-xs.btn-danger', {
                    onclick : ctx.eliminar
                },m('.ion-trash-a')),
            ]),
            m('.clearfix'),
            m('.flex-row', [
                m('.mt-inputer', [
                    m('label', 'Moneda'),
                    m('div',ctx.monedaSelector.view())
                ])
            ]),

            tipoDeCambio.codigo_moneda() ? m('div', [
                m('.flex-row', [
                    m('.mt-inputer', {'class' : ctx.errores.has('noTipoDeCambio') ? 'error' : ''}, [
                        m('label','Tipo de Cambio: '),
                        m('input[type=text][size=4][maxlength=2]', {
                            value : tipoDeCambio.codigo_moneda(),
                            readonly : true
                        }),
                        m('input[type=text][size=4][maxlength=2]'.concat(tipoDeCambio.$isNew() ? '' : '[disabled]'), {
                            value : ctx.sufijo(),
                            oninput : m.withAttr('value',ctx.asignarSufijo)
                        })
                    ]),
                    m('.mt-inputer', {'class' : ctx.errores.has('noNombre') ? 'error' : ''}, [
                        m('label','Nombre: '),
                        m('input[type=text]', {
                            value : tipoDeCambio.nombre(),
                            oninput : m.withAttr('value', tipoDeCambio.nombre)
                        })
                    ])
                ]),

                m('.flex-row', [
                    m('.mt-inputer', [
                        m('label','Descripción: '),
                        m('input[type=text]', {
                            value : tipoDeCambio.descripcion(),
                            oninput : m.withAttr('value', tipoDeCambio.descripcion)
                        })
                    ])
                ]),

                m('flex-row', [
                    m('small', 'Actualizaciones: '),
                    m('ul', [
                        m('li', [
                            m('label', [
                                m('input[type=radio][name=tipo]', {
                                    checked : tipoDeCambio.tipo() == 'manual',
                                    onchange : tipoDeCambio.tipo.bind(null,'manual')
                                }),
                                ' Manual'
                            ])
                        ]),
                        m('li', [
                             m('label', [
                                m('input[type=radio][name=tipo]', {
                                    checked : tipoDeCambio.tipo() == 'auto',
                                    onchange : tipoDeCambio.tipo.bind(null,'auto')
                                }),
                                ' Automático'
                            ])
                        ])
                    ])
                    
                   
                ]),

                m('.clear'),

                m('.flex-row', [
                    tipoDeCambio.$isNew() && tipoDeCambio.tipo() == 'manual'? m('.mt-inputer',  {
                        'class' : ctx.errores.has('noTasa') ? 'error' : ''
                    },[
                        m('label', 'Tasa de Cambio'),
                        m('input[type=text][class=text-right]', {
                            value : tipoDeCambio.tasa(),
                            oninput : m.withAttr('value', tipoDeCambio.tasa)
                        })
                    ]) : null,

                    tipoDeCambio.tipo() == 'auto' ? m.component(FuentesComponent, {
                        key : 'for-'.concat( tipoDeCambio.codigo_moneda() ),
                        codigo_moneda : tipoDeCambio.codigo_moneda(),
                        value : tipoDeCambio.fuente_id
                    }) : null
                ]),
               
        

            ]) : m('div', 'Selecciona una moneda para continuar')
        ]),
        m('.col-sm-6', [
            m('h5', 'Historial Tasas De Cambio'),
            
            m.component(Historial, {
                key : keyHistorial,
                tipoDeCambio : ctx.tipoDeCambio()
            })
        ])
    ]) : null;
};


/**
 *
 * CARGAR FUENTES
 */

FormTipoDeCambio.fuentes = m.prop();

FormTipoDeCambio.getFuentes = (function () {
    var fuentes;

    return function () {
        if(fuentes) return fuentes;

        fuentes = m.request({
            method : 'GET',
            url : '/monedas/fuentes',
            unwrapSuccess : function (r) { return r.data }
        }).then(FormTipoDeCambio.fuentes);

        return fuentes;
    }

})();

/**
 *
 * FUENTES DE CAMBIO
 *
 */

var FuentesComponent = {};

FuentesComponent.controller = function (params) {
    var ctx = this;
    ctx.fuentes = m.prop();
    

    FormTipoDeCambio.getFuentes().then(function () {
        var fuentes = FormTipoDeCambio.fuentes().filter(function (fte) {
            return f('codigo_moneda')(fte) == f('codigo_moneda')(params)
        });

        if(fuentes.length && !params.value()) {
           params.value( fuentes[0].fuente_global_id );
        }
        ctx.fuentes(fuentes);
    })
}

FuentesComponent.view = function (ctx, params) {
    return m('select', {
        value : params.value(),
        onchange : m.withAttr('value', params.value)
    }, [
        ctx.fuentes().map(function (fte) {
            return m('option', {value:fte.fuente_global_id}, fte.fuente_global)
        })
    ])
}



/**
 * MODELO DE TIPO DE CAMBIO
 */

function TipoDeCambio (params) {
    var tdc = this;

    params || (params = {});

    var properties = [
        'tipo_de_cambio_id',
        'tipo_de_cambio',
        'nombre',
        'descripcion',
        'codigo_moneda',
        'tasa',
        'fuente_id'
    ];

    properties.forEach(function (prop) {
        tdc[prop] = m.prop( f(prop)(params) || null);
    });

    tdc.tipo = m.prop();

    if(!tdc.fuente_id()) {
        tdc.tipo('manual');
    } else {
        tdc.tipo('auto');
    }

    tdc.$isNew = m.prop(false);
}


TipoDeCambio.guardar = function (tdc) {
    var method = tdc.$isNew() ? 'POST' : 'PUT';
    var url = tdc.$isNew() ? '/monedas/add' : '/monedas/edit/'.concat(tdc.tipo_de_cambio_id());

    return m.request({
        url : url,
        method : method,
        data :tdc
    })
}

TipoDeCambio.obtener = function (id) {
    return m.request({
        url : '/monedas/tcambio/' + id,
        method : 'GET',
        unwrapSuccess : function (r) {
            return r.data;
        }
    })
}


TipoDeCambio.paraRevaluaciones = function (id) {
    return m.request({
        url : '/monedas/revaluaciones/' + id,
        method : 'POST'
    });
}

TipoDeCambio.ejecutarAccion  = function (id, accion) {
    return m.request({
        url : '/monedas/' + accion + '/' + id,
        method : 'POST'
    })
}

TipoDeCambio.eliminar = function (id) {
    return m.request({
        url : '/monedas/remove/' + id,
        method : 'delete'
    })
}


/**
 *
 * COMPONENTE HISTORIAL
 *
 */

var Historial = {};

Historial.controller = function (params) {

    var ctx = this;

    this.tasas = m.prop([]);
    this.nuevaTasa = m.prop('');
    this.agregarTasa = agregarTasa;

    obtenerTasas();

    function obtenerTasas () {
        var q = {};
        var doQuery = false;

        if(params.tipoDeCambio.tipo() == 'manual') {
            q.tipo_de_cambio_id = params.tipoDeCambio.tipo_de_cambio_id();
            if(q.tipo_de_cambio_id && params.tipoDeCambio.$isNew() == false) doQuery = true;
        } else {
            q.fuente_global_id = params.tipoDeCambio.fuente_id();
             if(q.fuente_global_id) doQuery = true;
        }

        if(!doQuery) return;

        m.request({
            url : '/monedas/tasas',
            method : 'GET',
            data : q
        }).then(function (r) {
           ctx.tasas(r.data)
        })
    }

    function agregarTasa () {
        m.request({
            url : '/monedas/tasa/' + params.tipoDeCambio.tipo_de_cambio_id(),
            method : 'POST',
            data : {
                tasa : ctx.nuevaTasa()
            }
        })
        .then(obtenerTasas)
        .then(function () {
            ctx.nuevaTasa('');
            toastr.success('Tasa Actualizada');
        })
    }
}

Historial.view  = function (ctx, params) {
    var mostrarForm = params.tipoDeCambio.$isNew() == false && params.tipoDeCambio.tipo() == 'manual';

    return m('div', [
        mostrarForm ? m('form', {
            onsubmit : function (ev) {
                ev.stopPropagation();
                ev.preventDefault();
                ctx.agregarTasa();
                return false;
            }
        }, [
            m('input[type=text]', {
                placeholder : 'Actualizar Tasa',
                value : ctx.nuevaTasa(),
                oninput : m.withAttr('value', ctx.nuevaTasa)
            }),
            m('button.btn-flat.btn-sm.btn-primary[type=submit]', 'Guardar')
        ]) : null,

        m('table.table.megatable',[
            m('thead', [
                m('tr', [
                    m('th', 'Fecha'),
                    m('th', 'Tasa')
                ])
            ]),
            m('tbody', [
                ctx.tasas().map(function (t) {
                    return m('tr', [
                        m('td', f('fecha')(t) ),
                        m('td', f('tasa_de_cambio')(t) )
                    ])
                })
            ])
        ])
    ])
}
