var Field = require('../nahui/nahui.field');
var dynTable = require('../nahui/dtable');

var FormTipoDeCambio = require('./TipoDeCambioForm');
var TipoDeCambio = FormTipoDeCambio.TipoDeCambio;

var component = module.exports = {};

component.controller = function (args) {
    var ctx  = this;

    ctx.moneda = m.prop(null);
    ctx.divisas = m.prop([]);
    ctx.monedas = m.prop();
    ctx.cantidades = {};

    var allMonedas;
    var fields = fieldsMoneda();
    var dTable = dynTable().key(f('tipo_de_cambio'));
    var actualizarTabla = m.prop(false);
    var divisasMap = {};
    var dataDivisas;

    var columnas = [
        fields.main,
        fields.infoUTasa,
        fields.tasaInversa,
        fields.para_revaluaciones,
        fields.estatus
    ];

    ctx.actualizarTabla = actualizarTabla;

    ctx.setupTable = setupTableFunctor(ctx, dTable, columnas);

    /** 
     * Filtra tipos de cambio de acuerdo a la moneda seleccionada
     */
    ctx.filtrarMonedas = function () {
        var monedas = allMonedas.filter(function (mon) {
            if(mon.codigo_moneda == oorden.organizacion().codigo_moneda_base){
                return false;
            }

            if(ctx.moneda() == null) {
                return true;
            }

            return mon.codigo_moneda == ctx.moneda()
        });

        ctx.monedas(monedas);
        ctx.actualizarTabla(true);
    };

    /** 
     * 
     */
    ctx.agregarTipoDeCambio = function () {
        abrirModalTipoDeCambio.call(this, ctx, {
            codigo_moneda : ctx.moneda()
        });
    };

    /**
     *
     */
    ctx.abirTipoDeCambio = function (el, id) {
        abrirModalTipoDeCambio.call(el, ctx, {
            tipo_de_cambio_id : id
        })
    };

    /**
     * Recarga las monedas
     */
    ctx.actualizarMonedas = function () {
        return m.request({
            url : '/monedas/monedas',
            method : 'GET'
        }).then(function (r) {
            asignarMonedas(r.data);
        });
    };


    ctx.actualizarMonedas();

    /**
     * Se acomodan las monedas y los tipos de cambio
     */
    function asignarMonedas (iMonedas) {

        allMonedas = iMonedas.filter(function (m) {
            return Boolean(m.codigo_moneda);
        }).map(function (m) {
            m.nombre_tc = m.tipo_de_cambio.concat(' - ', m.nombre);
            m.fechaUltimaTasa = (m.fechaUltimaTasa || '').split(' ')[0];
            m.tasaInversa = String(Math.round(1000000/Number(m.tasa_de_cambio))/1000000);
            return m;
        });

        var monedas = allMonedas.map(nh.identity);
        var divisaSeleccionada;

        //Se establecen las divisas que se estan usando
        monedas.filter(function (mon) {
            return mon.codigo_moneda != oorden.organizacion().codigo_moneda_base;
        }).forEach(function (mon) {
            divisasMap[mon.codigo_moneda] = {
                id : mon.codigo_moneda,
                text : mon.nombre_moneda
            }
        });

        dataDivisas = Object.keys(divisasMap).map(function (k) {
            return divisasMap[k];
        });

        ctx.divisas(dataDivisas);
        divisaSeleccionada = dataDivisas[0];

        if(!ctx.moneda()) {
            if(divisaSeleccionada) {
                ctx.moneda(divisaSeleccionada.id);
            }
        }

        ctx.filtrarMonedas();
    }
};

component.view = function (ctx) {
    return m('div', [
        m('.row', [
            m('.col-sm-3', [
                m('h3.column-title', 'Monedas'),
                ctx.divisas().map(function (mon) {
                    return m('div.categoria-elem', {
                        class : ctx.moneda() == mon.id ? 'active' : '',
                        onclick : function () { 
                            ctx.moneda(mon.id);
                            ctx.filtrarMonedas();
                        } 
                    }, [
                        m('span', mon.id),
                        m('spam.label.label-default.pull-right'),
                        m('div', m('small', mon.text))
                    ])
                })
            ]),
            m('.col-sm-9', [
                m('button.pull-right.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs', {
                    onclick : ctx.agregarTipoDeCambio
                },'Agregar'),
                m('h3.column-title', 'Tipos De Cambio'),
                 m('.table-responsive', [
                    m('table.table', {config : ctx.setupTable})
                ])
            ])
        ]),

        MTModal.view()
    ]);
}

/** 
 * Abre el modal del tipo de cambio
 */
function abrirModalTipoDeCambio(gctx, params) {
    MTModal.open({
        controller : FormTipoDeCambio.controller,
        args : nh.extend({
            ondelete : gctx.actualizarMonedas
        }, params),
        top : function () {
            return [m('h3', 'Tipo de Cambio')]
        },
        bottom : function (ctx) {
            return [
                m('button.pull-right.btn-flat.btn-sm.btn-success', {
                    onclick : function () {
                        ctx.guardar()
                            .then(gctx.actualizarMonedas)
                            .then(ctx.$modal.close)
                    }
                },'Guardar'),

                 m('button.pull-left.btn-flat.btn-sm.btn-default', {
                    onclick : ctx.$modal.close
                },'Cancelar')
            ];
        },
        content : FormTipoDeCambio.view,
        el : this
    });
}



/**
 * Los campos de la tabla de Tipos de Cambio
 * Debe Irse al modelo cuando haya
 */
function fieldsMoneda(argument) {
    var fields = {};

    fields.nombre_moneda = Field('nombre_moneda', {
        value : f('nombre'),
        caption : 'Moneda'
    });

    fields.nombre_tc = Field.linkToResource('nombre_tc', {
        caption : 'Nombre',
        value : f('tipo_de_cambio'),
        url : function (t) {
            return '/monedas/editar/' + t.tipo_de_cambio_id
        },
        enter : function (selection) {
            selection.call(Field.linkToResource.enter)
                .append('span')
                .attr('class','fuente text-grey')
                .attr('style','font-size:10px');
        },
        update : function (selection) {
            selection.call(Field.linkToResource.update);
            
            selection.select('.fuente').html(function (d) {
                return d.row.fuente ? '<i class="ion-link" style="padding:4px"> '.concat( f('descripcion')(d.row.fuente) )  : '';
            });
        },
        'class' : 'text-indigo'
    });

    fields.main = Field.twice('main', {
        subfields : [fields.nombre_tc, fields.nombre_moneda]
    });

    fields.ultimaTasa = Field('ultimaTasa', {
        value : f('tasa_de_cambio'),
        caption : 'Tasa de Cambio',
        'class' : 'text-right'
    });

    fields.fechaUltimaTasa = Field('fechaUltimaTasa',{
        caption : 'Fecha Tasa',
        'class' : 'text-grey text-right'
    });

    fields.infoUTasa = Field.twice('infoUTasa', {
        subfields : [fields.ultimaTasa, fields.fechaUltimaTasa]
    });

    fields.para_revaluaciones = Field('para_revaluaciones',{
        caption : 'Revaluaciones',
        enter : function (sel) {
            sel.attr('class', 'text-center')
                .append('span')
                    .attr('x-id', function (d) {
                        return d.row.tipo_de_cambio_id;
                    })
                    .attr('class', 'label revaluaciones')
                    .style('cursor','pointer');

            sel.call(Field.enter);
        },
        update : function (sel) {
            sel.select('span.label')
                .classed({
                    'label-primary' : f('value'),
                    'label-default' : function (v) {
                        return v.value === false
                    }
                })

                .call(Field.update)
        },
        value : function (item) {
            return  Boolean(Number( f('para_revaluaciones')(item) ))
        },
        filter : function (val) {
            return val ? 'Sí' : 'No';
        },

    });

    fields.tasaInversa = Field('tasaInversa', {
        caption : 'Tasa Inversa',
        'class' : 'text-right'
    });

    fields.estatus = Field('estatus', {
        caption :'Estatus',
        'class' : 'text-center',
        filter : function (d) {
            return d == 'ACT' || d == '1' ? 'ACTIVO' : 'INACTIVO';
        },
        enter : function (sel) {
            sel.append('span')
                .attr('x-id', function (d) {
                    return d.row.tipo_de_cambio_id;
                })
                .attr('class', 'label status')
                .style('cursor','pointer');

            sel.call(Field.enter)

        },
        update : function (sel) {
            sel.select('span').text(f('text'))
                .classed({
                    'label-success' : function (d) { return d.text == 'ACTIVO'},
                    'label-default' : function (d) { return d.text != 'ACTIVO'}
                })
                .attr('x-activo', function (f) { return f.text == 'ACTIVO' ? 1 : 0 })
        }
    });

    return fields;
}


/** 
 * Inicializa la tabla en el controlador
 * y los eventos
 */
function setupTableFunctor (ctx, dTable, columnas) {
    return function (element, isInited) {
        if(isInited && ctx.actualizarTabla() == false) return;

        d3.select(element).call(dTable, columnas, ctx.monedas());

        if(isInited) return;

        //Abrir ventana tipo de cambio
        $(element).on('click', '.celda-nombre_tc', function (ev) {
            ev.preventDefault();
            ev.stopPropagation();
            var id = $(this).attr('href').split('/monedas/editar/').join('');
            ctx.abirTipoDeCambio(this, id);
            m.redraw();
            return false;
        });

        //Cambiar revaluaciones
        $(element).on('click', '.label.revaluaciones', function (ev) {
            ev.preventDefault();
            ev.stopPropagation();

            var si = confirm('¿Desea Asignar Este tipo de Cambio, "para revaluaciones"?')
            var id = $(this).attr('x-id');
            if(!si) return;

            TipoDeCambio.paraRevaluaciones(id,true)
                .then(ctx.actualizarMonedas); 
        });

        //Cambiar Estatus
        $(element).on('click', '.label.status', function (ev) {
            ev.preventDefault();
            ev.stopPropagation();

            var leyendas = [
                '¿Desea Activar este tipo de cambio?',
                '¿Desea Desactivar este tipo de cambio?'
            ];
            var actions = ['activar','desactivar'];
            var activo = Number($(this).attr('x-activo'));
            var si = confirm(leyendas[activo]);
            var id = $(this).attr('x-id');

            TipoDeCambio.ejecutarAccion(id, actions[activo])
                .then(ctx.actualizarMonedas)
        });
    }
}
