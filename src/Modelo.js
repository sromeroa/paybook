
    var Modelo = module.exports = {};


    Modelo.apiPrefix    = m.prop('/apiv2');
    Modelo.numberProp   = numberProp;
    Modelo.registerKeys = registerKeys;
    Modelo.crear        = CrearModelo;



    function numberProp (prop, precision, variableStr) {
        var factor = Math.pow(10 ,precision);
        var intProp = m.prop(null);
        var getStr = (variableStr == true) ? getStringVariable : getString;
        var format = d3.format(",.2f");

        nProp.getString = getString;
        nProp.fix = fix;
        nProp.$int = intGetSet;
        nProp.number= number;
        nProp.toJSON = function () { return nProp(); };

        set(prop());

        return nProp;



        function nProp (val) {
            if(!arguments.length) return prop();
            set(val);
        }

        function number () {
            return format(intProp() / factor);
        }


        function intGetSet (intVal) {
            if(arguments.length == 0) return intProp();
            intProp(Math.round(intVal));
            fix();
        }

        function set (val) {

            var num = Math.round(Number(cast(val)) * factor);
            isNaN(num) && (num = 0);

            prop(val);
            intProp(num);
        }

        function fix (val) {
            if(arguments.length) set(val);
            set(getStr());
        }

        function cast (val) {
            //console.log(val, String(val).split(',').join(''))
            return String(val).split(',').join('');
        }

        function getString () {
            return (intProp() / factor).toFixed(precision);
        }

        function getStringVariable () {
            var match  = String( intProp() ).match(/0+$/);
            var fixes  = Math.max(precision - (match ? match[0].length : 0) , 0);
            return (intProp() / factor).toFixed(fixes);
        }
    };

    function calcUrl (param) {
        return Modelo.apiPrefix() + '?' + $.param(param);
    }

    function registerKeys(self, keys, data) {
        data || (data = {});

        keys.forEach(function (k) {
            self[k] = m.prop( ((typeof data[k]) == 'undefined') ? null : data[k]);
        });

    }




    function CrearModelo (nombreModelo, modeloFn, params, proto) {
        modeloFn.keys = params.keys;

        modeloFn.get = function (id) {
            var param = {modelo : nombreModelo};
            param[params.idKey] = id;

            if(typeof modeloFn.include === 'function') {
                param.include = modeloFn.include();
            }

            return m.request({
                url:calcUrl(param),
                method:'GET',
                unwrapSuccess: function(response) {
                    return response.data[0];
                }
            });
        };

        modeloFn.crear = function () {
            var mod = new modeloFn();
            return mod;
        };

        modeloFn.find = function () {
            var param = {modelo : nombreModelo};

            return m.request({
                url:calcUrl(param),
                method:'GET',
                unwrapSuccess: function(response) {
                    return response.data;
                }
            });
        };


        modeloFn.guardar = function (recurso) {
            return m.request({
                    method : 'POST',
                    url : '/api/operacion/agregar',
                    data : recurso.toJSON(),
                }).then(function (l) {
                    console.log(l);
                });
        }

        modeloFn.prototype.toJSON = function () {
            var json = {};
            var self = this;

            modeloFn.keys.forEach(function (k) {

                if(k.indexOf('$') == 0) return;

                if(angular.isArray(self[k])) {
                    json[k] = self[k].map(function (o) {
                        if(angular.isObject(o) && ( (typeof o.toJSON) == 'function') ) {
                            return o.toJSON();
                        }
                        return null;
                    });

                    return;
                }

                json[k] = self[k]();
            });

            return json;
        };


        Object.keys(proto || {}).forEach(function (k) {
            modeloFn.prototype[k] = proto[k];
        });

        return modeloFn;
    }
