$(document).ready(function () {

    var selector = "";
    if (window.location.pathname == "/plantillas") selector = "#plantilla-lista";
    if (window.location.pathname.indexOf("/plantillas/editar") > -1) selector = "#editar-plantilla";
    if (window.location.pathname == "/plantillas/agregar") selector = "#subida-plantilla";

    if (selector) {
        var interval = setInterval(function () {
            if (typeof oorden() !== 'undefined' && typeof oorden.organizacion() !== 'undefined' && oorden.organizacion().hasOwnProperty("codigo_pais")) {
                clearInterval(interval);
                ejecutarVue();
            }
        }, 100);

        function ejecutarVue() {
            console.log("Runing");
            // Necesitamos cambiar los delimitadores a corchetes ( '[' y ']' ) porque hay conflitos con angular usando llaves ('{' '}')
            Vue.config.delimiters = ['[[', ']]'];
            var vm = new Vue({
                el: selector,
                data: {
                    plantillas: null,
                    tipo_plantilla: "poliza",
                    path_plantilla_actual: null,
                    esDO: false
                },

                ready: function () {
                    this.subirTemplate();
                    if (window.location.pathname == '/plantillas' || window.location.pathname == "/plantillas/editar") {
                        this.cargarTemplates();
                    }

                    this.esDO = oorden.organizacion().codigo_pais == "DO";
                },
                methods: {
                    cargarTemplates: function () {
                        $.ajax({
                            url: "/apiv2?modelo=templates&include=template.tipoDocumento",
                            method: "GET",
                            success: function (plantillas) {
                                if (oorden.obtenerParametroPorNombre("modelo")) {
                                    vm.tipo_plantilla = oorden.obtenerParametroPorNombre("modelo");
                                }

                                plantillas = plantillas.data;
                                for (var i in plantillas) {
                                    if (plantillas[i].created_at) {
                                        plantillas[i].date = plantillas[i].created_at;
                                    } else {
                                        plantillas[i].date = plantillas[i].updated_at;
                                    }
                                    plantillas[i].date = moment(plantillas[i].date).format("DD-MM-YYYY hh:mm:ss a");

                                    if (plantillas[i].modelo == vm.tipo_plantilla) {
                                        vm.path_plantilla_actual = plantillas[i].path;
                                    }
                                }
                                vm.plantillas = plantillas;
                            }
                        });
                    },

                    subirTemplate: function () {
                        $(".form-plantilla").on("submit", function (e) {
                            e.preventDefault();

                            var formdata = new FormData(this);
                            $.ajax({
                                url: $(this).attr("action"),
                                type: 'POST',
                                data: formdata,
                                cache: false,
                                dataType: 'json',
                                processData: false,
                                contentType: false,
                                beforeSend: function () {
                                    $(".loading-icon").removeClass("hide");
                                    $(".ion-arrow-up-a").addClass("hide");
                                },
                                success: function (data, textStatus, jqXHR) {
                                    if (data.type == 'FAILED') {
                                        return toastr.error(data.message)
                                    }
                                    toastr.success("Se ha subido la plantilla exitosamente");

                                    setTimeout(function () {
                                        window.location = "/plantillas";
                                    }, 1000);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    return toastr.error(textStatus)
                                }
                            }).done(function () {
                                $(".loading-icon").addClass("hide");
                                $(".ion-arrow-up-a").removeClass("hide");

                            });
                        });
                    }
                }
            });

        }
    }
});
