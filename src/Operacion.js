var Field = require('./nahui/nahui.field');

/**
 *
 * Modelo de Operación
 *
 **/
function Operacion (data) {
    var OperacionItem = oor.mm('OperacionItem');
    var Direccion = oor.mm('Direccion');
    var op = this;

    data || (data = {});
    Operacion.initializeInstance.call(this,data);

    this.$new = this.$isNew = m.prop(false);
    this.$detalleImpuestos = m.prop([]);
    this.$detalleRetenciones = m.prop([]);
    this.$sumaImportes = Modelo.numberProp(m.prop(0),2);

    this.total = Modelo.numberProp(m.prop(data.total), 2);
    this.saldo = Modelo.numberProp(m.prop(data.saldo),2);
    this.numero(data.numero == '0' ? '' : data.numero);

    this.total.fix();
    this.saldo.fix();

    this.ccto1 = m.prop();
    this.ccto2 = m.prop();

    this.eliminar([]);
    this.extras(data.extras || {});

    this.$direccion = m.prop(data.direccion ? new Direccion(data.direccion) : null);
    this.$direccionEnvio = m.prop(data.direccionEnvio ? new Direccion(data.direccionEnvio) : null);

    this.fecha_esperada( this.fecha_vencimiento() );

    //Se convierten a Booleanos
    Operacion.BooleanProps.forEach(function (k) {
        var value = Boolean(Number(op[k]()));
        op[k](value);
    });

    this.impuestos = Modelo.numberProp(this.impuestos, 2);
    this.retenciones = Modelo.numberProp(this.retenciones, 2);


    this.$impuestosIncluidos = m.prop( this.precios_con_impuestos() );

    this.$sinImpuestos       = Modelo.numberProp(m.prop(0) , 2);
    this.$totalImpuestos     = Modelo.numberProp(m.prop(0) , 2);
    this.$totalRetenciones   = Modelo.numberProp(m.prop(0) , 2);
    this.$sumaImportes       = Modelo.numberProp(m.prop(0) , 2);
    this.$residuo            = Modelo.numberProp(m.prop(0) , 2);
    this.$descuentos         = Modelo.numberProp(m.prop(0) , 2);

    this.$cuentaBanco        = m.prop(data.cuentaBanco || null);
    this.$tercero            = m.prop(data.tercero || null);

    this.$metodoDePago       = m.prop(oorden.metodoDePago(this.forma_pago_id()) || null);

    var tipoDeCambio = this.tipo_de_cambio() || oorden.tipoDeCambio.base().tipo_de_cambio;

    this.$tipoDeCambio       = m.prop(oorden.tipoDeCambio(this.tipo_de_cambio()) || null);
    this.$tipoDeDocumento    = m.prop(oorden.tipodeDocumento(data.tipo_documento) || null);

    this.$poliza = m.prop(data.poliza || null);

    if(data.pagos) {
        this.pagos(
            data.pagos.map(function (p) {
                var AplicacionDePago = oor.mm('AplicacionDePago');
                pago  = new AplicacionDePago(p);
                return pago;
            }).filter(function (pago) {
                return pago.monto_pagado.$int();
            })
        );
    } else {
        this.pagos([]);
    }

    var ccto1, ccto2;
    var operacion = this;

    this.items = (this.items() || [])
        .map(function (item) {
            ccto1 = ccto1 || item.ccto_1_id;
            ccto2 = ccto2 || item.ccto_2_id;

            return item;
        })
        .sort(function (i1, i2) {
            return Number(i1.posicion_item) - Number(i2.posicion_item);
        })
        .map(function (item) {
            item.operacion = operacion;
            return new OperacionItem(item);
        });

    this.items.toJSON = function () {
        var items =  this.items.filter(function (i) {
            return i.$integrable();
        });
        return items;
    }.bind(this);


    this.ccto1( oorden.buscarCcto(ccto1) );
    this.ccto2( oorden.buscarCcto(ccto2) );


    this.$errors = m.prop([]);
}

Operacion.BooleanProps = [
    'precios_con_impuestos',
    'mostrar_descuento',
    'mostrar_cctos',
    'mostrar_cuentas',
    'mostrar_retenciones',
    'mostrar_impuestos'
];

Operacion.agregarLinea = function (operacion) {
    var OperacionItem = oor.mm('OperacionItem');
    var item = new OperacionItem();
    item.operacion_item_id( oor.uuid() );
    item.operacion_id( operacion.operacion_id() );
    operacion.items.push(item);
};

Operacion.estatuses = m.prop([
    { estatus : 'P', nombre : 'EN PREPARACIÓN', color : 'amber', nombre_corto: 'Borrador'},
    { estatus : 'T', nombre : 'PENDIENTE', color : 'amber', nombre_corto: 'Finalizada'},
    { estatus : 'A', nombre : 'APLICADA', color : 'default', nombre_corto : 'Aplicada'},
    { estatus : 'S', nombre : 'SALDADA', color : 'green', nombre_corto : 'Saldada'},
    { estatus : 'X', nombre : 'ELIMINADA', color : 'grey', nombre_corto : 'Eliminada'},
]);

Operacion.estatuses().forEach(function (t) {
    Operacion.estatuses[t.estatus] = m.prop(t);
});



Operacion.include = function () {
    return 'operaciones.items,operaciones_items.cuenta,operaciones.tercero,operaciones.poliza,operaciones.direccion,operaciones.ncf';
};


Operacion.estatus = function (op, estatus) {
    return m.request({
        method : 'POST',
        url : '/api/operacion/estado_operacion/' + op.operacion_id() + '/' + estatus
    });
}

Operacion.tipoOperacion = function (item) {
    return f('tipo_operacion')(item).toLowerCase();
}

Operacion.url = function (item) {
    return '/operaciones/' + Operacion.tipoOperacion(item) + '/' + Operacion.id(item);
}



Operacion.fields = function () {
    var nFormat = d3.format(",.2f");
    var fields = {};
    var currency = d3.format(',.2f');

    function numberFormat (d) {
        return nFormat(d || 0);
    }

    numberFormat.hideZero = function (val) {
        if(val == 0) return '--';
        return numberFormat(val);
    }

    fields.fecha = Field('fecha',{
        caption:'Fecha'
    });

    fields.nombre_tercero = Field('nombre_tercero', {
        caption : 'Tercero'
    });

    fields.fecha_vencimiento = Field('fecha_vencimiento',{
        value : function (d) {
            if(d.seccion != 'E' && d.seccion != 'I'){
                return f('fecha_vencimiento')(d)
            }
            return null
        },
        caption:'Vence'
    });

    fields.fecha_esperada = Field('fecha_esperada', {
        value : function (d) {
            if(d.seccion != 'E' && d.seccion != 'I'){
                return f('fecha_esperada')(d)
            }
            return null
        },
        caption:'Esp.'
    });

    fields.estatus = Field('estatus', {
        'class':'text-right small',
        'headingClass':'text-right',
        caption : 'Estatus',
        filter :  function (st) {
            var status = oor.mm('Operacion').estatuses[st];
            return status ? status().nombre_corto : '';
        },
        update : function (selection) {
            selection.call(Field.update);

            Operacion.estatuses().filter(f('color')).forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) {
                    return d.value == st.estatus;
                });
            });

        }
    });

    fields.documento = Field.linkToResource('documento', {
        caption : 'Documento',
        url : Operacion.url,
        class : 'text-indigo',
    });

    fields.referencia = Field('referencia', {
        caption:'Referencia',
        filter : function (r) {
            if(!r) return '\u00A0';
            return r;
        },
        'class':'text-grey small'
    });

    fields.doc_ref = Field.twice('doc_ref', {
        subfields : [fields.documento,fields.referencia]
    });

    fields.moneda = Field('moneda', {
        caption : 'Moneda',
        'class' : 'text-right text-grey',
        'headingClass' : 'text-right',
        'filter' : function (v) {
            if(v == oorden.organizacion().codigo_moneda_base) return '';
            return v;
        }
    });


    fields.sucursal = Field('sucursal', {
        caption : 'Vendedor @ Sucursal',
        'class' : 'text-grey small',
        value : function (d) {
            var s = d.vendedor ?  d.vendedor.nombre : ' ';
            return s.concat(' @ ', d.sucursal.nombre);
        }
    });


    fields.infoVta = Field.twice('infoVta', {
        subfields : [fields.nombre_tercero, fields.sucursal]
    })


    fields.saldo = Field('saldo', {
        caption:'Saldo',
        filter : numberFormat.hideZero,
        'class':'text-right',
        'headingClass':'text-right',
        update : function (selection) {
            selection.call(Field.update);
            /*
            Operacion.estatuses().forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) {
                    return d.row.estatus == st.estatus;
                });
            });

            */
        }
    });


    fields.saldoYEstatus = Field.twice('saldoYEstatus', {
        subfields : [fields.saldo, fields.estatus]
    })


    fields.total = Field('total', {
        caption:'Total',
        filter:currency,
        'class':'text-right',
        'headingClass':'text-right'
    });

    fields.total_moneda = Field.twice('total_moneda', {
        caption : 'Total',
        subfields : [fields.total, fields.moneda]
    });

    fields.monto_recibido=  Field('monto_recibido', {
        caption : 'Recibido',
        value : function (item) {
            if(item.seccion == 'I') return f('total')(item);
            return 0;
        },
        filter : numberFormat.hideZero,
        'class' : 'text-right'
    })

    fields.monto_pagado = Field('monto_pagado', {
        caption : 'Pagado',
        value : function (item) {
            if(item.seccion == 'E') return f('total')(item);
            return 0;
        },
        filter : numberFormat.hideZero,
        'class' : 'text-right'
    });

    return fields;
}


oor.mm('Operacion', Operacion)
    .serviceName('operaciones')
    .idKey('operacion_id')

    .prop('operacion_id', {})
    .prop('organizacion_id', {})
    .prop('operacion_anterior_id', {})
    .prop('$operacionAnterior', {})
    .prop('sucursal_id', {})
    .prop('seccion', {})
    .prop('tipo_operacion', {})
    .prop('tipo_documento', {})
    .prop('serie', {})
    .prop('numero', {})
    .prop('fecha', {})
    .prop('tercero_id', {})
    .prop('cuenta_tercero_id', {})
    .prop('tercero_direccion_id', {})
    .prop('eliminar', {})
    .prop('extras', {})
    .prop('ncf', {})
    .prop('tipo_ncf_id', {})

    .prop('contacto_id', {})
    .prop('direccion_envio', {})
    .prop('cuenta_banco_mov', {})
    .prop('fecha_vencimiento', {})

    .prop('fecha_esperada', {})
    .prop('vendedor_id', {})
    .prop('referencia', {})
    .prop('tipo_de_cambio', {})
    .prop('moneda', {})
    .prop('tasa_de_cambio', {})

    .prop('forma_pago_id', {})

    .prop('num_cheque', {})
    .prop('ncf', {})
    .prop('tipo_ncf_id', {})
    .prop('fecha_cheque',{})
    .prop('clave_banco_destino',{})
    .prop('cuenta_banco_destino',{})
    .prop('nombre_banco_destino',{})

    .prop('descuentos',{})
    .prop('impuestos',{})
    .prop('retenciones',{})
    .prop('subtotal', {})
    //.prop('total', {})
    .prop('estatus', {})
    .prop('saldo',{})
    .prop('items',{})
    .prop('pagos', {})

    //Mostrar
    .prop('precios_con_impuestos', {type:'boolean'})
    .prop('mostrar_descuento', {type:'boolean'})
    .prop('mostrar_cctos', {type:'boolean'})
    .prop('mostrar_cuentas', {type:'boolean'})
    .prop('mostrar_retenciones', {type:'boolean'})
    .prop('mostrar_impuestos', {type:'boolean'})

    .prop('$cuentaBanco',{})
    .prop('$tipoDeDocumento',{})
    .prop('$metodoDePago', {})

    .method('asignarTercero', function (v) {
        if(!v) return;
        this.tercero_id( v.tercero_id);
        this.$tercero( v );
    })

    .method('asignarTipoDeDocumento', function (tipoDoc) {
        this.tipo_documento(tipoDoc.tipo_documento_id);
        this.$tipoDeDocumento(tipoDoc);
        this.serie(tipoDoc.serie);
    })

    .method('asignarMetodoDePago', function (mp) {
        this.$metodoDePago(mp);
        this.forma_pago_id(mp.id);
    })
    .method('asignarTipoDeCambio', function (tc) {
        var self = this;

        this.$tipoDeCambio( tc );
        this.tipo_de_cambio( tc.tipo_de_cambio );
        this.moneda(tc.codigo_moneda);

        if(tc.base == 1) {
            this.tasa_de_cambio('1.0000')
        } else {
            m.request({
                method:'GET',
                url : '/api/tasa/'.concat(tc.tipo_de_cambio)
            }).then(function (tasa) {
                self.tasa_de_cambio( tasa.ultimaTasa.tasa_de_cambio );
            });
        }
    })
    .method('label', function () {
        var tipoDoc = this.$tipoDeDocumento() || {};
        var label = tipoDoc.nombre || '';
        label += (this.serie() ? this.serie() : '');
        label += ' #' +(this.numero() ? this.numero() : '<auto>');
        return label;
    })
    .method('$estatus', function () {
        var estatus = Operacion.estatuses[this.estatus()];
        return estatus ? estatus() : {};
    })
    .method('buscarItem', function (itemId) {
        return this.items.filter(function (item) {
            return item.operacion_item_id() == itemId;
        })[0];
    })
    .method('asignarOperacionAnterior', function (operacionAnterior, asignarItems) {
        var self = this;
        this.$operacionAnterior(operacionAnterior);

        if(!asignarItems) return;

        self.items.forEach(function (item) {
            var itemAnteriorId = item.operacion_anterior_item_id();
            var itemAnterior;

            if(itemAnteriorId) {
                itemAnterior = operacionAnterior.buscarItem(itemAnteriorId);
                if(!itemAnterior) {
                    console.warn('item no encontrado: ', itemAnteriorId)
                }
                item.$operacionItemAnterior(itemAnterior);
            }
        });
    })
    .method('actualizar', function () {
        if(this.tipo_operacion() == 'EPP' || this.tipo_operacion() == 'IDC') return;

        var self = this;
        var subtotal = 0;      //Suma de los sin impuestos
        var tImpuestos = 0;
        var tRetenciones = 0;   //
        var total = 0;         //subtotal + impuestos - retenciones
        var sumaImportes = 0;  //Suma directa Importes
        var residuo = 0;
        var sumaDescuentos = 0;

        var impuestos = {};
        var retenciones = {};


        self.items.forEach(function (item) {
            item.$impuestosIncluidos( self.$impuestosIncluidos() );
            item.actualizar();

            subtotal += item.$sinImpuestos.$int();
            sumaImportes += item.importe.$int();
            sumaDescuentos += item.$descuento.$int();

            Object.keys(item.$impuestos).forEach(function (impKey) {
                if(! impuestos[impKey] ) {
                    impuestos[impKey] = 0;
                }
                impuestos[impKey] += item.$impuestos[impKey];
                tImpuestos += item.$impuestos[impKey];
            });

            Object.keys(item.$retenciones).forEach(function (retKey) {
                if(!retenciones[retKey]) {
                    retenciones[retKey] = 0;
                }
                retenciones[retKey] += item.$retenciones[retKey];
                tRetenciones += item.$retenciones[retKey];
            });
        });


        self.$descuentos.$int(sumaDescuentos);

        self.$detalleImpuestos(
            Object.keys(impuestos).map(function (impKey) {
                var monto = Modelo.numberProp(m.prop(), 2);
                monto.$int(impuestos[impKey]);
                return {
                    nombre : impKey,
                    monto : monto
                };
            })
        );

        self.$detalleRetenciones(
            Object.keys(retenciones).map(function (impKey) {
                var monto = Modelo.numberProp(m.prop(), 2);
                monto.$int(retenciones[impKey]);
                return {
                    nombre : impKey,
                    monto : monto
                };
            })
        );

        self.$totalImpuestos.$int(tImpuestos);
        self.$sumaImportes.$int(sumaImportes);

        self.impuestos.$int(tImpuestos);
        self.retenciones.$int(tRetenciones);

        total = subtotal + tImpuestos - tRetenciones;


        if(self.$impuestosIncluidos()) {
            residuo = sumaImportes - (total + tRetenciones);
            total = sumaImportes - tRetenciones;
        }

        self.$residuo.$int(residuo);
        self.total.$int(total);
        //self.saldo.$int(total);

    })
    .method('addError', function (err) {
        if(this.$errors().indexOf(err) == -1) {
            this.$errors().push(err);
        }
    })
    .method('remError', function (err) {
        var idx = this.$errors().indexOf(err);
        if(idx > -1) {
            this.$errors().splice(idx,1);
        }
    })
    .method('hasError', function (err) {
        return this.$errors().indexOf(err) > -1;
    })
    ;


Operacion.id =  f('operacion_id');

Operacion.search = function (item) {
    var search = f('referencia')(item);

    search += ' ' + f('documento')(item);
    search += ' ' + f('nombre_tercero')(item);
    return search;
}
