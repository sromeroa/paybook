module.exports = {
    view : TraspasoView,
    controller : TraspasoController
};
var Traspaso = require('./TraspasoModel');

function TraspasoController () {
    var ctx = this;
    var traspaso = new Traspaso({ fecha:oor.fecha.toSQL(new Date) });

    ctx.mostrarMontoDestino = m.prop(false);
    ctx.traspaso = m.prop(traspaso);
    ctx.mostrarTC = m.prop(false);
    ctx.vm = VMTraspaso(ctx.traspaso);
    ctx.guardar = guardar;
    ctx.guardando = m.prop();

    function cuentaOrigenModel () {
        if(arguments.length) {
            ctx.traspaso().cuenta_origen(arguments[0]);
        }
        return ctx.traspaso().cuenta_origen();
    }

    function cuentaDestinoModel () {
        if(arguments.length) {
            ctx.traspaso().cuenta_destino(arguments[0]);
        }
        return ctx.traspaso().cuenta_destino();
    }


    function guardar () {
        var t = ctx.traspaso();
        if(ctx.guardando()) return;

        if(!t.cuenta_origen()) {
            return toastr.error('Seleccionar Cuenta Origen')
        }
        if(!t.monto_origen()) {
            return toastr.error('Capturar Monto Egreso')
        }
        if(!t.cuenta_origen()) {
            return toastr.error('Seleccionar Cuenta Destino')
        }
        if(!t.monto_destino()) {
            return toastr.error('Capturar Monto Ingreso')
        }

        if(t.cuenta_origen() == t.cuenta_destino() ) {

        }

        if(!t.referencia()) {
            return toastr.error('Referencia Obligatoria')
        }

        var tText = JSON.stringify(t) ;
        var tData = JSON.parse(tText);

        ctx.guardando(true);
        oor.request('/movimientos/guardarTraspaso', 'POST', {
            data : tData
        }).then(function (d) {
            oor.lsMessage({type:'success', text:'¡Traspaso Registrado!'});
            history.back();
        });
    }


    function generate () {
        var cuentaOrigen = ctx.traspaso().cuenta_origen();
        var cuentaDestino = ctx.traspaso().cuenta_destino();
        var fecha = ctx.traspaso().fecha();

        if(!fecha || !cuentaDestino || !cuentaOrigen) {
            return;
        }

        if(cuentaDestino == cuentaOrigen) {
            return toastr.error('La cuenta de Origen y Destino debe ser distinta');
        }

        Traspaso.generar({
            cuenta_destino:cuentaDestino,
            cuenta_origen:cuentaOrigen,
            fecha:fecha
        }).then(function (traspaso) {
            var monedaOrg = oorden.organizacion().codigo_moneda_base;

            ctx.traspaso().moneda_origen(traspaso.moneda_origen);
            ctx.traspaso().moneda_destino(traspaso.moneda_destino);
            ctx.mostrarMontoDestino( traspaso.moneda_origen != traspaso.moneda_destino );

            /**
             * Si la
             **/
            if(traspaso.monedas) {
                ctx.mostrarTC(true);
            } else {
                ctx.mostrarTC(false);
            }

            if(ctx.traspaso().moneda_origen() == ctx.traspaso().moneda_destino()) {
                ctx.traspaso().monto_destino( ctx.traspaso().monto_origen() );
            }

            ctx.traspaso().monedas = traspaso.monedas;
        })
    }

    oorden().then(function () {
        var cuentasBancarias = oorden.cuentas()
                                .filter( f('es_bancos').is(1) )
                                .map(function (cta) { return {
                                    id : cta.cuenta_contable_id,
                                    text:(cta.cuenta || '').concat(' - ', cta.nombre)}
                                });


        var ctaOrigenSelector = oor.select2({
            model : cuentaOrigenModel,
            find : oorden.cuenta,
            data : cuentasBancarias,
            agregarCta : false,
            onchange : function () {
                var ctaOrigen =  ctaOrigenSelector.selected();
                var monedaOrigen = ctaOrigen.moneda;
                ctx.traspaso().moneda_origen(monedaOrigen);
                generate()
            },
            width: "100%"
        });

        var ctaDestinoSelector = oor.select2({
            model : cuentaDestinoModel,
            data : cuentasBancarias,
            agregarCta : false,
            onchange : generate
        });

        ctx.ctaOrigenSelector = ctaOrigenSelector;
        ctx.ctaDestinoSelector = ctaDestinoSelector;
    });

}


function VMTraspaso (traspaso) {
    var vm = {};
    vm.tasa = {};

    vm.monto_origen = function (val) {
        console.log('origen',val)
        traspaso().monto_origen(val);
        if(traspaso().moneda_origen() && traspaso().moneda_origen() == traspaso().moneda_destino()) {
            traspaso().monto_destino(val);
        }
        vm.calcularTasas()
    }

    vm.monto_destino = function (val) {
        traspaso().monto_destino(val);
        vm.calcularTasas()
    }

    vm.calcularTasas = function () {
        vm.tasa.tasa = vm.tasa.tasaInversa =  null;
        if(traspaso().monto_origen() && traspaso().monto_destino()) {
            vm.tasa.tasa = traspaso().monto_destino() / traspaso().monto_origen()
            vm.tasa.tasaInversa = 1/vm.tasa.tasa;
        }
        console.log(vm.tasa)
    }

    return vm;
}

function focus (el, isInited) {
    console.log(el)
    if(isInited) return;
    setTimeout(function () { el.focus() },100)
}

function TraspasoView (ctx) {
    var t = ctx.traspaso();
    var vm = ctx.vm;
    var mostrarTasas = (t.moneda_origen() != t.moneda_destino()) && vm.tasa.tasa

    return m('div', [
    m('#title-head', [

        m('.col-xs-12.col-md-6.title-bar', [
            m('h3', 'Traspaso Entre Cuentas'),
        ]),


        m('.clear'),

    ]),

    m('.panel', [
        // m('.panel-heading', [
        //     //m('.row', [
        //     m('.panel-title', [
        //         m('h4', 'Traspaso Entre Cuentas'),
        //     ]),
        // ]),


        m('.panel-body.cuenta-traspaso', [
            m('.row', [
                m('.col-xs-12.col-md-4.traspaso-item', [
                    m('h5.text-indigo', 'Cuenta Origen: '),
                    m('', [
                        m('.mt-inputer.line', {style:'flex:2 1 15em;'}, [
                            m('div.input-container', ctx.ctaOrigenSelector.view())
                        ]),
                        t.moneda_origen() ? m('.mt-inputer.line', {style:'flex:2 1 15em'}, [
                            m('label', 'Monto Egreso: '),
                            m('input', {
                                value : t.monto_origen(),
                                onchange:m.withAttr('value',ctx.vm.monto_origen),
                                config:focus
                            })
                        ]) : null
                    ]),
                ]),
                m('.col-xs-12.col-md-4.traspaso-item', [
                    m('h5.text-indigo', 'Cuenta Destino: '),
                    m('.flex-row', [
                        m('.mt-inputer.line', {style:'flex:2 1 15em'}, [
                            m('div.input-container', ctx.ctaDestinoSelector.view())
                        ]),
                        t.moneda_destino() ? m('.mt-inputer.line', {style:'flex:2 1 15em'}, [
                            m('label', 'Monto Ingreso: '),
                            m('input', {
                                value : t.monto_destino(),
                                onchange:m.withAttr('value',ctx.vm.monto_destino),
                                config:focus
                            })
                        ]) : null
                    ]),

                    mostrarTasas ? [
                        m('div', [
                            1 , ' ', t.moneda_origen() , ' = ', vm.tasa.tasa, ' ', t.moneda_destino()
                        ]),
                        m('div', [
                            1 , ' ', t.moneda_destino() , ' = ', vm.tasa.tasaInversa,  ' ', t.moneda_origen()
                        ])
                    ] : null
                ]),
                m('.col-xs-12.col-md-4.traspaso-item', [
                    m('.row', [


                        m('.col-xs-12.col-md-6', [
                            m('h5.text-indigo', 'Fecha:'),
                            m('.mt-inputer.line', {style:'flex:1 2 15em'}, [
                                m('input[type=date]', {
                                    value : t.fecha()
                                })
                            ])
                        ]),
                        m('.col-xs-12.col-md-6', [
                            m('h5.text-indigo', 'Referencia:'),
                            m('.mt-inputer.line', {style:'flex:2 1 15em'}, [
                                m('input[type=text]',{style:'height:34px; border:1px solid #e0e0e0;'}, {
                                    value : t.referencia(),
                                    onchange : m.withAttr('value', t.referencia)
                                })
                            ])
                        ]),

                    ]),
                    /*
                    (t.monedas && Object.keys(t.monedas).length) ? [
                        m('h6.text-center', 'Tasa de Cambio'),
                        Object.keys(t.monedas).map(function (d) {
                            return m('div',d)
                        })
                    ] : null
                    */
                ]),
            ]),
        ]),

        m('.row.text-center',[
            m('button.btn.btn-success.button-striped.button-full-striped.btn-ripple', {
                'class' : ctx.guardando() ? 'disabled' : '',
                onclick : ctx.guardar
            },'Guardar Traspaso')
        ])
    ]),
    ]);
}
