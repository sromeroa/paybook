module.exports = {
    view : MovimientoView,
    controller : MovimientoController
};

var T3Component = require('../operaciones/components/Tercero.js');
var T32Component = require('../operaciones/components/Tercero2.js');

function MovimientoController (params) {
    oor.fondoGris();
    var ctx         = this;
    var search      = m.route.parseQueryString(location.search);
    var movimientos = [];

    if(params.movimientos) {
        movimientos = params.movimientos;
    }
    else if(search.movimientos) {
        movimientos = search.movimientos.split(',')
    }

    if( (params.ingreso && params.egreso) || (!params.ingreso && !params.egreso) ) {
        throw 'Ingreso / Egreso';
    }

    ctx.movimientos = m.prop(movimientos.length ? movimientos : null);
    ctx.maxSize = m.prop(12);
    ctx.cuenta = m.prop();
    ctx.cuenta.cargando = m.prop(false);
    ctx.operaciones = m.prop();




    ctx.cuentaId   = m.prop(params.cuentaId || search.cuenta_id);
    ctx.ingreso    = m.prop(params.ingreso);
    ctx.egreso     = m.prop(params.egreso);
    ctx.nombre     = m.prop(params.ingreso == true ? 'Ingreso' : 'Egreso');
    ctx.fetching   = m.prop(false);

    var fijarMonto = Math.abs(params.fijarMonto);

    ctx.fijarMonto = m.prop(params.fijarMonto ? fijarMonto : false);
    ctx.ajustable  = m.prop(Boolean(params.ajustable));

    var terceroEnabled = m.prop( ctx.movimientos() ? false : true);
    /**
     * Selectores
     */

    //Modelo auxiliar para tercero_id
    function tercero_id () {
        if(arguments.length) {
            ctx.movimiento().tercero_id(arguments[0])
        }
        return ctx.movimiento().tercero_id()
    }

    ctx.tercero = m.prop();

    if(search.usarTerceroNuevo) {

        ctx.terceroSelector = m.component(T32Component, {
            //tercero_id : tercero_id(),
            placeholder :  'Seleccionar tercero...',
            onchange : function (values) {
                console.log(values);
                tercero_id( values.tercero_id() );

                ctx.movimiento().detalles([]);
                ctx.actualizarMovimiento();
                ctx.generarIngreso();
            }
        });

    } else {
        ctx.terceroSelector = m.component(T3Component, {
            tercero    : ctx.tercero,
            tercero_id : tercero_id,

            enabled    : terceroEnabled,
            placeholder : 'Seleccionar...',
            btnAgregar : false,

            onchange : function () {
                ctx.movimiento().detalles([]);
                ctx.actualizarMovimiento();
                ctx.generarIngreso();
            },
        });
    }







    ctx.montoCoincide = function () {
        if(!ctx.fijarMonto()) return true;
        console.log('montoCoincide', ctx.fijarMonto(), ctx.movimiento().total())
        return ctx.fijarMonto() == ctx.movimiento().total();
    }

    /**
     * Selectores
     */
    function formaDePagoId () {
        if(arguments.length) {
            ctx.movimiento().metodo_pago_id(arguments[0])
        }
        return ctx.movimiento().metodo_pago_id()
    }

    ctx.formaDePago = m.prop();

    ctx.formaDePagoSelector = oor.select2({
        model : formaDePagoId,
        data : Object.keys(oorden.metodosDePago)
                    .map(function (m) { return oorden.metodosDePago[m](); })
                    .map(function (metodo) { return {id:metodo.id, text:metodo.metodo} }),

        onchange : function () { console.log( ctx.formaDePagoSelector.selected() ) },
        find : oorden.metodoDePago
    });

    ctx.inicializar = function () {
        ctx.cargarCuenta();
    }

    ctx.cargarCuenta = function () {
        if(!ctx.cuentaId()) return;
        if(ctx.cuenta() || ctx.cuenta.cargando()) return;

        ctx.cuenta.cargando(true);

        oor.request('/apiv2?modelo=cuentas&cuenta_contable_id=' + ctx.cuentaId())
            .then(function (ctas) {
                ctx.cuenta(ctas[0]);

                if(!ctx.cuenta()) {
                    return console.error('Error: seleccionar cuenta');
                }

                ctx.cuenta.cargando(false);

                //Genero el movimiento
                ctx.movimiento = m.prop(
                    new Movimiento({
                        fecha : params.fecha || oor.fecha.toSQL(new Date),
                        cuenta_id : ctx.cuentaId(),
                        conciliarCon : params.conciliarCon || null
                    })
                );

                ctx.movimiento().monedas = m.prop({});
                ctx.generarIngreso();
            });
    }


    ctx.generarIngreso = function () {
        if(!ctx.movimientos() && !ctx.movimiento().tercero_id()) {
            return
        }
        if(ctx.fetching()) return;

        ctx.fetching(true);

        oor.request('/movimientos/generarMovimiento/' + ctx.nombre().toLowerCase(), 'GET', {
            data : {
                tercero_id : ctx.movimiento().tercero_id(),
                pagarOperaciones : ctx.movimientos() ? ctx.movimientos().join(',') : undefined,
                fecha : ctx.movimiento().fecha(),
                cuenta_id : ctx.movimiento().cuenta_id()
            }
        })
        .then(function (data) {
            var monedas = ctx.movimiento().monedas();

            ctx.movimiento().factor(data.factor);
            ctx.movimiento().moneda(data.moneda);
            ctx.movimiento().tercero_id(data.tercero_id);

            if(data.tercero) {
                ctx.tercero(data.tercero)
            }

            Object.keys(data.monedas).forEach(function (codigo) {
                var moneda = data.monedas[codigo];
                var tasa = moneda.tasaDeCambio;

                monedas[codigo] = {
                    moneda : m.prop(codigo),
                    tasa_de_cambio : m.prop(tasa.tasa_de_cambio || tasa.tasa),
                    tipo_de_cambio : m.prop(moneda.tipo_de_cambio)
                }
            });

            data.operaciones.map(crearDetallePago)
                .forEach(function (d) {
                    d.factor = m.prop(data.factor);
                    console.log(d.factor)
                    ctx.movimiento().detalles().push(d)
                });


            if(ctx.movimientos() || ctx.fijarMonto()) {
                ctx.aplicarTodas();
            }

            if(data.tercero) {
                if(data.factor == 1 && data.tercero.es_cliente) {
                    ctx.movimiento().detalles().push({
                        tipo_movimiento : 'ANTICIPO',
                        tipo_anticipo : m.prop('cliente'),
                        importe : m.prop(0),
                        factor : m.prop(1)
                    })
                }

                if(data.factor == -1 && data.tercero.es_proveedor) {
                    ctx.movimiento().detalles().push({
                        tipo_movimiento : 'ANTICIPO',
                        tipo_anticipo : m.prop('proveedor'),
                        importe : m.prop(0),
                        factor : m.prop(-1)
                    });
                }
            }
        })
        .then(ctx.fetching.bind(null, false));

        m.redraw();
    }


    ctx.actualizarSaldos = function () {
        ctx.movimiento().detalles()
            .filter( f('tipo_movimiento').is('PAGAR_OPERACION') )
            .forEach(function (pago) {
                var saldoMCAnterior = pago.saldo_moneda_cuenta();
                asignarSaldoMonedaCuenta(pago);

                if(pago.importe() == saldoMCAnterior) {
                    pago.importe( pago.saldo_moneda_cuenta() );
                }

                pago.importe( Math.min(pago.importe(), pago.saldo_moneda_cuenta()) );
                ctx.asignarMontoPago(pago);
            })
    }


    ctx.agregarDetalleDeOperacion = function (operacion) {
        var pago = crearDetallePago(operacion);
        var pagoMismaOp = ctx.movimiento().detalles().filter( f('operacion_id').is(operacion.operacion_id) );
        //Si ya esta pagada la operacion con esta no se agrega
        if(pagoMismaOp.length) return;
        ctx.movimiento().detalles().push(pago);
    }


    function crearDetallePago (operacion) {
        var pago = {};

        Object.keys(operacion).forEach(function (key) {
            pago[key] = m.prop(operacion[key]);
        });

        pago.tipo_movimiento = m.prop('PAGAR_OPERACION')
        pago.monto_pago = m.prop(0)
        pago.importe = m.prop(0)
        pago.saldo_moneda_cuenta = m.prop(0);
        asignarSaldoMonedaCuenta(pago);
        return pago;
    }

    function asignarSaldoMonedaCuenta (pago) {
        pago.saldo_moneda_cuenta( saldoMonedaCuenta(pago.saldo(), pago.moneda_pago()) );
    }

    function saldoMonedaCuenta (saldo, moneda) {
        var intertasa;
        if(moneda == ctx.movimiento().moneda()) {
            return oorden.organizacion.round(saldo);
        }
        if(moneda == ctx.movimiento().moneda()) {
            intertasa = 1;
        } else {
            intertasa =  tasaMoneda(moneda) /  tasaMoneda(ctx.movimiento().moneda());
        }
        return oorden.organizacion.round(saldo * intertasa);
    }


    function tasaMoneda (moneda) {
        var monedas = ctx.movimiento().monedas();
        return monedas[moneda].tasa_de_cambio();
    }


    ctx.aplicarTodas = function () {
        var hasta = ctx.fijarMonto();

        ctx.movimiento().detalles()
            .filter( f('tipo_movimiento').is('PAGAR_OPERACION') )
            .filter( f('importe').is(0) )
            .forEach(function (d) {
                if(ctx.fijarMonto()) {
                    if(hasta <= 0) return;
                    var aplicar = Math.min(d.saldo(), hasta);
                    hasta -= aplicar;
                    d.importe(aplicar);
                    ctx.asignarMontoPago(d)
                } else {
                    ctx.agregarPago(d);
                }
            });

        ctx.actualizarMovimiento();
    }

    ctx.agregarPago = function (d) {
        d.monto_pago( d.saldo() );
        d.importe( d.saldo_moneda_cuenta() )
    }


    ctx.asignarMontoPago = function (pago) {
        var montoPago;
        var intertasa;

       if(pago.saldo_moneda_cuenta()  == pago.importe()){
            montoPago = pago.saldo()
        } else {
            if(pago.moneda_pago() == ctx.movimiento().moneda()) {
                intertasa = 1;
            } else {
                intertasa =   tasaMoneda(ctx.movimiento().moneda()) / tasaMoneda(pago.moneda_pago());
            }
            montoPago = oorden.organizacion.round(pago.importe() * intertasa);
        }

        pago.monto_pago(montoPago);
    }


    ctx.actualizarMovimiento = function () {
        var factor = oorden.organizacion.factor();
        var factorMov = ctx.egreso() ? -1 : 1;

        var total = ctx.movimiento().detalles()
            .map(function (d) { return d.importe() * (factorMov * d.factor()) * factor })
            .reduce(function (acum, ant) { return acum + ant }, 0)

        total = Math.round(total);
        ctx.movimiento().total(total/factor);
    }

    ctx.guardando = m.prop(false);

    ctx.guardar = function () {
        if(ctx.guardando()) return;
        var txt = JSON.stringify(ctx.movimiento());
        var movimiento = JSON.parse(txt);

        /*
         * Validaciones simples
         */
        if(!movimiento.metodo_pago_id) {
            return toastr.error('Selecciona un método de pago')
        }

        if(!movimiento.referencia) {
            return toastr.error('Referencia obligatoria')
        }

        if(!movimiento.detalles  || movimiento.detalles.length == 0) {
            return toastr.error('Selecciona al menos un concepto para el ' + ctx.nombre())
        }

        if(movimiento.total <= 0 ) {
            return toastr.error('El total tiene que ser mayor cero')
        }

        if(ctx.fijarMonto()){
            if(ctx.fijarMonto() != ctx.movimiento().total()) {
                return toastr.error('El monto debe ser igual a ' + oorden.org.format(ctx.fijarMonto()));
            }
        }

        ctx.guardando(true);

        oor.request('/movimientos/agregar', 'POST', {data:movimiento})
            .then(afterSave)
            .then(ctx.guardando.bind(null,false));

    }


    ctx.ajusteACuentaContable = function () {
        var ajuste = {};

        // 2000 - 500

        var diferencia = ctx.fijarMonto() - ctx.movimiento().total();
        console.log('difreencia');

        var mainFactor = ctx.ingreso() ? -1 : 1;
        var factor = mainFactor * (diferencia > 0 ? -1 : 1);

        diferencia = Math.abs(diferencia);

        console.log('factor:::', factor);

        ajuste.tipo_movimiento = m.prop('CUENTA')
        ajuste.importe = m.prop(diferencia);
        ajuste.factor = m.prop(factor);
        ajuste.cuenta_destino_id = m.prop(null);

        ctx.movimiento().detalles().push(ajuste);
        ctx.actualizarMovimiento();
    }

    function afterSave (data) {
        if(nh.isFunction (params.afterSave)) {
            return params.afterSave(data);
        }

        var redirectTo = '/bancos/banco/' + ctx.movimiento().cuenta_id();
        var search = location.search;

        if(search) {
            search = m.route.parseQueryString(search);
        }

        if(search.redirect_url) {
            redirectTo = search.redirect_url;
        }

        location.pathname = redirectTo;
    }
}



function SeleccionarCuenta(ctx) {
    var cuentaSelector = ctx.cuentaSelector;

    if(!cuentaSelector) {
        cuentaSelector = oor.select2({
            data : oorden.cuentas()
                    .filter(f('pago_permitido').is(1))
                    .map(function (cta) {
                        return {id:cta.cuenta_contable_id, text:cta.cuenta + ' ' + cta.nombre}
                    }),

            model : function () {
                if(arguments.length) {
                    ctx.cuentaId(arguments[0])
                }
                return ctx.cuentaId();
            }
        })
    }

    return m('.flex-row', [
        m('label', 'Seleccione la cuenta para el pago:'),
        cuentaSelector.view()
    ]);
}



function SelectoresDeCuentasVM(pago) {
    if(!pago.selectorDeCuentas) {
        var cuentas = oorden.cuentas().map(function (s) { return s; });

        pago.selectorDeCuentas = oor.cuentasSelector({
            cuentas : cuentas,
            model : function () {
                if(arguments.length){
                    pago.cuenta_destino_id(arguments[0])
                }
                return pago.cuenta_destino_id()
            },
            onchange : function (v) {
                /*
                item.asignarCuenta( cuentasSelector.selected() );
                ctrl.actualizar(true);
                */
            }
        });
    }
    return pago.selectorDeCuentas;
}




function MovimientoView (ctx) {
    ctx.inicializar();

    return m('div', !ctx.cuentaId() ? SeleccionarCuenta(ctx) : [
        ctx.cuenta.cargando() ? oor.loading() : m('div', [
            m('.panel', [
                m('.panel-heading', [
                    m('.row', [ 
                        m('.col-xs-10', [
                            m('h4.text-indigo', [
                                'Nuevo ', ctx.nombre(),  /*' en la cuenta ', ctx.cuenta().cuenta , ' - ' , ctx.cuenta().nombre*/
                                ' en ',
                                m('span', [
                                    ctx.cuenta().nombre
                                ]),

                            ]),
                        ]),
                        m('.col-xs-12..col-md-2', [
                            'Fecha: ',
                            m('input[type=date]', {
                                style:'height:1.8em; border:none',
                                value : ctx.movimiento().fecha(),
                                onchange : m.withAttr('value', ctx.movimiento().fecha)
                            })
                        ]),
                    ]),
                ]),

                m('.panel-body', [

                    m('.row', [
                        m('.col-xs-12.col-md-6', [
                            m('div', [
                                m('div.movimientos-form-item', [
                                    m('div', ctx.ingreso() ? 'Ingreso de:' : 'Egreso de:'),
                                m('div', ctx.terceroSelector),
                            ]),

                                m('div.movimientos-form-item', [
                                    m('div', 'Método de Pago:'),
                                    m('div', ctx.formaDePagoSelector.view()),
                                ]),

                                m('div.movimientos-form-item', [
                                    m('div', 'Referencia:'),
                                    m('div', [
                                        m('input[type=text]', {
                                            value : ctx.movimiento().referencia(),
                                            onchange : m.withAttr('value', ctx.movimiento().referencia),
                                            onfocus : asignarReferencia(ctx)
                                        })
                                    ])
                                ])

                            ])

                        ]),
                        m('.col-xs-12.col-md-6', [
                            m('div', [

                                m('div.movimientos-form-item', [
                                    m('div', 'Banco:'),
                                    m('div', m('input'))
                                ]),

                                m('div.movimientos-form-item', [
                                    m('div.ilabel', 'Tipo Tarjeta:'),
                                    m('div', m('input')),
                                ]),

                                m('div.movimientos-form-item', [
                                    m('div', 'Terminación Tarjeta:'),
                                    m('div', m('input'))
                                ]),

                            ])

                        ]),

                    ]),
                    TablaDetalles(ctx)
                ]),
            ])
        ])
    ])
}



function FilasPagarOperacion (ctx) {
    return ctx.movimiento().detalles()
        .filter(f('tipo_movimiento').is('PAGAR_OPERACION'))
        .map(function (detalle) {
            return m('tr', [
                m('td', [
                    m('div.small.text-grey', 'PAGAR OPERACIÓN:'),
                    m('div', detalle.documento(), ' ', detalle.fecha()),
                    m('div.small.text-grey', detalle.referencia()),

                ]),

                m('td', [
                    m('div.text-right', oorden.organizacion.format(detalle.total())),
                    m('div.small.text-grey.text-right', detalle.moneda_pago())
                ]),

                m('td.text-right', [
                    oorden.organizacion.format(detalle.saldo_moneda_cuenta())
                ]),

                m('td', [
                    m('input[type=text].text-right.pull-right', {
                        value : detalle.importe() != 0 ? oorden.organizacion.format(detalle.importe()) : '',
                        size : ctx.maxSize(),
                        onchange : m.withAttr('value', function (v) {
                            v = oorden.organizacion.unformat(v);
                            var val = Math.min(v, detalle.saldo_moneda_cuenta());

                            if(val != v) {
                                toastr.error('¡El Pago no puede ser mayor al saldo!');
                            }

                            detalle.importe(val)
                            ctx.asignarMontoPago(detalle)
                            ctx.actualizarMovimiento()
                        }),
                        onfocus : function () {
                            if(!detalle.importe()) {
                                ctx.agregarPago(detalle);
                                ctx.actualizarMovimiento()
                            }
                        }
                    })
                ])
            ]);
    });
}



function FilasCuentas (ctx) {
    return ctx.movimiento().detalles()
        .filter(f('tipo_movimiento').is('CUENTA'))
        .map(function (detalle) {
            return m('tr', [
                m('td', [
                    m('div.text-grey.small', 'CUENTA CONTABLE:'),
                    m('div', { config:SelectoresDeCuentasVM(detalle) }),

                ]),

                m('td', [
                    m('.text-grey.small', 'Concepto:'),
                    m('textarea', {
                        style:'width:100%'
                    })
                ]),

                m('td.text-right', [
                    m('select', {
                        value:detalle.factor(),
                        onchange:m.withAttr('value', function (f) {
                            detalle.factor(f);
                            ctx.actualizarMovimiento();
                        })
                    }, [
                        m('option', {value : '1'},  'Aplicar Cargo '.concat('(', ctx.movimiento().factor() == 1 ? '+' : '-', ')' ,':')  ),
                        m('option', {value : '-1'}, 'Aplicar Abono '.concat('(', ctx.movimiento().factor() == -1 ? '+' : '-', ')' ,':') )
                    ])
                ]),

                m('td', [
                    //Esto es al revés de los estándares por que es el factor con respecto a ala póliza bancaria
                    m('input[type=text].text-right.pull-right', {
                        value : detalle.importe() != 0 ? oorden.organizacion.format(detalle.importe()) : '',
                        size : ctx.maxSize(),
                        onchange : m.withAttr('value', function (v) {
                            v = oorden.organizacion.unformat(v);
                            if(v < 0) {
                                toastr.warning('Solo números positivos')
                                v = Math.abs(v)
                            }
                            detalle.importe(v)
                            ctx.actualizarMovimiento()
                        })
                    })
                ])
            ]);
    });
}

function FilasAnticipos (ctx) {
    return [];
    return ctx.movimiento().detalles()
            .filter(f('tipo_movimiento').is('ANTICIPO'))
            .map(function (detalle) {
                return m('tr', [
                    m('td', 'Anticipo de ' + detalle.tipo_anticipo()),
                    m('td'),
                    m('td'),
                    m('td')
                ])
            })
}


function TablaDetalles(ctx) {
    if(ctx.fetching()) return oor.loading();

    if(!ctx.tercero()) {
        if(ctx.movimiento().detalles().length == 0) {
            return null;
        }
    }

    return m('div', [
        m('.row', [
            m('.col-sm-12', [

                m('br'),
                m('br'),

                m('table.table.megatable', [
                    m('thead', [
                        m('tr', [
                            m('th', 'Concepto'),
                            m('th.text-right', 'Total'),
                            m('th.text-right', 'Saldo ', ctx.movimiento().moneda()),
                            m('th.text-right', 'Monto ', ctx.movimiento().moneda()),
                        ])
                    ]),

                    m('tbody', [
                        FilasPagarOperacion(ctx),
                        FilasCuentas(ctx),
                        FilasAnticipos(ctx)
                    ]),
                ])
            ])
        ]),

        m('.row', [
            m('.col-sm-8', [
                TiposDeCambio(ctx),
                ctx.ajustable() && (ctx.montoCoincide() == false) ? Ajustar(ctx) : null
            ]),
            m('.col-sm-4', TableFooter(ctx))
        ]),

        m('.row.action-bar', [
            m('btn.btn.btn-success.button-striped.button-full-striped.btn-ripple.btn-xs', {
                'class' : ctx.guardando() ? 'disabled' : '',
                onclick : ctx.guardar
            }, 'Aplicar')
        ])
    ]);
}



function Ajustar(ctx) {
    return m('div', [
        m('h6', 'Ajustar con:'),
        oor.stripedButton('button.btn-primary[style="zoom:0.9"]', 'Ajuste a cuenta contable', {
            onclick : ctx.ajusteACuentaContable
        })
    ])
}


function TableFooter (ctx) {
    return m('table.table', [
        m('tbody', [
            m('tr', {style:'border-top:1px solid #333; border-bottom:1px solid #333'}, [
                m('td', 'Total ', ctx.movimiento().moneda(), ':'),
                m('td.text-right', [
                    oorden.organizacion.format(ctx.movimiento().total())
                ])
            ]),

            ctx.montoCoincide() == false ? m('tr', [
                m('td.text-red', 'Diferencia:'),
                m('td.text-red.text-right', [
                    oorden.org.format(ctx.fijarMonto() - ctx.movimiento().total())
                ])
            ]) : null
        ])
    ])
}


function TiposDeCambio (ctx) {
    var monedas = ctx.movimiento().monedas();

    return [
        m('h6', 'Tipos De Cambio:'),

        Object.keys(monedas).map(function (mon) {
            if(mon == oorden.organizacion().codigo_moneda_base){
                return;
            }

            var moneda = monedas[mon];
            var selector = tipoDeCambioSelector(ctx, moneda);
            return [
                m('h6', moneda.moneda()),
                m('.flex-row', [

                    m('.mt-inputer.line', [
                        m('label', 'Tipo de Cambio'),
                        selector.view()
                    ]),

                    m('.mt-inputer', {style:'flex:1'}, [
                        m('label', '1 ', moneda.moneda() , ' = '),
                        m('input[type=text].text-right', {
                            value : moneda.tasa_de_cambio(),
                            onchange : m.withAttr('value', function (val) {
                                moneda.tasa_de_cambio(val);
                                ctx.actualizarSaldos();
                                ctx.actualizarMovimiento();
                            }),
                            size:12
                        }),
                        m('label', {
                            style:'padding-left:10px'
                        }, ' ', oorden.organizacion().codigo_moneda_base)
                    ])
                ])
            ];
        })
    ]
}



function tipoDeCambioSelector(ctx, moneda) {
    if(!ctx.TiposDeCambio) {
        ctx.TiposDeCambio = {};
    }

    if(!ctx.TiposDeCambio[moneda.moneda()]) {
        ctx.TiposDeCambio[moneda.moneda()] = oor.select2({
            model : modelTipoDeCambio(ctx, moneda.moneda()),
            data : oorden.tiposDeCambio[moneda.moneda()]().map(function (d) {
                return {
                    id : d.tipo_de_cambio,
                    text : d.tipo_de_cambio + ' ' + d.nombre
                }
            })
        })
    }

    return ctx.TiposDeCambio[moneda.moneda()];
}



function modelTipoDeCambio (ctx, moneda) {
    return function () {
        return ctx.movimiento().monedas()[moneda].tipo_de_cambio()
    }
}



function metodosDePago (ctx) {
    var formaDePago =  ctx.formaDePagoSelector.selected();
    if(!formaDePago) return null;
    formaDePago = metodosDePago[formaDePago.metodo.toLowerCase() ]
    return formaDePago(ctx)
}


metodosDePago.cheque = function (ctx) {
    return [
        m('.mt-inputer line', [
            m('label', '# Cheque'),
            m('input[type=text]', {
                onchange : m.withAttr('value', ctx.movimiento().no_cheque),
                value : ctx.movimiento().no_cheque()
            })
        ]),

        m('.mt-inputer line', [
            m('label', 'Fecha Cheque'),
            m('input[type=date]', {
                onchange : m.withAttr('value', ctx.movimiento().fecha_cheque),
                value : ctx.movimiento().fecha_cheque()
            })
        ])
    ];
}

metodosDePago.transferencia = function (ctx) {
    return [
        m('.mt-inputer.line' ,[
            m('label', 'Banco Destino'),
            m('input[type=text]', {
                onchange : m.withAttr('value', ctx.movimiento().banco_externo),
                value : ctx.movimiento().banco_externo()
            })
        ]),

        m('.mt-inputer.line' ,[
            m('label', 'Cuenta Destino'),
            m('input[type=text]', {
                onchange : m.withAttr('value', ctx.movimiento().cuenta_externa),
                value : ctx.movimiento().cuenta_externa()
            })
        ])
    ];
}




metodosDePago['tarjeta de credito'] = function (ctx) {
    return [
        m('.mt-inputer.line' ,[
            m('label', 'Banco'),
            m('input[type=text]', {
                onchange : m.withAttr('value', ctx.movimiento().banco_externo),
                value : ctx.movimiento().banco_externo()
            })
        ]),

        m('.mt-inputer.line' ,[
            m('label', 'Terminación Tarjeta'),
            m('input[type=text]', {
                onchange : m.withAttr('value', ctx.movimiento().cuenta_externa),
                value : ctx.movimiento().cuenta_externa()
            })
        ])
    ];
}




function asignarReferencia(ctx) {
    return function () {
        if(ctx.movimiento().referencia()) return;

        var referencia = ctx.nombre() || '', formaDePago;

        if(formaDePago = ctx.formaDePagoSelector.selected()) {
            referencia = referencia.concat(' ', formaDePago.metodo || '')

            if(ctx.movimiento().no_cheque()) {
                referencia = referencia.concat(' ', ctx.movimiento().no_cheque() || '');
            }

            referencia = referencia.concat(' ', ctx.movimiento().banco_externo()  || '');
            referencia = referencia.concat(' ', ctx.movimiento().cuenta_externa() || '');
        }

        ctx.movimiento().referencia(referencia)
    }
}


var properties = {
    fecha : {},
    tercero_id : {},
    cuenta_id : {},
    factor : {},

    moneda : {},
    tasa_de_cambio : {},
    tipo_de_cambio : {},
    total : {},

    referencia : {},
    metodo_pago_id : {},
    cuenta_externa : {},
    banco_externo : {},
    no_cheque : {},
    fecha_cheque : {},
    conciliarCon : {}
};

function Movimiento (data) {
    data  || (data = {});
    var $this = this;

    Object.keys(properties).forEach(function (prop) {
        var val = f(prop)(data);
        if(typeof val == 'undefined') val = null;
        $this[prop] = m.prop(val);
    });

    $this.detalles = m.prop([]);

    $this.detalles.toJSON = function () {
        return this().filter(function (detalle) {
            return Number(detalle.importe())
        })
    };

    $this.$isNew = m.prop(false);
    $this.$isNew.toJSON = function () { return undefined }
}
