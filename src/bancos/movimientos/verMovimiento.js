module.exports = {
    view : VerMovimientoView,
    controller : VerMovimientoController
};

function VerMovimientoController (args) {
    var ctx = this;
    ctx.movimiento = m.prop(null);
    ctx.movimiento.cargando = m.prop(false);

    oor.fondoGris();

    ctx.cargarMovimiento = function () {
        if( ctx.movimiento() || ctx.movimiento.cargando() ) return;
        ctx.movimiento.cargando(true);

        oorden().then(function () {
            oor.request('/movimientos/getMovimiento/' + args.idMovimiento)
                .then(ctx.movimiento)
                .then(ctx.movimiento.cargando.bind(null, false))
        });
    }



}

function VerMovimientoView (ctx) {
    ctx.cargarMovimiento();
    if(ctx.movimiento.cargando()) return oor.loading();
    return VerMovimientoView[ctx.movimiento().clase](ctx);
}

VerMovimientoView.TRASPASO = function (ctx) {
    var traspaso = ctx.movimiento().traspaso;
    var origen = traspaso.origen;
    var destino = traspaso.destino;
    var montoOrigen = Math.abs(origen.total);
    var montoDestino = Math.abs(destino.total);

    return m('div', [
        m('h3.column-title', 'Traspaso entre cuentas'),
        m('.flex-row', [
            m('.mt-inputer', [
                m('label', 'Fecha: '),
                origen.fecha
            ]),
            m('.mt-inputer', [
                m('label', 'Fecha: '),
                origen.referencia
            ])
        ]),

        m('.row', [
            m('.col-sm-5', {'style' : 'background:#f2f2f2; padding:10px; border-radius:4px'}, [
                m('.mt-inputer.line', [
                    m('label','Cuenta Origen: '),
                    origen.cuenta.cuenta,
                    ' - ',
                    origen.cuenta.nombre
                ]),

                m('.row', [
                    m('.mt-inputer.line', [
                        m('label', 'Monto Origen ', origen.moneda, ': '),
                        oorden.organizacion.format(montoOrigen)
                    ])
                ])
            ]),

            m('.col-sm-2.text-center', [
                m('i.ion-android-arrow-forward.text-grey',{style:'font-size:4em'})
            ]),

            m('.col-sm-5', {'style' : 'background:#f2f2f2; padding:10px; border-radius:4px'}, [
                m('.mt-inputer.line',[
                    m('label','Cuenta Destino: '),
                    destino.cuenta.cuenta,
                    ' - ',
                    destino.cuenta.nombre
                ]),

                m('.row', [
                    m('.mt-inputer.line', [
                        m('label', 'Monto Destino ', destino.moneda, ': '),
                        m('div.pull-right', oorden.organizacion.format(montoDestino))
                    ])
                ])
            ])
        ])
    ]);
}

VerMovimientoView.BANCARIO = function (ctx) {
    var total = Number(  ctx.movimiento().total );
    var format = oorden.organizacion.numberFormat();
    var movimiento = ctx.movimiento();
    var metodoPago = null;

    if(movimiento.metodo_pago_id) {
        metodoPago = oorden.metodoDePago(movimiento.metodo_pago_id);
    }

    return m('.panel', [
        m('.panel-heading', [
            m('.panel-title', [
                m('.panel-buttons', [
                    m('a.btn.btn btn-warning button-striped button-full-striped  btn-ripple btn-xs',{
                        href:'javascript:;',
                        onclick:desaplicarPago(ctx)
                    },'Eliminar y Desaplicar'),
                ]),

                m('.row', [
                    m('h4', [
                        ctx.movimiento().total > 0 ? 'Ingreso' : 'Egreso',
                        ' en ',
                        m('a.text-indigo', {href:'/bancos/banco/' + ctx.movimiento().cuenta_id }, ctx.movimiento().cuenta.nombre),
                        ' por ',
                        format(Math.abs(total))
                    ])
                ])
            ]),
        ]),


        m('.panel-body', [
            m('br'),

            m('.row', [
                m('.col-md-8', [
                    m('div', movimiento.referencia),
                    m('div', movimiento.fecha),
                    m('div', metodoPago ? metodoPago.metodo : ''),
                    m('div', movimiento.cuenta_externa),
                    m('div', movimiento.banco_externo)
                ]),



                m('.col-md-4', [
                    movimiento.conciliacion ? VerConciliacion(ctx) : null
                ]),
            ]),

            m('br'),

            TablaDetallesView(ctx)
        ]),

    ]);
}



function VerConciliacion (ctx) {
    var movimiento = ctx.movimiento();
    var conciliacion = movimiento.conciliacion;

    return [
        m('h6.small', {style:'padding-bottom:4px; margin-bottom:4px'}, [
            'Conciliado '.concat(
                movimiento.conciliacion.movimientos == 1 ? '' : 'junto con '.concat(movimiento.conciliacion.movimientos - 1, 'movimientos más '),
                'con:'
            )
        ]),

        VistaConciliacion({
            conciliacion : conciliacion,
            before : m('.pull-right', [
                oor.stripedButton('button.btn-warning', 'Desconciliar', {
                    onclick:desconciliar(ctx),
                    style:'font-size:16px !important; zoom:0.7'
                })
            ])
        })
    ];
}


function VistaConciliacion (data) {
    var conciliacion = data.conciliacion;

    return m('div', {style:'border:1px solid #f0f0f0; border-radius:4px; padding:4px; height:6em'},[

        data.before ? data.before : null,

        m('h6.text-indigo.small', {style:'margin-bottom:6px; margin-top:6px'}, [
            conciliacion.concepto
        ]),

        m('.small', [
            m('.text-grey.small', [
                conciliacion.referencia
            ]),

            m('.text-grey.small', [
                conciliacion.descripcion
            ])
        ]),

        m('div.small', [
            m('span.pull-right.text-grey', [
                oorden.org.format(conciliacion.monto)
            ]),

            m('span.pull-left.text-grey', [
                conciliacion.fecha
            ])
        ]),

        m('.clear')
    ]);
}

function TablaDetallesView(ctx) {
    var movimiento = ctx.movimiento();
    var format = oorden.organizacion.numberFormat();

    return m('.table-responsive', [
        m('table.table.megatable', [

            m('thead', [
                m('tr', [
                    m('th', 'Concepto'),
                    m('th.text-right', 'Importe ' + movimiento.cuenta.moneda)
                ])
            ]),

            m('tbody', [
                ctx.movimiento().detalles.map(function (detalle) {
                    var mostrarMontoPago = detalle.moneda_pago && detalle.moneda_pago != movimiento.cuenta.moneda;
                    return m('tr', [
                        m('td', [
                            detalleCaption(detalle),
                            m('div.small.text-grey', detalle.concepto)
                        ]),
                        m('td.text-right',[
                            format(detalle.importe),
                            mostrarMontoPago ? m('div.small.text-grey', [
                                format(detalle.monto_pago) , ' ', detalle.moneda_pago
                            ]) : null
                        ])
                    ])
                })
            ]),

            m('tfoot', [
                m('th.text-right', 'Total ', movimiento.total > 0 ? 'Ingreso' : 'Egreso'),
                m('th.text-right', [
                    format(Math.abs(movimiento.total))
                ])
            ])
        ])
    ]);
}

function desconciliar (ctx) {
    if(!ctx.desconciliar) {
        ctx.desconciliar = { busy: m.prop(false) }
    }

    return function () {
        if(ctx.desconciliar.busy()) return;

        ctx.desconciliar.busy(true);

        var d = confirm('Al desconciliar este movimiento, se desconciliarán en total ' + ctx.movimiento().conciliacion.movimientos +  ' Movimientos ¿Estás seguro que quieres desconciliar?');
        if(!d) return ctx.desconciliar.busy(false);

        oor.request('/conciliacion/desconciliar/'.concat(ctx.movimiento().conciliacion_id), 'DELETE')
            .then(
                toastr.success.bind(toastr, '¡Movimiento Desconciliado!'),
                function () { }
            )
            .then(ctx.movimiento.bind(null,false))
            .then(ctx.desconciliar.busy.bind(null,false))
    }
}

function desaplicarPago (ctx) {
    if(!ctx.desaplicarPago) {
        ctx.desaplicarPago = { busy : m.prop(false)}
    }

    return function () {
        var movimientoId = ctx.movimiento().movimiento_banco_id;
        if(ctx.desaplicarPago.busy()) return;
        ctx.desaplicarPago.busy(true);

        oor.request('/movimientos/remover/' + movimientoId, 'DELETE')
            .then(function (d) {
                if(!d) return console.log(d);

                oor.lsMessage({type:'success', text:'Pago Desaplicado'});
                history.back();
            }, function (r) {
                toastr.error(r.status.message);
            })
            .then(ctx.desaplicarPago.busy.bind(null, false))
    }
}

function detalleCaption (detalle) {
    var Op = oor.mm('Operacion');
    if(detalle.tipo_movimiento == 'PAGAR_OPERACION') {
        return m('a', {
            href : Op.url(detalle.operacion)
        }, detalle.operacion.nombre_documento + ' ' + detalle.operacion.serie + detalle.operacion.numero)
    }
}
