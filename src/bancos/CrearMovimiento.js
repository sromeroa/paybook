var T3Component = require('../operaciones/components/Tercero');
var Movimiento = require('./MovimientoModel');

module.exports = {
    controller : CrearMovController,
    view : CrearMovView,
    openModal : openModal
};


function CrearMovController (params) {
    var ctx = this;
    ctx.egreso = m.prop(params.factor == -1)
    ctx.ingreso = m.prop(params.factor == 1)
    ctx.movimiento = m.prop()
    ctx.movimientos = m.prop([])
    ctx.tercero = m.prop();

    ctx.terceroComponent = m.component(T3Component, {
        tercero    : ctx.tercero,
        tercero_id : function ()  {
            arguments.length && ctx.movimiento().tercero_id(arguments[0]);
            return ctx.movimiento().tercero_id();
        },
        enabled    : m.prop(true),
        placeholder : 'Seleccionar...'
    });


    ctx.guardar = function () {
        var mov = nh.extend({}, ctx.movimiento(), {
            movimientos:ctx.movimientos()
        });

        console.log(JSON.parse(JSON.stringify(mov)));
        console.log(JSON.stringify(mov));
    }


    ctx.inicializar = function () {
        if(ctx.movimiento()) return;

        

        var movimiento = new Movimiento({
            fecha : oor.fecha.toSQL(new Date),
            factor : params.factor,
            cuenta_id : params.cuenta_id
        });

        ctx.movimiento(movimiento);

        /**
         * 
         */

        GenerarMovimientoBanco({
            fecha:movimiento.fecha(), 
            factor:movimiento.factor(), 
            cuenta_id:movimiento.cuenta_id() 
        }).then(function (d) {
            
            Object.keys(d).forEach(function (key) {
                var prop = ctx.movimiento()[key];

                if(nh.isFunction(prop)) {
                    prop( d[key] );
                }
            })
          
        })

    }
}

function CrearMovView(ctx) {
    ctx.inicializar();
    var movimiento = ctx.movimiento();

    return m('.row', [

        m('.col-sm-6', [  
            //m('h5', ctx.egreso() ? 'Receptor: ' : 'Emisor: '),
            ctx.terceroComponent,

            m('.flex-row', [
                m('.mt-inputer', {style:'flex:1 2'}, [
                    m('label', 'Fecha'),
                    m('input[type=date]', {
                        value : movimiento.fecha()
                    })
                ]),
                m('.mt-inputer', {style:'flex:1 2'}, [
                    m('label', 'Monto'),
                    m('input[type=text].text-right', {
                        size : '12',
                        value : movimiento.monto()
                    })
                ]),
            ]),

            /*
            m('.flex-row', [
                m('.mt-inputer.line', [
                    m('label', 'Método de Pago'),
                    m('input[type=text]')
                ])
            ]),

            //Sólo si la moneda es extranjera
            /*
            m('.flex-row', [
                m('.mt-inputer.line', {style:'flex : 1 4'}, [
                    m('label', 'Tipo De Cambio'),
                    m('input[type=text]', {

                    })
                ]),

                 m('.mt-inputer.line', {style:'flex : 1 4'}, [
                    m('label', 'Tasa De Cambio'),
                    m('input[type=text]')
                ])
            ])
            */
        ]),

        m('.col-sm-6', [
            m.component(AsignarMovimientos, {
                key : 'tercero:'.concat( movimiento.tercero_id() ),
                movimiento : ctx.movimiento,
                movimientos : ctx.movimientos
            })

        ])
    ])
}


function openModal (params) {
    MTModal.open({
        controller : CrearMovController,
        args : params,
        bottom : function (ctx) {
            return [
                m('button.pull-left.btn-flat.btn-sm.btn-default', {
                    onclick : ctx.$modal.close
                }, 'Cancelar'),

                 m('button.pull-right.btn-flat.btn-sm.btn-success', {
                    onclick : ctx.guardar
                }, params.title)
            ];
        },
        top : function () {
            return m('h4', params.title)
        },
        content : CrearMovView,
        el : this
    })
}




var AsignarMovimientos = {
    controller : AsignarMovimientosController,
    view : AsignarMovimientosView
};



function AsignarMovimientosController (params) {
    var ctx = this;

    this.movimiento = params.movimiento;

    this.movimientos = params.movimientos

    this.pagos = m.prop([]);

    this.documentos = m.prop();

    this.tiposDeCambio = m.prop({});

    this.buscarMovimiento = function (opId) {
        return this.movimientos().filter( f('operacion_id').is(opId) )[0];
    }

    this.crearMovimiento = function (documento) {
        var monto = f('saldo')(documento)
        var tasa = ctx.obtenerTasa(documento.moneda)
        var movimiento =  ctx.buscarMovimiento( f('operacion_id')(documento) )

        if(movimiento) {
            movimiento = ctx.movimientos().indexOf(movimiento);
            ctx.movimientos().splice(movimiento,1);
            ctx.calcularMonto()
            return;
        } 


        movimiento = new Movimiento ({
            monto : oorden.organizacion.round(monto*tasa),
            monto_pago : oorden.organizacion.round(monto),

            tasa_de_cambio_inter : tasa,
            tasa_de_cambio_pago : 1/tasa,

            moneda_pago : f('moneda')(documento),
            operacion_id : f('operacion_id')(documento),
            factor : f('factor')(ctx.movimiento()),
            tipo_movimiento : 'PAGAR_OPERACION'
        });


        movimiento.fecha = ctx.movimiento().fecha;
        movimiento.tercero_id = ctx.movimiento().tercero_id;
        movimiento.moneda = ctx.movimiento().moneda;
        movimiento.tipo_de_cambio = ctx.movimiento().tipo_de_cambio;
        movimiento.tasa_de_cambio = ctx.movimiento().tasa_de_cambio;
        movimiento.cuenta_id = ctx.movimiento().cuenta_id

        ctx.movimientos().push(movimiento);
        ctx.calcularMonto()
    }

    this.obtenerTasa = function (moneda) {
        return ctx.tiposDeCambio()[moneda].tasa()
    }


    this.calcularMonto = function () {
        var monto = ctx.movimientos().reduce(function (acum, mov) {
            return acum +  f('monto')(mov) * f('factor')(mov);
        }, 0);

        ctx.movimiento().monto( f('factor')(ctx.movimiento()) * monto);
    }


    ctx.asignarTiposDeCambio = function (tiposDeCambio) {
        var tipos = {};

        tiposDeCambio.forEach(function (tc) {
            tipos[f('codigo_moneda')(tc)] = {
                codigo_moneda : m.prop( tc.codigo_moneda ),
                tasa : m.prop(tc.tasa.tasa)
            }
        });

        tipos['MXN'] = {
            codigo_moneda : m.prop('MXN'),
            tasa : m.prop(1)
        }

        ctx.tiposDeCambio(tipos)
    }

    this.inicializar = function () {
        if(ctx.documentos()) return;
        ctx.documentos([]);

        if(!ctx.movimiento().tercero_id() ) return;

        oorden().then(function () {
            return m.request({
                method : 'GET',
                url : '/movimientos/documentos',
                data : {
                    factor : ctx.movimiento().factor(),
                    tercero_id : ctx.movimiento().tercero_id(), 
                    moneda : 'MXN'
                },
                unwrapSuccess : f('data')
            })
            
        })
        .then(ctx.documentos)
        .then(function () {
            return m.request({
                url : '/monedas/tasasFecha/' + ctx.movimiento().fecha(),
                method : 'GET',
                unwrapSuccess : f('data')
            })
        })
        .then(ctx.asignarTiposDeCambio)
       
    }

}




function GenerarMovimientoBanco (data) {
    return m.request({
        method : 'GET',
        url : '/bancos/generar/',
        data : data,
        unwrapSuccess : f('data')
    })
}


/**
 *
 *
 */

function AsignarMovimientosView (ctx) {
    ctx.inicializar();

    return m('div',  [
        m('h5', 'Aplicar a:'),

        ctx.documentos().map(function (doc) {
            var format = oorden.organizacion.numberFormat();
            var mov = ctx.buscarMovimiento(doc.operacion_id);
            var saldo = format(doc.saldo) || 0;
            var tasaDeCambio = ctx.obtenerTasa(doc.moneda)
            var saldoMonedaCuenta = format(doc.saldo * tasaDeCambio)
            var value = mov ? format(mov.monto()) : ''

            return m('div.aplicacion-pago', {'class' : mov ? 'con-pago' : ''}, [
                
                m('label', [
                    m('input[type=checkbox]', {
                        onchange : m.withAttr('checked', function () { ctx.crearMovimiento(doc) }),
                    }), ' ', f('fecha')(doc), ' ', f('documento')(doc)
                ]),

                m('.row', [
                    m('.col-xs-6', [
                        m('small.text-grey', f('referencia')(doc)),
                        m('div', m('span.text-grey', 'Vence: ', f('fecha_vencimiento')(doc)))
                    ]),
                    m('.col-xs-6', [
                        m('div.text-right', [
                            m('small.text-grey', 'Saldo ', f('moneda')(doc), ': '),
                            saldo
                        ]),

                        m('div.text-right', [
                            m('small.text-grey', 'Saldo ', f('moneda')(ctx.movimiento()), ': '),
                            saldoMonedaCuenta
                        ]),

                        m('div.text-right', [
                            m('small.text-grey', 'Aplicar ', f('moneda')(ctx.movimiento()), ': '),
                            m('input[type=text].text-right', {
                                size : saldo.length + 1,
                                value : value
                            })
                        ])
                    ])
                ])

            ])
        }),

        ctx.tiposDeCambio() ? Object.keys(ctx.tiposDeCambio()).map(function (moneda) {
            var tasa = ctx.tiposDeCambio()[moneda];
            return m('div.text-right' , '1 ', tasa.codigo_moneda(), ' = ', tasa.tasa() , ' MXN');
        }) : null,
    ])
}

