module.exports = TransaccionesTabla;

var dynTable = require('../nahui/dtable');
var Field = require('../nahui/nahui.field');
var TableHelper = require('../nahui/dtableHelper');
var nahuiCollection = require('../nahui/nahui.collection');

function TransaccionesTabla (ctx) {
    var table = dynTable().key( f('movimiento_banco_id') );
    var collection = nahuiCollection('operaciones', { identifier : 'movimiento_banco_id' });
    var query = collection.query({});

    /**
     * Fields
     */

    var fields = {};
    var nFormat = d3.format(",.2f");
    var nZeroFormat = function (d) { return d === null ? '--' : nFormat(d) }


    fields.poliza_partida_id = Field('movimiento_banco_id');

    fields.fecha = Field('fecha', {
        caption : 'Fecha',
        filter : oor.fecha.prettyFormat
    });

    fields.concepto = Field.linkToResource('referencia', {
        class : 'text-indigo',
        url : function (m) { return '/movimientos/movimiento/' + m.movimiento_banco_id},
        caption : 'Referencia'
    });

    fields.tercero = Field('tercero', {
        caption : 'De/Para'
    });

    fields.seccion = Field('seccion', {
        caption : 'Sección'
    });

    fields.montoPagado = Field('montoPagado', {
        caption : 'Egreso',
        'class' : 'text-right',
        'headingClass' : 'text-right',
        value : function (d) {
            var v = Number(f('total')(d));
            return v < 0 ? Math.abs(v) : null
        },
        filter : nZeroFormat
    });

    fields.montoRecibido = Field('montoRecibido', {
        caption : 'Ingreso',
        'class' : 'text-right',
        'headingClass' : 'text-right',
        value : function (d) {
            var v = Number(f('total')(d));
            return v > 0 ? Math.abs(v) : null
        },
        filter : nZeroFormat
    });


    fields.conciliacion = Field('conciliacion_bancaria_id', {
        caption : 'Conciliado',
        headingClass : 'text-center',
        'class' : 'text-right',
        enter : function (selection) {
            selection.call(Field.enter);
        },
        update : function (selection) {
            selection.html(function (d) {
                return  '<i data-conciliacion="" title="' + (d.row.conciliacion_info || '') + '" class="' + (d.value ? 'text-green fa-check-circle' : 'text-grey fa-minus-circle') + ' fa" style="font-size:1.5em"></i>'
            });
        }
    });


    table.columns = [ fields.fecha, fields.tercero, fields.concepto, fields.montoRecibido, fields.montoPagado, fields.conciliacion ];

    table.helper = TableHelper()
                    .table(table)
                    .query(query)
                    .columns(table.columns)
                    .set({ pageSize : 50 })

    table.collection = collection;
    return table;
}
