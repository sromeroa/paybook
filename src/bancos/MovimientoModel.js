module.exports = MovimientoModel;

var properties = {
    fecha : {},
    tercero_id : {},
    cuenta_id : {},
    factor : {},
    operacion_id : {},

    moneda : {},
    tipo_movimiento : {},
    tasa_de_cambio : {},
    tipo_de_cambio : {},
    monto : {},

    moneda_pago : {},
    monto_pago : {},
    tasa_de_cambio_pago : {},
    tipo_de_cambio_pago : {},
    tasa_de_cambio_inter : {},
};

function MovimientoModel (data) {
    data  || (data = {});
    var $this = this;

    Object.keys(properties).forEach(function (prop) {
        var val = f(prop)(data);

        if(typeof val == 'undefined') {
            val = null;
        }

        $this[prop] = m.prop(val);
    })


    $this.$isNew = m.prop(false);
}