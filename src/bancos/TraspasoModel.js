module.exports = TraspasoModel;

var fields =  {
    cuenta_origen : {},
    cuenta_destino : {},

    monto_origen : {},
    monto_destino : {},

    moneda_origen : {},
    moneda_destino : {},

    fecha : {},
    referencia : {}
};


function TraspasoModel (data) {
    var cat = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(nh.isFunction(fieldDef.filter)){
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined') {
            fieldVal = null;
        }

        cat[k] = m.prop(fieldVal || null);
    });

    cat.$isNew = m.prop(false);
}


TraspasoModel.generar = function (data) {
    return oor.request('/movimientos/generarTraspaso', 'GET', {data : data});
}
