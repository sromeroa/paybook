module.exports = {controller : ConcilarController, view : ConciliarView};

/**
 * Para controlar el flujo de conciliación:
 *  - Creación de movimiento
 *  - Conciliación con movimiento
 *  - Conciliación con operacion
 **/

function ConcilarController (args) {
    var ctx = this;
    //El registro de conciliacion
    ctx.registro = m.prop(args.registro);
    //La cuenta bancaria
    ctx.cuenta = m.prop(args.cuenta);
    //El movimiento conciliado
    ctx.movimiento  = m.prop();
    //Las operaciones que hacen match
    ctx.operaciones = m.prop();

    ctx.loading     = m.prop(false);
    ctx.moneda      = m.prop(ctx.cuenta().moneda);
    ctx.busqueda    = m.prop('')

    ctx.criterios   = m.prop({
        conciliacion_id : ctx.registro().conciliacion_bancaria_id,
        busqueda : ctx.busqueda,
        moneda : ctx.moneda
    });

    ctx.paso          = m.prop(0);

    ctx.coincidencias = args.coincidencias;

    ctx.inicializarCaptura = function () {
        ctx.paso(1);
    }

    ctx.recargarOperaciones = function () {
        ctx.operaciones(null);
        ctx.cargarOperaciones();
    }


    ctx.cargarOperaciones = function () {
        if(ctx.loading() || ctx.operaciones()) return;
        oor.loading(true);

        var criterios = ctx.criterios();
        criterios = JSON.parse(JSON.stringify(criterios));

        oor.request('/conciliacion/operaciones/','GET',{
            data: criterios ? JSON.parse(JSON.stringify(criterios)) : undefined
        }).then(function (d) {
            ctx.operaciones(d.operaciones);
            ctx.loading(false);
            ctx.incluidas = [];
        });
    };

    ctx.incluida = function (operacion) {
        if(!ctx.incluidas) return false;
        return Boolean(ctx.incluidas.indexOf(operacion.operacion_id) + 1);
    }


    ctx.incluir = function (operacion, value) {
        if(value && ctx.incluida(operacion) == false) {
            ctx.incluidas.push( operacion.operacion_id );
        }
        if(!value && ctx.incluida(operacion)) {
            ctx.incluidas.splice(ctx.incluidas.indexOf(operacion.operacion_id),1);
        }
    }

    ctx.conciliarFactura = function (idOperacion) {
        ctx.incluidas = [idOperacion];
        ctx.iniciarMovimiento();
    }



    ctx.iniciarMovimiento = function () {
        ctx.paso(2);


        ctx.movComponent = m.component(bancos.movimiento, {
            movimientos : ctx.incluidas.map(_.identity),
            ingreso     : Boolean(ctx.registro().monto > 0),
            egreso      : Boolean(ctx.registro().monto < 0),
            cuentaId    : ctx.cuenta().cuenta_contable_id,
            fecha       : ctx.registro().fecha,
            fijarMonto  : Math.abs(ctx.registro().monto),
            afterSave   : function (movimiento) {
                ctx.movimiento(movimiento);
                ctx.paso(0);
            },
            ajustable  : true,
            conciliarCon : ctx.registro().conciliacion_bancaria_id
        });
    }
}


/**
 * Vista Inicial de la conciliación falta  mostrar las conincidencias
 */
function ConciliarView (ctx) {
    var cargandoCoincidencias = ctx.coincidencias.cargando();
    var coincidencias;

    if(cargandoCoincidencias == false)
    {
        coincidencias = ctx.coincidencias();
    }

    return m('div', {style:'border:1px solid #f0f0f0; border-radius:4px; padding:4px; background:#f2f2f2; margin:4px'}, [
        m('.row', [
            m('.col-md-5', VistaConciliacion({ registro : ctx.registro() }) ),
            m('.col-md-7', [

                ctx.paso() == 0 ? [
                    ctx.movimiento() ? [
                        m('div', 'Ya Conciliado')
                    ] : [
                        cargandoCoincidencias || !coincidencias ? oor.loading() : [

                            oor.stripedButton('[style="zoom:0.8"].btn-primary.pull-right', 'Aplicar A...', { onclick : ctx.inicializarCaptura }),
                            coincidencias.operaciones.length ? m('h6', 'Operaciones:') :null,

                            coincidencias.operaciones.slice(0,3).map(function (op) {
                                return m('div', [
                                    m('div', [
                                        m('a.text-indigo', op.nombre_documento , ' ', op.serie , ' ', op.numero , 'de ', op.nombre_tercero),
                                        ' ',
                                        oor.stripedButton('button.btn-success[style="zoom:0.7"]','Conciliar', {
                                            onclick : ctx.conciliarFactura.bind(null, op.operacion_id)
                                        })
                                    ]),
                                    m('div.small.text-grey', {style:'margin-left:12px'}, op.referencia),
                                    m('div.text-grey', {style:'margin-left:12px'}, [
                                        'Saldo: ',
                                        oorden.org.format(op.saldo),
                                        ', Fecha: ', op.fecha,

                                    ])
                                ])
                            }),

                        ]
                    ]
                ] : null,

                ctx.paso() == 1 ? [
                    m('.flex-row', [
                        m('input', {placeholder : 'Buscar', value : ctx.busqueda(), onchange : m.withAttr('value', ctx.busqueda) })
                    ]),

                    m('label', [
                        m('input[type=checkbox]', { checked : Boolean(ctx.moneda()), onchange : m.withAttr('checked', function (v) { ctx.moneda( (v && ctx.cuenta().moneda) || undefined ) } )}),
                        ' Mostrar operaciones solo en '.concat( ctx.cuenta().moneda )
                    ]),

                    oor.stripedButton('.btn-success.pull-right', 'Buscar', {
                        onclick : ctx.recargarOperaciones
                    })

                ] : null,

            ])
        ]),

        ctx.paso() > 0 ? [m('br'), PantallaRegistros(ctx)] : null
    ])
}


/**
 * Muestra las operaciones para seleccionar y crear el movimiento
 */
function PantallaRegistros (ctx) {
    if(ctx.paso() == 1) {
        ctx.cargarOperaciones();
    }

    return ctx.loading() ? oor.loading() : m('.row', [
        m('.col-md-12', ctx.paso() == 2 ? ctx.movComponent : [
            m('h4', 'Seleccionar operaciones para aplicar'),

            ctx.operaciones() ? m('table.table.megatable', [
                m('thead', [
                    m('tr', [
                        m('th',''),
                        m('th','Fecha'),
                        m('th','Operación'),
                        m('th', 'Tercero'),
                        m('th.text-right','Total'),
                        m('th.text-right','Saldo')
                    ])
                ]),

                m('tbody', [
                    ctx.operaciones().map(function (op) {
                        return m('tr', {'class' : ctx.incluida(op) ? 'info' : ''}, [

                            m('td', [
                                m('input[type=checkbox]', {
                                    onchange : m.withAttr('checked', ctx.incluir.bind(null,op) )
                                })
                            ]),

                            m('td', [
                                op.fecha
                            ]),

                            m('td', [
                                op.nombre_documento, ' ',
                                op.serie ? op.serie : '',
                                op.numero ? op.numero : ' <auto>',
                                m('.small.text-grey', op.referencia)
                            ]),

                            m('td', op.nombre_tercero),

                            m('td.text-right', [
                                oorden.org.format(op.total),
                                m('div.text-grey.small', op.moneda)
                            ]),

                            m('td.text-right', [
                                m('a.text-indigo[href=javascript:;]', oorden.org.format(op.saldo))
                            ])
                        ])
                    })
                ])
            ]) : null,

            oor.stripedButton('button.pull-right.btn-success', 'Siguiente', {
                onclick : ctx.iniciarMovimiento
            }),
        ]),
    ])
}


var CONCILIACION_STYLE = 'border:1px solid #f0f0f0; border-radius:4px; padding:4px; height:6em; background:white;';

/**
 * Vista de la pantalla de conciliación
 *  "cuadrito donde se muetra el registro"
 */
function VistaConciliacion (data) {
    var conciliacion = data.registro;
    return m('div', {style:CONCILIACION_STYLE},[
        data.before ? data.before : null,

        m('h6.text-indigo.small', {style:'margin-bottom:6px; margin-top:6px'}, [
            conciliacion.concepto
        ]),

        m('.small', [
            m('.text-grey.small', [
                conciliacion.referencia
            ]),
            m('.text-grey.small', [
                conciliacion.descripcion
            ])
        ]),

        m('div.small', [
            m('span.pull-right.text-grey', [
                oorden.org.format(conciliacion.monto)
            ]),

            m('span.pull-left.text-grey', [
                conciliacion.fecha
            ])
        ]),

        m('.clear')
    ]);
}
