module.exports = {
    index : require('./BancosIndex'),
    verBanco : require('./VerBanco'),
    movimiento : require('./movimiento'),
    verMovimiento : require('./movimientos/verMovimiento'),
    traspaso : require('./traspaso'),
    conciliacionCuenta : require('./conciliacion/conciliarCuenta')
};
