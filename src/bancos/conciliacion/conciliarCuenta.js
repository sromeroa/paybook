module.exports = {
    controller : ConciliarCuentaController,
    view : ConciliarCuentaView
};

var InfoDatos = {
    _           : {value : '_', caption : 'Sin Seleccionar'},
    fecha       : {value : 'fecha', caption : 'Fecha', parse : parser({required:true, parser:getFecha})},
    folio       : {value : 'folio', caption : 'Folio', parse : parser({required:true})},
    referencia  : {value : 'referencia', caption : 'Referencia', parse : parser({required:true})},
    descripcion : {value : 'descripcion', caption : 'Descripción', parse:parser({required:false})},
    concepto    : {value : 'concepto', caption : 'Concepto', parse : parser({required:false})},
    monto       : {value : 'monto', caption : 'Monto', parse:parser({required:true, parser : getMonto})}
};

function parser (data) {
    data || (data = {});
    var get = data.parser;

    if(!get) get = data.required ? requiredDefaultGet : defaultGet;

    return function (val) {
        var prop = m.prop(null);
        prop.error = m.prop(false);
        get(val, prop, prop.error);
        return prop;
    }
}

function defaultGet (val, prop, error) {
    var value = String(val || '');
    prop(value);
}

function requiredDefaultGet (val, prop, error) {
    defaultGet(val, prop, error);
    error(prop() == '' ? 'Dato Requerido' : false)
}

function getFecha (val, prop, error) {
    var value;
    value = String(val).split('/').join('-')

    if( oor.fecha.validate(value) ) {
        prop(value)
    } else {
        error('La fecha tiene un formato incorrecto');
    }
}

function getMonto (val, prop, error) {
    var value;
    value = String(val);
    if(value == Number(val)) {
        prop(value);
    } else {
        prop(val);
        error('Esto no es un número');
    }
}

function getFormatoArray (formato) {
    return formato.map(f('value'))
}



function ConciliarCuentaController (params) {
    var ctx = this;
    oor.fondoGris();

    ctx.cuenta = m.prop(params.cuenta)

    ctx.cuenta.cargando = m.prop(false)
    ctx.cuentaId = m.prop(params.cuentaId);
    ctx.datosArchivo = m.prop();
    ctx.paso = m.prop(0);

    ctx.datosProcesados  = m.prop();
    ctx.datosProcesados.errors = m.prop(false);

    ctx.formato = m.prop();
    ctx.formatoChanged = m.prop(false);

    ctx.resultados = m.prop();
    ctx.init = function () {

    }

    function formatoEnBlanco (num) {
        var arr = [];
        for(i=0;i<num;i++){
            arr.push(InfoDatos['_']);
        }
        return arr;
    }

    ctx.validarFormato = function (num) {
        var formato, iFormato = ctx.cuenta().formato_importacion;
        var formatoOrigen = formatoEnBlanco(num)
        ctx.formato(formatoOrigen);

        if(!iFormato){
            console.log('no iFormato', iFormato);
            return false;
        }

        iFormato = iFormato.split(',');

        if(iFormato.length > num) {
            console.log(iFormato.length, '>', num)
            return false;
        }

        iFormato.forEach(function (d, i) {
            formatoOrigen[i] = InfoDatos[d] || InfoDatos['_'];
        });


        if(iFormato.length == num){
            console.log('todo OK')
            return true;
        }

        return false;
    };


    ctx.procesarArchivo = function (datos) {
        datos = datos.filter(function (d, i) {
            if(d.length > 1) return true;
            if(i == datos.length -1) return false;
            return true;
        });

        ctx.datosArchivo(datos);

        if(ctx.validarFormato(10)) {
            ctx.procesarDatos();
        } else {
            ctx.paso(1);
        }
    }

    ctx.procesarDatos = function () {
        var errors = [];

        var datos = ctx.datosArchivo().map(function (row) {
            var dato = {};

            dato.errors = m.prop([]);
            dato.errors.toJSON = function () {  };

            ctx.formato().forEach(function (celda, i) {
                if(celda.value == '_') return;
                var valor = row[i];
                var prop  = celda.parse(row[i]);
                var error = prop.error();

                dato[celda.value] = prop;

                if(error) {
                    dato.errors().push(error);
                }
            });

            if(dato.errors().length) {
                errors.push(dato.errors())
            }

            return dato;
        });

        ctx.datosProcesados(datos);
        ctx.datosProcesados.errors(errors.length ? errors : false);

        if(ctx.datosProcesados.errors()) {
            ctx.paso(2);
        } else {
            ctx.paso(3);
        }
    }

    ctx.subirEstadoDeCuenta = function () {
        if(ctx.paso() == 4) return;

        var data = {
            cuenta_id : ctx.cuentaId(),
            registros : ctx.datosProcesados(),
            formato   :  getFormatoArray(ctx.formato()).join(',')
        };

        ctx.paso(4);

        oor.request('/conciliacion/agregarRegistros', 'POST', {data : data})
            .then(function (d) {
                ctx.paso(5);
                ctx.resultados(d.registros);
            });
    }
}



function ConciliarCuentaView (ctx, params) {
    ctx.init();
    return oor.panel({
        title : 'Subir estado de cuenta para conciliación'
    }, [
        m('h3', ctx.cuenta().cuenta , ' ' , ctx.cuenta().nombre),
        ctx.paso() == 0 ? SubirArchivo(ctx) : null,
        ctx.paso() == 1 ? TablaDatosArchivo(ctx)  : null,
        ctx.paso() == 2 ? TablaDatosProcesados(ctx, true) : null,
        ctx.paso() == 3 ? TablaDatosProcesados(ctx) : null,
        ctx.paso() == 4 ? oor.loading() : null,
        ctx.paso() == 5 ? MostrarResultados(ctx) : null
    ]);
}



function TablaDatosProcesados (ctx, errors) {
    return m('div', [
        ctx.datosProcesados.errors() ? m('.alert.alert-danger', [
             'Se han encontrado errores en la(s) siguiente(s) '.concat( ctx.datosProcesados.errors().length, ' filas' )
        ]) : null,

        m('table.megatable.table', [
            m('thead', [
                m('th', '#'),
                Object.keys(InfoDatos).map(function (d) {
                    if(d == '_') return null;
                    return m('th', InfoDatos[d].caption);
                })
            ]),
            m('tbody', [
                ctx.datosProcesados().map(function (fila, i) {
                    if(errors && fila.errors().length == 0) return null;

                    return m('tr', {class : fila.errors().length ? 'danger' : ''}, [
                        m('td', i + 1),
                        Object.keys(InfoDatos).map(function (d) {
                            if(d == '_') return null;
                            return m('td', fila[d](), fila[d].error() ? m('.text-red', fila[d].error() ) : null);
                        })
                    ])
                })
            ])
        ]),

        m('div', [
            oor.stripedButton('button.btn-primary.pull-left', 'Volver a cargar archivo', {
                style : 'margin:5px',
                onclick : ctx.paso.bind(null,0)
            }),

            oor.stripedButton('button.btn-primary.pull-left', 'Configurar formato de Importación', {
                style : 'margin:5px',
                onclick : ctx.paso.bind(null,1)
            }),

            ctx.datosProcesados.errors() ? null : oor.stripedButton('button.btn-success.pull-right', 'Subir Estado de Cuenta '.concat('(', ctx.datosProcesados().length  , ' Filas)'), {
                style : 'margin:5px',
                onclick : ctx.subirEstadoDeCuenta
            })
        ])
    ]);
}



function SelectorDatoFormato (ctx, i) {
    var formato = ctx.formato();
    var datoActual = formato[i] || InfoDatos['_'];

    if(datoActual.value != '_') {
        return [
            m('.pull-left',datoActual.caption),
            m('a.text-grey[style=font-size:1.2em][href=javascript:;]', {
                onclick:function () { formato[i] = InfoDatos['_'] }
            }, m.trust('&times;')),
        ];
    }

    var options = Object.keys(InfoDatos).filter(function (d) {
        var dato = InfoDatos[d];
        if(d == '_') return true;
        if(formato[i] == dato) return true;
        if(formato.indexOf(dato) == -1) return true;
        return false;
    }).map(function (d) {
        return m('option', {value:d}, d);
    });

    return  options.length > 1 ? m('select', {
        onchange : m.withAttr('value', function (d) {
            formato[i] = InfoDatos[d];
            ctx.formatoChanged(true);
        }),
        value : formato[i]
    },options) : '';
}

function MostrarResultados (ctx) {
    return m('.alert.alert-success', 'Se han subido '.concat( ctx.resultados().length, ' registros') )
}



function TablaDatosArchivo (ctx) {
    var datosArchivo = ctx.datosArchivo();
    return m('div', [
        m('span', 'Seleccionar Columnas:'),
        Object.keys(InfoDatos).map(function (d) {
            if(d == '_') return null;
            var dato = InfoDatos[d];
            return m('span.label', {
                style : 'margin:5px',
                'class' : ctx.formato().indexOf(dato) > -1 ? 'label-success' : 'label-default'
            }, dato.caption);
        }),

        m('br'),
        m('br'),

        m('table.table.megatable', [
            m('thead', [
                m('tr', [
                    m('td', '#'),
                    datosArchivo[0].map(function (e,i) {
                       return m('td', SelectorDatoFormato(ctx,i))
                   })
                ])
            ]),
            m('tbody', [
                ctx.datosArchivo().map(function (d,i) {

                    return m('tr', m('td', i+1), d.map(function (e) {
                        return m('td', e)
                    }))
                })
            ])
        ]),

        m('br'),

        m('.pull-right',
            oor.stripedButton('button.btn-success','Siguiente Paso', {
                onclick:ctx.procesarDatos
            })
        ),
    ])
}

function SubirArchivo (ctx) {
    return m('div', [
        m('h6', 'Subir estado de cuenta en formato CSV'),
        m('.row', [
            m('input[type=file]', {
                config : ProcesarArchivo(ctx)
            })
        ])
    ]);
}

function ProcesarArchivo (ctx) {
    return function (el, isInited) {
        if(isInited) return;

        el.addEventListener('change', function () {
            Papa.parse(el.files[0], {
                encoding : 'UTF-8',
                complete : function (d) {ctx.procesarArchivo(d.data); m.redraw();}
            })
        })
    }
}
