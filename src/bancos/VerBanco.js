var TransaccionesTabla = require('./TransaccionesTabla');
var CrearMovimiento = require('./CrearMovimiento')
var PagarOperacion = require('../operaciones/components/PagarOperacionComponent');
var ConciliarRegistro = require('./ConciliarRegistro');


module.exports = {
    view : VerBancoView,
    controller : VerBancoController
};


var coincidencias = (function () {
    var map = {};

    rCoincidencias.obtenerDatos = function () {
        var idRegistros = Object.keys(map).filter(function (id) {
            return map[id]() === false
        });

        idRegistros.forEach(function (id) {
            var prop = map[id];
            prop.cargando(true);
            prop([]);
        });

        if(idRegistros.length) {
            setTimeout(function (d)  {
                oor.request('/conciliacion/coincidencias', 'POST', {
                    data : {registros : idRegistros}
                }).then(function (data) {
                    data.forEach(function (r) {
                        map[r.conciliacion_bancaria_id]( r.coincidencias )
                        map[r.conciliacion_bancaria_id].cargando(false);
                    })
                });
                m.redraw();
            },100);
        }
    };

    return rCoincidencias;

    function rCoincidencias (registro) {
        var id = registro.conciliacion_bancaria_id;
        if(!map[id]) {
            map[id] = m.prop(false);
            map[id].cargando = m.prop(false);
        }
        return map[id];
    };
})();

function VerBancoController (args) {
    var ctx = this;
    var tabla = TransaccionesTabla(ctx);

    oor.fondoGris();

    ctx.idBanco = m.prop(args.idBanco);
    ctx.banco = m.prop();
    ctx.banco.cargando = m.prop(false)
    ctx.transacciones = m.prop();
    ctx.activarConciliacion = m.prop(false);

    ctx.registrarEgreso = function () {
        ctx.registrarMovimiento.call(this,{
            factor : -1,
            title : 'Registrar Egreso'
        });
    }

    ctx.registrarIngreso = function () {
        ctx.registrarMovimiento.call(this,{
            factor : 1,
            'title' : 'Registrar Ingreso'
        });
    }

    ctx.registrarMovimiento = function (params) {
        CrearMovimiento.openModal.call(this, nh.extend({
            cuenta_id : ctx.idBanco()
        }, params));
    };


    ctx.conciliando = m.prop(false);

    function solicitudConciliacion (idMovimiento, method) {
        if(ctx.conciliando()) return;

        oor.request('/movimientos/conciliacionManual/' + idMovimiento, method)
            .then(function (data) {
                var transacciones = ctx.transacciones();
                var filterFn = f('movimiento_banco_id').is(data.movimiento_id);
                var mov = transacciones.filter(filterFn);

                mov[0].conciliacion_bancaria_id = data.conciliacion_bancaria_id;
                tabla.helper.draw();
            })
            .then(ctx.conciliando.bind(null, false));
    }

    ctx.desConciliar = function (idMovimiento) {
        return solicitudConciliacion(idMovimiento, 'DELETE');
    };

    ctx.conciliar = function (idMovimiento) {
        return solicitudConciliacion(idMovimiento, 'POST');
    };

    ctx.setupTable = function (element, isInited) {
        if(isInited) return;

        tabla.helper.element(element);
        tabla.helper.query().exec();

        setInterval(function () { $('[data-conciliacion]').tooltip(); },2000);
        $('[data-conciliacion]').tooltip();
    };

    ctx.obtenerBanco = function () {
        if(ctx.banco() || ctx.banco.cargando()) return false;
        ctx.banco.cargando(true);

        oor.request('/bancos/cuenta/' + ctx.idBanco(), 'GET')
        .then(function (d) {
            var transacciones = d.transacciones;
            delete(d.transacciones)
            ctx.transacciones(transacciones).forEach(tabla.collection.insert)
            ctx.banco(d);
            ctx.banco.cargando(false);
        });
    };

    ctx.inicializarConciliacion = Conciliacion(ctx);
}


function VerBancoView (ctx) {
    ctx.obtenerBanco();

    var banco = ctx.banco();

    return ctx.banco.cargando() ? oor.loading() : m('div', [

    m('#title-head', [

                m('.col-xs-12.col-md-6.title-bar', [
                    m('h3', 'Cuenta Bancaria:'),
                ]),

                m('.col-xs-12.col-md-6.action-bar', [

                    m('a.btn.btn-sm.btn-primary button-striped button-full-striped  btn-ripple btn-xs',{
                        style:'margin-right:10px',
                        href : 'javascript:;',
                         onclick : function () {
                            ctx.activarConciliacion( ! ctx.activarConciliacion() )
                        }
                    }, ctx.activarConciliacion() ? 'Ir a Movimientos' : 'Ir a Conciliación') ,

                    m('a.btn.btn btn-success button-striped button-full-striped  btn-ripple btn-xs',{
                        style:'margin-right:10px',
                        href : urlMovimiento('ingreso', banco.cuenta_contable_id)
                    },'Nuevo Ingreso'),

                    m('a.btn.btn btn-warning button-striped button-full-striped  btn-ripple btn-xs',  {
                         style:'margin-right:10px',
                        href : urlMovimiento('egreso', banco.cuenta_contable_id)
                    },'Nuevo Egreso'),
                    m('.btn-group', [
                        m('.btn btn-primary button-striped button-full-striped button-striped btn-default btn-xs dropdown-toggle btn-ripple ', {'data-toggle':'dropdown'},
                            ['Exportar  ',m("span.caret"),{style:'width:30px'}]
                        ),
                        m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                            m("li", m('a',{href:'/exportar/movimientosBancos/'+banco.cuenta_contable_id+'/csv'},  [
                                    m('i.fa fa-file-excel-o')," Excel"
                                ])
                            )
                        ])
                    ])
                ]),

                m('.clear'),

    ]),
    m('.panel', [
        // m('.panel-heading', [
        //     m('.panel-title', [

        //         m('h4',  {style:'text-transform:uppercase'}, 'Cuenta Bancaria:'),

        //         m('.panel-buttons', [
        //             m('div', [
        //                 m('.pull-right', [
        //                   //  m('a.btn.btn-sm.btn-primary', {

        //                     m('a.btn.btn-sm.btn-primary button-striped button-full-striped  btn-ripple btn-xs',{
        //                         style:'margin-right:10px',
        //                         href : 'javascript:;',
        //                          onclick : function () {
        //                             ctx.activarConciliacion( ! ctx.activarConciliacion() )
        //                         }
        //                     }, ctx.activarConciliacion() ? 'Ir a Movimientos' : 'Ir a Conciliación') ,

        //                     m('a.btn.btn btn-success button-striped button-full-striped  btn-ripple btn-xs',{
        //                         style:'margin-right:10px',
        //                         href : urlMovimiento('ingreso', banco.cuenta_contable_id)
        //                     },'Nuevo Ingreso'),

        //                     m('a.btn.btn btn-warning button-striped button-full-striped  btn-ripple btn-xs',  {
        //                          style:'margin-right:10px',
        //                         href : urlMovimiento('egreso', banco.cuenta_contable_id)
        //                     },'Nuevo Egreso'),
        //                 ]),
        //             ]),
        //         ]),
        //     ]),
        // ]),

        m('.panel-body', [
             m('h3' , {style:'margin-bottom:0px'}, [
                f('cuenta')(banco),
                ' ',
                f('nombre')(banco)
            ]),
            m('h5', [
                m('small.text-grey', 'Banco: '), f('banco')(banco), ' ',
                m('small.text-grey', 'No de Cta:'), f('cuenta_bancaria')(banco),
                ' ',
            ]),
            m('br'),

            m('a.pull-right.text-indigo', {
                'style' : 'margin:10px',
                href : '/conciliacion/cuenta/'.concat(banco.cuenta_contable_id)
            }, [
                'Subir archivo de conciliación'
            ]),

            ctx.activarConciliacion() ?  [
                m('h4', {style:'margin-bottom:0px'},'Conciliacion Bancaria'),
                ProcesoConciliacionView(ctx)
            ]: [
                 m('h4', 'Movimientos Bancarios '),
                 m('.table-responsive', m('table.table', {config:ctx.setupTable}))
             ]
        ]),

        MTModal.view()
    ]),
    ]);
}

function urlMovimiento(tipoMov, cuentaId) {
    return '/movimientos/'.concat(tipoMov, '?cuenta_id=', cuentaId);
}

function ProcesoConciliacion(ctx) {
    var pc = this;
    pc.registros = m.prop();
    pc.registros.cargando = m.prop();
    pc.registroSeleccionado = m.prop();

    pc.registro = m.prop();
    pc.registro.cargando = m.prop();
    pc.pestaniaSeleccionada = m.prop(false);

    pc.cargarRegistros = function () {
        if(pc.registros() || pc.registros.cargando()) {
            return;
        }

        pc.registros.cargando(true);
        pc.pestaniaSeleccionada(false)

        oorden().then(function () {
            oor.request('/conciliacion/registros','GET', {
                data : { cuenta_id : ctx.banco().cuenta_contable_id }
            })
            .then(pc.registros)
            .then(pc.asignarRegistros)
            .then(pc.registros.cargando.bind(null,false))
        });

    }


    pc.asignarRegistros = function () {
        var pestanias = {};

        pc.registros.pestanias = m.prop([]);

        pc.registros().forEach(function (reg) {
            var infoFecha = reg.fecha.split('-');
            var pestania = infoFecha[0] + '-' + infoFecha[1];
            if(!pestanias[pestania]) {
                pestanias[pestania] = [];
            }
            pestanias[pestania].push(reg);
        });

        Object.keys(pestanias).sort(function (p1, p2) { return p2 - p1 })
            .forEach(function (k) {
                var info = k.split('-');

                pc.registros.pestanias[k] = {
                    value : k,
                    label : oor.fecha.labelMes(info[1]) + ' ' + info[0],
                    registros : pestanias[k]
                };

                pc.registros.pestanias().push(pc.registros.pestanias[k]);


            });


        console.log( pc.registros.pestanias()[0] );

        pc.pestaniaSeleccionada(pc.registros.pestanias()[0]);

    }

    /*
    pc.conciliarMovimiento = function (idConciliacion, movimientos) {
        oor.request('/conciliacion/conciliar/'.concat(idConciliacion), 'POST', {
            data : {movimientos : movimientos}
        })
        .then(function () {
            toastr.success('¡Movimiento Conciliado!')
        }, function (r) {
            toastr.error(r.status.message)
        })
        .then(pc.registros.bind(null,null))
    }


    pc.conciliarOperacion = function (idConciliacion, operaciones) {
        oor.request('/conciliacion/conciliarOperacion/'.concat(idConciliacion), 'POST', {
            data : {operaciones : operaciones}
        })
        .then(function () {
            toastr.success('¡Movimiento Conciliado!')
        }, function (r) {
            toastr.error(r.status.message)
        })
        .then(pc.registros.bind(null,null))
    }


    pc.conciliarUnaOperacion = function (registro, operacion) {
        console.log(registro.referenciaConciliacion())

        PagarOperacion.generarPago({
            cuenta_id : ctx.banco().cuenta_contable_id,
            operacion_id : operacion.operacion_id,
            fecha : registro.fecha
        }).then(function (d) {

            d.importe_pago = operacion.saldo;
            d.monto_pago = operacion.saldo;
            d.referencia = registro.referenciaConciliacion;
            d.fecha = registro.fecha;

            return d;
        }).then(PagarOperacion.registrarPagoOperacion)
        .then( function (result) {
            pc.conciliarMovimiento(registro.conciliacion_bancaria_id, [result.movimiento.movimiento_banco_id])
        });
    }

    */

}


function ProcesoConciliacionView (ctx) {
    if(!ctx.pc) ctx.pc = new ProcesoConciliacion(ctx);
    ctx.pc.cargarRegistros();
    var registros;

    if(ctx.pc.pestaniaSeleccionada()) {
        registros = ctx.pc.pestaniaSeleccionada().registros;
        registros.forEach(coincidencias);
        coincidencias.obtenerDatos();
    }

    return ctx.pc.registros.cargando() ? oor.loading() : m('div',[

        m('select', {onchange: m.withAttr('vale', function (v) { ctx.pc.pestaniaSeleccionada(ctx.pc.registros.pestanias[v]) } ) }, [
            ctx.pc.registros.pestanias().map(function (d) {
                return m('option', {value:d.value}, d.label, ' (', d.registros.length , ')')
            })
        ]),

        m('br'),
        m('div',
            (registros || []).map(function (reg) {
                return regComponent = m.component(ConciliarRegistro, {
                    key           : reg.conciliacion_bancaria_id,
                    registro      : reg,
                    cuenta        : ctx.banco(),
                    coincidencias : coincidencias(reg),
                });
            })
        ),

        ConciliacionView(ctx),
    ]);
}








/*

function MostrarCoincidencias (ctx) {
    var registro = ctx.pc.registro();
    var coincidencias = 0;
    var O = oor.mm('Operacion');

    if(!registro || ctx.pc.registro.cargando()) {
        return oor.loading();
    }

    if(registro && registro.coincidencias) {
        coincidencias += registro.coincidencias.operaciones.length;
        coincidencias += registro.coincidencias.movimientos.length;
    }

    return m('div.coincidencias', {}, [
        coincidencias == 0 ? m('h6','No hay Concidencias') : '',

        registro.coincidencias.movimientos.map(function (coin) {
            return m('div', {}, [
                m('div',coin.fecha),
                m('div',[
                    coin.referencia,
                    m('span.label.label-primary', {
                        onclick : function () { ctx.pc.conciliarMovimiento(registro.conciliacion_bancaria_id, [coin.movimiento_banco_id]) }
                    }, 'Conciliar')
                ])
            ]);
        }),

        registro.coincidencias.operaciones.map(function (coin) {
            inicializarConincidenciaOperacion(registro, coin);

            return m('tr', {}, [
                m('td', [
                    m('a.text-indigo', {href:O.url(coin), target:'_blank'}, [
                        coin.fecha, ' ', coin.nombre_documento, ' ', coin.serie, ' ', coin.numero
                    ]),

                    m('div.small.text-grey', coin.referencia),

                    m('div', {style:'margin-left:2em'}, [
                        m('div.small',  'De: ', coin.tercero.nombre.concat(' (', coin.tercero.clave_fiscal, ')') ),
                        m('div', [
                            'Saldo: ',
                            m('strong', oorden.org.format(coin.saldo)),
                            m('form', {
                                onsubmit : function (ev) {
                                    ev.stopPropagation();
                                    ev.preventDefault();
                                    ctx.pc.conciliarUnaOperacion.call(null, registro, coin)

                                    return false;
                                }
                            }, [
                                m('input[type=text]', {
                                    value : registro.referenciaConciliacion(),
                                    onchange : m.withAttr('value', registro.referenciaConciliacion)
                                }),
                                m('button[type=submit]', 'Pagar y Conciliar')
                            ])
                        ])
                    ])
                ])
            ]);
        }),



        m('a.text-indigo', {
            onclick : function () {
                var monto = Number(registro.monto);

                ctx.registrarMovimiento = m.component(bancos.movimiento, {
                    ingreso : monto > 0 ? true : false,
                    egreso :  monto > 0 ? false : true,
                    fijarMonto : Math.abs(Number(registro.monto)),
                    cuentaId : registro.cuenta_id
                })

            }
        }, 'Capturar...'),


        ctx.registrarMovimiento


    ]);
}
*/



function inicializarConincidenciaOperacion (registro,coin) {
    if(registro.referenciaConciliacion) return;
    registro.referenciaConciliacion = m.prop(registro.referencia || registro.concepto)
}


function Conciliacion (ctx) {
    var procesar = {};

    procesar.fecha = function (d) {
        var date;
        d = String(d);

        if(date = d.match(/\d{2}\/\d{2}\/\d{4}/) ) {
            return date[0].split('/').reverse().join('-')
        }
    }

    procesar.monto = function () {

    }


    ctx.conciliacion = {};
    ctx.conciliacion.data = m.prop();
    ctx.conciliacion.plantilla = m.prop();
    ctx.conciliacion.registros = m.prop();

    ctx.conciliacion.columnas = m.prop([
        {key:'fecha',nombre : 'Fecha', column : m.prop(null) , procesar: procesar.fecha},
        {key:'folio', nombre : 'Folio', column : m.prop(null) },
        {key:'concepto', nombre : 'Concepto', column : m.prop(null) },
        {key:'referencia', nombre : 'Referencia', column : m.prop(null) },
        {key:'descripcion', nombre : 'Descripción', column : m.prop(null) },
        {key:'monto', nombre : 'Monto', column : m.prop(null) }
    ]);

    ctx.conciliacion.procesarDatos = function () {
        var registros = ctx.conciliacion.data()
            .map(function (d) {
                var r = {};
                ctx.conciliacion.columnas().forEach(function (c) {
                    var v = d[c.column()];
                    v = c.procesar ? c.procesar(v) : v;
                    r[c.key] = v;
                });
                return r;
            });


        oor.request('/conciliacion/agregarRegistros', 'POST', {
            data : {
                cuenta_id : ctx.banco().cuenta_contable_id,
                registros : registros
            }
        }).then(function (d) {
            ctx.conciliacion.data(null)
            ctx.pc.registros(null)
            toastr.success('Estado de Cuenta Cargado Correctamente')
        })
    };

    ctx.conciliacion.columnas.getCol = function (n) {
        return ctx.conciliacion.columnas().filter( f('column').is(n) )[0]
    };


    var colAsignada = m.prop();
    ctx.conciliacion.activarColumna = function (columna) {
        console.log(columna)
        colAsignada(columna);
    };

    ctx.conciliacion.asignarColumna = function (n) {
        colAsignada().column(n);
    };

    return function (el, isInited) {
        if(isInited) return;

        /**
         *
         */
        el.addEventListener('change', function () {
            Papa.parse(el.files[0], {
                encoding : 'UTF-8',
                complete : function (d) {

                    //Filtro los datos para no incluir filas vacías
                    var data = d.data.filter(function (d) {
                        for(var i = 0; i< d.length; i++) {
                            if(Boolean(d[i])) return true;
                        }
                        return false;
                    });

                    ctx.conciliacion.data(data);
                    m.redraw();
                }
            })
        });
    }
}



function ConciliacionView (ctx) {
    return '';
    /*
    var conciliacion = ctx.conciliacion;
    return [
        m('.col-sm-12', [
            conciliacion.data() ? TablaConcilacion(ctx) : m('fieldset', [
                m('h4', 'Conciliación Bancaria'),
                m('.row', [
                    m('input[type=file]', {
                        config : ctx.inicializarConciliacion
                    })
                ])
            ])
        ])
    ];
    */
}



function TablaConcilacion(ctx) {
    var data   = ctx.conciliacion.data();
    var length = d3.max(data.map(function (d) { return d.length }));
    var range  = d3.range(length);

    return m('div', [
        m('table', [
            m('tbody', [
                ctx.conciliacion.columnas().map(function (col) {
                    var label = 'Seleccionar Columna';

                    if(col.column() !== null) {
                        label = 'Col '.concat(col.column() + 1)
                    }

                    return m('tr', [
                        m('th', col.nombre),
                        m('th', [
                            m('a.text-indigo[href=javascript:;]', {
                                onclick : function () {
                                    ctx.conciliacion.activarColumna(col)
                                }
                            },label)
                        ])
                    ]);
                })
            ])
        ]),

        m('br'),

        m('.table-responsive', [
            m('table.table.megatable', [
                m('thead', [
                    m('tr', [
                        range.map(function (n) {
                            var col = ctx.conciliacion.columnas.getCol(n);
                            return m('th', [
                                m('a[href=javascript:;]', {
                                    onclick : function () {
                                        ctx.conciliacion.asignarColumna(n)
                                    }
                                }, col ? col.nombre : 'Col. '.concat(n + 1))
                            ]);
                        })
                    ])
                ]),

                m('tbody', [
                    ctx.conciliacion.data().map(function (d) {
                        var cells = [];
                        for(var i = 0; i<length; i++) {
                            cells.push( m('td', d[i]) );
                        }
                        return m('tr', cells);
                    })
                ])
            ])
        ]),

        m('br'),

        m('button.btn.btn-sm.pull-right.btn-primary', {

            onclick : ctx.conciliacion.procesarDatos
        }, 'Siguiente'),

        m('br'),

        MTModal.view()
    ]);
}
