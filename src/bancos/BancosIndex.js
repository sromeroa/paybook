module.exports = {
    view : BancosIndexView,
    controller : BancosIndexController
}


function BancosIndexController () {
    var ctx = this;
    ctx.bancos = m.prop();
    ctx.bancos.cargando = m.prop(false);
    ctx.bancos.grupos = m.prop();

    oor.fondoGris();

    ctx.cargarBancos = function () {
        if(ctx.bancos() || ctx.bancos.cargando()) return;
        cargarBancos();
    }

    function cargarBancos () {
        ctx.bancos.cargando(true);

        oor.request('/bancos/data').then(function (d) {
            var grupos = {};
            ctx.bancos(d);
            ctx.bancos.cargando(false);
            ctx.bancos().forEach(function (d) {
                var key = String( f('banco')(d) || '(sin nombre)' );
                if(!grupos[key]) grupos[key] = {bancos : [], nombre : key}
                grupos[key].bancos.push(d);
            });

            ctx.bancos.grupos(grupos);
        });
    }
}

function BancosIndexView (ctx) {
    ctx.cargarBancos();
    return m('div', [

        m('#title-head', [

                    m('.col-xs-12.col-md-6.title-bar', [
                        m('h3', 'Cuentas Bancarias'),
                    ]),

                    m('.col-xs-12.col-md-6.action-bar', [

                        m('a', {href:'/movimientos/traspaso', style : 'margin-right:18px'},[
                            m('.btn btn-success button-striped button-full-striped  btn-ripple btn-xs', 'Traspaso entre Cuentas '),
                        ]),
                        m('.btn-group', [
                            m('.btn btn-primary button-striped button-full-striped button-striped btn-default btn-xs dropdown-toggle btn-ripple ', {'data-toggle':'dropdown'},
                                ['Exportar  ',m("span.caret"),{style:'width:30px'}]
                            ),

                            m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                                m("li", m('a',{href:'/exportar/bancos/csv'},  [
                                        m('i.fa fa-file-excel-o')," Excel"
                                    ])
                                )
                            ])
                        ])
                    ]),

                    m('.clear'),

                   // m('h3.column-title', 'Cuentas Bancarias'),

                  /*  m('.row', [
                        m('.col-sm-12', [
                            m('a.btn.btn-sm.pull-right.btn btn-success button-striped button-full-striped  btn-ripple btn-xs', {
                                href : '/movimientos/traspaso'
                            },'Traspaso entre Cuentas'),
                            m('br')
                        ]),
                    ]),

                    m('br'),*/

                  /*  cuerpo
                  ctx.bancos.cargando() ? oor.loading() : m('div', [
                        TablaBancos(ctx)
                    ])*/
        ]),
        m('.panel', [
            // m('.panel-heading', [
            //     //m('.row', [
                

            // ]),
            m('.panel-body', [
                            ctx.bancos.cargando() ? oor.loading() : m('div.table-responsive', [
                                TablaBancos(ctx)
                            ]),


            ]),


        ]),
    ]);
}





function TablaBancos (ctx) {
    var format = d3.format(',.2f');

    return m('table.table.megatable', [
        Object.keys(ctx.bancos.grupos()).map(function (key) {
            var gpo = ctx.bancos.grupos()[key];
            return [
                m('thead', [
                    m('tr', [
                        m('th.text-center', {style:'background:#fafafa'}, f('nombre')(gpo)),
                        m('th.text-right', {style:'background:#fafafa'}, 'Saldo Inicial'),
                        m('th.text-right', {style:'background:#fafafa'}, 'Ingresos'),
                        m('th.text-right', {style:'background:#fafafa'}, 'Egresos'),
                        m('th.text-right', {style:'background:#fafafa'}, 'Saldo Final'),
                        m('th.text-right', {style:'background:#fafafa'}, 'Moneda'),
                    ])
                ]),

                m('tbody', [
                    gpo.bancos.map(function (banco) {
                       return m('tr', {style:'background:white'}, [
                            m('td', {style:'padding-left:30px'}, [
                                m('a.text-indigo', {
                                    href : '/bancos/banco/' + f('cuenta_contable_id')(banco)
                                }, f('nombre')(banco)),

                                m('div.text-grey', [
                                    f('cuenta_bancaria')(banco)
                                ])
                            ]),
                            m('td.text-right', [
                                /*m('div', [
                                    m('small.text-grey', 'Saldo Inicial:')
                                ]),*/
                                format(f('saldo_inicial')(banco))
                            ]),
                            m('td.text-right', [
                                /*m('div', [
                                    m('small.text-grey', 'Ingresos:')
                                ]),*/
                                m('small.text-grey','+ '), format(f('ingresos')(banco))
                            ]),
                            m('td.text-right', [
                                /*m('div', [
                                    m('small.text-grey', 'Egresos:')
                                ]),*/
                                m('small.text-grey',m.trust('&mdash; ')), format(f('egresos')(banco))
                            ]),

                            m('td.text-right', [
                                /*m('div', [
                                    m('small.text-grey', 'Saldo Final:')
                                ]),*/
                                format(f('saldo_final')(banco))
                            ]),

                            m('td.text-right', [
                                /*m('div', [
                                    m('small.text-grey', 'Moneda:')
                                ]),*/
                                f('moneda')(banco)
                            ])
                        ]);
                    })
                ])
            ];

        })
    ]);

}
