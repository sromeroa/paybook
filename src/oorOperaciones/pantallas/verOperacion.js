var verOperacion        = module.exports = {};
var FechaInput          = require('../../inputs/fechaInput');
var EncabezadoOperacion = require('../componentes/EncabezadoOperacion.js');
var inter               = require('../componentes/InterComponentes.js');


verOperacion.oninit = function (vnode) {
    var idOperacion = 'ff9067fa-9c35-3f43-dc59-44bd8c5dc0c7';

    this.loading          = m.prop(false);
    this.operacion        = m.prop();
    this.componenteActivo = m.prop();

    this.initialize = function () {
        if(this.loading() || this.operacion()) return;

        this.loading(true);

        oor.request('/operacion/vta/' + idOperacion).run(function (r) {
            vnode.state.operacion(r);
            vnode.state.loading(false);
            vnode.state.terceroId = m.prop(r.tercero_id);
        });
    };
}

verOperacion.view = function (vnode) {
    this.initialize();
    var operacion = this.operacion();

    return oor.panel({
        title   : this.heading(),
        inter   : this.interComponente(vnode),
        buttons : this.loading() ? null : this.buttons()
    }, [
        this.loading() ? oor.loading() : '',

        this.loading() ? null : [

            //Encabezado de la operación
            m(EncabezadoOperacion, {
                terceroId : vnode.state.terceroId,
                operacion : operacion
            }),

            m('br'),

            m('.clear'),

            m('h4', {style:'margin-top:10px'}, 'Conceptos:'),
            m('br'),


            /*
            m('.row', [
                oor.stripedButton('button.pull-right.btn-primary', [
                    m('i.ion-archive'), ' Guardar'
                ]),

                m('.btn-group.pull-right', {style: 'margin:0 40px'}, [

                    oor.stripedButton('button.btn-success', [
                        m('i.ion-checkmark-round'), ' Aplicar'
                    ]),

                    oor.stripedButton('button.btn-success', [
                        m('i.fa.fa-file-code-o'), ' Timbrar CFDI'
                    ]),
                ])
            ]),

            m('row', [
                m('label.pull-right', [
                    m('input[type=checkbox]'), '... y regresar a captura'
                ])
            ])

            */
        ]
    ]);
}


verOperacion.org = oorden.org;

verOperacion.interComponente = function () {
    if(!this.componenteActivo()) return null;
    return m(this.componenteActivo(), {
        close      : this.componenteActivo.bind(null,false),
        operacion  : this.operacion()
    });
};


verOperacion.btnComponente = function (aComponent) {
    return m('button.btn.btn-flat.btn-xs', {
        onclick : this.componenteActivo.bind(null, aComponent),
        'class' : this.componenteActivo() == aComponent ? 'btn-primary' : 'btn-default'
    }, aComponent.btnLabel);
}

verOperacion.buttons = function () {
    return [
        m('div.btn-group',[
            this.org().codigo_pais == 'MX' ? [
                this.operacion().folio_fiscal ? this.btnComponente(inter.cfdi) : 'this.btnTimbrarCfdi()'
            ] : null,
            this.btnComponente(inter.archivos),
            this.btnComponente(inter.bitacora),
            this.btnComponente(inter.notas)
        ]),

        m('div.btn-group',[
            oor.stripedButton('button.btn-primary.btn-xs', ['Acciones ', m('span.caret')], {})
        ])
    ];
}

verOperacion.heading = function () {
    if(! this.operacion() ) return null;

    return [
        m('h4.pull-left', {style:'margin:0'}, [
            this.operacion().tipoDocumento.nombre + ' ',
            m('span',{style:'color:#aaa; font-size:0.7em'}, ' Serie: '),
            m('span', this.operacion().serie),
            m('span',{style:'color:#aaa; font-size:0.7em'},' # '),
            m('span', this.operacion().numero),
            m('span',{style:'color:#aaa; font-size:0.7em'},' Fecha: '),
            m('span', oor.fecha.prettyFormat.year(this.operacion().fecha))
        ]),
    ];
}
