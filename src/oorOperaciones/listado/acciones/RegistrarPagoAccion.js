module.exports = RegistrarPago;

function RegistrarPago (ctx) {
    return function () {
        if(ctx.$sel.length == 0) {
            return toastr.error('Debes seleccionar al menos una operación');
        }
        var argumentosModal =  {
            operaciones : ctx.$sel,
            onEjecutar : ctx.reload.bind(ctx)
        };

        argumentosModal['VTA'] = 'ingreso';
        argumentosModal['VNC'] = 'egreso';
        argumentosModal['COM'] = 'egreso';
        argumentosModal['CNC'] = 'ingreso';
        argumentosModal.seccion = ctx.seccion;

        MTModal.open({
            args : argumentosModal,
            controller : RegistrarPagoController,
            //top : AplicarOperacionesTop,
            content : RegistrarPagoView,
            bottom : RegistrarPagoBottom,
            el : this,
            modalAttrs : {
                'class' : 'modal-small'
            }
        });
    }
}

function RegistrarPagoController (args) {
    var ctx = this;

    ctx.resultados = m.prop();
    ctx.loading = m.prop();
    ctx.seccion = args.seccion;


    ctx.initialize = function () {
        if(ctx.resultados() || ctx.loading()) {
            return;
        }

        oor.request('/operaciones/accion/pagar', 'POST', {
            data : {operaciones :args.operaciones}
        })
        .then(function (data) {
            var resultados = {ingreso : [], egreso : [], noPuede : []};
            data.forEach(function (r) {
                var key = args[r.operacion.tipo_operacion];
                if(r.accion.puede && resultados[key]) {
                    resultados[key].push(r.operacion);
                } else {
                    resultados.noPuede.push(r.operacion);
                }
            });
            return ctx.resultados(resultados);

        })
        .then(function (resultados) {
            var definir = false;
            var postUrl = '?movimientos=';

            resultados.ingreso.ids = resultados.ingreso.map( f('operacion_id') );
            resultados.egreso.ids = resultados.egreso.map( f('operacion_id') );

            resultados.ingreso.url = '/movimientos/ingreso'.concat(
                '?movimientos=',
                resultados.ingreso.ids.join(',')
            );

            resultados.egreso.url = '/movimientos/egreso'.concat(
                '?movimientos=',
                resultados.egreso.ids.join(',')
            );

            if(resultados.ingreso.length && !resultados.egreso.length) {
                definir = 'ingreso';
            } else if(resultados.egreso.length && !resultados.ingreso.length) {
                definir = 'egreso';
            }

            if(definir) {
                window.location = resultados[definir].url;
            }

        })
        .then(ctx.loading.bind(null,false))

    }
}

function RegistrarPagoView (ctx) {
    var resultados = ctx.resultados();
    ctx.initialize();

    if(!resultados) return oor.loading();

    return m('div',[
        resultados.noPuede.length ? m('div.alert.alert-warning', [
            resultados.noPuede.length, ' Operaciones no se pueden pagar'
        ]) : '',

        m('h5', 'Selecciona una opción:'),

        m('ul', [
            resultados.egreso.length ? m('li', [
                m('a', {href : resultados.egreso.url }, [
                   'Registrar egreso para ',
                   resultados.egreso.length,
                   ' ',
                   ctx.seccion() == 'V' ? 'Notas de Crédito' : 'Facturas',
               ])
           ]) : null,

          resultados.egreso.length ? m('li', [
               m('a', {href : resultados.ingreso.url }, [
                   'Registrar ingreso para ',
                   resultados.ingreso.length,
                   ' ',
                    ctx.seccion() == 'V' ? 'Compras' : 'Notas de Crédito de Compra',
               ])
          ]) : null
        ])
    ]);
}


function RegistrarPagoBottom (ctx) {
    return m('button..btn-sm.btn.pull-left.btn-default', {onclick:ctx.$modal.close}, [
        'Cancelar'
    ]);
}
