module.exports = Aplicar;

function Aplicar(ctx) {
    return function () {
        if(ctx.$sel.length == 0) {
            return toastr.error('Debes seleccionar al menos una operación');
        }

        MTModal.open({
            args : {
                operaciones : ctx.$sel,
                onEjecutar : ctx.reload.bind(ctx)
            },
            controller : AplicarOperacionesController,
            top : AplicarOperacionesTop,
            content : AplicarOperacionesContent,
            bottom : AplicarOperacionesBottom,
            el : this,
            modalAttrs : {
                'class' : 'modal-medium'
            }
        });
    }
}

function AplicarOperacionesTop () {
    return m('h4', 'Aplicar Operaciones');
}

function AplicarOperacionesBottom (ctx) {
    var mostrarAplicar = ctx.resultados() && ctx.resultados().validas.length;
    mostrarAplicar = mostrarAplicar && !ctx.ejecutando();

    return [
        m('button.btn-small.btn-flat.btn-default.pull-left',  {onclick : ctx.$modal.close}, [
            'Cancelar'
        ]),

        mostrarAplicar ? m('button.btn-small.btn-flat.btn-primary.pull-right', {onclick : ctx.ejecutarAccion}, [
            'Aplicar'
        ]) : null
    ];
}

function AplicarOperacionesContent (ctx) {
    var resultados = ctx.resultados();
    ctx.inicializar();
    if(ctx.inicializando()) {
        return oor.loading();
    }

    return [
        resultados.invalidas.length ? m('.alert.alert-warning', [
            resultados.invalidas.length, ' Operaciones No se pueden Aplicar '
        ]):null,

        resultados.validas.length ? [
            m('h5', 'Operaciones por Aplicar:'),

            resultados.validas.map(function (r) {
                return m('div.thmb-operacion', {},[
                    m('div', r.operacion.nombre_documento, ' ', r.operacion.serie, ' ', r.operacion.numero || '<auto>'),
                    m('.small.text-grey', r.referencia)
                ]);
            })

        ] : m('h5', 'No hay operaciones por aplicar')
    ];
}



function AplicarOperacionesController (args) {
    var ctx  = this;

    ctx.inicializando = m.prop(false);
    ctx.resultados = m.prop();
    ctx.ejecutando = m.prop(false);

    ctx.inicializar = function () {
        if(ctx.inicializando() || ctx.resultados()){
            return;
        }

        ctx.inicializando(true);

        oor.request('/operaciones/accion/aplicar', 'POST', {
            data : {operaciones : args.operaciones}
        })
        .then(function (r) {
            return {
                validas : r.filter( function (d) { return d.accion.puede }),
                invalidas : r.filter( function (d) { return !d.accion.puede })
            };
        })
        .then(ctx.resultados)
        .then(ctx.inicializando.bind(ctx, false));
    };


    ctx.ejecutarAccion = function () {
        if(ctx.ejecutando()) {
            return;
        }
        var operaciones = ctx.resultados().validas.map( function (d) {
            return d.operacion.operacion_id
        });

        ctx.ejecutando(true);

        oor.request('/operaciones/accion/aplicar/ejecutar', 'POST', {
            data : {operaciones : operaciones}
        }).then(function (resultados) {

            resultados.map(function (r) {
                console.log(r.accion)
            });


            ctx.$modal.close();
            toastr.success('¡Cambios Realizados!');

        }).then(function (d) {
            nh.isFunction(args.onEjecutar) && args.onEjecutar()
        })
    }
}
