module.exports = ActionsOperaciones;

var Aplicar = require('./acciones/AplicarAccion.js');
var RegistrarPago =  require('./acciones/RegistrarPagoAccion.js');

function ActionsOperaciones (ctx) {
    var selection = ctx.$sel.selection();

    return m('.row', {style:'padding: 10px 0'}, [
        m('.col-sm-12', [
            /*m('button.btn.btn-default.btn-sm.btn-flat.disabled', [
                selection.length ? String(selection.length).concat(' Operaciones seleccionadas:')  : null
            ]),*/
            m('button.btn.btn-default.btn-sm.btn-flat', {
                style : 'margin-right:5px',
                onclick : Aplicar(ctx),
            }, [m('i.ion-checkmark-round.text-green') ,' Aplicar']),

             m('button.btn.btn-default.btn-sm.btn-flat', {
                onclick : RegistrarPago(ctx),
            }, [m('i.ion-cash.text-indigo') ,' Registrar Pago'])
        ])
    ]);
}
