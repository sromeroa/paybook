module.exports = TablaOperaciones;

var Table = require('../../nahui/dtable');
var Field = require('../../nahui/nahui.field');
var columns;
var fields = {};

var estatuses = m.prop([
    { estatus : 'P', nombre : 'EN PREPARACIÓN', color : 'grey', nombre_corto: 'Borrador'},
    { estatus : 'T', nombre : 'PENDIENTE',      color : 'amber', nombre_corto: 'Finalizada'},
    { estatus : 'A', nombre : 'APLICADA',       color :null, nombre_corto : 'Aplicada'},
    { estatus : 'S', nombre : 'SALDADA',        color : 'green'  , nombre_corto : 'Saldada'},
    { estatus : 'X', nombre : 'CANCELADA',      color : 'red', nombre_corto : 'Cancelada'},
]);

estatuses().forEach(function (t) {
    estatuses[t.estatus] = m.prop(t);
});

function TablaOperaciones (ctx) {
    if(ctx.table){
        return;
    }

    // Aquí se crea una tabla, le tenemos que pasar a la tabla una función que
    // recupere el id de un registro, como el id está en la propiedad opOperacionId
    // f('opOperacionId') crea una función que devuelve la propiedad opOperacionId de cualquier objeto.
    // Ejemplo: f('opOperacionId')({ opOperacionId : 99}) == 99
    ctx.table = Table().key( f('opOperacionId') );
    ctx.table.mounted = m.prop(false);
    ctx.table.rows = m.prop()
    ctx.table.redraw = m.prop(false)

    var fields = {};

    //La función Field crea un campo para las tablas,
    //Acepta dos parametros el name y la configuración del Field
    //por default el nombre es la propiedad a la que se hace referencia
    fields.opOperacionId = Field('opOperacionId', {});


    //Si le ponemos un caption se edita el encabezado del renglón
    fields.opFecha = Field('opFecha', {
        caption : 'Fecha',
        filter :  oor.fecha.prettyFormat
    });


    // Si le ponemos una class le asignamos la clase que llevan las celdas de datos
    // Si queremos editar la clase que le ponemos a las celdas de titulo es headingClass
    // Filter: es para darle formato a los campos
    fields.opUpdatedAt = Field('opUpdatedAt', {
        caption : ' ',
        'class' : 'text-grey small',
        filter : function (d) { return d.split(' ').join('T').concat('.000Z');},
        update : function (d) { d.attr('x-time-till-now', f('text')); }
    });

    //Field.twice ayuda a crear campos conjuntos, en la tabla de operaciones,
    //Casi todos los campos son conjuntos (tienen más de un dato)
    //En subfields hay que pasarle un array con los datos que vamos a poner
    fields.fechas = Field.twice('fechas', {
        subfields : [fields.opFecha, fields.opUpdatedAt]
    });


    //Field.linkToResource  crea un vínculo
    //hay que pasar la function url que va a devolver lo que queremos poner en href
    fields.opTitulo = Field.linkToResource('opTitulo', {
        url : function (d) {
            return '/operaciones/'.concat(d.opTipoOperacion.toLowerCase(), '/', d.opOperacionId);
        },
        caption : 'Documento',
        'class' : 'text-indigo'
    });

    fields.opReferencia = Field('opReferencia', {
        caption : 'Referencia',
        'class' : 'text-grey small'
    });

    fields.operacion = Field.twice('operacion', {
        subfields : [fields.opTitulo, fields.opReferencia]
    });

    fields.tNombre = Field('tNombre',{
        'caption' : ctx.seccion() == 'ventas' ? 'Cliente' : 'Proveedor',
    });

    fields.tercero = fields.tNombre;


    fields.opTotal = Field('opTotal', {
        caption : 'Total',
        'class' : '',
        filter: oorden.organizacion.format,
        headingClass:  ''
    });

    fields.opMoneda = Field('opMoneda', {
        caption : 'Moneda',
        'class' : 'text-grey small',
        headingClass:  ''
    });

    fields.totalMoneda = Field.twice('totalMoneda', {
        subfields : [fields.opTotal, fields.opMoneda]
    });


    var opEstatuses = estatuses().filter(f('color'));

    fields.opSaldo = Field('opSaldo', {
        caption : 'Saldo',
        'class': 'text-right',
        filter: oorden.organizacion.format,
        headingClass:  'text-right',

        update : function (selection) {
            selection.text(function (d) {
                var opEstatus = d.row.opEstatus;
                if(opEstatus == 'A') return oorden.org.format(d.row.opSaldo)
                var status = estatuses[opEstatus];
                return status ? status().nombre_corto : '';
            });

            opEstatuses.forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) {
                    return d.row.opEstatus == st.estatus;
                });
            });
        }
    });




    fields.opEstatus = Field('opEstatus', {
        caption : 'Estatus',
        'class': '',
        headingClass:  '',

        filter :  function (st) {
            var status = oor.mm('Operacion').estatuses[st];
            return status ? status().nombre_corto : '';
        },

        update : function (selection) {

            selection.text(function (d) {
                return d.value == 'A' ? oorden.org.format(d.row.opSaldo) : d.text;
            });


            opEstatuses.forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) { console.log(); return d.value == st.estatus; });
            });
        }
    });

    fields.saldoEstatus = fields.opEstatus;
    /*
    fields.saldoEstatus = Field.twice('saldoEstatus', {
        subfields : [fields.opSaldo, fields.opEstatus]7
    });
    */

    ctx.$sel = [];

    ctx.$sel.isSelected = function (id) {
        return ctx.$sel.indexOf(id) > -1;
    }

    ctx.$sel.select = function (id) {
        if(ctx.$sel.isSelected(id)) return;
        ctx.$sel.push(id);
    }

    ctx.$sel.unselect = function (id) {
        var idx = ctx.$sel.indexOf(id)
        if(idx == -1) return;
        ctx.$sel.splice(idx,1);
    }

    ctx.$sel.selection = function (s) {
        return ctx.$sel;
    }


    fields.cfdi = Field('opFolioFiscal', {
        caption : 'cfdi',
        class : 'small',
        headingClass : 'text-center',
        'class' : 'text-center small',
        enter : function (sel) {
            sel.call(Field.enter);

            sel.append('a')
                .attr('class', 'text-indigo cfdi-xml')
                .style('margin', '2px').text('XML');
        },

        update : function (sel) {
            sel.selectAll('a').style('display',  function (d) { return d.value ? undefined : 'none' });
            sel.select('.cfdi-xml').attr('href', downloadCFDI('xml') )
        }
    })


    function downloadCFDI(ext) {
        return function (d) {
            return '/facturacion/getXML/' + d.row.opFolioFiscal;
        }
    }

    fields.checkers = Field('checkers', {
        value : f('opOperacionId'),
        caption : ' ',
        enterHeading : function (sel) {
            sel.call(Field.enterHeading);

            sel.append('input')
                .attr('type', 'checkbox')
                .attr('value', f('value'))
                .on('change', function (d) {

                    var checked = d3.select(this).property('checked');

                    ctx.table.rows()
                        .map(f('opOperacionId'))
                        .map(ctx.$sel[checked ? 'select' : 'unselect'])

                    ctx.table.redraw(true);
                    m.redraw();
                });
        },
        enter : function (sel) {
            sel.call(Field.enter);
            sel.append('input')
                .attr('type', 'checkbox')
                .attr('value', f('value'))
                .on('change', function (d) {
                    if(d3.select(this).property('checked') == true) {
                        ctx.$sel.select(d.value);
                    } else {
                        ctx.$sel.unselect(d.value)
                    }

                    ctx.table.redraw(true);
                    m.redraw();
                })
        },
        update : function (sel) {
            sel.select('input').property('checked', function (d) { return ctx.$sel.isSelected(d.value); })
        }
    });

    ctx.table.rowUpdate(function (sel) {
        sel.classed('info', function (d) {
            return ctx.$sel.isSelected( d.opOperacionId )
        })
    });

    ctx.table.columns = [
        fields.checkers,
        fields.opFecha,
        fields.operacion,
        //fields.opReferencia,
        fields.tercero,
        fields.opMoneda,
        fields.opTotal,
        fields.saldoEstatus
    ];


    ctx.table.columns = [
        fields.checkers,
        //fields.opUpdatedAt,
        fields.opFecha,
        //fields.fechas,
        //fields.operacion,
        fields.opTitulo,
        fields.opReferencia,
        fields.tercero,
        fields.totalMoneda,
        fields.opSaldo
    ];

    if(oorden.org().codigo_pais === 'MX') {
        ctx.table.columns.push(fields.cfdi);
    }
}


TablaOperaciones.config = function (ctx) {
    return function (element, isInitialized) {
        if( ctx.table.mounted() ) return;

        if(ctx.table.redraw() == true || (ctx.operaciones() && ctx.operaciones() != ctx.table.rows()) ) {
            ctx.table.rows( ctx.operaciones() );
            ctx.table.redraw(false);

            d3.select(element).call(ctx.table, ctx.table.columns, ctx.table.rows(), {
                sort : ctx.sort,
                sortDirection : ctx.sortDirection,
                total : ctx.total(),
                offset : ctx.offset(),
                pageSize : ctx.limit()
            });

            oor.time.update(element)
        }

        if(isInitialized) return;


        $(element).on('click', '[data-sort-by]', function (ev) {
            var sort = $(this).attr('data-sort-by');
            if(ctx.sort() == sort) {
                ctx.sortDirection(ctx.sortDirection() == '+' ? '-' : '+')
            }

            ctx.sort(sort);
            ctx.ready(false);
            m.redraw()
        });


        $(element).on('click', '[data-page-prev]', function () {
            var offset = Number(ctx.offset()) - Number(ctx.limit());
            if(offset < 0) return;

            ctx.offset(offset);
            ctx.ready(false);
            m.redraw();
        });


        $(element).on('click', '[data-page-next]', function () {
            var offset = Number(ctx.offset()) + Number(ctx.limit()) ;
            if(offset >= ctx.total()) return;

            ctx.offset(offset);
            ctx.ready(false);
            m.redraw();
        });

    }
}
