module.exports = {
    oninit : function (vnode) { this.ctx = new ListadoController(vnode.attrs); },
    view   : function (vnode) { return ListadoView(this.ctx) },
};

var SearchBar  = require('../../misc/components/SearchBar');
var FechasBar  = require('../../misc/components/FechasBar2');
var TablaOperaciones = require('./tablaOperaciones');
var ActionsOperaciones = require('./actionsOperaciones');
var FechaInput = require('../../inputs/fechaInput');

function ListadoController (params) {
    var ctx = this;
    var search = m.prop('');
    var queryParams = m.parseQueryString(location.search);
    oor.fondoGris();

    this.seccion = m.prop(params.seccion);
    this.loading = m.prop(false);
    this.ready = m.prop(false);
    this.tabs = m.prop([]);
    this.search = m.prop('');
    this.tab = m.prop();
    this.sortDirection = m.prop('+');
    this.total = m.prop();
    this.offset = m.prop();
    this.count = m.prop();
    this.limit = m.prop(50);
    this.sort = m.prop();
    this.operaciones = m.prop();

    ctx.idAccion    = m.prop(this.seccion() == 'compras' ? 'Compra' : 'Venta');
    ctx.principal   = m.prop(this.seccion() == 'compras' ? 'com'    : 'vta');
    ctx.notaCredito = m.prop(this.seccion() == 'compras' ? 'cnc'    : 'vnc');

    //Extra Params
    ctx.tTerceroId      = m.prop(queryParams.tTerceroId);
    ctx.opVendedorId    = m.prop(queryParams.opVendedorId);
    ctx.opSucursalId    = m.prop(queryParams.opSucursalId);
    ctx.opMoneda        = m.prop(queryParams.opMoneda);
    ctx.opTipoOperacion = m.prop(queryParams.opTipoOperacion);


    ctx.searchBar = new SearchBar.controller({
        search : search,
        onsearch : function () {
            if(ctx.search() != search()) {
                ctx.search(search());
                ctx.offset(0);
                ctx.ready(false);
                m.redraw();
            }
        }
    });

    /*
    ctx.fechasBar = new FechasBar.controller({
        onchange : function () {
            ctx.offset(0);
            ctx.ready(false);
            //m.redraw();
        }
    });
    if(!queryParams.fechaDesde && !queryParams.fechaHasta) {
        ctx.fechasBar.esteAno();
    } else {
        if(queryParams.fechaDesde) ctx.fechasBar.fechaDesde(queryParams.fechaDesde);
        if(queryParams.fechaHasta) ctx.fechasBar.fechaHasta(queryParams.fechaHasta);
    }
    */

    this.initialize = function () {
        if(this.ready() || this.loading()) return;
        this.loading(true);

        TablaOperaciones(ctx);

        oor.request('/operaciones/listado/'.concat(ctx.seccion()), 'GET', {
            data : {
                //busqueda : ctx.search() ? ctx.search() : undefined,
                tab    : ctx.tab(),
                order  : ctx.sort() ? ctx.sortDirection() + ctx.sort() : undefined,
                offset : ctx.offset() ? ctx.offset() : undefined,
                limit  : ctx.limit() ? ctx.limit() : undefined
                /*
                fecha_desde : ctx.fechasBar.value.fechaDesde() ? ctx.fechasBar.value.fechaDesde() : undefined,
                fecha_hasta : ctx.fechasBar.value.fechaHasta() ? ctx.fechasBar.value.fechaHasta() : undefined,
                tTerceroId : ctx.tTerceroId() ? ctx.tTerceroId() : undefined,
                opVendedorId : ctx.opVendedorId() ? ctx.opVendedorId() : undefined,
                opSucursalId : ctx.opSucursalId() ? ctx.opSucursalId() : undefined,
                opMoneda : ctx.opMoneda() ? ctx.opMoneda() : undefined,
                opTipoOperacion : ctx.opTipoOperacion() ? ctx.opTipoOperacion() : undefined
                */
            }
        })
        .run(asignarDatos)

    };


    this.reload = function () {
        this.ready(false);
        this.initialize();
    }

    function asignarDatos (data) {
        ctx.loading(false)
        ctx.ready(true)

        Object.keys(data).forEach(function(key) {
            var prop = ctx[key];
            if(nh.isFunction(prop)) prop(data[key]);
        });
    }


}


window.date = m.prop('2016-01-01');


function ListadoView (ctx) {
    ctx.initialize();

    return oor.panel({
        title : ctx.seccion().toUpperCase(),
        buttons : [
            oor.stripedButton('a.btn-success', 'Nueva '.concat(ctx.idAccion()), {
                style : 'margin-right:18px',
                href:'/operaciones/'.concat(ctx.principal().concat('/crear')),

            }),

            oor.stripedButton('a.btn-warning', 'Nueva Devolución', {
                style : 'margin-right:18px',
                href:'/operaciones/'.concat(ctx.notaCredito().concat('/crear')),

            }),

            /*
            oor.dropdown({
                button : oor.stripedButton('button.btn-primary', ['Exportar', m('span.caret')]),
                options : [
                    m('a#make-pdf[href=javascript:;]', {}, [m('i.fa fa-file-pdf-o')," PDF"]),
                    m('a#make-csv[href=javascript:;]', {}, [m('i.fa fa-file-csv-o')," CSV"]),
                ]
            })
            */

            m('.btn-group', [
                m('.btn btn-primary button-striped button-full-striped button-striped btn-default btn-xs dropdown-toggle btn-ripple ', {'data-toggle':'dropdown'},
                    ['Exportar ', m("span.caret") ]
                ),

                m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                    m("li", m('a#make-pdf-operaciones', {
                        href:'javascript:', modelo : 'Operaciones',
                        vista : 'operaciones-multi'}, [
                            m('i.fa fa-file-pdf-o')," PDF"
                        ])
                    ),

                    m("li", m('a#make-csv-operaciones',{href:'javascript:', modelo : 'Operaciones', vista : 'operaciones-multi'},  [
                            m('i.fa fa-file-excel-o')," Excel"
                        ])
                    )
                ])
            ])
        ]
    }, [

        m(FechaInput.component, { model : date }),

        ListadoTabs(ctx),

        ctx.loading() ? oor.loading() : m('.table-responsive', [
             m('table.table', {
                oncreate : function (vnode) {
                    TablaOperaciones.config(ctx)(vnode.dom,false);
                }
             })
        ])
    ]);


}

function ListadoTabs (ctx) {
    return m('ul.nav.nav-tabs', [
        ctx.tabs().map(function (tab) {
            var klass = tab.name == ctx.tab() ? 'active' : '';
            return m('li', {'class':klass, onclick:selectTab(ctx,tab),filtro:tab.name}, [
                m('a', {href:'javascript:'}, [
                    tab.caption,
                    ' ',
                    tab.badge ? m('span.t.small', {style:'border-radius:12px; background:#f0f0f0; padding:4px; width:2em'}, tab.badge) : null
                ])
            ]);
        })
    ]);
}

function selectTab(ctx, tab) {
    return function () {
        if(ctx.tab() == tab.name) return;
        ctx.tab(tab.name);
        ctx.ready(false);
        m.redraw();
    }
}
