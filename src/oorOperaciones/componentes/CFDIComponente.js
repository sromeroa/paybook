
var CFDIComponente = module.exports = {
    title    : m.prop('CFDI'),
    btnLabel : [ m('i.fa.fa-file-code-o'), ' CFDI'],
    view : cfdiView,
    xmlLink : function (o) { return '/facturacion/getXML/' + o.folio_fiscal },
    pdfLink : function (o) { return '/facturacion/getXML/' + o.folio_fiscal }
};



function cfdiView (vnode) {
    return m('.row.operacion-cfdi', [
        m('ul.col-xs-6', [
            m('li',[
                m('a.btn.btn-flat.btn-default', {href:'/facturacion/getXML/' + this.xmlLink(vnode.attrs.operacion) }, [
                    m('i.fa.fa-file-code-o'), ' Descargar XML'
                ]),
            ]),
            m('li',[
                m('a.btn.btn-flat.btn-default', {href:'/facturacion/getXML/' + this.pdfLink(vnode.attrs.operacion) }, [
                    m('i.fa.fa-file-pdf-o'),  ' Descargar PDF'
                ])
            ]),
        ]),
        m('form.col-xs-6', [
            m('.input-form', [
                m('label', 'Enviar a:'),
                m('input[type=email]'),
                m('button.btn.btn-flat.btn-primary.btn-xs', 'Enviar')
            ])

        ])
    ]);
}
