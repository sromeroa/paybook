
var EncabezadoOperacion = module.exports = {};
var FechaInput  = require('../../inputs/fechaInput');
var TerceroSelector = require('../../misc/selectors/TerceroSelector.v1.js');

var vencimiento = m.prop();

EncabezadoOperacion.view = function (vnode) {
    var operacion = vnode.attrs.operacion;

    return m('.encabezado-operacion', [

        m('.col-sm-6', [
            m(TerceroSelector, {
                model   : vnode.attrs.terceroId,
                tercero : operacion.tercero,
                onchange : function () {
                    var tercero = this.selected();
                    if(tercero) {
                        operacion.tercero = this.selected().data;
                    } else {
                        operacion.tercero = null;
                    }
                }
            }),

            /*
            m('hr', {style:'margin:2px'}),

            m('h6', {style:'margin-bottom:10px'}, m('span.small', 'Dirección Fiscal: '), 'Jose Luis Lagrange #103'),

            m('h6',m('span.small', 'Entregar En: '), 'Jose Luis Lagrange #103')
            */
        ]),

        m('.col-sm-6', [

            //m('div[style=position:relative;top:-20px;right:-20px;background-color:#f0f0f0;padding: 2px 8px;border-radius:4px]', [



                m('h6.small.pull-right', {
                    style : 'background-color:#f9f9f9;padding:4px;border-radius:4px; position:relative; top:-20px;right:-20px;margin-bottom:-20px'
                }, [
                    'Estatus: ', m('strong.text-indigo', 'APLICADA'),
                ]),

                m('.clear'),

            //]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '}, [
                    m('label', 'Referencia: '),
                    m('input[type=text]', {
                        value : operacion.referencia,
                        disabled : true
                    })
                ])
            ]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '}, [
                    m('label', 'Vendedor: '),
                    m('input[type=text]', {
                        value : operacion.referencia,
                        disabled : true
                    })
                ])
            ]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Vencimiento: '),
                    m(FechaInput.component, { model : vencimiento })
                ]),

                m('.mt-inputer',  {style:'flex: 1 5 '},[
                    m('label', 'Moneda:'),
                    m('input[type=text]', {
                        value : operacion.moneda,
                        disabled : true
                    })
                ])
            ]),


            m('.flex-row', [

                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Tipo de Cambio: '),
                    m('input[type=text]', { value : operacion.tipo_de_cambio })
                ]),


                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Tasa de Cambio: '),
                    m('input[type=text]', { value : operacion.tasa_de_cambio })
                ]),

            ]),


            m('h6.small.text-center',{
                style:'margin:0 20px; background-color:#f0f0f0;border-radius:4px;padding:4px'
            }, [
                'Afectación Contable: ', m('strong.text-indigo', 'Diario #3')
            ]),


        ])
    ]);
}
