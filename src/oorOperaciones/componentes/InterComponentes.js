var archivos = require('./ArchivosComponente');
var notas = require('./NotasComponente')
var cfdi = require('./CFDIComponente')
var bitacora = require('./BitacoraComponente')

module.exports = {
    archivos : convertir(archivos),
    bitacora : convertir(bitacora),
    cfdi : convertir(cfdi),
    notas : convertir(notas)
};

/**
 * Le pone un wrap alas funciones
 */
function convertir (iComponente) {
    var originalView = iComponente.view;

    return _.extend({}, iComponente, {

        view : function (vnode) {
            return m('div[style=padding:10px 20px]', [
                m('.close', { onclick:vnode.attrs.close },'×'),
                m('h4.text-indigo', vnode.state.btnLabel),
                originalView.call(vnode.state, vnode)
            ]);
        }

    });
}
