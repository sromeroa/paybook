/**
 *  Creamos nuestro Modelo
 */
var ncf = module.exports = {};


/**
 * Creamos una lista vacia de la lista
 * @type Array
 */
ncf.lista = Array;

/**
 * Seteamos los valores en un objeto que meneje mithril
 * @param  array data
 * @return void
 */
ncf.insertar = function(data) {
    /**
     * Creamos la propiedades de la lista
     * @type Varios
     */
    this.ncf_id = m.prop(data.ncf_id);
    this.tipo_ncf_id = m.prop(data.tipo_ncf_id);
    this.organizacion_id = m.prop(data.organizacion_id);
    this.ncf = m.prop(data.ncf);
    this.estatus = m.prop(data.estatus);
    this.created_at = m.prop(data.created_at);
    this.updated_at = m.prop(data.updated_at);
    this.tipo = m.prop(data.tipo.nombre);
};

/**
 * View-Model interaccion
 * @return objeto
 */
ncf.vm = (function() {
    var vm = {};
    /**
     * Metodo inicial de la operacion
     * @return {[type]}        [description]
     */
    vm.init = function() {
        //a running list of todos
        vm.lista = new ncf.lista();
        this.getNcf();
    };

    vm.getNcf = function(){
       m.request({
            url : '/apiv2?modelo=ncf&include=ncf.tipo',
            method : 'GET'
        }).then(function (r) {
            if(r.data != 'No hay registros para mostrar.'){
                r.data.map(function (data) {
                    vm.lista.push(new ncf.insertar(data));
                });
            }
        });
    };
    return vm;
}())

ncf.controller = function()  {
    ncf.vm.init(null, null);
};

ncf.view = function(ctrl) {
    return m('div', [
        m('div.table-responsive', [
            m('table.tabla-partidas.table.bill', [
                m('thead', 
                    m('tr',[
                        m('th','Tipo'),
                        m('th','NCF'),
                        m('th','Estatus'),
                        m('th','Creado'),
                        m('th','Usado'),
                    ])
                ),
                m('tbody',[
                    ncf.vm.lista.map(function(data) {
                        return m('tr',[
                            m('td',data.tipo()),
                            m('td',data.ncf()),
                            m('td',data.estatus()),
                            m('td',oor.fecha.format(data.created_at())),
                            m('td',oor.fecha.format(data.updated_at())),
                        ]);
                    })
                ])
            ])
        ])
    ]);
};
