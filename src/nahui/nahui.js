(function (glb) {

    var nahui = {
        globals : ['f']
    };

    function noop () {

    }

    function _isFunction (d) {
        return typeof d === 'function';
    };

    function identity () {
        return arguments[0]
    }

    function extend (source) {
        var i;

        [].forEach.call(arguments, function (obj) {
            if(obj === source) return;

            for(i in obj) {
                source[i] = obj[i];
            }
        });

        return source;
    }


    /**
     *
     */
    function memoize (fn) {
        var mem = {};
        return function (val) {
            if(!mem[val]) {
                mem[val] = fn(val);

            }
            return mem[val]
        }
    }


    /**
     *
     */
    function f (name) {
        fget.str = function () { return name };

        fget.is = function (search) {
            return function (obj) { return fget(obj) == search; }
        };

        return fget;

        function fget (obj) {
            return _isFunction(obj[name]) ? obj[name]() : obj[name];
        }
    }


    function xProp (callback, value) {
        if(!callback) callback = identity;

        xprop(value);

        return xprop;

        function xprop () {
            if(arguments.length) {
                value = callback(arguments[0]);
            }
            return value;
        }
    }

    function iProp (value, set) {
        return function iprop () {
            if(arguments.length) {
                value = set(arguments[0]);
            }
            return value;
        }
    }


    function getSetProp (value, setter) {

    }


    extend(nahui, {
        noop : noop,
        identity : identity,
        xProp : xProp,
        iProp : iProp,
        f : memoize(f),
        memoize : memoize,
        extend : extend,
        isFunction : _isFunction
    });

    nahui.globals.forEach(function (key) {
        glb[key] = nahui[key]
    });

    glb.nahui = glb.nh = nahui;

}).call(window, window)
