module.exports = Collection;

var Field = require('./nahui.field');

function Collection (name, config) {
    if(!(this instanceof Collection)) return new Collection(name,config);
    var coll = this;
    var elements = [];
    var fields = {};
    var events = {};

    if(!config) config = {};

    coll.name = name;
    coll.identifier = config.identifier || 'id';
    coll.id = f(coll.identifier);

    (coll.fields || []).forEach(function (field) {
        fields[field.name] = field;
    });

    coll.getField = function (name) {
        if(!fields[name]) {
            console.warn('Creating Field For Query '.concat(name))
            fields[name] = new Field(name);
        }
        return fields[name];
    }

    coll.insert = function (item) {
        elements.push(item);
    };

    coll.find = function (iQuery) {
        var query = iQuery ? iQuery : {};

        //Armo la consulta que quiero
        if(typeof query != 'object') {
            query = {};
            query[coll.identifier] = iQuery;
        }

        var filter = Collection.createFilterFromQuery.call(this, query);
        return elements.filter(filter);
    };


    coll.query = function (query) {
        return Query(coll, query)
    }

    coll.removeAll = function (d) {
        var els = elements.map(nh.identity);

        els.forEach(coll.remove);
    }

    coll.remove = function (d) {
        var idx = elements.indexOf(d);

        if(idx > -1) {
            elements.splice(idx,1);
        }
    }
}




function Query (collection, initialQuery) {

    if(!(this instanceof Query)) return new Query(collection, initialQuery);

    if(!(collection instanceof Collection)) {
        throw 'Collection Invalid'
    }

    initialQuery = nh.extend({}, initialQuery);

    var diffQuery = {};
    var currentQuery;
    var query = this;
    var events = {};

    updateCurrentQuery();



    query.add = function (conditions) {
        nh.extend(diffQuery, conditions);
        updateCurrentQuery();

        return query;
    };

    query.rem = function (k) {
        delete(diffQuery[k]);
        updateCurrentQuery();

        return query;
    };

    query.on = function (eventName, listener) {
        if(! events[eventName] ) {
            events[eventName] = [];
        }
        events[eventName].push(listener);
    };

    query.exec = function () {
        getResults();
    };

    query.fire = function (eventName, data) {
        (events[eventName] || []).forEach(function (listener) {
            listener.call(null, data);
        });
    };

    function updateCurrentQuery () {
        currentQuery = nh.extend({}, initialQuery, diffQuery, initialQuery);

        Object.keys(currentQuery).forEach(function (k) {
            if(typeof currentQuery[k] === 'undefined') {
                delete currentQuery[k];
            }
        })
    }

    function getResults () {
        var results = collection.find(currentQuery);
        setTimeout(query.fire.bind(this, 'results', results),10);
    }
}



/**
 * Prueba un field contra un valor
 */
function test (field, value) {
    return function (item) {
        return field.value(item) == value;
    }
}

/**
 * Tests
 */

tests = {};

tests.$equals = test;

tests.$gt = function (field, value) {
    return function (item) {
        return field.value(item) > value;
    }
};

tests.$lt = function (field, value) {
    return function (item) {
        return field.value(item) < value;
    }
};

tests.$gte = function (field, value) {
    return function (item) {
        return field.value(item) >= value;
    }
};

tests.$lte = function (field, value) {
    return function (item) {
        return field.value(item) <= value;
    }
}

tests.$in = function (field, value) {
    var filterFunctions = value.map(test.bind(null, field));
    return Collection.testOR(filterFunctions);
}

tests.$contains = function (field, value) {
    value = tests.$contains.filter(value);
    return function (item) {
        return tests.$contains.filter(field.value(item)).indexOf(value) > -1;
    }
}


tests.$contains.filter = function (value) {
    return String(value).toLowerCase().latinise();
}

function test_object (field, object) {
    var testsArray = Object.keys(object).map(function (cond) {
        if(! tests.hasOwnProperty(cond) ) {
            throw 'Condicion inválida: '.concat(cond)
        }
        return tests[cond](field, object[cond]);
    });
    return Collection.testAND(testsArray);
}



/**
 * Pasándole un array de funciones (filters)
 * Devuelve una funcion (filter)
 * que preuba que todas las funciones se cumplan
 */
Collection.testAND = function (arr) {
    var i;
    return function (item) {
        for(i in arr) {
            if(! arr[i](item)) return false;
        }
        return true;
    }
}

Collection.testOR  = function (arr) {
    var i;
    return function (item) {
        for(i in arr) {
            if(arr[i](item)) return true;
        }
        return false;
    }
}

Collection.createFilterFromQuery = function (q) {
    var coll = this;

    var filterArray = Object.keys(q).map(function (key) {
        var val = q[key];

        if(typeof val == 'object'){
            return test_object(coll.getField(key), val);
        }

        return test(coll.getField(key), val);
    });

    return Collection.testAND(filterArray);
}
