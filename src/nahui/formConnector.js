module.exports = FormConnector;



function FormConnector (cfg) {
    if(!(this instanceof FormConnector)) return new FormConnector(cfg);
    var conn = this;


    conn.resource = m.prop();

    conn.onMessage = m.prop();

    conn.element = nh.iProp(null, function (element) {


        $(element).on('change', '[x-cell-input]', function (ev) {
            var input = $(this);
            var xRow = input.parentsUntil(element, '[x-row]');
            var message = {};


            message.kind = 'change';
            message.prop = input.attr('x-cell-input');
            message.xRow = xRow.attr('x-row');
            message.value = input.val();


            conn.emit(message);
        });


        $(element).on('click', '[x-action]', function (ev) {
            var elem = $(this);
            var xRow = elem.parentsUntil(element, '[x-row]');
            var message = {};

            message.kind = elem.attr('x-action');
            message.prop = elem.attr('x-cell-input');
            message.xRow = xRow.attr('x-row');

            conn.emit(message);
        })

    });

    conn.emit = function (message) {
        var callback = conn.onMessage();

        if(nh.isFunction(callback)) {
            callback(message)
        }
    }

    /*
    conn.selector = function (propName) {
        return '[x-cell-input=' + propName + ' ]'
    }

    conn.set = function (prop, value) {
        console.log(prop, value);

        var resource = conn.resource();
        var input  = conn.getInput(prop);

        if(resource && nh.isFunction(resource[prop])) {
            resource[prop](value);
        }

        if(input) {
            input.val(value);
        }
    }

    conn.get = function (prop) {
        var resource = conn.resource();

        if(resource) {
            return f(prop)(resource);
        }
    }
    */


}