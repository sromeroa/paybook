
var Field = require ('./nahui.field');
module.exports = example;


var DTABLE;

function example(selector, DTable) {
    setTimeout(execExample.bind(null, selector, DTable), 1000);
}

function execExample(selector, DTable) {
    var dTable = DTable();

    DTABLE = DTable;

    var currency = d3.format(',.2f');

    var documento = Field.linkToResource('documento', {
        caption : 'Documento',
        class : 'text-indigo',
    });

    var referencia = Field('referencia', {
        caption:'Referencia',
        'class':'text-grey'
    });


    var moneda = Field('moneda', {
        'class' : 'text-right text-grey'
    });

    var saldo = Field('saldo', {
        caption:'Saldo',
        filter:currency,
        'class':'text-right'
    });

    var total = Field('total', {
        caption:'Total',
        filter:currency,
        'class':'text-right'
    });


    var columns = [
        Field('fecha',{
            caption:'Fecha'
        }),

        Field('operacion_id'),

        Field.twice('doc_ref', {
            subfields : [documento,referencia]
        }),

        Field('fecha_vencimiento',{
            caption:'Vence'
        }),

        Field('fecha_esperada', {
            caption:'Esp.'
        }),

        Field.twice('total_moneda', {
            caption : 'Total',
            subfields : [total, moneda]
        }),

        saldo,

        Field('estatus', {
            'class':'text-center',
            caption : 'Estatus',
            filter :  function (st) {
                return oor.mm('Operacion').estatuses[st]().nombre_corto;
            }
        })
    ];



    m.request({
        url : '/apiv2?modelo=operaciones',
        method : 'GET'
    }).then(function (d) {
        d3.select(selector)
            .html('')
            .append('table')
            .attr('class','table')
            .call(dTable, columns, d.data);
    })
}
