require('./nahui');

var Field = require('./nahui.field');

module.exports = DTable;


function computeHeading (columns, settings) {
    settings || (settings = {});

    return function () {
        return columns.map(computeHeader);
        function computeHeader  (col) {
            var celda = {};

            celda.name = col.name;
            celda.text = col.caption;

            celda.html = function () {
                var caption = '';
                if(settings.sort && settings.sort() == col.name) {
                    caption = '<i class="'.concat(settings.sortDirection() == '+' ? 'ion-arrow-down-b' : 'ion-arrow-up-b', '"></i> '); 
                }
                return caption + col.caption;
            }

            celda.update = col.updateHeading;
            celda.enter = col.enterHeading;
            celda.column = col;

            if(col.subfields) {
                celda.subfields = col.subfields.map(computeHeader)
            }

            return celda;
        }
    }
}

/**
 * Armar los datos de las celdas
 */
function computeData (columns, settings) {
    return function (row) {
        return columns.map(computeColumn);

        function computeColumn (col) {
            var celda = {};

            celda.name = col.name;
            celda.column = col;
            celda.row = row;

            celda.value = col.value(row);
            celda.text = col.filter(celda.value);

            celda.update = col.update;
            celda.enter = col.enter;

            celda.enterEdit = col.enterEdit;

            if(col.subfields) {
                celda.subfields = col.subfields.map(computeColumn)
            }

            return celda;
        }
    }
}



function DTable () {
    var columns, rows, rowUpdate;
    
    rowUpdate = _.noop; 

    var HEAD = {
        compute : computeHeading,
        cellTag : 'th',
        enter : Field.enterHeading,
        update : Field.updateHeading,
        key : function (d,i) {
            return i
        }
    };

    var BODY = {
        compute : computeData,
        cellTag : 'td',
        enter : Field.enter,
        update : Field.update,
        key : f('id')
    };


    dtable.key = function () {
        if(arguments.length == 0) return BODY.key;
        BODY.key = arguments[0];
        return dtable;
    };


    dtable.rowUpdate = function () {
        if(arguments.length == 0) return rowUpdate;
        rowUpdate = arguments[0];
        return dtable;
    }

    function updateRows(tHead, columns, rows, kind, settings) {
        var filas, celdas, nuevasCeldas, filasEnter, filasEdit;

        var rowIds = rows.map(kind.key);

        // Se crean las filas
        // verificando si es editable
        filas = tHead.selectAll('tr').data(rows, function (d) { 
            return ( f('$edit')(d) ? '$edit:' : '').concat(kind.key(d));
        });

        filas.exit().remove();

        filas.enter()
            .append('tr')
            .attr('x-row', kind.key)

        //Sort, hay que verificarlo
        filas.sort(function (r1, r2) {
            return rowIds.indexOf(kind.key(r1)) - rowIds.indexOf(kind.key(r2));
        })
        .call(dtable.rowUpdate())

        /*
        filasEdit = filas.filter(f('$edit')).call(function (filas) {
            //Se seleccionan las filas y las celdas
            celdas = filas.selectAll(kind.cellTag).data(kind.compute(columns), f('name'))
            nuevasCeldas = celdas.enter().append(kind.cellTag)
        })
        filas.filter(function (d) { return !f('$edit')(d); })
        */
        //Se seleccionan las filas y las celdas

        celdas = filas.selectAll(kind.cellTag).data(kind.compute(columns, settings), f('name'));

        nuevasCeldas = celdas.enter().append(kind.cellTag)

        nuevasCeldas
            .filter(function (c) { return c.row && f('$edit')(c.row) })
            .each(function (c) { d3.select(this).call( c.enterEdit || c.enter || nh.noop) })

        //Aplicar enter a las celdas
        nuevasCeldas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.enter) === true })
            .each(function (c) { d3.select(this).call(c.enter); })

        nuevasCeldas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.enter) === false })
            .each(function (c) { d3.select(this).call(kind.enter); })

        //Actualizar las celdas que no tienen UpdateFunction
        celdas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.update) === false })
            .call(kind.update)

        //Actualiar las celdas con actualizador
        celdas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.update) === true })
            .each(function (c) { d3.select(this).call(c.update) })
    }

    function tFooter (tFoot, columns , settings) {
        if(!settings) return;

        var filas = tFoot.selectAll('tr').data([1]);
        var thEnter = filas.enter().append('tr').append('th')


        thEnter.append('span')

        thEnter.append('div').attr('class', 'btn-group')


        thEnter.select('.btn-group')
            .append('button')
            .attr('class', 'btn btn-default btn-xs btn-flat')
            .attr('data-page-prev','')
            .append('i')
            .attr('class','ion-chevron-left')

        thEnter.select('.btn-group')
            .append('button')
            .attr('class', 'btn btn-default btn-xs btn-flat')
            .attr('data-page-next','')
            .append('i')
            .attr('class','ion-chevron-right')



        filas.select('th')
            .attr('class', '')
            .attr('colspan', columns.length);

        filas.select('span').text(
            ''.concat(
                settings.offset + 1,
                ' \u2014 ',
                Math.min(settings.total, settings.offset + settings.pageSize), 
                ' de ',
                settings.total
            )
        );

    }




    function dtable (table, columns, rows, settings) {
        table.classed('megatable', true);

        if(table.select('thead').empty()){
            table.append('thead');
        }

        if(table.select('tbody').empty()) {
            table.append('tbody')
        }

        if(table.select('tfoot').empty()) {
            table.append('tfoot')
        }

        table.select('tbody').call(updateRows, columns, rows, BODY, settings);

        table.select('thead').call(updateRows, columns, [1], HEAD, settings);

        table.select('tfoot').call(tFooter, columns, settings)
    }

    return dtable;
}
