module.exports = dtableHelper;

/**

    Forma Vieja
    ============

    table = dynTable().key( f('operacion_id') );
    coll = nahuiCollection('operaciones', {
      identifier : 'operacion_id'
    });
  
    query = coll.query({})
  
    ctx.columnas =  m.prop([
        checkers,
        fields.fecha,
        fields.doc_ref,
        fields.nombre_tercero,
        fields.total_moneda,
        fields.saldo,
        fields.estatus
    ]);


    thelper = TableHelper()
                .table(table)
                .query(query)
                .columns(ctx.columnas())
                .set({ pageSize : 10 })

    


    Forma nueva
    ===========

    thelper = TableHelper({
        identifier : 'operacion_id'
    });
    
    thelper.collection()
    thelper.query()
    thelper.table() //Table
    thelper.columns()


    
**/

function dtableHelper () {
    var tableFn;
    var element;
    var query;
    var rows;
    var lastResults;

    var settings = {
        offset : 0,
        pageSize : 25
    };

    function draw () {
        if(!rows) return;
        d3.select(element).call(tableFn, columns, rows, settings)
    }


    function pageizeRows () {
        rows = lastResults.slice(
            settings.offset,
            settings.offset + settings.pageSize
        );
    }

    helper.draw = draw;

    helper.table = function () {
        if(arguments.length) {
            tableFn = arguments[0];
            return helper;
        }
        return tableFn
    };

    helper.element = function () {
        if(arguments.length) {
            element = arguments[0];


            element.addEventListener('click', function (ev) {
                if(ev.target.matches('[data-page-next], [data-page-next] *')) {
                    var offset = settings.offset + settings.pageSize;
                    if(offset >= settings.total) offset = settings.offset;

                    helper.set({ offset : offset })
                    pageizeRows();
                    draw();
                    return;
                }

                if(ev.target.matches('[data-page-prev], [data-page-prev] *')) {
                    var offset = settings.offset - settings.pageSize;
                    if(offset < 0) offset = 0;

                    helper.set({ offset : offset });
                    pageizeRows();
                    draw();
                }
            }, false);


            return helper;
        }

        return element;
    };



    helper.query = function () {
        if(arguments.length) {
            query = arguments[0];

            query.on('results', function (results) {
                lastResults = results;
                
                helper.set({
                    offset : 0,
                    total:results.length
                });

                pageizeRows();
                draw();
            });

            return helper
        }
        return query;
    };

    helper.set = function (iSettings) {
        nh.extend(settings, iSettings)
        return helper;
    };


    helper.columns = function () {
        if(arguments.length) {
            columns = arguments[0];
            return helper;
        }
        return columns;
    }


    function helper () {
        query.exec()
    }

    return helper;
}


