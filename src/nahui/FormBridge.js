

module.exports = Bridge;

function Bridge (settings) {
    var context = settings.context;


    return bridge;
    
    function bridge (element, isInited) {

        if(arguments.length > 1 && isInited === false) {
            return;
        } 

        $(element).on('change', '[x-prop]', bridgeChange);
        $(element).on('input', '[x-prop]', bridgeChange)
    }


    function bridgeChange (ev) {
        var input, prop, scope, model;

        input = $(this);
        model = input.attr('x-model');
        scope = model ? f(model)(context) : context;
        prop = scope[input.attr('x-prop')];
        prop(input.val());


        console.log(ev.type);
        
    }

}


