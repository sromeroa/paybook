
module.exports = Field;

function Field (name, config) {
    if(!(this instanceof Field)) return new Field(name,config);
    var field = this;
    if(!config) config = {};

    nh.extend(field, {
        name : name,
        caption : config.caption || name,
        value : nh.isFunction(config.value) ? config.value : f(name),
        filter : nh.isFunction(config.filter) ? config.filter : nh.identity,
        sortFilter : nh.isFunction(config.sortFilter) ? config.sortFilter : nh.identity
    });

    if(config.enterEdit === true) {
        field.enterEdit = Field.enterEdit
    }

    //Copiar todo lo extra que tenga el campo
    Object.keys(config).forEach(function (k) {
        typeof field[k] == 'undefined' && (field[k] = config[k]);
    });
}

Field.updateHeading = function (sel) {
    sel.select('.t-title').html( f('html') )
}


Field.enterHeading = function (d) {
    d.append('div')
        .attr('class', function (celda) {
            return 't-title'.concat(' ', celda.column.headingClass || '')
        })
        .attr('data-sort-by', f('name'))
        .style('cursor', 'pointer')
}

Field.update = function (selection) {
    selection.text( f('text') );
}

Field.enter = function (selection) {
    selection
        .attr('class', function (celda) {
            var attr = this.getAttribute('class') || '';
            return attr.concat(' ','celda-',celda.name, ' ', celda.column.class || '');
        })
        .attr('x-cell', f('name'))
}

Field.enterEdit = function (selection) {
    selection
        .attr('x-cell', f('name'))
        .append('input')
            .attr('x-prop', f('name'))
            .attr('type', 'text')

}

/**
 * Link To Resource
 */
Field.linkToResource = function (name, config) {
    config = nh.extend({
        enter : Field.linkToResource.enter,
        update :  Field.linkToResource.update
    }, config || {});

    return new Field(name, config);
};

Field.linkToResource.enter = function (selection) {

    selection.append('a').attr('data-id', function (d) {
        return d.column.id ? d.column.id(d.row) : null
    }).call(Field.enter);
};

Field.linkToResource.update = function (selection) {
    selection.select('a')
        .attr('href', function (d) { return d.column.url(d.row) })
        .call(Field.update)
};

/**
 * Field para twice
 */
Field.twice = function (name, config) {
    var subfields = config.subfields.map(function (f) {
        return f;
    });

    config = nh.extend({
        updateHeading : Field.twice.updateHeading,
        enterHeading : Field.twice.enterHeading,
        enter : Field.twice.enter,
        update : Field.twice.update
    }, config || {}, {
        value : nh.noop,
        subfields : subfields
    });

    return new Field(name, config);
};

Field.twice.update = function (selection) {

    selection.datum().subfields.forEach(function (f, i) {
        selection.select('.twice'.concat(i+1))
            .datum(f)
            .call(f.update || Field.update)
    });
};

Field.twice.enter = function (selection) {
    selection.call(Field.enter);

    selection.datum().subfields.forEach(function (f, i) {
        var editMode = nh.f('$edit')(selection.datum().row);

        selection.append('div')
            .classed('twice'.concat(i+1), true)
            .datum(f)
            .call( (editMode && f.enterEdit)  || f.enter || Field.enter )
    });
};

Field.twice.updateHeading  = function (sel) {
    sel.datum().subfields.forEach(function (f,i) {
        sel.select('.twice'.concat(i+1))
            .datum(f)
            .call(f.updateHeading || Field.updateHeading)
    });
};

Field.twice.enterHeading = function (sel) {
    sel.datum().subfields.forEach(function (f,i) {
        sel.append('div')
            .attr('class', 'twice'.concat(i+1))
            .datum(f)
            .call(f.enterHeading || Field.enterHeading)
    })
}
