(function (self) {

window.TablaConsulta = TablaConsulta;
window.MtTable = MtTable;

function TablaConsulta (params) {
    var columnas = m.prop(params.columnas);
    var include  = m.prop(params.include || '');
    var defCol   = params.defCol || {};
    var items    = m.prop();

    return {
        view : view,
        controller : controller,
        getItems : getItems,
    };

    function getItems (consulta) {
        var qParams  = angular.extend({
            modelo : params.modeloConsulta,
            include : include()
        }, consulta ,params.parametros);

        return m.request({
            method:'GET',
            url:Modelo.apiPrefix() + '?' + $.param(qParams),
            unwrapSuccess : function (d) {
                return d.data.map(function (i) {
                    i.checked = m.prop(false);
                    return i;
                });
            }
        }).then(items);
    }

    function controller () {
        var self     = this;

        self.columnas = columnas;
        self.items = items;
        self.getItems = getItems;

        self.getValue = function(item, colName) {
            var columna = defCol[colName];

            if(columna && (typeof columna.value === 'function')){
                return columna.value.call(self, item);
            }

            if(typeof item[colName] === 'function') {
                return item[colName];
            }

            return item[colName];
        };

        self.getTitle = function (colName) {
             var columna = defCol[colName];

            if(columna && (typeof columna.caption != 'undefined')) {
                return (typeof columna.caption == 'function') ? columna.caption.call(self) : columna.caption;
            }

            return colName;
        };


        self.getHeading = function (colName) {
            var columna = defCol[colName];
            var title = self.getTitle(colName);

            console.log(columna);

            if(columna.sortable === false) return title;

            return  [
                title, m('i.ion-arrow-up-b')
            ]
        }

        //m.startComputation();
    }

    function view (ctrl, params) {
        return m('div.table-responsive', [
            m('table.table', [
                m('thead', tHead(ctrl)),
                m('tbody', ctrl.items() && tBody(ctrl))
            ])
        ]);
    }

    function tBody (ctrl) {
        return ctrl.items().map(function (item) {
            return m('tr', {'class' : item.checked() ? 'info' : ' '},
                ctrl.columnas().map(function (columna) {
                    return m('td'.concat('.cell-', columna) , ctrl.getValue(item,columna) );
                })
            );
        });
    }

    function tHead (ctrl) {
        return m('tr', [
            ctrl.columnas().map(function (columna) {
                return m('th'.concat('.cell-', columna), ctrl.getHeading(columna));
            })
        ]);

    }
}



function MtTable (args) {
    var columnas  = m.prop(args.columnas || []);
    var items     = m.prop(args.items || []);
    var defCol    = args.defCol;

    view.items    = items;
    view.columnas = columnas;

    view.tFoot = function () {};
    view.tBody = tBody;
    view.tHead = tHead;
    view.sortColumn  = m.prop(null);
    view.sortOrder  = m.prop(null);

    view.itemTrTag = function () {
        return 'tr'
    };

    view.itemTrParams = function () {
        return null
    };

    view.sort = function (colName, order) {
        if(view.sortColumn() == colName && view.sortOrder() == order) return;
        view.sortColumn(colName);
        view.sortOrder(order);

        var factor = order == 'asc' ? 1 : -1;

        view.items().sort(function (a,b) {
            var result = 0;
            var aVal = view.getSortValue(a,colName);
            var bVal = view.getSortValue(b,colName);

            if(aVal > bVal) {
                result = 1;
            } else if(bVal > aVal){
                result = -1;
            }

            return result * factor;
        });
    };


    view.getSortValue = function (item, colName) {
        var sortVal = view.getRawValue(item, colName);
        console.log(sortVal);
        
        if(typeof sortVal === 'undefined') sortVal = view.getValue(item,colName);
        if(typeof sortVal === 'string') return sortVal.latinize().toLowerCase();
        return sortVal;
    };


    view.getValue = function(item, colName) {
        var columna = defCol[colName];
        if(columna && (typeof columna.value === 'function')){
            return columna.value.call(self, item);
        }
        return view.getRawValue(item, colName);
    };

    view.getRawValue = function (item, colName) {
        if(typeof item[colName] === 'function') {
            return item[colName]();
        }
        return item[colName];
    };

    view.getTitle = function (colName) {
        var columna = defCol[colName];
        if(columna && (typeof columna.caption != 'undefined')) {
            return (typeof columna.caption == 'function') ? columna.caption.call() : columna.caption;
        }
        return colName;
    };


    view.getHeading = function (colName) {
        var columna = defCol[colName];
        var title = view.getTitle(colName);
        var isSortable = (!columna || typeof columna.sortable == 'undefined')? true : Boolean(columna.sortable);
        var sorted = view.sortColumn() === colName;
        var order = view.sortOrder ? '.'.concat(view.sortOrder()) : '';

        return  m('div'.concat(isSortable ? '.sortable' : '', sorted ? '.sorted' : '', order), [
            title,
            ' ',
            isSortable ? m('i.ion-arrow-up-b.arrow-sort.asc', {
                onclick:view.sort.bind(null, colName, 'asc')
            }) : '',

            isSortable ? m('i.ion-arrow-down-b.arrow-sort.desc', {
                onclick:view.sort.bind(null,colName ,'desc')
            }) : ''
        ]);
    };

    view.colHeader = function (colName) {
        return m('th.cell-'.concat(colName), view.getHeading(colName));
    };

    function view ( params) {
        return m('div.table-responsive', params, [
            m('table.table', [
                m('thead', view.tHead()),
                m('tbody', items() && view.tBody()),
                m('tfoot', view.tFoot() )
            ])
        ]);
    }

    function tBody () {
        return items().map(function (item) {
            return m(view.itemTrTag(item), view.itemTrParams(item),
                columnas().map(function (columna) {
                    return m('td'.concat('.cell-', columna), [
                        view.getValue(item,columna)
                    ]);
                })
            );
        });
    }

    function tHead () {
        return m('tr', [
            columnas().map(view.colHeader)
        ]);
    }

    return view;
}

//INCLUIMOS LOS HEADERS NECESARIOS (HELPER PARA INYECCIONES)
/**
 * Aqui se configura el injector para mas ejemplos de uso ver cada uno de los módulos
 */
var DIRECTIVE  = 'directive';
var CONTROLLER = 'controller';
var FACTORY    = 'factory';
var SERVICE    = 'service';
var PROVIDER   = 'provider';
var FILTER   = 'filter';
var INJECT     = '$inject';

var VALUE      = 'value';
var CONSTANT   = 'constant';

var RUN        = 'run';
var CONFIG     = 'config';


function createInjector () 
{
    var items = [];

    function inject (type, name, deps, fn) 
    {
        if(!type || !name) 
        {
            console.log('error on dependency');
        }

        items.push({
            type : type,
            name : name,
            fn   : (fn || angular.noop),
            deps : (deps || new Array())
        }); 
    };

    inject.mount = function (module) 
    {
        items.forEach(function(item)
        {
            if(item.type != VALUE && item.type != CONSTANT) 
            {
                item.fn[INJECT] = item.deps;
            }

            
            if(item.type == RUN || item.type == CONFIG) 
            {
                module[item.type](item.fn);
            } 
            else
            {
                module[item.type](item.name,item.fn);
            }
        });
    };

    return inject;
}

/* MODULO BASE */
(function () {

var module = angular.module('oordenBase', ['ngMaterial']);
var inject = createInjector();


inject(FACTORY, 'oorden', ['$q', '$http'], function ($q,$http) {

    return new Oorden();

    function Oorden () {
        var oor = this;
        oor.dNum = dNumber;

        /** 
         * Regresa un tipo de cambio
         */
        var tiposDeCambio = [];
        var tiposDeCambioMap = {}; 
        var getTipoDeCambio = createGetter(tiposDeCambioMap);

        var centrosDeCosto = [];
        var centrosDeCostoActivos = [];
        var centrosDeCostoMap = {};
        var getCentroDeCosto = createGetter(centrosDeCostoMap);

        var elementosDeCosto    = [];
        var elementosDeCostoMap = {};
        var getElementoDeCosto = createGetter(elementosDeCostoMap);

        var cuentasContables = [];
        var cuentasContablesMap = {};
        var getCuentaContable = createGetter(cuentasContablesMap);


        var retenciones = [];
        var retencionesMap = {};

        var impuestos = [];
        var impuestosMap = {};


        var metodosDePago = [];
        var metodosDePagoMap = {};
        var getMetodoDePago = createGetter(metodosDePagoMap);

        
        function centroDeCosto (id) {
            var ccto = getCentroDeCosto(id);
            if(ccto) return ccto;
            else return getCentroDeCosto(getElementoDeCosto(id).centro_de_costo_id);
        };

        oor.metodosDePago = metodosDePago;
        oor.metodoDePago = getMetodoDePago;

        oor.retenciones = retenciones;
        oor.retencion = createGetter(retencionesMap);


        oor.impuestos = impuestos;
        oor.impuesto = createGetter(impuestosMap);


        oor.centroDeCosto   = centroDeCosto;
        oor.centrosDeCosto  = centrosDeCosto;
        oor.centrosDeCostoActivos = centrosDeCostoActivos;
        oor.elementoDeCosto = getElementoDeCosto;
        oor.elementosDeCosto = elementosDeCosto;

        oor.cuentaContable  = getCuentaContable;
        oor.cuentasContables = cuentasContables;

        oor.tipoDeCambio    = getTipoDeCambio;
        oor.tiposDeCambio   = tiposDeCambio;

        oor.obtenerTasa  = obtenerTasa;

        var $ready = false;
        oor.ready = function () { return $ready };

        var defer;

        oor.inicializar = function () {
            if(!defer) {
                defer = $q.defer();
                $http.get('/organizacion/config').success(function (r) {
                    inicializar(r);
                    $ready = true;
                    defer.resolve(true);
                });
            }
            return defer.promise;
        };


        function obtenerTasa (tipoDeCambio) {
            var defer = $q.defer();

            if(! Boolean(Number(tipoDeCambio.base)) ) {
                $http.get('/api/tasa/' + tipoDeCambio.tipo_de_cambio).success(function (r) {
                    defer.resolve(r.ultimaTasa);
                });
            } else {
                defer.resolve({tasa_de_cambio : '1.00000'})
            }

            return defer.promise;
        }

        function cifrasFn (org) {
            var nDecimales  = Number(org.numero_decimales) || 0;
            return function () {
                return nDecimales;
            }
        }


        //Inciializacion
        function inicializar (data) {
            window.OORDEN = oor;
            
            oor.organizacion = data.organizacion;
            oor.organizacion.cifras = cifrasFn(data.organizacion);

            oor.sucursal     = data.sucursal;
            oor.usuario      = data.usuario;
            
            inicializarTiposDeCambio(data.tiposDeCambio);
            inicializarCentrosDeCosto(data.centrosDeCosto);
            inicializarCuentasContables(data.cuentasContables);
            inicializarRetenciones(data.retenciones);
            inicializarImpuestos(data.impuestos);
            inicializarMetodosDePago(data.metodosDePago);
        }

        function inicializarMetodosDePago (data) {
            _inicializar({
                arr  : metodosDePago,
                map  : metodosDePagoMap,
                data : data,
                id   : 'id' 
            });
        }

        function inicializarImpuestos (data) {
            _inicializar({
                arr  : impuestos,
                map  : impuestosMap,
                data : data,
                id   : 'impuesto_conjunto_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarRetenciones (data) {
            _inicializar({
                arr  : retenciones,
                map  : retencionesMap,
                data : data,
                id   : 'retencion_conjunto_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarCuentasContables (data) {
            _inicializar({
                arr  : cuentasContables,
                map  : cuentasContablesMap,
                data : data,
                id   : 'cuenta_contable_id' 
            });

           // console.log(cuentasContablesMap);
        }

        function inicializarTiposDeCambio (data) {
            _inicializar({
                arr  : tiposDeCambio,
                map  : tiposDeCambioMap,
                data : data,
                id   : 'tipo_de_cambio_id' 
            });

            tiposDeCambio.forEach(function (d) {
                getTipoDeCambio[d.tipo_de_cambio] = d;
                
                if(d.base == '1') {
                    getTipoDeCambio.base = d;
                }
            });
        }

        function inicializarCentrosDeCosto (data) {
             _inicializar({
                arr  : centrosDeCosto,
                map  : centrosDeCostoMap,
                data : data,
                id   : 'centro_de_costo_id' 
            });

             centrosDeCosto.forEach(function (cc) {
                cc.elementos.forEach(function (e) {
                    var elemento = angular.extend({}, e, {
                        id : e.elemento_ccosto_id
                    });
                    elementosDeCosto.push(elemento);
                    elementosDeCostoMap[elemento.id] = elemento;
                });
             });

             centrosDeCosto.forEach(function (cto) {
                if(cto.activo == "1") centrosDeCostoActivos.push(cto);
             });
        }

        function _inicializar (params) {
            params.data.forEach(function (d) {
                var item = angular.extend({}, d, {id : d[params.id]});
                params.arr.push(item);
                params.map[item.id] = item;
            });
        }
    };

    function prop (iVal) {
        var val = iVal;
        return function () {
            if(arguments.length) {
                val = arguments[0];
                return;
            }
            return val;
        }
    };

    function createGetter(map) {
        return function (id) {
            return map[id];
        }
    };


    function dNumber (decimales) {
        if (!(this instanceof dNumber)) return new dNumber(decimales);
        var value = 0;

        this.value = function () {
            if(!arguments.length) {
                return value;
            };

            value = Number.round(arguments[0]);
            return this;
        };

        this.asDecimal = function () {
            var str = String(value);
            return [
                str.substring(0, str.length - decimales),
                str.substring(st.length - decimales),
            ].join('.');
        };

        this.asFloat = function () {
            return Number.round(Number(this.asDecimal()));
        };

        this.fromString = function (str) {
            var arr = str.split('.');
            var ent = arr[0];
            var dec = comoDecimales(arr[1]);
            
            if(!dec) {
                dec = '';
            };

            dec = dec.substring(0, decimales);

            value = Number(ent.concat(dec));
        }

        function comoDecimales (d) {
            if(d.length == decimales) return d;

            if(d.length > decimales)  {
                return d.substring(0,decimales);
            }

            if(d.length < decimales) {
                return d.concat(String(Math.pow(10, decimales)).substring(1));
            }
        }

    };


});
inject(FACTORY, 'uSetting', ['$http', '$q'], function ($http, $q) {
    return {
        //get      : getSettings,
        get      : getSetting, 
        set      : setSettings,
        usuario  : usuarioSetting,
        sucursal : sucursalSetting
    };

    function setSettings (r1, r2, settings) {
        var defer = $q.defer();
        var url   = getUrl(r1,r2);

        $http.post(url, settings).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    }

    function getSetting (r1, r2, namespaces)
    {
        var defer = $q.defer();

        var url    = getUrl(r1,r2);
        var params = getParams(namespaces);

        $http.get(url, {params:params}).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;

    }

    function getParams (namespaces) {
        return { namespaces : angular.isArray(namespaces) ? namespaces.split(',') : namespaces};
    }



    function getUrl (r1, r2) {
        var url = '/u-settings/index/';

        url += angular.isArray(r1) ? r1.join(':') : String(r1);
       
        if(r2){
            url += '/';
            url += angular.isArray(r2) ? r2.join(':') : String(r2);
        }

      
        return url;
    }

    function getSettings (settings) {
        var defer = $q.defer();

        console.log('inpit-settings', settings, getUrlParams(settings));

        $http.get('/u-settings', {params :getUrlParams(settings)}).success(function (r) {
            defer.resolve(makeSettings(settings, r.data));
        });

        return defer.promise;
    }

    function makeSettings(iSettings, data) {
        var oSettings = {};

        iSettings.forEach(function (setting) {
            if(!oSettings[setting.type]) {
                oSettings[setting.type] = {};
            }
            oSettings[setting.type][setting.namespace] = {};
        });

        Object.keys(data).forEach(function (k) {
            var keys  = k.split('/');
            var sKeys = keys[2].split('.');
            oSettings[ keys[0] ][ sKeys[0] ][ sKeys[1] ] = data[k];
        });

        return oSettings;
    }


    function getUrlParams (settings) {
        return settings.map(function (setting) {
            if(angular.isString(setting)) return setting;
            var str = setting.type + '/' + setting[setting.type];
            str += (setting.namespace) ? '/' + setting.namespace : '';
            return str;
        }).reduce(function (obj, str, i) {
            obj['setting_' + i] = str;
            return obj;
        }, {});
    }

    function usuarioSetting (opts) {
        return createSetting(opts, {type : 'usuario'});
    }

    function sucursalSetting (opts) {
        return createSetting(opts, {type : 'sucursal'});
    }

    function createSetting (opts, defOpts) {
        return angular.extend({}, defOpts, opts);
    }
});
inject(FACTORY, 'uuid', [], function () {
    return oor.uuid;
})
inject(SERVICE, 'fecha', [], function () {
    return oor.fecha;
});

inject(DIRECTIVE, 'niceNumberInput',['$parse'], function ($parse) {
    var matcher = /[^0-9\.]/g;
    
    function dotReplacer () {
        var times = 0;
        return function () {
            if(times++ == 0) return '.';
            return '';
        }
    }

    return function (scope, element, attrs) {
        element.bind('blur', function () {
            var val = ($(this).val() || '');

            val = val.replace(/\./g, dotReplacer());
            val = Number(val.replace(matcher, ''));

            if(!angular.isUndefined(attrs.decimales)){
                val = val.toFixed(attrs.decimales);
            }

            $parse(attrs.ngModel).assign(scope, val);
            scope.$apply();
        });
    }
});

inject(FILTER, 'contable', ['numberFilter'], function (nf)  {
    return function (d) {
        if(d === null) return '----';
        return nf(d,2);
    }
});


inject(FILTER, 'fechaFromTime', ['fecha'], function (fecha) {
    var cache = {};

    return function (t) {
        if(!cache[t]) {
            cache[t] = fecha.str(new Date(t*1000));   
        }
        return cache[t];
    }
});


inject(DIRECTIVE, 'focusAt', [], function () {

    return function (scope, element, attrs) {
        
        scope.$watch(attrs.focusAt, function (focus) {
            if(!focus) return;
            setFocus();
        });

        function setFocus () {
            scope.$evalAsync(function () {
                setTimeout(function () {
                    element[0].focus();
                },100);
            });
        };
    };

});
inject(DIRECTIVE, 'oorAutocomplete', ['$q', '$http', '$compile', 'Organizacion'], function ($q, $http, $compile, Organizacion) {

    var types = {

        'cuentasContables' : {
            pk : 'cuenta_contable_id',
            controller : controller_cuentasContables,
            name : function (c) { return c.cuenta + ' ' + c.nombre}
        },

        'elementosDeCosto' : {
            pk : 'elemento_ccosto_id', 
            controller: controller_elementosDeCosto,
            name  : 'elemento'
        },

        'impuestos'        : {
            pk : 'impuesto_conjunto_id',
           // controller : controller_impuestos,
            name : 'impuesto_conjunto'
        }, 

        'retenciones'      : {
            pk : 'retencion_conjunto_id',
            //controller : controller_retenciones,
            name : 'retencion_conjunto'
        },


    };

    //terceros, cliente, proveedor, es="cliente|provedor"

    return {
        scope      : true,
        template   : '<md-autocomplete md-items="item in ac.search()" md-item-text="ac.caption(item)" md-selected-item="ac.selected" md-search-text="ac.searchText"> {{ ac.caption(item) }} </md-autocomplete>',
        controller : ['$scope','$attrs',controller],
        require    : 'ngModel',
        link       : link,
        terminal : true,
        controllerAs : 'ac'
    };



    function link (scope, element, attrs, ngModel) {
        //Algo le pone el tab index, se quita
        //para que no sea seleccionable este nodo
        setTimeout(function () {
            element.removeAttr('tabindex')
         },200);

       
        var rmReady = scope.$watch('ac.$ready', function (r) {
            if(!r) return;

            rmReady();

            ngModel.$render = function () {
                scope.ac.setValue(ngModel.$viewValue);
            };

            $compile(element.contents())(scope);
            ngModel.$render();

        });
        

        scope.$watch(scope.ac.selectedKey() , function (selected) {
            if(!selected) return;
            ngModel.$setViewValue(selected);
        });

        if(angular.isDefined(attrs.label)) {
            $('md-autocomplete',element).attr('md-floating-label', attrs.label);
        }
    }
    

    function controller ($scope, $attrs) {
        var ac   = this;
        var type = $attrs.oorAutocomplete;
        var typeDef = types[$attrs.oorAutocomplete];

        ac.items = [];

        if(!typeDef) 
        {
            throw 'oorAutocomplete, el tipo no está configurado ' +  type;
        }



        ac.search = function () {
            return ac.items.filter( ac.createQueryFilter(ac.searchText) );
        };

        ac.caption = function (item) {
            if(!item) return ' ';

            if(angular.isFunction(typeDef.name)) {
                return typeDef.name(item);
            }

            return item[typeDef.name || typeDef.pk];
        };

        ac.setValue = function (value) {
            if(!value) return;

            if(!ac.$ready) {
                ac.renderValue = value;
                return;
            }

            ac.renderValue = null;

            ac.items.forEach(function (i) {
                if(i[typeDef.pk] == value) {
                    ac.selected = i;
                }
            });
        };

        var filters = {};

        ac.createQueryFilter = function (search) {
            var text = angular.lowercase(search);
            
            if(!filters[text]) {
                filters[text] =  function (item) {
                   return angular.lowercase(ac.caption(item)).indexOf(text) > -1;
                }
            }

            return filters[text];
        };

        ac.selectedKey = function () {
            return 'ac.selected.' + typeDef.pk;
        };

        if(typeDef.controller) {
           typeDef.controller.call(this, $scope, $attrs);
        }
    }


    function controller_elementosDeCosto ($scope, $attrs) {
        var ac = this;

        Organizacion.centrosDeCosto().then(function (cc) {
            var rm = $scope.$watch($attrs.centroCosto, function (centro) {
                if(!centro) return;
                ac.$ready = true;
                rm();

                if(cc.centro[centro]) {
                    ac.items = cc.centro[centro].elementos;
                }
                else {
                    ac.items = [];
                }
                
            });
        });

    }

    function controller_cuentasContables ($scope, $attrs) {
        var ac = this;

        function esDetalle (cc) {
            return Number(cc.acumulativa) === 0;
        }

        ac.search = function () {
            return ac.items
                     .filter( esDetalle )
                     .filter( ac.createQueryFilter(ac.searchText) );
        };

        Organizacion.cuentasContables().then(function (cuentas) {
            ac.items  = cuentas;
            ac.$ready = true;
        });
    }

    
});



inject(DIRECTIVE, 'adjuntarDocumentos', [], function () {
    
    return {
        templateUrl : '/partials/base/adjuntar-documentos.html',
        scope : {
            nombreRecurso : '@',
            idRecurso : '@',
            idOrganizacion : '@'
        },
        link : link,
        controller : ['$scope', '$http', adjuntarDocumentosCtrl]
    };

    function link (scope, element, attrs) {

        scope.$watch(function () {
            if(!scope.nombreRecurso || !scope.idRecurso || !scope.idOrganizacion) return false;
            return scope.idOrganizacion + '/' + scope.nombreRecurso + 's/' + scope.idRecurso; 
        }, function (n) {
            console.log(n);
            if(!n) return;
            scope.inicializar(n);
        });

        var iframe = $('iframe', element);

    
        scope.iframe = iframe;

        iframe.bind('load', function () {
            var d;
            try{
                d = iframe[0].contentWindow.location;
                if(d.pathname.indexOf('/api/documento/') == 0) {
                    scope.recargarDocumentos();
                }
            } catch (e) {
                if(scope.ruta) scope.recargarDocumentos();
            }
        });
    }


    function adjuntarDocumentosCtrl ($scope, $http) {

        $scope.inicializar = function (d) {
            $scope.ruta = d;
            $scope.postUrl = d;
            $scope.formUrl = '/documentos/adjuntar?postUrl=' + $scope.postUrl;
            $scope.recargarDocumentos();
        };

        $scope.recargarDocumentos = function () {
            $http.get('/api/documentos/' + $scope.ruta).success(function (r) {
                $scope.documentos = r.data ? r.data.filter(function (d) { return d.type == 'file' }) : [];
                $scope.documentos.sort(function (d, b) {
                    return d.timestamp - b.timestamp;
                });
            });

            $scope.iframe.attr('src', $scope.formUrl);
        }

        $scope.eliminarDocumento = function (path) {
            $http.delete('/api/borrardocumento?path='.concat(path)).success(function () {
                $scope.recargarDocumentos();
            });
        }
    }
})
inject(DIRECTIVE, 'urlDataProvider', ['$parse'], function ($parse) {
    'use strict';

    var name = 'urlDataProvider';

    return {
        require : name,
        controller : ['$scope', '$http', '$attrs', '$timeout', urlDataProviderCtrl],
        link : link
    };

    function link (scope, element, attrs, ctrl) {
        ctrl.name = attrs.providerName || name;
        $parse(ctrl.name).assign(scope, ctrl);
    }



    function urlDataProviderCtrl ($scope, $http, $attrs, $timeout) {
        var ctrl     = this;
        var autoLoad = angular.isDefined($attrs.autoLoad);

        ctrl.load = (function () {
            var t;
            return function () {
                if(t) $timeout.cancel(t);
                t = $timeout(load,100);
            }
        })();

        $scope.$watch(watchUrl, function (url) {
            ctrl.url = url;
            if(autoLoad) ctrl.load();
        });

        if(autoLoad && angular.isDefined($attrs.providerParams)) {
            $scope.$watch($attrs.providerParams, function () {
                 ctrl.load();
            }, true);
        }


        function watchUrl () { return $attrs.providerUrl };

        function getParams () {
            return $parse($attrs.providerParams)($scope);
        };

        function load () {
            ctrl.busy = true;

            $http.get(ctrl.url, {params : getParams()}).success(function (d) {
                ctrl.data = d;
                ctrl.busy = false;
                $parse($attrs.onLoad)($scope);
            });
        };
    }




});
inject(DIRECTIVE, 'oorTercero', ['Tercero'], function (Tercero) {
    return {
        scope : {
            tercero : '=oorTercero'
        },
        templateUrl : '/partials/terceros/oor-tercero.html',
        link : link,
        controller : ['$scope','oorden',oorTerceroCtrl],
        controllerAs : 'tCtrl'
    };

    function link () {

    }

    function oorTerceroCtrl ($scope, oorden) {
        var tCtrl = this;

        oorden.inicializar().then(function () {
            $scope.$watch('tercero', function (tercero) {
                if(!tercero) return;
                initializetercero();
            });
        });

        function initializetercero () {
            tCtrl.tipoDeCambio = oorden.tipoDeCambio.base;
        }

        $scope.guardarTercero = function () {
            if($scope.t3Form.$valid == false) {
                toastr.error('Faltan campos');
                return;
            }

            var tercero = angular.extend({}, $scope.tercero, {
                direccion:$scope.direccion
            });

            Tercero.guardar(tercero, $scope.direccion).then(function (t3) {
                $scope.$emit('$tercero', t3);
            });
        };

        $scope.$on('guardarTercero', function () {
            $scope.guardarTercero();
        })
        

    }
})
inject(DIRECTIVE, 'oorDireccion', [], function () {
    


    return {
        scope:{
            direccion : '=oorDireccion'
        }, 
        templateUrl: '/partials/base/direccion.html'
    };





});
inject(DIRECTIVE, 'tablaConsulta', [], function () {
    return {
        scope : true,
        link : link,
        terminal : true
    }

    function link (scope, element, attrs) {

        var startWatch = scope.$watch(attrs.defCol, function (cols) {
            if(!cols) return;

            startWatch();

            var tabla = TablaConsulta({
                parametros : scope.$eval(attrs.parametros),
                modeloConsulta : attrs.modeloConsulta,
                include: 'operaciones.tipoDocumento,operaciones.tercero',
                columnas : ['checkbox','fecha','documento','tercero','referencia', 'total','estatus'],
                defCol : scope.$eval(attrs.defCol)
            });

            m.mount(element[0], tabla);

            scope.$watch(attrs.consulta, function (consulta) {
                tabla.getItems(consulta);
                tabla.ready();
            });

        });
       
    }
})

inject(DIRECTIVE,'selectTwo', ['oorden','$parse'], function (oorden, $parse) {
     var selectores = {
        cuentas : cuentasSelector,
        impuestos : impuestosSelector,
        retenciones : retencionesSelector
    };

    return {
        link : link,
        template : '<select></select>',
        terminal:true
    }

    function link (scope, el, attrs) {
        var selElement  = $('select', el)[0];
        var selector    = selectores[attrs.selectTwo](modelFn, onchange);
        var selected    = null;
        var selectedKey = null;
        var parser      = $parse(attrs.selectTwoSelected);
        var ngChange    = attrs.onChange ? $parse(attrs.onChange) : angular.noop;

        selector.config(selElement, false);

        scope.$watch(attrs.selectTwoSelected, function (val) {
            if(val) {
                selected    = val;
                selectedKey = selected[selector.key];

                if(selectedKey != selector.element().select2('val')) {
                    selector.element().select2('val', selectedKey);
                }
            } else {
                selector.element().select2('val', null);
            }
        });
        
        function modelFn () {
            if(arguments.length) {
                selectedKey = arguments[0];
            }
            return selectedKey ? selectedKey : null;
        }

        function onchange () {
            selected = selector.selected();
            parser.assign(scope, selected);
            ngChange(scope);
            console.log(scope.$$phase);
            scope.$$phase || scope.$apply();
        }
    }


    function cuentasSelector (modelFn, onchange) {
        var cuentas = oorden.cuentasContables.map(function (s) { return s; });
        return oor.cuentasSelect2({
            cuentas : cuentas,
            model : modelFn,
            key : 'cuenta_contable_id',
            onchange : onchange
        });
    }

    function impuestosSelector (modelFn, onchange) {
        return oor.impuestosSelect2({
            impuestos : oorden.impuestos,
            model : modelFn,
            key : 'impuesto_conjunto_id',
            onchange : onchange
        });
    }

    function retencionesSelector (modelFn, onchange) {
        return oor.retencionesSelect2({
            retenciones :oorden.retenciones,
            model : modelFn,
            key : 'retencion_conjunto_id',
            onchange : onchange
        });
    }

    function centrosDeCostoSelector (modelFn, oncahnge) {
        return oor.retencionesSelect2({
            
        })
    }
});


inject.mount(module);
})();
/* FIN MODULO BASE */ 

//import 'modules/oorden-usuarios/oorden-usuarios.src.js';

/* MODULO ORGANIZACIONEs */
(function () {
var module = angular.module('oordenOrganizaciones', ['oordenBase']);
var inject = createInjector();


inject(FILTER, 'numeroCifras', ['$filter'], function ($filter) {
    var memo = {};
    var numberFilter = $filter('number');

    return function (numero, cifras) {
        return numberFilter(Number(numero) / getMult(cifras), cifras);
    };

    function getMult (cifras) {
        if(!memo[cifras]) {
            memo[cifras] = Math.pow(10, cifras);
        }

        return memo[cifras];
    };
})

inject(FACTORY, 'Organizacion', ['$q', '$http', '$timeout'] , function ($q, $http, $timeout) {

    /**
     * Este modelo es provisional no va a functionar al final
     */
    var org = {
        centrosDeCosto    : getCentrosDeCosto(),
        cuentasContables  : getCuentasContables(),
        tiposDeCambio     : getTiposDeCambio(),
        impuestos : getImpuestos(),
        retenciones : getRetenciones(),
        sucursales : getSucursales(),
        vendedores : getVendeodres(),
         productos : getProductos(),
        configuracion     : config
    };

    org.terceros = getTerceros();

    return org;



    /**
     * Función falsa 
     */
    function config () {
        var defer = $q.defer();

        $timeout(function () {
            var config = getOrganizacionConfig();
            defer.resolve(config);
        },100);

        return defer.promise;
    };

    
    /**
     * Configuracion de la organizacion
     */
    function getOrganizacionConfig () {
        return {
            mesesContables : [
                {mes : '01', nombre : 'Enero',   mesContable : '01'},
                {mes : '02', nombre : 'Febrero', mesContable : '02'},
                {mes : '03', nombre : 'Marzo',   mesContable : '03'},
                {mes : '04', nombre : 'Abril',   mesContable : '04'},
                {mes : '05', nombre : 'Mayo',    mesContable : '05'},
                {mes : '06', nombre : 'Junio',   mesContable : '06'},

                {mes : '07', nombre : 'Julio',      mesContable : '07'},
                {mes : '08', nombre : 'Agosto',     mesContable : '08'},
                {mes : '09', nombre : 'Septiembre', mesContable : '09'},
                {mes : '10', nombre : 'Octubre',    mesContable : '10'},
                {mes : '11', nombre : 'Noviembre',  mesContable : '11'},
                {mes : '12', nombre : 'Diciembre',  mesContable : '12'},
                {mes : '13', nombre : 'Mes 13',  mesContable : '13'},
                {mes : '14', nombre : 'Mes 14',  mesContable : '14'},
            ]
        }
    };


    function getTiposDeCambio () {
        var defer;
        var tiposDeCambio = {
            tipos : [],
            base : null,
            porTipoDeCambio : function (tc) {
                var s = tiposDeCambio.tipos.filter(function (t) {
                    return t.tipo_de_cambio == tc;
                })[0];
                return s;
            }
        };

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();
                
                $http.get('/api/tiposdecambio').success(function (r) {
                    r.tipos.forEach(function (tipo) {
                        tiposDeCambio.tipos.push(tipo);
                        if(tipo.base == 1) tiposDeCambio.base = tipo;
                    });

                    defer.resolve(tiposDeCambio);
                });
            }

            return defer.promise;
        }
    }

    /** 
     * Centros De Costo
     */
    function getCentrosDeCosto () {
        var defer;

        var ccosto = {
            centros : [],
            elementos : [],
            centro : {},
            elemento : {}
        };

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/centrosdecosto').success(function (r) {

                    (r.centros || []).forEach(function (d) {
                        ccosto.centros.push(d);
                        ccosto.centro[d.centro_de_costo_id] = d;
                        d.elementos = [];
                    });


                    (r.elementos || []).forEach(function (d) {
                        ccosto.elemento[d.elemento_ccosto_id] = d;
                        if(ccosto.centro[d.centro_de_costo_id]){
                            ccosto.centro[d.centro_de_costo_id].elementos.push(d);
                        }
                    });

                    ready = true;

                    defer.resolve(ccosto);
                });

            }
            return defer.promise
        }
    };

    /**
     * Cuentas Contables
     */
    function getCuentasContables () {
        var defer;
        var cuentas = [];
         cuentas.cuenta = {};

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/json/cuentascontables').success(function (r) {

                    r.data.forEach(function (cuenta) {
                        cuentas.push(cuenta);
                        cuentas.cuenta[cuenta.cuenta_contable_id] = cuenta;
                    });

                    defer.resolve(cuentas);
                });

            }
            return defer.promise
        }
    }


    /** 
     * Terceros
     */

    function getTerceros () {
        var defer;
        var terceros = [];
        terceros.tercero = {};
        org.Terceros = terceros;

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/terceros').success(function (r) {
                    
                    r.data.forEach(function (tercero) {
                        terceros.push(tercero);
                        terceros.tercero[tercero.tercero_contable_id] = tercero;
                    });

                    defer.resolve(terceros);
                });
            }

            return defer.promise
        }
    }


    function getImpuestos () {
        var defer;
        var impuestos = [];

        impuestos.impuesto = {};
      

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/impuestos').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        impuestos.push(imp);
                        impuestos.impuesto[imp.impuesto_conjunto_id] = imp;
                    });

                    defer.resolve(impuestos);
                });
            }

            return defer.promise
        }
    }

    function getRetenciones () {
        var defer;
        var impuestos = [];

        impuestos.impuesto = {};
      

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/retenciones').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        impuestos.push(imp);
                        impuestos.impuesto[imp.retencion_conjunto_id] = imp;
                    });

                    defer.resolve(impuestos);
                });
            }

            return defer.promise
        }
    }

    function getSucursales () {
        var defer;
        var sucursales = [];

    
        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/sucursales').success(function (r) {
                    
                    r.data.forEach(function (imp) {
                        sucursales.push(imp);
                    });

                    defer.resolve(sucursales);
                });
            }

            return defer.promise
        }
    }


    function getVendeodres () {
        var defer;
        var vendedores = [];

    
        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/vendedores').success(function (r) {
                    
                    r.data.forEach(function (vnd) {
                        vendedores.push(vnd);
                    });

                    defer.resolve(vendedores);
                });
            }

            return defer.promise
        }
    }


    function getProductos () {
        var defer;
        var productos = [];

        return obtener;

        function obtener () {
            if(!defer) {
                defer = $q.defer();

                $http.get('/api/productos').success(function (r) {
                    
                    r.data.forEach(function (vnd) {
                        productos.push(vnd);
                    });

                    defer.resolve(productos);
                });
            }

            return defer.promise
        }
    }

});
inject(FACTORY, 'Poliza', ['$q', '$rootScope', '$http', 'Organizacion', 'uuid', 'fecha'] , function ($q, $rootScope, $http, Organizacion, uuid, fecha) {

    var campos = 'poliza_id.organizacion_id.concepto.total.total_m_base.poliza_id.ano_contable.mes_contable.referencia.numero.tasa_de_cambio.notas.manual';

  
    Poliza.takeRaw = function (raw) {
        return campos.split('.').reduce(function (result, k) { 
            result[k] = raw[k]; 
            return result; 
        }, {});
    }

    function obtenerCentroElemento (e) {
        var elemento = Poliza.centrosDeCosto.elemento[e];
        if(!elemento) return null;
        return Poliza.centrosDeCosto.centro[elemento.centro_de_costo_id];
    }

    Poliza.fromRaw = function (poliza, raw) {
        var centroDeCosto1, centroDeCosto2;
        //Extiendo los valores a heredar directamente
        angular.extend(poliza, Poliza.takeRaw(raw));
        
        //Tipo de Cambio
        poliza.tipoDeCambio = Poliza.tiposDeCambio.porTipoDeCambio(raw.tipo_de_cambio);
        poliza.seleccionarMoneda = poliza.tipoDeCambio.base != '1';
        
        //Obtengo los centros de costo a partir de los elementos
        var partida = raw.partidas[0];
        
        if(partida) {
            poliza.centroDeCosto1 = obtenerCentroElemento(partida.ccto_1_id);
            poliza.centroDeCosto2 = obtenerCentroElemento(partida.ccto_2_id);
        }
        
        //Obtengo las partidas
        poliza.partidas = [];

        raw.partidas.forEach(function (p) {
            poliza.partidas.push(p);
        });

        poliza.tipo = Poliza.tipos.tipo[raw.tipo];
        poliza.fecha = fecha.desdeSQL(raw.fecha);

        //Condición para a falta de estatus, poner el default, no debe ser asi
        poliza.estatus = Poliza.estatuses.estatus[raw.estatus] || Poliza.estatuses.estatus['P'] ;
    };


    Poliza.hayCopia = function () {
        return false;
    };

    Poliza.establecerCopia = function (poliza) {
        localStorage.setItem('Poliza-copia', poliza.poliza_id);
    };

    Poliza.obtener = function (id) {
        var defer = $q.defer();

        Poliza.preparar().then(function () {
            console.log(id);
        
            $http.get('/api/poliza/' + id).success(function (d) {
                
                if(!d.data.partidas) d.data.partidas = [];
                
                //Ordena las partidas
                d.data.partidas.sort(function (p, d) {
                    return Number(p.posicion_partida) - Number(d.posicion_partida);
                }).forEach(function (p) {
                    p.$integrable = true;
                })

                var poliza = new Poliza;

                Poliza.fromRaw(poliza, d.data);

                poliza.actualizarPartidas();
                defer.resolve(poliza);
            }).error(function (d) {
                defer.reject(d);
            })
        });

        return defer.promise;
    }

    /** 
     * Crea una tasa nueva 
     */
    Poliza.crear = function (datos) {
        var defer  = $q.defer();

        Poliza.preparar().then(function () {
            var poliza   = Poliza();
            var fecha    = new Date;
        

            angular.extend(poliza, {
                poliza_id : uuid(),
                partidas  : [],
                concepto  : '',
                fecha     : fecha,
                tipo      : Poliza.tipos.tipo['D'],
                estatus   : Poliza.estatuses.estatus['P'],
                $local    : true
            }, datos);


            //Consulta de los centros de costo para asignar
            if(Poliza.centrosDeCosto.centros) {
                var centros = Poliza.centrosDeCosto.centros.filter(function (c) {return c.activo == "1"});
                poliza.centroDeCosto1 = centros[0];
                poliza.centroDeCosto2 = centros[1];
            }

            //Consulta del tipo de cambio para asignar
            poliza.tipoDeCambio   = Poliza.tiposDeCambio.base;
            poliza.tasa_de_cambio = poliza.tipoDeCambio.tasa.tasa_de_cambio;

            defer.resolve(poliza);
        });

        return defer.promise;
    };


    var estatuses = [
        { estatus : 'P', nombre : 'En Preparacion', color : 'blue', nombre_corto: 'PRP'},
        { estatus : 'T', nombre : 'Por Autorizar', color : 'amber', nombre_corto: 'PDTE'},
        { estatus : 'A', nombre : 'Autorizada', color : 'green',  nombre_corto : 'AUT'},
        { estatus : 'X', nombre : 'Eliminada', color : 'red', nombre_corto : 'ELIM'},
    ];

    estatuses.estatus = estatuses.reduce(function (r,e) { r[e.estatus] = e; return r;}, {});

    var tipos = [
        { tipo : 'D', nombre : 'Diario'},
        { tipo : 'I', nombre : 'Ingreso'},
        { tipo : 'E', nombre : 'Egreso'},
    ];

    tipos.tipo = tipos.reduce(function (r,t) { r[t.tipo] = t; return r;}, {});


    var prepararDefer;

    function check () {
        if(Boolean(Poliza.tiposDeCambio) && Boolean(Poliza.centrosDeCosto) && Boolean(Poliza.cuentasContables)){
            prepararDefer.resolve(true);
        }
    };

    Poliza.preparar = function () {
        if(!prepararDefer) {
            prepararDefer = $q.defer(); 

            Organizacion.centrosDeCosto().then(function (cc) {
                Poliza.centrosDeCosto = cc;
                check();
            });

            Organizacion.tiposDeCambio().then(function (tc) {
                Poliza.tiposDeCambio = tc;
                check();
            });

            Organizacion.cuentasContables().then(function (cc) {
                Poliza.cuentasContables = cc;
                check();
            });

            Poliza.estatuses = estatuses;
            Poliza.tipos     = tipos;
        }

        return prepararDefer.promise;
    };


    function Poliza () {
        if(!(this instanceof Poliza)) return new Poliza;
        var poliza = this;
    };

    /**
     * Actualiza las partidas para su manejo
     */
    Poliza.prototype.actualizarPartidas = function () {
        var poliza = this;

        poliza.partidas.forEach(function (partida) {
            partida.moneda         = poliza.tipoDeCambio.codigo_moneda;
            partida.tasa_de_cambio = poliza.tasa_de_cambio;
            partida.importe_m_base = Number(partida.tasa_de_cambio) * partida.importe;
            
            if(partida.debe_o_haber == 1) {
                partida.debe  = partida.importe;
                partida.haber  = null;
            } else if(partida.debe_o_haber == -1) {
                partida.haber = partida.importe;
                partida.debe = null;
            }
        });

        poliza.calcularTotales();
    };


    Poliza.prototype.agregarPartida = function() {
        this.partidas.push({
            poliza_id : this.poliza_id, 
            poliza_partida_id : uuid(),
            cuenta_id : null,
            concepto : null,
            debe_o_haber : 1,
            importe : 0,
            tasa_de_cambio : this.tasa_de_cambio,
            moneda : this.moneda,
            operacion_item_id : null,
            importe_m_base : 0
        });
    };

    Poliza.prototype.eliminarPartida = function(partida) {
        var index = this.partidas.indexOf(partida);
        if(index === -1) return;

        this.partidas.splice(index,1);
        
        if(!this.partidasAEliminar) {
            this.partidasAEliminar = [];
        }

        this.partidasAEliminar.push(partida);
        this.calcularTotales();
    };

    /**
     *
     * Cálculo de totales en Debe, haber y moneda base
     */
    Poliza.prototype.calcularTotales = function () {
        var total = {'1': 0, '-1': 0};
            
        this.partidas.forEach(function (partida){
            total[String(partida.debe_o_haber)] += Number(partida.importe);
        });

        this.totalDebe  = total['1'];
        this.totalHaber = total['-1'];
        this.total = this.totalDebe;
        this.total_m_base = this.tasa_de_cambio * this.total;
    };

    Poliza.prototype.asignarTipoDeCambio = function (tipoDeCambio, tasaDeCambio) {
        this.tipoDeCambio = tipoDeCambio;
        this.tasa_de_cambio = tasaDeCambio;
        this.seleccionarMoneda = this.tipoDeCambio.base != '1';
        this.actualizarPartidas();
    };


    Poliza.prototype.asignarDebe = function(partida, debe) {
        partida.debe     = debe;
        partida.haber    = null;
        partida.importe  = debe;
        partida.debe_o_haber = 1;

        this.calcularTotales();
    };
    

    Poliza.prototype.asignarHaber = function(partida, haber) {
        partida.debe     = null;
        partida.haber    = haber;
        partida.importe  = haber;
        partida.debe_o_haber = -1;

        this.calcularTotales();
    };

    Poliza.prototype.elementoCosto = function (partida, num) {
        num = Number(num);
        if(num !== 1 && num !== 2) num = 1;
        return Poliza.centrosDeCosto.elemento[partida['ccto_'+ num +'_id']];
    };

    Poliza.prototype.cuentaContable = function (partida) {
        if(!partida.cuenta_id) return '';
        return Poliza.cuentasContables.cuenta[partida['cuenta_id']].nombre;
    };

    Poliza.prototype.cta = function (partida) {
        if(!partida.cuenta_id) return '';
        return Poliza.cuentasContables.cuenta[partida['cuenta_id']];
    };

    Poliza.prototype.finalizar = function () {
        this.estatus = Poliza.estatuses.estatus['T'];
        return this.guardar();
    };

    Poliza.prototype.aplicar = function () {
        this.estatus = Poliza.estatuses.estatus['A'];
        return this.guardar();
    };

    Poliza.prototype.eliminar = function () {
        return $http.post('/api/poliza/eliminar/' + this.poliza_id).success(function (R) {
            console.log(R);
        });
    };

    Poliza.prototype.cantidadCctos = function () {
        var num = 0;
        if(this.centroDeCosto1) num++;
        if(this.centroDeCosto2) num++;
        return num;
    }
    

    /**
     * Guarda la poliza
     * @return $q().promise
     */
    Poliza.prototype.guardar = function  () {
        if(this.$guardando) return;

        var self = this;
        self.$guardando = true;
        
        var defer             = $q.defer();
        var poliza            = angular.extend({}, Poliza.takeRaw(self));
        var partidas          = self.partidas;
        
        //Agregar partidas
        poliza.partidas = partidas.filter(function (p) { return p.$integrable });
        
        //Partidas a Eliminar
        if(self.partidasAEliminar) {
            poliza.eliminar = self.partidasAEliminar.map(function (p) { return p.poliza_partida_id });
        }
        
        //Tipo de Cambio y moneda
        poliza.tipo_de_cambio = self.tipoDeCambio.tipo_de_cambio;
        poliza.moneda         = self.tipoDeCambio.codigo_moneda;

        //Tipo
        poliza.tipo           = self.tipo.tipo;
        //Estatus
        poliza.estatus        = self.estatus.estatus;

        poliza.fecha          = fecha.toSQL(self.fecha);

        var url = (!self.$local) ? '/v1/api/polizas/editar/' + self.poliza_id : '/api/poliza/agregar';
        
        //Guardar
        $http.post(url, poliza).success(function (d) {
            $http.post('/api/poliza/estado_poliza/' + poliza.poliza_id + '/' + poliza.estatus).success(function () {
                Poliza.obtener(poliza.poliza_id).then(function (result) {
                    self.$guardando = false;
                    defer.resolve(result);
                });
            });
        })
        .error(function (d) {
            console.log(d);
            defer.reject(d);
        })

        return defer.promise;
    };

    return Poliza;
});
inject(FACTORY, 'Operacion', ['$q', '$rootScope', '$http', 'oorden', 'Organizacion', 'uuid', 'fecha'] , function ($q, $rootScope, $http, oorden, Organizacion, uuid, fecha) {


    Operacion.hayCopia = function () {
        return false;
    };

    // Obtener operacion
    Operacion.obtener = function (id) {
        var defer = $q.defer();
        $http.get('/api/operacion/' + id)
        .success(function (d) {
            defer.resolve({ operacion : d.data });
        })
        .error(function (d) {
            defer.reject(d);
        });

        return defer.promise;
    };

    // Obtener datos extra de la operación
    Operacion.obtenerDatosExtra = function (id) {
        var defer = $q.defer();

        $http.get('/api/operacionesextra/' + id).success(function (e) {
            defer.resolve(e);
        });

        return defer.promise;
    };
    

    Operacion.crear = function (datos) {
        var defer  = $q.defer();

        Operacion.preparar().then(function () {
            var operacion   = Operacion();
        
            angular.extend(operacion, {
                operacion_id : uuid(),
                items        : [],
                $new         : true
            }, datos);

            defer.resolve(operacion);
        });

        return defer.promise;
    };

    //Se agrega el item
    Operacion.prototype.agregarItem = function () {
        this.items.push({
            operacion_item_id : uuid()
        });
    };

    function Operacion () {
        if(!(this instanceof Operacion)) return new Operacion;
    };

    // Guardar Operacion
    Operacion.guardar = function (operacion) {
        var defer = $q.defer();

        $http.post('/api/operacion/agregar', operacion)
            .success(function (r) { defer.resolve(r); })
            .error(function (r) { defer.reject(r); });

        return defer.promise;
    };

    // Guardar Estatus de la operació 
    Operacion.estatus = function (operacion, estatus) {
        var defer = $q.defer();

        $http.post('/api/operacion/estado_operacion/' + operacion.operacion_id + '/' + estatus)
            .success(function (r) {
                defer.resolve(); 
            })
            .error(function (r) {
                defer.reject(r);
            })

        return defer.promise;
    };

    Operacion.preparar = (function () {
        var prepararDefer;

        var estatuses = [
            { estatus : 'P', nombre : 'En Preparacion', color : 'blue'},
            { estatus : 'T', nombre : 'Por Autorizar', color : 'amber'},
            { estatus : 'A', nombre : 'Autorizada', color : 'green'},
            { estatus : 'S', nombre : 'Saldada', color:'grey'},
            { estatus : 'X', nombre : 'Eliminada', color:'red'}
        ];

        estatuses.estatus = estatuses.reduce(function (r,e) {
            r[e.estatus] = e;
            return r;
        }, {});

        return function () {
            if(!prepararDefer) {
                prepararDefer = $q.defer(); 

                Operacion.estatuses = estatuses;

                oorden.inicializar().then(function () {
                    Operacion.centrosDeCosto   = oorden.centrosDeCosto;
                    Operacion.tiposDeCambio    = oorden.tiposDeCambio;
                    Operacion.cuentasContables = oorden.cuentasContables;
                    check();
                });

                
            }

            return prepararDefer.promise;
        }

        function check () {
            if(Boolean(Operacion.tiposDeCambio) && Boolean(Operacion.centrosDeCosto) && Boolean(Operacion.cuentasContables)){
                prepararDefer.resolve(true);
            }
        }

    })();

    return Operacion;
});
inject(FACTORY, 'Tercero', ['$q', '$http', 'oorden', 'uuid', '$timeout'] , function ($q, $http, oorden, uuid, $timeout) {

    function Tercero () {
        if(!(this instanceof Tercero)) return new Tercero;
    };

    Tercero.obtener = function (id) {
        var defer = $q.defer();

        $http.get('/api/terceros/' + id).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    };

    
    Tercero.crear = function (datos) {
        var defer  = $q.defer();

        Tercero.preparar().then(function () {
            var tercero   = Tercero();

            angular.extend(tercero, {
                tercero_id : uuid(),
                $nuevo     : true
            }, datos);

            defer.resolve(tercero);
        });

        return defer.promise;
    };


    Tercero.preparar = (function () {
        var prepararDefer;

        return function () {
            if(!prepararDefer) {
                prepararDefer = $q.defer(); 
                oorden.inicializar().then(function () {
                   prepararDefer.resolve(true);
                });
            }
            return prepararDefer.promise;
        }
    })();


    Tercero.guardar = function (tercero, dir) {
        var defer = $q.defer();
        var direccion;

        if(dir) {
            direccion = angular.extend({}, dir, {
                direccion_id: uuid(), 
                pertenece_a_id : tercero.tercero_id,
                tipo_id : 3
            });
        }

        if(direccion) {
            guardarDireccion(direccion).then(function () {
                var t = angular.extend({}, tercero, {
                    ultima_direccion_fiscal : direccion.direccion_id
                });
                guardarTercero(t).then(resolve);
            });
        } else {
            guardarTercero(tercero).then(resolve);
        }

        function resolve (rTercero) { defer.resolve(rTercero); }
    
        return defer.promise;
    };


    Tercero.obtenerDirecciones = function (id) {
        var defer = $q.defer();

        $http.get('/api/direcciones/' +id).success(function (r) {
            defer.resolve(r.data);
        });

        return defer.promise;
    };


    function guardarTercero (tercero) {
        var defer = $q.defer();

        $http.post('/api/terceros/agregar', tercero).success(function (t) {
            defer.resolve(t.data);
        });

        return defer.promise;
    }


    function guardarDireccion (direccion) {
        var defer = $q.defer();

        $http.post('/api/direcciones/agregar', direccion).success(function (d) {
            defer.resolve(d.data);
        });

        return defer.promise;
    }

    return Tercero;
});

//import './services/models/Cliente.js';

inject(FACTORY, 'oorDialogoCuenta', ['$mdDialog'], function ($mdDialog) {










  
});
inject(FACTORY, 'oorDialogoImpuesto', ['$mdDialog'], function ($mdDialog) {

});
inject(FACTORY, 'oorDialogoProducto', ['$mdDialog'], function ($mdDialog) {
   
    function oorDialogoProducto ($event) {
        var parentEl = angular.element(document.body);

        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/partials/productos/dialogo-producto.html',
            controller : ['$scope', '$mdDialog', oorDialogoProductoCtrl]
        });
    };

    function oorDialogoProductoCtrl ($scope, $mdDialog) {

        $scope.closeDialog  = function () {
            $mdDialog.hide({pro:'mesa'});
        }

    }


    return oorDialogoProducto;
}); 
inject(FACTORY, 'oorDialogoRetencion', ['$mdDialog'], function ($mdDialog) {

});
inject(FACTORY, 'oorDialogoItemFactura', ['$mdDialog'], function ($mdDialog) {

    function oorDialogoItemFactura ($event, item, factura) {

        var parentEl = angular.element(document.body);

        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/partials/operaciones/dialogo-item-factura.html',
            locals : {
                iItem : item,
                factura : factura
            },
            controller : ['$scope', '$mdDialog', 'iItem', 'factura', oorDialogoItemFacturaCtrl]
        });

    }

    var fields = 'producto|concepto|cantidad|unidad|precioUnitario|descuento|importe|impuesto|retencion|cuenta|elementosDeCosto'.split('|');

    function oorDialogoItemFacturaCtrl ($scope, mdDialog, iItem, factura) {
        var item =  $scope.item =  {};
        $scope.factura = factura;

        angular.forEach(fields, function (key) {
            item[key] = iItem[key];
        });

        if(!item.elementosDeCosto) item.elementosDeCosto = [null, null];

        $scope.closeDialog  = function () {
            $mdDialog.hide(null);
        };

        $scope.aplicarCambios = function () {
            $mdDialog.hide($scope.item);
        };

        $scope.asignarProducto = function () {
            var item = $scope.item;
            var prod = item.producto;
            if(!prod) return;

            item.precioUnitario = prod.precio_unitario_venta;
            item.unidad = prod.unidad;
            item.concepto = prod.nombre;
        }

        var decimales = 4;
        var fDec = 1000;

        $scope.calcularImporte = function () {
            var item = $scope.item;
            var descuento = (100 - Number(item.descuento  || 0))/100;
            var importe   = Number(item.cantidad) * Number(item.precioUnitario) * descuento;

            importe = Math.round(fDec*importe)/fDec;
            $scope.item.importe = importe;
        };
    };


    return oorDialogoItemFactura;

});
inject(FACTORY, 'oorDialogoTercero', [ '$mdDialog', 'uuid', 'oorden'], function ($mdDialog,uuid,oorden) {
    
    return showDialog;

    function showDialog($event) {
        var parentEl = angular.element(document.body);
        
        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            template:
               '<md-dialog aria-label="List dialog">' +
               '  <md-content>'+
               '    <div oor-tercero="tercero" style="min-width:800px"></div>' +
               '  </md-content>' +
               '  <div class="md-actions">' +
               '    <md-button ng-click="closeDialog()">' +
               '      Cancelar' +
               '    </md-button>' +
               '    <md-button class="md-primary" ng-click="guardarTercero(t3Form)">' +
               '      Guardar' +
               '    </md-button>' +
               '  </div>' +
               '</md-dialog>',
            locals: {
            
            },
            controller: ['$scope','Tercero', AuxiliarDialogCtrl]
        });
    }

    function AuxiliarDialogCtrl (scope, Tercero) {
        scope.closeDialog = function() {
            $mdDialog.hide(null);
        };

        scope.tercero   = { 
            tercero_id : uuid(),
            es_cliente : showDialog.TERCERO_CLIENTE ? true : false,
            es_proveedor : showDialog.TERCERO_PROVEEDOR ? true : false
        };

        scope.direccion = {
            codigo_pais : oorden.organizacion.codigo_pais
        };

        scope.guardarTercero = function (t3Form) {
          console.log(t3Form);
          
            var tercero = angular.extend({}, scope.tercero, {direccion:scope.direccion});
            Tercero.guardar(tercero, scope.direccion).then(function (t3) {
                $mdDialog.hide(t3);
            });
        };
    }

});




inject(CONTROLLER,'crearOrganizacionCtrl', ['$scope'], function ($scope) {
   
   console.log($scope);

});
inject(CONTROLLER, 'TiposDeCambioCtrl', ['$scope', '$http', '$timeout', '$window'], function ($scope, $http, $timeout, $window) {
    
    var baseUrl = '/tipos-de-cambio/';
    var Monedas = {};

    $scope.Monedas = Monedas;

    //Obtener las monedas
    $http.get(baseUrl.concat('monedas')).success(function (monedas) {
        monedas.forEach(function (moneda) {
            Monedas[moneda.codigo_moneda] = moneda;
        });
    });

    //Obtener los tipos de cambio
    $scope.getTiposDeCambio = function () {
        $http.get(baseUrl).success(function (r) {
            $scope.tiposDeCambio = r.data.filter(function (t) { return t.base != '1' });
            $scope.tcBase = r.data.filter(function (t) { return t.base == '1' })[0];
            $scope.grupos = agruparTiposDeCambio($scope.tiposDeCambio);
        });
    };

    /*
    $scope.agregar = function (tipoDeCambio) {
        if(tipoDeCambio.$saving) {
            return;
        }
        
        tipoDeCambio.$saving = true;
    
        $http.post(baseUrl.concat('/create'), tipoDeCambio)
        .success(function (r) {

            angular.extend(tipoDeCambio, {$saving:false, $recent:true}, r);
            inactiveRecent(tipoDeCambio);

            $scope.tiposDeCambio.push(tipoDeCambio);
            $scope.grupos = agruparTiposDeCambio($scope.tiposDeCambio);
            $scope.tipoDeCambio = null;
        })
        .error(function (r) {
            alert(r[0].message);
        });
    };

    $scope.editarTC = function (tc) 
    {
        tc.sufijo = tc.tipo_de_cambio.split(tc.codigo_moneda).join('');
        tc.$edit_tc = true;
    }

    $scope.editarNombre = function (tc) 
    {
        tc.nombre_old = tc.nombre;
        tc.$edit_nombre = true;
    }

    $scope.cancelarEdicionNombre = function (tc) 
    {
        tc.nombre = tc.nombre_old;
        tc.$edit_nombre = false;
    }

    $scope.guardarTC = function (tc) 
    {
        tc.$busy = true;

        $http.put(baseUrl.concat('/edit/', tc.tipo_de_cambio_id), { sufijo : tc.sufijo })
        .success(function (r) {
            tc.tipo_de_cambio = r.tipo_de_cambio;
            tc.$busy = false;
            tc.$edit_tc = false;
        });
    }

    $scope.guardarNombre = function (tc)
    {
        tc.$busy = true;

        $http.put(baseUrl.concat('/edit/', tc.tipo_de_cambio_id), { nombre : tc.nombre })
        .success(function (r) {
            tc.nombre = r.nombre;
            tc.$busy = false;
            tc.$edit_nombre = false;
        });
    }

    $scope.asignarRevaluaciones = function (tc)
    {
        return $http.post(baseUrl.concat('revaluaciones/' + tc.tipo_de_cambio_id )).success($scope.getTiposDeCambio);
    }

    $scope.get = function (id) {
        return $http.get(baseUrl.concat('get/', id));
    }
    */

    $scope.agregarTasa = function (tc, tasa) {
        var tcId = tc.tipo_de_cambio_id;
        tc.$busy = true;

        $http.post(baseUrl.concat('tasa/',tcId), {tasa : tasa}).success(function () {
            /*
            $scope.get(tcId).success(function (rTC) {
                angular.extend(tc, rTC);
                tc.$busy = false;
            });
            */
            $window.location.reload();
        });
    };

    /*
    
    $scope.eliminarTc = function (tc) {
        $http.delete(baseUrl.concat('delete/', tc.tipo_de_cambio_id)).success(function (d) {
            var c = confirm("¿Desea Eliminar?");
            if(c) {
                $http.delete(baseUrl.concat('delete/', tc.tipo_de_cambio_id, '/', d.token))
                    .success(function () {
                        $scope.getTiposDeCambio()
                    })
            }
        });
    }
    */

   
    $scope.getTiposDeCambio();

    /*
    function inactiveRecent(tipoDeCambio) {
        $timeout(function () {
            tipoDeCambio.$recent = false;
        },1000);
    }
    */

    function agruparTiposDeCambio (tiposDeCambio) {
        var grupos = {};

        tiposDeCambio.forEach(function (tipo) {
            grupos[tipo.codigo_moneda] = true;
        });

        function byCodigoMoneda (cm) {
            return function (tc) {
                return tc.codigo_moneda == cm; 
            };
        };

        return Object.keys(grupos).map(function (grupo) {
            return {
                codigo_moneda : grupo, 
                tiposDeCambio : tiposDeCambio.filter(byCodigoMoneda(grupo))
            };
        });
    }

});
inject(CONTROLLER, 'CategoriasCtrl', ['$scope', '$http'], function ($scope, $http) {

    var baseUrl = '/categorias/';
    var ctrl = this;

    $scope.guardar = guardar;
    $scope.abrir = abrirCategoria;
    $scope.eliminar = eliminar;

    obtenerCategorias();

    function eliminar (categoria) {
        var uri = baseUrl.concat('delete/', categoria.categoria_id);

        $http.delete(uri)
        .success(function (r) {
            $http.delete(uri.concat('/', r.token)).success(function (d) {
                ctrl.categoria = null;
                obtenerCategorias();
            });
        });
    }

    function abrirCategoria (categoria) {
        ctrl.categoria = angular.copy(categoria);
    }

    function obtenerCategorias () {
        $http.get(baseUrl).success(function (r) {
            $scope.categorias = r;
        });
    }

    function guardarCategoria (categoria) {
        var url = categoria.categoria_id ? 'edit/' + categoria.categoria_id : 'create';
        var method = categoria.categoria_id ? 'put' : 'post';
        return $http[method](baseUrl.concat(url), categoria);
    }

    function guardar (categoria) {
        var request;

        if(categoria.$saving) {
            return;
        } 

        categoria.$saving = true;
        request = guardarCategoria(categoria);
      
        request
        .success(function (rCat) {
            categoria.$saving = false;
            ctrl.categoria = rCat;
        })
        .success(obtenerCategorias);
        

        return request;
    }

});
inject(CONTROLLER, 'ProductosCtrl', ['$scope','$http'], function ($scope, $http) {

    
        
});

inject(CONTROLLER, 'OorPolizasIndexCtrl', ['$scope', 'Organizacion', '$http', 'Poliza', 'fecha'], function ($scope, Organizacion, $http, Poliza, fecha) {

    this.selectedTabIndex = 0;

    $scope.contable   = {};
    $scope.control    = { active_all : false };
    $scope.Poliza     = Poliza;
    $scope.formulario = { tipo : '__todas__'};


    this.firstLoaded = false;


    Poliza.preparar().then(function () {
        $scope.estatuses = [{ estatus : 'Todas', nombre : 'Todas' }];
        Organizacion.configuracion().then(function (d) {
            $scope.mesesContables = d.mesesContables;
            
            Poliza.estatuses.forEach(function (d) {
                $scope.estatuses.push(d);
                $scope.$ready = true;
            });


            $scope.tipos =  Poliza.tipos.map(function (c){ return c});
            $scope.tipos.unshift({tipo:'__todas__', nombre : 'Todas'})
        });   
    });
    
    function to10(n) {
        var d = String(n);
        return (d.length == 1) ? '0'.concat(d) : d; 
    }


    $scope.aplicarBusquedaFormulario = function () {
        var formulario  = angular.copy($scope.formulario);
        var params = {};

        if(formulario.fechaInicial) {
            params['fecha>'] = fecha.toSQL(formulario.fechaInicial);
        }

        if(formulario.tipo != '__todas__') {
            params['tipo'] = formulario.tipo;
        }

        if(formulario.fechaFinal) {
            params['fecha<'] = fecha.toSQL(formulario.fechaFinal);
        }

        if(formulario.concepto) {
            params['concepto*'] = formulario.concepto;
        }

        if(formulario.referencia) {
            params['referencia*'] = formulario.referencia;
        }

        if(formulario.numero) {
            params['numero'] = formulario.numero;
        }
        $scope.buscar(params);
        
    };
    
    $scope.cambiarEstatus = function (estatus) {
        if(!$scope.busquedaFormulario) {
            $scope.aplicarBusquedaContable();
        } else {
            $scope.formulario.estatus = estatus.estatus;
            $scope.aplicarBusquedaFormulario();
        }
    };

    $scope.buscar = function (busqueda) {
        if($scope.buscando) return;
        $scope.buscando = true;
        $scope.polizas = null;
        $scope.control.active_all = false;

        angular.extend(busqueda, {modelo:'polizas'});

        if(busqueda.estatus == 'Todas') {
            delete(busqueda.estatus);
            busqueda['estatus!'] = 'X';
        }

        return $http.get('/apiv2', {params : busqueda})
            .success(function (d) {
                $scope.buscando = false;
                $scope.index.firstLoaded = true;
                $scope.polizas  = [];
                if(d.data) {
                    d.data.forEach(function (p) { $scope.polizas.push(p); });
                }
            });
    };


    $scope.aplicarBusquedaContable = function () {
        $scope.buscar({
            mes_contable : $scope.contable.mes, 
            ano_contable : $scope.contable.ano,
            estatus      : getSelectedStatus().estatus
        });

    }

    $scope.activarFormulario = function () {
        $scope.busquedaFormulario =true;
        $scope.formulario = { estatus : getSelectedStatus().estatus };
    } 

    $scope.desactivarFormulario = function () {
        $scope.busquedaFormulario = false;
        $scope.formulario = null;
        $scope.aplicarBusquedaContable();
    }
    

    function getSelectedStatus () {
        return $scope.estatuses[$scope.index.selectedTabIndex];
    };


    var removeReadyWatcher = $scope.$watch('$ready', function (ready) {
        if(!ready) return;

        removeReadyWatcher();

        //Se asignan las fechas contables a "hoy"
        var hoy = new Date();
        $scope.contable.ano = hoy.getFullYear();
        $scope.contable.mes = hoy.getMonth() + 1;

        //Se reacciona a los cambios en el cambio de estatus
        $scope.$watch('index.selectedTabIndex', function (d, o) {
            if(d === o) return;
            $scope.cambiarEstatus(getSelectedStatus());
        });

        $scope.$watch('control.active_all', function (activeAll) {
            if(! $scope.polizas ) {
                return;
            }
            if(activeAll === true || activeAll === false) {
                $scope.polizas.forEach(function (p) {
                    p.$seleccionada = activeAll;
                });
            }
        });

        $scope.aplicarBusquedaContable();
    });

});
inject(CONTROLLER, 'OorOperacionesIndexCtrl', ['$scope', '$attrs', 'Organizacion', 'Operacion', 'fecha'], function ($scope, $attrs, Organizacion, Operacion, fecha) {
   
    var index = this;
    index.selectedTabIndex = 0;
    index.firstSearch = false;

    $scope.Operacion  = Operacion; 

    Operacion.preparar().then(function () {
        Organizacion.configuracion().then(function (d) {
            $scope.mesesContables = d.mesesContables;
            $scope.estatuses = [{estatus : 'Todas', nombre : 'Todas'}];

            Operacion.estatuses.forEach(function (d) {
                $scope.estatuses.push(d);
            });

            index.$ready = true;
        }); 
    });

    $scope.seccion = $attrs.seccion;


    function getSelectedStatus () {
        return $scope.estatuses[index.selectedTabIndex];
    };

    
    var removeReadyWatcher = $scope.$watch(isReady, function (ready) {
        if(!ready) return;

        removeReadyWatcher();
        
        //Se asignan las fechas contables a "hoy"
        var hoy = new Date();

        index.estatus = getSelectedStatus();

        index.busqueda = {
            fecha_inicial : fecha.inicialMes(hoy),
            fecha_final   : fecha.finalMes(hoy),
            estatus       : index.estatus.estatus,
            seccion       : $attrs.seccion,
            modelo        : 'operaciones',
            include       : 'operaciones.tercero:embed,operaciones.tipoDocumento:embed'
        };

        //Se reacciona a los cambios en el cambio de estatus
        $scope.$watch('index.selectedTabIndex', function (d, o) {
            if(d === o) return;
            index.estatus = getSelectedStatus();
            index.busqueda.estatus = index.estatus.estatus;
        });


        $scope.$watch('index.busqueda', function (b) {
            $scope.parametros = angular.copy(b);
        
            if(angular.isUndefined(b.estatus) || b.estatus == 'Todas') {
                $scope.parametros['estatus!'] = 'X';
                delete($scope.parametros.estatus);
            }

            if($scope.parametros.fecha_inicial){
                $scope.parametros['fecha>'] = fecha.toSQL($scope.parametros.fecha_inicial);
                delete($scope.parametros.fecha_inicial);
            }

            if($scope.parametros.fecha_final){
                $scope.parametros['fecha<'] = fecha.toSQL($scope.parametros.fecha_final);
                delete($scope.parametros.fecha_final);
            }
            
            index.provider.load();
        }, true);

        $scope.$watch('index.provider.data.data', function (d) {
            index.operaciones = d;
            index.firstSearch = true;
        });
    });

    function isReady () {
        return index.provider && index.$ready;
    }

    

});

inject.mount(module);
})();
/* FIN MODULO ORGANIZACIONEs */

/* MODULO Operaciones */
(function () {

var module = angular.module('oordenOperaciones', ['oordenBase']);
var inject = createInjector();

inject(CONTROLLER, 'OperacionesDeCuentaCtrl', ['$scope','Operacion'], function ($scope, Operacion) {
    
    Operacion.preparar().then(function () {
        var eMap = Operacion.estatuses.estatus;

        $scope.definiciones = {
            
            checkbox : {
                caption : function () { 
                    var items = this.items();

                    return m('input[type="checkbox"]');
                },
                value : function (item) {
                    return m('input[type="checkbox"]', { 
                        checked  : item.checked(),
                        onchange : m.withAttr('checked', item.checked)
                    });
                }
            },

            tercero : {
                caption : 'Destinatario',
                value : function (item) {
                    return item.tercero ? item.tercero.nombre : '';
                }
            },

            documento : {
                caption : 'Documento',
                value : function (item) {
                    return m('a', {
                            href:'/operaciones/' + item.tipo_operacion.toLowerCase() + '/'.concat(item.operacion_id)
                        }, 
                        m('strong',item.tipoDocumento.nombre.concat(' ', item.serie, ' #', item.numero ? item.numero : '<auto>'))
                    );
                }
            },

            total : {
                value : function (item) {
                    return m('div.text-right', Number(item.total).toFixed(2));
                }
            },

            estatus : {
                caption : 'Estatus',
                value : function (item) {
                    var estatus = eMap[item.estatus || 'P'];
                    return m('span.pull-right.text-'.concat(estatus.color), estatus.nombre);
                }
            }

        };

    });


});
/**
 * POLIZAS
 */
//import './controllers/OorPolizaPantallaCtrl.js';
inject(DIRECTIVE, 'oorPoliza', ['oorden', 'uuid', 'fecha', 'oorDialogoTercero'], function (oorden, uuid, fecha, oorDialogoTercero) {
    'use strict';

    return  {
        link : link,
        terminal :true
    };

    function link (scope, element, attrs, ctrls) {
        
        var polizaComp = m.component(operaciones.PolizaComponent, {
            polizaId : attrs.polizaId
        });


        console.log( operaciones.PolizaComponent );

        /**
         * Inicializamos oorden y luego se carga el componente en el DOM
         */
        oorden.inicializar().then(function () {
            m.mount(element[0], polizaComp);
        });
    };

});
//import './directives/OorPolizaPartidaCtrl.js';
//import './directives/OorPolizaPartida.js';

/**
* OPERACIONES
*/
inject(CONTROLLER, 'OorOperacionPantallaCtrl', ['$scope', '$attrs', '$window', 'Operacion', 'oorden', 'fecha', '$http', '$q', 'Tercero', 'uSetting', 'uuid'], function ($scope, $attrs, $window, Operacion, oorden, fecha, $http, $q, Tercero, uSetting, uuid) {
    
    var pathname = $window.location.pathname.replace($attrs.path, '');
    var match    = /^(crear|[\d|\-|a|b|c|d|e|f]{36})(\?|\/)?$/.exec(pathname);
    var id       = false;
    var _oorinit = false;


    console.log(pathname, match);

    
    angular.extend($scope, {guardar : guardar});

    //Inicializa los servicios de oorden
    oorden.inicializar().then(function () {
        _oorinit = true;     
    });

    //Carga los impuestos
    $http.get('/api/impuestos').success(function (r) {
        $scope.impuestos = r.data;
    });

    //Carga las retenciones
    $http.get('/api/retenciones').success(function (r) {
        $scope.retenciones = r.data;
    });


    $scope.$watch(function () {
        return _oorinit && Boolean($scope.retenciones) && Boolean($scope.impuestos);
    }, function (isReady) {
        if(!isReady) return;
        if(!match) {
            $scope.notFound = {status:{message:'No encontramos la operacion'}};
            return;
        }
        
        id = match[0];

        if(id === 'crear') {
            //if(Operacion.hayCopia()) {
            //    Operacion.obtenerCopia().then(setOperacion);
            //} else {
                Operacion.crear().then(function (op) {
                    nuevaOperacion(op).then(setOperacion); 
                });
            //}
        } else if(id) {
    
            Operacion.obtener(id).then(function (rOp){
                var op = rOp.operacion;

                if(op.tipo_operacion != angular.uppercase($attrs.tipoOperacion)) {
                    //No es el tipo de operacion correcto, se redirige
                    location.pathname = '/operaciones/' + angular.lowercase(op.tipo_operacion) + '/' + id;
                    return;
                }

                op.$editar = false;

                $http.get('/api/operacionesextra/' + id).success(function (e) {
                    $scope.productos = e.data.productos;
                    $scope.poliza    = e.data.poliza;


                    if(!op.operacion_anterior_id){
                        //Si no hay operacion anterior ya está listo
                        setOperacion(op);
                        return;
                    }

                    Operacion.obtener(op.operacion_anterior_id).then(function (rOpAnterior) {
                        //Asignar la op Anterior
                        op.operacionAnterior = rOpAnterior.operacion;
                        setOperacion(op);
                    });
                });
            }, function (a) {
        

                $scope.notFound = a;

            });
        }
    });


    function nuevaOperacion (operacion) {
        var defer = $q.defer();

        uSetting.get(['usuario', oorden.usuario.usuario_id], null, 'factura').then(function (r) {
            var fSettings = r.factura || {};

            $http.get('/api/tipodocumentos').success(function (t) {

                

                angular.extend(operacion, {
                    precios_con_impuestos : Boolean(Number(fSettings.precios_con_impuestos)),
                    mostrar_descuento :     Boolean(Number(fSettings.mostrar_descuento)),
                    mostrar_cctos :         Boolean(Number(fSettings.mostrar_cctos)),
                    mostrar_cuentas :       Boolean(Number(fSettings.mostrar_cuentas)),
                    mostrar_retenciones :   Boolean(Number(fSettings.mostrar_retenciones)),
                    mostrar_impuestos :     Boolean(Number(fSettings.mostrar_impuestos)),

                    tipo_de_cambio : oorden.tipoDeCambio.base.tipo_de_cambio,
                    tasa_de_cambio : 1,
                    sucursal_id : oorden.sucursal.sucursal_id,
                    $new : true,
                    $editar : true,
                    estatus : 'P',

                    items : [
                        {
                            operacion_item_id : uuid(),
                            posicion : 0
                        },
                        {
                            operacion_item_id : uuid(),
                            posicion : 1
                        }
                    ]
                });
                
                operacion.tipo_operacion = angular.uppercase($attrs.tipoOperacion);
            
                var T = t.data.filter(function (t) {
                    return operacion.tipo_operacion == t.tipo_operacion;
                })[0];
         
                operacion.tipo_documento =  T ? T.tipo_documento_id :null;
                operacion.serie = T? T.serie : null;
                defer.resolve(operacion);
            }); 
        });

        return defer.promise;
    }

    function guardar (capturarNueva) {
        var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }

        Operacion.guardar(factura).then(function (r) {
            uSetting.set(['usuario', oorden.usuario.usuario_id], null, {
                factura : {
                    mostrar_impuestos : Number(factura.mostrar_impuestos),
                    mostrar_retenciones : Number(factura.mostrar_retenciones),
                    mostrar_descuento : Number(factura.mostrar_descuento),
                    mostrar_cctos : Number(factura.mostrar_cctos),
                    mostrar_cuentas : Number(factura.mostrar_cuentas)
                }
            })
            .then(function () {
                afterSave(capturarNueva, factura.$new, factura)(r);
            });
        }, saveError);

    };


    function __afterSave (r, factura) {
           
    }

    function saveError (errResponse) {
        toastr.error(errResponse.status.message); 
    }

    $scope.aplicar = function (redirigir) {
         var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }

        Operacion.guardar(factura).then(function () {
            Operacion.estatus($scope.operacion, 'A').then(afterSave(redirigir, true, factura), saveError);
        }, saveError);
        
    };

    $scope.porAutorizar = function (redirigir) {
         var factura   = $scope.opCtrl.obtenerFactura();

        if(!factura.items.length) {
            alert('Introduzca un ítem');
            return;
        }
        
        Operacion.guardar(factura).then(function () {
            return Operacion.estatus($scope.operacion, 'T').then(afterSave(redirigir, true, factura), saveError);
        },saveError);
    }

    /** 
     * Despues de guardar...
     */
    function afterSave (redirigir, esNuevo, factura) {
        return function (p) {
            $scope.lsMessage({text : 'Operacion Guardada'});
            
            if(redirigir){
                location.pathname = '/operaciones/' + factura.tipo_operacion.toLowerCase() + '/crear';
                return;
            }
            if(esNuevo || true) {
                location.pathname = '/operaciones/' + angular.lowercase(factura.seccion);
                return;
            }
        }
    }

    function setOperacion (p) {
        $scope.operacion = p; 
        if($scope.puede.editar()) {
            $scope.editar();
        }
    }


    /**
     * Permisos
     */
    $scope.puede = {
        eliminar : function () {
            return $scope.operacion.estatus != 'S';
        },
        editar : function () {
            return ($scope.operacion.estatus != 'X' && $scope.operacion.estatus != 'A' && $scope.operacion.estatus != 'S'); 
        },
        aplicar : function () {
            return $scope.operacion.estatus === 'T' ||  $scope.operacion.estatus === 'P'; 
        },
        porAutorizar : function () {
            return $scope.operacion.estatus === 'P';
        }
    };


    /**
     * Acciones
     */
   
    $scope.cancelar = function () {
        location.pathname = '/operaciones/' + angular.lowercase($scope.operacion.tipo_operacion) + '/' + $scope.operacion.operacion_id
    };

    $scope.eliminar = function () {
        if(! confirm('¿Desea Eliminar la Operación?')) return;

        $http.post('/api/operacion/eliminar/' + $scope.operacion.operacion_id).success(function (d) {
            $scope.lsMessage({type:'warning', text : 'Oepración Eliminada'});
            location.pathname = '/operaciones/'+ $attrs.tipoOperacion[0];
        });
    };

    $scope.editar = function () {
        $scope.operacion.$editar = true;
    };
});

inject(CONTROLLER, 'OorOperacionItemCtrl', ['$scope', 'oorDialogoProducto', '$timeout', 'oorden'], function ($scope, oorDialogoProducto, $timeout, oorden) {
    
    var ctrl = this;
    var mult = Math.pow(10, $scope.cifras());
    var opCtrl = $scope.opCtrl;

    //Valores de cálculo iniciales
    ctrl.importeAsInt         = 0;
    ctrl.montoDescuentoAsInt  = 0;
    ctrl.sinImpuestosAsInt    = 0;

    ctrl.impuestosAgrupados   = [];
    ctrl.retencionesAgrupadas = [];
    ctrl.elementosDeCosto     = [];

    //Metodos del scope
    angular.extend($scope, {
        asignarProducto : asignarProducto,
        actualizarItem : actualizarItem,
        asignarImpuesto : asignarImpuesto
    });

    //Métodos del controlador
    angular.extend(ctrl, {
        getItem : getItem,
        actualizar : actualizar,
        editar : editar()
    });


    function asignarProducto (producto) {
        ctrl.producto = producto;
        if(!producto) return;

        $scope.item.precio_unitario = opCtrl.seccion() == 'V' ? producto.precio_unitario_venta : producto.precio_unitario_compra;
        $scope.item.unidad          = producto.unidad;
        $scope.item.concepto        = producto.nombre;
        
        //debugger;
        //La cuenta viene en producto, revisar tipoDoc
        var ctaId   = opCtrl.ccProducto(ctrl.producto);
        var cuenta  = $scope.oorden.cuentaContable(ctaId);

        ctrl.cuenta =  cuenta;

        // El impuesto, según los criteros, revisar tipoDoc
        var impuestoId;
        var impuesto  = null;

        if(opCtrl.tercero && opCtrl.tercero.impuesto_venta_id) {
            impuestoId = opCtrl.tercero.impuesto_venta_id;
        } else if(ctrl.producto && ctrl.producto.impuesto_venta) {
            impuestoId = ctrl.producto.impuesto_venta;
        } else if (ctrl.cuenta && ctrl.cuenta.impuesto_conjunto_id) {
            impuestoId = ctrl.cuenta.impuesto_conjunto_id;
        }

        if(impuestoId) {
            impuesto = $scope.impuestos.filter(function (i) {
                return i.impuesto_conjunto_id == impuestoId;
            })[0];
        }

        //Retención
        if($scope.$index != 0) {
            asignarRetencion($scope.opCtrl.items[$scope.$index - 1].retencion);
        }

        asignarImpuesto(impuesto || null);
        actualizarItem();
    }


    function asignarImpuesto (impuesto) {
        ctrl.impuesto = impuesto;
        actualizarItem();
    }

    function asignarRetencion (retencion) {
        ctrl.retencion = retencion;
        if(!retencion) return;
        $scope.item.retencion_conjunto_id = retencion.retencion_conjunto_id;
        actualizarItem();
    }

    var actualizarTimeout;

    function actualizarItem () {
        actualizarTimeout && $timeout.cancel(actualizarTimeout);

        actualizarTimeout = $timeout(function () {
            actualizar();
            $scope.actualizarOperacion(); //ActualizarFactura
            actualizarTimeout = null;
        }, 200);
    }; 


    function getItem () {
        var item = angular.copy($scope.item);

        item.operacion_anterior_item_id = ctrl.itemAnterior ? ctrl.itemAnterior.operacion_item_id : null;

        item.producto_id            = ctrl.producto ? ctrl.producto.producto_id : null;
        item.producto_variante_id   = ctrl.variante ? ctrl.variante.producto_variante_id : null; 
        item.producto_nombre        = item.concepto;

        item.importe = String(ctrl.importeAsInt / mult);

        item.cuenta_id             = ctrl.cuenta ? ctrl.cuenta.cuenta_contable_id : null;
        item.impuesto_conjunto_id  = ctrl.impuesto? ctrl.impuesto.impuesto_conjunto_id : null;
        item.retencion_conjunto_id = ctrl.retencion ? ctrl.retencion.retencion_conjunto_id : null;

        item.ccto_1_id = ctrl.elementosDeCosto[0] ? ctrl.elementosDeCosto[0].elemento_ccosto_id : null;
        item.ccto_2_id = ctrl.elementosDeCosto[1] ? ctrl.elementosDeCosto[1].elemento_ccosto_id : null;

    
        return item;
    }

    function actualizar () {
        var item = $scope.item;
        var cantidad            = Number(item.cantidad ) || 0;
        var precioUnitario      = Number(item.precio_unitario) || 0;
        var descuento           = (Number(item.descuento) || 0) / 100;
        var impuestosIncluidos  = Boolean($scope.operacion.precios_con_impuestos);
        var importe;

        importe = Math.round(cantidad * precioUnitario * mult);

        ctrl.montoDescuentoAsInt  = Math.round(importe * descuento);
        ctrl.importeAsInt         = importe - ctrl.montoDescuentoAsInt;
        ctrl.impuestosAgrupados   = [];
        ctrl.retencionesAgrupadas = [];

        if(ctrl.impuesto) {

            ctrl.sinImpuestosAsInt = sinImpuestos(ctrl.importeAsInt, impuestosIncluidos, ctrl.impuesto);

            ctrl.impuesto.componentes.forEach(function (componente) {
                var nombre    = componente.componente + ' ' + String(Number(componente.tasa)) + '%';
                var tasa      = Number(componente.tasa) / 100;
                var montoInt  = Math.round(ctrl.sinImpuestosAsInt * tasa);

                ctrl.impuestosAgrupados.push({
                    nombre     : nombre,
                    tasa       : tasa,
                    monto      : montoInt / mult,
                    montoAsInt : montoInt
                });
            });

            if(!ctrl.retencion) return;

            ctrl.retencion.componentes.forEach(function (componente) {
                var nombre = componente.componente + ' ' + String(Number(componente.tasa)) + '%';
                var tasa   = Number(componente.tasa) / 100;
                var montoInt = Math.round(ctrl.sinImpuestosAsInt * tasa);

                ctrl.retencionesAgrupadas.push({
                    nombre : nombre,
                    tasa : tasa,
                    monto : montoInt / mult,
                    montoAsInt : montoInt
                });
                
            });

        } else {
            ctrl.sinImpuestosAsInt = ctrl.importeAsInt;
        }
    };


    function sinImpuestos(importe, impuestosIncluidos, impuesto) {
        var tasa = impuesto.tasa_total || impuesto.tasa;
        
        if(!impuestosIncluidos) return importe;
        return Math.round(importe / (1 + (tasa / 100)));
    };


    $scope.$watch('item', function (item) {
        if(!item) return;

        ctrl.elementosDeCosto = [];

        //Item Anterior
        if(item.itemAnterior) {
            ctrl.itemAnterior = item.itemAnterior;
            delete(item.itemAnterior);
        } else if(item.operacion_anterior_item_id) {
            ctrl.itemAnterior = opCtrl.operacionAnterior.items.filter(function (i) {
                return i.operacion_item_id == item.operacion_anterior_item_id;
            })[0];
        }

        //Impuestos 
        ctrl.impuesto = $scope.impuestos.filter(function (i) {
            return i.impuesto_conjunto_id == item.impuesto_conjunto_id;
        })[0];

        ctrl.retencion = $scope.retenciones.filter(function (i) {
            return i.retencion_conjunto_id == item.retencion_conjunto_id;
        })[0];

        //Cuentas Contables
        ctrl.cuenta = oorden.cuentaContable(item.cuenta_id);


        //Productos 
        if(item.producto) {
            ctrl.producto = item.producto;
            delete(item.producto);
        } else if(item.producto_id) {
            ctrl.producto = $scope.productos.filter(function (i) {
                return i.producto_id == item.producto_id;
            })[0];
        }



        //Variantes
        if(item.producto_variante_id) {
            ctrl.producto = $scope.variantes.filter(function (i) {
                return i.producto_variante_id == item.producto_variante_id;
            })[0];
        }

        //Centros de Costo
        if(item.ccto_1_id) {
            ctrl.elementosDeCosto[0] = oorden.elementoDeCosto(item.ccto_1_id);
        }
        if(item.ccto_2_id) {
            ctrl.elementosDeCosto[1] = oorden.elementoDeCosto(item.ccto_2_id);
        }

        item.concepto = item.producto_nombre;

        actualizar();

        ctrl.$ready = true;
        $scope.registrarItemCtrl(ctrl);
    });


    $scope.$on('$destroy', function () {
        $scope.eliminarItemCtrl(ctrl);
        $scope.actualizarOperacion(true);
    });


    $scope.$watch(validate);

    function validate () {
        ctrl.$integrable = Boolean($scope.item.concepto) || Boolean(ctrl.importeAsInt);
        ctrl.err = {};

        if(ctrl.importeAsInt){
            if(!ctrl.cuenta) {
                ctrl.err.noCuenta = true;
            }

            if(!ctrl.impuesto) {
                ctrl.err.noImpuesto = true;
            }
        } 

     

        if(ctrl.$integrable) {
            if(Object.keys(ctrl.err).length) {
                opCtrl.addErrItem(ctrl);
            } else {
                opCtrl.remErrItem(ctrl);
            }
        }
    }


    function editar () {
        return  {
            producto : function () {
                if(!$scope.operacion.$editar) return false;
                //hay operacion anterior no se puede editar el producto
                if(Boolean(opCtrl.operacionAnterior)) return false;
                return true;
            },

            concepto : function () {
                return Boolean($scope.operacion.$editar);
            },

            cantidad : function () {
                return Boolean($scope.operacion.$editar);
            },

            unidad : function () {
                return Boolean($scope.operacion.$editar);
            },

            precioUnitario : function () {
                if(!$scope.operacion.$editar) return false;
                //hay operacion anterior no se puede editar el precio
                if(Boolean(opCtrl.operacionAnterior)) return false;
                return true;
            },

            descuento : function () {
                if(!$scope.operacion.$editar) return false;
                //hay operacion anterior no se puede editar el precio
                if(Boolean(opCtrl.operacionAnterior)) return false;
                return true;
            },

            impuesto : function () {
                if(!$scope.operacion.$editar) return false;
                //hay operacion anterior no se puede editar el precio
                if(Boolean(opCtrl.operacionAnterior)) return false;
                return true;
            },

            retencion : function () {
                if(!$scope.operacion.$editar) return false;
                //hay operacion anterior no se puede editar el precio
                if(Boolean(opCtrl.operacionAnterior)) return false;
                return true;
            },

            cuenta : function () {
                return Boolean($scope.operacion.$editar);
            },

            ccto : function () {
                return Boolean($scope.operacion.$editar);
            }
        }
    };
    
   

});
//DIRECTIVAS
inject(DIRECTIVE, 'oorOperacion', ['oorden','Operacion','uuid','Organizacion','Tercero','uSetting', 'fecha'], function (oorden, Operacion, uuid, Organizacion, Tercero, uSetting, fecha) {
    'use strict';
    
    return  {
        scope : true,
        restrict : 'A',
        controller : ['$scope', OorOperacionCtrl],
        controllerAs : 'opCtrl',
    };

    function OorOperacionCtrl ($scope) {
        var ctrl           = this;
        var diasDeCredito  = 0;
        var totalColumnas  = 10;
        var totalNColumnas = 6;
        var itemCtrls      = [];
        var errItems       = [];


        ctrl.errors = {};

        ctrl.eliminar = [];

        

        


        //Asigno el controlador al scope padre
        //para que pueda acceder a los métodos
        $scope.$parent.opCtrl = ctrl;

        $scope.Operacion = Operacion;

        Operacion.preparar().then(function () {
            $scope.Operacion = Operacion;
        });

        $scope.cifras = function () {
            return oorden.organizacion.cifras() 
        };

        var mult = 10;

        angular.extend($scope, {
            asignarTercero : asignarTercero,
            asignarCliente : asignarCliente,
            asignarVencimiento : asignarVencimiento,
            asignarFecha : asignarFecha,
            asignarSucursal : asignarSucursal,
            asignarTipoDeCambio : asignarTipoDeCambio,

            actualizarColumnas : actualizarColumnas,
            registrarItemCtrl : registrarItemCtrl,
            eliminarItemCtrl : eliminarItemCtrl,
            actualizarOperacion : actualizarOperacion,
            asignarTipoDeDocumento : asignarTipoDeDocumento
        });


        angular.extend(ctrl, {
            obtenerFactura : obtenerFactura,
            mount : mount,
            addErrItem : addErrItem,
            remErrItem : remErrItem, 
            hasErrors :  hasErrors,
            eliminarItem : eliminarItem,
            eliminarTodosLosItems : eliminarTodosLosItems
        });

        function addErrItem (itemCtrl) {
            if(errItems.indexOf(itemCtrl) == -1) {
                errItems.push(itemCtrl);
            }
        }

        function remErrItem (itemCtrl) {
            var index = errItems.indexOf(itemCtrl)
            if(index+1) {
                errItems.splice(index,1);
            }
        }


        function validar () {
            ctrl.errors = {};

            if(!$scope.operacion.tipo_documento) {
                ctrl.errors.noTipoDoc = true;
            }
        }

        function hasErrors () {
            return errItems.length + Object.keys(ctrl.errors).length;
        }


        //Espera a que la operacion esté lista para inicializar
        function mount () {
            $scope.$watch('operacion', function (operacion) {
                if(!operacion) return;
                initializeCtrl();
            });
        }

        var camposBooleanos = ['precios_con_impuestos','mostrar_descuento','mostrar_cctos','mostrar_cuentas','mostrar_retenciones','mostrar_impuestos'];
        /**
         * Inicializa el controlador cargando todos los campos que corresponden
         * Ajustando columnas
         * Verificar que los items esten listos
         */
        function initializeCtrl () {
            var testReadyWatcher;

            
            angular.extend($scope, {
                impuestos : $scope.$parent.impuestos,
                retenciones : $scope.$parent.retenciones,
                productos : $scope.$parent.productos,
                Operacion : Operacion,
                oorden : oorden
            });

            mult = Math.pow(10, $scope.cifras());

            ctrl.centrosDeCosto = [];
            //Se ordenan los items
            ctrl.items = $scope.operacion.items.sort(function(a,b) {
                return Number(a.posicion_item) - Number(b.posicion_item);
            });

            delete($scope.operacion.items);

            //Campos booleanos necesitan transformatse
            camposBooleanos.forEach(function (c) {
                $scope.operacion[c] = Boolean(Number($scope.operacion[c]));
            });

            //Cargar la sucursal
            if($scope.operacion.sucursal_id) {
                Organizacion.sucursales().then(function (sucursales) {
                    ctrl.sucursal = sucursales.filter(function (s) { return s.sucursal_id == $scope.operacion.sucursal_id; })[0];
                });
            } else {
                ctrl.sucursal = null;
            }

            ctrl.operacionAnterior = $scope.operacion.operacionAnterior;

            //Ajustar la fecha 
            if($scope.operacion.fecha){
                ctrl.fecha = fecha.fromSQL($scope.operacion.fecha);
            }else if($scope.operacion.$new) {
                ctrl.fecha = new Date;
                ctrl.vence = new Date;
            }

            if($scope.operacion.fecha_vencimiento) {
                ctrl.vence = fecha.fromSQL($scope.operacion.fecha_vencimiento);
            }

            //Cargar el Tercero
            if($scope.operacion.tercero_id) {
                Tercero.obtener($scope.operacion.tercero_id).then(function (tercero) {
                    ctrl.tercero = tercero[0];
                });
            }

            //Cargar la dirección
            
            if($scope.operacion.tercero_direccion_id) {
                Tercero.obtenerDirecciones($scope.operacion.tercero_id).then(function (direcciones) {

                    direcciones.forEach(function (d) {
                        if(d.direccion_id == $scope.operacion.tercero_direccion_id) {
                            ctrl.direccionFiscal = d;
                        }

                        if(d.direccion_id == $scope.operacion.direccion_envio) {
                            ctrl.direccionEnvio = d;
                        }
                    });

                    ctrl.seleccionarDireccionFiscal = ( $scope.operacion.direccion_envio == $scope.operacion.tercero_direccion_id);
                });
            }
            

            //Cargar el tipo de cambio
            if($scope.operacion.tipo_de_cambio) {
                ctrl.tipoDeCambio = oorden.tipoDeCambio[$scope.operacion.tipo_de_cambio];
            }


            if($scope.operacion.$new) {
                //Cuando la factura es nueva, los centros de costo son los activos
                oorden.centrosDeCostoActivos.forEach(function (cc) {
                    cc && ctrl.centrosDeCosto.push(cc);
                });
            } else if(ctrl.items[0]){
                //Cuando la factura ya existe lee los centros de costo de las partidas
                if(ctrl.items[0].ccto_1_id) {
                    ctrl.centrosDeCosto.push(oorden.centroDeCosto(ctrl.items[0].ccto_1_id));
                }
                if(ctrl.items[0].ccto_2_id) {
                    ctrl.centrosDeCosto.push(oorden.centroDeCosto(ctrl.items[0].ccto_2_id));
                }
            }

            
            if(ctrl.items.length == 0){
                //Si no hay items, inmediatamente está listo
                ctrl.$ready = true;
            } else {
                //Si hay items debe checar que todos se reporten listos
                testReadyWatcher = $scope.$watch(allItemCtrlsAreReady, function (isReady) {
                    if(!isReady) return;
                    testReadyWatcher();
                    actualizarOperacion(true);
                });
            }

            //Las columnas de la tabla
            actualizarColumnas();
            $scope.$watch(validar);
        }

        /**
         * checa si todos items están listos
         */
        function allItemCtrlsAreReady () {
            if(!itemCtrls.length) return false;
            return itemCtrls.reduce(function (o,i) {
                return o && Boolean(i.$ready);
            }, true);
        }

        /**
         * Devuelve un objeto de factura listo para el api
         */
        function obtenerFactura () {
            var factura = angular.copy($scope.operacion);

            delete(factura.operacionAnterior);

            if(factura.$new) {
                factura.estatus = 'P';
            }

            factura.numero = (factura.numero == '<auto>') ? null : factura.numero;

            factura.operacion_anterior_id = ctrl.operacionAnterior ? ctrl.operacionAnterior.operacion_id : null;

            factura.sucursal_id         = ctrl.sucursal ? ctrl.sucursal.sucursal_id : null;
            factura.fecha               = fecha.toSQL(ctrl.fecha);
            factura.fecha_vencimiento   = fecha.toSQL(ctrl.vence);
            factura.tercero_id          = ctrl.tercero ? ctrl.tercero.tercero_id : null;

            factura.tipo_de_cambio      = ctrl.tipoDeCambio ? ctrl.tipoDeCambio.tipo_de_cambio : null;
            factura.moneda              = ctrl.tipoDeCambio ? ctrl.tipoDeCambio.codigo_moneda: null;

            factura.tercero_direccion_id= ctrl.direccionFiscal ? ctrl.direccionFiscal.direccion_id : null
            factura.direccion_envio     = ctrl.direccionEnvio ? ctrl.direccionEnvio.direccion_id : null;
            factura.vendedor_id         = ctrl.vendedor ? ctrl.vendedor.usuario_id : null;

            factura.subtotal            = ctrl.subtotalAsInt / mult;
            factura.total               = ctrl.totalAsInt / mult;
            factura.saldo = factura.total;
            
            factura.descuentos          = ctrl.descuentoAsInt / mult;
            factura.impuestos           = ctrl.totalImpuestosAsInt / mult;
            factura.retenciones         = ctrl.totalRetencionesAsInt / mult;

            factura.tipo_operacion      = ctrl.tipoDeOperacion();
            factura.seccion             = ctrl.seccion();

            factura.eliminar            = ctrl.eliminar;



            factura.items = itemCtrls.filter(esIntegrable).map(function (itemCtrl) {
                return itemCtrl.getItem();
            });

            return factura;
        }

        function esIntegrable(itemCtrl) {
             return itemCtrl.$integrable === true;
        }

        /** 
         * para que los items de los controladores se registren
         */
        function registrarItemCtrl (itemCtrl) {
            itemCtrls.push(itemCtrl);
        }

        /**
         * para que los items de  los controladores se des-registren
         */
        function eliminarItemCtrl (itemCtrl) {
            var idx = itemCtrls.indexOf(itemCtrl);
            itemCtrls.splice(idx, 1);
        }


        function eliminarItem (item) {
            var idx = ctrl.items.indexOf(item);

            if(idx > -1) {
                ctrl.items.splice(idx,1);
                ctrl.eliminar.push(item.operacion_item_id);
            }
        }


        function eliminarTodosLosItems () {
            ctrl.items.forEach(function (item) {
                eliminarItem(item);
            });
        }


        function obtenerTasa () {
            var factura = $scope.operacion;
            factura.tasa_de_cambio = null;
            if(!ctrl.tipoDeCambio) return;

            oorden.obtenerTasa(ctrl.tipoDeCambio).then(function (tasa) {
                factura.tasa_de_cambio = tasa.tasa_de_cambio;
            });
        }

        function asignarTipoDeDocumento (tipoDoc) {
            $scope.operacion.serie = null;
            $scope.opCtrl.tipoDoc = tipoDoc;

           

            if(!tipoDoc) return;

            $scope.operacion.serie = tipoDoc.serie;
            $scope.operacion.numero =  $scope.operacion.numero ?  $scope.operacion.numero : null;
        }   

        function asignarTipoDeCambio () {
            obtenerTasa();
        }

        function asignarSucursal (sucursal) {
            ctrl.sucursal = sucursal;
            $scope.operacion.sucursal_id = sucursal.sucursal_id;
        }

        function asignarTercero (tercero) {
            if(ctrl.seccion() == 'V') return asignarCliente(tercero);
            if(ctrl.seccion() == 'C') return asignarProveedor(tercero);
        }

        function asignarCliente (cliente) {
            ctrl.tercero = cliente;

            if(cliente == null) {
                $scope.operacion.tercero_id = null;
                return;
            }

            $scope.operacion.tercero_id = cliente.tercero_id;

            //Asigno los dias de vencimiento
            diasDeCredito = Number(cliente.dias_credito_venta) || 0;
            asignarVencimiento();

        
            if(cliente.clave_moneda) {
                var d = oorden.tipoDeCambio[cliente.clave_moneda];

                if(d) {
                    ctrl.tipoDeCambio = d;
                } else {
                    ctrl.tipoDeCambio = oorden.tipoDeCambio.base;
                }
            }

            ctrl.items.forEach(function (item) {
                item.descuento = cliente.descuento;
            });
            
            
            Tercero.obtenerDirecciones(cliente.tercero_id).then(function (d) {
                ctrl.direcciones = d;

                ctrl.direcciones.forEach(function (dir) {
                    if(cliente.ultima_direccion_fiscal == dir.direccion_id) {
                        ctrl.direccionFiscal = dir;
                    }
                });
            });
        }


        function asignarProveedor (proveedor) {
            ctrl.tercero = proveedor;

            if(proveedor == null) {
                $scope.operacion.tercero_id = null;
                return;
            }

            $scope.operacion.tercero_id = proveedor.tercero_id;

            //Asigno los dias de vencimiento
            diasDeCredito = Number(proveedor.dias_credito_compra) || 0;
            asignarVencimiento();

        
            if(proveedor.clave_moneda) {
                var d = oorden.tipoDeCambio[proveedor.clave_moneda];

                if(d) {
                    ctrl.tipoDeCambio = d;
                } else {
                    ctrl.tipoDeCambio = oorden.tipoDeCambio.base;
                }
            }

            ctrl.items.forEach(function (item) {
                item.descuento = proveedor.descuento;
            });
            
          
            
            Tercero.obtenerDirecciones(proveedor.tercero_id).then(function (d) {
                ctrl.direcciones = d;

                ctrl.direcciones.forEach(function (dir) {
                    if(proveedor.ultima_direccion_fiscal == dir.direccion_id) {
                        ctrl.direccionFiscal = dir;
                    }
                });

    
            });
        }

        function asignarFecha (date) {
            ctrl.fecha = date;
            ctrl.fecha_sql = $scope.operacion.fecha = fecha.toSQL(date);
            asignarVencimiento();
        }

        function asignarVencimiento () {
            ctrl.vence = new Date(ctrl.fecha);
            ctrl.vence.setDate(ctrl.fecha.getDate() + Number(diasDeCredito));
            ctrl.vence_sql = $scope.operacion.vence = fecha.toSQL(ctrl.vence);
        }

        function actualizarColumnas () {
            var cctoLength = ctrl.centrosDeCosto.length;
            var factura = $scope.operacion;
            
            factura.columnas = totalColumnas - 4;

            factura.columnas += Number(factura.mostrar_cctos);
            factura.columnas += Number(factura.mostrar_descuento);
            factura.columnas += Number(factura.mostrar_cuentas);
            factura.columnas += Number(factura.mostrar_retenciones || factura.mostrar_impuestos);


            console.log( factura.columnas );
    

            factura.mostrar_contable = Boolean(factura.mostrar_cuenta) || Boolean(factura.mostrar_impuestos) || Boolean(factura.mostrar_retenciones);

            factura.nColumnas = totalNColumnas - 2;
            factura.nColumnas += Number(factura.mostrar_contable);
            factura.nColumnas += Number(factura.mostrar_cctos);

        
        };

        $scope.agregarItemDirecto = function ($event) {
            ctrl.items.push({ 
                operacion_item_id : uuid() ,
                descuento : ctrl.cliente ? Number(ctrl.cliente.descuento)  : 0
            });
        };

        function actualizarOperacion (actualizarItems) {
            var subtotal = 0;
            var descuentos = 0;
            var impuestosNoIncluidos = Number(!Boolean($scope.operacion.precios_con_impuestos));
            var sumaImportes = 0;

            itemCtrls.forEach(function (iCtrl) {
                actualizarItems && iCtrl.actualizar();

                subtotal    += iCtrl.sinImpuestosAsInt;
                descuentos  += iCtrl.montoDescuentoAsInt;
                sumaImportes+= iCtrl.importeAsInt;
            });


            actualizarImpuestos();
            actualizarRetenciones();

            ctrl.subtotalAsInt = subtotal;
            ctrl.sumaImportesAsInt = sumaImportes;
           
            if(impuestosNoIncluidos) {
                ctrl.totalAsInt = subtotal + ctrl.totalImpuestosAsInt - ctrl.totalRetencionesAsInt;
            } else {
                ctrl.totalAsInt = sumaImportes - ctrl.totalRetencionesAsInt;
            }

            ctrl.descuentoAsInt = descuentos;

     
        };

        

        function actualizarImpuestos () {
            var impuestos = {};
            var grupoImpuestos = [];
            var totalImpuestos = 0;

            itemCtrls.forEach(function (item) {
                item.impuestosAgrupados.forEach(function (grupo) {
                    if(!impuestos[grupo.nombre]) {
                        impuestos[grupo.nombre] = 0;
                    }
                    impuestos[grupo.nombre] += grupo.montoAsInt;
                    totalImpuestos          += grupo.montoAsInt;
                });
            });

            angular.forEach(impuestos, function (montoAsInt, grupo) {
                grupoImpuestos.push({
                    grupo : grupo, 
                    montoAsInt : montoAsInt,
                    monto : montoAsInt / mult
                });
            });

            ctrl.grupoImpuestos      = grupoImpuestos;
            ctrl.totalImpuestosAsInt = totalImpuestos;
        };

        function actualizarRetenciones () {
            var retenciones = {};
            var grupoRetenciones = [];
            var totalRetenciones = 0;

            itemCtrls.forEach(function (item) {
                item.retencionesAgrupadas.forEach(function (grupo) {
                    if(!retenciones[grupo.nombre]) {
                        retenciones[grupo.nombre] = 0;
                    }
                    retenciones[grupo.nombre] += grupo.montoAsInt;
                    totalRetenciones          += grupo.montoAsInt;
                });
            });

            angular.forEach(retenciones, function (montoAsInt, grupo) {
                grupoRetenciones.push({
                    grupo : grupo, 
                    montoAsInt : montoAsInt,
                    monto : montoAsInt / mult
                });
            });

            ctrl.grupoRetenciones      = grupoRetenciones;
            ctrl.totalRetencionesAsInt = totalRetenciones;
        }


        $scope.tcModificable = function () {
            if(!Operacion.tiposDeCambio) return;
            if(ctrl.operacionAnterior) return false;
            return Operacion.tiposDeCambio.length > 1;
        };

        $scope.monedaModificable = function () {
            return false;
        };

        $scope.tasaModificable = function () {
            if(!$scope.operacion) return;
            if(!ctrl.tipoDeCambio) return false;

            if(Number(ctrl.tipoDeCambio.base) == 1) return false;
            return true;
        };
    };
});
inject(DIRECTIVE, 'oorFactura', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-factura.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorFacturaCtrl],
        require : ['^oorOperacion', 'oorFactura'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1], {VTA : true});
        ctrls[0].mount();
        
    }

    function OorFacturaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });

        function tipoDeOperacion () {
            return 'VTA'; //FACTURA "VENTAS"
        }

        function seccion () {
            return 'V'; //VENTAS
        }

        function ccProducto(producto) {
            return producto.cuenta_venta;
        }
    };

});
inject(DIRECTIVE, 'oorNdcVenta', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-ndc-venta.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorNdcVenta'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{VNC : true}) ;
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'VNC';
        }

        function seccion () {
            return 'V'; 
        }

        function ccProducto (producto) {
            return producto ? producto.cuenta_nc_venta : '';
        }
    };

});

inject(CONTROLLER, 'OorNdcFacturaCtrl', ['$scope', '$mdDialog', '$http', 'uuid', 'Operacion'], function ($scope, $mdDialog, $http, uuid, Operacion) {

    /** 
     * Modificar la factura de origen de
     */
    $scope.modificarFactura = function ($event) {

        if($scope.opCtrl.items.length){
            var confirmar = confirm('Se eliminarán todas las líneas del documento y se crearán nuevas con la nueva información.');
            if(!confirmar) return;
        }

        showDialog($event).then(function (op) {
            if(!op) return;

            $scope.opCtrl.operacionAnterior = op;

            Operacion.obtenerDatosExtra(op.operacion_id).then(function (extra) {
                $scope.opCtrl.eliminarTodosLosItems();
                $scope.opCtrl.items = obtenerItemsDevolucion(op, extra.data);

                $scope.operacion.referencia = op.referencia;
                $scope.operacion.tipo_de_cambio = op.tipo_de_cambio;
                $scope.operacion.moneda = op.moneda;
                $scope.operacion.tasa_de_cambio = op.tasa_de_cambio;
                //Se actualiza la operacion y luego se aplica al scope para actualizar la vista
                setTimeout(function () {
                    $scope.actualizarOperacion();
                    $scope.$apply();
                }, 500);
            });
         
        });
    };



    function obtenerItemsDevolucion(d, datosExtra) {
        return d.items.filter(incluir).map(function (i) {
            var producto = datosExtra.productos.filter(function (p) { 
                return p.producto_id == i.producto_id 
            })[0];

            return {
                operacion_item_id : uuid(),
                
                //referencias al origen
                operacion_anterior_item_id : i.operacion_item_id,
                itemAnterior : i,
                
                //producto
                producto :  producto,

                //Cantidad sería el máximo posible

                cantidad : Number(i.cantidad) - Number(i.cantidad_devuelta || 0),

                //se cambia la cta por la de nota de crédito
                cuenta_id : $scope.opCtrl.ccProducto(producto),

                //los valores que se copian
                producto_nombre   : i.producto_nombre,
                unidad : i.unidad,
                impuesto_conjunto_id : i.impuesto_conjunto_id,
                retencion_conjunto_id : i.retencion_conjunto_id,
                ccto_1_id : i.ccto_1_id,
                ccto_2_id : i.ccto_2_id,
                precio_unitario : i.precio_unitario,
                descuento : i.descuento   
            };
        });
    }

    function incluir (i) { return i.$incluir; }
    

    function showDialog($event) {
        var parentEl = angular.element(document.body);
        
        return $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl : '/partials/operaciones/dialogo-ndc-factura.html',
            locals: {
                tercero : $scope.opCtrl.tercero,
                operacionAnterior : $scope.opCtrl.operacionAnterior,
                tipo_operacion : $scope.opCtrl.VNC ? 'VTA' : 'COM',
                itemsAnteriores : $scope.opCtrl.items.map(function (i) { return i.operacion_anterior_item_id })
            },
            controller: ['$scope', 'tercero', 'Operacion', 'itemsAnteriores' ,'operacionAnterior' , 'tipo_operacion',AuxiliarDialogCtrl]
        });
    }


    function AuxiliarDialogCtrl ($scope, tercero, Operacion, itemsAnteriores, operacionAnterior, tipo_operacion) {
        $scope.tercero = tercero;

        if(operacionAnterior) {
            //si hay operacion anterior se muestra
            $scope.facturaSeleccionada = operacionAnterior;
        } else {
            //se cargan las facturas
            cargarFacturas();
        }

        function cargarFacturas () {
            var params = {
                tercero_id : tercero.tercero_id,
                tipo_operacion : tipo_operacion
                //estatus : 'A,S'
            };

            $http.get('/api/operaciones/', {params : params}).success(function (d) {
                $scope.facturas = d.data.filter(function (op) {
                    return ['A','S'].indexOf( angular.uppercase(op.estatus) ) > -1;
                });
            });
        }

        $scope.abrirFactura = function (factura) {
            $scope.facturaSeleccionada = factura;
        };

        $scope.cerrarFactura = function () {
            $scope.facturaSeleccionada = null;
            if(!$scope.facturas) {
                cargarFacturas();
            }
        };

        $scope.finalizar = function (operacion) {
            if(!operacion) return;
            $mdDialog.hide(operacion);
        };

        $scope.cancelar = function () {
            $mdDialog.hide(null);
        };

        /**
         * Al seleccionar una factura, se carga la operacion
         * y se llena una tabla con los items que ya estan en referenciados
         */
        $scope.$watch('facturaSeleccionada', function (facturaSeleccionada) {
            $scope.operacion = null;

            if(!facturaSeleccionada) return;

            $scope.cargandoOperacion = true;

            //Obtiene la operación seleccionada
            Operacion.obtener(facturaSeleccionada.operacion_id).then(function (data) {
                $scope.operacion = data.operacion;
                $scope.cargandoOperacion = false;

                //por cada item se busca si ya está en la operación para asinarle $incluir
                $scope.operacion.items.forEach(function (it) {
                    it.$incluir = Boolean( (itemsAnteriores.indexOf(it.operacion_item_id)) + 1);
                });
            });
            
        });
    }
});
inject(DIRECTIVE, 'oorCompra', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-compra.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorCompra'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{COM : true});
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'COM';
        }

        function seccion () {
            return 'C'; 
        }

        function ccProducto(producto) {
            return producto.cuenta_compra;
        }
    };

});
inject(DIRECTIVE, 'oorNdcCompra', [], function () {
    'use strict';
    
    return  {
        templateUrl : '/partials/operaciones/oor-ndc-compra.html',
        controller : ['$scope','$attrs','Operacion','oorden', OorNdcVentaCtrl],
        require : ['^oorOperacion', 'oorNdcCompra'],
        link : link
    };


    function link (scope, element, attrs, ctrls) {
        angular.extend(ctrls[0], ctrls[1],{CNC : true}) ;
        ctrls[0].mount();
    }

    function OorNdcVentaCtrl ($scope, $attrs, Operacion, oorden) {
        
        angular.extend(this, {
            tipoDeOperacion : tipoDeOperacion,
            seccion : seccion,
            ccProducto : ccProducto
        });
        
        function tipoDeOperacion () {
            return 'CNC';
        }

        function seccion () {
            return 'C'; 
        }

        function ccProducto (producto) {
            return producto && producto.cuenta_nc_compra;
        }
    };

});

inject(DIRECTIVE, 'oorTransaccion', [ 'uuid', 'fecha', 'oorDialogoTercero'], function (uuid, fecha, oorDialogoTercero) {
    'use strict';

    return  {
        link : link,
        terminal :true
    };

    function link (scope, element, attrs, ctrls) {
        
        var opComponent = m.component(operaciones.OperacionComponent, {
            cuentaId          : attrs.cuentaId,
            operacionId       : attrs.operacionId,
            tipoOperacion     : attrs.tipoOperacion
        });

        /**
         * Inicializamos oorden y luego se carga el componente en el DOM
         */
        oorden().then(function () {
            oorden.getVendedores().then(function () {
                m.mount(element[0], opComponent);
            });
        });
    };
});
//import './directives/OorPago.js';


/**
 * REPORTES
 */
//import './directives/reportes/reporteBalanza.js';
//import './directives/reportes/reporteAuxiliar.js';
//import './controllers/OorReporteBalanza.js';


/**
* AUTOCOMPLETES
*/
inject(CONTROLLER, 'TercerosAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog', '$http','$q', 'uuid', '$parse', '$element', 'oorden'], function ($scope, Organizacion, $mdDialog, $http, $q, uuid, $parse, $element, oorden) {
    var ac = this;
    var seccion = $scope.opCtrl ? $scope.opCtrl.seccion() : '';

    ac.search = function (buscar) {
        var defer = $q.defer();
        var params = {buscar : buscar};

        $http.get('/api/terceros/search', {params : params}).success(function (r) {
            defer.resolve(r.data || []);
        });

        return defer.promise;
    }

    $scope.agregarTercero = showDialog;

    function showDialog($event) {
        var parentEl = angular.element(document.body);

        $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            template:
               '<md-dialog aria-label="List dialog">' +
               '  <md-content>'+
               '    <div oor-tercero="tercero" style="min-width:800px"></div>' +
               '  </md-content>' +
               '  <div class="md-actions">' +
               '    <md-button ng-click="closeDialog()">' +
               '      Cancelar' +
               '    </md-button>' +
               '    <md-button class="md-primary" ng-click="guardarTercero(t3Form)">' +
               '      Guardar' +
               '    </md-button>' +
               '  </div>' +
               '</md-dialog>',
            locals: {
            
            },
            controller: ['$scope', 'Tercero',AuxiliarDialogCtrl]
        }).then(function (tercero) {
            var sel = $('md-autocomplete', $element).attr('md-selected-item');
            console.log(tercero);
            $parse(sel).assign($scope, tercero);
            console.log($scope);
        });
    }

    function AuxiliarDialogCtrl (scope, Tercero) {
        scope.closeDialog = function() {
            $mdDialog.hide(null);
        };

        scope.tercero   = { 
            tercero_id : uuid(),
            es_cliente : seccion == 'V',
            es_proveedor : seccion == 'C'
        };

        scope.direccion = {
            codigo_pais : oorden.organizacion.codigo_pais
        };

        scope.guardarTercero = function () {
          scope.$broadcast('guardarTercero')
        }

        scope.$on('$tercero', function ($event,val) {
          $mdDialog.hide(val);
        })


    }

});
inject(CONTROLLER, 'ProductosAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function () {
        return ac.items;
    };

    Organizacion.productos().then(function (t) {
        ac.items = t;
    });
});
inject(CONTROLLER, 'CtasContablesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function (k) {
        var q = angular.lowercase(k);
        
        return ac.items.filter(function (item) {
            if(item.acumulativa == "1") return false;
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        return item.cuenta + ' - ' + item.nombre;
    };

    Organizacion.cuentasContables().then(function (c) {
        ac.items = c;
    });
});
inject(CONTROLLER, 'ImpuestosAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function () {
        return ac.items;
    };

    Organizacion.impuestos().then(function (t) {
        ac.items = t;
    });
});
inject(CONTROLLER, 'RetencionesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function () {
        return ac.items;
    };


    Organizacion.retenciones().then(function (t) {
        ac.items = t;
    });


});
inject(CONTROLLER, 'SucursalesAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function (k) {
        var q = angular.lowercase(k);

        return ac.items.filter(function (item) {
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        return item.clave + ' - ' + item.nombre;
    }


    Organizacion.sucursales().then(function (sucursales) {
        ac.items = sucursales;
    });
});
inject(CONTROLLER, 'VendedoresAutoCompleteCtrl', ['$scope', 'Organizacion', '$mdDialog'], function ($scope, Organizacion, $mdDialog) {
    var ac = this;

    ac.items = [];

    ac.search = function (k) {
        var q = angular.lowercase(k);

        return ac.items.filter(function (item) {
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        return item.nombre;
    }


    Organizacion.vendedores().then(function (vendedores) {
        ac.items = vendedores;
    });
});
inject(CONTROLLER, 'TiposDeDocumentoAutoCompleteCtrl', ['$scope','$http'], function ($scope, $http) {
    var ac = this;

    ac.items = [];
    ac.itemsByTipo = {};

    ac.search = function () {
        return ac.items;
    };

    ac.filterItems = function (tipoDoc) {
        if(!tipoDoc) return ac.items;
        return ac.itemsByTipo[tipoDoc];
    };

    $scope.buscar = function (tipoDocId){
        return ac.items.filter(function (t) { 
            return t.tipo_documento_id == tipoDocId;
        })[0];
    }

    $http.get('/api/tipodocumentos').success(function (t) {
        ac.items = t.data;
    
        ac.items.forEach(function (item) {
            if(!ac.itemsByTipo[item.tipo_operacion]) {
                ac.itemsByTipo[item.tipo_operacion] = [];   
            }

            ac.itemsByTipo[item.tipo_operacion].push(item);
        })
    }); 
});
inject(CONTROLLER, 'TiposDeCambioAutoCompleteCtrl', ['$scope','$http'], function ($scope, $http) {
    var ac = this;

    ac.items = [];
    ac.itemsByTipo = {};

    ac.search = function (k) {
        var q = angular.lowercase(k);

        return ac.items.sort(function (item, item2) {
            return ac.caption(item2) - ac.caption(item);
        }).filter(function (item) {
            return angular.lowercase(ac.caption(item)).indexOf(q) > -1;
        });
    };

    ac.caption = function (item) {
        if(!item) return '';
        return item.tipo_de_cambio + ' ' + item.nombre;
    }


    $http.get('/api/tiposdecambio').success(function (t) {
        ac.items = t.tipos;
    }); 

});

/**
* SELECTORES
*/

//import './directives/OorSelectorMesesContables.js';
//import './directives/selectores/oorSelectorTipoDeCambio.js';

inject.mount(module);

})();
/* FIN MODULO Operaciones */

angular.module('OordenWeb', ['oordenBase', 'oordenOrganizaciones', 'oordenOperaciones', 'oorden-web']);


/** 
 * MESSAGE API
 */
angular.module('OordenWeb').run([
    '$rootScope',
    '$compile',
    function ($rootScope, $compile) {
        $rootScope.__compiler__ = $compile;
    }
]);

}).call(this, this);


