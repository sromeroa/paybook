(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = {
    controller : CuentaFormController,
    view : CuentaFormView,
    openModal : openModal
}

var Cuenta = require('./CuentaModel');
var CuentaViewModel = require('./CuentaViewModel');

function CuentaFormController (params) {
    var ctx = this;

    ctx.cuenta = m.prop();
    ctx.guardar = guardar;

    ctx.vm = new CuentaViewModel({
        onsave : afterSave,
        onsaveError : function (errors) {
            toastr.error(errors[0].message);
        }
    });

    ctx.vm.cuenta = ctx.cuenta;

    ctx.impuestosSelector = oor.impuestosSelect2({
        impuestos : oorden.impuestos(),
        model : function () {
            if(arguments.length) {
                ctx.cuenta().impuesto_conjunto_id(arguments[0]);
            }
            return ctx.cuenta().impuesto_conjunto_id();
        }
    });


    ctx.initializeMask = function (element, isInited) {
        if(isInited) return;
        var formatoCuentas = f('formato_cuentas')( oorden.organizacion() );
        formatoCuentas = formatoCuentas.replace(/\d/g, '*');
        $(element).mask(formatoCuentas);

    }

    function guardar () {
        ctx.vm.guardar()
    }

    function afterSave (cuenta) {
        ctx.$modal && ctx.$modal.close();
        nh.isFunction(params.onsave) && params.onsave(cuenta);
    }


    ctx.cargarCuenta = function (argument) {
        Cuenta.obtenerTipos()
            .then(oorden.paisesYMonedas)
            .then(selectorMoneda)
            .then(cargarCuenta);
    }



    function selectorMoneda () {
        ctx.selectorMoneda = oor.select2({
            data : oorden.monedasDisponibles(),
            model : function () {
                if(arguments.length) {
                    ctx.cuenta().moneda( arguments[0] )
                }
                return ctx.cuenta().moneda();
            }
        })
    }

    function cargarCuenta () {
        if( ctx.cuenta() ) return;

        if(!params.cuenta_contable_id) {
            ctx.cuenta( new Cuenta );
            ctx.cuenta().$isNew(true);
            return;
        }

        return m.request({
            method : 'GET',
            url : '/apiv2',
            background : true,
            data : {
                modelo : 'cuentas',
                cuenta_contable_id : params.cuenta_contable_id
            },
            unwrapSuccess : function (r) {
                return new Cuenta(r.data[0]);
            }
        })
        .then(function (data) {
            ctx.cuenta(data);
        })
        .then(m.redraw);
    }
}

function CuentaFormView (ctx) {
    ctx.cargarCuenta();

    return m('div', [
        ctx.cuenta() ? CuentaFormulario(ctx) : oor.loading()
    ]);
}

function radioer (label, id, fn) {
    return [
        m('div', label),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'si' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(checked)
                }),
                checked : fn()
            }),
            m('label', {'for':id + 'si'}, 'Sí')
        ]),
        m('.radioer.form-inline.radioer-indigo', [
            m('input#' + id + 'no' + '[type=radio][name=' + id + ']', {
                onchange : m.withAttr('checked', function (checked) {
                    fn(!checked)
                }),
                checked : !fn()
            }),
            m('label', {'for': id + 'no'}, 'No')
        ])
    ];
}

/**
 * El formulario de la cuenta
 */
function CuentaFormulario (ctx) {
    var cuenta = ctx.cuenta();
    var ctaPadre = ctx.vm.cuentaPadre();

    return [

        m('.row', [
            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'No. de cuenta'),
                    m('input[type=text]', {
                        config : ctx.initializeMask,
                        placeholder :'Número de Cuenta',
                        value : cuenta.cuenta(),
                        onchange : m.withAttr('value', function (val) {
                            cuenta.cuenta(val);
                            ctx.vm.buscarCuentaPadre()
                        })
                    })
                ])
            ]),

            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'Nombre'),
                    m('input[type=text]', {
                        value : cuenta.nombre(),
                        placeholder : 'Nombre de la cuenta',
                        onchange : m.withAttr('value', cuenta.nombre)
                    })
                ])
            ])
        ]),

        m('.row', [
            m('.col-sm-12', [
                ctaPadre ? m('div', [
                    'Subcuenta de: ',
                    f('nombre')(ctaPadre),
                    '(' + f('cuenta')(ctaPadre) + ')'
                ]) : null
            ])
        ]),

        m('.row', [
            m('.col-sm-6', [
                m('.mt-inputer.line', [
                    m('label', 'Tipo'),
                    SelectTipo(cuenta.tipo_id)
                ])
            ])
        ]),

        m('.row', [
            m('.col-sm-12', [
                m('.mt-inputer.line', [
                    m('label', 'Descripción'),
                    m('textarea', {
                        placeholder : 'Descripción',
                        onchange : m.withAttr('value', cuenta.descripcion)
                    },cuenta.descripcion())
                ])

            ])
        ]),

        m('br'),

        m('.row', [


            m('.col-sm-6', [
                radioer('¿Es banco o Caja?', 'es-banco-o-caja', cuenta.es_bancos),
            ], cuenta.es_bancos() ? [

                m('.mt-inputer.line', [
                    m('label', 'Banco'),
                    m('input[type=text]', {
                        value : cuenta.banco(),
                        onchange : m.withAttr('value', cuenta.banco)
                    })
                ]),

                m('.flex-row.flex-end', [

                    m('.mt-inputer.line', {style : 'flex:2 2'}, [
                        m('label', 'Cuenta Bancaria'),
                        m('input[type=text]', {
                            size : 16,
                            value : cuenta.cuenta_bancaria(),
                            onchange : m.withAttr('value', cuenta.cuenta_bancaria)
                        })
                    ]),

                    m('.mt-inputer.line', {style:'flex:1 2'}, [
                        m('label', '# Cheque'),
                        m('input[type=text]', {
                            size: 5,
                            value : cuenta.numero_cheque(),
                            onchange : m.withAttr('value', cuenta.numero_cheque)
                        })
                    ])
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Moneda'),
                    ctx.selectorMoneda.view()
                ])

            ] : null),

            m('.col-sm-6', [
                m('div', {style:'margin:10px'},[
                    radioer('¿Pagos Permitidos?', 'pagos-permitidos', cuenta.pago_permitido)
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Código Agrupador SAT'),
                    m('input[type=text]', {
                        value : cuenta.codigo_oficial(),
                        readonly : 'readonly'
                    })
                ]),

                m('.mt-inputer.line', [
                    m('label', 'Conjunto de Impuestos'),
                    ctx.impuestosSelector.view()
                ])
            ])
        ])

    ]
}

function SelectNaturaleza (fn) {
    return m('select', {
        value : fn(),
        onchange : m.withAttr('value', fn)
    }, [
        m('option[value=A]', 'Acredora'),
        m('option[value=D]', 'Deudora')
    ])
}


function SelectTipo (fn) {
    var tipos = Cuenta.tipos();

    return m('select', {
        value : fn(),
        onchange : m.withAttr('value', fn)
    }, [
        Cuenta.tiposAgrupados().map(function (group) {
            return m('optgroup', {label : group.nombre}, [
                group.children.map(function (tipo) {
                    return m('option', {value : tipo.tipo_id}, tipo.nombre);
                })
            ]);
        })
    ])
}

function openModal (settings) {
     MTModal.open({
        controller :CuentaFormController,
        content : CuentaFormView,
        args : settings,
        top : function () {
            return m('h4', settings.nombre )
        },
        bottom : function (ctx) {
            return [
                m('button.pull-left.btn-flat.btn-sm.btn-default', {
                    onclick : ctx.$modal.close
                }, 'Cancelar'),

                m('button.pull-right.btn-flat.btn-sm.btn-success', {
                    onclick : ctx.guardar
                }, 'Guardar')
            ];
        },
        el : this,
        modalAttrs : {
            'class' : 'modal-medium'
        }
    });

}

},{"./CuentaModel":2,"./CuentaViewModel":3}],2:[function(require,module,exports){
module.exports = Cuenta;


var fields = {
    cuenta_contable_id : {},
    nombre : {},
    cuenta : {},
    subcuenta_de : {},
    tipo_id:{},
    naturaleza : {},
    descripcion : {},
    codigo_oficial : {},
    pago_permitido : { filter :function (d) { return Boolean(Number(d))}},
    es_bancos : { filter :function (d) { return Boolean(Number(d))}},

    banco : {},
    cuenta_bancaria : {},
    moneda : {},
    numero_cheque : {},

    impuesto_conjunto_id : {}
};


var obtenerTipos = null;


function Cuenta (data) {
    var obj = this;
    data || (data = {});

    Object.keys(fields).forEach(function (k) {
        var fieldDef = fields[k];
        var fieldVal = f(k)(data);

        if(typeof fieldDef.filter == 'function') {
            fieldVal = fieldDef.filter(fieldVal);
        }

        if(typeof fieldVal == 'undefined')
        {
            fieldVal = null;
        }

        obj[k] = m.prop(fieldVal);
    });

    obj.$isNew = m.prop(false);
    obj.$isNew.toJSON = function () { return undefined }
}

Cuenta.id = f('cuenta_contable_id');

Cuenta.tipos = m.prop({});
Cuenta.tiposAgrupados = m.prop([]);

Cuenta.obtenerTipos = function () {

    if(!obtenerTipos) {
        obtenerTipos = m.request({
            url : '/cuentas-contables/tipos',
            method : 'GET',
            unwrapSuccess : f('data')
        })
        .then(function (tipos) {
            var glbTipos = Cuenta.tipos();

            tipos.forEach(function (tipo) { glbTipos[tipo.tipo_id] = tipo; });

            var oGroups = Object.keys(tipos)
                .filter(function (t) {
                    return !tipos[t].padre
                })
                .map(function (t) {
                    var mTipo = tipos[t];
                    var children =  tipos.filter(function (tipo) { 
                        return tipo.padre == mTipo.tipo_id 
                    });

                    return nh.extend(mTipo, {children:children});
                });

            Cuenta.tiposAgrupados(oGroups)
        });
    }

    return obtenerTipos;
}

Cuenta.validar = function (cuenta) {
    var errores = [];

    if(!cuenta.nombre()) {
        errores.push({ prop : 'nombre', message : 'Se necesita una cuenta'})
    }

    if(! cuenta.cuenta()) {
        errores.push({ prop : 'cuenta', message : 'Especifique una número de cuenta válido'})
    }


    return errores;
}


Cuenta.guardar = function (cuenta) {

    var isNew = cuenta.$isNew();
    var method = isNew ? 'POST' : 'PUT';
    var url = '/apiv2/';
    url = url + (isNew ? 'agregar' : 'editar/' + Cuenta.id(cuenta)) + '?modelo=cuentas';

    return m.request({
        method : method,
        url : url,
        data : cuenta,
        unwrapSuccess : f('data')
    })
}
},{}],3:[function(require,module,exports){
module.exports = CuentaViewModel;

var Cuenta = require('./CuentaModel');

function CuentaViewModel (params) {
    this.cuenta = m.prop();

    this.cuentaPadre = m.prop();

    this.buscandoPadre = m.prop();

    this.errorCuenta = m.prop();

    this.guardando = m.prop();

    this.buscarCuentaPadre = function () {
        this.buscandoPadre(true);

        return m.request({
            method : 'GET',
            url : '/api/buscarCuentaPadre/' + this.cuenta().cuenta()
        }).then(function (d) {
           this.cuentaPadre(null);
           this.errorCuenta(null);

            if(d.error) {
                this.errorCuenta(d.error)
                return;
            }

            this.cuentaPadre(d);

            this.cuenta().tipo_id( d.tipo_id );
            this.cuenta().subcuenta_de( d.cuenta_contable_id )
            this.cuenta().naturaleza( d.naturaleza)

        }.bind(this));

    }.bind(this);



    this.guardar = function () {
        var errors = Cuenta.validar(this.cuenta());

        if(this.errorCuenta()) {
            errors.push(this.errorCuenta())
        }

        if(errors.length) {
            if(nh.isFunction(params.onsaveError)) {
                params.onsaveError(errors)
            }
            return;
        }

        this.guardando(true);
        update();

        Cuenta.guardar(this.cuenta())
            .then(function (rCuenta) {
                this.guardando(false);
                update();
                toastr.success('¡Cuenta Guardada!');

                if(nh.isFunction(params.onsave)) {
                    params.onsave( rCuenta )
                }
            }.bind(this));

    }.bind(this);



    var update = function () {
        if(!update.element()) {
            return;
        }
        d3.select(update.element()).call(updateForm, this)
    }.bind(this);

    update.element = m.prop();


    this.bridge = function (element) {

        var events = {};

        events.change = function (message) {
            var prop = this.cuenta()[message.prop];

            prop(message.value);
            
            if(message.prop == 'cuenta') {
                update();
                if(!message.value) {
                    toastr.error('¡Número de cuenta inválido!')
                }

                this.buscarCuentaPadre()
                    .then(update)
            }

        }.bind(this);


        events.commit = this.guardar;

        events.cancel = function () {
            nh.isFunction(params.oncancel) && params.oncancel();
        }


        update.element(element)

        $(element).on('change', '[x-prop]', function () {
            var input = $(this);
            var msg = {
                type : 'change',
                prop : input.attr('x-prop'),
                value : input.val()
            };
            emit(msg);
        });


        $(element).on('input', '[x-prop]', function () {
            var input = $(this);
            var msg = {
                type : 'input',
                prop : input.attr('x-prop'),
                value : input.val()
            };
            emit(msg);
        });


        $(element).on('click', '[x-action]', function () {
            var button = $(this);
            var msg = {
                type : button.attr('x-action')
            }

            emit(msg);
        })


        function emit (message) {
            console.log(message)
            if(nh.isFunction(events[message.type])){
                events[message.type](message)
            }
        }

    }.bind(this);

}



function updateForm (selection, context) {
    var cuentaPadre = context.cuentaPadre();
    var errCtaPadre = context.errorCuenta();

    selection.select('.cuenta-padre-cuenta')
        .text(cuentaPadre ? f('cuenta')(cuentaPadre) : '')

    selection.select('.cuenta-padre-nombre')
        .text(cuentaPadre ? f('nombre')(cuentaPadre) : '')

    selection.select('.cuenta-padre-error')
        .text(errCtaPadre ? errCtaPadre : '')

    selection.select('[x-prop=tipo_id]')
        .attr('disabled', cuentaPadre ? 'disabled' : undefined)
        .property('value', context.cuenta().tipo_id())

    selection.select('[x-action=commit]')
        .classed('disabled', context.guardando() ? true : false)
        .select('i')
        .classed({
            'fa-save' : !context.guardando(),
            'fa-spinner' : context.guardando(),
            'fa-spin' : context.guardando()
        })

}
},{"./CuentaModel":2}],4:[function(require,module,exports){
require('./nahui/nahui');


window.oor         = require('./oor');
window.oor.v1      = true;
window.oorden      = require('./oor/OordenData.js');
window.MTModal     = require('./mt-modal/mt-modal');
window.oor.operaciones = require('./oorOperaciones');

},{"./mt-modal/mt-modal":11,"./nahui/nahui":14,"./oor":19,"./oor/OordenData.js":16,"./oorOperaciones":28}],5:[function(require,module,exports){


module.exports = D3Date;



function D3Date () {
    var value     = m.prop();
    var formatted = m.prop();
    var format    = oor.fecha.prettyFormat.year;
    var element;
    var popUp;

    dateFn.onchange = m.prop();

    dateFn.popUp = function () {
        return popUp;
    }

    dateFn.value = function () {
        if(arguments.length)  {
            var v = value();
            value(arguments[0]);
            formatted( format(value()) );
            element && (element.value = formatted());

            if(v != value() ) {
                dateFn.onchange && dateFn.onchange(value());
            }
        }
        return value();
    }

    dateFn.formatted = function () {
        return formatted();
    }

    dateFn.element = function (el) {
        mount(el, dateFn);
        element = el;
    }

    dateFn.close = function () {
        if(popUp) popUp.transition().style('opacity', 0).remove();
        popUp = null;
    }

    dateFn.update = function () {
        popUp.call(dateFn)
    }

    dateFn.open = function () {
        if(!element) return;

        var el = element;
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();
        var top = pos.top + 10 + height;
        captureFocus = false;
        if(popUp) return;

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        opacity : 0,
                        position:'absolute',
                        'padding' : '20px',
                        width:'250px',
                        height:'280px',
                        background:'white',
                        'text-align' : 'center',
                        top  : String(pos.top).concat('px'),
                        left : String(pos.left).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0',
                        'box-shadow' : '3px 3px 6px rgba(0,0,0,0.3)',
                        'border-radius' : '8px',
                    });


        popUp.transition()
            .style({opacity : 1, top : String(top).concat('px') })

        popUp.call(dateFn);

        return popUp;
    }


    var weekStart = 1;
    var weekLen   = 7;
    var dias = ['L', 'M', 'X', 'J', 'V', 'S', 'D']

    function dateFn (selection, dateCalendar) {
        var valSQL = value();
        var val;
        var calendar = {};

        if(dateCalendar) {
            val = dateCalendar
        } else {
            if(!valSQL) {
                val = oor.fecha.toSQL(new Date);
            } else {
                val = oor.fecha.fromSQL(valSQL)
            }
        }

        if( selection.select('table.calendar').empty() ) {
            selection.append('button')
                .attr('class', 'pull-left btn btn-xs btn-flat btn-default')
                .html('&laquo;');

            selection.append('button')
                .attr('class', 'pull-right btn btn-xs btn-flat btn-default')
                .html('&raquo;');


            selection.append('h6')


            selection.append('table').attr('class', 'calendar')
            selection.select('table.calendar').append('thead').append('tr');
            selection.select('table.calendar').append('tbody');

            for(var h= 0; h<7; h++){
                selection.select('table.calendar thead tr').append('th').text(dias[h])
            }

            selection.append('button')
                .attr('class', 'btn btn-xs btn-flat btn-primary')
                .text('Seleccionar Hoy')
                .on('click', function () {
                    dateFn.value( oor.fecha.toSQL(new Date) );
                    selection.call(dateFn);
                    dateFn.close()
                })
        }


        calendar.startMonth = oor.fecha.inicialMes(val);
        calendar.endMonth   = oor.fecha.finalMes(val);
        calendar.startDay   = calendar.startMonth.getDay();
        calendar.startDiff  = calendar.startMonth.getDay() - weekStart ;

        if(calendar.startDiff < 0) {
            calendar.startDiff = 7  + calendar.startDiff;
        }

        calendar.startDate  = new Date(calendar.startMonth);
        calendar.startDate.setDate( calendar.startDate.getDate() - calendar.startDiff );

        calendar.weeks = [];
        var week;
        var currDate = new Date(calendar.startDate);

        for(var i = 0; i<6; i++)
        {
            calendar.weeks.push([])
            for(var j= 0; j<7; j++)
            {
                calendar.weeks[i].push({
                    value : oor.fecha.toSQL(currDate),
                    date  : currDate.getDate(),
                    month : currDate.getMonth() + 1
                })
                currDate.setDate( currDate.getDate() + 1);
            }
        }


        selection.select('h6').text( oor.fecha.fullLabelMes(calendar.startMonth.getMonth() + 1) + ' ' + calendar.startMonth.getFullYear() )

        var rows = selection.select('table.calendar tbody').selectAll('tr').data(calendar.weeks)

        rows.enter().append('tr')

        var cells = rows.selectAll('td').data(function (d) { return d })

        cells.enter().append('td')

        cells.text( f('date') );

        cells.attr('class','text-grey');

        cells.filter( f('month').is( calendar.startMonth.getMonth() + 1 ) ).attr('class', '')

        cells.filter( f('value').is(valSQL) ).attr('class', 'selected');

        cells.on('click', function (d) {
            dateFn.value(d.value);
            selection.call(dateFn);
            dateFn.close()
        });


        selection.select('button.pull-right')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() + 1);
                selection.call(dateFn, nDate);
            });

        selection.select('button.pull-left')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() - 1);
                selection.call(dateFn, nDate);
            });
    }


    var cadenas = {
        dia: 'date',   d:'date',  dias:'date',
        mes: 'month',  m:'month', meses:'month'
    }

    var metodos =  {
        date  : {set : 'setDate',  get : 'getDate'},
        month : {set : 'setMonth', get : 'getMonth'}
    }

    var factores = {'-' : -1, '+' : 1};

    dateFn.parse = function (val) {
        val = val.split(' ').join('');
        val = val.split('/').join('-')

        if(oor.fecha.validateSQL(val)) {
            return val;
        }

        var hoy = new Date, match;
        if(val == 'h' || val == 'hoy') {
            return oor.fecha.toSQL(hoy)
        }

        if(match = val.match( /([\+|\-])(\s+)?(\d{1,})(\s+)?([A-Za-z]+)/) ) {
            var que    = match[5].latinize().toLowerCase();
            var date   = new Date(hoy);
            var factor = match[1] == '-' ? -1 : 1;
            var monto  = Number(match[3]);

            if(!cadenas[que]) return false;
            que = metodos[ cadenas[que] ];

            if(!que) return false;

            date[que.set].call(date, date[que.get]() + (factor * monto) );
            return oor.fecha.toSQL(date);
        }

        if(match = val.match( /(\d{1,2})([A-Za-z]{3,15})(\d{1,4})?/) ) {
            var date = new Date;
            var nMes = match[2].toLowerCase();
            var nAno = match[3] ? Number(match[3]) : null

            var mes = oor.fecha.meses().filter( function (s) {
                return s.toLowerCase().indexOf(nMes) > -1;
            })[0];

            if(!mes) return false;
            date.setMonth(oor.fecha.meses().indexOf(mes) - 1);
            date.setDate(match[1]);

            if(nAno !== null) {
                if(nAno < 1000) {
                    nAno += 2000;
                }
                date.setFullYear(nAno);
            }

            return oor.fecha.toSQL(date);
        }
    }

    return dateFn;
}



function mount (el, date) {
    $(el).val( date.formatted() );

    el.addEventListener('focus', function () {
        if(el.value) el.setSelectionRange(0,el.value.length)
        date.open();
    });
    el.addEventListener('change', function () {
        var d = date.parse(el.value);
        if(d) {
            date.value(d);
        }
        date.update();
    });


    var captureFocus = false;

    document.body.addEventListener('click', function (ev) {
        if(ev.target!= el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },false);

    document.body.addEventListener('focus', function (ev) {
        if(ev.target != el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },true);

    el.addEventListener('blur', function (ev) { captureFocus = true; });

    function nodeIsParentOf(parent, child){
        var children = [];
        var curr = child;

        while(curr){
            children.push(curr);
            if(curr == parent) {
                return true;
            }
            curr = curr.offsetParent;
        }

        return false;
    }
}




D3Date.mounter = function (dateFn, attrs) {
    if(attrs.model()) { dateFn.value( attrs.model() ); }

    return function (element, isInit) {
        if(isInit == false) {
            if(attrs.model()) { dateFn.value( attrs.model() ); }


            dateFn.onchange = function (v) {
                attrs.model(v);
                attrs.onchange && attrs.onchange(v);
                m.redraw();
            };
        }

        if(attrs.model() != dateFn.value()) {
            dateFn.value(attrs.model());
        }
    }
}



D3Date.component = {
    oncreate : function (vnode) {
        vnode.state.dateFn = new D3Date();

        if(vnode.attrs.model()) {
            vnode.state.dateFn.value( vnode.attrs.model() );
        }
        vnode.state.dateFn.element(vnode.dom);

        vnode.state.dateFn.onchange = function (v) {
            vnode.attrs.model(v);
            nh.isFunction(vnode.attrs.onchange) && attrs.onchange(v);
            m.redraw();
        };
    },
    onupdate : function (vnode) {
        if(vnode.attrs.model() != vnode.state.dateFn.value()) {
            vnode.state.dateFn.value( vnode.attrs.model() );
        }
    },
    view : function () {
        return m('input[type=text]');
    }
}

},{}],6:[function(require,module,exports){
/**
 *
 */

module.exports = ASSelector;

function ASSelector (inputNode, properties) {
    if((this instanceof ASSelector) === false) return new ASSelector(inputNode, properties);
    var $this   = this;
    var value   = m.prop(null);
    var textVal = m.prop('');
    var text    = f('text');
    var popUp   = null;
    var data    = m.prop()
    var html    = defaultHTML;
    var update  = defaultUpdate;
    var searchString = m.prop('');
    var computer = computerDefault;
    var onchange;

    $this.computedData = m.prop();
    $this.searchString = searchString;
    $this.highlightID  = m.prop(null);
    $this.selected     = m.prop(null);
    $this.searching    = m.prop(false)


    $this.select = function (item) {
        var oldVal = $this.value();
        $this.selected(item);

        if(item != null && (typeof item != 'undefined')) {
            $this.value(item.id, text(item));
        } else {
            $this.value(null,'');
        }

        if(oldVal != $this.value()) {
            nh.isFunction(onchange) && onchange.call($this);
        }

        $this.updateInput();
        $this.close();
    }


    $this.selectId = function (id) {
        if(id != null && (typeof id != 'undefined')) {
            var item = $this.findById(id);
            $this.select(item);
        }
    }


    $this.findById = function (id) {
        if(id != null && (typeof id != 'undefined')) {
            return $this.getData().filter( f('id').is(id) )[0];
        }
    }

    $this.message = function () {
        return popUp.select('.message');
    }


    $this.compute = function (callback) {
        $this.computedData([]);

        $this.computer().call($this,$this, function (computedData) {
            $this.computedData(computedData);
            $this.redraw();
            nh.isFunction(callback) && callback();
        });
    }


    $this.computer = function () {
        if(arguments.length) {
            computer = arguments[0]
        }

        return computer;
    }


    $this.html =  function () {
        if(arguments.length) {
            html = arguments[0]
        }
        return html;
    }


    $this.update =  function () {
        if(arguments.length) {
            update = arguments[0]
        }
        return update;
    }


    $this.onchange = function ()  {
        if(arguments.length) {
            onchange = arguments[0];
        }
        return onchange;
    }

    $this.popUp = function () {
        return popUp;
    }


    $this.setData = function (aData) {
        if(arguments.length) {
            if(nh.isFunction(aData)) {
                data = aData;
            } else {
                data(aData);
            }
        }
    }


    $this.getData = function () {
        return data();
    };

    $this.isOpen = function () {
        return Boolean(popUp);
    };

    $this.updatePopUp = function (callback) {
        if(this.isOpen()) {
            $this.compute(callback);
        } else {
            $this.open();
        }
    };

    $this.redraw = function () {
        popUp.call(asSelector, $this, {});
    }

    $this.value = function () {
        if(arguments.length) {
            value(arguments[0]);
            textVal(arguments.length > 1 ? arguments[1] : "" );
        }

        return value();
    };


    $this.text = function () {
        if(arguments.length) {
            text = arguments[0];
        }
        return text;
    };

    $this.updateInput = function () {
        inputNode.value = textVal();
    };

    $this.highlight = function (attrs) {
        if(!attrs)attrs = {};
        var highlighted = $this.highlightID();

        var available = $this.computedData().filter(function (d) { return d.$show && d.$selectable;});
        var current = available.indexOf(available.filter( f('id').is(highlighted) )[0]);
        var nIdex;

        if(typeof attrs.byPosition === 'number') {
            nIndex = attrs.byPosition;
        } else if(typeof attrs.move === 'number') {
            nIndex = attrs.move + (current || 0);
        }

        if(typeof nIndex != 'undefined') {
            nIndex = Math.min(available.length-1, nIndex);
            nIndex = Math.max(0, nIndex);
            $this.highlightID(available[nIndex] ? available[nIndex].id : null);
        } else if (typeof attrs.id !== 'undefined') {
            $this.highlightID(attrs.id)
        }

        if(popUp) popUp.call(updateHighlight, $this.highlightID())
    };

    $this.close = function () {
        if($this.isOpen()) {
            popUp.remove();
            popUp = null;

            d3.select(inputNode)
                .style('position', undefined)
                .style('z-index', undefined)
        }

        $this.updateInput();
    }

    $this.selectHighlighted = function () {
        var d = popUp.selectAll('.ctitem').filter(function (d) {
            return $this.highlightID() === d.id;
        });
        $this.select( d3.select(d.node()).datum() );
    }

    $this.open = function () {
        if(popUp) return;
        if(value()) {
            $this.highlightID( value() );
        }

        var el = inputNode;
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();
        var top = pos.top - 10 //+ height + 5;
        captureFocus = false;

        d3.select(el)
            .style('position', 'relative')
            .style('z-index', 5001)

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        opacity : 0,
                        position:'absolute',
                        padding : '60px 10px 10px 10px',
                        height:'450px',
                        width: String(width + 128).concat('px'),
                        background:'white',
                        top  : String(top).concat('px'),
                        left : String(pos.left - 10).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0',
                        'box-shadow' : '3px 3px 6px rgba(0,0,0,0.3)',
                        'border-radius' : '8px'
                    });

        popUp.append('div').attr('class', 'message');
        popUp.append('div').attr('class','ct');
        popUp.transition().style({ opacity : 1 });

        $this.updatePopUp(function () {
            if($this.value()) {
                $this.highlight({ id:$this.value() })
            }
        });



        return popUp;
    }

    $this.performSearch = function (search) {
        $this.searching(true);
        $this.searchString(search);

        $this.updatePopUp(function () {
            $this.highlight({ byPosition:0 });
        });

        $this.searching(false);
    }

    if(properties.data) {
        $this.setData(properties.data);
    }

    if(properties.onchange) {
        $this.onchange(properties.onchange)
    }

    mountSelector(inputNode, $this);
    return $this;
}

function asSelector (selection, selector, args) {
    var allData = selector.getData();
    var data, filter, ct, items;

    ct     = selection.select('.ct');
    data   = selector.computedData().filter( f('$show').is(true) )
    items  = ct.selectAll('.ctitem').data(data);

    items.exit().remove();
    //items.style('display', function (d) { return d.$show ? undefined : 'none' });
    items.style('cursor', function(d) {  return d.$selectable ? 'pointer' : undefined });
    items.enter().append('div').attr('class', 'ctitem');
    items.exit().remove();
    items.call(selector.update(), selector);
    items.on('click', selector.select);


    if(selector.afterUpdate) {
        selection.call(selector.afterUpdate, items, selector);
    }

    //items.classed('Highlight', f('id').is(selector.highlightID()) )
}

function updateHighlight(selection, highlightID) {
    var items  = selection.selectAll('.ctitem')
    var hItems = items.filter(function (d) { return d.id == highlightID; });
    items.classed('Highlight', false);
    hItems.classed('Highlight', true);
    var node = hItems.node();
    if(node) {
        node.parentNode.scrollTop = -100 + node.offsetTop - node.parentNode.offsetTop;
    }
}

/**
 * Default Functioning config
 */

function fnFrom(value) {
    return function () { return value };
}

function computerDefault (selector) {
    var data = selector.getData();
    var searchString = selector.searchString().toLowerCase().latinize();
    var searching = selector.searching();
    var value = selector.value();


    data.forEach(function (d) {
        if(searching) {
            d.$show = searchString ? Boolean(d.search.indexOf(searchString) +1) : true;
        } else {
            d.$show = value ? d.id === value : true;
        }
        d.$selectable = 1;
    });
    return data;
}

function defaultUpdate (items, selector, args) {
    items.html( selector.html() );
}

function defaultHTML (d) {
    return d.text;
}


/**
 * Mount the selector on the input element
 */
function mountSelector (el, selector) {
    el.addEventListener('focus', function () {
        if(el.value) {
            el.setSelectionRange(0,el.value.length);
        }
        selector.searchString('');
        selector.open();
    });

    el.addEventListener('input', function () {
        selector.performSearch(el.value)
    });

    el.addEventListener('keydown', function (ev) {
        if( ev.keyCode === 40 || ev.keyCode === 38 ) {
            ev.stopPropagation();
            ev.preventDefault();
            selector.highlight({ move : ev.keyCode == 40 ? 1 : -1 });

        } else if( ev.keyCode === 13 || ev.keyCode === 9) {
            if(ev.keyCode === 13) {
                ev.stopPropagation();
                ev.preventDefault();
            }
            if( selector.highlightID() ) {
                //selector.select( TerceroComponent.byId[ selector.highlightID() ] );
                selector.selectHighlighted();
            } else {
                selector.select( selector.selected() );
            }
        } else if(ev.keyCode === 27) {
            //selector.select( selector.isOpen() ? selector.selected() : null);

            if(selector.isOpen()) {
                selector.close()
            }
        }
    });




    var captureFocus = false;

    document.body.addEventListener('click', function (ev) {
        if(ev.target!= el && selector.isOpen() && captureFocus) {
            var p = nodeIsParentOf(selector.popUp().node(), ev.target);
            if(!p) selector.close()
        }
    },false);

    document.body.addEventListener('focus', function (ev) {
        if(ev.target!= el && selector.isOpen() && captureFocus) {
            var p = nodeIsParentOf(selector.popUp().node(), ev.target);
            if(!p) selector.close()
        }
    },true);

    el.addEventListener('blur', function (ev) {
        captureFocus = true;
    });

    function nodeIsParentOf(parent, child){
        var children = [];
        var curr = child;

        while(curr){
            children.push(curr);
            if(curr == parent) {
                return true;
            }
            curr = curr.offsetParent;
        }

        return false;
    }


}

},{}],7:[function(require,module,exports){

module.exports = AdvSelector;

window.xtSelector = AdvSelector;


AdvSelector.current = m.prop(null);

function AdvSelector (settings) {
    var triggerTime = settings.triggerTime || 200;
    var limitResults = settings.limitResults || 5;
    var search = settings.search || searchFn;
    var text = settings.text || f('text');
    var value = settings.value || f('id');
    var enterFunction = settings.enter || enter;
    var data = nh.isFunction(settings.data) ?  settings.data : m.prop(settings.data || []);


    advSelector.model = settings.model;
    advSelector.drawedValue = m.prop();
    advSelector.highlighted = m.prop(0);
    advSelector.results = m.prop([]);
    advSelector.selection = m.prop();
    advSelector.selected = m.prop();

    advSelector.select = function (d) {
        advSelector.selected(d);
        advSelector.model(value(d));
        advSelector.update();
        nh.isFunction(settings.onchange) && settings.onchange();
        advSelector.hide();

        console.log('Select');
        m.redraw();
    }

    advSelector.show = function () {
        advSelector.selection().classed({ opened : true })

        if(advSelector.selected()) {
            advSelector.selection().select('input').property('value', text( advSelector.selected() ));
            advSelector.update([advSelector.selected()])
        }
    }

    advSelector.hide = function () {
        advSelector.selection().classed({ opened : false })
    }

    advSelector.byID = function (id) {
        return data().filter(function (d) { return value(d) == id; })[0]
    }



    advSelector.update = function (results) {
        var resultNodes = advSelector.selection().select('.results-container').selectAll('.result');

        advSelector.drawedValue( advSelector.model() );

        advSelector.selection()
            .select('.xt-view-template')
            .datum(advSelector.selected() || {})
            .call(enterFunction)

        if(results) {
            advSelector.results(results);
            advSelector.highlighted(0);

            resultNodes = resultNodes.data(results, value);

            resultNodes.enter()
                .append('div')
                .attr('class','result')
                .attr('x-value', value)
                .call(enterFunction)
                .on('keyup', function (d) {  d3.event.keyCode == 13 && select(d) } )

            resultNodes.exit().remove();
        }

        resultNodes.style('background-color', function (d,i) {
            return advSelector.highlighted() == i ? 'lightblue' : 'white'
        });
    }

    return advSelector;


    function advSelector (node, isInitialized) {
        var selection, list, container;


        if(advSelector.drawedValue() != advSelector.model()) {
            advSelector.selected(advSelector.byID(advSelector.model()));
            if(isInitialized === true) {
                 advSelector.update()
            }
        }



        if(arguments.length > 1 && isInitialized === true) {
            return;
        }



        node.innerHTML = '';

        selection = d3.select(node);

        selection.classed({'xt-selector':true}).attr('tabindex',0)

        selection.append('div').attr('class', 'xt-view-template');

        selection.append('div').attr('class', 'xt-list');

        list = selection.select('.xt-list');

        list.append('input').attr('type', 'text').attr('placeholder', 'Buscar Cuenta...')

        container = list.append('div').attr('class', 'results-container');

        advSelector.selection(selection)


        var notShow = m.prop(false);

        $(node).on('focus', function () {
            if(notShow()) {
                notShow(false)
                return;
            }

            advSelector.show();
            $('input', node).focus();
        });



        $('input', node).on('blur', function (ev) {
            setTimeout(function () {
                advSelector.hide();
                notShow(true)
            },1000);
        })



        $(node).on('click', '[x-value]', function (ev) {
            var d = advSelector.byID( $(this).attr('x-value') );
            advSelector.select(d);
            ev.stopPropagation();
            ev.preventDefault()
        });

        $(node).on('click', function () {
            advSelector.show();
            $('input', node).focus();
        });



        $(node).on('keypress', function (ev) {
            if(ev.target == this && ev.keyCode == 13) {
                advSelector.show();
                $('input', node).focus();
            }
        });


        $('input', node).on('keydown', function (ev) {
            var h = advSelector.highlighted();

            if(ev.keyCode == 38 || ev.keyCode == 40) {
                ev.stopPropagation()
                ev.preventDefault()

                if(ev.keyCode == 38) {
                    advSelector.highlighted(Math.max(0,h-1));
                    advSelector.update()
                }

                if(ev.keyCode == 40 ){
                    advSelector.highlighted( Math.min(advSelector.results().length-1, h+1));
                    advSelector.update();
                }
            }

            //ENTER
            if(ev.keyCode == 13) {
                advSelector.select(advSelector.results()[advSelector.highlighted()]);
            }

            if(ev.keyCode == 9) {
                advSelector.select(advSelector.results()[advSelector.highlighted()])
            }
        })

        $('input',node).on('input', _.debounce(function () {
            var val = $(this).val();
            var results;

            if(!val) {
                results = [];
                advSelector.highlighted(0);
            } else {
                results = data().filter(search(val, text));
                results = results.slice(0,limitResults);
                advSelector.highlighted(0);
            }
            
            advSelector.update(results);
        },triggerTime));

        advSelector.update();
    }

    function enter (selection) {
        selection.text(text)
    }
}

function searchFn (str, textFn) {
    str = format(str);
    return function (d) {
        return format(textFn(d)).indexOf(str) > -1;
    }
}

function format (d) {
    return String(d).latinise().toLowerCase();
}

},{}],8:[function(require,module,exports){
var FechasBar = module.exports = { D3Date : D3Date };

FechasBar.controller = function (args) {
    var ctx  = this;

    ctx.fechaDesde = m.prop(null);
    ctx.fechaHasta = m.prop(null);
    ctx.pre = m.prop('personalizado');

    ctx.value = {fechaDesde:ctx.fechaDesde, fechaHasta:ctx.fechaHasta};

    ctx.selectorFechaHasta = D3Date.mounter(D3Date(), { model : ctx.fechaHasta, onchange : changeV });
    ctx.selectorFechaDesde = D3Date.mounter(D3Date(), { model : ctx.fechaDesde, onchange : changeV });

    ctx.onchange = function () {
        nh.isFunction(args.onchange) && args.onchange();
    };

    function changeV () {
        ctx.personalizado();
        ctx.onchange();
    }

    ctx.esteAno = function () {
        var ahora = new Date;
        var inicial = oor.fecha.toSQL( oor.fecha.inicialAno(ahora) );
        var final = oor.fecha.toSQL( oor.fecha.finalAno(ahora) );

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteAno');
        //ctx.onchange();
    }

    ctx.anoAnterior = function () {
        var ahora = new Date;
        ahora.setFullYear( ahora.getFullYear() - 1);
        var inicial = oor.fecha.toSQL( oor.fecha.inicialAno(ahora) );
        var final = oor.fecha.toSQL( oor.fecha.finalAno(ahora) );

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteAno');
        //ctx.onchange();
    }

    ctx.esteMes = function () {
        var ahora = new Date;
        var inicial = oor.fecha.toSQL( oor.fecha.inicialMes(ahora) );
        var final = oor.fecha.toSQL( oor.fecha.finalMes(ahora) );

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteMes');
        //ctx.onchange();
    }

    ctx.mesAnterior = function () {
        var ahora = new Date;
        ahora.setMonth( ahora.getMonth() - 1);
        var inicial = oor.fecha.toSQL( oor.fecha.inicialMes(ahora) );
        var final = oor.fecha.toSQL( oor.fecha.finalMes(ahora) );

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteMes');
        //ctx.onchange();
    }

    ctx.ultimosDias = function (dias) {
        var ahora = new Date;
        var final = oor.fecha.toSQL(ahora);
        ahora.setDate( ahora.getDate() - dias);
        var inicial = oor.fecha.toSQL(ahora);

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteMes');
        //ctx.onchange();
    }


    ctx.ultimos30Dias = function (dias) {
        var ahora = new Date;
        var final = oor.fecha.toSQL(ahora);
        ahora.setMonth( ahora.getMonth() - 1);
        var inicial = oor.fecha.toSQL(ahora);

        ctx.fechaDesde(inicial);
        ctx.fechaHasta(final);
        ctx.pre('esteMes')
    }

    ctx.personalizado = function () {
        ctx.pre('personalizado')
    }

    ctx.asignarPre = function (value) {
        if(value && (value = ctx[value])) {
            value.call()
        }
    }


    ctx.update = function (fn) {
        return function () {
            fn();
            ctx.onchange();
        }
    };
};


FechasBar.view = function (ctx) {
    return m('.flex-row', [

            m('.btn-group.dropdown.mt-inputer.dateRange', {style:'flex:1 1 15em'}, [
                m('button.btn.btn-default.btn-sm.btn-flat', {'data-toggle':'dropdown'}, [
                    m('.small', m('i.fa.fa-chevron-down'), ' Rango de Fechas')
                ]),

                m('ul.dropdown-menu', {style:'font-size:14px'}, [
                    m('li', [
                        m('a[href=javascript:;]', m('strong','Por mes'))
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', {onclick : ctx.update(ctx.esteMes) },' - Este mes')
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', {onclick : ctx.update(ctx.mesAnterior) }, ' - Mes anterior')
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', m('strong','Por año'))
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', {onclick : ctx.update(ctx.esteAno) }, ' - Este año')
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', {onclick : ctx.update(ctx.anoAnterior) }, ' - Año anterior')
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', m('strong','Rango de fechas'))
                    ]),

                    m('li', [
                        m('a[href=javascript:;]', {onclick : ctx.update(ctx.ultimos30Dias) }, ' - Últimos 30 días')
                    ])
                ])
            ]),

        m('.mt-inputer', {style:'flex:3 1 15em'},[
            m('label', 'Desde: '),
            m('input[type=text]', {
                config : ctx.selectorFechaDesde,
                id : "fechai"
            })
        ]),

        m('.mt-inputer', {style:'flex:3 1 15em'}, [
            m('label','Hasta: '),
            m('input[type=text]', {
                config : ctx.selectorFechaHasta,
                id : "fechaf"
            })
        ])

    ]);
};


/*
var value = m.prop( oor.fecha.toSQL(new Date) );

function D3DateConfig (el, isInited) {
    if(isInited) return;
    var popUp;


    $(el).val( oor.fecha.prettyFormat( value() ) );

    el.addEventListener('focus', function () {
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();

        popUp && popUp.remove();

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        position:'absolute',
                        width:'300px',
                        height:'200px',
                        background:'white',
                        top  : String(10+height+pos.top).concat('px'),
                        left : String(pos.left).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0'
                    });
    });

    el.addEventListener('blur', function () {
        //reomove();
    })


    function remove () {
        popUp.remove()
    }
}
*/

D3Date.mounter = function (dateFn, attrs) {
    if(attrs.model()) { dateFn.value( attrs.model() ); }

    return function (element, isInit) {
        if(isInit == false) {
            if(attrs.model()) { dateFn.value( attrs.model() ); }
            dateFn.element(element);

            dateFn.onchange = function (v) {
                attrs.model(v);
                attrs.onchange && attrs.onchange(v);
                m.redraw();
            };
        }

        if(attrs.model() != dateFn.value()) {
            dateFn.value(attrs.model());
        }
    }
}


function D3Date () {
    var value     = m.prop();
    var formatted = m.prop();
    var format    = oor.fecha.prettyFormat.year;
    var element;
    var popUp;

    dateFn.onchange = m.prop();


    dateFn.popUp = function () {
        return popUp;
    }

    dateFn.value = function () {
        if(arguments.length)  {
            var v = value();
            value(arguments[0]);
            formatted( format(value()) );
            element && (element.value = formatted());

            if(v != value() ) {
                dateFn.onchange && dateFn.onchange(value());
            }
        }
        return value();
    }

    dateFn.formatted = function () {
        return formatted();
    }

    dateFn.element = function (el) {
        mount(el, dateFn);
        element = el;
    }

    dateFn.close = function () {
        if(popUp) popUp.transition().style('opacity', 0).remove();
        popUp = null;
    }

    dateFn.update = function () {
        popUp.call(dateFn)
    }

    dateFn.open = function () {
        if(!element) return;

        var el = element;
        var pos = $(el).offset();
        var height = $(el).height();
        var width = $(el).width();
        var top = pos.top + 10 + height;
        captureFocus = false;
        if(popUp) return;

        popUp = d3.select(document.body)
                    .append('div')
                    .style({
                        opacity : 0,
                        position:'absolute',
                        'padding' : '20px',
                        width:'250px',
                        height:'280px',
                        background:'white',
                        'text-align' : 'center',
                        top  : String(pos.top).concat('px'),
                        left : String(pos.left).concat('px'),
                        'z-index' : 5000,
                        border : '1px solid #f0f0f0',
                        'box-shadow' : '3px 3px 6px rgba(0,0,0,0.3)',
                        'border-radius' : '8px',
                    });


        popUp.transition()
            .style({opacity : 1, top : String(top).concat('px') })

        popUp.call(dateFn);

        return popUp;
    }


    var weekStart = 1;
    var weekLen   = 7;
    var dias = ['L', 'M', 'X', 'J', 'V', 'S', 'D']

    function dateFn (selection, dateCalendar) {
        var valSQL = value();
        var val;
        var calendar = {};

        if(dateCalendar) {
            val = dateCalendar
        } else {
            if(!valSQL) {
                val = oor.fecha.toSQL(new Date);
            } else {
                val = oor.fecha.fromSQL(valSQL)
            }
        }

        if( selection.select('table.calendar').empty() ) {
            selection.append('button')
                .attr('class', 'pull-left btn btn-xs btn-flat btn-default')
                .html('&laquo;');

            selection.append('button')
                .attr('class', 'pull-right btn btn-xs btn-flat btn-default')
                .html('&raquo;');


            selection.append('h6')


            selection.append('table').attr('class', 'calendar')
            selection.select('table.calendar').append('thead').append('tr');
            selection.select('table.calendar').append('tbody');

            for(var h= 0; h<7; h++){
                selection.select('table.calendar thead tr').append('th').text(dias[h])
            }

            selection.append('button')
                .attr('class', 'btn btn-xs btn-flat btn-primary')
                .text('Seleccionar Hoy')
                .on('click', function () {
                    dateFn.value( oor.fecha.toSQL(new Date) );
                    selection.call(dateFn);
                    dateFn.close()
                })
        }


        calendar.startMonth = oor.fecha.inicialMes(val);
        calendar.endMonth   = oor.fecha.finalMes(val);
        calendar.startDay   = calendar.startMonth.getDay();
        calendar.startDiff  = calendar.startMonth.getDay() - weekStart ;

        if(calendar.startDiff < 0) {
            calendar.startDiff = 7  + calendar.startDiff;
        }

        calendar.startDate  = new Date(calendar.startMonth);
        calendar.startDate.setDate( calendar.startDate.getDate() - calendar.startDiff );

        calendar.weeks = [];
        var week;
        var currDate = new Date(calendar.startDate);

        for(var i = 0; i<6; i++)
        {
            calendar.weeks.push([])
            for(var j= 0; j<7; j++)
            {
                calendar.weeks[i].push({
                    value : oor.fecha.toSQL(currDate),
                    date  : currDate.getDate(),
                    month : currDate.getMonth() + 1
                })
                currDate.setDate( currDate.getDate() + 1);
            }
        }


        selection.select('h6').text( oor.fecha.fullLabelMes(calendar.startMonth.getMonth() + 1) + ' ' + calendar.startMonth.getFullYear() )

        var rows = selection.select('table.calendar tbody').selectAll('tr').data(calendar.weeks)

        rows.enter().append('tr')

        var cells = rows.selectAll('td').data(function (d) { return d })

        cells.enter().append('td')

        cells.text( f('date') );

        cells.attr('class','text-grey');

        cells.filter( f('month').is( calendar.startMonth.getMonth() + 1 ) ).attr('class', '')

        cells.filter( f('value').is(valSQL) ).attr('class', 'selected');

        cells.on('click', function (d) {
            dateFn.value(d.value);
            selection.call(dateFn);
            dateFn.close()
        });


        selection.select('button.pull-right')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() + 1);
                selection.call(dateFn, nDate);
            });

        selection.select('button.pull-left')
            .on('click', function () {
                var nDate = new Date(dateCalendar || val);
                nDate.setMonth( nDate.getMonth() - 1);
                selection.call(dateFn, nDate);
            });
    }


    var cadenas = {
        dia: 'date',   d:'date',  dias:'date',
        mes: 'month',  m:'month', meses:'month'
    }

    var metodos =  {
        date  : {set : 'setDate',  get : 'getDate'},
        month : {set : 'setMonth', get : 'getMonth'}
    }

    var factores = {'-' : -1, '+' : 1};

    dateFn.parse = function (val) {
        val = val.split(' ').join('');
        val = val.split('/').join('-')

        if(oor.fecha.validateSQL(val)) {
            return val;
        }

        var hoy = new Date, match;
        if(val == 'h' || val == 'hoy') {
            return oor.fecha.toSQL(hoy)
        }

        if(match = val.match( /([\+|\-])(\s+)?(\d{1,})(\s+)?([A-Za-z]+)/) ) {
            var que    = match[5].latinize().toLowerCase();
            var date   = new Date(hoy);
            var factor = match[1] == '-' ? -1 : 1;
            var monto  = Number(match[3]);

            if(!cadenas[que]) return false;
            que = metodos[ cadenas[que] ];

            if(!que) return false;

            date[que.set].call(date, date[que.get]() + (factor * monto) );
            return oor.fecha.toSQL(date);
        }

        if(match = val.match( /(\d{1,2})([A-Za-z]{3,15})(\d{1,4})?/) ) {
            var date = new Date;
            var nMes = match[2].toLowerCase();
            var nAno = match[3] ? Number(match[3]) : null

            var mes = oor.fecha.meses().filter( function (s) {
                return s.toLowerCase().indexOf(nMes) > -1;
            })[0];

            if(!mes) return false;
            date.setMonth(oor.fecha.meses().indexOf(mes) - 1);
            date.setDate(match[1]);

            if(nAno !== null) {
                if(nAno < 1000) {
                    nAno += 2000;
                }
                date.setFullYear(nAno);
            }

            return oor.fecha.toSQL(date);
        }
    }

    return dateFn;
}



function mount(el, date) {
    $(el).val( date.formatted() );

    el.addEventListener('focus', function () {
        if(el.value) el.setSelectionRange(0,el.value.length)
        date.open();
    });
    el.addEventListener('change', function () {
        var d = date.parse(el.value);
        if(d) {
            date.value(d);
        }
        date.update();
    });


    var captureFocus = false;

    document.body.addEventListener('click', function (ev) {
        if(ev.target!= el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },false);

    document.body.addEventListener('focus', function (ev) {
        if(ev.target != el && date.popUp() && captureFocus) {
            var p = nodeIsParentOf(date.popUp().node(), ev.target);
            if(!p) date.close()
        }
    },true);

    el.addEventListener('blur', function (ev) { captureFocus = true; });

    function nodeIsParentOf(parent, child){
        var children = [];
        var curr = child;

        while(curr){
            children.push(curr);
            if(curr == parent) {
                return true;
            }
            curr = curr.offsetParent;
        }

        return false;
    }

}

},{}],9:[function(require,module,exports){

var SearchBar = module.exports = {};

SearchBar.controller  = function (args) {
    var ctx = this;

    ctx.internalSearch = m.prop();
    ctx.search = args.search;
    ctx.internalSearch( ctx.search() );

    ctx.placeholder = m.prop(args.placeholder || 'Buscar')

    var searchDebounced = _.debounce(function () {
        ctx.performSearch( ctx.internalSearch() );
    }, 500);

    ctx.performSearch = function (val) {
        ctx.internalSearch(val)
        ctx.search(val)

        if(args.onsearch) {
            args.onsearch();
        }
    };

    ctx.bindEvent = function (element, isInitialized) {
        if(args.autofocus) oor.autofocus(element, isInitialized);
        if(isInitialized) return;

        element.addEventListener('input', function () {
            var value = this.value;
            ctx.internalSearch(value);
            searchDebounced();
        });
    }
}


SearchBar.view = function (ctx) {
    return m('.mt-inputer', {style:'flex:1 2'}, [
        m('label', {style:'font-size:15px'}, m('i.ion-ios-search')),
        m('input[type=text]', {
            style : 'height:23px',
            placeholder : ctx.placeholder(),
            value : ctx.internalSearch(),
            config : ctx.bindEvent,
            id : 'search',
            onchange : m.withAttr('value', ctx.performSearch)
        })
    ])
}

},{}],10:[function(require,module,exports){
var TerceroSelector = module.exports = {};
var ASSelector  = require('../../misc/ASSelector');

TerceroSelector.oninit = function (vnode) {
    this.disabled = m.prop( Boolean(vnode.attrs.tercero) );
    this.setFocus = m.prop(false);

    this.setNull = function () {
        vnode.state.disabled(false);
        vnode.state.setFocus(true);
    };
};

TerceroSelector.getData = function (runCallback) {
    if(! TerceroSelector.data ) {
        TerceroSelector.data = oor.request('/apiv2?modelo=terceros')
    }
    if(TerceroSelector.data() == undefined)  {
        TerceroSelector.data.run(runCallback);
    } else {
        runCallback( TerceroSelector.data() );
    }
}

TerceroSelector.oncreate = function (vnode) {
    vnode.state.selector = CreateSelector(vnode);
};


TerceroSelector.onupdate = function (vnode) {
    if(vnode.state.setFocus() == true) {
        vnode.state.setFocus(false);
        requestAnimationFrame(function () { vnode.dom.querySelector('input.tsa').focus()});
    }
};



TerceroSelector.view = function (vnode) {
    return m('div.oor-selector-tercero', [
        m('.mt-inputer', [
            m('label', 'Cliente: '),

            m('input[type=text].tsa', {
                disabled : this.disabled()
            }),

            m('button.btn-flat.btn-xs.pull-left',{
                style : {
                    'margin-right':'10px',
                    'display' : this.disabled() ? 'inherit' : 'none'
                },
                onclick : this.setNull
            },'×'),
        ]),
        vnode.attrs.model() ? vnode.state.terceroView(vnode) : null
    ]);
};


TerceroSelector.terceroView = function (vnode) {
    return m('[style=padding:10px]', [
        m('h5', {style:'margin:0px'}, [
            m('strong', vnode.attrs.tercero.codigo),
            ' — ',
            vnode.attrs.tercero.nombre
        ]),
        m('div', {style:'padding-top:4px'},[
            m('span.text-grey.small', 'RFC: '),
            m('span', vnode.attrs.tercero.clave_fiscal),
        ])
    ])
}


function CreateSelector (vnode) {
    var tSelector = ASSelector(vnode.dom.querySelector('input.tsa'), {});

    tSelector.computer(SelectorComputer);

    tSelector.onchange(function () {
        vnode.state.disabled(true)
        vnode.attrs.onchange.call(tSelector);
        m.redraw();
    });

    tSelector.html(function (d) {
        var str = '<div><h5 style="margin:0"><strong>'  + d.data.codigo + '</strong>';
        str += ' — ' + d.data.nombre;
        str += '</h5>';
        str += '<h6>' + d.data.clave_fiscal ? d.data.clave_fiscal : '' + '</h6>'
        return  str;
    });

    tSelector.value(vnode.attrs.model(), vnode.attrs.tercero ? vnode.attrs.tercero.codigo : '');
    tSelector.updateInput();
}


function SelectorComputer (selector, done) {
    selector.message().html('cargando datos...');

    TerceroSelector.getData(function (iData) {
        selector.message().html('')

        var open  = selector.searching() === false;
        var searchString = selector.searchString();

        var data = iData.map(function (d) {
            return {data:d, id:d.tercero_id, text:d.codigo, $selectable:true, $show:true, search: String(d.codigo + ' ' + d.nombre + ' ' + d.clave_fiscal).toLowerCase() };
        });

        if(open && selector.value()) {
            data.forEach(function (d){ d.$show = selector.value() == d.id });
        } else {
            searchString = searchString.toLowerCase().latinize();
            data.forEach(function (d) { d.$show = d.search.indexOf(searchString) > -1; });
        }

        selector.setData(data);
        done(data);
    });


}

},{"../../misc/ASSelector":6}],11:[function(require,module,exports){
module.exports = MtModal;

MtModal.opened = m.prop([]);

MtModal.open = function (component) {
    MtModal.opened().push(MtModal(component));
 }

function MtModal (component) {
    function view(ctrl) {
        return m('.mt-modal', _.extend({config:open}, component.modalAttrs), [
            m('.mt-modal-top', top(ctrl)),
            m('.mt-modal-content', content(ctrl)),
            m('.mt-modal-bottom', bottom(ctrl))
        ])
    }


    function open (element, isInitialized) {
        if(!isInitialized && component.el) {
            modal.element = element;

            var el = component.el;
            var elRect= el.getBoundingClientRect();

            modal.elRect = elRect;

            var width = element.offsetWidth;
            var height = element.offsetHeight;

            var bodyHeight = document.body.offsetHeight;
            var bodyWidth = document.body.offsetWidth;


            element.style.top = ''.concat(elRect.top,'px');
            element.style['margin-top'] = 0;

            element.style.left = ''.concat(elRect.left,'px');
            element.style['margin-left'] = 0;

            modal.zoom = 'scale('
            modal.zoom += elRect.width / width;
            modal.zoom += ', ';
            modal.zoom += elRect.height / height;
            modal.zoom += ')';

            d3.select(element)
                .style('transform', modal.zoom)
                .style('transform-origin', '0 0')
                .style('opacity', 0)
                .transition().duration(500)
                    .style('transform', 'scale(1, 1)')
                    .style('opacity', 1)
                    .style('left', '' + (bodyWidth-width)/2 + 'px' )
                    .style('top', ''.concat((bodyHeight-height)/2,'px'))
        }
    }

    function content (ctrl) {
        if (typeof component.content === 'function') {
            return component.content(ctrl);
        }
        return component.content;
    }


    function top (ctrl) {
        if (typeof component.top === 'function') {
            return component.top(ctrl);
        }
        return component.top;
    }

    function bottom (ctrl) {
        if (typeof component.bottom === 'function') {
            return component.bottom(ctrl);
        }
        return component.bottom;
    }

    function closeModal() {
        var idx = MtModal.opened().indexOf(modal);
        if(idx > -1) MtModal.opened().splice(idx,1);
    }

    function close () {
        if(!component.el || !modal.element) return closeModal();

        //m.startComputation();

        d3.select(modal.element)
            .transition().duration(500)
                .style('opacity', 0)
                .tween('transform', function () {
                    var i = d3.interpolate('scale(1, 1)', modal.zoom) ;
                    return function (t) { this.style['transform'] = i(t); }
                })
                .style('top', ''.concat(modal.elRect.top + 'px'))
                .style('left', ''.concat(modal.elRect.left + 'px'))
                .each('end', _.once(function () {
                    closeModal();
                    m.redraw();
                }))

    }

    var modal = {
        controller : function () {
            var ctrl = new component.controller(component.args);
            ctrl.$modal = modal;
            return ctrl;
        },
        view : view,
        close : close
    }


    console.log(modal)

    return modal;
}

MtModal.initialize = function () {
    $(document).keyup(function (ev) {
        if(ev.keyCode == 27 && MtModal.opened().length) {
            MtModal.opened()[MtModal.opened().length -1].close()
        }
    })

    MtModal.initialize = nh.noop;
}

MtModal.view = function () {
    MtModal.initialize();
    return MtModal.opened().length ? [MtModal.overlay(), MtModal.opened()] : '';
}

MtModal.overlay = function () { return m('div.mt-modal-overlay') }

},{}],12:[function(require,module,exports){
require('./nahui');

var Field = require('./nahui.field');

module.exports = DTable;


function computeHeading (columns, settings) {
    settings || (settings = {});

    return function () {
        return columns.map(computeHeader);
        function computeHeader  (col) {
            var celda = {};

            celda.name = col.name;
            celda.text = col.caption;

            celda.html = function () {
                var caption = '';
                if(settings.sort && settings.sort() == col.name) {
                    caption = '<i class="'.concat(settings.sortDirection() == '+' ? 'ion-arrow-down-b' : 'ion-arrow-up-b', '"></i> '); 
                }
                return caption + col.caption;
            }

            celda.update = col.updateHeading;
            celda.enter = col.enterHeading;
            celda.column = col;

            if(col.subfields) {
                celda.subfields = col.subfields.map(computeHeader)
            }

            return celda;
        }
    }
}

/**
 * Armar los datos de las celdas
 */
function computeData (columns, settings) {
    return function (row) {
        return columns.map(computeColumn);

        function computeColumn (col) {
            var celda = {};

            celda.name = col.name;
            celda.column = col;
            celda.row = row;

            celda.value = col.value(row);
            celda.text = col.filter(celda.value);

            celda.update = col.update;
            celda.enter = col.enter;

            celda.enterEdit = col.enterEdit;

            if(col.subfields) {
                celda.subfields = col.subfields.map(computeColumn)
            }

            return celda;
        }
    }
}



function DTable () {
    var columns, rows, rowUpdate;
    
    rowUpdate = _.noop; 

    var HEAD = {
        compute : computeHeading,
        cellTag : 'th',
        enter : Field.enterHeading,
        update : Field.updateHeading,
        key : function (d,i) {
            return i
        }
    };

    var BODY = {
        compute : computeData,
        cellTag : 'td',
        enter : Field.enter,
        update : Field.update,
        key : f('id')
    };


    dtable.key = function () {
        if(arguments.length == 0) return BODY.key;
        BODY.key = arguments[0];
        return dtable;
    };


    dtable.rowUpdate = function () {
        if(arguments.length == 0) return rowUpdate;
        rowUpdate = arguments[0];
        return dtable;
    }

    function updateRows(tHead, columns, rows, kind, settings) {
        var filas, celdas, nuevasCeldas, filasEnter, filasEdit;

        var rowIds = rows.map(kind.key);

        // Se crean las filas
        // verificando si es editable
        filas = tHead.selectAll('tr').data(rows, function (d) { 
            return ( f('$edit')(d) ? '$edit:' : '').concat(kind.key(d));
        });

        filas.exit().remove();

        filas.enter()
            .append('tr')
            .attr('x-row', kind.key)

        //Sort, hay que verificarlo
        filas.sort(function (r1, r2) {
            return rowIds.indexOf(kind.key(r1)) - rowIds.indexOf(kind.key(r2));
        })
        .call(dtable.rowUpdate())

        /*
        filasEdit = filas.filter(f('$edit')).call(function (filas) {
            //Se seleccionan las filas y las celdas
            celdas = filas.selectAll(kind.cellTag).data(kind.compute(columns), f('name'))
            nuevasCeldas = celdas.enter().append(kind.cellTag)
        })
        filas.filter(function (d) { return !f('$edit')(d); })
        */
        //Se seleccionan las filas y las celdas

        celdas = filas.selectAll(kind.cellTag).data(kind.compute(columns, settings), f('name'));

        nuevasCeldas = celdas.enter().append(kind.cellTag)

        nuevasCeldas
            .filter(function (c) { return c.row && f('$edit')(c.row) })
            .each(function (c) { d3.select(this).call( c.enterEdit || c.enter || nh.noop) })

        //Aplicar enter a las celdas
        nuevasCeldas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.enter) === true })
            .each(function (c) { d3.select(this).call(c.enter); })

        nuevasCeldas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.enter) === false })
            .each(function (c) { d3.select(this).call(kind.enter); })

        //Actualizar las celdas que no tienen UpdateFunction
        celdas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.update) === false })
            .call(kind.update)

        //Actualiar las celdas con actualizador
        celdas
            .filter(function (c) {  return !c.row || !f('$edit')(c.row) })
            .filter(function (c) { return Boolean(c.update) === true })
            .each(function (c) { d3.select(this).call(c.update) })
    }

    function tFooter (tFoot, columns , settings) {
        if(!settings) return;

        var filas = tFoot.selectAll('tr').data([1]);
        var thEnter = filas.enter().append('tr').append('th')


        thEnter.append('span')

        thEnter.append('div').attr('class', 'btn-group')


        thEnter.select('.btn-group')
            .append('button')
            .attr('class', 'btn btn-default btn-xs btn-flat')
            .attr('data-page-prev','')
            .append('i')
            .attr('class','ion-chevron-left')

        thEnter.select('.btn-group')
            .append('button')
            .attr('class', 'btn btn-default btn-xs btn-flat')
            .attr('data-page-next','')
            .append('i')
            .attr('class','ion-chevron-right')



        filas.select('th')
            .attr('class', '')
            .attr('colspan', columns.length);

        filas.select('span').text(
            ''.concat(
                settings.offset + 1,
                ' \u2014 ',
                Math.min(settings.total, settings.offset + settings.pageSize), 
                ' de ',
                settings.total
            )
        );

    }




    function dtable (table, columns, rows, settings) {
        table.classed('megatable', true);

        if(table.select('thead').empty()){
            table.append('thead');
        }

        if(table.select('tbody').empty()) {
            table.append('tbody')
        }

        if(table.select('tfoot').empty()) {
            table.append('tfoot')
        }

        table.select('tbody').call(updateRows, columns, rows, BODY, settings);

        table.select('thead').call(updateRows, columns, [1], HEAD, settings);

        table.select('tfoot').call(tFooter, columns, settings)
    }

    return dtable;
}

},{"./nahui":14,"./nahui.field":13}],13:[function(require,module,exports){

module.exports = Field;

function Field (name, config) {
    if(!(this instanceof Field)) return new Field(name,config);
    var field = this;
    if(!config) config = {};

    nh.extend(field, {
        name : name,
        caption : config.caption || name,
        value : nh.isFunction(config.value) ? config.value : f(name),
        filter : nh.isFunction(config.filter) ? config.filter : nh.identity,
        sortFilter : nh.isFunction(config.sortFilter) ? config.sortFilter : nh.identity
    });

    if(config.enterEdit === true) {
        field.enterEdit = Field.enterEdit
    }

    //Copiar todo lo extra que tenga el campo
    Object.keys(config).forEach(function (k) {
        typeof field[k] == 'undefined' && (field[k] = config[k]);
    });
}

Field.updateHeading = function (sel) {
    sel.select('.t-title').html( f('html') )
}


Field.enterHeading = function (d) {
    d.append('div')
        .attr('class', function (celda) {
            return 't-title'.concat(' ', celda.column.headingClass || '')
        })
        .attr('data-sort-by', f('name'))
        .style('cursor', 'pointer')
}

Field.update = function (selection) {
    selection.text( f('text') );
}

Field.enter = function (selection) {
    selection
        .attr('class', function (celda) {
            var attr = this.getAttribute('class') || '';
            return attr.concat(' ','celda-',celda.name, ' ', celda.column.class || '');
        })
        .attr('x-cell', f('name'))
}

Field.enterEdit = function (selection) {
    selection
        .attr('x-cell', f('name'))
        .append('input')
            .attr('x-prop', f('name'))
            .attr('type', 'text')

}

/**
 * Link To Resource
 */
Field.linkToResource = function (name, config) {
    config = nh.extend({
        enter : Field.linkToResource.enter,
        update :  Field.linkToResource.update
    }, config || {});

    return new Field(name, config);
};

Field.linkToResource.enter = function (selection) {

    selection.append('a').attr('data-id', function (d) {
        return d.column.id ? d.column.id(d.row) : null
    }).call(Field.enter);
};

Field.linkToResource.update = function (selection) {
    selection.select('a')
        .attr('href', function (d) { return d.column.url(d.row) })
        .call(Field.update)
};

/**
 * Field para twice
 */
Field.twice = function (name, config) {
    var subfields = config.subfields.map(function (f) {
        return f;
    });

    config = nh.extend({
        updateHeading : Field.twice.updateHeading,
        enterHeading : Field.twice.enterHeading,
        enter : Field.twice.enter,
        update : Field.twice.update
    }, config || {}, {
        value : nh.noop,
        subfields : subfields
    });

    return new Field(name, config);
};

Field.twice.update = function (selection) {

    selection.datum().subfields.forEach(function (f, i) {
        selection.select('.twice'.concat(i+1))
            .datum(f)
            .call(f.update || Field.update)
    });
};

Field.twice.enter = function (selection) {
    selection.call(Field.enter);

    selection.datum().subfields.forEach(function (f, i) {
        var editMode = nh.f('$edit')(selection.datum().row);

        selection.append('div')
            .classed('twice'.concat(i+1), true)
            .datum(f)
            .call( (editMode && f.enterEdit)  || f.enter || Field.enter )
    });
};

Field.twice.updateHeading  = function (sel) {
    sel.datum().subfields.forEach(function (f,i) {
        sel.select('.twice'.concat(i+1))
            .datum(f)
            .call(f.updateHeading || Field.updateHeading)
    });
};

Field.twice.enterHeading = function (sel) {
    sel.datum().subfields.forEach(function (f,i) {
        sel.append('div')
            .attr('class', 'twice'.concat(i+1))
            .datum(f)
            .call(f.enterHeading || Field.enterHeading)
    })
}

},{}],14:[function(require,module,exports){
(function (glb) {

    var nahui = {
        globals : ['f']
    };

    function noop () {

    }

    function _isFunction (d) {
        return typeof d === 'function';
    };

    function identity () {
        return arguments[0]
    }

    function extend (source) {
        var i;

        [].forEach.call(arguments, function (obj) {
            if(obj === source) return;

            for(i in obj) {
                source[i] = obj[i];
            }
        });

        return source;
    }


    /**
     *
     */
    function memoize (fn) {
        var mem = {};
        return function (val) {
            if(!mem[val]) {
                mem[val] = fn(val);

            }
            return mem[val]
        }
    }


    /**
     *
     */
    function f (name) {
        fget.str = function () { return name };

        fget.is = function (search) {
            return function (obj) { return fget(obj) == search; }
        };

        return fget;

        function fget (obj) {
            return _isFunction(obj[name]) ? obj[name]() : obj[name];
        }
    }


    function xProp (callback, value) {
        if(!callback) callback = identity;

        xprop(value);

        return xprop;

        function xprop () {
            if(arguments.length) {
                value = callback(arguments[0]);
            }
            return value;
        }
    }

    function iProp (value, set) {
        return function iprop () {
            if(arguments.length) {
                value = set(arguments[0]);
            }
            return value;
        }
    }


    function getSetProp (value, setter) {

    }


    extend(nahui, {
        noop : noop,
        identity : identity,
        xProp : xProp,
        iProp : iProp,
        f : memoize(f),
        memoize : memoize,
        extend : extend,
        isFunction : _isFunction
    });

    nahui.globals.forEach(function (key) {
        glb[key] = nahui[key]
    });

    glb.nahui = glb.nh = nahui;

}).call(window, window)

},{}],15:[function(require,module,exports){

var Latinise={};Latinise.latin_map={"Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"};
String.prototype.latinise=function(){return this.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})};
String.prototype.latinize=String.prototype.latinise;
String.prototype.isLatin=function(){return this==this.latinise()}

},{}],16:[function(require,module,exports){
module.exports = oorden;
/*
 * Este módulo maneja datos de OORden de la organizacion
 */
var promise;
var metodosSalvables = ['cheque','transferencia', 'tarjeta de credito'];

oorden.initialize = initialize;
oorden.organizacion  = m.prop();
oorden.org = oorden.organizacion;


oorden.organizacion.unformat = function (v) {
    v = Number(v.split(',').join(''))
    return oorden.organizacion.round(v);
}

oorden.organizacion.factor = function () {
    return Math.pow(10, oorden.organizacion().numero_decimales);
}

oorden.organizacion.format = function (aNumber) {
    return oorden.organizacion.numberFormat()(aNumber)
}

oorden.organizacion.numberFormat = function () {
    if(!oorden.organizacion() ) {
        throw 'OORDEN data no cargada'
    }
    return d3.format(',.'.concat(oorden.organizacion().numero_decimales || 2, 'f'));
}



oorden.organizacion.round = function (number) {
    if(!oorden.organizacion() ) {
        throw 'OORDEN data no cargada'
    }
    var factor = oorden.organizacion.factor();
    return Math.round(number * factor) / factor;
}


oorden.ready = function (callback) {
    oorden.ready.callbacks.push(callback)
}
oorden.ready.callbacks = [];

function ready () {
    oorden.ready.callbacks.forEach(function (callback) {
        callback();
    })
}

oorden.sucursal      = m.prop();
oorden.usuario       = m.prop();

oorden.cuentas       = m.prop();
oorden.cctos         = m.prop();
oorden.tiposDeCambio = m.prop();
oorden.impuestos     = m.prop();
oorden.retenciones   = m.prop();
oorden.metodosDePago = m.prop();
oorden.tiposDeDocumento = m.prop();

oorden.paises = m.prop();
oorden.monedas = m.prop();
oorden.vendedores = m.prop();

var paisesYMonedas;
var vendedores;

oorden.monedasDisponibles = function () {
    var codigosMoneda = {};

    oorden.tiposDeCambio().forEach(function (tc) {
        codigosMoneda[tc.codigo_moneda] = true;
    });

    return oorden.monedas().filter(function (mon) {
        return codigosMoneda[mon.codigo_moneda] || false;
    }).map(function (mon) {
        return {
            id: mon.codigo_moneda ,
            text : mon.codigo_moneda + ' ' + mon.nombre
        }
    });
}


oorden.paisesYMonedas = function () {
    if(paisesYMonedas) return paisesYMonedas;

    paisesYMonedas = m.request({
        method : 'GET',
        url : '/api/paises'
    }).then(function (d) {
        oorden.paises(d.data.paises);
        oorden.monedas(d.data.monedas);
    });

    return paisesYMonedas
}


oorden.getVendedores = function () {
    if(vendedores) return vendedores;

    vendedores = m.request({
        method : 'GET',
        url : '/organizacion/vendedores',
        unwrapSuccess : f('data')
    }).then(oorden.vendedores);

    return vendedores;
}

function oorden () {
    return oorden.initialize();
}

function getPromise () {
    return promise;
}

/**
 * Obtiene la ayuda para los tooltips
 * @param string ayuda
 */
oorden.obtenerAyuda = function (ayuda) {
    /*
    return m.request({
        method : 'GET',
        url : '/api/ayuda/' + ayuda,
        unwrapSuccess : function (resultado) {
            var valor = resultado.data[Object.keys(resultado.data)[0]];
            if (valor == false) return "Ayuda no encontrada";
            return resultado.data[Object.keys(resultado.data)[0]];
        }
    });
    */
};



/**
 * La vista crea un node con <div data-oorden-org=""> y los datos de la organizacion incrustados
 */
 
function fastInit () {
    var data = document.querySelector('[data-oorden-org]');
    data = JSON.parse(data.getAttribute('data-oorden-org'));
    oorden.organizacion(data);
    ready();
}


/**
 * Obtiene los datos cacheados del localstorage, cada 5 min se consultan
 * Con el fin de reducir las llamadas a esta función
 */

function getData () {
    var id = document.querySelector('html').getAttribute('x-oorden-org');
    var metakey = 'OORDEN/data/' + id, time, key, datos;
    var def = m.deferred();

    now = new Date();
    now = Math.floor(now.getTime() / 1000);

    if(time = localStorage.getItem(metakey)) {
        //Si la key es vieja elimino todos los caches
        if((now - time) > 300) {
            Object.keys(localStorage).forEach(function (k) {
                if(k.indexOf('OORDEN/data/') == 0 ) {
                    localStorage.removeItem(k)
                }
            });
            time = false
        }
    }

    if(time && (datos = localStorage.getItem('OORDEN/data/' + id + '/' + time))) {
        datos = JSON.parse(datos);
        def.resolve(datos);
    }

    if(!datos) {
        m.request({method:'GET', url:'/organizacion/config'})
            .then(function (d) {
                localStorage.setItem(metakey, now);
                localStorage.setItem('OORDEN/data/' + id + '/' + now, JSON.stringify(d));
                def.resolve(d);
            });
    }

    return def.promise;
}



function initialize () {
    promise = getData().then(setData);
    oorden.initialize = getPromise;
    return promise;
}


function setData(data) {

   oorden.organizacion(data.organizacion);
   oorden.sucursal(data.sucursal);
   oorden.usuario(data.usuario);

    //CUENTAS
    oorden.cuentas( data.cuentasContables );
    oorden.cuenta = makeGetter(oorden.cuentas(), 'cuenta_contable_id');



    //CENTROS DE COSTO
    oorden.cctos(data.centrosDeCosto);
    oorden.cctos.activos = m.prop(
        data.centrosDeCosto.filter(function (ccto) {
            return Boolean(Number(ccto.activo));
        })
    );
    oorden.ccto = makeGetter(oorden.cctos(), 'centro_de_costo_id');

    oorden.buscarCcto = (function () {
        var ccto_elementos = {};
        oorden.cctos().forEach(function (ccto) {
            ccto.elementos.forEach(function (elem) {
                ccto_elementos[elem.elemento_ccosto_id] = ccto;
            });
        });
        return function (elementoId) {
            return ccto_elementos[elementoId];
        }
    }());


    oorden.obtenerParametroPorNombre = function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    //Tipos De Cambio
    oorden.tiposDeCambio(data.tiposDeCambio);
    oorden.tipoDeCambio = makeGetter(oorden.tiposDeCambio(), 'tipo_de_cambio');
    group(oorden.tiposDeCambio, oorden.tiposDeCambio(), 'codigo_moneda');
    oorden.tiposDeCambio().forEach(function (tdc) {
        if(tdc.base == "1") {
            oorden.tipoDeCambio.base = m.prop(tdc);
        }
    });


    //Tipos De Documento
    oorden.tiposDeDocumento(data.tiposDeDocumento);
    oorden.tipoDeDocumento = makeGetter(data.tiposDeDocumento, 'tipo_documento_id');
    oorden.tipodeDocumento = oorden.tipoDeDocumento;
    group(oorden.tiposDeDocumento, oorden.tiposDeDocumento(), 'tipo_operacion');


    //Impuestos
    oorden.impuestos(data.impuestos);
    var impuestoGetter = makeGetter(oorden.impuestos(), 'impuesto_conjunto_id');
    oorden.impuesto = impuestoGetter;

    //Retenciones
    oorden.retenciones(data.retenciones);
    oorden.retencion = makeGetter(oorden.retenciones(),'retencion_conjunto_id');


    //Metodos de Pago
    oorden.metodosDePago(data.metodosDePago)
    oorden.metodoDePago = makeGetter(oorden.metodosDePago(), 'id');
    delete(oorden.metodosDePago.toJSON);

    oorden.metodosDePago().forEach(function (mp) {
        var metodo = mp.metodo.toLowerCase();
        if(metodosSalvables.indexOf(metodo) >= 0) {
            oorden.metodosDePago[metodo] = m.prop(mp);
        }
    });
}

function makeGetter(data, key) {
    var map = {};
    data.forEach(function (d) { map[d[key]] = d; });
    return function (val) {
        return map[val];
    }
}

function group (holder, data, key) {
    data.forEach(function (d) {
        var keyVal = d[key];
        var groupProp = holder[keyVal];

        if(!groupProp) {
            holder[keyVal] = groupProp = m.prop([]);
        }

        groupProp().push(d);
    });
}

document.addEventListener('DOMContentLoaded', fastInit);

},{}],17:[function(require,module,exports){
module.exports = timeManager;


function timeManager () {
    var attr = 'x-time-till-now';

    var getNow =  _.throttle(function () { return new Date }, 5000);

    var getTime = _.memoize(createTime);

    function getTextDiff(ref, time) {
        var diff = Math.floor((ref - time)/ 1000);
        var k = 1;
        var l = 60;

        if(diff < 10) {
            return 'ahora';
        } else if(diff < 60) {
            return '1 min.';
        } else if(diff < 60 * 60) {
            return Math.floor(diff/60) + ' mins';
        } else if(diff < 60 * 60 * 24) {
            return Math.floor(diff/(60 * 60)) + ' hrs';
        }

        return Math.floor(diff/(60 * 60 * 24)) + ' días';
    }

    setInterval(function () { update(document.body); },10000);

    function update (node, isInited) {
        if(arguments.length > 1 && isInited == true) return;
        var nodes = node.querySelectorAll('[' + attr + ']');
        [].forEach.call(nodes, updateNode);
    }

    function updateNode (node) {
        var time = getTime(node.getAttribute(attr));
        node.innerHTML = getTextDiff(getNow(),time);
    }

    function createTime (t) {
        return new Date(t);
    }

    return  {update : update}
}

},{}],18:[function(require,module,exports){
module.exports = fecha;

function fecha () {
    var labelMeses = ['0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov','Dic'];
    var fullLabelMeses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    var now = m.prop(new Date);

    prettyFormat.year = function (str) {
        return prettyFormat(str, true);
    }

    return {
        fromSQL : desdeSQL,
        desdeSQL : desdeSQL,
        toSQL : toSQL,
        inicialMes : inicialMes,
        finalMes : finalMes,
        inicialAno : inicialAno,
        finalAno : finalAno,
        str : str,
        format : nh.memoize(format),
        prettyFormat : prettyFormat,

        to10 : to10,
        labelMes : labelMes,
        fullLabelMes : fullLabelMes,
        validate : validate,
        validateSQL : validateSQL,

        meses : function () { return fullLabelMeses }
    };


    function inicialMes (fecha) {
        var inicial = new Date(fecha);
        inicial.setDate(1);
        return inicial;
    }

    function finalMes (fecha) {
        var ffinal = new Date(fecha);
        ffinal.setMonth(ffinal.getMonth() + 1);
        ffinal.setDate(0);
        return ffinal;
    }

    function inicialAno (fecha) {
        var i = new Date(fecha);
        i.setMonth(0);
        i.setDate(1);
        return i;
    }

    function finalAno (fecha) {
        var f = new Date(fecha);
        f.setMonth(11);
        f.setDate(31);
        return f;
    }

    function desdeSQL (str) {
        var d = str.split('-');
        return new Date(to10(d[1]) + '/' + to10(d[2]) + '/' + d[0]);
    }

    function format (str) {
        if(!str){
            return '-';
        }
        var d = new Date(str);
        return d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
    }

    function prettyFormat (str, forceYear) {
        var info = str.split('-');
        var year = ' ' + info[0];

        if(year == now().getFullYear() && !forceYear) {
            year = '';
        }
        return to10(Number(info[2])).concat(' ', labelMes(info[1]),  year);
    }


    function toSQL (date) {
        return date.getFullYear() + '-' + to10(date.getMonth() + 1) + '-' + to10(date.getDate());
    }

    function to10 (n) {
        if(n < 10) return '0'.concat(n);
        return String(n);
    }

    function str (d) {
        return toSQL(d) + ' ' + to10(d.getHours()) + ':' + to10(d.getMinutes()) + ':' + to10(d.getSeconds());
    }

    function labelMes (d) {
        return labelMeses[Number(d)];
    }

    function fullLabelMes (d) {
        return fullLabelMeses[Number(d)];
    }

    function validate (d) {
        var data = String(d).match(/^\d{2}-\d{2}-\d{4}$/);
        return (data && data.length) ? true: false;
    }

    function validateSQL (d) {
        var data = String(d).match(/^\d{4}-\d{2}-\d{2}$/);
        return (data && data.length) ? true: false;
    }
};

},{}],19:[function(require,module,exports){
require('./Latinise');
require('../misc/advSelector');

var timeManager = require('./TimeManager');
var fecha = require('./fecha');
var cuentasSelect2 = require('./selectors/cuentasSelect2');
var cuentasSelector = require('./selectors/cuentasSelector');
window.Papa = require('../papaparse/papaparse');

var oor = {
    select2 : select2,
    tercerosSelect2 : tercerosSelect2,

    loading : function (inline) {
        return m(inline ? 'span' : 'div.text-center', {key : '$loading'}, m('i.fa.fa-spin.fa-spinner'));
    },

    fondoGris : function () {
        $('html').css('background',"#f3f3f3");
        $('body').css('background',"#f3f3f3");
    },

    autofocus : function (el, inited) {
        if(!inited) { el.focus(); }
    },

    cuentasSelect2 : cuentasSelect2,
    cuentasSelector : cuentasSelector,
    impuestosSelect2 : impuestosSelect2,
    retencionesSelect2 : retencionesSelect2,

    cctoSelect2 : cctoSelect2,
    tipoDocSelect2 : tipoDocSelect2,

    inputer : inputer,
    uSettings : userSettings,

    uuid : guid,
    mm : OorModels(),
    fecha : fecha(),

    time : timeManager(),
    xPropConfig : xPropConfig,
    displayMessages : displayMessages,
    lsMessage : lsMessageInitializer(),
    performHighlight : performHighlight,
    tooltip : tooltip,

    request :request,
    autoGrow : autoGrow
};

templateFunctions(oor);

module.exports = oor;


function autoGrow (el, isInitialized, data) {
    if(!isInitialized) {
        data.grow = function () {
            el.style.height = '10px';
            el.scrollTop = 20000;
            if(el.scrollTop < 20000) {
                el.style.height = String(Math.max(50,10 + el.scrollTop)).concat('px')
            }
        };

        data.grow.debounced = _.debounce(data.grow, 100);
        el.addEventListener('input', data.grow.debounced);
        el.style['min-width'] = '300px';
        data.grow();
    }
}




function request (url, method, extraParams) {

    var data = {
        method : method || 'GET',
        url : url,
        unwrapSuccess : f('data')
    };

    if(oor.v1 == true) {
        data.type = function (r) { return r.data }
    }

    var params = nh.extend(data, extraParams);

    return m.request(params);
}


function performHighlight (selector, color, startColor) {
    if(!startColor){
        startColor = '#fff'
    }

    return function () {
        d3.selectAll(selector)
            .style('background', startColor)
            .transition().duration(500)
            .style('background', '#79e078')
            .transition().delay(500).duration(500)
            .style('background', startColor)
            .each('end', function () {
                d3.select(this).style('background', undefined);
            })

    }
}



/**
 * Función que muestra los mensajes almacenados en localStorage
 */
function lsMessageInitializer () {
    var MESSAGE_LOCAL_STORAGE = 'MESSAGE_LOCAL_STORAGE';

    setTimeout(function () {
        var message;

        if(message = localStorage.getItem(MESSAGE_LOCAL_STORAGE)) {
            localStorage.removeItem(MESSAGE_LOCAL_STORAGE);

            message = JSON.parse(message);
            message.type = message.type || 'success';
            toastr[message.type](message.text);
        }

    },200);


    function lsMessage (msg) {
        localStorage.setItem(MESSAGE_LOCAL_STORAGE, JSON.stringify(msg));
    }

    window.lsMessage = lsMessage;
    return lsMessage;

}



m.redraw.debounced = _.debounce(m.redraw, 500);

function displayMessages () {
    var msgNode = document.querySelector('#messages-toastr');
    if(!msgNode) return;

    var messages = JSON.parse(msgNode.innerHTML);
    if(!messages) return;

    Object.keys(messages).forEach(function (kind) {
        messages[kind].forEach(function (msg) {
            toastr[kind](msg);
        })
    });
}

function xPropConfig(ctx) {
    return function (element, isInitialized) {
        if(isInitialized) return;

        $(element).on('input', '[x-prop]', function () {
            var input = $(this);
            var model = f(input.attr('x-model'))(ctx);

            model[input.attr('x-prop')](input.val())

            m.redraw.debounced();
        })
    }
}

$(function () {
    $(document.body).on('focus', '.mt-inputer > input, .mt-inputer > textarea', function () {
        $(this).parent().addClass('focus');
    });

    $(document.body).on('blur', '.mt-inputer > input, .mt-inputer > textarea', function () {
        $(this).parent().removeClass('focus');
    })
})


/**
 * Select2 Adapter
 *
 */

select2.NULL_VALUE = '::NULL_VALUE::';

function select2 (params) {
    var model    = m.prop( params.model );

    var data = ( typeof params.data === 'function') ? params.data : m.prop(params.data);
    var isInitialized = m.prop( false);
    var options       = m.prop( null );
    var selected      = m.prop( null );
    var el            = m.prop( null );
    var currVal       = m.prop( null );

    var enabled = true;

    var self = {
        view  : view,
        model : model,
        data  : data,
        selected : selected,
        element : el,
        rebuild : rebuild,
        config : config,
        key : params.key,
        onchange : params.onchange,
        enabled : params.enabled,
        el : el
    };

    return self;


    function rebuild () {
        if(isInitialized() == false) return;
        config(el()[0], false);
    }


    function notify () {
        var val = model();
        //Se actualiza el valor del modelo
        val(el().select2('val'));

        if(typeof params.find == 'function') {
            selected( params.find(val()) );
        }

        if(typeof self.onchange == 'function') {
            self.onchange( val() );
        }

        if(typeof val.onchange == 'function') {
            val.onchange( val() );
        }
    }

    function setValue(force) {
        if(force === true || (model())() != currVal()) {
            currVal( (model())() );
            if(! el() ) return;
            el().select2('val', currVal());
        }
    }

    function config (node, initialized) {
        var element   = $(node);
        var initValue = (model())();

        if(initialized === false) {
            el(element);
            var select2Params = {
                data : data(),
                templateSelection : params.templateSelection,
                templateResult    : params.templateResult,
                placeholder       : params.placeholder
            };

            if(params.url) {
                select2Params.ajax = {
                    url : params.url,
                    dataType : 'json',
                    delay : 250,
                    data :  params.ajaxData,
                    processResults : params.ajaxProcessResults,
                    cache : true
                }
                delete(select2Params.data);
            }


            function onChange() {
                var val = model();
                if(element.select2('val') == val()) return;

                if(element.select2('val') == select2.NULL_VALUE){
                    element.select2('val', null);
                }

                m.startComputation();
                notify();
                m.endComputation();
            }


            element.select2(select2Params)
                .on('change', function (e) {
                    isInitialized() && onChange()
                })


            element.parent().on('focus', '.select2-selection', function () {
                element.select2('open');
            });



            if((typeof self.enabled == 'function')) {
                element.select2('enable', self.enabled());
                enabled = self.enabled();
            }


            setValue(true);
            isInitialized(true);
        }

        if((typeof self.enabled == 'function') && self.enabled() != enabled) {
            element.select2('enable', self.enabled());
            enabled = self.enabled();
        }

        setValue();
    }

    function view (vParams) {
        vParams || (vParams = {});
        vParams.config = config;

        return [
            params.quitBtn ? m('div.pull-right', {
                style:'cursor:pointer',
                onclick:function () { el().select2('val',null) }
            }, '×') : '',
            m('select', vParams)

        ]
    }
}

/**
 * Return TercerosSelector
 */
function tercerosSelect2 (iParams) {

    var data = m.prop();

    getOptions();

    var selector = select2(_.extend({
        data : data,
        debug : true,
        find : function (id) {
            var f = (data() || []).filter(function (t) { return t.id == id })[0];
            return f && f.data;
        }
    }, iParams));

    selector.getOptions = getOptions;

    return selector;

    function getOptions () {
        var query = {modelo:'terceros'};



        if(iParams.tipo == 'cliente') {
            console.log('EsCliente')
            query.es_cliente = '1';
        } else if (iParams.tipo == 'proveedor') {
            console.log('EsProveedor')
            query.es_proveedor = '1';
        }

        return m.request({method:'GET', url:'/apiv2', data : query}).then(function (t) {
            data(
                t.data.map(function (t3) {
                    return {id:t3.tercero_id, text:t3.nombre + ' - ' + t3.clave_fiscal + ' - (' + t3.codigo + ')', data:t3};
                })
            );
            selector.rebuild();
        });
    }
}


/**
 * Return TercerosSelector
 */
function productosSelect2 (iParams) {

    var data = m.prop();

    getOptions();

    var selector = select2(_.extend({
        data : data,
        debug : true,
        find : function (id) {
            var f = (data() || []).filter(function (t) { return t.id == id })[0];
            return f && f.data;
        }
    }, iParams));

    selector.getOptions = getOptions;

    return selector;

    function getOptions () {
        return m.request({method:'GET', url:'/apiv2?modelo=productos'}).then(function (t) {
            data(
                t.data.map(function (t3) {
                    return {id:t3.tercero_id, text:t3.codigo +  ' - ' + t3.nombre + ' - ' + t3.clave_fiscal, data:t3};
                })
            );
            selector.rebuild();
        });
    }
}

/**
 * @return retencionesSelector
 */


function retencionesSelect2 (iParams) {

    var retenciones = iParams.retenciones
        .filter(function (c) { return c.estatus == 1})
        .map(function (c) {
            return {
                id:c.retencion_conjunto_id,
                text:c.retencion_conjunto
            };
        });

    retenciones.unshift({id:select2.NULL_VALUE, text:'(ninguna)'});

    return select2(
        _.extend({
            placeholder : '(ninguna)',
            data : retenciones,
            find : function (id) {
                return iParams.retenciones.filter(function (c) {
                    return c.retencion_conjunto_id == id;
                })[0];
            }
        }, iParams)
    );
}

/**
 * @return impuestosSelector
 */
function impuestosSelect2 (iParams) {
    return select2(
        _.extend({
            data : iParams.impuestos
            .filter(function (c) { return c.estatus == 1})
            .map(function (c) {
                return {
                    id:c.impuesto_conjunto_id,
                    text:c.impuesto_conjunto + ' (' + Number(c.tasa) + '%)'
                };
            }),
            find : function (id) {
                return iParams.impuestos.filter(function (c) {
                    return c.impuesto_conjunto_id == id;
                })[0];
            }
        }, iParams)
    );
}


function cctoSelect2  (iParams) {
    return select2(
        _.extend({
            data : iParams.centroDeCosto.elementos.map(function (e) {
                return {id:e.elemento_ccosto_id, text:e.elemento}
            })
        },iParams)
    );
}


/**
 * @return Selector de Tipos de Documento
 */

function tipoDocSelect2 (iParams) {
    return select2(
        _.extend({
            data : iParams.tiposDoc.map(function (t) {
                return {id:t.tipo_documento_id, text:t.nombre};
            }),
            find : function (id) {
                return iParams.tiposDoc.filter(function (t) {
                    return t.tipo_documento_id == id;
                })[0];
            }

        }, iParams)
    );
}

/**
 * @return
 */

function inputer (attrs, attrsInputer, attrsInput) {
    attrsInputer = _.extend({}, attrsInputer, {config: config});

    attrsInput = _.extend({}, {
        placeholder : attrs.placeholder || attrs.label,
        onchange : m.withAttr('value', function (v) {
            attrs.model(v);
            if(typeof attrs.model.onchange === 'function') attrs.model.onchange(v);
        }),
        onkeyup : m.withAttr('value',attrs.model),
        value : attrs.model()
    }, attrsInput);

    return m('div.mt-inputer', attrsInputer, [
        m('label', attrs.label),
        m((attrs.input || 'input[type="text"]').concat( attrs.readonly ? '[disabled]' : ''), attrsInput)
    ]);

    function config (node, initialized) {
        if(!initialized) {
            var el = $(node);

            el.delegate('input', 'focus', function () {
                el.addClass('focus');
            });

            el.delegate('input', 'blur', function () {
                el.removeClass('focus');
            });
        }
    }
}




/** USER SETTINGS  **/

function userSettings () {

    var settings = [].map.call(arguments, function (item) {
            return item.join(':');
        }).join(',');

    return m.request({
        method : 'GET',
        url : '/u-settings/settings?' + $.param({'settings' : settings}),
        unwrapSuccess : function (r) { return r.data; }
    });
}


userSettings.save = function (data) {
    return m.request({
        method : 'PUT',
        url : '/u-settings/settings',
        data  : data
    });
};

userSettings.choose = function (key, defaultValue) {
    return function () {
        for(var i = 0; i<arguments.length; i++) {
            if(arguments[i] && (typeof arguments[i][key]) != 'undefined') {
                return arguments[i][key];
            }
        }
        return defaultValue;
    }
};

/**
 *
 * UUID
 **/

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}




function tooltip (tooltipKey) {
    return tooltipKey;
}

/**
 * Modelos
 */

function OorModels () {
    if(!window.mtk) return;
    var mm = mtk.ModelManager();

    var map = {
        polizas : 'poliza',
        terceros : 'terceros',
        operaciones : 'operacion'
    };

    mm.api.prefix('/apiv2');

    mm.api.get = function (id) {

        var qParams = {modelo : this.serviceName()};
        qParams[this.idKey()] =  id;

        if(this.include()) {
            qParams.include = this.include();
        }

        return {
            method : 'GET',
            url : mm.api.prefix().concat('?', m.route.buildQueryString(qParams)),
        }
    };


    mm.api.create = function (resource) {
        var url;
        console.log( map[this.serviceName()]);

        if(map[this.serviceName()]) {
            if(resource.$new && resource.$new() == false && resource.editUrl) {
                url = resource.editUrl()
            } else {
                url = '/api/' + map[this.serviceName()] + '/agregar';
            }
        } else {
            url = mm.api.prefix().concat('?modelo=' + this.serviceName() );
        }


        return  {
            method : 'POST',
            url : url,
            data : resource.toJSON()
        };
    };

    return mm;
}


function templateFunctions (oor) {

    oor.panel = function (settings, body) {
        var title = settings.title;
        if(typeof title == 'string'){
            title = m('h3', title)
        }

        return m('div', [
            m('#title-head', [
                m('.col-xs-12.col-md-10.title-bar', title),
                m('.col-xs-12.col-md-6.action-bar', [ settings.buttons ? m('.panel-buttons', settings.buttons) : null ]),
                m('.clear'),
            ]),

            settings.inter ? settings.inter : null,

            m('.panel', [
                m('.panel-body',body)
            ]),
        ]);
    };

    oor.stripedButton = function (tag,children,attrs) {
        return m((tag || 'button').concat('.btn.button-striped.button-full-striped.btn-ripple.btn-xs'),attrs,children)
    }

}

},{"../misc/advSelector":7,"../papaparse/papaparse":35,"./Latinise":15,"./TimeManager":17,"./fecha":18,"./selectors/cuentasSelect2":20,"./selectors/cuentasSelector":21}],20:[function(require,module,exports){
module.exports = cuentasSelect2;

var CuentaForm = require('../../ajustes/cuentas-contables/CuentaForm');
 var add = '::ADD::'
/**
 * return CuentasSelector
 */
function cuentasSelect2(iParams) {


   
    var cuentas = m.prop();
    var lCuentas = buildData(iParams);
    var cuentasMap = lCuentas.map;

    cuentas(lCuentas.cuentas);

    var selector = oor.select2(
        _.extend({
            data : cuentas,
            find : function (id) {
                return iParams.cuentas.filter(function (c) {
                    return c.cuenta_contable_id == id;
                })[0];
            },
            templateResult : formatCuenta(true),
            templateSelection : formatCuenta()
        }, iParams, {
            onchange : function (d) {
                if(d == add) {

                    CuentaForm.openModal.call(selector.el()[0],{
                        nombre : 'Crear Cuenta',
                        onsave : function (rCuenta) {
                            var cuenta = JSON.parse(JSON.stringify(rCuenta));
                            cuenta.acumulativa = '0';

                            iParams.cuentas.push(cuenta);

                            var lCuentas = buildData(iParams);
                            cuentasMap = lCuentas.map;

                            cuentas(lCuentas.cuentas);

                            selector.rebuild();
                            iParams.model(cuenta.cuenta_contable_id);
                            m.redraw();
                        }
                    });

                    return;

                } else if(nh.isFunction (iParams.onchange)) {
                    iParams.onchange(d);
                }
            }
        })
    );

    return selector;


    function formatCuenta (detalle) {
        return function  (item) {
            var cuenta = cuentasMap[item.id || item.cta_id];
            var cuentaPadre, nombreCuenta;
            cuenta || ( cuenta = {} );

            if(cuenta.subcuenta_de) {
                cuentaPadre = cuentasMap[cuenta.subcuenta_de];
            }

            nombreCuenta = '<div class="cuenta-nombre">';

            if(cuentaPadre) {
                nombreCuenta += '<small>' +cuentaPadre.nombre  + '</small>' + ' &raquo; '
            }

            nombreCuenta += '<strong>' + cuenta.nombre + '</strong>';
            nombreCuenta += '</div>';

            return $('<div class="cuenta-display ' + (item.id && detalle ? 'detalle' : 'acumulativa') + '"><div class="cuenta-cuenta">' + cuenta.cuenta + '</div>' + nombreCuenta + '</div>');
        }
    }
}




function buildData (iParams) {
    var cuentasMap = {};
    var incluirCta = {};
    var agregarCta = (typeof iParams.agregarCta  == 'undefined') ? true : iParams.agregarCta;

    
    iParams.cuentas.filter(function (cta) {
        return cta.acumulativa == '0';
    }).forEach(function (cta) {
        incluirCta[cta.cuenta_contable_id] = true;
        incluirCta[cta.subcuenta_de] = true;
    });

    iParams.cuentas.forEach(function (c) {
        cuentasMap[c.cuenta_contable_id] = c;
    });

    var lCuentas = iParams.cuentas.filter(function (cta) {
            return incluirCta[cta.cuenta_contable_id];
        })
        .map(function (cta) {
            var id = cta.cuenta_contable_id;

            if(cta.acumulativa == '0'){
                return {id:id, text:cta.cuenta + ' ' + cta.nombre};
            } else {
                return {cta_id :id, text:cta.cuenta + ' ' + cta.nombre};
            }
        });

    if(agregarCta) {

        lCuentas.unshift({
           text : 'Agregar Nueva Cuenta',
           id : add
        });

        cuentasMap[add] = {
            nombre:'', 
            cuenta: 'Agregar Cuenta...'
        };
    }


    return {
        cuentas : lCuentas,
        map : cuentasMap
    };

}

},{"../../ajustes/cuentas-contables/CuentaForm":1}],21:[function(require,module,exports){

module.exports = cuentasSelector;

function cuentasSelector (params) {
    var cuentasMap = {};

    var cuentas = params.cuentas
        .filter( f('estatus').is('1') )
        .filter(function (d) {
            cuentasMap[d.cuenta_contable_id] = d;
            return d.acumulativa == 0;
        });

    cuentas.forEach(function (d) {
        if(d.subcuenta_de) {
            d.padre = cuentasMap[d.subcuenta_de]
        }
    });

    return xtSelector(
        nh.extend({
            data : cuentas,
            text : function (d) { 
                return String(f('cuenta')(d)).concat(' ',f('nombre')(d)); 
            },
            enter : function (selection) {
                selection.html('');
                selection.append('div').text( f('cuenta') );

                selection.append('div').attr('class','opt-nombre-cuenta')
                    .html(function (d) {
                        var s = '';
                        //if(d.padre) s = s.concat('<small class="text-grey">', d.padre.nombre ,'</small> &raquo; ')
                        return  s.concat('<small>', d.nombre || '', '</small>');
                    });
            },
            value : f('cuenta_contable_id')
        }, params)
    );
}
},{}],22:[function(require,module,exports){
var ArchivosComponente = module.exports = {
    title    : m.prop('Archivos'),
    btnLabel : [ m('i.fa.fa-paperclip'), ' Archivos'],
    view : archivosView
};


function archivosView (vnode) {

    return m('.row.operacion-cfdi', [
        m('h6', 'Componente Deshabilitado')
    ]);

}

},{}],23:[function(require,module,exports){
module.exports = {
    title    : m.prop('Bitácora'),
    btnLabel : [ m('i.fa.fa-list-ol'), ' Bitácora'],
    view : bitacoraView
};



function bitacoraView (vnode) {
    return m('.row.operacion-cfdi', [
        m('h6', 'Componente Deshabilitado')
    ]);
}

},{}],24:[function(require,module,exports){

var CFDIComponente = module.exports = {
    title    : m.prop('CFDI'),
    btnLabel : [ m('i.fa.fa-file-code-o'), ' CFDI'],
    view : cfdiView,
    xmlLink : function (o) { return '/facturacion/getXML/' + o.folio_fiscal },
    pdfLink : function (o) { return '/facturacion/getXML/' + o.folio_fiscal }
};



function cfdiView (vnode) {


    return m('.row.operacion-cfdi', [
        m('ul.col-xs-6', [
            m('li',[
                m('a.btn.btn-flat.btn-default', {href:'/facturacion/getXML/' + this.xmlLink(vnode.attrs.operacion) }, [
                    m('i.fa.fa-file-code-o'), ' Descargar XML'
                ]),
            ]),
            m('li',[
                m('a.btn.btn-flat.btn-default', {href:'/facturacion/getXML/' + this.pdfLink(vnode.attrs.operacion) }, [
                    m('i.fa.fa-file-pdf-o'),  ' Descargar PDF'
                ])
            ]),
        ]),

        m('form.col-xs-6', [
            m('.input-form', [
                m('label', 'Enviar a:'),
                m('input[type=email]'),
                m('button.btn.btn-flat.btn-primary.btn-xs', 'Enviar')
            ])

        ])
    ]);
}

},{}],25:[function(require,module,exports){

var EncabezadoOperacion = module.exports = {};
var FechaInput  = require('../../inputs/fechaInput');
var TerceroSelector = require('../../misc/selectors/TerceroSelector.v1.js');

var vencimiento = m.prop();

EncabezadoOperacion.view = function (vnode) {
    var operacion = vnode.attrs.operacion;

    return m('.encabezado-operacion', [

        m('.col-sm-6', [
            m(TerceroSelector, {
                model   : vnode.attrs.terceroId,
                tercero : operacion.tercero,
                onchange : function () {
                    var tercero = this.selected();
                    if(tercero) {
                        operacion.tercero = this.selected().data;
                    } else {
                        operacion.tercero = null;
                    }
                }
            }),

            /*
            m('hr', {style:'margin:2px'}),

            m('h6', {style:'margin-bottom:10px'}, m('span.small', 'Dirección Fiscal: '), 'Jose Luis Lagrange #103'),

            m('h6',m('span.small', 'Entregar En: '), 'Jose Luis Lagrange #103')
            */
        ]),

        m('.col-sm-6', [

            //m('div[style=position:relative;top:-20px;right:-20px;background-color:#f0f0f0;padding: 2px 8px;border-radius:4px]', [



                m('h6.small.pull-right', {
                    style : 'background-color:#f9f9f9;padding:4px;border-radius:4px; position:relative; top:-20px;right:-20px;margin-bottom:-20px'
                }, [
                    'Estatus: ', m('strong.text-indigo', 'APLICADA'),
                ]),

                m('.clear'),

            //]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '}, [
                    m('label', 'Referencia: '),
                    m('input[type=text]', {
                        value : operacion.referencia,
                        disabled : true
                    })
                ])
            ]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '}, [
                    m('label', 'Vendedor: '),
                    m('input[type=text]', {
                        value : operacion.referencia,
                        disabled : true
                    })
                ])
            ]),

            m('.flex-row', [
                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Vencimiento: '),
                    m(FechaInput.component, { model : vencimiento })
                ]),

                m('.mt-inputer',  {style:'flex: 1 5 '},[
                    m('label', 'Moneda:'),
                    m('input[type=text]', {
                        value : operacion.moneda,
                        disabled : true
                    })
                ])
            ]),


            m('.flex-row', [

                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Tipo de Cambio: '),
                    m('input[type=text]', { value : operacion.tipo_de_cambio })
                ]),


                m('.mt-inputer', {style:'flex: 1 5 '},[
                    m('label', 'Tasa de Cambio: '),
                    m('input[type=text]', { value : operacion.tasa_de_cambio })
                ]),

            ]),


            m('h6.small.text-center',{
                style:'margin:0 20px; background-color:#f0f0f0;border-radius:4px;padding:4px'
            }, [
                'Afectación Contable: ', m('strong.text-indigo', 'Diario #3')
            ]),


        ])
    ]);
}

},{"../../inputs/fechaInput":5,"../../misc/selectors/TerceroSelector.v1.js":10}],26:[function(require,module,exports){
var archivos = require('./ArchivosComponente');
var notas = require('./NotasComponente')
var cfdi = require('./CFDIComponente')
var bitacora = require('./BitacoraComponente')

module.exports = {
    archivos : convertir(archivos),
    bitacora : convertir(bitacora),
    cfdi : convertir(cfdi),
    notas : convertir(notas)
};

/**
 * Le pone un wrap alas funciones
 */
function convertir (iComponente) {
    var originalView = iComponente.view;

    return _.extend({}, iComponente, {

        view : function (vnode) {
            return m('div[style=padding:10px 20px]', [
                m('.close', { onclick:vnode.attrs.close },'×'),
                m('h4.text-indigo', vnode.state.btnLabel),
                originalView.call(vnode.state, vnode)
            ]);
        }

    });
}

},{"./ArchivosComponente":22,"./BitacoraComponente":23,"./CFDIComponente":24,"./NotasComponente":27}],27:[function(require,module,exports){
module.exports = {
    title    : m.prop('Notas'),
    btnLabel : [ m('i.fa.fa-comments-o'), ' Notas'],
    view : notasView
};

function notasView (vnode) {
    return m('.row.operacion-cfdi', [
        m('h6', 'Componente Deshabilitado')
    ]);
}

},{}],28:[function(require,module,exports){

var Listado      = require('./listado/listado.js');
var verOperacion = require('./pantallas/verOperacion.js');

module.exports = {
    listado      : Listado,
    verOperacion : verOperacion
};

},{"./listado/listado.js":32,"./pantallas/verOperacion.js":34}],29:[function(require,module,exports){
module.exports = Aplicar;

function Aplicar(ctx) {
    return function () {
        if(ctx.$sel.length == 0) {
            return toastr.error('Debes seleccionar al menos una operación');
        }

        MTModal.open({
            args : {
                operaciones : ctx.$sel,
                onEjecutar : ctx.reload.bind(ctx)
            },
            controller : AplicarOperacionesController,
            top : AplicarOperacionesTop,
            content : AplicarOperacionesContent,
            bottom : AplicarOperacionesBottom,
            el : this,
            modalAttrs : {
                'class' : 'modal-medium'
            }
        });
    }
}

function AplicarOperacionesTop () {
    return m('h4', 'Aplicar Operaciones');
}

function AplicarOperacionesBottom (ctx) {
    var mostrarAplicar = ctx.resultados() && ctx.resultados().validas.length;
    mostrarAplicar = mostrarAplicar && !ctx.ejecutando();

    return [
        m('button.btn-small.btn-flat.btn-default.pull-left',  {onclick : ctx.$modal.close}, [
            'Cancelar'
        ]),

        mostrarAplicar ? m('button.btn-small.btn-flat.btn-primary.pull-right', {onclick : ctx.ejecutarAccion}, [
            'Aplicar'
        ]) : null
    ];
}

function AplicarOperacionesContent (ctx) {
    var resultados = ctx.resultados();
    ctx.inicializar();
    if(ctx.inicializando()) {
        return oor.loading();
    }

    return [
        resultados.invalidas.length ? m('.alert.alert-warning', [
            resultados.invalidas.length, ' Operaciones No se pueden Aplicar '
        ]):null,

        resultados.validas.length ? [
            m('h5', 'Operaciones por Aplicar:'),

            resultados.validas.map(function (r) {
                return m('div.thmb-operacion', {},[
                    m('div', r.operacion.nombre_documento, ' ', r.operacion.serie, ' ', r.operacion.numero || '<auto>'),
                    m('.small.text-grey', r.referencia)
                ]);
            })

        ] : m('h5', 'No hay operaciones por aplicar')
    ];
}



function AplicarOperacionesController (args) {
    var ctx  = this;

    ctx.inicializando = m.prop(false);
    ctx.resultados = m.prop();
    ctx.ejecutando = m.prop(false);

    ctx.inicializar = function () {
        if(ctx.inicializando() || ctx.resultados()){
            return;
        }

        ctx.inicializando(true);

        oor.request('/operaciones/accion/aplicar', 'POST', {
            data : {operaciones : args.operaciones}
        })
        .then(function (r) {
            return {
                validas : r.filter( function (d) { return d.accion.puede }),
                invalidas : r.filter( function (d) { return !d.accion.puede })
            };
        })
        .then(ctx.resultados)
        .then(ctx.inicializando.bind(ctx, false));
    };


    ctx.ejecutarAccion = function () {
        if(ctx.ejecutando()) {
            return;
        }
        var operaciones = ctx.resultados().validas.map( function (d) {
            return d.operacion.operacion_id
        });

        ctx.ejecutando(true);

        oor.request('/operaciones/accion/aplicar/ejecutar', 'POST', {
            data : {operaciones : operaciones}
        }).then(function (resultados) {

            resultados.map(function (r) {
                console.log(r.accion)
            });


            ctx.$modal.close();
            toastr.success('¡Cambios Realizados!');

        }).then(function (d) {
            nh.isFunction(args.onEjecutar) && args.onEjecutar()
        })
    }
}

},{}],30:[function(require,module,exports){
module.exports = RegistrarPago;

function RegistrarPago (ctx) {
    return function () {
        if(ctx.$sel.length == 0) {
            return toastr.error('Debes seleccionar al menos una operación');
        }
        var argumentosModal =  {
            operaciones : ctx.$sel,
            onEjecutar : ctx.reload.bind(ctx)
        };

        argumentosModal['VTA'] = 'ingreso';
        argumentosModal['VNC'] = 'egreso';
        argumentosModal['COM'] = 'egreso';
        argumentosModal['CNC'] = 'ingreso';
        argumentosModal.seccion = ctx.seccion;

        MTModal.open({
            args : argumentosModal,
            controller : RegistrarPagoController,
            //top : AplicarOperacionesTop,
            content : RegistrarPagoView,
            bottom : RegistrarPagoBottom,
            el : this,
            modalAttrs : {
                'class' : 'modal-small'
            }
        });
    }
}

function RegistrarPagoController (args) {
    var ctx = this;

    ctx.resultados = m.prop();
    ctx.loading = m.prop();
    ctx.seccion = args.seccion;


    ctx.initialize = function () {
        if(ctx.resultados() || ctx.loading()) {
            return;
        }

        oor.request('/operaciones/accion/pagar', 'POST', {
            data : {operaciones :args.operaciones}
        })
        .then(function (data) {
            var resultados = {ingreso : [], egreso : [], noPuede : []};
            data.forEach(function (r) {
                var key = args[r.operacion.tipo_operacion];
                if(r.accion.puede && resultados[key]) {
                    resultados[key].push(r.operacion);
                } else {
                    resultados.noPuede.push(r.operacion);
                }
            });
            return ctx.resultados(resultados);

        })
        .then(function (resultados) {
            var definir = false;
            var postUrl = '?movimientos=';

            resultados.ingreso.ids = resultados.ingreso.map( f('operacion_id') );
            resultados.egreso.ids = resultados.egreso.map( f('operacion_id') );

            resultados.ingreso.url = '/movimientos/ingreso'.concat(
                '?movimientos=',
                resultados.ingreso.ids.join(',')
            );

            resultados.egreso.url = '/movimientos/egreso'.concat(
                '?movimientos=',
                resultados.egreso.ids.join(',')
            );

            if(resultados.ingreso.length && !resultados.egreso.length) {
                definir = 'ingreso';
            } else if(resultados.egreso.length && !resultados.ingreso.length) {
                definir = 'egreso';
            }

            if(definir) {
                window.location = resultados[definir].url;
            }

        })
        .then(ctx.loading.bind(null,false))

    }
}

function RegistrarPagoView (ctx) {
    var resultados = ctx.resultados();
    ctx.initialize();

    if(!resultados) return oor.loading();

    return m('div',[
        resultados.noPuede.length ? m('div.alert.alert-warning', [
            resultados.noPuede.length, ' Operaciones no se pueden pagar'
        ]) : '',

        m('h5', 'Selecciona una opción:'),

        m('ul', [
            resultados.egreso.length ? m('li', [
                m('a', {href : resultados.egreso.url }, [
                   'Registrar egreso para ',
                   resultados.egreso.length,
                   ' ',
                   ctx.seccion() == 'V' ? 'Notas de Crédito' : 'Facturas',
               ])
           ]) : null,

          resultados.egreso.length ? m('li', [
               m('a', {href : resultados.ingreso.url }, [
                   'Registrar ingreso para ',
                   resultados.ingreso.length,
                   ' ',
                    ctx.seccion() == 'V' ? 'Compras' : 'Notas de Crédito de Compra',
               ])
          ]) : null
        ])
    ]);
}


function RegistrarPagoBottom (ctx) {
    return m('button..btn-sm.btn.pull-left.btn-default', {onclick:ctx.$modal.close}, [
        'Cancelar'
    ]);
}

},{}],31:[function(require,module,exports){
module.exports = ActionsOperaciones;

var Aplicar = require('./acciones/AplicarAccion.js');
var RegistrarPago =  require('./acciones/RegistrarPagoAccion.js');

function ActionsOperaciones (ctx) {
    var selection = ctx.$sel.selection();

    return m('.row', {style:'padding: 10px 0'}, [
        m('.col-sm-12', [
            /*m('button.btn.btn-default.btn-sm.btn-flat.disabled', [
                selection.length ? String(selection.length).concat(' Operaciones seleccionadas:')  : null
            ]),*/
            m('button.btn.btn-default.btn-sm.btn-flat', {
                style : 'margin-right:5px',
                onclick : Aplicar(ctx),
            }, [m('i.ion-checkmark-round.text-green') ,' Aplicar']),

             m('button.btn.btn-default.btn-sm.btn-flat', {
                onclick : RegistrarPago(ctx),
            }, [m('i.ion-cash.text-indigo') ,' Registrar Pago'])
        ])
    ]);
}

},{"./acciones/AplicarAccion.js":29,"./acciones/RegistrarPagoAccion.js":30}],32:[function(require,module,exports){
module.exports = {view:ListadoView, controller:ListadoController};

var SearchBar  = require('../../misc/components/SearchBar');
var FechasBar  = require('../../misc/components/FechasBar2');
var TablaOperaciones = require('./tablaOperaciones');
var ActionsOperaciones = require('./actionsOperaciones');

function ListadoController (params) {
    var ctx = this;
    var search = m.prop('');
    var queryParams = m.route.parseQueryString(location.search);
    oor.fondoGris();

    this.seccion = m.prop(params.seccion);
    this.loading = m.prop(false);
    this.ready = m.prop(false);
    this.tabs = m.prop([]);
    this.search = m.prop('');
    this.tab = m.prop();
    this.sortDirection = m.prop('+');
    this.total = m.prop();
    this.offset = m.prop();
    this.count = m.prop();
    this.limit = m.prop(50);
    this.sort = m.prop();
    this.operaciones = m.prop();

    ctx.idAccion    = m.prop(this.seccion() == 'compras' ? 'Compra' : 'Venta');
    ctx.principal   = m.prop(this.seccion() == 'compras' ? 'com'    : 'vta');
    ctx.notaCredito = m.prop(this.seccion() == 'compras' ? 'cnc'    : 'vnc');

    //Extra Params
    ctx.tTerceroId      = m.prop(queryParams.tTerceroId);
    ctx.opVendedorId    = m.prop(queryParams.opVendedorId);
    ctx.opSucursalId    = m.prop(queryParams.opSucursalId);
    ctx.opMoneda        = m.prop(queryParams.opMoneda);
    ctx.opTipoOperacion = m.prop(queryParams.opTipoOperacion);


    ctx.searchBar = new SearchBar.controller({
        search : search,
        onsearch : function () {
            if(ctx.search() != search()) {
                ctx.search(search());
                ctx.offset(0);
                ctx.ready(false);
                m.redraw();
            }
        }
    });

    ctx.fechasBar = new FechasBar.controller({
        onchange : function () {
            ctx.offset(0);
            ctx.ready(false);
            //m.redraw();
        }
    });


    if(!queryParams.fechaDesde && !queryParams.fechaHasta) {
        ctx.fechasBar.esteAno();
    } else {
        if(queryParams.fechaDesde) ctx.fechasBar.fechaDesde(queryParams.fechaDesde);
        if(queryParams.fechaHasta) ctx.fechasBar.fechaHasta(queryParams.fechaHasta);
    }



    this.initialize = function () {
        if(this.ready() || this.loading()) return;
        this.loading(true);

        oorden().then(function () {
            TablaOperaciones(ctx);

            oor.request('/operaciones/listado/'.concat(ctx.seccion()), 'GET', {
                data : {
                    busqueda : ctx.search() ? ctx.search() : undefined,
                    tab : ctx.tab(),
                    order : ctx.sort() ? ctx.sortDirection() + ctx.sort() : undefined,
                    offset : ctx.offset() ? ctx.offset() : undefined,
                    limit : ctx.limit() ? ctx.limit() : undefined,
                    fecha_desde : ctx.fechasBar.value.fechaDesde() ? ctx.fechasBar.value.fechaDesde() : undefined,
                    fecha_hasta : ctx.fechasBar.value.fechaHasta() ? ctx.fechasBar.value.fechaHasta() : undefined,
                    tTerceroId : ctx.tTerceroId() ? ctx.tTerceroId() : undefined,
                    opVendedorId : ctx.opVendedorId() ? ctx.opVendedorId() : undefined,
                    opSucursalId : ctx.opSucursalId() ? ctx.opSucursalId() : undefined,
                    opMoneda : ctx.opMoneda() ? ctx.opMoneda() : undefined,
                    opTipoOperacion : ctx.opTipoOperacion() ? ctx.opTipoOperacion() : undefined
                }
            })
            .then(asignarDatos)
        });
    };


    this.reload = function () {
        this.ready(false);
        this.initialize();
    }

    function asignarDatos (data) {
        ctx.loading(false)
        ctx.ready(true)

        Object.keys(data).forEach(function(key) {
            var prop = ctx[key];
            if(nh.isFunction(prop)) prop(data[key]);
        });
    }


}

function ListadoView (ctx) {
    ctx.initialize();


    return oor.panel({
        title : ctx.seccion().toUpperCase(),
        buttons : [
            oor.stripedButton('a.btn-success', 'Nueva '.concat(ctx.idAccion()), {
                style : 'margin-right:18px',
                href:'/operaciones/'.concat(ctx.principal().concat('/crear')),

            }),

            oor.stripedButton('a.btn-warning', 'Nueva Devolución', {
                style : 'margin-right:18px',
                href:'/operaciones/'.concat(ctx.notaCredito().concat('/crear')),

            }),

            /*
            oor.dropdown({
                button : oor.stripedButton('button.btn-primary', ['Exportar', m('span.caret')]),
                options : [
                    m('a#make-pdf[href=javascript:;]', {}, [m('i.fa fa-file-pdf-o')," PDF"]),
                    m('a#make-csv[href=javascript:;]', {}, [m('i.fa fa-file-csv-o')," CSV"]),
                ]
            })
            */

            m('.btn-group', [
                m('.btn btn-primary button-striped button-full-striped button-striped btn-default btn-xs dropdown-toggle btn-ripple ', {'data-toggle':'dropdown'},
                    ['Exportar  ',m("span.caret"),{style:'width:30px'}]
                ),

                m("ul.dropdown-menu.dropdown-menu-right",{style:'color: #21618c ;font-size:14px;'},[
                    m("li", m('a#make-pdf-operaciones', {
                        href:'javascript:', modelo : 'Operaciones',
                        vista : 'operaciones-multi'}, [
                            m('i.fa fa-file-pdf-o')," PDF"
                        ])
                    ),

                    m("li", m('a#make-csv-operaciones',{href:'javascript:', modelo : 'Operaciones', vista : 'operaciones-multi'},  [
                            m('i.fa fa-file-excel-o')," Excel"
                        ])
                    )
                ])
            ])
        ]
    }, [
        m('.row', [
            m('.col-md-8', FechasBar.view(ctx.fechasBar)),
            m('.col-md-4', [
                m('.flex-row', [
                    SearchBar.view(ctx.searchBar)
                ])
            ]),
        ]),

        ListadoTabs(ctx),

        ctx.loading() ? oor.loading() : m('.table-responsive', [
             m('table.table', {config: TablaOperaciones.config(ctx) })
        ])
    ]);


}

function ListadoTabs (ctx) {
    return m('ul.nav.nav-tabs', [
        ctx.tabs().map(function (tab) {
            var klass = tab.name == ctx.tab() ? 'active' : '';
            return m('li', {'class':klass, onclick:selectTab(ctx,tab),filtro:tab.name}, [
                m('a', {href:'javascript:'}, [
                    tab.caption,
                    ' ',
                    tab.badge ? m('span.t.small', {style:'border-radius:12px; background:#f0f0f0; padding:4px; width:2em'}, tab.badge) : null
                ])
            ]);
        })
    ]);
}

function selectTab(ctx, tab) {
    return function () {
        if(ctx.tab() == tab.name) return;
        ctx.tab(tab.name);
        ctx.ready(false);
        m.redraw();
    }
}

},{"../../misc/components/FechasBar2":8,"../../misc/components/SearchBar":9,"./actionsOperaciones":31,"./tablaOperaciones":33}],33:[function(require,module,exports){
module.exports = TablaOperaciones;

var Table = require('../../nahui/dtable');
var Field = require('../../nahui/nahui.field');
var columns;
var fields = {};

var estatuses = m.prop([
    { estatus : 'P', nombre : 'EN PREPARACIÓN', color : 'grey', nombre_corto: 'Borrador'},
    { estatus : 'T', nombre : 'PENDIENTE',      color : 'amber', nombre_corto: 'Finalizada'},
    { estatus : 'A', nombre : 'APLICADA',       color :null, nombre_corto : 'Aplicada'},
    { estatus : 'S', nombre : 'SALDADA',        color : 'green'  , nombre_corto : 'Saldada'},
    { estatus : 'X', nombre : 'CANCELADA',      color : 'red', nombre_corto : 'Cancelada'},
]);

estatuses().forEach(function (t) {
    estatuses[t.estatus] = m.prop(t);
});

function TablaOperaciones (ctx) {
    if(ctx.table){
        return;
    }

    // Aquí se crea una tabla, le tenemos que pasar a la tabla una función que
    // recupere el id de un registro, como el id está en la propiedad opOperacionId
    // f('opOperacionId') crea una función que devuelve la propiedad opOperacionId de cualquier objeto.
    // Ejemplo: f('opOperacionId')({ opOperacionId : 99}) == 99
    ctx.table = Table().key( f('opOperacionId') );
    ctx.table.mounted = m.prop(false);
    ctx.table.rows = m.prop()
    ctx.table.redraw = m.prop(false)

    var fields = {};

    //La función Field crea un campo para las tablas,
    //Acepta dos parametros el name y la configuración del Field
    //por default el nombre es la propiedad a la que se hace referencia
    fields.opOperacionId = Field('opOperacionId', {});


    //Si le ponemos un caption se edita el encabezado del renglón
    fields.opFecha = Field('opFecha', {
        caption : 'Fecha',
        filter :  oor.fecha.prettyFormat
    });


    // Si le ponemos una class le asignamos la clase que llevan las celdas de datos
    // Si queremos editar la clase que le ponemos a las celdas de titulo es headingClass
    // Filter: es para darle formato a los campos
    fields.opUpdatedAt = Field('opUpdatedAt', {
        caption : ' ',
        'class' : 'text-grey small',
        filter : function (d) { return d.split(' ').join('T').concat('.000Z');},
        update : function (d) { d.attr('x-time-till-now', f('text')); }
    });

    //Field.twice ayuda a crear campos conjuntos, en la tabla de operaciones,
    //Casi todos los campos son conjuntos (tienen más de un dato)
    //En subfields hay que pasarle un array con los datos que vamos a poner
    fields.fechas = Field.twice('fechas', {
        subfields : [fields.opFecha, fields.opUpdatedAt]
    });


    //Field.linkToResource  crea un vínculo
    //hay que pasar la function url que va a devolver lo que queremos poner en href
    fields.opTitulo = Field.linkToResource('opTitulo', {
        url : function (d) {
            return '/operaciones/'.concat(d.opTipoOperacion.toLowerCase(), '/', d.opOperacionId);
        },
        caption : 'Documento',
        'class' : 'text-indigo'
    });

    fields.opReferencia = Field('opReferencia', {
        caption : 'Referencia',
        'class' : 'text-grey small'
    });

    fields.operacion = Field.twice('operacion', {
        subfields : [fields.opTitulo, fields.opReferencia]
    });

    fields.tNombre = Field('tNombre',{
        'caption' : ctx.seccion() == 'ventas' ? 'Cliente' : 'Proveedor',
    });

    fields.tercero = fields.tNombre;


    fields.opTotal = Field('opTotal', {
        caption : 'Total',
        'class' : '',
        filter: oorden.organizacion.format,
        headingClass:  ''
    });

    fields.opMoneda = Field('opMoneda', {
        caption : 'Moneda',
        'class' : 'text-grey small',
        headingClass:  ''
    });

    fields.totalMoneda = Field.twice('totalMoneda', {
        subfields : [fields.opTotal, fields.opMoneda]
    });


    var opEstatuses = estatuses().filter(f('color'));

    fields.opSaldo = Field('opSaldo', {
        caption : 'Saldo',
        'class': 'text-right',
        filter: oorden.organizacion.format,
        headingClass:  'text-right',

        update : function (selection) {
            selection.text(function (d) {
                var opEstatus = d.row.opEstatus;
                if(opEstatus == 'A') return oorden.org.format(d.row.opSaldo)
                var status = estatuses[opEstatus];
                return status ? status().nombre_corto : '';
            });

            opEstatuses.forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) {
                    return d.row.opEstatus == st.estatus;
                });
            });
        }
    });




    fields.opEstatus = Field('opEstatus', {
        caption : 'Estatus',
        'class': '',
        headingClass:  '',

        filter :  function (st) {
            var status = oor.mm('Operacion').estatuses[st];
            return status ? status().nombre_corto : '';
        },

        update : function (selection) {

            selection.text(function (d) {
                return d.value == 'A' ? oorden.org.format(d.row.opSaldo) : d.text;
            });


            opEstatuses.forEach(function (st) {
                selection.classed('text-'.concat(st.color), function (d) { console.log(); return d.value == st.estatus; });
            });
        }
    });

    fields.saldoEstatus = fields.opEstatus;
    /*
    fields.saldoEstatus = Field.twice('saldoEstatus', {
        subfields : [fields.opSaldo, fields.opEstatus]7
    });
    */

    ctx.$sel = [];

    ctx.$sel.isSelected = function (id) {
        return ctx.$sel.indexOf(id) > -1;
    }

    ctx.$sel.select = function (id) {
        if(ctx.$sel.isSelected(id)) return;
        ctx.$sel.push(id);
    }

    ctx.$sel.unselect = function (id) {
        var idx = ctx.$sel.indexOf(id)
        if(idx == -1) return;
        ctx.$sel.splice(idx,1);
    }

    ctx.$sel.selection = function (s) {
        return ctx.$sel;
    }


    fields.cfdi = Field('opFolioFiscal', {
        caption : 'cfdi',
        class : 'small',
        headingClass : 'text-center',
        'class' : 'text-center small',
        enter : function (sel) {
            sel.call(Field.enter);

            sel.append('a')
                .attr('class', 'text-indigo cfdi-xml')
                .style('margin', '2px').text('XML');
        },

        update : function (sel) {
            sel.selectAll('a').style('display',  function (d) { return d.value ? undefined : 'none' });
            sel.select('.cfdi-xml').attr('href', downloadCFDI('xml') )
        }
    })


    function downloadCFDI(ext) {
        return function (d) {
            return '/facturacion/getXML/' + d.row.opFolioFiscal;
        }
    }

    fields.checkers = Field('checkers', {
        value : f('opOperacionId'),
        caption : ' ',
        enterHeading : function (sel) {
            sel.call(Field.enterHeading);

            sel.append('input')
                .attr('type', 'checkbox')
                .attr('value', f('value'))
                .on('change', function (d) {

                    var checked = d3.select(this).property('checked');

                    ctx.table.rows()
                        .map(f('opOperacionId'))
                        .map(ctx.$sel[checked ? 'select' : 'unselect'])

                    ctx.table.redraw(true);
                    m.redraw();
                });
        },
        enter : function (sel) {
            sel.call(Field.enter);
            sel.append('input')
                .attr('type', 'checkbox')
                .attr('value', f('value'))
                .on('change', function (d) {
                    if(d3.select(this).property('checked') == true) {
                        ctx.$sel.select(d.value);
                    } else {
                        ctx.$sel.unselect(d.value)
                    }

                    ctx.table.redraw(true);
                    m.redraw();
                })
        },
        update : function (sel) {
            sel.select('input').property('checked', function (d) { return ctx.$sel.isSelected(d.value); })
        }
    });

    ctx.table.rowUpdate(function (sel) {
        sel.classed('info', function (d) {
            return ctx.$sel.isSelected( d.opOperacionId )
        })
    });

    ctx.table.columns = [
        fields.checkers,
        fields.opFecha,
        fields.operacion,
        //fields.opReferencia,
        fields.tercero,
        fields.opMoneda,
        fields.opTotal,
        fields.saldoEstatus
    ];


    ctx.table.columns = [
        fields.checkers,
        //fields.opUpdatedAt,
        fields.opFecha,
        //fields.fechas,
        //fields.operacion,
        fields.opTitulo,
        fields.opReferencia,
        fields.tercero,
        fields.totalMoneda,
        fields.opSaldo
    ];

    if(oorden.org().codigo_pais === 'MX') {
        ctx.table.columns.push(fields.cfdi);
    }
}


TablaOperaciones.config = function (ctx) {
    return function (element, isInitialized) {
        if( ctx.table.mounted() ) return;

        if(ctx.table.redraw() == true || (ctx.operaciones() && ctx.operaciones() != ctx.table.rows()) ) {
            ctx.table.rows( ctx.operaciones() );
            ctx.table.redraw(false);

            d3.select(element).call(ctx.table, ctx.table.columns, ctx.table.rows(), {
                sort : ctx.sort,
                sortDirection : ctx.sortDirection,
                total : ctx.total(),
                offset : ctx.offset(),
                pageSize : ctx.limit()
            });

            oor.time.update(element)
        }

        if(isInitialized) return;


        $(element).on('click', '[data-sort-by]', function (ev) {
            var sort = $(this).attr('data-sort-by');
            if(ctx.sort() == sort) {
                ctx.sortDirection(ctx.sortDirection() == '+' ? '-' : '+')
            }

            ctx.sort(sort);
            ctx.ready(false);
            m.redraw()
        });


        $(element).on('click', '[data-page-prev]', function () {
            var offset = Number(ctx.offset()) - Number(ctx.limit());
            if(offset < 0) return;

            ctx.offset(offset);
            ctx.ready(false);
            m.redraw();
        });


        $(element).on('click', '[data-page-next]', function () {
            var offset = Number(ctx.offset()) + Number(ctx.limit()) ;
            if(offset >= ctx.total()) return;

            ctx.offset(offset);
            ctx.ready(false);
            m.redraw();
        });

    }
}

},{"../../nahui/dtable":12,"../../nahui/nahui.field":13}],34:[function(require,module,exports){
var verOperacion        = module.exports = {};
var FechaInput          = require('../../inputs/fechaInput');
var EncabezadoOperacion = require('../componentes/EncabezadoOperacion.js');
var inter               = require('../componentes/InterComponentes.js');


verOperacion.oninit = function (vnode) {
    var idOperacion = 'ff9067fa-9c35-3f43-dc59-44bd8c5dc0c7';

    this.loading          = m.prop(false);
    this.operacion        = m.prop();
    this.componenteActivo = m.prop();

    this.initialize = function () {
        if(this.loading() || this.operacion()) return;

        this.loading(true);

        oor.request('/operacion/vta/' + idOperacion).run(function (r) {
            vnode.state.operacion(r);
            vnode.state.loading(false);
            vnode.state.terceroId = m.prop(r.tercero_id);
        });
    };
}

verOperacion.view = function (vnode) {
    this.initialize();
    var operacion = this.operacion();

    return oor.panel({
        title   : this.heading(),
        inter   : this.interComponente(vnode),
        buttons : this.loading() ? null : this.buttons()
    }, [
        this.loading() ? oor.loading() : '',

        this.loading() ? null : [

            //Encabezado de la operación
            m(EncabezadoOperacion, {
                terceroId : vnode.state.terceroId,
                operacion : operacion
            }),

            m('br'),

            m('.clear'),

            m('h4', {style:'margin-top:10px'}, 'Conceptos:'),
            m('br'),


            /*
            m('.row', [
                oor.stripedButton('button.pull-right.btn-primary', [
                    m('i.ion-archive'), ' Guardar'
                ]),

                m('.btn-group.pull-right', {style: 'margin:0 40px'}, [

                    oor.stripedButton('button.btn-success', [
                        m('i.ion-checkmark-round'), ' Aplicar'
                    ]),

                    oor.stripedButton('button.btn-success', [
                        m('i.fa.fa-file-code-o'), ' Timbrar CFDI'
                    ]),
                ])
            ]),

            m('row', [
                m('label.pull-right', [
                    m('input[type=checkbox]'), '... y regresar a captura'
                ])
            ])

            */
        ]
    ]);
}


verOperacion.org = oorden.org;

verOperacion.interComponente = function () {
    if(!this.componenteActivo()) return null;
    return m(this.componenteActivo(), {
        close      : this.componenteActivo.bind(null,false),
        operacion  : this.operacion()
    });
};


verOperacion.btnComponente = function (aComponent) {
    return m('button.btn.btn-flat.btn-xs', {
        onclick : this.componenteActivo.bind(null, aComponent),
        'class' : this.componenteActivo() == aComponent ? 'btn-primary' : 'btn-default'
    }, aComponent.btnLabel);
}

verOperacion.buttons = function () {
    return [
        m('div.btn-group',[
            this.org().codigo_pais == 'MX' ? [
                this.operacion().folio_fiscal ? this.btnComponente(inter.cfdi) : 'this.btnTimbrarCfdi()'
            ] : null,
            this.btnComponente(inter.archivos),
            this.btnComponente(inter.bitacora),
            this.btnComponente(inter.notas)
        ]),

        m('div.btn-group',[
            oor.stripedButton('button.btn-primary.btn-xs', ['Acciones ', m('span.caret')], {})
        ])
    ];
}

verOperacion.heading = function () {
    if(! this.operacion() ) return null;

    return [
        m('h4.pull-left', {style:'margin:0'}, [
            this.operacion().tipoDocumento.nombre + ' ',
            m('span',{style:'color:#aaa; font-size:0.7em'}, ' Serie: '),
            m('span', this.operacion().serie),
            m('span',{style:'color:#aaa; font-size:0.7em'},' # '),
            m('span', this.operacion().numero),
            m('span',{style:'color:#aaa; font-size:0.7em'},' Fecha: '),
            m('span', oor.fecha.prettyFormat.year(this.operacion().fecha))
        ]),
    ];
}

},{"../../inputs/fechaInput":5,"../componentes/EncabezadoOperacion.js":25,"../componentes/InterComponentes.js":26}],35:[function(require,module,exports){
/*!
	Papa Parse
	v4.1.2
	https://github.com/mholt/PapaParse
*/
(function(global)
{
	"use strict";

	console.log(global);

	var IS_WORKER = !global.document && !!global.postMessage,
		IS_PAPA_WORKER = IS_WORKER && /(\?|&)papaworker(=|&|$)/.test(global.location.search),
		LOADED_SYNC = false, AUTO_SCRIPT_PATH;
	var workers = {}, workerIdCounter = 0;

	var Papa = {};

	Papa.parse = CsvToJson;
	Papa.unparse = JsonToCsv;

	Papa.RECORD_SEP = String.fromCharCode(30);
	Papa.UNIT_SEP = String.fromCharCode(31);
	Papa.BYTE_ORDER_MARK = "\ufeff";
	Papa.BAD_DELIMITERS = ["\r", "\n", "\"", Papa.BYTE_ORDER_MARK];
	Papa.WORKERS_SUPPORTED = !IS_WORKER && !!global.Worker;
	Papa.SCRIPT_PATH = null;	// Must be set by your code if you use workers and this lib is loaded asynchronously

	// Configurable chunk sizes for local and remote files, respectively
	Papa.LocalChunkSize = 1024 * 1024 * 10;	// 10 MB
	Papa.RemoteChunkSize = 1024 * 1024 * 5;	// 5 MB
	Papa.DefaultDelimiter = ",";			// Used if not specified and detection fails

	// Exposed for testing and development only
	Papa.Parser = Parser;
	Papa.ParserHandle = ParserHandle;
	Papa.NetworkStreamer = NetworkStreamer;
	Papa.FileStreamer = FileStreamer;
	Papa.StringStreamer = StringStreamer;

	if (typeof module !== 'undefined' && module.exports)
	{
		// Export to Node...
		module.exports = Papa;
	}
	else if (isFunction(global.define) && global.define.amd)
	{
		// Wireup with RequireJS
		define(function() { return Papa; });
	}
	else
	{
		// ...or as browser global
		global.Papa = Papa;
	}

	if (global.jQuery)
	{
		var $ = global.jQuery;
		$.fn.parse = function(options)
		{
			var config = options.config || {};
			var queue = [];

			this.each(function(idx)
			{
				var supported = $(this).prop('tagName').toUpperCase() == "INPUT"
								&& $(this).attr('type').toLowerCase() == "file"
								&& global.FileReader;

				if (!supported || !this.files || this.files.length == 0)
					return true;	// continue to next input element

				for (var i = 0; i < this.files.length; i++)
				{
					queue.push({
						file: this.files[i],
						inputElem: this,
						instanceConfig: $.extend({}, config)
					});
				}
			});

			parseNextFile();	// begin parsing
			return this;		// maintains chainability


			function parseNextFile()
			{
				if (queue.length == 0)
				{
					if (isFunction(options.complete))
						options.complete();
					return;
				}

				var f = queue[0];

				if (isFunction(options.before))
				{
					var returned = options.before(f.file, f.inputElem);

					if (typeof returned === 'object')
					{
						if (returned.action == "abort")
						{
							error("AbortError", f.file, f.inputElem, returned.reason);
							return;	// Aborts all queued files immediately
						}
						else if (returned.action == "skip")
						{
							fileComplete();	// parse the next file in the queue, if any
							return;
						}
						else if (typeof returned.config === 'object')
							f.instanceConfig = $.extend(f.instanceConfig, returned.config);
					}
					else if (returned == "skip")
					{
						fileComplete();	// parse the next file in the queue, if any
						return;
					}
				}

				// Wrap up the user's complete callback, if any, so that ours also gets executed
				var userCompleteFunc = f.instanceConfig.complete;
				f.instanceConfig.complete = function(results)
				{
					if (isFunction(userCompleteFunc))
						userCompleteFunc(results, f.file, f.inputElem);
					fileComplete();
				};

				Papa.parse(f.file, f.instanceConfig);
			}

			function error(name, file, elem, reason)
			{
				if (isFunction(options.error))
					options.error({name: name}, file, elem, reason);
			}

			function fileComplete()
			{
				queue.splice(0, 1);
				parseNextFile();
			}
		}
	}


	if (IS_PAPA_WORKER)
	{
		global.onmessage = workerThreadReceivedMessage;
	}
	else if (Papa.WORKERS_SUPPORTED)
	{
		AUTO_SCRIPT_PATH = getScriptPath();

		// Check if the script was loaded synchronously
		if (!document.body)
		{
			// Body doesn't exist yet, must be synchronous
			LOADED_SYNC = true;
		}
		else
		{
			document.addEventListener('DOMContentLoaded', function () {
				LOADED_SYNC = true;
			}, true);
		}
	}




	function CsvToJson(_input, _config)
	{
		_config = _config || {};

		if (_config.worker && Papa.WORKERS_SUPPORTED)
		{
			var w = newWorker();

			w.userStep = _config.step;
			w.userChunk = _config.chunk;
			w.userComplete = _config.complete;
			w.userError = _config.error;

			_config.step = isFunction(_config.step);
			_config.chunk = isFunction(_config.chunk);
			_config.complete = isFunction(_config.complete);
			_config.error = isFunction(_config.error);
			delete _config.worker;	// prevent infinite loop

			w.postMessage({
				input: _input,
				config: _config,
				workerId: w.id
			});

			return;
		}

		var streamer = null;
		if (typeof _input === 'string')
		{
			if (_config.download)
				streamer = new NetworkStreamer(_config);
			else
				streamer = new StringStreamer(_config);
		}
		else if ((global.File && _input instanceof File) || _input instanceof Object)	// ...Safari. (see issue #106)
			streamer = new FileStreamer(_config);

		return streamer.stream(_input);
	}






	function JsonToCsv(_input, _config)
	{
		var _output = "";
		var _fields = [];

		// Default configuration

		/** whether to surround every datum with quotes */
		var _quotes = false;

		/** delimiting character */
		var _delimiter = ",";

		/** newline character(s) */
		var _newline = "\r\n";

		unpackConfig();

		if (typeof _input === 'string')
			_input = JSON.parse(_input);

		if (_input instanceof Array)
		{
			if (!_input.length || _input[0] instanceof Array)
				return serialize(null, _input);
			else if (typeof _input[0] === 'object')
				return serialize(objectKeys(_input[0]), _input);
		}
		else if (typeof _input === 'object')
		{
			if (typeof _input.data === 'string')
				_input.data = JSON.parse(_input.data);

			if (_input.data instanceof Array)
			{
				if (!_input.fields)
					_input.fields = _input.data[0] instanceof Array
									? _input.fields
									: objectKeys(_input.data[0]);

				if (!(_input.data[0] instanceof Array) && typeof _input.data[0] !== 'object')
					_input.data = [_input.data];	// handles input like [1,2,3] or ["asdf"]
			}

			return serialize(_input.fields || [], _input.data || []);
		}

		// Default (any valid paths should return before this)
		throw "exception: Unable to serialize unrecognized input";


		function unpackConfig()
		{
			if (typeof _config !== 'object')
				return;

			if (typeof _config.delimiter === 'string'
				&& _config.delimiter.length == 1
				&& Papa.BAD_DELIMITERS.indexOf(_config.delimiter) == -1)
			{
				_delimiter = _config.delimiter;
			}

			if (typeof _config.quotes === 'boolean'
				|| _config.quotes instanceof Array)
				_quotes = _config.quotes;

			if (typeof _config.newline === 'string')
				_newline = _config.newline;
		}


		/** Turns an object's keys into an array */
		function objectKeys(obj)
		{
			if (typeof obj !== 'object')
				return [];
			var keys = [];
			for (var key in obj)
				keys.push(key);
			return keys;
		}

		/** The double for loop that iterates the data and writes out a CSV string including header row */
		function serialize(fields, data)
		{
			var csv = "";

			if (typeof fields === 'string')
				fields = JSON.parse(fields);
			if (typeof data === 'string')
				data = JSON.parse(data);

			var hasHeader = fields instanceof Array && fields.length > 0;
			var dataKeyedByField = !(data[0] instanceof Array);

			// If there a header row, write it first
			if (hasHeader)
			{
				for (var i = 0; i < fields.length; i++)
				{
					if (i > 0)
						csv += _delimiter;
					csv += safe(fields[i], i);
				}
				if (data.length > 0)
					csv += _newline;
			}

			// Then write out the data
			for (var row = 0; row < data.length; row++)
			{
				var maxCol = hasHeader ? fields.length : data[row].length;

				for (var col = 0; col < maxCol; col++)
				{
					if (col > 0)
						csv += _delimiter;
					var colIdx = hasHeader && dataKeyedByField ? fields[col] : col;
					csv += safe(data[row][colIdx], col);
				}

				if (row < data.length - 1)
					csv += _newline;
			}

			return csv;
		}

		/** Encloses a value around quotes if needed (makes a value safe for CSV insertion) */
		function safe(str, col)
		{
			if (typeof str === "undefined" || str === null)
				return "";

			str = str.toString().replace(/"/g, '""');

			var needsQuotes = (typeof _quotes === 'boolean' && _quotes)
							|| (_quotes instanceof Array && _quotes[col])
							|| hasAny(str, Papa.BAD_DELIMITERS)
							|| str.indexOf(_delimiter) > -1
							|| str.charAt(0) == ' '
							|| str.charAt(str.length - 1) == ' ';

			return needsQuotes ? '"' + str + '"' : str;
		}

		function hasAny(str, substrings)
		{
			for (var i = 0; i < substrings.length; i++)
				if (str.indexOf(substrings[i]) > -1)
					return true;
			return false;
		}
	}

	/** ChunkStreamer is the base prototype for various streamer implementations. */
	function ChunkStreamer(config)
	{
		this._handle = null;
		this._paused = false;
		this._finished = false;
		this._input = null;
		this._baseIndex = 0;
		this._partialLine = "";
		this._rowCount = 0;
		this._start = 0;
		this._nextChunk = null;
		this.isFirstChunk = true;
		this._completeResults = {
			data: [],
			errors: [],
			meta: {}
		};
		replaceConfig.call(this, config);

		this.parseChunk = function(chunk)
		{
			// First chunk pre-processing
			if (this.isFirstChunk && isFunction(this._config.beforeFirstChunk))
			{
				var modifiedChunk = this._config.beforeFirstChunk(chunk);
				if (modifiedChunk !== undefined)
					chunk = modifiedChunk;
			}
			this.isFirstChunk = false;

			// Rejoin the line we likely just split in two by chunking the file
			var aggregate = this._partialLine + chunk;
			this._partialLine = "";

			var results = this._handle.parse(aggregate, this._baseIndex, !this._finished);
			
			if (this._handle.paused() || this._handle.aborted())
				return;
			
			var lastIndex = results.meta.cursor;
			
			if (!this._finished)
			{
				this._partialLine = aggregate.substring(lastIndex - this._baseIndex);
				this._baseIndex = lastIndex;
			}

			if (results && results.data)
				this._rowCount += results.data.length;

			var finishedIncludingPreview = this._finished || (this._config.preview && this._rowCount >= this._config.preview);

			if (IS_PAPA_WORKER)
			{
				global.postMessage({
					results: results,
					workerId: Papa.WORKER_ID,
					finished: finishedIncludingPreview
				});
			}
			else if (isFunction(this._config.chunk))
			{
				this._config.chunk(results, this._handle);
				if (this._paused)
					return;
				results = undefined;
				this._completeResults = undefined;
			}

			if (!this._config.step && !this._config.chunk) {
				this._completeResults.data = this._completeResults.data.concat(results.data);
				this._completeResults.errors = this._completeResults.errors.concat(results.errors);
				this._completeResults.meta = results.meta;
			}

			if (finishedIncludingPreview && isFunction(this._config.complete) && (!results || !results.meta.aborted))
				this._config.complete(this._completeResults);

			if (!finishedIncludingPreview && (!results || !results.meta.paused))
				this._nextChunk();

			return results;
		};

		this._sendError = function(error)
		{
			if (isFunction(this._config.error))
				this._config.error(error);
			else if (IS_PAPA_WORKER && this._config.error)
			{
				global.postMessage({
					workerId: Papa.WORKER_ID,
					error: error,
					finished: false
				});
			}
		};

		function replaceConfig(config)
		{
			// Deep-copy the config so we can edit it
			var configCopy = copy(config);
			configCopy.chunkSize = parseInt(configCopy.chunkSize);	// parseInt VERY important so we don't concatenate strings!
			if (!config.step && !config.chunk)
				configCopy.chunkSize = null;  // disable Range header if not streaming; bad values break IIS - see issue #196
			this._handle = new ParserHandle(configCopy);
			this._handle.streamer = this;
			this._config = configCopy;	// persist the copy to the caller
		}
	}


	function NetworkStreamer(config)
	{
		config = config || {};
		if (!config.chunkSize)
			config.chunkSize = Papa.RemoteChunkSize;
		ChunkStreamer.call(this, config);

		var xhr;

		if (IS_WORKER)
		{
			this._nextChunk = function()
			{
				this._readChunk();
				this._chunkLoaded();
			};
		}
		else
		{
			this._nextChunk = function()
			{
				this._readChunk();
			};
		}

		this.stream = function(url)
		{
			this._input = url;
			this._nextChunk();	// Starts streaming
		};

		this._readChunk = function()
		{
			if (this._finished)
			{
				this._chunkLoaded();
				return;
			}

			xhr = new XMLHttpRequest();
			
			if (!IS_WORKER)
			{
				xhr.onload = bindFunction(this._chunkLoaded, this);
				xhr.onerror = bindFunction(this._chunkError, this);
			}

			xhr.open("GET", this._input, !IS_WORKER);
			
			if (this._config.chunkSize)
			{
				var end = this._start + this._config.chunkSize - 1;	// minus one because byte range is inclusive
				xhr.setRequestHeader("Range", "bytes="+this._start+"-"+end);
				xhr.setRequestHeader("If-None-Match", "webkit-no-cache"); // https://bugs.webkit.org/show_bug.cgi?id=82672
			}

			try {
				xhr.send();
			}
			catch (err) {
				this._chunkError(err.message);
			}

			if (IS_WORKER && xhr.status == 0)
				this._chunkError();
			else
				this._start += this._config.chunkSize;
		}

		this._chunkLoaded = function()
		{
			if (xhr.readyState != 4)
				return;

			if (xhr.status < 200 || xhr.status >= 400)
			{
				this._chunkError();
				return;
			}

			this._finished = !this._config.chunkSize || this._start > getFileSize(xhr);
			this.parseChunk(xhr.responseText);
		}

		this._chunkError = function(errorMessage)
		{
			var errorText = xhr.statusText || errorMessage;
			this._sendError(errorText);
		}

		function getFileSize(xhr)
		{
			var contentRange = xhr.getResponseHeader("Content-Range");
			return parseInt(contentRange.substr(contentRange.lastIndexOf("/") + 1));
		}
	}
	NetworkStreamer.prototype = Object.create(ChunkStreamer.prototype);
	NetworkStreamer.prototype.constructor = NetworkStreamer;


	function FileStreamer(config)
	{
		config = config || {};
		if (!config.chunkSize)
			config.chunkSize = Papa.LocalChunkSize;
		ChunkStreamer.call(this, config);

		var reader, slice;

		// FileReader is better than FileReaderSync (even in worker) - see http://stackoverflow.com/q/24708649/1048862
		// But Firefox is a pill, too - see issue #76: https://github.com/mholt/PapaParse/issues/76
		var usingAsyncReader = typeof FileReader !== 'undefined';	// Safari doesn't consider it a function - see issue #105

		this.stream = function(file)
		{
			this._input = file;
			slice = file.slice || file.webkitSlice || file.mozSlice;

			if (usingAsyncReader)
			{
				reader = new FileReader();		// Preferred method of reading files, even in workers
				reader.onload = bindFunction(this._chunkLoaded, this);
				reader.onerror = bindFunction(this._chunkError, this);
			}
			else
				reader = new FileReaderSync();	// Hack for running in a web worker in Firefox

			this._nextChunk();	// Starts streaming
		};

		this._nextChunk = function()
		{
			if (!this._finished && (!this._config.preview || this._rowCount < this._config.preview))
				this._readChunk();
		}

		this._readChunk = function()
		{
			var input = this._input;
			if (this._config.chunkSize)
			{
				var end = Math.min(this._start + this._config.chunkSize, this._input.size);
				input = slice.call(input, this._start, end);
			}
			var txt = reader.readAsText(input, this._config.encoding);
			if (!usingAsyncReader)
				this._chunkLoaded({ target: { result: txt } });	// mimic the async signature
		}

		this._chunkLoaded = function(event)
		{
			// Very important to increment start each time before handling results
			this._start += this._config.chunkSize;
			this._finished = !this._config.chunkSize || this._start >= this._input.size;
			this.parseChunk(event.target.result);
		}

		this._chunkError = function()
		{
			this._sendError(reader.error);
		}

	}
	FileStreamer.prototype = Object.create(ChunkStreamer.prototype);
	FileStreamer.prototype.constructor = FileStreamer;


	function StringStreamer(config)
	{
		config = config || {};
		ChunkStreamer.call(this, config);

		var string;
		var remaining;
		this.stream = function(s)
		{
			string = s;
			remaining = s;
			return this._nextChunk();
		}
		this._nextChunk = function()
		{
			if (this._finished) return;
			var size = this._config.chunkSize;
			var chunk = size ? remaining.substr(0, size) : remaining;
			remaining = size ? remaining.substr(size) : '';
			this._finished = !remaining;
			return this.parseChunk(chunk);
		}
	}
	StringStreamer.prototype = Object.create(StringStreamer.prototype);
	StringStreamer.prototype.constructor = StringStreamer;



	// Use one ParserHandle per entire CSV file or string
	function ParserHandle(_config)
	{
		// One goal is to minimize the use of regular expressions...
		var FLOAT = /^\s*-?(\d*\.?\d+|\d+\.?\d*)(e[-+]?\d+)?\s*$/i;

		var self = this;
		var _stepCounter = 0;	// Number of times step was called (number of rows parsed)
		var _input;				// The input being parsed
		var _parser;			// The core parser being used
		var _paused = false;	// Whether we are paused or not
		var _aborted = false;   // Whether the parser has aborted or not
		var _delimiterError;	// Temporary state between delimiter detection and processing results
		var _fields = [];		// Fields are from the header row of the input, if there is one
		var _results = {		// The last results returned from the parser
			data: [],
			errors: [],
			meta: {}
		};

		if (isFunction(_config.step))
		{
			var userStep = _config.step;
			_config.step = function(results)
			{
				_results = results;

				if (needsHeaderRow())
					processResults();
				else	// only call user's step function after header row
				{
					processResults();

					// It's possbile that this line was empty and there's no row here after all
					if (_results.data.length == 0)
						return;

					_stepCounter += results.data.length;
					if (_config.preview && _stepCounter > _config.preview)
						_parser.abort();
					else
						userStep(_results, self);
				}
			};
		}

		/**
		 * Parses input. Most users won't need, and shouldn't mess with, the baseIndex
		 * and ignoreLastRow parameters. They are used by streamers (wrapper functions)
		 * when an input comes in multiple chunks, like from a file.
		 */
		this.parse = function(input, baseIndex, ignoreLastRow)
		{
			if (!_config.newline)
				_config.newline = guessLineEndings(input);

			_delimiterError = false;
			if (!_config.delimiter)
			{
				var delimGuess = guessDelimiter(input);
				if (delimGuess.successful)
					_config.delimiter = delimGuess.bestDelimiter;
				else
				{
					_delimiterError = true;	// add error after parsing (otherwise it would be overwritten)
					_config.delimiter = Papa.DefaultDelimiter;
				}
				_results.meta.delimiter = _config.delimiter;
			}

			var parserConfig = copy(_config);
			if (_config.preview && _config.header)
				parserConfig.preview++;	// to compensate for header row

			_input = input;
			_parser = new Parser(parserConfig);
			_results = _parser.parse(_input, baseIndex, ignoreLastRow);
			processResults();
			return _paused ? { meta: { paused: true } } : (_results || { meta: { paused: false } });
		};

		this.paused = function()
		{
			return _paused;
		};

		this.pause = function()
		{
			_paused = true;
			_parser.abort();
			_input = _input.substr(_parser.getCharIndex());
		};

		this.resume = function()
		{
			_paused = false;
			self.streamer.parseChunk(_input);
		};

		this.aborted = function () {
			return _aborted;
		}

		this.abort = function()
		{
			_aborted = true;
			_parser.abort();
			_results.meta.aborted = true;
			if (isFunction(_config.complete))
				_config.complete(_results);
			_input = "";
		};

		function processResults()
		{
			if (_results && _delimiterError)
			{
				addError("Delimiter", "UndetectableDelimiter", "Unable to auto-detect delimiting character; defaulted to '"+Papa.DefaultDelimiter+"'");
				_delimiterError = false;
			}

			if (_config.skipEmptyLines)
			{
				for (var i = 0; i < _results.data.length; i++)
					if (_results.data[i].length == 1 && _results.data[i][0] == "")
						_results.data.splice(i--, 1);
			}

			if (needsHeaderRow())
				fillHeaderFields();

			return applyHeaderAndDynamicTyping();
		}

		function needsHeaderRow()
		{
			return _config.header && _fields.length == 0;
		}

		function fillHeaderFields()
		{
			if (!_results)
				return;
			for (var i = 0; needsHeaderRow() && i < _results.data.length; i++)
				for (var j = 0; j < _results.data[i].length; j++)
					_fields.push(_results.data[i][j]);
			_results.data.splice(0, 1);
		}

		function applyHeaderAndDynamicTyping()
		{
			if (!_results || (!_config.header && !_config.dynamicTyping))
				return _results;

			for (var i = 0; i < _results.data.length; i++)
			{
				var row = {};

				for (var j = 0; j < _results.data[i].length; j++)
				{
					if (_config.dynamicTyping)
					{
						var value = _results.data[i][j];
						if (value == "true" || value == "TRUE")
							_results.data[i][j] = true;
						else if (value == "false" || value == "FALSE")
							_results.data[i][j] = false;
						else
							_results.data[i][j] = tryParseFloat(value);
					}

					if (_config.header)
					{
						if (j >= _fields.length)
						{
							if (!row["__parsed_extra"])
								row["__parsed_extra"] = [];
							row["__parsed_extra"].push(_results.data[i][j]);
						}
						else
							row[_fields[j]] = _results.data[i][j];
					}
				}

				if (_config.header)
				{
					_results.data[i] = row;
					if (j > _fields.length)
						addError("FieldMismatch", "TooManyFields", "Too many fields: expected " + _fields.length + " fields but parsed " + j, i);
					else if (j < _fields.length)
						addError("FieldMismatch", "TooFewFields", "Too few fields: expected " + _fields.length + " fields but parsed " + j, i);
				}
			}

			if (_config.header && _results.meta)
				_results.meta.fields = _fields;
			return _results;
		}

		function guessDelimiter(input)
		{
			var delimChoices = [",", "\t", "|", ";", Papa.RECORD_SEP, Papa.UNIT_SEP];
			var bestDelim, bestDelta, fieldCountPrevRow;

			for (var i = 0; i < delimChoices.length; i++)
			{
				var delim = delimChoices[i];
				var delta = 0, avgFieldCount = 0;
				fieldCountPrevRow = undefined;

				var preview = new Parser({
					delimiter: delim,
					preview: 10
				}).parse(input);

				for (var j = 0; j < preview.data.length; j++)
				{
					var fieldCount = preview.data[j].length;
					avgFieldCount += fieldCount;

					if (typeof fieldCountPrevRow === 'undefined')
					{
						fieldCountPrevRow = fieldCount;
						continue;
					}
					else if (fieldCount > 1)
					{
						delta += Math.abs(fieldCount - fieldCountPrevRow);
						fieldCountPrevRow = fieldCount;
					}
				}

				if (preview.data.length > 0)
					avgFieldCount /= preview.data.length;

				if ((typeof bestDelta === 'undefined' || delta < bestDelta)
					&& avgFieldCount > 1.99)
				{
					bestDelta = delta;
					bestDelim = delim;
				}
			}

			_config.delimiter = bestDelim;

			return {
				successful: !!bestDelim,
				bestDelimiter: bestDelim
			}
		}

		function guessLineEndings(input)
		{
			input = input.substr(0, 1024*1024);	// max length 1 MB

			var r = input.split('\r');

			if (r.length == 1)
				return '\n';

			var numWithN = 0;
			for (var i = 0; i < r.length; i++)
			{
				if (r[i][0] == '\n')
					numWithN++;
			}

			return numWithN >= r.length / 2 ? '\r\n' : '\r';
		}

		function tryParseFloat(val)
		{
			var isNumber = FLOAT.test(val);
			return isNumber ? parseFloat(val) : val;
		}

		function addError(type, code, msg, row)
		{
			_results.errors.push({
				type: type,
				code: code,
				message: msg,
				row: row
			});
		}
	}





	/** The core parser implements speedy and correct CSV parsing */
	function Parser(config)
	{
		// Unpack the config object
		config = config || {};
		var delim = config.delimiter;
		var newline = config.newline;
		var comments = config.comments;
		var step = config.step;
		var preview = config.preview;
		var fastMode = config.fastMode;

		// Delimiter must be valid
		if (typeof delim !== 'string'
			|| Papa.BAD_DELIMITERS.indexOf(delim) > -1)
			delim = ",";

		// Comment character must be valid
		if (comments === delim)
			throw "Comment character same as delimiter";
		else if (comments === true)
			comments = "#";
		else if (typeof comments !== 'string'
			|| Papa.BAD_DELIMITERS.indexOf(comments) > -1)
			comments = false;

		// Newline must be valid: \r, \n, or \r\n
		if (newline != '\n' && newline != '\r' && newline != '\r\n')
			newline = '\n';

		// We're gonna need these at the Parser scope
		var cursor = 0;
		var aborted = false;

		this.parse = function(input, baseIndex, ignoreLastRow)
		{
			// For some reason, in Chrome, this speeds things up (!?)
			if (typeof input !== 'string')
				throw "Input must be a string";

			// We don't need to compute some of these every time parse() is called,
			// but having them in a more local scope seems to perform better
			var inputLen = input.length,
				delimLen = delim.length,
				newlineLen = newline.length,
				commentsLen = comments.length;
			var stepIsFunction = typeof step === 'function';

			// Establish starting state
			cursor = 0;
			var data = [], errors = [], row = [], lastCursor = 0;

			if (!input)
				return returnable();

			if (fastMode || (fastMode !== false && input.indexOf('"') === -1))
			{
				var rows = input.split(newline);
				for (var i = 0; i < rows.length; i++)
				{
					var row = rows[i];
					cursor += row.length;
					if (i !== rows.length - 1)
						cursor += newline.length;
					else if (ignoreLastRow)
						return returnable();
					if (comments && row.substr(0, commentsLen) == comments)
						continue;
					if (stepIsFunction)
					{
						data = [];
						pushRow(row.split(delim));
						doStep();
						if (aborted)
							return returnable();
					}
					else
						pushRow(row.split(delim));
					if (preview && i >= preview)
					{
						data = data.slice(0, preview);
						return returnable(true);
					}
				}
				return returnable();
			}

			var nextDelim = input.indexOf(delim, cursor);
			var nextNewline = input.indexOf(newline, cursor);

			// Parser loop
			for (;;)
			{
				// Field has opening quote
				if (input[cursor] == '"')
				{
					// Start our search for the closing quote where the cursor is
					var quoteSearch = cursor;

					// Skip the opening quote
					cursor++;

					for (;;)
					{
						// Find closing quote
						var quoteSearch = input.indexOf('"', quoteSearch+1);

						if (quoteSearch === -1)
						{
							if (!ignoreLastRow) {
								// No closing quote... what a pity
								errors.push({
									type: "Quotes",
									code: "MissingQuotes",
									message: "Quoted field unterminated",
									row: data.length,	// row has yet to be inserted
									index: cursor
								});
							}
							return finish();
						}

						if (quoteSearch === inputLen-1)
						{
							// Closing quote at EOF
							var value = input.substring(cursor, quoteSearch).replace(/""/g, '"');
							return finish(value);
						}

						// If this quote is escaped, it's part of the data; skip it
						if (input[quoteSearch+1] == '"')
						{
							quoteSearch++;
							continue;
						}

						if (input[quoteSearch+1] == delim)
						{
							// Closing quote followed by delimiter
							row.push(input.substring(cursor, quoteSearch).replace(/""/g, '"'));
							cursor = quoteSearch + 1 + delimLen;
							nextDelim = input.indexOf(delim, cursor);
							nextNewline = input.indexOf(newline, cursor);
							break;
						}

						if (input.substr(quoteSearch+1, newlineLen) === newline)
						{
							// Closing quote followed by newline
							row.push(input.substring(cursor, quoteSearch).replace(/""/g, '"'));
							saveRow(quoteSearch + 1 + newlineLen);
							nextDelim = input.indexOf(delim, cursor);	// because we may have skipped the nextDelim in the quoted field

							if (stepIsFunction)
							{
								doStep();
								if (aborted)
									return returnable();
							}
							
							if (preview && data.length >= preview)
								return returnable(true);

							break;
						}
					}

					continue;
				}

				// Comment found at start of new line
				if (comments && row.length === 0 && input.substr(cursor, commentsLen) === comments)
				{
					if (nextNewline == -1)	// Comment ends at EOF
						return returnable();
					cursor = nextNewline + newlineLen;
					nextNewline = input.indexOf(newline, cursor);
					nextDelim = input.indexOf(delim, cursor);
					continue;
				}

				// Next delimiter comes before next newline, so we've reached end of field
				if (nextDelim !== -1 && (nextDelim < nextNewline || nextNewline === -1))
				{
					row.push(input.substring(cursor, nextDelim));
					cursor = nextDelim + delimLen;
					nextDelim = input.indexOf(delim, cursor);
					continue;
				}

				// End of row
				if (nextNewline !== -1)
				{
					row.push(input.substring(cursor, nextNewline));
					saveRow(nextNewline + newlineLen);

					if (stepIsFunction)
					{
						doStep();
						if (aborted)
							return returnable();
					}

					if (preview && data.length >= preview)
						return returnable(true);

					continue;
				}

				break;
			}


			return finish();


			function pushRow(row)
			{
				data.push(row);
				lastCursor = cursor;
			}

			/**
			 * Appends the remaining input from cursor to the end into
			 * row, saves the row, calls step, and returns the results.
			 */
			function finish(value)
			{
				if (ignoreLastRow)
					return returnable();
				if (typeof value === 'undefined')
					value = input.substr(cursor);
				row.push(value);
				cursor = inputLen;	// important in case parsing is paused
				pushRow(row);
				if (stepIsFunction)
					doStep();
				return returnable();
			}

			/**
			 * Appends the current row to the results. It sets the cursor
			 * to newCursor and finds the nextNewline. The caller should
			 * take care to execute user's step function and check for
			 * preview and end parsing if necessary.
			 */
			function saveRow(newCursor)
			{
				cursor = newCursor;
				pushRow(row);
				row = [];
				nextNewline = input.indexOf(newline, cursor);
			}

			/** Returns an object with the results, errors, and meta. */
			function returnable(stopped)
			{
				return {
					data: data,
					errors: errors,
					meta: {
						delimiter: delim,
						linebreak: newline,
						aborted: aborted,
						truncated: !!stopped,
						cursor: lastCursor + (baseIndex || 0)
					}
				};
			}

			/** Executes the user's step function and resets data & errors. */
			function doStep()
			{
				step(returnable());
				data = [], errors = [];
			}
		};

		/** Sets the abort flag */
		this.abort = function()
		{
			aborted = true;
		};

		/** Gets the cursor position */
		this.getCharIndex = function()
		{
			return cursor;
		};
	}


	// If you need to load Papa Parse asynchronously and you also need worker threads, hard-code
	// the script path here. See: https://github.com/mholt/PapaParse/issues/87#issuecomment-57885358
	function getScriptPath()
	{
		var scripts = document.getElementsByTagName('script');
		return scripts.length ? scripts[scripts.length - 1].src : '';
	}

	function newWorker()
	{
		if (!Papa.WORKERS_SUPPORTED)
			return false;
		if (!LOADED_SYNC && Papa.SCRIPT_PATH === null)
			throw new Error(
				'Script path cannot be determined automatically when Papa Parse is loaded asynchronously. ' +
				'You need to set Papa.SCRIPT_PATH manually.'
			);
		var workerUrl = Papa.SCRIPT_PATH || AUTO_SCRIPT_PATH;
		// Append "papaworker" to the search string to tell papaparse that this is our worker.
		workerUrl += (workerUrl.indexOf('?') !== -1 ? '&' : '?') + 'papaworker';
		var w = new global.Worker(workerUrl);
		w.onmessage = mainThreadReceivedMessage;
		w.id = workerIdCounter++;
		workers[w.id] = w;
		return w;
	}

	/** Callback when main thread receives a message */
	function mainThreadReceivedMessage(e)
	{
		var msg = e.data;
		var worker = workers[msg.workerId];
		var aborted = false;

		if (msg.error)
			worker.userError(msg.error, msg.file);
		else if (msg.results && msg.results.data)
		{
			var abort = function() {
				aborted = true;
				completeWorker(msg.workerId, { data: [], errors: [], meta: { aborted: true } });
			};

			var handle = {
				abort: abort,
				pause: notImplemented,
				resume: notImplemented
			};

			if (isFunction(worker.userStep))
			{
				for (var i = 0; i < msg.results.data.length; i++)
				{
					worker.userStep({
						data: [msg.results.data[i]],
						errors: msg.results.errors,
						meta: msg.results.meta
					}, handle);
					if (aborted)
						break;
				}
				delete msg.results;	// free memory ASAP
			}
			else if (isFunction(worker.userChunk))
			{
				worker.userChunk(msg.results, handle, msg.file);
				delete msg.results;
			}
		}

		if (msg.finished && !aborted)
			completeWorker(msg.workerId, msg.results);
	}

	function completeWorker(workerId, results) {
		var worker = workers[workerId];
		if (isFunction(worker.userComplete))
			worker.userComplete(results);
		worker.terminate();
		delete workers[workerId];
	}

	function notImplemented() {
		throw "Not implemented.";
	}

	/** Callback when worker thread receives a message */
	function workerThreadReceivedMessage(e)
	{
		var msg = e.data;

		if (typeof Papa.WORKER_ID === 'undefined' && msg)
			Papa.WORKER_ID = msg.workerId;

		if (typeof msg.input === 'string')
		{
			global.postMessage({
				workerId: Papa.WORKER_ID,
				results: Papa.parse(msg.input, msg.config),
				finished: true
			});
		}
		else if ((global.File && msg.input instanceof File) || msg.input instanceof Object)	// thank you, Safari (see issue #106)
		{
			var results = Papa.parse(msg.input, msg.config);
			if (results)
				global.postMessage({
					workerId: Papa.WORKER_ID,
					results: results,
					finished: true
				});
		}
	}

	/** Makes a deep copy of an array or object (mostly) */
	function copy(obj)
	{
		if (typeof obj !== 'object')
			return obj;
		var cpy = obj instanceof Array ? [] : {};
		for (var key in obj)
			cpy[key] = copy(obj[key]);
		return cpy;
	}

	function bindFunction(f, self)
	{
		return function() { f.apply(self, arguments); };
	}

	function isFunction(func)
	{
		return typeof func === 'function';
	}
})(typeof window !== 'undefined' ? window : this);

},{}]},{},[4])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29vcmRlbi1qcy9ub2RlX21vZHVsZXMvZ3VscC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL2FqdXN0ZXMvY3VlbnRhcy1jb250YWJsZXMvQ3VlbnRhRm9ybS5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvYWp1c3Rlcy9jdWVudGFzLWNvbnRhYmxlcy9DdWVudGFNb2RlbC5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvYWp1c3Rlcy9jdWVudGFzLWNvbnRhYmxlcy9DdWVudGFWaWV3TW9kZWwuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL2Zha2VfMWI5Y2JkOS5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvaW5wdXRzL2ZlY2hhSW5wdXQuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL21pc2MvQVNTZWxlY3Rvci5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvbWlzYy9hZHZTZWxlY3Rvci5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvbWlzYy9jb21wb25lbnRzL0ZlY2hhc0JhcjIuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL21pc2MvY29tcG9uZW50cy9TZWFyY2hCYXIuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL21pc2Mvc2VsZWN0b3JzL1RlcmNlcm9TZWxlY3Rvci52MS5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvbXQtbW9kYWwvbXQtbW9kYWwuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL25haHVpL2R0YWJsZS5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvbmFodWkvbmFodWkuZmllbGQuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL25haHVpL25haHVpLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3IvTGF0aW5pc2UuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vci9Pb3JkZW5EYXRhLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3IvVGltZU1hbmFnZXIuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vci9mZWNoYS5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvb29yL2luZGV4LmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3Ivc2VsZWN0b3JzL2N1ZW50YXNTZWxlY3QyLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3Ivc2VsZWN0b3JzL2N1ZW50YXNTZWxlY3Rvci5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvb29yT3BlcmFjaW9uZXMvY29tcG9uZW50ZXMvQXJjaGl2b3NDb21wb25lbnRlLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9jb21wb25lbnRlcy9CaXRhY29yYUNvbXBvbmVudGUuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vck9wZXJhY2lvbmVzL2NvbXBvbmVudGVzL0NGRElDb21wb25lbnRlLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9jb21wb25lbnRlcy9FbmNhYmV6YWRvT3BlcmFjaW9uLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9jb21wb25lbnRlcy9JbnRlckNvbXBvbmVudGVzLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9jb21wb25lbnRlcy9Ob3Rhc0NvbXBvbmVudGUuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vck9wZXJhY2lvbmVzL2luZGV4LmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9saXN0YWRvL2FjY2lvbmVzL0FwbGljYXJBY2Npb24uanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vck9wZXJhY2lvbmVzL2xpc3RhZG8vYWNjaW9uZXMvUmVnaXN0cmFyUGFnb0FjY2lvbi5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvb29yT3BlcmFjaW9uZXMvbGlzdGFkby9hY3Rpb25zT3BlcmFjaW9uZXMuanMiLCIvaG9tZS9vb3JkZW4tanMvc3JjL29vck9wZXJhY2lvbmVzL2xpc3RhZG8vbGlzdGFkby5qcyIsIi9ob21lL29vcmRlbi1qcy9zcmMvb29yT3BlcmFjaW9uZXMvbGlzdGFkby90YWJsYU9wZXJhY2lvbmVzLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9vb3JPcGVyYWNpb25lcy9wYW50YWxsYXMvdmVyT3BlcmFjaW9uLmpzIiwiL2hvbWUvb29yZGVuLWpzL3NyYy9wYXBhcGFyc2UvcGFwYXBhcnNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0SEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUxBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbGJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2akJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25EQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDclBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbEhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9IQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9NQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3Rocm93IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIil9dmFyIGY9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGYuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sZixmLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIm1vZHVsZS5leHBvcnRzID0ge1xuICAgIGNvbnRyb2xsZXIgOiBDdWVudGFGb3JtQ29udHJvbGxlcixcbiAgICB2aWV3IDogQ3VlbnRhRm9ybVZpZXcsXG4gICAgb3Blbk1vZGFsIDogb3Blbk1vZGFsXG59XG5cbnZhciBDdWVudGEgPSByZXF1aXJlKCcuL0N1ZW50YU1vZGVsJyk7XG52YXIgQ3VlbnRhVmlld01vZGVsID0gcmVxdWlyZSgnLi9DdWVudGFWaWV3TW9kZWwnKTtcblxuZnVuY3Rpb24gQ3VlbnRhRm9ybUNvbnRyb2xsZXIgKHBhcmFtcykge1xuICAgIHZhciBjdHggPSB0aGlzO1xuXG4gICAgY3R4LmN1ZW50YSA9IG0ucHJvcCgpO1xuICAgIGN0eC5ndWFyZGFyID0gZ3VhcmRhcjtcblxuICAgIGN0eC52bSA9IG5ldyBDdWVudGFWaWV3TW9kZWwoe1xuICAgICAgICBvbnNhdmUgOiBhZnRlclNhdmUsXG4gICAgICAgIG9uc2F2ZUVycm9yIDogZnVuY3Rpb24gKGVycm9ycykge1xuICAgICAgICAgICAgdG9hc3RyLmVycm9yKGVycm9yc1swXS5tZXNzYWdlKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgY3R4LnZtLmN1ZW50YSA9IGN0eC5jdWVudGE7XG5cbiAgICBjdHguaW1wdWVzdG9zU2VsZWN0b3IgPSBvb3IuaW1wdWVzdG9zU2VsZWN0Mih7XG4gICAgICAgIGltcHVlc3RvcyA6IG9vcmRlbi5pbXB1ZXN0b3MoKSxcbiAgICAgICAgbW9kZWwgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgY3R4LmN1ZW50YSgpLmltcHVlc3RvX2Nvbmp1bnRvX2lkKGFyZ3VtZW50c1swXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gY3R4LmN1ZW50YSgpLmltcHVlc3RvX2Nvbmp1bnRvX2lkKCk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuXG4gICAgY3R4LmluaXRpYWxpemVNYXNrID0gZnVuY3Rpb24gKGVsZW1lbnQsIGlzSW5pdGVkKSB7XG4gICAgICAgIGlmKGlzSW5pdGVkKSByZXR1cm47XG4gICAgICAgIHZhciBmb3JtYXRvQ3VlbnRhcyA9IGYoJ2Zvcm1hdG9fY3VlbnRhcycpKCBvb3JkZW4ub3JnYW5pemFjaW9uKCkgKTtcbiAgICAgICAgZm9ybWF0b0N1ZW50YXMgPSBmb3JtYXRvQ3VlbnRhcy5yZXBsYWNlKC9cXGQvZywgJyonKTtcbiAgICAgICAgJChlbGVtZW50KS5tYXNrKGZvcm1hdG9DdWVudGFzKTtcblxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGd1YXJkYXIgKCkge1xuICAgICAgICBjdHgudm0uZ3VhcmRhcigpXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWZ0ZXJTYXZlIChjdWVudGEpIHtcbiAgICAgICAgY3R4LiRtb2RhbCAmJiBjdHguJG1vZGFsLmNsb3NlKCk7XG4gICAgICAgIG5oLmlzRnVuY3Rpb24ocGFyYW1zLm9uc2F2ZSkgJiYgcGFyYW1zLm9uc2F2ZShjdWVudGEpO1xuICAgIH1cblxuXG4gICAgY3R4LmNhcmdhckN1ZW50YSA9IGZ1bmN0aW9uIChhcmd1bWVudCkge1xuICAgICAgICBDdWVudGEub2J0ZW5lclRpcG9zKClcbiAgICAgICAgICAgIC50aGVuKG9vcmRlbi5wYWlzZXNZTW9uZWRhcylcbiAgICAgICAgICAgIC50aGVuKHNlbGVjdG9yTW9uZWRhKVxuICAgICAgICAgICAgLnRoZW4oY2FyZ2FyQ3VlbnRhKTtcbiAgICB9XG5cblxuXG4gICAgZnVuY3Rpb24gc2VsZWN0b3JNb25lZGEgKCkge1xuICAgICAgICBjdHguc2VsZWN0b3JNb25lZGEgPSBvb3Iuc2VsZWN0Mih7XG4gICAgICAgICAgICBkYXRhIDogb29yZGVuLm1vbmVkYXNEaXNwb25pYmxlcygpLFxuICAgICAgICAgICAgbW9kZWwgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICBjdHguY3VlbnRhKCkubW9uZWRhKCBhcmd1bWVudHNbMF0gKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gY3R4LmN1ZW50YSgpLm1vbmVkYSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNhcmdhckN1ZW50YSAoKSB7XG4gICAgICAgIGlmKCBjdHguY3VlbnRhKCkgKSByZXR1cm47XG5cbiAgICAgICAgaWYoIXBhcmFtcy5jdWVudGFfY29udGFibGVfaWQpIHtcbiAgICAgICAgICAgIGN0eC5jdWVudGEoIG5ldyBDdWVudGEgKTtcbiAgICAgICAgICAgIGN0eC5jdWVudGEoKS4kaXNOZXcodHJ1ZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbS5yZXF1ZXN0KHtcbiAgICAgICAgICAgIG1ldGhvZCA6ICdHRVQnLFxuICAgICAgICAgICAgdXJsIDogJy9hcGl2MicsXG4gICAgICAgICAgICBiYWNrZ3JvdW5kIDogdHJ1ZSxcbiAgICAgICAgICAgIGRhdGEgOiB7XG4gICAgICAgICAgICAgICAgbW9kZWxvIDogJ2N1ZW50YXMnLFxuICAgICAgICAgICAgICAgIGN1ZW50YV9jb250YWJsZV9pZCA6IHBhcmFtcy5jdWVudGFfY29udGFibGVfaWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1bndyYXBTdWNjZXNzIDogZnVuY3Rpb24gKHIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEN1ZW50YShyLmRhdGFbMF0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgY3R4LmN1ZW50YShkYXRhKTtcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4obS5yZWRyYXcpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gQ3VlbnRhRm9ybVZpZXcgKGN0eCkge1xuICAgIGN0eC5jYXJnYXJDdWVudGEoKTtcblxuICAgIHJldHVybiBtKCdkaXYnLCBbXG4gICAgICAgIGN0eC5jdWVudGEoKSA/IEN1ZW50YUZvcm11bGFyaW8oY3R4KSA6IG9vci5sb2FkaW5nKClcbiAgICBdKTtcbn1cblxuZnVuY3Rpb24gcmFkaW9lciAobGFiZWwsIGlkLCBmbikge1xuICAgIHJldHVybiBbXG4gICAgICAgIG0oJ2RpdicsIGxhYmVsKSxcbiAgICAgICAgbSgnLnJhZGlvZXIuZm9ybS1pbmxpbmUucmFkaW9lci1pbmRpZ28nLCBbXG4gICAgICAgICAgICBtKCdpbnB1dCMnICsgaWQgKyAnc2knICsgJ1t0eXBlPXJhZGlvXVtuYW1lPScgKyBpZCArICddJywge1xuICAgICAgICAgICAgICAgIG9uY2hhbmdlIDogbS53aXRoQXR0cignY2hlY2tlZCcsIGZ1bmN0aW9uIChjaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgIGZuKGNoZWNrZWQpXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgY2hlY2tlZCA6IGZuKClcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgbSgnbGFiZWwnLCB7J2Zvcic6aWQgKyAnc2knfSwgJ1PDrScpXG4gICAgICAgIF0pLFxuICAgICAgICBtKCcucmFkaW9lci5mb3JtLWlubGluZS5yYWRpb2VyLWluZGlnbycsIFtcbiAgICAgICAgICAgIG0oJ2lucHV0IycgKyBpZCArICdubycgKyAnW3R5cGU9cmFkaW9dW25hbWU9JyArIGlkICsgJ10nLCB7XG4gICAgICAgICAgICAgICAgb25jaGFuZ2UgOiBtLndpdGhBdHRyKCdjaGVja2VkJywgZnVuY3Rpb24gKGNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgZm4oIWNoZWNrZWQpXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgY2hlY2tlZCA6ICFmbigpXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIG0oJ2xhYmVsJywgeydmb3InOiBpZCArICdubyd9LCAnTm8nKVxuICAgICAgICBdKVxuICAgIF07XG59XG5cbi8qKlxuICogRWwgZm9ybXVsYXJpbyBkZSBsYSBjdWVudGFcbiAqL1xuZnVuY3Rpb24gQ3VlbnRhRm9ybXVsYXJpbyAoY3R4KSB7XG4gICAgdmFyIGN1ZW50YSA9IGN0eC5jdWVudGEoKTtcbiAgICB2YXIgY3RhUGFkcmUgPSBjdHgudm0uY3VlbnRhUGFkcmUoKTtcblxuICAgIHJldHVybiBbXG5cbiAgICAgICAgbSgnLnJvdycsIFtcbiAgICAgICAgICAgIG0oJy5jb2wtc20tNicsIFtcbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlci5saW5lJywgW1xuICAgICAgICAgICAgICAgICAgICBtKCdsYWJlbCcsICdOby4gZGUgY3VlbnRhJyksXG4gICAgICAgICAgICAgICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25maWcgOiBjdHguaW5pdGlhbGl6ZU1hc2ssXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlciA6J07Dum1lcm8gZGUgQ3VlbnRhJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlIDogY3VlbnRhLmN1ZW50YSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgb25jaGFuZ2UgOiBtLndpdGhBdHRyKCd2YWx1ZScsIGZ1bmN0aW9uICh2YWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdWVudGEuY3VlbnRhKHZhbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3R4LnZtLmJ1c2NhckN1ZW50YVBhZHJlKClcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICBtKCcuY29sLXNtLTYnLCBbXG4gICAgICAgICAgICAgICAgbSgnLm10LWlucHV0ZXIubGluZScsIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnTm9tYnJlJyksXG4gICAgICAgICAgICAgICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA6IGN1ZW50YS5ub21icmUoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyIDogJ05vbWJyZSBkZSBsYSBjdWVudGEnLFxuICAgICAgICAgICAgICAgICAgICAgICAgb25jaGFuZ2UgOiBtLndpdGhBdHRyKCd2YWx1ZScsIGN1ZW50YS5ub21icmUpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuXG4gICAgICAgIG0oJy5yb3cnLCBbXG4gICAgICAgICAgICBtKCcuY29sLXNtLTEyJywgW1xuICAgICAgICAgICAgICAgIGN0YVBhZHJlID8gbSgnZGl2JywgW1xuICAgICAgICAgICAgICAgICAgICAnU3ViY3VlbnRhIGRlOiAnLFxuICAgICAgICAgICAgICAgICAgICBmKCdub21icmUnKShjdGFQYWRyZSksXG4gICAgICAgICAgICAgICAgICAgICcoJyArIGYoJ2N1ZW50YScpKGN0YVBhZHJlKSArICcpJ1xuICAgICAgICAgICAgICAgIF0pIDogbnVsbFxuICAgICAgICAgICAgXSlcbiAgICAgICAgXSksXG5cbiAgICAgICAgbSgnLnJvdycsIFtcbiAgICAgICAgICAgIG0oJy5jb2wtc20tNicsIFtcbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlci5saW5lJywgW1xuICAgICAgICAgICAgICAgICAgICBtKCdsYWJlbCcsICdUaXBvJyksXG4gICAgICAgICAgICAgICAgICAgIFNlbGVjdFRpcG8oY3VlbnRhLnRpcG9faWQpXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuXG4gICAgICAgIG0oJy5yb3cnLCBbXG4gICAgICAgICAgICBtKCcuY29sLXNtLTEyJywgW1xuICAgICAgICAgICAgICAgIG0oJy5tdC1pbnB1dGVyLmxpbmUnLCBbXG4gICAgICAgICAgICAgICAgICAgIG0oJ2xhYmVsJywgJ0Rlc2NyaXBjacOzbicpLFxuICAgICAgICAgICAgICAgICAgICBtKCd0ZXh0YXJlYScsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyIDogJ0Rlc2NyaXBjacOzbicsXG4gICAgICAgICAgICAgICAgICAgICAgICBvbmNoYW5nZSA6IG0ud2l0aEF0dHIoJ3ZhbHVlJywgY3VlbnRhLmRlc2NyaXBjaW9uKVxuICAgICAgICAgICAgICAgICAgICB9LGN1ZW50YS5kZXNjcmlwY2lvbigpKVxuICAgICAgICAgICAgICAgIF0pXG5cbiAgICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuXG4gICAgICAgIG0oJ2JyJyksXG5cbiAgICAgICAgbSgnLnJvdycsIFtcblxuXG4gICAgICAgICAgICBtKCcuY29sLXNtLTYnLCBbXG4gICAgICAgICAgICAgICAgcmFkaW9lcignwr9FcyBiYW5jbyBvIENhamE/JywgJ2VzLWJhbmNvLW8tY2FqYScsIGN1ZW50YS5lc19iYW5jb3MpLFxuICAgICAgICAgICAgXSwgY3VlbnRhLmVzX2JhbmNvcygpID8gW1xuXG4gICAgICAgICAgICAgICAgbSgnLm10LWlucHV0ZXIubGluZScsIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnQmFuY28nKSxcbiAgICAgICAgICAgICAgICAgICAgbSgnaW5wdXRbdHlwZT10ZXh0XScsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlIDogY3VlbnRhLmJhbmNvKCksXG4gICAgICAgICAgICAgICAgICAgICAgICBvbmNoYW5nZSA6IG0ud2l0aEF0dHIoJ3ZhbHVlJywgY3VlbnRhLmJhbmNvKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgbSgnLmZsZXgtcm93LmZsZXgtZW5kJywgW1xuXG4gICAgICAgICAgICAgICAgICAgIG0oJy5tdC1pbnB1dGVyLmxpbmUnLCB7c3R5bGUgOiAnZmxleDoyIDInfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnQ3VlbnRhIEJhbmNhcmlhJyksXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPXRleHRdJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpemUgOiAxNixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA6IGN1ZW50YS5jdWVudGFfYmFuY2FyaWEoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbmNoYW5nZSA6IG0ud2l0aEF0dHIoJ3ZhbHVlJywgY3VlbnRhLmN1ZW50YV9iYW5jYXJpYSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgICAgIG0oJy5tdC1pbnB1dGVyLmxpbmUnLCB7c3R5bGU6J2ZsZXg6MSAyJ30sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIG0oJ2xhYmVsJywgJyMgQ2hlcXVlJyksXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPXRleHRdJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpemU6IDUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgOiBjdWVudGEubnVtZXJvX2NoZXF1ZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uY2hhbmdlIDogbS53aXRoQXR0cigndmFsdWUnLCBjdWVudGEubnVtZXJvX2NoZXF1ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlci5saW5lJywgW1xuICAgICAgICAgICAgICAgICAgICBtKCdsYWJlbCcsICdNb25lZGEnKSxcbiAgICAgICAgICAgICAgICAgICAgY3R4LnNlbGVjdG9yTW9uZWRhLnZpZXcoKVxuICAgICAgICAgICAgICAgIF0pXG5cbiAgICAgICAgICAgIF0gOiBudWxsKSxcblxuICAgICAgICAgICAgbSgnLmNvbC1zbS02JywgW1xuICAgICAgICAgICAgICAgIG0oJ2RpdicsIHtzdHlsZTonbWFyZ2luOjEwcHgnfSxbXG4gICAgICAgICAgICAgICAgICAgIHJhZGlvZXIoJ8K/UGFnb3MgUGVybWl0aWRvcz8nLCAncGFnb3MtcGVybWl0aWRvcycsIGN1ZW50YS5wYWdvX3Blcm1pdGlkbylcbiAgICAgICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgICAgIG0oJy5tdC1pbnB1dGVyLmxpbmUnLCBbXG4gICAgICAgICAgICAgICAgICAgIG0oJ2xhYmVsJywgJ0PDs2RpZ28gQWdydXBhZG9yIFNBVCcpLFxuICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPXRleHRdJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgOiBjdWVudGEuY29kaWdvX29maWNpYWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRvbmx5IDogJ3JlYWRvbmx5J1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgbSgnLm10LWlucHV0ZXIubGluZScsIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnQ29uanVudG8gZGUgSW1wdWVzdG9zJyksXG4gICAgICAgICAgICAgICAgICAgIGN0eC5pbXB1ZXN0b3NTZWxlY3Rvci52aWV3KClcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgXSlcblxuICAgIF1cbn1cblxuZnVuY3Rpb24gU2VsZWN0TmF0dXJhbGV6YSAoZm4pIHtcbiAgICByZXR1cm4gbSgnc2VsZWN0Jywge1xuICAgICAgICB2YWx1ZSA6IGZuKCksXG4gICAgICAgIG9uY2hhbmdlIDogbS53aXRoQXR0cigndmFsdWUnLCBmbilcbiAgICB9LCBbXG4gICAgICAgIG0oJ29wdGlvblt2YWx1ZT1BXScsICdBY3JlZG9yYScpLFxuICAgICAgICBtKCdvcHRpb25bdmFsdWU9RF0nLCAnRGV1ZG9yYScpXG4gICAgXSlcbn1cblxuXG5mdW5jdGlvbiBTZWxlY3RUaXBvIChmbikge1xuICAgIHZhciB0aXBvcyA9IEN1ZW50YS50aXBvcygpO1xuXG4gICAgcmV0dXJuIG0oJ3NlbGVjdCcsIHtcbiAgICAgICAgdmFsdWUgOiBmbigpLFxuICAgICAgICBvbmNoYW5nZSA6IG0ud2l0aEF0dHIoJ3ZhbHVlJywgZm4pXG4gICAgfSwgW1xuICAgICAgICBDdWVudGEudGlwb3NBZ3J1cGFkb3MoKS5tYXAoZnVuY3Rpb24gKGdyb3VwKSB7XG4gICAgICAgICAgICByZXR1cm4gbSgnb3B0Z3JvdXAnLCB7bGFiZWwgOiBncm91cC5ub21icmV9LCBbXG4gICAgICAgICAgICAgICAgZ3JvdXAuY2hpbGRyZW4ubWFwKGZ1bmN0aW9uICh0aXBvKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBtKCdvcHRpb24nLCB7dmFsdWUgOiB0aXBvLnRpcG9faWR9LCB0aXBvLm5vbWJyZSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIF0pO1xuICAgICAgICB9KVxuICAgIF0pXG59XG5cbmZ1bmN0aW9uIG9wZW5Nb2RhbCAoc2V0dGluZ3MpIHtcbiAgICAgTVRNb2RhbC5vcGVuKHtcbiAgICAgICAgY29udHJvbGxlciA6Q3VlbnRhRm9ybUNvbnRyb2xsZXIsXG4gICAgICAgIGNvbnRlbnQgOiBDdWVudGFGb3JtVmlldyxcbiAgICAgICAgYXJncyA6IHNldHRpbmdzLFxuICAgICAgICB0b3AgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gbSgnaDQnLCBzZXR0aW5ncy5ub21icmUgKVxuICAgICAgICB9LFxuICAgICAgICBib3R0b20gOiBmdW5jdGlvbiAoY3R4KSB7XG4gICAgICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgICAgIG0oJ2J1dHRvbi5wdWxsLWxlZnQuYnRuLWZsYXQuYnRuLXNtLmJ0bi1kZWZhdWx0Jywge1xuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrIDogY3R4LiRtb2RhbC5jbG9zZVxuICAgICAgICAgICAgICAgIH0sICdDYW5jZWxhcicpLFxuXG4gICAgICAgICAgICAgICAgbSgnYnV0dG9uLnB1bGwtcmlnaHQuYnRuLWZsYXQuYnRuLXNtLmJ0bi1zdWNjZXNzJywge1xuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrIDogY3R4Lmd1YXJkYXJcbiAgICAgICAgICAgICAgICB9LCAnR3VhcmRhcicpXG4gICAgICAgICAgICBdO1xuICAgICAgICB9LFxuICAgICAgICBlbCA6IHRoaXMsXG4gICAgICAgIG1vZGFsQXR0cnMgOiB7XG4gICAgICAgICAgICAnY2xhc3MnIDogJ21vZGFsLW1lZGl1bSdcbiAgICAgICAgfVxuICAgIH0pO1xuXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IEN1ZW50YTtcblxuXG52YXIgZmllbGRzID0ge1xuICAgIGN1ZW50YV9jb250YWJsZV9pZCA6IHt9LFxuICAgIG5vbWJyZSA6IHt9LFxuICAgIGN1ZW50YSA6IHt9LFxuICAgIHN1YmN1ZW50YV9kZSA6IHt9LFxuICAgIHRpcG9faWQ6e30sXG4gICAgbmF0dXJhbGV6YSA6IHt9LFxuICAgIGRlc2NyaXBjaW9uIDoge30sXG4gICAgY29kaWdvX29maWNpYWwgOiB7fSxcbiAgICBwYWdvX3Blcm1pdGlkbyA6IHsgZmlsdGVyIDpmdW5jdGlvbiAoZCkgeyByZXR1cm4gQm9vbGVhbihOdW1iZXIoZCkpfX0sXG4gICAgZXNfYmFuY29zIDogeyBmaWx0ZXIgOmZ1bmN0aW9uIChkKSB7IHJldHVybiBCb29sZWFuKE51bWJlcihkKSl9fSxcblxuICAgIGJhbmNvIDoge30sXG4gICAgY3VlbnRhX2JhbmNhcmlhIDoge30sXG4gICAgbW9uZWRhIDoge30sXG4gICAgbnVtZXJvX2NoZXF1ZSA6IHt9LFxuXG4gICAgaW1wdWVzdG9fY29uanVudG9faWQgOiB7fVxufTtcblxuXG52YXIgb2J0ZW5lclRpcG9zID0gbnVsbDtcblxuXG5mdW5jdGlvbiBDdWVudGEgKGRhdGEpIHtcbiAgICB2YXIgb2JqID0gdGhpcztcbiAgICBkYXRhIHx8IChkYXRhID0ge30pO1xuXG4gICAgT2JqZWN0LmtleXMoZmllbGRzKS5mb3JFYWNoKGZ1bmN0aW9uIChrKSB7XG4gICAgICAgIHZhciBmaWVsZERlZiA9IGZpZWxkc1trXTtcbiAgICAgICAgdmFyIGZpZWxkVmFsID0gZihrKShkYXRhKTtcblxuICAgICAgICBpZih0eXBlb2YgZmllbGREZWYuZmlsdGVyID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGZpZWxkVmFsID0gZmllbGREZWYuZmlsdGVyKGZpZWxkVmFsKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHR5cGVvZiBmaWVsZFZhbCA9PSAndW5kZWZpbmVkJylcbiAgICAgICAge1xuICAgICAgICAgICAgZmllbGRWYWwgPSBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgb2JqW2tdID0gbS5wcm9wKGZpZWxkVmFsKTtcbiAgICB9KTtcblxuICAgIG9iai4kaXNOZXcgPSBtLnByb3AoZmFsc2UpO1xuICAgIG9iai4kaXNOZXcudG9KU09OID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdW5kZWZpbmVkIH1cbn1cblxuQ3VlbnRhLmlkID0gZignY3VlbnRhX2NvbnRhYmxlX2lkJyk7XG5cbkN1ZW50YS50aXBvcyA9IG0ucHJvcCh7fSk7XG5DdWVudGEudGlwb3NBZ3J1cGFkb3MgPSBtLnByb3AoW10pO1xuXG5DdWVudGEub2J0ZW5lclRpcG9zID0gZnVuY3Rpb24gKCkge1xuXG4gICAgaWYoIW9idGVuZXJUaXBvcykge1xuICAgICAgICBvYnRlbmVyVGlwb3MgPSBtLnJlcXVlc3Qoe1xuICAgICAgICAgICAgdXJsIDogJy9jdWVudGFzLWNvbnRhYmxlcy90aXBvcycsXG4gICAgICAgICAgICBtZXRob2QgOiAnR0VUJyxcbiAgICAgICAgICAgIHVud3JhcFN1Y2Nlc3MgOiBmKCdkYXRhJylcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHRpcG9zKSB7XG4gICAgICAgICAgICB2YXIgZ2xiVGlwb3MgPSBDdWVudGEudGlwb3MoKTtcblxuICAgICAgICAgICAgdGlwb3MuZm9yRWFjaChmdW5jdGlvbiAodGlwbykgeyBnbGJUaXBvc1t0aXBvLnRpcG9faWRdID0gdGlwbzsgfSk7XG5cbiAgICAgICAgICAgIHZhciBvR3JvdXBzID0gT2JqZWN0LmtleXModGlwb3MpXG4gICAgICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAodCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gIXRpcG9zW3RdLnBhZHJlXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAubWFwKGZ1bmN0aW9uICh0KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBtVGlwbyA9IHRpcG9zW3RdO1xuICAgICAgICAgICAgICAgICAgICB2YXIgY2hpbGRyZW4gPSAgdGlwb3MuZmlsdGVyKGZ1bmN0aW9uICh0aXBvKSB7IFxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRpcG8ucGFkcmUgPT0gbVRpcG8udGlwb19pZCBcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5oLmV4dGVuZChtVGlwbywge2NoaWxkcmVuOmNoaWxkcmVufSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIEN1ZW50YS50aXBvc0FncnVwYWRvcyhvR3JvdXBzKVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gb2J0ZW5lclRpcG9zO1xufVxuXG5DdWVudGEudmFsaWRhciA9IGZ1bmN0aW9uIChjdWVudGEpIHtcbiAgICB2YXIgZXJyb3JlcyA9IFtdO1xuXG4gICAgaWYoIWN1ZW50YS5ub21icmUoKSkge1xuICAgICAgICBlcnJvcmVzLnB1c2goeyBwcm9wIDogJ25vbWJyZScsIG1lc3NhZ2UgOiAnU2UgbmVjZXNpdGEgdW5hIGN1ZW50YSd9KVxuICAgIH1cblxuICAgIGlmKCEgY3VlbnRhLmN1ZW50YSgpKSB7XG4gICAgICAgIGVycm9yZXMucHVzaCh7IHByb3AgOiAnY3VlbnRhJywgbWVzc2FnZSA6ICdFc3BlY2lmaXF1ZSB1bmEgbsO6bWVybyBkZSBjdWVudGEgdsOhbGlkbyd9KVxuICAgIH1cblxuXG4gICAgcmV0dXJuIGVycm9yZXM7XG59XG5cblxuQ3VlbnRhLmd1YXJkYXIgPSBmdW5jdGlvbiAoY3VlbnRhKSB7XG5cbiAgICB2YXIgaXNOZXcgPSBjdWVudGEuJGlzTmV3KCk7XG4gICAgdmFyIG1ldGhvZCA9IGlzTmV3ID8gJ1BPU1QnIDogJ1BVVCc7XG4gICAgdmFyIHVybCA9ICcvYXBpdjIvJztcbiAgICB1cmwgPSB1cmwgKyAoaXNOZXcgPyAnYWdyZWdhcicgOiAnZWRpdGFyLycgKyBDdWVudGEuaWQoY3VlbnRhKSkgKyAnP21vZGVsbz1jdWVudGFzJztcblxuICAgIHJldHVybiBtLnJlcXVlc3Qoe1xuICAgICAgICBtZXRob2QgOiBtZXRob2QsXG4gICAgICAgIHVybCA6IHVybCxcbiAgICAgICAgZGF0YSA6IGN1ZW50YSxcbiAgICAgICAgdW53cmFwU3VjY2VzcyA6IGYoJ2RhdGEnKVxuICAgIH0pXG59IiwibW9kdWxlLmV4cG9ydHMgPSBDdWVudGFWaWV3TW9kZWw7XG5cbnZhciBDdWVudGEgPSByZXF1aXJlKCcuL0N1ZW50YU1vZGVsJyk7XG5cbmZ1bmN0aW9uIEN1ZW50YVZpZXdNb2RlbCAocGFyYW1zKSB7XG4gICAgdGhpcy5jdWVudGEgPSBtLnByb3AoKTtcblxuICAgIHRoaXMuY3VlbnRhUGFkcmUgPSBtLnByb3AoKTtcblxuICAgIHRoaXMuYnVzY2FuZG9QYWRyZSA9IG0ucHJvcCgpO1xuXG4gICAgdGhpcy5lcnJvckN1ZW50YSA9IG0ucHJvcCgpO1xuXG4gICAgdGhpcy5ndWFyZGFuZG8gPSBtLnByb3AoKTtcblxuICAgIHRoaXMuYnVzY2FyQ3VlbnRhUGFkcmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuYnVzY2FuZG9QYWRyZSh0cnVlKTtcblxuICAgICAgICByZXR1cm4gbS5yZXF1ZXN0KHtcbiAgICAgICAgICAgIG1ldGhvZCA6ICdHRVQnLFxuICAgICAgICAgICAgdXJsIDogJy9hcGkvYnVzY2FyQ3VlbnRhUGFkcmUvJyArIHRoaXMuY3VlbnRhKCkuY3VlbnRhKClcbiAgICAgICAgfSkudGhlbihmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICB0aGlzLmN1ZW50YVBhZHJlKG51bGwpO1xuICAgICAgICAgICB0aGlzLmVycm9yQ3VlbnRhKG51bGwpO1xuXG4gICAgICAgICAgICBpZihkLmVycm9yKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvckN1ZW50YShkLmVycm9yKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jdWVudGFQYWRyZShkKTtcblxuICAgICAgICAgICAgdGhpcy5jdWVudGEoKS50aXBvX2lkKCBkLnRpcG9faWQgKTtcbiAgICAgICAgICAgIHRoaXMuY3VlbnRhKCkuc3ViY3VlbnRhX2RlKCBkLmN1ZW50YV9jb250YWJsZV9pZCApXG4gICAgICAgICAgICB0aGlzLmN1ZW50YSgpLm5hdHVyYWxlemEoIGQubmF0dXJhbGV6YSlcblxuICAgICAgICB9LmJpbmQodGhpcykpO1xuXG4gICAgfS5iaW5kKHRoaXMpO1xuXG5cblxuICAgIHRoaXMuZ3VhcmRhciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGVycm9ycyA9IEN1ZW50YS52YWxpZGFyKHRoaXMuY3VlbnRhKCkpO1xuXG4gICAgICAgIGlmKHRoaXMuZXJyb3JDdWVudGEoKSkge1xuICAgICAgICAgICAgZXJyb3JzLnB1c2godGhpcy5lcnJvckN1ZW50YSgpKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYoZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYobmguaXNGdW5jdGlvbihwYXJhbXMub25zYXZlRXJyb3IpKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zLm9uc2F2ZUVycm9yKGVycm9ycylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZ3VhcmRhbmRvKHRydWUpO1xuICAgICAgICB1cGRhdGUoKTtcblxuICAgICAgICBDdWVudGEuZ3VhcmRhcih0aGlzLmN1ZW50YSgpKVxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJDdWVudGEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmd1YXJkYW5kbyhmYWxzZSk7XG4gICAgICAgICAgICAgICAgdXBkYXRlKCk7XG4gICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ8KhQ3VlbnRhIEd1YXJkYWRhIScpO1xuXG4gICAgICAgICAgICAgICAgaWYobmguaXNGdW5jdGlvbihwYXJhbXMub25zYXZlKSkge1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMub25zYXZlKCByQ3VlbnRhIClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LmJpbmQodGhpcykpO1xuXG4gICAgfS5iaW5kKHRoaXMpO1xuXG5cblxuICAgIHZhciB1cGRhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKCF1cGRhdGUuZWxlbWVudCgpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgZDMuc2VsZWN0KHVwZGF0ZS5lbGVtZW50KCkpLmNhbGwodXBkYXRlRm9ybSwgdGhpcylcbiAgICB9LmJpbmQodGhpcyk7XG5cbiAgICB1cGRhdGUuZWxlbWVudCA9IG0ucHJvcCgpO1xuXG5cbiAgICB0aGlzLmJyaWRnZSA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XG5cbiAgICAgICAgdmFyIGV2ZW50cyA9IHt9O1xuXG4gICAgICAgIGV2ZW50cy5jaGFuZ2UgPSBmdW5jdGlvbiAobWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyIHByb3AgPSB0aGlzLmN1ZW50YSgpW21lc3NhZ2UucHJvcF07XG5cbiAgICAgICAgICAgIHByb3AobWVzc2FnZS52YWx1ZSk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmKG1lc3NhZ2UucHJvcCA9PSAnY3VlbnRhJykge1xuICAgICAgICAgICAgICAgIHVwZGF0ZSgpO1xuICAgICAgICAgICAgICAgIGlmKCFtZXNzYWdlLnZhbHVlKcKge1xuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoJ8KhTsO6bWVybyBkZSBjdWVudGEgaW52w6FsaWRvIScpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy5idXNjYXJDdWVudGFQYWRyZSgpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHVwZGF0ZSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9LmJpbmQodGhpcyk7XG5cblxuICAgICAgICBldmVudHMuY29tbWl0ID0gdGhpcy5ndWFyZGFyO1xuXG4gICAgICAgIGV2ZW50cy5jYW5jZWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBuaC5pc0Z1bmN0aW9uKHBhcmFtcy5vbmNhbmNlbCkgJiYgcGFyYW1zLm9uY2FuY2VsKCk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIHVwZGF0ZS5lbGVtZW50KGVsZW1lbnQpXG5cbiAgICAgICAgJChlbGVtZW50KS5vbignY2hhbmdlJywgJ1t4LXByb3BdJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGlucHV0ID0gJCh0aGlzKTtcbiAgICAgICAgICAgIHZhciBtc2cgPSB7XG4gICAgICAgICAgICAgICAgdHlwZSA6ICdjaGFuZ2UnLFxuICAgICAgICAgICAgICAgIHByb3AgOiBpbnB1dC5hdHRyKCd4LXByb3AnKSxcbiAgICAgICAgICAgICAgICB2YWx1ZSA6IGlucHV0LnZhbCgpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgZW1pdChtc2cpO1xuICAgICAgICB9KTtcblxuXG4gICAgICAgICQoZWxlbWVudCkub24oJ2lucHV0JywgJ1t4LXByb3BdJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGlucHV0ID0gJCh0aGlzKTtcbiAgICAgICAgICAgIHZhciBtc2cgPSB7XG4gICAgICAgICAgICAgICAgdHlwZSA6ICdpbnB1dCcsXG4gICAgICAgICAgICAgICAgcHJvcCA6IGlucHV0LmF0dHIoJ3gtcHJvcCcpLFxuICAgICAgICAgICAgICAgIHZhbHVlIDogaW5wdXQudmFsKClcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBlbWl0KG1zZyk7XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgJChlbGVtZW50KS5vbignY2xpY2snLCAnW3gtYWN0aW9uXScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBidXR0b24gPSAkKHRoaXMpO1xuICAgICAgICAgICAgdmFyIG1zZyA9IHtcbiAgICAgICAgICAgICAgICB0eXBlIDogYnV0dG9uLmF0dHIoJ3gtYWN0aW9uJylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZW1pdChtc2cpO1xuICAgICAgICB9KVxuXG5cbiAgICAgICAgZnVuY3Rpb24gZW1pdCAobWVzc2FnZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2cobWVzc2FnZSlcbiAgICAgICAgICAgIGlmKG5oLmlzRnVuY3Rpb24oZXZlbnRzW21lc3NhZ2UudHlwZV0pKXtcbiAgICAgICAgICAgICAgICBldmVudHNbbWVzc2FnZS50eXBlXShtZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9LmJpbmQodGhpcyk7XG5cbn1cblxuXG5cbmZ1bmN0aW9uIHVwZGF0ZUZvcm0gKHNlbGVjdGlvbiwgY29udGV4dCkge1xuICAgIHZhciBjdWVudGFQYWRyZSA9IGNvbnRleHQuY3VlbnRhUGFkcmUoKTtcbiAgICB2YXIgZXJyQ3RhUGFkcmUgPSBjb250ZXh0LmVycm9yQ3VlbnRhKCk7XG5cbiAgICBzZWxlY3Rpb24uc2VsZWN0KCcuY3VlbnRhLXBhZHJlLWN1ZW50YScpXG4gICAgICAgIC50ZXh0KGN1ZW50YVBhZHJlID8gZignY3VlbnRhJykoY3VlbnRhUGFkcmUpIDogJycpXG5cbiAgICBzZWxlY3Rpb24uc2VsZWN0KCcuY3VlbnRhLXBhZHJlLW5vbWJyZScpXG4gICAgICAgIC50ZXh0KGN1ZW50YVBhZHJlID8gZignbm9tYnJlJykoY3VlbnRhUGFkcmUpIDogJycpXG5cbiAgICBzZWxlY3Rpb24uc2VsZWN0KCcuY3VlbnRhLXBhZHJlLWVycm9yJylcbiAgICAgICAgLnRleHQoZXJyQ3RhUGFkcmUgPyBlcnJDdGFQYWRyZSA6ICcnKVxuXG4gICAgc2VsZWN0aW9uLnNlbGVjdCgnW3gtcHJvcD10aXBvX2lkXScpXG4gICAgICAgIC5hdHRyKCdkaXNhYmxlZCcsIGN1ZW50YVBhZHJlID8gJ2Rpc2FibGVkJyA6IHVuZGVmaW5lZClcbiAgICAgICAgLnByb3BlcnR5KCd2YWx1ZScsIGNvbnRleHQuY3VlbnRhKCkudGlwb19pZCgpKVxuXG4gICAgc2VsZWN0aW9uLnNlbGVjdCgnW3gtYWN0aW9uPWNvbW1pdF0nKVxuICAgICAgICAuY2xhc3NlZCgnZGlzYWJsZWQnLCBjb250ZXh0Lmd1YXJkYW5kbygpID8gdHJ1ZSA6IGZhbHNlKVxuICAgICAgICAuc2VsZWN0KCdpJylcbiAgICAgICAgLmNsYXNzZWQoe1xuICAgICAgICAgICAgJ2ZhLXNhdmUnIDogIWNvbnRleHQuZ3VhcmRhbmRvKCksXG4gICAgICAgICAgICAnZmEtc3Bpbm5lcicgOiBjb250ZXh0Lmd1YXJkYW5kbygpLFxuICAgICAgICAgICAgJ2ZhLXNwaW4nIDogY29udGV4dC5ndWFyZGFuZG8oKVxuICAgICAgICB9KVxuXG59IiwicmVxdWlyZSgnLi9uYWh1aS9uYWh1aScpO1xuXG5cbndpbmRvdy5vb3IgICAgICAgICA9IHJlcXVpcmUoJy4vb29yJyk7XG53aW5kb3cub29yLnYxICAgICAgPSB0cnVlO1xud2luZG93Lm9vcmRlbiAgICAgID0gcmVxdWlyZSgnLi9vb3IvT29yZGVuRGF0YS5qcycpO1xud2luZG93Lk1UTW9kYWwgICAgID0gcmVxdWlyZSgnLi9tdC1tb2RhbC9tdC1tb2RhbCcpO1xud2luZG93Lm9vci5vcGVyYWNpb25lcyA9IHJlcXVpcmUoJy4vb29yT3BlcmFjaW9uZXMnKTtcbiIsIlxuXG5tb2R1bGUuZXhwb3J0cyA9IEQzRGF0ZTtcblxuXG5cbmZ1bmN0aW9uIEQzRGF0ZSAoKSB7XG4gICAgdmFyIHZhbHVlICAgICA9IG0ucHJvcCgpO1xuICAgIHZhciBmb3JtYXR0ZWQgPSBtLnByb3AoKTtcbiAgICB2YXIgZm9ybWF0ICAgID0gb29yLmZlY2hhLnByZXR0eUZvcm1hdC55ZWFyO1xuICAgIHZhciBlbGVtZW50O1xuICAgIHZhciBwb3BVcDtcblxuICAgIGRhdGVGbi5vbmNoYW5nZSA9IG0ucHJvcCgpO1xuXG4gICAgZGF0ZUZuLnBvcFVwID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gcG9wVXA7XG4gICAgfVxuXG4gICAgZGF0ZUZuLnZhbHVlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSAge1xuICAgICAgICAgICAgdmFyIHYgPSB2YWx1ZSgpO1xuICAgICAgICAgICAgdmFsdWUoYXJndW1lbnRzWzBdKTtcbiAgICAgICAgICAgIGZvcm1hdHRlZCggZm9ybWF0KHZhbHVlKCkpICk7XG4gICAgICAgICAgICBlbGVtZW50ICYmIChlbGVtZW50LnZhbHVlID0gZm9ybWF0dGVkKCkpO1xuXG4gICAgICAgICAgICBpZih2ICE9IHZhbHVlKCkgKSB7XG4gICAgICAgICAgICAgICAgZGF0ZUZuLm9uY2hhbmdlICYmIGRhdGVGbi5vbmNoYW5nZSh2YWx1ZSgpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsdWUoKTtcbiAgICB9XG5cbiAgICBkYXRlRm4uZm9ybWF0dGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gZm9ybWF0dGVkKCk7XG4gICAgfVxuXG4gICAgZGF0ZUZuLmVsZW1lbnQgPSBmdW5jdGlvbiAoZWwpIHtcbiAgICAgICAgbW91bnQoZWwsIGRhdGVGbik7XG4gICAgICAgIGVsZW1lbnQgPSBlbDtcbiAgICB9XG5cbiAgICBkYXRlRm4uY2xvc2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKHBvcFVwKSBwb3BVcC50cmFuc2l0aW9uKCkuc3R5bGUoJ29wYWNpdHknLCAwKS5yZW1vdmUoKTtcbiAgICAgICAgcG9wVXAgPSBudWxsO1xuICAgIH1cblxuICAgIGRhdGVGbi51cGRhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHBvcFVwLmNhbGwoZGF0ZUZuKVxuICAgIH1cblxuICAgIGRhdGVGbi5vcGVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZighZWxlbWVudCkgcmV0dXJuO1xuXG4gICAgICAgIHZhciBlbCA9IGVsZW1lbnQ7XG4gICAgICAgIHZhciBwb3MgPSAkKGVsKS5vZmZzZXQoKTtcbiAgICAgICAgdmFyIGhlaWdodCA9ICQoZWwpLmhlaWdodCgpO1xuICAgICAgICB2YXIgd2lkdGggPSAkKGVsKS53aWR0aCgpO1xuICAgICAgICB2YXIgdG9wID0gcG9zLnRvcCArIDEwICsgaGVpZ2h0O1xuICAgICAgICBjYXB0dXJlRm9jdXMgPSBmYWxzZTtcbiAgICAgICAgaWYocG9wVXApIHJldHVybjtcblxuICAgICAgICBwb3BVcCA9IGQzLnNlbGVjdChkb2N1bWVudC5ib2R5KVxuICAgICAgICAgICAgICAgICAgICAuYXBwZW5kKCdkaXYnKVxuICAgICAgICAgICAgICAgICAgICAuc3R5bGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSA6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjonYWJzb2x1dGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3BhZGRpbmcnIDogJzIwcHgnLFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6JzI1MHB4JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDonMjgwcHgnLFxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDond2hpdGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3RleHQtYWxpZ24nIDogJ2NlbnRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3AgIDogU3RyaW5nKHBvcy50b3ApLmNvbmNhdCgncHgnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQgOiBTdHJpbmcocG9zLmxlZnQpLmNvbmNhdCgncHgnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICd6LWluZGV4JyA6IDUwMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXIgOiAnMXB4IHNvbGlkICNmMGYwZjAnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2JveC1zaGFkb3cnIDogJzNweCAzcHggNnB4IHJnYmEoMCwwLDAsMC4zKScsXG4gICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cycgOiAnOHB4JyxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cblxuICAgICAgICBwb3BVcC50cmFuc2l0aW9uKClcbiAgICAgICAgICAgIC5zdHlsZSh7b3BhY2l0eSA6IDEsIHRvcCA6IFN0cmluZyh0b3ApLmNvbmNhdCgncHgnKSB9KVxuXG4gICAgICAgIHBvcFVwLmNhbGwoZGF0ZUZuKTtcblxuICAgICAgICByZXR1cm4gcG9wVXA7XG4gICAgfVxuXG5cbiAgICB2YXIgd2Vla1N0YXJ0ID0gMTtcbiAgICB2YXIgd2Vla0xlbiAgID0gNztcbiAgICB2YXIgZGlhcyA9IFsnTCcsICdNJywgJ1gnLCAnSicsICdWJywgJ1MnLCAnRCddXG5cbiAgICBmdW5jdGlvbiBkYXRlRm4gKHNlbGVjdGlvbiwgZGF0ZUNhbGVuZGFyKSB7XG4gICAgICAgIHZhciB2YWxTUUwgPSB2YWx1ZSgpO1xuICAgICAgICB2YXIgdmFsO1xuICAgICAgICB2YXIgY2FsZW5kYXIgPSB7fTtcblxuICAgICAgICBpZihkYXRlQ2FsZW5kYXIpIHtcbiAgICAgICAgICAgIHZhbCA9IGRhdGVDYWxlbmRhclxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYoIXZhbFNRTCkge1xuICAgICAgICAgICAgICAgIHZhbCA9IG9vci5mZWNoYS50b1NRTChuZXcgRGF0ZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhbCA9IG9vci5mZWNoYS5mcm9tU1FMKHZhbFNRTClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmKCBzZWxlY3Rpb24uc2VsZWN0KCd0YWJsZS5jYWxlbmRhcicpLmVtcHR5KCkgKSB7XG4gICAgICAgICAgICBzZWxlY3Rpb24uYXBwZW5kKCdidXR0b24nKVxuICAgICAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdwdWxsLWxlZnQgYnRuIGJ0bi14cyBidG4tZmxhdCBidG4tZGVmYXVsdCcpXG4gICAgICAgICAgICAgICAgLmh0bWwoJyZsYXF1bzsnKTtcblxuICAgICAgICAgICAgc2VsZWN0aW9uLmFwcGVuZCgnYnV0dG9uJylcbiAgICAgICAgICAgICAgICAuYXR0cignY2xhc3MnLCAncHVsbC1yaWdodCBidG4gYnRuLXhzIGJ0bi1mbGF0IGJ0bi1kZWZhdWx0JylcbiAgICAgICAgICAgICAgICAuaHRtbCgnJnJhcXVvOycpO1xuXG5cbiAgICAgICAgICAgIHNlbGVjdGlvbi5hcHBlbmQoJ2g2JylcblxuXG4gICAgICAgICAgICBzZWxlY3Rpb24uYXBwZW5kKCd0YWJsZScpLmF0dHIoJ2NsYXNzJywgJ2NhbGVuZGFyJylcbiAgICAgICAgICAgIHNlbGVjdGlvbi5zZWxlY3QoJ3RhYmxlLmNhbGVuZGFyJykuYXBwZW5kKCd0aGVhZCcpLmFwcGVuZCgndHInKTtcbiAgICAgICAgICAgIHNlbGVjdGlvbi5zZWxlY3QoJ3RhYmxlLmNhbGVuZGFyJykuYXBwZW5kKCd0Ym9keScpO1xuXG4gICAgICAgICAgICBmb3IodmFyIGg9IDA7IGg8NzsgaCsrKXtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb24uc2VsZWN0KCd0YWJsZS5jYWxlbmRhciB0aGVhZCB0cicpLmFwcGVuZCgndGgnKS50ZXh0KGRpYXNbaF0pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHNlbGVjdGlvbi5hcHBlbmQoJ2J1dHRvbicpXG4gICAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2J0biBidG4teHMgYnRuLWZsYXQgYnRuLXByaW1hcnknKVxuICAgICAgICAgICAgICAgIC50ZXh0KCdTZWxlY2Npb25hciBIb3knKVxuICAgICAgICAgICAgICAgIC5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGRhdGVGbi52YWx1ZSggb29yLmZlY2hhLnRvU1FMKG5ldyBEYXRlKSApO1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb24uY2FsbChkYXRlRm4pO1xuICAgICAgICAgICAgICAgICAgICBkYXRlRm4uY2xvc2UoKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuXG4gICAgICAgIGNhbGVuZGFyLnN0YXJ0TW9udGggPSBvb3IuZmVjaGEuaW5pY2lhbE1lcyh2YWwpO1xuICAgICAgICBjYWxlbmRhci5lbmRNb250aCAgID0gb29yLmZlY2hhLmZpbmFsTWVzKHZhbCk7XG4gICAgICAgIGNhbGVuZGFyLnN0YXJ0RGF5ICAgPSBjYWxlbmRhci5zdGFydE1vbnRoLmdldERheSgpO1xuICAgICAgICBjYWxlbmRhci5zdGFydERpZmYgID0gY2FsZW5kYXIuc3RhcnRNb250aC5nZXREYXkoKSAtIHdlZWtTdGFydCA7XG5cbiAgICAgICAgaWYoY2FsZW5kYXIuc3RhcnREaWZmIDwgMCkge1xuICAgICAgICAgICAgY2FsZW5kYXIuc3RhcnREaWZmID0gNyAgKyBjYWxlbmRhci5zdGFydERpZmY7XG4gICAgICAgIH1cblxuICAgICAgICBjYWxlbmRhci5zdGFydERhdGUgID0gbmV3IERhdGUoY2FsZW5kYXIuc3RhcnRNb250aCk7XG4gICAgICAgIGNhbGVuZGFyLnN0YXJ0RGF0ZS5zZXREYXRlKCBjYWxlbmRhci5zdGFydERhdGUuZ2V0RGF0ZSgpIC0gY2FsZW5kYXIuc3RhcnREaWZmICk7XG5cbiAgICAgICAgY2FsZW5kYXIud2Vla3MgPSBbXTtcbiAgICAgICAgdmFyIHdlZWs7XG4gICAgICAgIHZhciBjdXJyRGF0ZSA9IG5ldyBEYXRlKGNhbGVuZGFyLnN0YXJ0RGF0ZSk7XG5cbiAgICAgICAgZm9yKHZhciBpID0gMDsgaTw2OyBpKyspXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNhbGVuZGFyLndlZWtzLnB1c2goW10pXG4gICAgICAgICAgICBmb3IodmFyIGo9IDA7IGo8NzsgaisrKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGNhbGVuZGFyLndlZWtzW2ldLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZSA6IG9vci5mZWNoYS50b1NRTChjdXJyRGF0ZSksXG4gICAgICAgICAgICAgICAgICAgIGRhdGUgIDogY3VyckRhdGUuZ2V0RGF0ZSgpLFxuICAgICAgICAgICAgICAgICAgICBtb250aCA6IGN1cnJEYXRlLmdldE1vbnRoKCkgKyAxXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBjdXJyRGF0ZS5zZXREYXRlKCBjdXJyRGF0ZS5nZXREYXRlKCkgKyAxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgc2VsZWN0aW9uLnNlbGVjdCgnaDYnKS50ZXh0KCBvb3IuZmVjaGEuZnVsbExhYmVsTWVzKGNhbGVuZGFyLnN0YXJ0TW9udGguZ2V0TW9udGgoKSArIDEpICsgJyAnICsgY2FsZW5kYXIuc3RhcnRNb250aC5nZXRGdWxsWWVhcigpIClcblxuICAgICAgICB2YXIgcm93cyA9IHNlbGVjdGlvbi5zZWxlY3QoJ3RhYmxlLmNhbGVuZGFyIHRib2R5Jykuc2VsZWN0QWxsKCd0cicpLmRhdGEoY2FsZW5kYXIud2Vla3MpXG5cbiAgICAgICAgcm93cy5lbnRlcigpLmFwcGVuZCgndHInKVxuXG4gICAgICAgIHZhciBjZWxscyA9IHJvd3Muc2VsZWN0QWxsKCd0ZCcpLmRhdGEoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQgfSlcblxuICAgICAgICBjZWxscy5lbnRlcigpLmFwcGVuZCgndGQnKVxuXG4gICAgICAgIGNlbGxzLnRleHQoIGYoJ2RhdGUnKSApO1xuXG4gICAgICAgIGNlbGxzLmF0dHIoJ2NsYXNzJywndGV4dC1ncmV5Jyk7XG5cbiAgICAgICAgY2VsbHMuZmlsdGVyKCBmKCdtb250aCcpLmlzKCBjYWxlbmRhci5zdGFydE1vbnRoLmdldE1vbnRoKCkgKyAxICkgKS5hdHRyKCdjbGFzcycsICcnKVxuXG4gICAgICAgIGNlbGxzLmZpbHRlciggZigndmFsdWUnKS5pcyh2YWxTUUwpICkuYXR0cignY2xhc3MnLCAnc2VsZWN0ZWQnKTtcblxuICAgICAgICBjZWxscy5vbignY2xpY2snLCBmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgZGF0ZUZuLnZhbHVlKGQudmFsdWUpO1xuICAgICAgICAgICAgc2VsZWN0aW9uLmNhbGwoZGF0ZUZuKTtcbiAgICAgICAgICAgIGRhdGVGbi5jbG9zZSgpXG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgc2VsZWN0aW9uLnNlbGVjdCgnYnV0dG9uLnB1bGwtcmlnaHQnKVxuICAgICAgICAgICAgLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgbkRhdGUgPSBuZXcgRGF0ZShkYXRlQ2FsZW5kYXIgfHwgdmFsKTtcbiAgICAgICAgICAgICAgICBuRGF0ZS5zZXRNb250aCggbkRhdGUuZ2V0TW9udGgoKSArIDEpO1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5jYWxsKGRhdGVGbiwgbkRhdGUpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgc2VsZWN0aW9uLnNlbGVjdCgnYnV0dG9uLnB1bGwtbGVmdCcpXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHZhciBuRGF0ZSA9IG5ldyBEYXRlKGRhdGVDYWxlbmRhciB8fCB2YWwpO1xuICAgICAgICAgICAgICAgIG5EYXRlLnNldE1vbnRoKCBuRGF0ZS5nZXRNb250aCgpIC0gMSk7XG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uLmNhbGwoZGF0ZUZuLCBuRGF0ZSk7XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cblxuICAgIHZhciBjYWRlbmFzID0ge1xuICAgICAgICBkaWE6ICdkYXRlJywgICBkOidkYXRlJywgIGRpYXM6J2RhdGUnLFxuICAgICAgICBtZXM6ICdtb250aCcsICBtOidtb250aCcsIG1lc2VzOidtb250aCdcbiAgICB9XG5cbiAgICB2YXIgbWV0b2RvcyA9IMKge1xuICAgICAgICBkYXRlICA6IHtzZXQgOiAnc2V0RGF0ZScsICBnZXQgOiAnZ2V0RGF0ZSd9LFxuICAgICAgICBtb250aCA6IHtzZXQgOiAnc2V0TW9udGgnLCBnZXQgOiAnZ2V0TW9udGgnfVxuICAgIH1cblxuICAgIHZhciBmYWN0b3JlcyA9IHsnLScgOiAtMSwgJysnIDogMX07XG5cbiAgICBkYXRlRm4ucGFyc2UgPSBmdW5jdGlvbiAodmFsKcKge1xuICAgICAgICB2YWwgPSB2YWwuc3BsaXQoJyAnKS5qb2luKCcnKTtcbiAgICAgICAgdmFsID0gdmFsLnNwbGl0KCcvJykuam9pbignLScpXG5cbiAgICAgICAgaWYob29yLmZlY2hhLnZhbGlkYXRlU1FMKHZhbCkpIHtcbiAgICAgICAgICAgIHJldHVybiB2YWw7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgaG95ID0gbmV3IERhdGUsIG1hdGNoO1xuICAgICAgICBpZih2YWwgPT0gJ2gnIHx8IHZhbCA9PSAnaG95Jykge1xuICAgICAgICAgICAgcmV0dXJuIG9vci5mZWNoYS50b1NRTChob3kpXG4gICAgICAgIH1cblxuICAgICAgICBpZihtYXRjaCA9IHZhbC5tYXRjaCggLyhbXFwrfFxcLV0pKFxccyspPyhcXGR7MSx9KShcXHMrKT8oW0EtWmEtel0rKS8pICkge1xuICAgICAgICAgICAgdmFyIHF1ZSAgICA9IG1hdGNoWzVdLmxhdGluaXplKCkudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHZhciBkYXRlICAgPSBuZXcgRGF0ZShob3kpO1xuICAgICAgICAgICAgdmFyIGZhY3RvciA9IG1hdGNoWzFdID09ICctJyA/IC0xIDogMTtcbiAgICAgICAgICAgIHZhciBtb250byAgPSBOdW1iZXIobWF0Y2hbM10pO1xuXG4gICAgICAgICAgICBpZighY2FkZW5hc1txdWVdKSByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICBxdWUgPSBtZXRvZG9zWyBjYWRlbmFzW3F1ZV0gXTtcblxuICAgICAgICAgICAgaWYoIXF1ZSkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgICAgICBkYXRlW3F1ZS5zZXRdLmNhbGwoZGF0ZSwgZGF0ZVtxdWUuZ2V0XSgpICsgKGZhY3RvciAqIG1vbnRvKSApO1xuICAgICAgICAgICAgcmV0dXJuIG9vci5mZWNoYS50b1NRTChkYXRlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKG1hdGNoID0gdmFsLm1hdGNoKCAvKFxcZHsxLDJ9KShbQS1aYS16XXszLDE1fSkoXFxkezEsNH0pPy8pICkge1xuICAgICAgICAgICAgdmFyIGRhdGUgPSBuZXcgRGF0ZTtcbiAgICAgICAgICAgIHZhciBuTWVzID0gbWF0Y2hbMl0udG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHZhciBuQW5vID0gbWF0Y2hbM10gPyBOdW1iZXIobWF0Y2hbM10pIDogbnVsbFxuXG4gICAgICAgICAgICB2YXIgbWVzID0gb29yLmZlY2hhLm1lc2VzKCkuZmlsdGVyKCBmdW5jdGlvbiAocykge1xuICAgICAgICAgICAgICAgIHJldHVybiBzLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihuTWVzKSA+IC0xO1xuICAgICAgICAgICAgfSlbMF07XG5cbiAgICAgICAgICAgIGlmKCFtZXMpIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIGRhdGUuc2V0TW9udGgob29yLmZlY2hhLm1lc2VzKCkuaW5kZXhPZihtZXMpIC0gMSk7XG4gICAgICAgICAgICBkYXRlLnNldERhdGUobWF0Y2hbMV0pO1xuXG4gICAgICAgICAgICBpZihuQW5vICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgaWYobkFubyA8IDEwMDApIHtcbiAgICAgICAgICAgICAgICAgICAgbkFubyArPSAyMDAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBkYXRlLnNldEZ1bGxZZWFyKG5Bbm8pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gb29yLmZlY2hhLnRvU1FMKGRhdGUpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGRhdGVGbjtcbn1cblxuXG5cbmZ1bmN0aW9uIG1vdW50IChlbCwgZGF0ZSkge1xuICAgICQoZWwpLnZhbCggZGF0ZS5mb3JtYXR0ZWQoKSApO1xuXG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignZm9jdXMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGVsLnZhbHVlKSBlbC5zZXRTZWxlY3Rpb25SYW5nZSgwLGVsLnZhbHVlLmxlbmd0aClcbiAgICAgICAgZGF0ZS5vcGVuKCk7XG4gICAgfSk7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZCA9IGRhdGUucGFyc2UoZWwudmFsdWUpO1xuICAgICAgICBpZihkKSB7XG4gICAgICAgICAgICBkYXRlLnZhbHVlKGQpO1xuICAgICAgICB9XG4gICAgICAgIGRhdGUudXBkYXRlKCk7XG4gICAgfSk7XG5cblxuICAgIHZhciBjYXB0dXJlRm9jdXMgPSBmYWxzZTtcblxuICAgIGRvY3VtZW50LmJvZHkuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgaWYoZXYudGFyZ2V0IT0gZWwgJiYgZGF0ZS5wb3BVcCgpICYmIGNhcHR1cmVGb2N1cykge1xuICAgICAgICAgICAgdmFyIHAgPSBub2RlSXNQYXJlbnRPZihkYXRlLnBvcFVwKCkubm9kZSgpLCBldi50YXJnZXQpO1xuICAgICAgICAgICAgaWYoIXApwqBkYXRlLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sZmFsc2UpO1xuXG4gICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIGZ1bmN0aW9uIChldikge1xuICAgICAgICBpZihldi50YXJnZXQgIT0gZWwgJiYgZGF0ZS5wb3BVcCgpICYmIGNhcHR1cmVGb2N1cykge1xuICAgICAgICAgICAgdmFyIHAgPSBub2RlSXNQYXJlbnRPZihkYXRlLnBvcFVwKCkubm9kZSgpLCBldi50YXJnZXQpO1xuICAgICAgICAgICAgaWYoIXApwqBkYXRlLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sdHJ1ZSk7XG5cbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdibHVyJywgZnVuY3Rpb24gKGV2KSB7IGNhcHR1cmVGb2N1cyA9IHRydWU7IH0pO1xuXG4gICAgZnVuY3Rpb24gbm9kZUlzUGFyZW50T2YocGFyZW50LCBjaGlsZCl7XG4gICAgICAgIHZhciBjaGlsZHJlbiA9IFtdO1xuICAgICAgICB2YXIgY3VyciA9IGNoaWxkO1xuXG4gICAgICAgIHdoaWxlKGN1cnIpe1xuICAgICAgICAgICAgY2hpbGRyZW4ucHVzaChjdXJyKTtcbiAgICAgICAgICAgIGlmKGN1cnIgPT0gcGFyZW50KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjdXJyID0gY3Vyci5vZmZzZXRQYXJlbnQ7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxufVxuXG5cblxuXG5EM0RhdGUubW91bnRlciA9IGZ1bmN0aW9uIChkYXRlRm4sIGF0dHJzKSB7XG4gICAgaWYoYXR0cnMubW9kZWwoKSkgeyBkYXRlRm4udmFsdWUoIGF0dHJzLm1vZGVsKCkgKTsgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChlbGVtZW50LCBpc0luaXQpIHtcbiAgICAgICAgaWYoaXNJbml0ID09IGZhbHNlKSB7XG4gICAgICAgICAgICBpZihhdHRycy5tb2RlbCgpKSB7IGRhdGVGbi52YWx1ZSggYXR0cnMubW9kZWwoKSApOyB9XG5cblxuICAgICAgICAgICAgZGF0ZUZuLm9uY2hhbmdlID0gZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgICAgICAgICBhdHRycy5tb2RlbCh2KTtcbiAgICAgICAgICAgICAgICBhdHRycy5vbmNoYW5nZSAmJiBhdHRycy5vbmNoYW5nZSh2KTtcbiAgICAgICAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKGF0dHJzLm1vZGVsKCkgIT0gZGF0ZUZuLnZhbHVlKCkpIHtcbiAgICAgICAgICAgIGRhdGVGbi52YWx1ZShhdHRycy5tb2RlbCgpKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG5cbkQzRGF0ZS5jb21wb25lbnQgPSB7XG4gICAgb25jcmVhdGUgOiBmdW5jdGlvbiAodm5vZGUpIHtcbiAgICAgICAgdm5vZGUuc3RhdGUuZGF0ZUZuID0gbmV3IEQzRGF0ZSgpO1xuXG4gICAgICAgIGlmKHZub2RlLmF0dHJzLm1vZGVsKCkpIHtcbiAgICAgICAgICAgIHZub2RlLnN0YXRlLmRhdGVGbi52YWx1ZSggdm5vZGUuYXR0cnMubW9kZWwoKSApO1xuICAgICAgICB9XG4gICAgICAgIHZub2RlLnN0YXRlLmRhdGVGbi5lbGVtZW50KHZub2RlLmRvbSk7XG5cbiAgICAgICAgdm5vZGUuc3RhdGUuZGF0ZUZuLm9uY2hhbmdlID0gZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgICAgIHZub2RlLmF0dHJzLm1vZGVsKHYpO1xuICAgICAgICAgICAgbmguaXNGdW5jdGlvbih2bm9kZS5hdHRycy5vbmNoYW5nZSkgJiYgYXR0cnMub25jaGFuZ2Uodik7XG4gICAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICB9O1xuICAgIH0sXG4gICAgb251cGRhdGUgOiBmdW5jdGlvbiAodm5vZGUpIHtcbiAgICAgICAgaWYodm5vZGUuYXR0cnMubW9kZWwoKSAhPSB2bm9kZS5zdGF0ZS5kYXRlRm4udmFsdWUoKSkge1xuICAgICAgICAgICAgdm5vZGUuc3RhdGUuZGF0ZUZuLnZhbHVlKCB2bm9kZS5hdHRycy5tb2RlbCgpICk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIHZpZXcgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBtKCdpbnB1dFt0eXBlPXRleHRdJyk7XG4gICAgfVxufVxuIiwiLyoqXG4gKlxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gQVNTZWxlY3RvcjtcblxuZnVuY3Rpb24gQVNTZWxlY3RvciAoaW5wdXROb2RlLCBwcm9wZXJ0aWVzKSB7XG4gICAgaWYoKHRoaXMgaW5zdGFuY2VvZiBBU1NlbGVjdG9yKSA9PT0gZmFsc2UpIHJldHVybiBuZXcgQVNTZWxlY3RvcihpbnB1dE5vZGUsIHByb3BlcnRpZXMpO1xuICAgIHZhciAkdGhpcyAgID0gdGhpcztcbiAgICB2YXIgdmFsdWUgICA9IG0ucHJvcChudWxsKTtcbiAgICB2YXIgdGV4dFZhbCA9IG0ucHJvcCgnJyk7XG4gICAgdmFyIHRleHQgICAgPSBmKCd0ZXh0Jyk7XG4gICAgdmFyIHBvcFVwICAgPSBudWxsO1xuICAgIHZhciBkYXRhICAgID0gbS5wcm9wKClcbiAgICB2YXIgaHRtbCAgICA9IGRlZmF1bHRIVE1MO1xuICAgIHZhciB1cGRhdGUgID0gZGVmYXVsdFVwZGF0ZTtcbiAgICB2YXIgc2VhcmNoU3RyaW5nID0gbS5wcm9wKCcnKTtcbiAgICB2YXIgY29tcHV0ZXIgPSBjb21wdXRlckRlZmF1bHQ7XG4gICAgdmFyIG9uY2hhbmdlO1xuXG4gICAgJHRoaXMuY29tcHV0ZWREYXRhID0gbS5wcm9wKCk7XG4gICAgJHRoaXMuc2VhcmNoU3RyaW5nID0gc2VhcmNoU3RyaW5nO1xuICAgICR0aGlzLmhpZ2hsaWdodElEICA9IG0ucHJvcChudWxsKTtcbiAgICAkdGhpcy5zZWxlY3RlZCAgICAgPSBtLnByb3AobnVsbCk7XG4gICAgJHRoaXMuc2VhcmNoaW5nICAgID0gbS5wcm9wKGZhbHNlKVxuXG5cbiAgICAkdGhpcy5zZWxlY3QgPSBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICB2YXIgb2xkVmFsID0gJHRoaXMudmFsdWUoKTtcbiAgICAgICAgJHRoaXMuc2VsZWN0ZWQoaXRlbSk7XG5cbiAgICAgICAgaWYoaXRlbSAhPSBudWxsICYmICh0eXBlb2YgaXRlbSAhPSAndW5kZWZpbmVkJykpIHtcbiAgICAgICAgICAgICR0aGlzLnZhbHVlKGl0ZW0uaWQsIHRleHQoaXRlbSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJHRoaXMudmFsdWUobnVsbCwnJyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZihvbGRWYWwgIT0gJHRoaXMudmFsdWUoKSkge1xuICAgICAgICAgICAgbmguaXNGdW5jdGlvbihvbmNoYW5nZSkgJiYgb25jaGFuZ2UuY2FsbCgkdGhpcyk7XG4gICAgICAgIH1cblxuICAgICAgICAkdGhpcy51cGRhdGVJbnB1dCgpO1xuICAgICAgICAkdGhpcy5jbG9zZSgpO1xuICAgIH1cblxuXG4gICAgJHRoaXMuc2VsZWN0SWQgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgaWYoaWQgIT0gbnVsbCAmJiAodHlwZW9mIGlkICE9ICd1bmRlZmluZWQnKSkge1xuICAgICAgICAgICAgdmFyIGl0ZW0gPSAkdGhpcy5maW5kQnlJZChpZCk7XG4gICAgICAgICAgICAkdGhpcy5zZWxlY3QoaXRlbSk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgICR0aGlzLmZpbmRCeUlkID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIGlmKGlkICE9IG51bGwgJiYgKHR5cGVvZiBpZCAhPSAndW5kZWZpbmVkJykpIHtcbiAgICAgICAgICAgIHJldHVybiAkdGhpcy5nZXREYXRhKCkuZmlsdGVyKCBmKCdpZCcpLmlzKGlkKSApWzBdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgJHRoaXMubWVzc2FnZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBvcFVwLnNlbGVjdCgnLm1lc3NhZ2UnKTtcbiAgICB9XG5cblxuICAgICR0aGlzLmNvbXB1dGUgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcbiAgICAgICAgJHRoaXMuY29tcHV0ZWREYXRhKFtdKTtcblxuICAgICAgICAkdGhpcy5jb21wdXRlcigpLmNhbGwoJHRoaXMsJHRoaXMsIGZ1bmN0aW9uIChjb21wdXRlZERhdGEpIHtcbiAgICAgICAgICAgICR0aGlzLmNvbXB1dGVkRGF0YShjb21wdXRlZERhdGEpO1xuICAgICAgICAgICAgJHRoaXMucmVkcmF3KCk7XG4gICAgICAgICAgICBuaC5pc0Z1bmN0aW9uKGNhbGxiYWNrKSAmJiBjYWxsYmFjaygpO1xuICAgICAgICB9KTtcbiAgICB9XG5cblxuICAgICR0aGlzLmNvbXB1dGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBjb21wdXRlciA9IGFyZ3VtZW50c1swXVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNvbXB1dGVyO1xuICAgIH1cblxuXG4gICAgJHRoaXMuaHRtbCA9ICBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGh0bWwgPSBhcmd1bWVudHNbMF1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaHRtbDtcbiAgICB9XG5cblxuICAgICR0aGlzLnVwZGF0ZSA9ICBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHVwZGF0ZSA9IGFyZ3VtZW50c1swXVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB1cGRhdGU7XG4gICAgfVxuXG5cbiAgICAkdGhpcy5vbmNoYW5nZSA9IGZ1bmN0aW9uICgpIMKge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBvbmNoYW5nZSA9IGFyZ3VtZW50c1swXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gb25jaGFuZ2U7XG4gICAgfVxuXG4gICAgJHRoaXMucG9wVXAgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBwb3BVcDtcbiAgICB9XG5cblxuICAgICR0aGlzLnNldERhdGEgPSBmdW5jdGlvbiAoYURhdGEpIHtcbiAgICAgICAgaWYoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYobmguaXNGdW5jdGlvbihhRGF0YSkpIHtcbiAgICAgICAgICAgICAgICBkYXRhID0gYURhdGE7XG4gICAgICAgICAgICB9IGVsc2XCoHtcbiAgICAgICAgICAgICAgICBkYXRhKGFEYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgJHRoaXMuZ2V0RGF0YSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIGRhdGEoKTtcbiAgICB9O1xuXG4gICAgJHRoaXMuaXNPcGVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gQm9vbGVhbihwb3BVcCk7XG4gICAgfTtcblxuICAgICR0aGlzLnVwZGF0ZVBvcFVwID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIGlmKHRoaXMuaXNPcGVuKCkpIHtcbiAgICAgICAgICAgICR0aGlzLmNvbXB1dGUoY2FsbGJhY2spO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJHRoaXMub3BlbigpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgICR0aGlzLnJlZHJhdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcG9wVXAuY2FsbChhc1NlbGVjdG9yLCAkdGhpcywge30pO1xuICAgIH1cblxuICAgICR0aGlzLnZhbHVlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICB2YWx1ZShhcmd1bWVudHNbMF0pO1xuICAgICAgICAgICAgdGV4dFZhbChhcmd1bWVudHMubGVuZ3RoID4gMSA/IGFyZ3VtZW50c1sxXSA6IFwiXCIgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB2YWx1ZSgpO1xuICAgIH07XG5cblxuICAgICR0aGlzLnRleHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHRleHQgPSBhcmd1bWVudHNbMF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRleHQ7XG4gICAgfTtcblxuICAgICR0aGlzLnVwZGF0ZUlucHV0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpbnB1dE5vZGUudmFsdWUgPSB0ZXh0VmFsKCk7XG4gICAgfTtcblxuICAgICR0aGlzLmhpZ2hsaWdodCA9IGZ1bmN0aW9uIChhdHRycykge1xuICAgICAgICBpZighYXR0cnMpYXR0cnMgPSB7fTtcbiAgICAgICAgdmFyIGhpZ2hsaWdodGVkID0gJHRoaXMuaGlnaGxpZ2h0SUQoKTtcblxuICAgICAgICB2YXIgYXZhaWxhYmxlID0gJHRoaXMuY29tcHV0ZWREYXRhKCkuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLiRzaG93ICYmIGQuJHNlbGVjdGFibGU7fSk7XG4gICAgICAgIHZhciBjdXJyZW50ID0gYXZhaWxhYmxlLmluZGV4T2YoYXZhaWxhYmxlLmZpbHRlciggZignaWQnKS5pcyhoaWdobGlnaHRlZCkgKVswXSk7XG4gICAgICAgIHZhciBuSWRleDtcblxuICAgICAgICBpZih0eXBlb2YgYXR0cnMuYnlQb3NpdGlvbiA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIG5JbmRleCA9IGF0dHJzLmJ5UG9zaXRpb247XG4gICAgICAgIH0gZWxzZSBpZih0eXBlb2YgYXR0cnMubW92ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIG5JbmRleCA9IGF0dHJzLm1vdmUgKyAoY3VycmVudCB8fCAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHR5cGVvZiBuSW5kZXggIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIG5JbmRleCA9IE1hdGgubWluKGF2YWlsYWJsZS5sZW5ndGgtMSwgbkluZGV4KTtcbiAgICAgICAgICAgIG5JbmRleCA9IE1hdGgubWF4KDAsIG5JbmRleCk7XG4gICAgICAgICAgICAkdGhpcy5oaWdobGlnaHRJRChhdmFpbGFibGVbbkluZGV4XSA/IGF2YWlsYWJsZVtuSW5kZXhdLmlkIDogbnVsbCk7XG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGF0dHJzLmlkICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgJHRoaXMuaGlnaGxpZ2h0SUQoYXR0cnMuaWQpXG4gICAgICAgIH1cblxuICAgICAgICBpZihwb3BVcCkgcG9wVXAuY2FsbCh1cGRhdGVIaWdobGlnaHQsICR0aGlzLmhpZ2hsaWdodElEKCkpXG4gICAgfTtcblxuICAgICR0aGlzLmNsb3NlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZigkdGhpcy5pc09wZW4oKSkge1xuICAgICAgICAgICAgcG9wVXAucmVtb3ZlKCk7XG4gICAgICAgICAgICBwb3BVcCA9IG51bGw7XG5cbiAgICAgICAgICAgIGQzLnNlbGVjdChpbnB1dE5vZGUpXG4gICAgICAgICAgICAgICAgLnN0eWxlKCdwb3NpdGlvbicsIHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAuc3R5bGUoJ3otaW5kZXgnLCB1bmRlZmluZWQpXG4gICAgICAgIH1cblxuICAgICAgICAkdGhpcy51cGRhdGVJbnB1dCgpO1xuICAgIH1cblxuICAgICR0aGlzLnNlbGVjdEhpZ2hsaWdodGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZCA9IHBvcFVwLnNlbGVjdEFsbCgnLmN0aXRlbScpLmZpbHRlcihmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgcmV0dXJuICR0aGlzLmhpZ2hsaWdodElEKCkgPT09IGQuaWQ7XG4gICAgICAgIH0pO1xuICAgICAgICAkdGhpcy5zZWxlY3QoIGQzLnNlbGVjdChkLm5vZGUoKSkuZGF0dW0oKSApO1xuICAgIH1cblxuICAgICR0aGlzLm9wZW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKHBvcFVwKSByZXR1cm47XG4gICAgICAgIGlmKHZhbHVlKCkpIHtcbiAgICAgICAgICAgICR0aGlzLmhpZ2hsaWdodElEKCB2YWx1ZSgpICk7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgZWwgPSBpbnB1dE5vZGU7XG4gICAgICAgIHZhciBwb3MgPSAkKGVsKS5vZmZzZXQoKTtcbiAgICAgICAgdmFyIGhlaWdodCA9ICQoZWwpLmhlaWdodCgpO1xuICAgICAgICB2YXIgd2lkdGggPSAkKGVsKS53aWR0aCgpO1xuICAgICAgICB2YXIgdG9wID0gcG9zLnRvcCAtIDEwIC8vKyBoZWlnaHQgKyA1O1xuICAgICAgICBjYXB0dXJlRm9jdXMgPSBmYWxzZTtcblxuICAgICAgICBkMy5zZWxlY3QoZWwpXG4gICAgICAgICAgICAuc3R5bGUoJ3Bvc2l0aW9uJywgJ3JlbGF0aXZlJylcbiAgICAgICAgICAgIC5zdHlsZSgnei1pbmRleCcsIDUwMDEpXG5cbiAgICAgICAgcG9wVXAgPSBkMy5zZWxlY3QoZG9jdW1lbnQuYm9keSlcbiAgICAgICAgICAgICAgICAgICAgLmFwcGVuZCgnZGl2JylcbiAgICAgICAgICAgICAgICAgICAgLnN0eWxlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246J2Fic29sdXRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmcgOiAnNjBweCAxMHB4IDEwcHggMTBweCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6JzQ1MHB4JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBTdHJpbmcod2lkdGggKyAxMjgpLmNvbmNhdCgncHgnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6J3doaXRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcCAgOiBTdHJpbmcodG9wKS5jb25jYXQoJ3B4JyksXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0IDogU3RyaW5nKHBvcy5sZWZ0IC0gMTApLmNvbmNhdCgncHgnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICd6LWluZGV4JyA6IDUwMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXIgOiAnMXB4IHNvbGlkICNmMGYwZjAnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2JveC1zaGFkb3cnIDogJzNweCAzcHggNnB4IHJnYmEoMCwwLDAsMC4zKScsXG4gICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cycgOiAnOHB4J1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICBwb3BVcC5hcHBlbmQoJ2RpdicpLmF0dHIoJ2NsYXNzJywgJ21lc3NhZ2UnKTtcbiAgICAgICAgcG9wVXAuYXBwZW5kKCdkaXYnKS5hdHRyKCdjbGFzcycsJ2N0Jyk7XG4gICAgICAgIHBvcFVwLnRyYW5zaXRpb24oKS5zdHlsZSh7IG9wYWNpdHkgOiAxIH0pO1xuXG4gICAgICAgICR0aGlzLnVwZGF0ZVBvcFVwKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmKCR0aGlzLnZhbHVlKCkpIHtcbiAgICAgICAgICAgICAgICAkdGhpcy5oaWdobGlnaHQoeyBpZDokdGhpcy52YWx1ZSgpIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG5cblxuICAgICAgICByZXR1cm4gcG9wVXA7XG4gICAgfVxuXG4gICAgJHRoaXMucGVyZm9ybVNlYXJjaCA9IGZ1bmN0aW9uIChzZWFyY2gpIHtcbiAgICAgICAgJHRoaXMuc2VhcmNoaW5nKHRydWUpO1xuICAgICAgICAkdGhpcy5zZWFyY2hTdHJpbmcoc2VhcmNoKTtcblxuICAgICAgICAkdGhpcy51cGRhdGVQb3BVcChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkdGhpcy5oaWdobGlnaHQoeyBieVBvc2l0aW9uOjAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICR0aGlzLnNlYXJjaGluZyhmYWxzZSk7XG4gICAgfVxuXG4gICAgaWYocHJvcGVydGllcy5kYXRhKSB7XG4gICAgICAgICR0aGlzLnNldERhdGEocHJvcGVydGllcy5kYXRhKTtcbiAgICB9XG5cbiAgICBpZihwcm9wZXJ0aWVzLm9uY2hhbmdlKSB7XG4gICAgICAgICR0aGlzLm9uY2hhbmdlKHByb3BlcnRpZXMub25jaGFuZ2UpXG4gICAgfVxuXG4gICAgbW91bnRTZWxlY3RvcihpbnB1dE5vZGUsICR0aGlzKTtcbiAgICByZXR1cm4gJHRoaXM7XG59XG5cbmZ1bmN0aW9uIGFzU2VsZWN0b3IgKHNlbGVjdGlvbiwgc2VsZWN0b3IsIGFyZ3MpIHtcbiAgICB2YXIgYWxsRGF0YSA9IHNlbGVjdG9yLmdldERhdGEoKTtcbiAgICB2YXIgZGF0YSwgZmlsdGVyLCBjdCwgaXRlbXM7XG5cbiAgICBjdCAgICAgPSBzZWxlY3Rpb24uc2VsZWN0KCcuY3QnKTtcbiAgICBkYXRhICAgPSBzZWxlY3Rvci5jb21wdXRlZERhdGEoKS5maWx0ZXIoIGYoJyRzaG93JykuaXModHJ1ZSkgKVxuICAgIGl0ZW1zICA9IGN0LnNlbGVjdEFsbCgnLmN0aXRlbScpLmRhdGEoZGF0YSk7XG5cbiAgICBpdGVtcy5leGl0KCkucmVtb3ZlKCk7XG4gICAgLy9pdGVtcy5zdHlsZSgnZGlzcGxheScsIGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLiRzaG93ID8gdW5kZWZpbmVkIDogJ25vbmUnIH0pO1xuICAgIGl0ZW1zLnN0eWxlKCdjdXJzb3InLCBmdW5jdGlvbihkKSB7ICByZXR1cm4gZC4kc2VsZWN0YWJsZSA/ICdwb2ludGVyJyA6IHVuZGVmaW5lZCB9KTtcbiAgICBpdGVtcy5lbnRlcigpLmFwcGVuZCgnZGl2JykuYXR0cignY2xhc3MnLCAnY3RpdGVtJyk7XG4gICAgaXRlbXMuZXhpdCgpLnJlbW92ZSgpO1xuICAgIGl0ZW1zLmNhbGwoc2VsZWN0b3IudXBkYXRlKCksIHNlbGVjdG9yKTtcbiAgICBpdGVtcy5vbignY2xpY2snLCBzZWxlY3Rvci5zZWxlY3QpO1xuXG5cbiAgICBpZihzZWxlY3Rvci5hZnRlclVwZGF0ZSkge1xuICAgICAgICBzZWxlY3Rpb24uY2FsbChzZWxlY3Rvci5hZnRlclVwZGF0ZSwgaXRlbXMsIHNlbGVjdG9yKTtcbiAgICB9XG5cbiAgICAvL2l0ZW1zLmNsYXNzZWQoJ0hpZ2hsaWdodCcsIGYoJ2lkJykuaXMoc2VsZWN0b3IuaGlnaGxpZ2h0SUQoKSkgKVxufVxuXG5mdW5jdGlvbiB1cGRhdGVIaWdobGlnaHQoc2VsZWN0aW9uLCBoaWdobGlnaHRJRCkge1xuICAgIHZhciBpdGVtcyAgPSBzZWxlY3Rpb24uc2VsZWN0QWxsKCcuY3RpdGVtJylcbiAgICB2YXIgaEl0ZW1zID0gaXRlbXMuZmlsdGVyKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmlkID09IGhpZ2hsaWdodElEOyB9KTtcbiAgICBpdGVtcy5jbGFzc2VkKCdIaWdobGlnaHQnLCBmYWxzZSk7XG4gICAgaEl0ZW1zLmNsYXNzZWQoJ0hpZ2hsaWdodCcsIHRydWUpO1xuICAgIHZhciBub2RlID0gaEl0ZW1zLm5vZGUoKTtcbiAgICBpZihub2RlKSB7XG4gICAgICAgIG5vZGUucGFyZW50Tm9kZS5zY3JvbGxUb3AgPSAtMTAwICsgbm9kZS5vZmZzZXRUb3AgLSBub2RlLnBhcmVudE5vZGUub2Zmc2V0VG9wO1xuICAgIH1cbn1cblxuLyoqXG4gKiBEZWZhdWx0IEZ1bmN0aW9uaW5nIGNvbmZpZ1xuICovXG5cbmZ1bmN0aW9uIGZuRnJvbSh2YWx1ZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7IHJldHVybiB2YWx1ZSB9O1xufVxuXG5mdW5jdGlvbiBjb21wdXRlckRlZmF1bHQgKHNlbGVjdG9yKSB7XG4gICAgdmFyIGRhdGEgPSBzZWxlY3Rvci5nZXREYXRhKCk7XG4gICAgdmFyIHNlYXJjaFN0cmluZyA9IHNlbGVjdG9yLnNlYXJjaFN0cmluZygpLnRvTG93ZXJDYXNlKCkubGF0aW5pemUoKTtcbiAgICB2YXIgc2VhcmNoaW5nID0gc2VsZWN0b3Iuc2VhcmNoaW5nKCk7XG4gICAgdmFyIHZhbHVlID0gc2VsZWN0b3IudmFsdWUoKTtcblxuXG4gICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgIGlmKHNlYXJjaGluZykge1xuICAgICAgICAgICAgZC4kc2hvdyA9IHNlYXJjaFN0cmluZyA/IEJvb2xlYW4oZC5zZWFyY2guaW5kZXhPZihzZWFyY2hTdHJpbmcpICsxKSA6IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBkLiRzaG93ID0gdmFsdWUgPyBkLmlkID09PSB2YWx1ZSA6IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgZC4kc2VsZWN0YWJsZSA9IDE7XG4gICAgfSk7XG4gICAgcmV0dXJuIGRhdGE7XG59XG5cbmZ1bmN0aW9uIGRlZmF1bHRVcGRhdGUgKGl0ZW1zLCBzZWxlY3RvciwgYXJncykge1xuICAgIGl0ZW1zLmh0bWwoIHNlbGVjdG9yLmh0bWwoKSApO1xufVxuXG5mdW5jdGlvbiBkZWZhdWx0SFRNTCAoZCkge1xuICAgIHJldHVybiBkLnRleHQ7XG59XG5cblxuLyoqXG4gKiBNb3VudCB0aGUgc2VsZWN0b3Igb24gdGhlIGlucHV0IGVsZW1lbnRcbiAqL1xuZnVuY3Rpb24gbW91bnRTZWxlY3RvciAoZWwsIHNlbGVjdG9yKSB7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignZm9jdXMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGVsLnZhbHVlKSB7XG4gICAgICAgICAgICBlbC5zZXRTZWxlY3Rpb25SYW5nZSgwLGVsLnZhbHVlLmxlbmd0aCk7XG4gICAgICAgIH1cbiAgICAgICAgc2VsZWN0b3Iuc2VhcmNoU3RyaW5nKCcnKTtcbiAgICAgICAgc2VsZWN0b3Iub3BlbigpO1xuICAgIH0pO1xuXG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNlbGVjdG9yLnBlcmZvcm1TZWFyY2goZWwudmFsdWUpXG4gICAgfSk7XG5cbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdrZXlkb3duJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgIGlmKCBldi5rZXlDb2RlID09PSA0MCB8fCBldi5rZXlDb2RlID09PSAzOCApIHtcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHNlbGVjdG9yLmhpZ2hsaWdodCh7IG1vdmUgOiBldi5rZXlDb2RlID09IDQwID8gMSA6IC0xIH0pO1xuXG4gICAgICAgIH0gZWxzZSBpZiggZXYua2V5Q29kZSA9PT0gMTMgfHwgZXYua2V5Q29kZSA9PT0gOSkge1xuICAgICAgICAgICAgaWYoZXYua2V5Q29kZSA9PT0gMTMpIHtcbiAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIHNlbGVjdG9yLmhpZ2hsaWdodElEKCkgKSB7XG4gICAgICAgICAgICAgICAgLy9zZWxlY3Rvci5zZWxlY3QoIFRlcmNlcm9Db21wb25lbnQuYnlJZFsgc2VsZWN0b3IuaGlnaGxpZ2h0SUQoKSBdICk7XG4gICAgICAgICAgICAgICAgc2VsZWN0b3Iuc2VsZWN0SGlnaGxpZ2h0ZWQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgc2VsZWN0b3Iuc2VsZWN0KCBzZWxlY3Rvci5zZWxlY3RlZCgpICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZihldi5rZXlDb2RlID09PSAyNykge1xuICAgICAgICAgICAgLy9zZWxlY3Rvci5zZWxlY3QoIHNlbGVjdG9yLmlzT3BlbigpID8gc2VsZWN0b3Iuc2VsZWN0ZWQoKSA6IG51bGwpO1xuXG4gICAgICAgICAgICBpZihzZWxlY3Rvci5pc09wZW4oKSkge1xuICAgICAgICAgICAgICAgIHNlbGVjdG9yLmNsb3NlKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuXG5cblxuXG4gICAgdmFyIGNhcHR1cmVGb2N1cyA9IGZhbHNlO1xuXG4gICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uIChldikge1xuICAgICAgICBpZihldi50YXJnZXQhPSBlbCAmJiBzZWxlY3Rvci5pc09wZW4oKSAmJiBjYXB0dXJlRm9jdXMpIHtcbiAgICAgICAgICAgIHZhciBwID0gbm9kZUlzUGFyZW50T2Yoc2VsZWN0b3IucG9wVXAoKS5ub2RlKCksIGV2LnRhcmdldCk7XG4gICAgICAgICAgICBpZighcCnCoHNlbGVjdG9yLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sZmFsc2UpO1xuXG4gICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIGZ1bmN0aW9uIChldikge1xuICAgICAgICBpZihldi50YXJnZXQhPSBlbCAmJiBzZWxlY3Rvci5pc09wZW4oKSAmJiBjYXB0dXJlRm9jdXMpIHtcbiAgICAgICAgICAgIHZhciBwID0gbm9kZUlzUGFyZW50T2Yoc2VsZWN0b3IucG9wVXAoKS5ub2RlKCksIGV2LnRhcmdldCk7XG4gICAgICAgICAgICBpZighcCnCoHNlbGVjdG9yLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sdHJ1ZSk7XG5cbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdibHVyJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgIGNhcHR1cmVGb2N1cyA9IHRydWU7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBub2RlSXNQYXJlbnRPZihwYXJlbnQsIGNoaWxkKXtcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gW107XG4gICAgICAgIHZhciBjdXJyID0gY2hpbGQ7XG5cbiAgICAgICAgd2hpbGUoY3Vycil7XG4gICAgICAgICAgICBjaGlsZHJlbi5wdXNoKGN1cnIpO1xuICAgICAgICAgICAgaWYoY3VyciA9PSBwYXJlbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGN1cnIgPSBjdXJyLm9mZnNldFBhcmVudDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cblxufVxuIiwiXG5tb2R1bGUuZXhwb3J0cyA9IEFkdlNlbGVjdG9yO1xuXG53aW5kb3cueHRTZWxlY3RvciA9IEFkdlNlbGVjdG9yO1xuXG5cbkFkdlNlbGVjdG9yLmN1cnJlbnQgPSBtLnByb3AobnVsbCk7XG5cbmZ1bmN0aW9uIEFkdlNlbGVjdG9yIChzZXR0aW5ncykge1xuICAgIHZhciB0cmlnZ2VyVGltZSA9IHNldHRpbmdzLnRyaWdnZXJUaW1lIHx8IDIwMDtcbiAgICB2YXIgbGltaXRSZXN1bHRzID0gc2V0dGluZ3MubGltaXRSZXN1bHRzIHx8IDU7XG4gICAgdmFyIHNlYXJjaCA9IHNldHRpbmdzLnNlYXJjaCB8fCBzZWFyY2hGbjtcbiAgICB2YXIgdGV4dCA9IHNldHRpbmdzLnRleHQgfHwgZigndGV4dCcpO1xuICAgIHZhciB2YWx1ZSA9IHNldHRpbmdzLnZhbHVlIHx8IGYoJ2lkJyk7XG4gICAgdmFyIGVudGVyRnVuY3Rpb24gPSBzZXR0aW5ncy5lbnRlciB8fCBlbnRlcjtcbiAgICB2YXIgZGF0YSA9IG5oLmlzRnVuY3Rpb24oc2V0dGluZ3MuZGF0YSkgPyAgc2V0dGluZ3MuZGF0YSA6IG0ucHJvcChzZXR0aW5ncy5kYXRhIHx8IFtdKTtcblxuXG4gICAgYWR2U2VsZWN0b3IubW9kZWwgPSBzZXR0aW5ncy5tb2RlbDtcbiAgICBhZHZTZWxlY3Rvci5kcmF3ZWRWYWx1ZSA9IG0ucHJvcCgpO1xuICAgIGFkdlNlbGVjdG9yLmhpZ2hsaWdodGVkID0gbS5wcm9wKDApO1xuICAgIGFkdlNlbGVjdG9yLnJlc3VsdHMgPSBtLnByb3AoW10pO1xuICAgIGFkdlNlbGVjdG9yLnNlbGVjdGlvbiA9IG0ucHJvcCgpO1xuICAgIGFkdlNlbGVjdG9yLnNlbGVjdGVkID0gbS5wcm9wKCk7XG5cbiAgICBhZHZTZWxlY3Rvci5zZWxlY3QgPSBmdW5jdGlvbiAoZCkge1xuICAgICAgICBhZHZTZWxlY3Rvci5zZWxlY3RlZChkKTtcbiAgICAgICAgYWR2U2VsZWN0b3IubW9kZWwodmFsdWUoZCkpO1xuICAgICAgICBhZHZTZWxlY3Rvci51cGRhdGUoKTtcbiAgICAgICAgbmguaXNGdW5jdGlvbihzZXR0aW5ncy5vbmNoYW5nZSkgJiYgc2V0dGluZ3Mub25jaGFuZ2UoKTtcbiAgICAgICAgYWR2U2VsZWN0b3IuaGlkZSgpO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdTZWxlY3QnKTtcbiAgICAgICAgbS5yZWRyYXcoKTtcbiAgICB9XG5cbiAgICBhZHZTZWxlY3Rvci5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBhZHZTZWxlY3Rvci5zZWxlY3Rpb24oKS5jbGFzc2VkKHsgb3BlbmVkIDogdHJ1ZSB9KVxuXG4gICAgICAgIGlmKGFkdlNlbGVjdG9yLnNlbGVjdGVkKCkpIHtcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNlbGVjdGlvbigpLnNlbGVjdCgnaW5wdXQnKS5wcm9wZXJ0eSgndmFsdWUnLCB0ZXh0KCBhZHZTZWxlY3Rvci5zZWxlY3RlZCgpICkpO1xuICAgICAgICAgICAgYWR2U2VsZWN0b3IudXBkYXRlKFthZHZTZWxlY3Rvci5zZWxlY3RlZCgpXSlcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFkdlNlbGVjdG9yLmhpZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFkdlNlbGVjdG9yLnNlbGVjdGlvbigpLmNsYXNzZWQoeyBvcGVuZWQgOiBmYWxzZSB9KVxuICAgIH1cblxuICAgIGFkdlNlbGVjdG9yLmJ5SUQgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgcmV0dXJuIGRhdGEoKS5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuIHZhbHVlKGQpID09IGlkOyB9KVswXVxuICAgIH1cblxuXG5cbiAgICBhZHZTZWxlY3Rvci51cGRhdGUgPSBmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICB2YXIgcmVzdWx0Tm9kZXMgPSBhZHZTZWxlY3Rvci5zZWxlY3Rpb24oKS5zZWxlY3QoJy5yZXN1bHRzLWNvbnRhaW5lcicpLnNlbGVjdEFsbCgnLnJlc3VsdCcpO1xuXG4gICAgICAgIGFkdlNlbGVjdG9yLmRyYXdlZFZhbHVlKCBhZHZTZWxlY3Rvci5tb2RlbCgpICk7XG5cbiAgICAgICAgYWR2U2VsZWN0b3Iuc2VsZWN0aW9uKClcbiAgICAgICAgICAgIC5zZWxlY3QoJy54dC12aWV3LXRlbXBsYXRlJylcbiAgICAgICAgICAgIC5kYXR1bShhZHZTZWxlY3Rvci5zZWxlY3RlZCgpIHx8wqB7fSlcbiAgICAgICAgICAgIC5jYWxsKGVudGVyRnVuY3Rpb24pXG5cbiAgICAgICAgaWYocmVzdWx0cykge1xuICAgICAgICAgICAgYWR2U2VsZWN0b3IucmVzdWx0cyhyZXN1bHRzKTtcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLmhpZ2hsaWdodGVkKDApO1xuXG4gICAgICAgICAgICByZXN1bHROb2RlcyA9IHJlc3VsdE5vZGVzLmRhdGEocmVzdWx0cywgdmFsdWUpO1xuXG4gICAgICAgICAgICByZXN1bHROb2Rlcy5lbnRlcigpXG4gICAgICAgICAgICAgICAgLmFwcGVuZCgnZGl2JylcbiAgICAgICAgICAgICAgICAuYXR0cignY2xhc3MnLCdyZXN1bHQnKVxuICAgICAgICAgICAgICAgIC5hdHRyKCd4LXZhbHVlJywgdmFsdWUpXG4gICAgICAgICAgICAgICAgLmNhbGwoZW50ZXJGdW5jdGlvbilcbiAgICAgICAgICAgICAgICAub24oJ2tleXVwJywgZnVuY3Rpb24gKGQpIHsgIGQzLmV2ZW50LmtleUNvZGUgPT0gMTMgJiYgc2VsZWN0KGQpIH0gKVxuXG4gICAgICAgICAgICByZXN1bHROb2Rlcy5leGl0KCkucmVtb3ZlKCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXN1bHROb2Rlcy5zdHlsZSgnYmFja2dyb3VuZC1jb2xvcicsIGZ1bmN0aW9uIChkLGkpIHtcbiAgICAgICAgICAgIHJldHVybiBhZHZTZWxlY3Rvci5oaWdobGlnaHRlZCgpID09IGkgPyAnbGlnaHRibHVlJyA6ICd3aGl0ZSdcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFkdlNlbGVjdG9yO1xuXG5cbiAgICBmdW5jdGlvbiBhZHZTZWxlY3RvciAobm9kZSwgaXNJbml0aWFsaXplZCkge1xuICAgICAgICB2YXIgc2VsZWN0aW9uLCBsaXN0LCBjb250YWluZXI7XG5cblxuICAgICAgICBpZihhZHZTZWxlY3Rvci5kcmF3ZWRWYWx1ZSgpICE9IGFkdlNlbGVjdG9yLm1vZGVsKCkpIHtcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNlbGVjdGVkKGFkdlNlbGVjdG9yLmJ5SUQoYWR2U2VsZWN0b3IubW9kZWwoKSkpO1xuICAgICAgICAgICAgaWYoaXNJbml0aWFsaXplZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICBhZHZTZWxlY3Rvci51cGRhdGUoKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuXG4gICAgICAgIGlmKGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGlzSW5pdGlhbGl6ZWQgPT09IHRydWUpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cblxuICAgICAgICBub2RlLmlubmVySFRNTCA9ICcnO1xuXG4gICAgICAgIHNlbGVjdGlvbiA9IGQzLnNlbGVjdChub2RlKTtcblxuICAgICAgICBzZWxlY3Rpb24uY2xhc3NlZCh7J3h0LXNlbGVjdG9yJzp0cnVlfSkuYXR0cigndGFiaW5kZXgnLDApXG5cbiAgICAgICAgc2VsZWN0aW9uLmFwcGVuZCgnZGl2JykuYXR0cignY2xhc3MnLCAneHQtdmlldy10ZW1wbGF0ZScpO1xuXG4gICAgICAgIHNlbGVjdGlvbi5hcHBlbmQoJ2RpdicpLmF0dHIoJ2NsYXNzJywgJ3h0LWxpc3QnKTtcblxuICAgICAgICBsaXN0ID0gc2VsZWN0aW9uLnNlbGVjdCgnLnh0LWxpc3QnKTtcblxuICAgICAgICBsaXN0LmFwcGVuZCgnaW5wdXQnKS5hdHRyKCd0eXBlJywgJ3RleHQnKS5hdHRyKCdwbGFjZWhvbGRlcicsICdCdXNjYXIgQ3VlbnRhLi4uJylcblxuICAgICAgICBjb250YWluZXIgPSBsaXN0LmFwcGVuZCgnZGl2JykuYXR0cignY2xhc3MnLCAncmVzdWx0cy1jb250YWluZXInKTtcblxuICAgICAgICBhZHZTZWxlY3Rvci5zZWxlY3Rpb24oc2VsZWN0aW9uKVxuXG5cbiAgICAgICAgdmFyIG5vdFNob3cgPSBtLnByb3AoZmFsc2UpO1xuXG4gICAgICAgICQobm9kZSkub24oJ2ZvY3VzJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYobm90U2hvdygpKSB7XG4gICAgICAgICAgICAgICAgbm90U2hvdyhmYWxzZSlcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNob3coKTtcbiAgICAgICAgICAgICQoJ2lucHV0Jywgbm9kZSkuZm9jdXMoKTtcbiAgICAgICAgfSk7XG5cblxuXG4gICAgICAgICQoJ2lucHV0Jywgbm9kZSkub24oJ2JsdXInLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGFkdlNlbGVjdG9yLmhpZGUoKTtcbiAgICAgICAgICAgICAgICBub3RTaG93KHRydWUpXG4gICAgICAgICAgICB9LDEwMDApO1xuICAgICAgICB9KVxuXG5cblxuICAgICAgICAkKG5vZGUpLm9uKCdjbGljaycsICdbeC12YWx1ZV0nLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgICAgIHZhciBkID0gYWR2U2VsZWN0b3IuYnlJRCggJCh0aGlzKS5hdHRyKCd4LXZhbHVlJykgKTtcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNlbGVjdChkKTtcbiAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKVxuICAgICAgICB9KTtcblxuICAgICAgICAkKG5vZGUpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNob3coKTtcbiAgICAgICAgICAgICQoJ2lucHV0Jywgbm9kZSkuZm9jdXMoKTtcbiAgICAgICAgfSk7XG5cblxuXG4gICAgICAgICQobm9kZSkub24oJ2tleXByZXNzJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgICAgICBpZihldi50YXJnZXQgPT0gdGhpcyAmJiBldi5rZXlDb2RlID09IDEzKSB7XG4gICAgICAgICAgICAgICAgYWR2U2VsZWN0b3Iuc2hvdygpO1xuICAgICAgICAgICAgICAgICQoJ2lucHV0Jywgbm9kZSkuZm9jdXMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkKCdpbnB1dCcsIG5vZGUpLm9uKCdrZXlkb3duJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgICAgICB2YXIgaCA9IGFkdlNlbGVjdG9yLmhpZ2hsaWdodGVkKCk7XG5cbiAgICAgICAgICAgIGlmKGV2LmtleUNvZGUgPT0gMzggfHwgZXYua2V5Q29kZSA9PSA0MCkge1xuICAgICAgICAgICAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKVxuXG4gICAgICAgICAgICAgICAgaWYoZXYua2V5Q29kZSA9PSAzOCkge1xuICAgICAgICAgICAgICAgICAgICBhZHZTZWxlY3Rvci5oaWdobGlnaHRlZChNYXRoLm1heCgwLGgtMSkpO1xuICAgICAgICAgICAgICAgICAgICBhZHZTZWxlY3Rvci51cGRhdGUoKVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKGV2LmtleUNvZGUgPT0gNDAgKXtcbiAgICAgICAgICAgICAgICAgICAgYWR2U2VsZWN0b3IuaGlnaGxpZ2h0ZWQoIE1hdGgubWluKGFkdlNlbGVjdG9yLnJlc3VsdHMoKS5sZW5ndGgtMSwgaCsxKSk7XG4gICAgICAgICAgICAgICAgICAgIGFkdlNlbGVjdG9yLnVwZGF0ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy9FTlRFUlxuICAgICAgICAgICAgaWYoZXYua2V5Q29kZSA9PSAxMykge1xuICAgICAgICAgICAgICAgIGFkdlNlbGVjdG9yLnNlbGVjdChhZHZTZWxlY3Rvci5yZXN1bHRzKClbYWR2U2VsZWN0b3IuaGlnaGxpZ2h0ZWQoKV0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZihldi5rZXlDb2RlID09IDkpIHtcbiAgICAgICAgICAgICAgICBhZHZTZWxlY3Rvci5zZWxlY3QoYWR2U2VsZWN0b3IucmVzdWx0cygpW2FkdlNlbGVjdG9yLmhpZ2hsaWdodGVkKCldKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuXG4gICAgICAgICQoJ2lucHV0Jyxub2RlKS5vbignaW5wdXQnLCBfLmRlYm91bmNlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciB2YWwgPSAkKHRoaXMpLnZhbCgpO1xuICAgICAgICAgICAgdmFyIHJlc3VsdHM7XG5cbiAgICAgICAgICAgIGlmKCF2YWwpIHtcbiAgICAgICAgICAgICAgICByZXN1bHRzID0gW107XG4gICAgICAgICAgICAgICAgYWR2U2VsZWN0b3IuaGlnaGxpZ2h0ZWQoMCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc3VsdHMgPSBkYXRhKCkuZmlsdGVyKHNlYXJjaCh2YWwsIHRleHQpKTtcbiAgICAgICAgICAgICAgICByZXN1bHRzID0gcmVzdWx0cy5zbGljZSgwLGxpbWl0UmVzdWx0cyk7XG4gICAgICAgICAgICAgICAgYWR2U2VsZWN0b3IuaGlnaGxpZ2h0ZWQoMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGFkdlNlbGVjdG9yLnVwZGF0ZShyZXN1bHRzKTtcbiAgICAgICAgfSx0cmlnZ2VyVGltZSkpO1xuXG4gICAgICAgIGFkdlNlbGVjdG9yLnVwZGF0ZSgpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGVudGVyIChzZWxlY3Rpb24pIHtcbiAgICAgICAgc2VsZWN0aW9uLnRleHQodGV4dClcbiAgICB9XG59XG5cbmZ1bmN0aW9uIHNlYXJjaEZuIChzdHIsIHRleHRGbikge1xuICAgIHN0ciA9IGZvcm1hdChzdHIpO1xuICAgIHJldHVybiBmdW5jdGlvbiAoZCkge1xuICAgICAgICByZXR1cm4gZm9ybWF0KHRleHRGbihkKSkuaW5kZXhPZihzdHIpID4gLTE7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBmb3JtYXQgKGQpIHtcbiAgICByZXR1cm4gU3RyaW5nKGQpLmxhdGluaXNlKCkudG9Mb3dlckNhc2UoKTtcbn1cbiIsInZhciBGZWNoYXNCYXIgPSBtb2R1bGUuZXhwb3J0cyA9IHsgRDNEYXRlIDogRDNEYXRlIH07XG5cbkZlY2hhc0Jhci5jb250cm9sbGVyID0gZnVuY3Rpb24gKGFyZ3MpIHtcbiAgICB2YXIgY3R4ICA9IHRoaXM7XG5cbiAgICBjdHguZmVjaGFEZXNkZSA9IG0ucHJvcChudWxsKTtcbiAgICBjdHguZmVjaGFIYXN0YSA9IG0ucHJvcChudWxsKTtcbiAgICBjdHgucHJlID0gbS5wcm9wKCdwZXJzb25hbGl6YWRvJyk7XG5cbiAgICBjdHgudmFsdWUgPSB7ZmVjaGFEZXNkZTpjdHguZmVjaGFEZXNkZSwgZmVjaGFIYXN0YTpjdHguZmVjaGFIYXN0YX07XG5cbiAgICBjdHguc2VsZWN0b3JGZWNoYUhhc3RhID0gRDNEYXRlLm1vdW50ZXIoRDNEYXRlKCksIHsgbW9kZWwgOiBjdHguZmVjaGFIYXN0YSwgb25jaGFuZ2UgOiBjaGFuZ2VWIH0pO1xuICAgIGN0eC5zZWxlY3RvckZlY2hhRGVzZGUgPSBEM0RhdGUubW91bnRlcihEM0RhdGUoKSwgeyBtb2RlbCA6IGN0eC5mZWNoYURlc2RlLCBvbmNoYW5nZSA6IGNoYW5nZVYgfSk7XG5cbiAgICBjdHgub25jaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIG5oLmlzRnVuY3Rpb24oYXJncy5vbmNoYW5nZSkgJiYgYXJncy5vbmNoYW5nZSgpO1xuICAgIH07XG5cbiAgICBmdW5jdGlvbiBjaGFuZ2VWICgpIHtcbiAgICAgICAgY3R4LnBlcnNvbmFsaXphZG8oKTtcbiAgICAgICAgY3R4Lm9uY2hhbmdlKCk7XG4gICAgfVxuXG4gICAgY3R4LmVzdGVBbm8gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBhaG9yYSA9IG5ldyBEYXRlO1xuICAgICAgICB2YXIgaW5pY2lhbCA9IG9vci5mZWNoYS50b1NRTCggb29yLmZlY2hhLmluaWNpYWxBbm8oYWhvcmEpICk7XG4gICAgICAgIHZhciBmaW5hbCA9IG9vci5mZWNoYS50b1NRTCggb29yLmZlY2hhLmZpbmFsQW5vKGFob3JhKSApO1xuXG4gICAgICAgIGN0eC5mZWNoYURlc2RlKGluaWNpYWwpO1xuICAgICAgICBjdHguZmVjaGFIYXN0YShmaW5hbCk7XG4gICAgICAgIGN0eC5wcmUoJ2VzdGVBbm8nKTtcbiAgICAgICAgLy9jdHgub25jaGFuZ2UoKTtcbiAgICB9XG5cbiAgICBjdHguYW5vQW50ZXJpb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBhaG9yYSA9IG5ldyBEYXRlO1xuICAgICAgICBhaG9yYS5zZXRGdWxsWWVhciggYWhvcmEuZ2V0RnVsbFllYXIoKSAtIDEpO1xuICAgICAgICB2YXIgaW5pY2lhbCA9IG9vci5mZWNoYS50b1NRTCggb29yLmZlY2hhLmluaWNpYWxBbm8oYWhvcmEpICk7XG4gICAgICAgIHZhciBmaW5hbCA9IG9vci5mZWNoYS50b1NRTCggb29yLmZlY2hhLmZpbmFsQW5vKGFob3JhKSApO1xuXG4gICAgICAgIGN0eC5mZWNoYURlc2RlKGluaWNpYWwpO1xuICAgICAgICBjdHguZmVjaGFIYXN0YShmaW5hbCk7XG4gICAgICAgIGN0eC5wcmUoJ2VzdGVBbm8nKTtcbiAgICAgICAgLy9jdHgub25jaGFuZ2UoKTtcbiAgICB9XG5cbiAgICBjdHguZXN0ZU1lcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGFob3JhID0gbmV3IERhdGU7XG4gICAgICAgIHZhciBpbmljaWFsID0gb29yLmZlY2hhLnRvU1FMKCBvb3IuZmVjaGEuaW5pY2lhbE1lcyhhaG9yYSkgKTtcbiAgICAgICAgdmFyIGZpbmFsID0gb29yLmZlY2hhLnRvU1FMKCBvb3IuZmVjaGEuZmluYWxNZXMoYWhvcmEpICk7XG5cbiAgICAgICAgY3R4LmZlY2hhRGVzZGUoaW5pY2lhbCk7XG4gICAgICAgIGN0eC5mZWNoYUhhc3RhKGZpbmFsKTtcbiAgICAgICAgY3R4LnByZSgnZXN0ZU1lcycpO1xuICAgICAgICAvL2N0eC5vbmNoYW5nZSgpO1xuICAgIH1cblxuICAgIGN0eC5tZXNBbnRlcmlvciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGFob3JhID0gbmV3IERhdGU7XG4gICAgICAgIGFob3JhLnNldE1vbnRoKCBhaG9yYS5nZXRNb250aCgpIC0gMSk7XG4gICAgICAgIHZhciBpbmljaWFsID0gb29yLmZlY2hhLnRvU1FMKCBvb3IuZmVjaGEuaW5pY2lhbE1lcyhhaG9yYSkgKTtcbiAgICAgICAgdmFyIGZpbmFsID0gb29yLmZlY2hhLnRvU1FMKCBvb3IuZmVjaGEuZmluYWxNZXMoYWhvcmEpICk7XG5cbiAgICAgICAgY3R4LmZlY2hhRGVzZGUoaW5pY2lhbCk7XG4gICAgICAgIGN0eC5mZWNoYUhhc3RhKGZpbmFsKTtcbiAgICAgICAgY3R4LnByZSgnZXN0ZU1lcycpO1xuICAgICAgICAvL2N0eC5vbmNoYW5nZSgpO1xuICAgIH1cblxuICAgIGN0eC51bHRpbW9zRGlhcyA9IGZ1bmN0aW9uIChkaWFzKSB7XG4gICAgICAgIHZhciBhaG9yYSA9IG5ldyBEYXRlO1xuICAgICAgICB2YXIgZmluYWwgPSBvb3IuZmVjaGEudG9TUUwoYWhvcmEpO1xuICAgICAgICBhaG9yYS5zZXREYXRlKCBhaG9yYS5nZXREYXRlKCkgLSBkaWFzKTtcbiAgICAgICAgdmFyIGluaWNpYWwgPSBvb3IuZmVjaGEudG9TUUwoYWhvcmEpO1xuXG4gICAgICAgIGN0eC5mZWNoYURlc2RlKGluaWNpYWwpO1xuICAgICAgICBjdHguZmVjaGFIYXN0YShmaW5hbCk7XG4gICAgICAgIGN0eC5wcmUoJ2VzdGVNZXMnKTtcbiAgICAgICAgLy9jdHgub25jaGFuZ2UoKTtcbiAgICB9XG5cblxuICAgIGN0eC51bHRpbW9zMzBEaWFzID0gZnVuY3Rpb24gKGRpYXMpIHtcbiAgICAgICAgdmFyIGFob3JhID0gbmV3IERhdGU7XG4gICAgICAgIHZhciBmaW5hbCA9IG9vci5mZWNoYS50b1NRTChhaG9yYSk7XG4gICAgICAgIGFob3JhLnNldE1vbnRoKCBhaG9yYS5nZXRNb250aCgpIC0gMSk7XG4gICAgICAgIHZhciBpbmljaWFsID0gb29yLmZlY2hhLnRvU1FMKGFob3JhKTtcblxuICAgICAgICBjdHguZmVjaGFEZXNkZShpbmljaWFsKTtcbiAgICAgICAgY3R4LmZlY2hhSGFzdGEoZmluYWwpO1xuICAgICAgICBjdHgucHJlKCdlc3RlTWVzJylcbiAgICB9XG5cbiAgICBjdHgucGVyc29uYWxpemFkbyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY3R4LnByZSgncGVyc29uYWxpemFkbycpXG4gICAgfVxuXG4gICAgY3R4LmFzaWduYXJQcmUgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgaWYodmFsdWUgJiYgKHZhbHVlID0gY3R4W3ZhbHVlXSkpIHtcbiAgICAgICAgICAgIHZhbHVlLmNhbGwoKVxuICAgICAgICB9XG4gICAgfVxuXG5cbiAgICBjdHgudXBkYXRlID0gZnVuY3Rpb24gKGZuKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBmbigpO1xuICAgICAgICAgICAgY3R4Lm9uY2hhbmdlKCk7XG4gICAgICAgIH1cbiAgICB9O1xufTtcblxuXG5GZWNoYXNCYXIudmlldyA9IGZ1bmN0aW9uIChjdHgpIHtcbiAgICByZXR1cm4gbSgnLmZsZXgtcm93JywgW1xuXG4gICAgICAgICAgICBtKCcuYnRuLWdyb3VwLmRyb3Bkb3duLm10LWlucHV0ZXIuZGF0ZVJhbmdlJywge3N0eWxlOidmbGV4OjEgMSAxNWVtJ30sIFtcbiAgICAgICAgICAgICAgICBtKCdidXR0b24uYnRuLmJ0bi1kZWZhdWx0LmJ0bi1zbS5idG4tZmxhdCcsIHsnZGF0YS10b2dnbGUnOidkcm9wZG93bid9LCBbXG4gICAgICAgICAgICAgICAgICAgIG0oJy5zbWFsbCcsIG0oJ2kuZmEuZmEtY2hldnJvbi1kb3duJyksICcgUmFuZ28gZGUgRmVjaGFzJylcbiAgICAgICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgICAgIG0oJ3VsLmRyb3Bkb3duLW1lbnUnLCB7c3R5bGU6J2ZvbnQtc2l6ZToxNHB4J30sIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGknLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdhW2hyZWY9amF2YXNjcmlwdDo7XScsIG0oJ3N0cm9uZycsJ1BvciBtZXMnKSlcbiAgICAgICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICAgICAgbSgnbGknLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdhW2hyZWY9amF2YXNjcmlwdDo7XScsIHtvbmNsaWNrIDogY3R4LnVwZGF0ZShjdHguZXN0ZU1lcykgfSwnIC0gRXN0ZSBtZXMnKVxuICAgICAgICAgICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgICAgICAgICBtKCdsaScsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIG0oJ2FbaHJlZj1qYXZhc2NyaXB0OjtdJywge29uY2xpY2sgOiBjdHgudXBkYXRlKGN0eC5tZXNBbnRlcmlvcikgfSwgJyAtIE1lcyBhbnRlcmlvcicpXG4gICAgICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgICAgIG0oJ2xpJywgW1xuICAgICAgICAgICAgICAgICAgICAgICAgbSgnYVtocmVmPWphdmFzY3JpcHQ6O10nLCBtKCdzdHJvbmcnLCdQb3IgYcOxbycpKVxuICAgICAgICAgICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgICAgICAgICBtKCdsaScsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIG0oJ2FbaHJlZj1qYXZhc2NyaXB0OjtdJywge29uY2xpY2sgOiBjdHgudXBkYXRlKGN0eC5lc3RlQW5vKSB9LCAnIC0gRXN0ZSBhw7FvJylcbiAgICAgICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICAgICAgbSgnbGknLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdhW2hyZWY9amF2YXNjcmlwdDo7XScsIHtvbmNsaWNrIDogY3R4LnVwZGF0ZShjdHguYW5vQW50ZXJpb3IpIH0sICcgLSBBw7FvIGFudGVyaW9yJylcbiAgICAgICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICAgICAgbSgnbGknLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdhW2hyZWY9amF2YXNjcmlwdDo7XScsIG0oJ3N0cm9uZycsJ1JhbmdvIGRlIGZlY2hhcycpKVxuICAgICAgICAgICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgICAgICAgICBtKCdsaScsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIG0oJ2FbaHJlZj1qYXZhc2NyaXB0OjtdJywge29uY2xpY2sgOiBjdHgudXBkYXRlKGN0eC51bHRpbW9zMzBEaWFzKSB9LCAnIC0gw5psdGltb3MgMzAgZMOtYXMnKVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcblxuICAgICAgICBtKCcubXQtaW5wdXRlcicsIHtzdHlsZTonZmxleDozIDEgMTVlbSd9LFtcbiAgICAgICAgICAgIG0oJ2xhYmVsJywgJ0Rlc2RlOiAnKSxcbiAgICAgICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICAgICAgY29uZmlnIDogY3R4LnNlbGVjdG9yRmVjaGFEZXNkZSxcbiAgICAgICAgICAgICAgICBpZCA6IFwiZmVjaGFpXCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgIF0pLFxuXG4gICAgICAgIG0oJy5tdC1pbnB1dGVyJywge3N0eWxlOidmbGV4OjMgMSAxNWVtJ30sIFtcbiAgICAgICAgICAgIG0oJ2xhYmVsJywnSGFzdGE6ICcpLFxuICAgICAgICAgICAgbSgnaW5wdXRbdHlwZT10ZXh0XScsIHtcbiAgICAgICAgICAgICAgICBjb25maWcgOiBjdHguc2VsZWN0b3JGZWNoYUhhc3RhLFxuICAgICAgICAgICAgICAgIGlkIDogXCJmZWNoYWZcIlxuICAgICAgICAgICAgfSlcbiAgICAgICAgXSlcblxuICAgIF0pO1xufTtcblxuXG4vKlxudmFyIHZhbHVlID0gbS5wcm9wKCBvb3IuZmVjaGEudG9TUUwobmV3IERhdGUpICk7XG5cbmZ1bmN0aW9uIEQzRGF0ZUNvbmZpZyAoZWwsIGlzSW5pdGVkKSB7XG4gICAgaWYoaXNJbml0ZWQpIHJldHVybjtcbiAgICB2YXIgcG9wVXA7XG5cblxuICAgICQoZWwpLnZhbCggb29yLmZlY2hhLnByZXR0eUZvcm1hdCggdmFsdWUoKSApICk7XG5cbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHBvcyA9ICQoZWwpLm9mZnNldCgpO1xuICAgICAgICB2YXIgaGVpZ2h0ID0gJChlbCkuaGVpZ2h0KCk7XG4gICAgICAgIHZhciB3aWR0aCA9ICQoZWwpLndpZHRoKCk7XG5cbiAgICAgICAgcG9wVXAgJiYgcG9wVXAucmVtb3ZlKCk7XG5cbiAgICAgICAgcG9wVXAgPSBkMy5zZWxlY3QoZG9jdW1lbnQuYm9keSlcbiAgICAgICAgICAgICAgICAgICAgLmFwcGVuZCgnZGl2JylcbiAgICAgICAgICAgICAgICAgICAgLnN0eWxlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOidhYnNvbHV0ZScsXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDonMzAwcHgnLFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OicyMDBweCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOid3aGl0ZScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3AgIDogU3RyaW5nKDEwK2hlaWdodCtwb3MudG9wKS5jb25jYXQoJ3B4JyksXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0IDogU3RyaW5nKHBvcy5sZWZ0KS5jb25jYXQoJ3B4JyksXG4gICAgICAgICAgICAgICAgICAgICAgICAnei1pbmRleCcgOiA1MDAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyIDogJzFweCBzb2xpZCAjZjBmMGYwJ1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICB9KTtcblxuICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2JsdXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vcmVvbW92ZSgpO1xuICAgIH0pXG5cblxuICAgIGZ1bmN0aW9uIHJlbW92ZSAoKSB7XG4gICAgICAgIHBvcFVwLnJlbW92ZSgpXG4gICAgfVxufVxuKi9cblxuRDNEYXRlLm1vdW50ZXIgPSBmdW5jdGlvbiAoZGF0ZUZuLCBhdHRycykge1xuICAgIGlmKGF0dHJzLm1vZGVsKCkpIHsgZGF0ZUZuLnZhbHVlKCBhdHRycy5tb2RlbCgpICk7IH1cblxuICAgIHJldHVybiBmdW5jdGlvbiAoZWxlbWVudCwgaXNJbml0KSB7XG4gICAgICAgIGlmKGlzSW5pdCA9PSBmYWxzZSkge1xuICAgICAgICAgICAgaWYoYXR0cnMubW9kZWwoKSkgeyBkYXRlRm4udmFsdWUoIGF0dHJzLm1vZGVsKCkgKTsgfVxuICAgICAgICAgICAgZGF0ZUZuLmVsZW1lbnQoZWxlbWVudCk7XG5cbiAgICAgICAgICAgIGRhdGVGbi5vbmNoYW5nZSA9IGZ1bmN0aW9uICh2KSB7XG4gICAgICAgICAgICAgICAgYXR0cnMubW9kZWwodik7XG4gICAgICAgICAgICAgICAgYXR0cnMub25jaGFuZ2UgJiYgYXR0cnMub25jaGFuZ2Uodik7XG4gICAgICAgICAgICAgICAgbS5yZWRyYXcoKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICBpZihhdHRycy5tb2RlbCgpICE9IGRhdGVGbi52YWx1ZSgpKSB7XG4gICAgICAgICAgICBkYXRlRm4udmFsdWUoYXR0cnMubW9kZWwoKSk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuZnVuY3Rpb24gRDNEYXRlICgpIHtcbiAgICB2YXIgdmFsdWUgICAgID0gbS5wcm9wKCk7XG4gICAgdmFyIGZvcm1hdHRlZCA9IG0ucHJvcCgpO1xuICAgIHZhciBmb3JtYXQgICAgPSBvb3IuZmVjaGEucHJldHR5Rm9ybWF0LnllYXI7XG4gICAgdmFyIGVsZW1lbnQ7XG4gICAgdmFyIHBvcFVwO1xuXG4gICAgZGF0ZUZuLm9uY2hhbmdlID0gbS5wcm9wKCk7XG5cblxuICAgIGRhdGVGbi5wb3BVcCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBvcFVwO1xuICAgIH1cblxuICAgIGRhdGVGbi52YWx1ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoYXJndW1lbnRzLmxlbmd0aCkgIHtcbiAgICAgICAgICAgIHZhciB2ID0gdmFsdWUoKTtcbiAgICAgICAgICAgIHZhbHVlKGFyZ3VtZW50c1swXSk7XG4gICAgICAgICAgICBmb3JtYXR0ZWQoIGZvcm1hdCh2YWx1ZSgpKSApO1xuICAgICAgICAgICAgZWxlbWVudCAmJiAoZWxlbWVudC52YWx1ZSA9IGZvcm1hdHRlZCgpKTtcblxuICAgICAgICAgICAgaWYodiAhPSB2YWx1ZSgpICkge1xuICAgICAgICAgICAgICAgIGRhdGVGbi5vbmNoYW5nZSAmJiBkYXRlRm4ub25jaGFuZ2UodmFsdWUoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHZhbHVlKCk7XG4gICAgfVxuXG4gICAgZGF0ZUZuLmZvcm1hdHRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIGZvcm1hdHRlZCgpO1xuICAgIH1cblxuICAgIGRhdGVGbi5lbGVtZW50ID0gZnVuY3Rpb24gKGVsKSB7XG4gICAgICAgIG1vdW50KGVsLCBkYXRlRm4pO1xuICAgICAgICBlbGVtZW50ID0gZWw7XG4gICAgfVxuXG4gICAgZGF0ZUZuLmNsb3NlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihwb3BVcCkgcG9wVXAudHJhbnNpdGlvbigpLnN0eWxlKCdvcGFjaXR5JywgMCkucmVtb3ZlKCk7XG4gICAgICAgIHBvcFVwID0gbnVsbDtcbiAgICB9XG5cbiAgICBkYXRlRm4udXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBwb3BVcC5jYWxsKGRhdGVGbilcbiAgICB9XG5cbiAgICBkYXRlRm4ub3BlbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoIWVsZW1lbnQpIHJldHVybjtcblxuICAgICAgICB2YXIgZWwgPSBlbGVtZW50O1xuICAgICAgICB2YXIgcG9zID0gJChlbCkub2Zmc2V0KCk7XG4gICAgICAgIHZhciBoZWlnaHQgPSAkKGVsKS5oZWlnaHQoKTtcbiAgICAgICAgdmFyIHdpZHRoID0gJChlbCkud2lkdGgoKTtcbiAgICAgICAgdmFyIHRvcCA9IHBvcy50b3AgKyAxMCArIGhlaWdodDtcbiAgICAgICAgY2FwdHVyZUZvY3VzID0gZmFsc2U7XG4gICAgICAgIGlmKHBvcFVwKSByZXR1cm47XG5cbiAgICAgICAgcG9wVXAgPSBkMy5zZWxlY3QoZG9jdW1lbnQuYm9keSlcbiAgICAgICAgICAgICAgICAgICAgLmFwcGVuZCgnZGl2JylcbiAgICAgICAgICAgICAgICAgICAgLnN0eWxlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246J2Fic29sdXRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdwYWRkaW5nJyA6ICcyMHB4JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOicyNTBweCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6JzI4MHB4JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6J3doaXRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICd0ZXh0LWFsaWduJyA6ICdjZW50ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wICA6IFN0cmluZyhwb3MudG9wKS5jb25jYXQoJ3B4JyksXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0IDogU3RyaW5nKHBvcy5sZWZ0KS5jb25jYXQoJ3B4JyksXG4gICAgICAgICAgICAgICAgICAgICAgICAnei1pbmRleCcgOiA1MDAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyIDogJzFweCBzb2xpZCAjZjBmMGYwJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdib3gtc2hhZG93JyA6ICczcHggM3B4IDZweCByZ2JhKDAsMCwwLDAuMyknLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlci1yYWRpdXMnIDogJzhweCcsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG5cbiAgICAgICAgcG9wVXAudHJhbnNpdGlvbigpXG4gICAgICAgICAgICAuc3R5bGUoe29wYWNpdHkgOiAxLCB0b3AgOiBTdHJpbmcodG9wKS5jb25jYXQoJ3B4JykgfSlcblxuICAgICAgICBwb3BVcC5jYWxsKGRhdGVGbik7XG5cbiAgICAgICAgcmV0dXJuIHBvcFVwO1xuICAgIH1cblxuXG4gICAgdmFyIHdlZWtTdGFydCA9IDE7XG4gICAgdmFyIHdlZWtMZW4gICA9IDc7XG4gICAgdmFyIGRpYXMgPSBbJ0wnLCAnTScsICdYJywgJ0onLCAnVicsICdTJywgJ0QnXVxuXG4gICAgZnVuY3Rpb24gZGF0ZUZuIChzZWxlY3Rpb24sIGRhdGVDYWxlbmRhcikge1xuICAgICAgICB2YXIgdmFsU1FMID0gdmFsdWUoKTtcbiAgICAgICAgdmFyIHZhbDtcbiAgICAgICAgdmFyIGNhbGVuZGFyID0ge307XG5cbiAgICAgICAgaWYoZGF0ZUNhbGVuZGFyKSB7XG4gICAgICAgICAgICB2YWwgPSBkYXRlQ2FsZW5kYXJcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmKCF2YWxTUUwpIHtcbiAgICAgICAgICAgICAgICB2YWwgPSBvb3IuZmVjaGEudG9TUUwobmV3IERhdGUpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB2YWwgPSBvb3IuZmVjaGEuZnJvbVNRTCh2YWxTUUwpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiggc2VsZWN0aW9uLnNlbGVjdCgndGFibGUuY2FsZW5kYXInKS5lbXB0eSgpICkge1xuICAgICAgICAgICAgc2VsZWN0aW9uLmFwcGVuZCgnYnV0dG9uJylcbiAgICAgICAgICAgICAgICAuYXR0cignY2xhc3MnLCAncHVsbC1sZWZ0IGJ0biBidG4teHMgYnRuLWZsYXQgYnRuLWRlZmF1bHQnKVxuICAgICAgICAgICAgICAgIC5odG1sKCcmbGFxdW87Jyk7XG5cbiAgICAgICAgICAgIHNlbGVjdGlvbi5hcHBlbmQoJ2J1dHRvbicpXG4gICAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ3B1bGwtcmlnaHQgYnRuIGJ0bi14cyBidG4tZmxhdCBidG4tZGVmYXVsdCcpXG4gICAgICAgICAgICAgICAgLmh0bWwoJyZyYXF1bzsnKTtcblxuXG4gICAgICAgICAgICBzZWxlY3Rpb24uYXBwZW5kKCdoNicpXG5cblxuICAgICAgICAgICAgc2VsZWN0aW9uLmFwcGVuZCgndGFibGUnKS5hdHRyKCdjbGFzcycsICdjYWxlbmRhcicpXG4gICAgICAgICAgICBzZWxlY3Rpb24uc2VsZWN0KCd0YWJsZS5jYWxlbmRhcicpLmFwcGVuZCgndGhlYWQnKS5hcHBlbmQoJ3RyJyk7XG4gICAgICAgICAgICBzZWxlY3Rpb24uc2VsZWN0KCd0YWJsZS5jYWxlbmRhcicpLmFwcGVuZCgndGJvZHknKTtcblxuICAgICAgICAgICAgZm9yKHZhciBoPSAwOyBoPDc7IGgrKyl7XG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uLnNlbGVjdCgndGFibGUuY2FsZW5kYXIgdGhlYWQgdHInKS5hcHBlbmQoJ3RoJykudGV4dChkaWFzW2hdKVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzZWxlY3Rpb24uYXBwZW5kKCdidXR0b24nKVxuICAgICAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdidG4gYnRuLXhzIGJ0bi1mbGF0IGJ0bi1wcmltYXJ5JylcbiAgICAgICAgICAgICAgICAudGV4dCgnU2VsZWNjaW9uYXIgSG95JylcbiAgICAgICAgICAgICAgICAub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBkYXRlRm4udmFsdWUoIG9vci5mZWNoYS50b1NRTChuZXcgRGF0ZSkgKTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uLmNhbGwoZGF0ZUZuKTtcbiAgICAgICAgICAgICAgICAgICAgZGF0ZUZuLmNsb3NlKClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9XG5cblxuICAgICAgICBjYWxlbmRhci5zdGFydE1vbnRoID0gb29yLmZlY2hhLmluaWNpYWxNZXModmFsKTtcbiAgICAgICAgY2FsZW5kYXIuZW5kTW9udGggICA9IG9vci5mZWNoYS5maW5hbE1lcyh2YWwpO1xuICAgICAgICBjYWxlbmRhci5zdGFydERheSAgID0gY2FsZW5kYXIuc3RhcnRNb250aC5nZXREYXkoKTtcbiAgICAgICAgY2FsZW5kYXIuc3RhcnREaWZmICA9IGNhbGVuZGFyLnN0YXJ0TW9udGguZ2V0RGF5KCkgLSB3ZWVrU3RhcnQgO1xuXG4gICAgICAgIGlmKGNhbGVuZGFyLnN0YXJ0RGlmZiA8IDApIHtcbiAgICAgICAgICAgIGNhbGVuZGFyLnN0YXJ0RGlmZiA9IDcgICsgY2FsZW5kYXIuc3RhcnREaWZmO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FsZW5kYXIuc3RhcnREYXRlICA9IG5ldyBEYXRlKGNhbGVuZGFyLnN0YXJ0TW9udGgpO1xuICAgICAgICBjYWxlbmRhci5zdGFydERhdGUuc2V0RGF0ZSggY2FsZW5kYXIuc3RhcnREYXRlLmdldERhdGUoKSAtIGNhbGVuZGFyLnN0YXJ0RGlmZiApO1xuXG4gICAgICAgIGNhbGVuZGFyLndlZWtzID0gW107XG4gICAgICAgIHZhciB3ZWVrO1xuICAgICAgICB2YXIgY3VyckRhdGUgPSBuZXcgRGF0ZShjYWxlbmRhci5zdGFydERhdGUpO1xuXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGk8NjsgaSsrKVxuICAgICAgICB7XG4gICAgICAgICAgICBjYWxlbmRhci53ZWVrcy5wdXNoKFtdKVxuICAgICAgICAgICAgZm9yKHZhciBqPSAwOyBqPDc7IGorKylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjYWxlbmRhci53ZWVrc1tpXS5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgOiBvb3IuZmVjaGEudG9TUUwoY3VyckRhdGUpLFxuICAgICAgICAgICAgICAgICAgICBkYXRlICA6IGN1cnJEYXRlLmdldERhdGUoKSxcbiAgICAgICAgICAgICAgICAgICAgbW9udGggOiBjdXJyRGF0ZS5nZXRNb250aCgpICsgMVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgY3VyckRhdGUuc2V0RGF0ZSggY3VyckRhdGUuZ2V0RGF0ZSgpICsgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuXG4gICAgICAgIHNlbGVjdGlvbi5zZWxlY3QoJ2g2JykudGV4dCggb29yLmZlY2hhLmZ1bGxMYWJlbE1lcyhjYWxlbmRhci5zdGFydE1vbnRoLmdldE1vbnRoKCkgKyAxKSArICcgJyArIGNhbGVuZGFyLnN0YXJ0TW9udGguZ2V0RnVsbFllYXIoKSApXG5cbiAgICAgICAgdmFyIHJvd3MgPSBzZWxlY3Rpb24uc2VsZWN0KCd0YWJsZS5jYWxlbmRhciB0Ym9keScpLnNlbGVjdEFsbCgndHInKS5kYXRhKGNhbGVuZGFyLndlZWtzKVxuXG4gICAgICAgIHJvd3MuZW50ZXIoKS5hcHBlbmQoJ3RyJylcblxuICAgICAgICB2YXIgY2VsbHMgPSByb3dzLnNlbGVjdEFsbCgndGQnKS5kYXRhKGZ1bmN0aW9uIChkKSB7IHJldHVybiBkIH0pXG5cbiAgICAgICAgY2VsbHMuZW50ZXIoKS5hcHBlbmQoJ3RkJylcblxuICAgICAgICBjZWxscy50ZXh0KCBmKCdkYXRlJykgKTtcblxuICAgICAgICBjZWxscy5hdHRyKCdjbGFzcycsJ3RleHQtZ3JleScpO1xuXG4gICAgICAgIGNlbGxzLmZpbHRlciggZignbW9udGgnKS5pcyggY2FsZW5kYXIuc3RhcnRNb250aC5nZXRNb250aCgpICsgMSApICkuYXR0cignY2xhc3MnLCAnJylcblxuICAgICAgICBjZWxscy5maWx0ZXIoIGYoJ3ZhbHVlJykuaXModmFsU1FMKSApLmF0dHIoJ2NsYXNzJywgJ3NlbGVjdGVkJyk7XG5cbiAgICAgICAgY2VsbHMub24oJ2NsaWNrJywgZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgIGRhdGVGbi52YWx1ZShkLnZhbHVlKTtcbiAgICAgICAgICAgIHNlbGVjdGlvbi5jYWxsKGRhdGVGbik7XG4gICAgICAgICAgICBkYXRlRm4uY2xvc2UoKVxuICAgICAgICB9KTtcblxuXG4gICAgICAgIHNlbGVjdGlvbi5zZWxlY3QoJ2J1dHRvbi5wdWxsLXJpZ2h0JylcbiAgICAgICAgICAgIC5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIG5EYXRlID0gbmV3IERhdGUoZGF0ZUNhbGVuZGFyIHx8IHZhbCk7XG4gICAgICAgICAgICAgICAgbkRhdGUuc2V0TW9udGgoIG5EYXRlLmdldE1vbnRoKCkgKyAxKTtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb24uY2FsbChkYXRlRm4sIG5EYXRlKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIHNlbGVjdGlvbi5zZWxlY3QoJ2J1dHRvbi5wdWxsLWxlZnQnKVxuICAgICAgICAgICAgLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgbkRhdGUgPSBuZXcgRGF0ZShkYXRlQ2FsZW5kYXIgfHwgdmFsKTtcbiAgICAgICAgICAgICAgICBuRGF0ZS5zZXRNb250aCggbkRhdGUuZ2V0TW9udGgoKSAtIDEpO1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5jYWxsKGRhdGVGbiwgbkRhdGUpO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgICB2YXIgY2FkZW5hcyA9IHtcbiAgICAgICAgZGlhOiAnZGF0ZScsICAgZDonZGF0ZScsICBkaWFzOidkYXRlJyxcbiAgICAgICAgbWVzOiAnbW9udGgnLCAgbTonbW9udGgnLCBtZXNlczonbW9udGgnXG4gICAgfVxuXG4gICAgdmFyIG1ldG9kb3MgPSDCoHtcbiAgICAgICAgZGF0ZSAgOiB7c2V0IDogJ3NldERhdGUnLCAgZ2V0IDogJ2dldERhdGUnfSxcbiAgICAgICAgbW9udGggOiB7c2V0IDogJ3NldE1vbnRoJywgZ2V0IDogJ2dldE1vbnRoJ31cbiAgICB9XG5cbiAgICB2YXIgZmFjdG9yZXMgPSB7Jy0nIDogLTEsICcrJyA6IDF9O1xuXG4gICAgZGF0ZUZuLnBhcnNlID0gZnVuY3Rpb24gKHZhbCnCoHtcbiAgICAgICAgdmFsID0gdmFsLnNwbGl0KCcgJykuam9pbignJyk7XG4gICAgICAgIHZhbCA9IHZhbC5zcGxpdCgnLycpLmpvaW4oJy0nKVxuXG4gICAgICAgIGlmKG9vci5mZWNoYS52YWxpZGF0ZVNRTCh2YWwpKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGhveSA9IG5ldyBEYXRlLCBtYXRjaDtcbiAgICAgICAgaWYodmFsID09ICdoJyB8fCB2YWwgPT0gJ2hveScpIHtcbiAgICAgICAgICAgIHJldHVybiBvb3IuZmVjaGEudG9TUUwoaG95KVxuICAgICAgICB9XG5cbiAgICAgICAgaWYobWF0Y2ggPSB2YWwubWF0Y2goIC8oW1xcK3xcXC1dKShcXHMrKT8oXFxkezEsfSkoXFxzKyk/KFtBLVphLXpdKykvKSApIHtcbiAgICAgICAgICAgIHZhciBxdWUgICAgPSBtYXRjaFs1XS5sYXRpbml6ZSgpLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICB2YXIgZGF0ZSAgID0gbmV3IERhdGUoaG95KTtcbiAgICAgICAgICAgIHZhciBmYWN0b3IgPSBtYXRjaFsxXSA9PSAnLScgPyAtMSA6IDE7XG4gICAgICAgICAgICB2YXIgbW9udG8gID0gTnVtYmVyKG1hdGNoWzNdKTtcblxuICAgICAgICAgICAgaWYoIWNhZGVuYXNbcXVlXSkgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgcXVlID0gbWV0b2Rvc1sgY2FkZW5hc1txdWVdIF07XG5cbiAgICAgICAgICAgIGlmKCFxdWUpIHJldHVybiBmYWxzZTtcblxuICAgICAgICAgICAgZGF0ZVtxdWUuc2V0XS5jYWxsKGRhdGUsIGRhdGVbcXVlLmdldF0oKSArIChmYWN0b3IgKiBtb250bykgKTtcbiAgICAgICAgICAgIHJldHVybiBvb3IuZmVjaGEudG9TUUwoZGF0ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZihtYXRjaCA9IHZhbC5tYXRjaCggLyhcXGR7MSwyfSkoW0EtWmEtel17MywxNX0pKFxcZHsxLDR9KT8vKSApIHtcbiAgICAgICAgICAgIHZhciBkYXRlID0gbmV3IERhdGU7XG4gICAgICAgICAgICB2YXIgbk1lcyA9IG1hdGNoWzJdLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICB2YXIgbkFubyA9IG1hdGNoWzNdID8gTnVtYmVyKG1hdGNoWzNdKSA6IG51bGxcblxuICAgICAgICAgICAgdmFyIG1lcyA9IG9vci5mZWNoYS5tZXNlcygpLmZpbHRlciggZnVuY3Rpb24gKHMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcy50b0xvd2VyQ2FzZSgpLmluZGV4T2Yobk1lcykgPiAtMTtcbiAgICAgICAgICAgIH0pWzBdO1xuXG4gICAgICAgICAgICBpZighbWVzKSByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICBkYXRlLnNldE1vbnRoKG9vci5mZWNoYS5tZXNlcygpLmluZGV4T2YobWVzKSAtIDEpO1xuICAgICAgICAgICAgZGF0ZS5zZXREYXRlKG1hdGNoWzFdKTtcblxuICAgICAgICAgICAgaWYobkFubyAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGlmKG5Bbm8gPCAxMDAwKSB7XG4gICAgICAgICAgICAgICAgICAgIG5Bbm8gKz0gMjAwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGF0ZS5zZXRGdWxsWWVhcihuQW5vKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIG9vci5mZWNoYS50b1NRTChkYXRlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBkYXRlRm47XG59XG5cblxuXG5mdW5jdGlvbiBtb3VudChlbCwgZGF0ZSkge1xuICAgICQoZWwpLnZhbCggZGF0ZS5mb3JtYXR0ZWQoKSApO1xuXG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignZm9jdXMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGVsLnZhbHVlKSBlbC5zZXRTZWxlY3Rpb25SYW5nZSgwLGVsLnZhbHVlLmxlbmd0aClcbiAgICAgICAgZGF0ZS5vcGVuKCk7XG4gICAgfSk7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZCA9IGRhdGUucGFyc2UoZWwudmFsdWUpO1xuICAgICAgICBpZihkKSB7XG4gICAgICAgICAgICBkYXRlLnZhbHVlKGQpO1xuICAgICAgICB9XG4gICAgICAgIGRhdGUudXBkYXRlKCk7XG4gICAgfSk7XG5cblxuICAgIHZhciBjYXB0dXJlRm9jdXMgPSBmYWxzZTtcblxuICAgIGRvY3VtZW50LmJvZHkuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgaWYoZXYudGFyZ2V0IT0gZWwgJiYgZGF0ZS5wb3BVcCgpICYmIGNhcHR1cmVGb2N1cykge1xuICAgICAgICAgICAgdmFyIHAgPSBub2RlSXNQYXJlbnRPZihkYXRlLnBvcFVwKCkubm9kZSgpLCBldi50YXJnZXQpO1xuICAgICAgICAgICAgaWYoIXApwqBkYXRlLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sZmFsc2UpO1xuXG4gICAgZG9jdW1lbnQuYm9keS5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIGZ1bmN0aW9uIChldikge1xuICAgICAgICBpZihldi50YXJnZXQgIT0gZWwgJiYgZGF0ZS5wb3BVcCgpICYmIGNhcHR1cmVGb2N1cykge1xuICAgICAgICAgICAgdmFyIHAgPSBub2RlSXNQYXJlbnRPZihkYXRlLnBvcFVwKCkubm9kZSgpLCBldi50YXJnZXQpO1xuICAgICAgICAgICAgaWYoIXApwqBkYXRlLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0sdHJ1ZSk7XG5cbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdibHVyJywgZnVuY3Rpb24gKGV2KSB7IGNhcHR1cmVGb2N1cyA9IHRydWU7IH0pO1xuXG4gICAgZnVuY3Rpb24gbm9kZUlzUGFyZW50T2YocGFyZW50LCBjaGlsZCl7XG4gICAgICAgIHZhciBjaGlsZHJlbiA9IFtdO1xuICAgICAgICB2YXIgY3VyciA9IGNoaWxkO1xuXG4gICAgICAgIHdoaWxlKGN1cnIpe1xuICAgICAgICAgICAgY2hpbGRyZW4ucHVzaChjdXJyKTtcbiAgICAgICAgICAgIGlmKGN1cnIgPT0gcGFyZW50KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjdXJyID0gY3Vyci5vZmZzZXRQYXJlbnQ7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG59XG4iLCJcbnZhciBTZWFyY2hCYXIgPSBtb2R1bGUuZXhwb3J0cyA9IHt9O1xuXG5TZWFyY2hCYXIuY29udHJvbGxlciAgPSBmdW5jdGlvbiAoYXJncykge1xuICAgIHZhciBjdHggPSB0aGlzO1xuXG4gICAgY3R4LmludGVybmFsU2VhcmNoID0gbS5wcm9wKCk7XG4gICAgY3R4LnNlYXJjaCA9IGFyZ3Muc2VhcmNoO1xuICAgIGN0eC5pbnRlcm5hbFNlYXJjaCggY3R4LnNlYXJjaCgpICk7XG5cbiAgICBjdHgucGxhY2Vob2xkZXIgPSBtLnByb3AoYXJncy5wbGFjZWhvbGRlciB8fCAnQnVzY2FyJylcblxuICAgIHZhciBzZWFyY2hEZWJvdW5jZWQgPSBfLmRlYm91bmNlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY3R4LnBlcmZvcm1TZWFyY2goIGN0eC5pbnRlcm5hbFNlYXJjaCgpICk7XG4gICAgfSwgNTAwKTtcblxuICAgIGN0eC5wZXJmb3JtU2VhcmNoID0gZnVuY3Rpb24gKHZhbCkge1xuICAgICAgICBjdHguaW50ZXJuYWxTZWFyY2godmFsKVxuICAgICAgICBjdHguc2VhcmNoKHZhbClcblxuICAgICAgICBpZihhcmdzLm9uc2VhcmNoKSB7XG4gICAgICAgICAgICBhcmdzLm9uc2VhcmNoKCk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgY3R4LmJpbmRFdmVudCA9IGZ1bmN0aW9uIChlbGVtZW50LCBpc0luaXRpYWxpemVkKSB7XG4gICAgICAgIGlmKGFyZ3MuYXV0b2ZvY3VzKSBvb3IuYXV0b2ZvY3VzKGVsZW1lbnQsIGlzSW5pdGlhbGl6ZWQpO1xuICAgICAgICBpZihpc0luaXRpYWxpemVkKSByZXR1cm47XG5cbiAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdpbnB1dCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciB2YWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgICAgICAgICBjdHguaW50ZXJuYWxTZWFyY2godmFsdWUpO1xuICAgICAgICAgICAgc2VhcmNoRGVib3VuY2VkKCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuXG5TZWFyY2hCYXIudmlldyA9IGZ1bmN0aW9uIChjdHgpIHtcbiAgICByZXR1cm4gbSgnLm10LWlucHV0ZXInLCB7c3R5bGU6J2ZsZXg6MSAyJ30sIFtcbiAgICAgICAgbSgnbGFiZWwnLCB7c3R5bGU6J2ZvbnQtc2l6ZToxNXB4J30sIG0oJ2kuaW9uLWlvcy1zZWFyY2gnKSksXG4gICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICBzdHlsZSA6ICdoZWlnaHQ6MjNweCcsXG4gICAgICAgICAgICBwbGFjZWhvbGRlciA6IGN0eC5wbGFjZWhvbGRlcigpLFxuICAgICAgICAgICAgdmFsdWUgOiBjdHguaW50ZXJuYWxTZWFyY2goKSxcbiAgICAgICAgICAgIGNvbmZpZyA6IGN0eC5iaW5kRXZlbnQsXG4gICAgICAgICAgICBpZCA6ICdzZWFyY2gnLFxuICAgICAgICAgICAgb25jaGFuZ2UgOiBtLndpdGhBdHRyKCd2YWx1ZScsIGN0eC5wZXJmb3JtU2VhcmNoKVxuICAgICAgICB9KVxuICAgIF0pXG59XG4iLCJ2YXIgVGVyY2Vyb1NlbGVjdG9yID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcbnZhciBBU1NlbGVjdG9yICA9IHJlcXVpcmUoJy4uLy4uL21pc2MvQVNTZWxlY3RvcicpO1xuXG5UZXJjZXJvU2VsZWN0b3Iub25pbml0ID0gZnVuY3Rpb24gKHZub2RlKSB7XG4gICAgdGhpcy5kaXNhYmxlZCA9IG0ucHJvcCggQm9vbGVhbih2bm9kZS5hdHRycy50ZXJjZXJvKSApO1xuICAgIHRoaXMuc2V0Rm9jdXMgPSBtLnByb3AoZmFsc2UpO1xuXG4gICAgdGhpcy5zZXROdWxsID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2bm9kZS5zdGF0ZS5kaXNhYmxlZChmYWxzZSk7XG4gICAgICAgIHZub2RlLnN0YXRlLnNldEZvY3VzKHRydWUpO1xuICAgIH07XG59O1xuXG5UZXJjZXJvU2VsZWN0b3IuZ2V0RGF0YSA9IGZ1bmN0aW9uIChydW5DYWxsYmFjaykge1xuICAgIGlmKCEgVGVyY2Vyb1NlbGVjdG9yLmRhdGEgKSB7XG4gICAgICAgIFRlcmNlcm9TZWxlY3Rvci5kYXRhID0gb29yLnJlcXVlc3QoJy9hcGl2Mj9tb2RlbG89dGVyY2Vyb3MnKVxuICAgIH1cbiAgICBpZihUZXJjZXJvU2VsZWN0b3IuZGF0YSgpID09IHVuZGVmaW5lZCkgIHtcbiAgICAgICAgVGVyY2Vyb1NlbGVjdG9yLmRhdGEucnVuKHJ1bkNhbGxiYWNrKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBydW5DYWxsYmFjayggVGVyY2Vyb1NlbGVjdG9yLmRhdGEoKSApO1xuICAgIH1cbn1cblxuVGVyY2Vyb1NlbGVjdG9yLm9uY3JlYXRlID0gZnVuY3Rpb24gKHZub2RlKSB7XG4gICAgdm5vZGUuc3RhdGUuc2VsZWN0b3IgPSBDcmVhdGVTZWxlY3Rvcih2bm9kZSk7XG59O1xuXG5cblRlcmNlcm9TZWxlY3Rvci5vbnVwZGF0ZSA9IGZ1bmN0aW9uICh2bm9kZSkge1xuICAgIGlmKHZub2RlLnN0YXRlLnNldEZvY3VzKCkgPT0gdHJ1ZSkge1xuICAgICAgICB2bm9kZS5zdGF0ZS5zZXRGb2N1cyhmYWxzZSk7XG4gICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiAoKSB7IHZub2RlLmRvbS5xdWVyeVNlbGVjdG9yKCdpbnB1dC50c2EnKS5mb2N1cygpfSk7XG4gICAgfVxufTtcblxuXG5cblRlcmNlcm9TZWxlY3Rvci52aWV3ID0gZnVuY3Rpb24gKHZub2RlKSB7XG4gICAgcmV0dXJuIG0oJ2Rpdi5vb3Itc2VsZWN0b3ItdGVyY2VybycsIFtcbiAgICAgICAgbSgnLm10LWlucHV0ZXInLCBbXG4gICAgICAgICAgICBtKCdsYWJlbCcsICdDbGllbnRlOiAnKSxcblxuICAgICAgICAgICAgbSgnaW5wdXRbdHlwZT10ZXh0XS50c2EnLCB7XG4gICAgICAgICAgICAgICAgZGlzYWJsZWQgOiB0aGlzLmRpc2FibGVkKClcbiAgICAgICAgICAgIH0pLFxuXG4gICAgICAgICAgICBtKCdidXR0b24uYnRuLWZsYXQuYnRuLXhzLnB1bGwtbGVmdCcse1xuICAgICAgICAgICAgICAgIHN0eWxlIDoge1xuICAgICAgICAgICAgICAgICAgICAnbWFyZ2luLXJpZ2h0JzonMTBweCcsXG4gICAgICAgICAgICAgICAgICAgICdkaXNwbGF5JyA6IHRoaXMuZGlzYWJsZWQoKSA/ICdpbmhlcml0JyA6ICdub25lJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb25jbGljayA6IHRoaXMuc2V0TnVsbFxuICAgICAgICAgICAgfSwnw5cnKSxcbiAgICAgICAgXSksXG4gICAgICAgIHZub2RlLmF0dHJzLm1vZGVsKCkgPyB2bm9kZS5zdGF0ZS50ZXJjZXJvVmlldyh2bm9kZSkgOiBudWxsXG4gICAgXSk7XG59O1xuXG5cblRlcmNlcm9TZWxlY3Rvci50ZXJjZXJvVmlldyA9IGZ1bmN0aW9uICh2bm9kZSkge1xuICAgIHJldHVybiBtKCdbc3R5bGU9cGFkZGluZzoxMHB4XScsIFtcbiAgICAgICAgbSgnaDUnLCB7c3R5bGU6J21hcmdpbjowcHgnfSwgW1xuICAgICAgICAgICAgbSgnc3Ryb25nJywgdm5vZGUuYXR0cnMudGVyY2Vyby5jb2RpZ28pLFxuICAgICAgICAgICAgJyDigJQgJyxcbiAgICAgICAgICAgIHZub2RlLmF0dHJzLnRlcmNlcm8ubm9tYnJlXG4gICAgICAgIF0pLFxuICAgICAgICBtKCdkaXYnLCB7c3R5bGU6J3BhZGRpbmctdG9wOjRweCd9LFtcbiAgICAgICAgICAgIG0oJ3NwYW4udGV4dC1ncmV5LnNtYWxsJywgJ1JGQzogJyksXG4gICAgICAgICAgICBtKCdzcGFuJywgdm5vZGUuYXR0cnMudGVyY2Vyby5jbGF2ZV9maXNjYWwpLFxuICAgICAgICBdKVxuICAgIF0pXG59XG5cblxuZnVuY3Rpb24gQ3JlYXRlU2VsZWN0b3IgKHZub2RlKSB7XG4gICAgdmFyIHRTZWxlY3RvciA9IEFTU2VsZWN0b3Iodm5vZGUuZG9tLnF1ZXJ5U2VsZWN0b3IoJ2lucHV0LnRzYScpLCB7fSk7XG5cbiAgICB0U2VsZWN0b3IuY29tcHV0ZXIoU2VsZWN0b3JDb21wdXRlcik7XG5cbiAgICB0U2VsZWN0b3Iub25jaGFuZ2UoZnVuY3Rpb24gKCkge1xuICAgICAgICB2bm9kZS5zdGF0ZS5kaXNhYmxlZCh0cnVlKVxuICAgICAgICB2bm9kZS5hdHRycy5vbmNoYW5nZS5jYWxsKHRTZWxlY3Rvcik7XG4gICAgICAgIG0ucmVkcmF3KCk7XG4gICAgfSk7XG5cbiAgICB0U2VsZWN0b3IuaHRtbChmdW5jdGlvbiAoZCkge1xuICAgICAgICB2YXIgc3RyID0gJzxkaXY+PGg1IHN0eWxlPVwibWFyZ2luOjBcIj48c3Ryb25nPicgICsgZC5kYXRhLmNvZGlnbyArICc8L3N0cm9uZz4nO1xuICAgICAgICBzdHIgKz0gJyDigJQgJyArIGQuZGF0YS5ub21icmU7XG4gICAgICAgIHN0ciArPSAnPC9oNT4nO1xuICAgICAgICBzdHIgKz0gJzxoNj4nICsgZC5kYXRhLmNsYXZlX2Zpc2NhbCA/IGQuZGF0YS5jbGF2ZV9maXNjYWwgOiAnJyArICc8L2g2PidcbiAgICAgICAgcmV0dXJuICBzdHI7XG4gICAgfSk7XG5cbiAgICB0U2VsZWN0b3IudmFsdWUodm5vZGUuYXR0cnMubW9kZWwoKSwgdm5vZGUuYXR0cnMudGVyY2VybyA/IHZub2RlLmF0dHJzLnRlcmNlcm8uY29kaWdvIDogJycpO1xuICAgIHRTZWxlY3Rvci51cGRhdGVJbnB1dCgpO1xufVxuXG5cbmZ1bmN0aW9uIFNlbGVjdG9yQ29tcHV0ZXIgKHNlbGVjdG9yLCBkb25lKSB7XG4gICAgc2VsZWN0b3IubWVzc2FnZSgpLmh0bWwoJ2NhcmdhbmRvIGRhdG9zLi4uJyk7XG5cbiAgICBUZXJjZXJvU2VsZWN0b3IuZ2V0RGF0YShmdW5jdGlvbiAoaURhdGEpIHtcbiAgICAgICAgc2VsZWN0b3IubWVzc2FnZSgpLmh0bWwoJycpXG5cbiAgICAgICAgdmFyIG9wZW4gID0gc2VsZWN0b3Iuc2VhcmNoaW5nKCkgPT09IGZhbHNlO1xuICAgICAgICB2YXIgc2VhcmNoU3RyaW5nID0gc2VsZWN0b3Iuc2VhcmNoU3RyaW5nKCk7XG5cbiAgICAgICAgdmFyIGRhdGEgPSBpRGF0YS5tYXAoZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgIHJldHVybiB7ZGF0YTpkLCBpZDpkLnRlcmNlcm9faWQsIHRleHQ6ZC5jb2RpZ28sICRzZWxlY3RhYmxlOnRydWUsICRzaG93OnRydWUsIHNlYXJjaDogU3RyaW5nKGQuY29kaWdvICsgJyAnICsgZC5ub21icmUgKyAnICcgKyBkLmNsYXZlX2Zpc2NhbCkudG9Mb3dlckNhc2UoKSB9O1xuICAgICAgICB9KTtcblxuICAgICAgICBpZihvcGVuICYmIHNlbGVjdG9yLnZhbHVlKCkpIHtcbiAgICAgICAgICAgIGRhdGEuZm9yRWFjaChmdW5jdGlvbiAoZCl7IGQuJHNob3cgPSBzZWxlY3Rvci52YWx1ZSgpID09IGQuaWQgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzZWFyY2hTdHJpbmcgPSBzZWFyY2hTdHJpbmcudG9Mb3dlckNhc2UoKS5sYXRpbml6ZSgpO1xuICAgICAgICAgICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChkKSB7IGQuJHNob3cgPSBkLnNlYXJjaC5pbmRleE9mKHNlYXJjaFN0cmluZykgPiAtMTsgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBzZWxlY3Rvci5zZXREYXRhKGRhdGEpO1xuICAgICAgICBkb25lKGRhdGEpO1xuICAgIH0pO1xuXG5cbn1cbiIsIm1vZHVsZS5leHBvcnRzID0gTXRNb2RhbDtcblxuTXRNb2RhbC5vcGVuZWQgPSBtLnByb3AoW10pO1xuXG5NdE1vZGFsLm9wZW4gPSBmdW5jdGlvbiAoY29tcG9uZW50KSB7XG4gICAgTXRNb2RhbC5vcGVuZWQoKS5wdXNoKE10TW9kYWwoY29tcG9uZW50KSk7XG4gfVxuXG5mdW5jdGlvbiBNdE1vZGFsIChjb21wb25lbnQpIHtcbiAgICBmdW5jdGlvbiB2aWV3KGN0cmwpIHtcbiAgICAgICAgcmV0dXJuIG0oJy5tdC1tb2RhbCcsIF8uZXh0ZW5kKHtjb25maWc6b3Blbn0sIGNvbXBvbmVudC5tb2RhbEF0dHJzKSwgW1xuICAgICAgICAgICAgbSgnLm10LW1vZGFsLXRvcCcsIHRvcChjdHJsKSksXG4gICAgICAgICAgICBtKCcubXQtbW9kYWwtY29udGVudCcsIGNvbnRlbnQoY3RybCkpLFxuICAgICAgICAgICAgbSgnLm10LW1vZGFsLWJvdHRvbScsIGJvdHRvbShjdHJsKSlcbiAgICAgICAgXSlcbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIG9wZW4gKGVsZW1lbnQsIGlzSW5pdGlhbGl6ZWQpIHtcbiAgICAgICAgaWYoIWlzSW5pdGlhbGl6ZWQgJiYgY29tcG9uZW50LmVsKSB7XG4gICAgICAgICAgICBtb2RhbC5lbGVtZW50ID0gZWxlbWVudDtcblxuICAgICAgICAgICAgdmFyIGVsID0gY29tcG9uZW50LmVsO1xuICAgICAgICAgICAgdmFyIGVsUmVjdD0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgICAgICAgIG1vZGFsLmVsUmVjdCA9IGVsUmVjdDtcblxuICAgICAgICAgICAgdmFyIHdpZHRoID0gZWxlbWVudC5vZmZzZXRXaWR0aDtcbiAgICAgICAgICAgIHZhciBoZWlnaHQgPSBlbGVtZW50Lm9mZnNldEhlaWdodDtcblxuICAgICAgICAgICAgdmFyIGJvZHlIZWlnaHQgPSBkb2N1bWVudC5ib2R5Lm9mZnNldEhlaWdodDtcbiAgICAgICAgICAgIHZhciBib2R5V2lkdGggPSBkb2N1bWVudC5ib2R5Lm9mZnNldFdpZHRoO1xuXG5cbiAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUudG9wID0gJycuY29uY2F0KGVsUmVjdC50b3AsJ3B4Jyk7XG4gICAgICAgICAgICBlbGVtZW50LnN0eWxlWydtYXJnaW4tdG9wJ10gPSAwO1xuXG4gICAgICAgICAgICBlbGVtZW50LnN0eWxlLmxlZnQgPSAnJy5jb25jYXQoZWxSZWN0LmxlZnQsJ3B4Jyk7XG4gICAgICAgICAgICBlbGVtZW50LnN0eWxlWydtYXJnaW4tbGVmdCddID0gMDtcblxuICAgICAgICAgICAgbW9kYWwuem9vbSA9ICdzY2FsZSgnXG4gICAgICAgICAgICBtb2RhbC56b29tICs9IGVsUmVjdC53aWR0aCAvIHdpZHRoO1xuICAgICAgICAgICAgbW9kYWwuem9vbSArPSAnLCAnO1xuICAgICAgICAgICAgbW9kYWwuem9vbSArPSBlbFJlY3QuaGVpZ2h0IC8gaGVpZ2h0O1xuICAgICAgICAgICAgbW9kYWwuem9vbSArPSAnKSc7XG5cbiAgICAgICAgICAgIGQzLnNlbGVjdChlbGVtZW50KVxuICAgICAgICAgICAgICAgIC5zdHlsZSgndHJhbnNmb3JtJywgbW9kYWwuem9vbSlcbiAgICAgICAgICAgICAgICAuc3R5bGUoJ3RyYW5zZm9ybS1vcmlnaW4nLCAnMCAwJylcbiAgICAgICAgICAgICAgICAuc3R5bGUoJ29wYWNpdHknLCAwKVxuICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKCkuZHVyYXRpb24oNTAwKVxuICAgICAgICAgICAgICAgICAgICAuc3R5bGUoJ3RyYW5zZm9ybScsICdzY2FsZSgxLCAxKScpXG4gICAgICAgICAgICAgICAgICAgIC5zdHlsZSgnb3BhY2l0eScsIDEpXG4gICAgICAgICAgICAgICAgICAgIC5zdHlsZSgnbGVmdCcsICcnICsgKGJvZHlXaWR0aC13aWR0aCkvMiArICdweCcgKVxuICAgICAgICAgICAgICAgICAgICAuc3R5bGUoJ3RvcCcsICcnLmNvbmNhdCgoYm9keUhlaWdodC1oZWlnaHQpLzIsJ3B4JykpXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjb250ZW50IChjdHJsKSB7XG4gICAgICAgIGlmICh0eXBlb2YgY29tcG9uZW50LmNvbnRlbnQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHJldHVybiBjb21wb25lbnQuY29udGVudChjdHJsKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY29tcG9uZW50LmNvbnRlbnQ7XG4gICAgfVxuXG5cbiAgICBmdW5jdGlvbiB0b3AgKGN0cmwpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBjb21wb25lbnQudG9wID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXR1cm4gY29tcG9uZW50LnRvcChjdHJsKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY29tcG9uZW50LnRvcDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBib3R0b20gKGN0cmwpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBjb21wb25lbnQuYm90dG9tID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXR1cm4gY29tcG9uZW50LmJvdHRvbShjdHJsKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY29tcG9uZW50LmJvdHRvbTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjbG9zZU1vZGFsKCkge1xuICAgICAgICB2YXIgaWR4ID0gTXRNb2RhbC5vcGVuZWQoKS5pbmRleE9mKG1vZGFsKTtcbiAgICAgICAgaWYoaWR4ID4gLTEpIE10TW9kYWwub3BlbmVkKCkuc3BsaWNlKGlkeCwxKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjbG9zZSAoKSB7XG4gICAgICAgIGlmKCFjb21wb25lbnQuZWwgfHwgIW1vZGFsLmVsZW1lbnQpIHJldHVybiBjbG9zZU1vZGFsKCk7XG5cbiAgICAgICAgLy9tLnN0YXJ0Q29tcHV0YXRpb24oKTtcblxuICAgICAgICBkMy5zZWxlY3QobW9kYWwuZWxlbWVudClcbiAgICAgICAgICAgIC50cmFuc2l0aW9uKCkuZHVyYXRpb24oNTAwKVxuICAgICAgICAgICAgICAgIC5zdHlsZSgnb3BhY2l0eScsIDApXG4gICAgICAgICAgICAgICAgLnR3ZWVuKCd0cmFuc2Zvcm0nLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBpID0gZDMuaW50ZXJwb2xhdGUoJ3NjYWxlKDEsIDEpJywgbW9kYWwuem9vbSkgO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHQpIHsgdGhpcy5zdHlsZVsndHJhbnNmb3JtJ10gPSBpKHQpOyB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuc3R5bGUoJ3RvcCcsICcnLmNvbmNhdChtb2RhbC5lbFJlY3QudG9wICsgJ3B4JykpXG4gICAgICAgICAgICAgICAgLnN0eWxlKCdsZWZ0JywgJycuY29uY2F0KG1vZGFsLmVsUmVjdC5sZWZ0ICsgJ3B4JykpXG4gICAgICAgICAgICAgICAgLmVhY2goJ2VuZCcsIF8ub25jZShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGNsb3NlTW9kYWwoKTtcbiAgICAgICAgICAgICAgICAgICAgbS5yZWRyYXcoKTtcbiAgICAgICAgICAgICAgICB9KSlcblxuICAgIH1cblxuICAgIHZhciBtb2RhbCA9IHtcbiAgICAgICAgY29udHJvbGxlciA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBjdHJsID0gbmV3IGNvbXBvbmVudC5jb250cm9sbGVyKGNvbXBvbmVudC5hcmdzKTtcbiAgICAgICAgICAgIGN0cmwuJG1vZGFsID0gbW9kYWw7XG4gICAgICAgICAgICByZXR1cm4gY3RybDtcbiAgICAgICAgfSxcbiAgICAgICAgdmlldyA6IHZpZXcsXG4gICAgICAgIGNsb3NlIDogY2xvc2VcbiAgICB9XG5cblxuICAgIGNvbnNvbGUubG9nKG1vZGFsKVxuXG4gICAgcmV0dXJuIG1vZGFsO1xufVxuXG5NdE1vZGFsLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgJChkb2N1bWVudCkua2V5dXAoZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgIGlmKGV2LmtleUNvZGUgPT0gMjcgJiYgTXRNb2RhbC5vcGVuZWQoKS5sZW5ndGgpIHtcbiAgICAgICAgICAgIE10TW9kYWwub3BlbmVkKClbTXRNb2RhbC5vcGVuZWQoKS5sZW5ndGggLTFdLmNsb3NlKClcbiAgICAgICAgfVxuICAgIH0pXG5cbiAgICBNdE1vZGFsLmluaXRpYWxpemUgPSBuaC5ub29wO1xufVxuXG5NdE1vZGFsLnZpZXcgPSBmdW5jdGlvbiAoKSB7XG4gICAgTXRNb2RhbC5pbml0aWFsaXplKCk7XG4gICAgcmV0dXJuIE10TW9kYWwub3BlbmVkKCkubGVuZ3RoID8gW010TW9kYWwub3ZlcmxheSgpLCBNdE1vZGFsLm9wZW5lZCgpXSA6ICcnO1xufVxuXG5NdE1vZGFsLm92ZXJsYXkgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtKCdkaXYubXQtbW9kYWwtb3ZlcmxheScpIH1cbiIsInJlcXVpcmUoJy4vbmFodWknKTtcblxudmFyIEZpZWxkID0gcmVxdWlyZSgnLi9uYWh1aS5maWVsZCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IERUYWJsZTtcblxuXG5mdW5jdGlvbiBjb21wdXRlSGVhZGluZyAoY29sdW1ucywgc2V0dGluZ3MpIHtcbiAgICBzZXR0aW5ncyB8fCAoc2V0dGluZ3MgPSB7fSk7XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gY29sdW1ucy5tYXAoY29tcHV0ZUhlYWRlcik7XG4gICAgICAgIGZ1bmN0aW9uIGNvbXB1dGVIZWFkZXIgIChjb2wpIHtcbiAgICAgICAgICAgIHZhciBjZWxkYSA9IHt9O1xuXG4gICAgICAgICAgICBjZWxkYS5uYW1lID0gY29sLm5hbWU7XG4gICAgICAgICAgICBjZWxkYS50ZXh0ID0gY29sLmNhcHRpb247XG5cbiAgICAgICAgICAgIGNlbGRhLmh0bWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNhcHRpb24gPSAnJztcbiAgICAgICAgICAgICAgICBpZihzZXR0aW5ncy5zb3J0ICYmIHNldHRpbmdzLnNvcnQoKSA9PSBjb2wubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXB0aW9uID0gJzxpIGNsYXNzPVwiJy5jb25jYXQoc2V0dGluZ3Muc29ydERpcmVjdGlvbigpID09ICcrJyA/ICdpb24tYXJyb3ctZG93bi1iJyA6ICdpb24tYXJyb3ctdXAtYicsICdcIj48L2k+ICcpOyBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNhcHRpb24gKyBjb2wuY2FwdGlvbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY2VsZGEudXBkYXRlID0gY29sLnVwZGF0ZUhlYWRpbmc7XG4gICAgICAgICAgICBjZWxkYS5lbnRlciA9IGNvbC5lbnRlckhlYWRpbmc7XG4gICAgICAgICAgICBjZWxkYS5jb2x1bW4gPSBjb2w7XG5cbiAgICAgICAgICAgIGlmKGNvbC5zdWJmaWVsZHMpIHtcbiAgICAgICAgICAgICAgICBjZWxkYS5zdWJmaWVsZHMgPSBjb2wuc3ViZmllbGRzLm1hcChjb21wdXRlSGVhZGVyKVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2VsZGE7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8qKlxuICogQXJtYXIgbG9zIGRhdG9zIGRlIGxhcyBjZWxkYXNcbiAqL1xuZnVuY3Rpb24gY29tcHV0ZURhdGEgKGNvbHVtbnMsIHNldHRpbmdzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChyb3cpIHtcbiAgICAgICAgcmV0dXJuIGNvbHVtbnMubWFwKGNvbXB1dGVDb2x1bW4pO1xuXG4gICAgICAgIGZ1bmN0aW9uIGNvbXB1dGVDb2x1bW4gKGNvbCkge1xuICAgICAgICAgICAgdmFyIGNlbGRhID0ge307XG5cbiAgICAgICAgICAgIGNlbGRhLm5hbWUgPSBjb2wubmFtZTtcbiAgICAgICAgICAgIGNlbGRhLmNvbHVtbiA9IGNvbDtcbiAgICAgICAgICAgIGNlbGRhLnJvdyA9IHJvdztcblxuICAgICAgICAgICAgY2VsZGEudmFsdWUgPSBjb2wudmFsdWUocm93KTtcbiAgICAgICAgICAgIGNlbGRhLnRleHQgPSBjb2wuZmlsdGVyKGNlbGRhLnZhbHVlKTtcblxuICAgICAgICAgICAgY2VsZGEudXBkYXRlID0gY29sLnVwZGF0ZTtcbiAgICAgICAgICAgIGNlbGRhLmVudGVyID0gY29sLmVudGVyO1xuXG4gICAgICAgICAgICBjZWxkYS5lbnRlckVkaXQgPSBjb2wuZW50ZXJFZGl0O1xuXG4gICAgICAgICAgICBpZihjb2wuc3ViZmllbGRzKSB7XG4gICAgICAgICAgICAgICAgY2VsZGEuc3ViZmllbGRzID0gY29sLnN1YmZpZWxkcy5tYXAoY29tcHV0ZUNvbHVtbilcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNlbGRhO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5cblxuZnVuY3Rpb24gRFRhYmxlICgpIHtcbiAgICB2YXIgY29sdW1ucywgcm93cywgcm93VXBkYXRlO1xuICAgIFxuICAgIHJvd1VwZGF0ZSA9IF8ubm9vcDsgXG5cbiAgICB2YXIgSEVBRCA9IHtcbiAgICAgICAgY29tcHV0ZSA6IGNvbXB1dGVIZWFkaW5nLFxuICAgICAgICBjZWxsVGFnIDogJ3RoJyxcbiAgICAgICAgZW50ZXIgOiBGaWVsZC5lbnRlckhlYWRpbmcsXG4gICAgICAgIHVwZGF0ZSA6IEZpZWxkLnVwZGF0ZUhlYWRpbmcsXG4gICAgICAgIGtleSA6IGZ1bmN0aW9uIChkLGkpIHtcbiAgICAgICAgICAgIHJldHVybiBpXG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIEJPRFkgPSB7XG4gICAgICAgIGNvbXB1dGUgOiBjb21wdXRlRGF0YSxcbiAgICAgICAgY2VsbFRhZyA6ICd0ZCcsXG4gICAgICAgIGVudGVyIDogRmllbGQuZW50ZXIsXG4gICAgICAgIHVwZGF0ZSA6IEZpZWxkLnVwZGF0ZSxcbiAgICAgICAga2V5IDogZignaWQnKVxuICAgIH07XG5cblxuICAgIGR0YWJsZS5rZXkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGFyZ3VtZW50cy5sZW5ndGggPT0gMCkgcmV0dXJuIEJPRFkua2V5O1xuICAgICAgICBCT0RZLmtleSA9IGFyZ3VtZW50c1swXTtcbiAgICAgICAgcmV0dXJuIGR0YWJsZTtcbiAgICB9O1xuXG5cbiAgICBkdGFibGUucm93VXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoID09IDApIHJldHVybiByb3dVcGRhdGU7XG4gICAgICAgIHJvd1VwZGF0ZSA9IGFyZ3VtZW50c1swXTtcbiAgICAgICAgcmV0dXJuIGR0YWJsZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGVSb3dzKHRIZWFkLCBjb2x1bW5zLCByb3dzLCBraW5kLCBzZXR0aW5ncykge1xuICAgICAgICB2YXIgZmlsYXMsIGNlbGRhcywgbnVldmFzQ2VsZGFzLCBmaWxhc0VudGVyLCBmaWxhc0VkaXQ7XG5cbiAgICAgICAgdmFyIHJvd0lkcyA9IHJvd3MubWFwKGtpbmQua2V5KTtcblxuICAgICAgICAvLyBTZSBjcmVhbiBsYXMgZmlsYXNcbiAgICAgICAgLy8gdmVyaWZpY2FuZG8gc2kgZXMgZWRpdGFibGVcbiAgICAgICAgZmlsYXMgPSB0SGVhZC5zZWxlY3RBbGwoJ3RyJykuZGF0YShyb3dzLCBmdW5jdGlvbiAoZCkgeyBcbiAgICAgICAgICAgIHJldHVybiAoIGYoJyRlZGl0JykoZCkgPyAnJGVkaXQ6JyA6ICcnKS5jb25jYXQoa2luZC5rZXkoZCkpO1xuICAgICAgICB9KTtcblxuICAgICAgICBmaWxhcy5leGl0KCkucmVtb3ZlKCk7XG5cbiAgICAgICAgZmlsYXMuZW50ZXIoKVxuICAgICAgICAgICAgLmFwcGVuZCgndHInKVxuICAgICAgICAgICAgLmF0dHIoJ3gtcm93Jywga2luZC5rZXkpXG5cbiAgICAgICAgLy9Tb3J0LCBoYXkgcXVlIHZlcmlmaWNhcmxvXG4gICAgICAgIGZpbGFzLnNvcnQoZnVuY3Rpb24gKHIxLCByMikge1xuICAgICAgICAgICAgcmV0dXJuIHJvd0lkcy5pbmRleE9mKGtpbmQua2V5KHIxKSkgLSByb3dJZHMuaW5kZXhPZihraW5kLmtleShyMikpO1xuICAgICAgICB9KVxuICAgICAgICAuY2FsbChkdGFibGUucm93VXBkYXRlKCkpXG5cbiAgICAgICAgLypcbiAgICAgICAgZmlsYXNFZGl0ID0gZmlsYXMuZmlsdGVyKGYoJyRlZGl0JykpLmNhbGwoZnVuY3Rpb24gKGZpbGFzKSB7XG4gICAgICAgICAgICAvL1NlIHNlbGVjY2lvbmFuIGxhcyBmaWxhcyB5IGxhcyBjZWxkYXNcbiAgICAgICAgICAgIGNlbGRhcyA9IGZpbGFzLnNlbGVjdEFsbChraW5kLmNlbGxUYWcpLmRhdGEoa2luZC5jb21wdXRlKGNvbHVtbnMpLCBmKCduYW1lJykpXG4gICAgICAgICAgICBudWV2YXNDZWxkYXMgPSBjZWxkYXMuZW50ZXIoKS5hcHBlbmQoa2luZC5jZWxsVGFnKVxuICAgICAgICB9KVxuICAgICAgICBmaWxhcy5maWx0ZXIoZnVuY3Rpb24gKGQpIHsgcmV0dXJuICFmKCckZWRpdCcpKGQpOyB9KVxuICAgICAgICAqL1xuICAgICAgICAvL1NlIHNlbGVjY2lvbmFuIGxhcyBmaWxhcyB5IGxhcyBjZWxkYXNcblxuICAgICAgICBjZWxkYXMgPSBmaWxhcy5zZWxlY3RBbGwoa2luZC5jZWxsVGFnKS5kYXRhKGtpbmQuY29tcHV0ZShjb2x1bW5zLCBzZXR0aW5ncyksIGYoJ25hbWUnKSk7XG5cbiAgICAgICAgbnVldmFzQ2VsZGFzID0gY2VsZGFzLmVudGVyKCkuYXBwZW5kKGtpbmQuY2VsbFRhZylcblxuICAgICAgICBudWV2YXNDZWxkYXNcbiAgICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKGMpIHsgcmV0dXJuIGMucm93ICYmIGYoJyRlZGl0JykoYy5yb3cpIH0pXG4gICAgICAgICAgICAuZWFjaChmdW5jdGlvbiAoYykgeyBkMy5zZWxlY3QodGhpcykuY2FsbCggYy5lbnRlckVkaXQgfHwgYy5lbnRlciB8fCBuaC5ub29wKSB9KVxuXG4gICAgICAgIC8vQXBsaWNhciBlbnRlciBhIGxhcyBjZWxkYXNcbiAgICAgICAgbnVldmFzQ2VsZGFzXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChjKSB7ICByZXR1cm4gIWMucm93IHx8ICFmKCckZWRpdCcpKGMucm93KSB9KVxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gQm9vbGVhbihjLmVudGVyKSA9PT0gdHJ1ZSB9KVxuICAgICAgICAgICAgLmVhY2goZnVuY3Rpb24gKGMpIHsgZDMuc2VsZWN0KHRoaXMpLmNhbGwoYy5lbnRlcik7IH0pXG5cbiAgICAgICAgbnVldmFzQ2VsZGFzXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChjKSB7ICByZXR1cm4gIWMucm93IHx8ICFmKCckZWRpdCcpKGMucm93KSB9KVxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gQm9vbGVhbihjLmVudGVyKSA9PT0gZmFsc2UgfSlcbiAgICAgICAgICAgIC5lYWNoKGZ1bmN0aW9uIChjKSB7IGQzLnNlbGVjdCh0aGlzKS5jYWxsKGtpbmQuZW50ZXIpOyB9KVxuXG4gICAgICAgIC8vQWN0dWFsaXphciBsYXMgY2VsZGFzIHF1ZSBubyB0aWVuZW4gVXBkYXRlRnVuY3Rpb25cbiAgICAgICAgY2VsZGFzXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChjKSB7ICByZXR1cm4gIWMucm93IHx8ICFmKCckZWRpdCcpKGMucm93KSB9KVxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gQm9vbGVhbihjLnVwZGF0ZSkgPT09IGZhbHNlIH0pXG4gICAgICAgICAgICAuY2FsbChraW5kLnVwZGF0ZSlcblxuICAgICAgICAvL0FjdHVhbGlhciBsYXMgY2VsZGFzIGNvbiBhY3R1YWxpemFkb3JcbiAgICAgICAgY2VsZGFzXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChjKSB7ICByZXR1cm4gIWMucm93IHx8ICFmKCckZWRpdCcpKGMucm93KSB9KVxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gQm9vbGVhbihjLnVwZGF0ZSkgPT09IHRydWUgfSlcbiAgICAgICAgICAgIC5lYWNoKGZ1bmN0aW9uIChjKSB7IGQzLnNlbGVjdCh0aGlzKS5jYWxsKGMudXBkYXRlKSB9KVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHRGb290ZXIgKHRGb290LCBjb2x1bW5zICwgc2V0dGluZ3MpIHtcbiAgICAgICAgaWYoIXNldHRpbmdzKSByZXR1cm47XG5cbiAgICAgICAgdmFyIGZpbGFzID0gdEZvb3Quc2VsZWN0QWxsKCd0cicpLmRhdGEoWzFdKTtcbiAgICAgICAgdmFyIHRoRW50ZXIgPSBmaWxhcy5lbnRlcigpLmFwcGVuZCgndHInKS5hcHBlbmQoJ3RoJylcblxuXG4gICAgICAgIHRoRW50ZXIuYXBwZW5kKCdzcGFuJylcblxuICAgICAgICB0aEVudGVyLmFwcGVuZCgnZGl2JykuYXR0cignY2xhc3MnLCAnYnRuLWdyb3VwJylcblxuXG4gICAgICAgIHRoRW50ZXIuc2VsZWN0KCcuYnRuLWdyb3VwJylcbiAgICAgICAgICAgIC5hcHBlbmQoJ2J1dHRvbicpXG4gICAgICAgICAgICAuYXR0cignY2xhc3MnLCAnYnRuIGJ0bi1kZWZhdWx0IGJ0bi14cyBidG4tZmxhdCcpXG4gICAgICAgICAgICAuYXR0cignZGF0YS1wYWdlLXByZXYnLCcnKVxuICAgICAgICAgICAgLmFwcGVuZCgnaScpXG4gICAgICAgICAgICAuYXR0cignY2xhc3MnLCdpb24tY2hldnJvbi1sZWZ0JylcblxuICAgICAgICB0aEVudGVyLnNlbGVjdCgnLmJ0bi1ncm91cCcpXG4gICAgICAgICAgICAuYXBwZW5kKCdidXR0b24nKVxuICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2J0biBidG4tZGVmYXVsdCBidG4teHMgYnRuLWZsYXQnKVxuICAgICAgICAgICAgLmF0dHIoJ2RhdGEtcGFnZS1uZXh0JywnJylcbiAgICAgICAgICAgIC5hcHBlbmQoJ2knKVxuICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywnaW9uLWNoZXZyb24tcmlnaHQnKVxuXG5cblxuICAgICAgICBmaWxhcy5zZWxlY3QoJ3RoJylcbiAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICcnKVxuICAgICAgICAgICAgLmF0dHIoJ2NvbHNwYW4nLCBjb2x1bW5zLmxlbmd0aCk7XG5cbiAgICAgICAgZmlsYXMuc2VsZWN0KCdzcGFuJykudGV4dChcbiAgICAgICAgICAgICcnLmNvbmNhdChcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5vZmZzZXQgKyAxLFxuICAgICAgICAgICAgICAgICcgXFx1MjAxNCAnLFxuICAgICAgICAgICAgICAgIE1hdGgubWluKHNldHRpbmdzLnRvdGFsLCBzZXR0aW5ncy5vZmZzZXQgKyBzZXR0aW5ncy5wYWdlU2l6ZSksIFxuICAgICAgICAgICAgICAgICcgZGUgJyxcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy50b3RhbFxuICAgICAgICAgICAgKVxuICAgICAgICApO1xuXG4gICAgfVxuXG5cblxuXG4gICAgZnVuY3Rpb24gZHRhYmxlICh0YWJsZSwgY29sdW1ucywgcm93cywgc2V0dGluZ3MpIHtcbiAgICAgICAgdGFibGUuY2xhc3NlZCgnbWVnYXRhYmxlJywgdHJ1ZSk7XG5cbiAgICAgICAgaWYodGFibGUuc2VsZWN0KCd0aGVhZCcpLmVtcHR5KCkpe1xuICAgICAgICAgICAgdGFibGUuYXBwZW5kKCd0aGVhZCcpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYodGFibGUuc2VsZWN0KCd0Ym9keScpLmVtcHR5KCkpIHtcbiAgICAgICAgICAgIHRhYmxlLmFwcGVuZCgndGJvZHknKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYodGFibGUuc2VsZWN0KCd0Zm9vdCcpLmVtcHR5KCkpIHtcbiAgICAgICAgICAgIHRhYmxlLmFwcGVuZCgndGZvb3QnKVxuICAgICAgICB9XG5cbiAgICAgICAgdGFibGUuc2VsZWN0KCd0Ym9keScpLmNhbGwodXBkYXRlUm93cywgY29sdW1ucywgcm93cywgQk9EWSwgc2V0dGluZ3MpO1xuXG4gICAgICAgIHRhYmxlLnNlbGVjdCgndGhlYWQnKS5jYWxsKHVwZGF0ZVJvd3MsIGNvbHVtbnMsIFsxXSwgSEVBRCwgc2V0dGluZ3MpO1xuXG4gICAgICAgIHRhYmxlLnNlbGVjdCgndGZvb3QnKS5jYWxsKHRGb290ZXIsIGNvbHVtbnMsIHNldHRpbmdzKVxuICAgIH1cblxuICAgIHJldHVybiBkdGFibGU7XG59XG4iLCJcbm1vZHVsZS5leHBvcnRzID0gRmllbGQ7XG5cbmZ1bmN0aW9uIEZpZWxkIChuYW1lLCBjb25maWcpIHtcbiAgICBpZighKHRoaXMgaW5zdGFuY2VvZiBGaWVsZCkpIHJldHVybiBuZXcgRmllbGQobmFtZSxjb25maWcpO1xuICAgIHZhciBmaWVsZCA9IHRoaXM7XG4gICAgaWYoIWNvbmZpZykgY29uZmlnID0ge307XG5cbiAgICBuaC5leHRlbmQoZmllbGQsIHtcbiAgICAgICAgbmFtZSA6IG5hbWUsXG4gICAgICAgIGNhcHRpb24gOiBjb25maWcuY2FwdGlvbiB8fCBuYW1lLFxuICAgICAgICB2YWx1ZSA6IG5oLmlzRnVuY3Rpb24oY29uZmlnLnZhbHVlKSA/IGNvbmZpZy52YWx1ZSA6IGYobmFtZSksXG4gICAgICAgIGZpbHRlciA6IG5oLmlzRnVuY3Rpb24oY29uZmlnLmZpbHRlcikgPyBjb25maWcuZmlsdGVyIDogbmguaWRlbnRpdHksXG4gICAgICAgIHNvcnRGaWx0ZXIgOiBuaC5pc0Z1bmN0aW9uKGNvbmZpZy5zb3J0RmlsdGVyKSA/IGNvbmZpZy5zb3J0RmlsdGVyIDogbmguaWRlbnRpdHlcbiAgICB9KTtcblxuICAgIGlmKGNvbmZpZy5lbnRlckVkaXQgPT09IHRydWUpIHtcbiAgICAgICAgZmllbGQuZW50ZXJFZGl0ID0gRmllbGQuZW50ZXJFZGl0XG4gICAgfVxuXG4gICAgLy9Db3BpYXIgdG9kbyBsbyBleHRyYSBxdWUgdGVuZ2EgZWwgY2FtcG9cbiAgICBPYmplY3Qua2V5cyhjb25maWcpLmZvckVhY2goZnVuY3Rpb24gKGspIHtcbiAgICAgICAgdHlwZW9mIGZpZWxkW2tdID09ICd1bmRlZmluZWQnICYmIChmaWVsZFtrXSA9IGNvbmZpZ1trXSk7XG4gICAgfSk7XG59XG5cbkZpZWxkLnVwZGF0ZUhlYWRpbmcgPSBmdW5jdGlvbiAoc2VsKSB7XG4gICAgc2VsLnNlbGVjdCgnLnQtdGl0bGUnKS5odG1sKCBmKCdodG1sJykgKVxufVxuXG5cbkZpZWxkLmVudGVySGVhZGluZyA9IGZ1bmN0aW9uIChkKSB7XG4gICAgZC5hcHBlbmQoJ2RpdicpXG4gICAgICAgIC5hdHRyKCdjbGFzcycsIGZ1bmN0aW9uIChjZWxkYSkge1xuICAgICAgICAgICAgcmV0dXJuICd0LXRpdGxlJy5jb25jYXQoJyAnLCBjZWxkYS5jb2x1bW4uaGVhZGluZ0NsYXNzIHx8ICcnKVxuICAgICAgICB9KVxuICAgICAgICAuYXR0cignZGF0YS1zb3J0LWJ5JywgZignbmFtZScpKVxuICAgICAgICAuc3R5bGUoJ2N1cnNvcicsICdwb2ludGVyJylcbn1cblxuRmllbGQudXBkYXRlID0gZnVuY3Rpb24gKHNlbGVjdGlvbikge1xuICAgIHNlbGVjdGlvbi50ZXh0KCBmKCd0ZXh0JykgKTtcbn1cblxuRmllbGQuZW50ZXIgPSBmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG4gICAgc2VsZWN0aW9uXG4gICAgICAgIC5hdHRyKCdjbGFzcycsIGZ1bmN0aW9uIChjZWxkYSkge1xuICAgICAgICAgICAgdmFyIGF0dHIgPSB0aGlzLmdldEF0dHJpYnV0ZSgnY2xhc3MnKSB8fCAnJztcbiAgICAgICAgICAgIHJldHVybiBhdHRyLmNvbmNhdCgnICcsJ2NlbGRhLScsY2VsZGEubmFtZSwgJyAnLCBjZWxkYS5jb2x1bW4uY2xhc3MgfHzCoCcnKTtcbiAgICAgICAgfSlcbiAgICAgICAgLmF0dHIoJ3gtY2VsbCcsIGYoJ25hbWUnKSlcbn1cblxuRmllbGQuZW50ZXJFZGl0ID0gZnVuY3Rpb24gKHNlbGVjdGlvbikge1xuICAgIHNlbGVjdGlvblxuICAgICAgICAuYXR0cigneC1jZWxsJywgZignbmFtZScpKVxuICAgICAgICAuYXBwZW5kKCdpbnB1dCcpXG4gICAgICAgICAgICAuYXR0cigneC1wcm9wJywgZignbmFtZScpKVxuICAgICAgICAgICAgLmF0dHIoJ3R5cGUnLCAndGV4dCcpXG5cbn1cblxuLyoqXG4gKiBMaW5rIFRvIFJlc291cmNlXG4gKi9cbkZpZWxkLmxpbmtUb1Jlc291cmNlID0gZnVuY3Rpb24gKG5hbWUsIGNvbmZpZykge1xuICAgIGNvbmZpZyA9IG5oLmV4dGVuZCh7XG4gICAgICAgIGVudGVyIDogRmllbGQubGlua1RvUmVzb3VyY2UuZW50ZXIsXG4gICAgICAgIHVwZGF0ZSA6ICBGaWVsZC5saW5rVG9SZXNvdXJjZS51cGRhdGVcbiAgICB9LCBjb25maWcgfHzCoHt9KTtcblxuICAgIHJldHVybiBuZXcgRmllbGQobmFtZSwgY29uZmlnKTtcbn07XG5cbkZpZWxkLmxpbmtUb1Jlc291cmNlLmVudGVyID0gZnVuY3Rpb24gKHNlbGVjdGlvbikge1xuXG4gICAgc2VsZWN0aW9uLmFwcGVuZCgnYScpLmF0dHIoJ2RhdGEtaWQnLCBmdW5jdGlvbiAoZCkge1xuICAgICAgICByZXR1cm4gZC5jb2x1bW4uaWQgPyBkLmNvbHVtbi5pZChkLnJvdykgOiBudWxsXG4gICAgfSkuY2FsbChGaWVsZC5lbnRlcik7XG59O1xuXG5GaWVsZC5saW5rVG9SZXNvdXJjZS51cGRhdGUgPSBmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG4gICAgc2VsZWN0aW9uLnNlbGVjdCgnYScpXG4gICAgICAgIC5hdHRyKCdocmVmJywgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQuY29sdW1uLnVybChkLnJvdykgfSlcbiAgICAgICAgLmNhbGwoRmllbGQudXBkYXRlKVxufTtcblxuLyoqXG4gKiBGaWVsZCBwYXJhIHR3aWNlXG4gKi9cbkZpZWxkLnR3aWNlID0gZnVuY3Rpb24gKG5hbWUsIGNvbmZpZykge1xuICAgIHZhciBzdWJmaWVsZHMgPSBjb25maWcuc3ViZmllbGRzLm1hcChmdW5jdGlvbiAoZikge1xuICAgICAgICByZXR1cm4gZjtcbiAgICB9KTtcblxuICAgIGNvbmZpZyA9IG5oLmV4dGVuZCh7XG4gICAgICAgIHVwZGF0ZUhlYWRpbmcgOiBGaWVsZC50d2ljZS51cGRhdGVIZWFkaW5nLFxuICAgICAgICBlbnRlckhlYWRpbmcgOiBGaWVsZC50d2ljZS5lbnRlckhlYWRpbmcsXG4gICAgICAgIGVudGVyIDogRmllbGQudHdpY2UuZW50ZXIsXG4gICAgICAgIHVwZGF0ZSA6IEZpZWxkLnR3aWNlLnVwZGF0ZVxuICAgIH0sIGNvbmZpZyB8fCB7fSwge1xuICAgICAgICB2YWx1ZSA6IG5oLm5vb3AsXG4gICAgICAgIHN1YmZpZWxkcyA6IHN1YmZpZWxkc1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIG5ldyBGaWVsZChuYW1lLCBjb25maWcpO1xufTtcblxuRmllbGQudHdpY2UudXBkYXRlID0gZnVuY3Rpb24gKHNlbGVjdGlvbikge1xuXG4gICAgc2VsZWN0aW9uLmRhdHVtKCkuc3ViZmllbGRzLmZvckVhY2goZnVuY3Rpb24gKGYsIGkpIHtcbiAgICAgICAgc2VsZWN0aW9uLnNlbGVjdCgnLnR3aWNlJy5jb25jYXQoaSsxKSlcbiAgICAgICAgICAgIC5kYXR1bShmKVxuICAgICAgICAgICAgLmNhbGwoZi51cGRhdGUgfHwgRmllbGQudXBkYXRlKVxuICAgIH0pO1xufTtcblxuRmllbGQudHdpY2UuZW50ZXIgPSBmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG4gICAgc2VsZWN0aW9uLmNhbGwoRmllbGQuZW50ZXIpO1xuXG4gICAgc2VsZWN0aW9uLmRhdHVtKCkuc3ViZmllbGRzLmZvckVhY2goZnVuY3Rpb24gKGYsIGkpIHtcbiAgICAgICAgdmFyIGVkaXRNb2RlID0gbmguZignJGVkaXQnKShzZWxlY3Rpb24uZGF0dW0oKS5yb3cpO1xuXG4gICAgICAgIHNlbGVjdGlvbi5hcHBlbmQoJ2RpdicpXG4gICAgICAgICAgICAuY2xhc3NlZCgndHdpY2UnLmNvbmNhdChpKzEpLCB0cnVlKVxuICAgICAgICAgICAgLmRhdHVtKGYpXG4gICAgICAgICAgICAuY2FsbCggKGVkaXRNb2RlICYmIGYuZW50ZXJFZGl0KSAgfHwgZi5lbnRlciB8fCBGaWVsZC5lbnRlciApXG4gICAgfSk7XG59O1xuXG5GaWVsZC50d2ljZS51cGRhdGVIZWFkaW5nICA9IGZ1bmN0aW9uIChzZWwpIHtcbiAgICBzZWwuZGF0dW0oKS5zdWJmaWVsZHMuZm9yRWFjaChmdW5jdGlvbiAoZixpKSB7XG4gICAgICAgIHNlbC5zZWxlY3QoJy50d2ljZScuY29uY2F0KGkrMSkpXG4gICAgICAgICAgICAuZGF0dW0oZilcbiAgICAgICAgICAgIC5jYWxsKGYudXBkYXRlSGVhZGluZyB8fCBGaWVsZC51cGRhdGVIZWFkaW5nKVxuICAgIH0pO1xufTtcblxuRmllbGQudHdpY2UuZW50ZXJIZWFkaW5nID0gZnVuY3Rpb24gKHNlbCkge1xuICAgIHNlbC5kYXR1bSgpLnN1YmZpZWxkcy5mb3JFYWNoKGZ1bmN0aW9uIChmLGkpIHtcbiAgICAgICAgc2VsLmFwcGVuZCgnZGl2JylcbiAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICd0d2ljZScuY29uY2F0KGkrMSkpXG4gICAgICAgICAgICAuZGF0dW0oZilcbiAgICAgICAgICAgIC5jYWxsKGYuZW50ZXJIZWFkaW5nIHx8IEZpZWxkLmVudGVySGVhZGluZylcbiAgICB9KVxufVxuIiwiKGZ1bmN0aW9uIChnbGIpIHtcblxuICAgIHZhciBuYWh1aSA9IHtcbiAgICAgICAgZ2xvYmFscyA6IFsnZiddXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIG5vb3AgKCkge1xuXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gX2lzRnVuY3Rpb24gKGQpIHtcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBkID09PSAnZnVuY3Rpb24nO1xuICAgIH07XG5cbiAgICBmdW5jdGlvbiBpZGVudGl0eSAoKSB7XG4gICAgICAgIHJldHVybiBhcmd1bWVudHNbMF1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBleHRlbmQgKHNvdXJjZSkge1xuICAgICAgICB2YXIgaTtcblxuICAgICAgICBbXS5mb3JFYWNoLmNhbGwoYXJndW1lbnRzLCBmdW5jdGlvbiAob2JqKSB7XG4gICAgICAgICAgICBpZihvYmogPT09IHNvdXJjZSkgcmV0dXJuO1xuXG4gICAgICAgICAgICBmb3IoaSBpbiBvYmopIHtcbiAgICAgICAgICAgICAgICBzb3VyY2VbaV0gPSBvYmpbaV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBzb3VyY2U7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKlxuICAgICAqL1xuICAgIGZ1bmN0aW9uIG1lbW9pemUgKGZuKSB7XG4gICAgICAgIHZhciBtZW0gPSB7fTtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICh2YWwpIHtcbiAgICAgICAgICAgIGlmKCFtZW1bdmFsXSkge1xuICAgICAgICAgICAgICAgIG1lbVt2YWxdID0gZm4odmFsKTtcblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1lbVt2YWxdXG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqXG4gICAgICovXG4gICAgZnVuY3Rpb24gZiAobmFtZSkge1xuICAgICAgICBmZ2V0LnN0ciA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG5hbWUgfTtcblxuICAgICAgICBmZ2V0LmlzID0gZnVuY3Rpb24gKHNlYXJjaCkge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIGZnZXQob2JqKSA9PSBzZWFyY2g7IH1cbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gZmdldDtcblxuICAgICAgICBmdW5jdGlvbiBmZ2V0IChvYmopIHtcbiAgICAgICAgICAgIHJldHVybiBfaXNGdW5jdGlvbihvYmpbbmFtZV0pID8gb2JqW25hbWVdKCkgOiBvYmpbbmFtZV07XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIHhQcm9wIChjYWxsYmFjaywgdmFsdWUpIHtcbiAgICAgICAgaWYoIWNhbGxiYWNrKSBjYWxsYmFjayA9IGlkZW50aXR5O1xuXG4gICAgICAgIHhwcm9wKHZhbHVlKTtcblxuICAgICAgICByZXR1cm4geHByb3A7XG5cbiAgICAgICAgZnVuY3Rpb24geHByb3AgKCkge1xuICAgICAgICAgICAgaWYoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIHZhbHVlID0gY2FsbGJhY2soYXJndW1lbnRzWzBdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGlQcm9wICh2YWx1ZSwgc2V0KSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiBpcHJvcCAoKSB7XG4gICAgICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgdmFsdWUgPSBzZXQoYXJndW1lbnRzWzBdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgZnVuY3Rpb24gZ2V0U2V0UHJvcCAodmFsdWUsIHNldHRlcikge1xuXG4gICAgfVxuXG5cbiAgICBleHRlbmQobmFodWksIHtcbiAgICAgICAgbm9vcCA6IG5vb3AsXG4gICAgICAgIGlkZW50aXR5IDogaWRlbnRpdHksXG4gICAgICAgIHhQcm9wIDogeFByb3AsXG4gICAgICAgIGlQcm9wIDogaVByb3AsXG4gICAgICAgIGYgOiBtZW1vaXplKGYpLFxuICAgICAgICBtZW1vaXplIDogbWVtb2l6ZSxcbiAgICAgICAgZXh0ZW5kIDogZXh0ZW5kLFxuICAgICAgICBpc0Z1bmN0aW9uIDogX2lzRnVuY3Rpb25cbiAgICB9KTtcblxuICAgIG5haHVpLmdsb2JhbHMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIGdsYltrZXldID0gbmFodWlba2V5XVxuICAgIH0pO1xuXG4gICAgZ2xiLm5haHVpID0gZ2xiLm5oID0gbmFodWk7XG5cbn0pLmNhbGwod2luZG93LCB3aW5kb3cpXG4iLCJcbnZhciBMYXRpbmlzZT17fTtMYXRpbmlzZS5sYXRpbl9tYXA9e1wiw4FcIjpcIkFcIixcIsSCXCI6XCJBXCIsXCLhuq5cIjpcIkFcIixcIuG6tlwiOlwiQVwiLFwi4bqwXCI6XCJBXCIsXCLhurJcIjpcIkFcIixcIuG6tFwiOlwiQVwiLFwix41cIjpcIkFcIixcIsOCXCI6XCJBXCIsXCLhuqRcIjpcIkFcIixcIuG6rFwiOlwiQVwiLFwi4bqmXCI6XCJBXCIsXCLhuqhcIjpcIkFcIixcIuG6qlwiOlwiQVwiLFwiw4RcIjpcIkFcIixcIseeXCI6XCJBXCIsXCLIplwiOlwiQVwiLFwix6BcIjpcIkFcIixcIuG6oFwiOlwiQVwiLFwiyIBcIjpcIkFcIixcIsOAXCI6XCJBXCIsXCLhuqJcIjpcIkFcIixcIsiCXCI6XCJBXCIsXCLEgFwiOlwiQVwiLFwixIRcIjpcIkFcIixcIsOFXCI6XCJBXCIsXCLHulwiOlwiQVwiLFwi4biAXCI6XCJBXCIsXCLIulwiOlwiQVwiLFwiw4NcIjpcIkFcIixcIuqcslwiOlwiQUFcIixcIsOGXCI6XCJBRVwiLFwix7xcIjpcIkFFXCIsXCLHolwiOlwiQUVcIixcIuqctFwiOlwiQU9cIixcIuqctlwiOlwiQVVcIixcIuqcuFwiOlwiQVZcIixcIuqculwiOlwiQVZcIixcIuqcvFwiOlwiQVlcIixcIuG4glwiOlwiQlwiLFwi4biEXCI6XCJCXCIsXCLGgVwiOlwiQlwiLFwi4biGXCI6XCJCXCIsXCLJg1wiOlwiQlwiLFwixoJcIjpcIkJcIixcIsSGXCI6XCJDXCIsXCLEjFwiOlwiQ1wiLFwiw4dcIjpcIkNcIixcIuG4iFwiOlwiQ1wiLFwixIhcIjpcIkNcIixcIsSKXCI6XCJDXCIsXCLGh1wiOlwiQ1wiLFwiyLtcIjpcIkNcIixcIsSOXCI6XCJEXCIsXCLhuJBcIjpcIkRcIixcIuG4klwiOlwiRFwiLFwi4biKXCI6XCJEXCIsXCLhuIxcIjpcIkRcIixcIsaKXCI6XCJEXCIsXCLhuI5cIjpcIkRcIixcIseyXCI6XCJEXCIsXCLHhVwiOlwiRFwiLFwixJBcIjpcIkRcIixcIsaLXCI6XCJEXCIsXCLHsVwiOlwiRFpcIixcIseEXCI6XCJEWlwiLFwiw4lcIjpcIkVcIixcIsSUXCI6XCJFXCIsXCLEmlwiOlwiRVwiLFwiyKhcIjpcIkVcIixcIuG4nFwiOlwiRVwiLFwiw4pcIjpcIkVcIixcIuG6vlwiOlwiRVwiLFwi4buGXCI6XCJFXCIsXCLhu4BcIjpcIkVcIixcIuG7glwiOlwiRVwiLFwi4buEXCI6XCJFXCIsXCLhuJhcIjpcIkVcIixcIsOLXCI6XCJFXCIsXCLEllwiOlwiRVwiLFwi4bq4XCI6XCJFXCIsXCLIhFwiOlwiRVwiLFwiw4hcIjpcIkVcIixcIuG6ulwiOlwiRVwiLFwiyIZcIjpcIkVcIixcIsSSXCI6XCJFXCIsXCLhuJZcIjpcIkVcIixcIuG4lFwiOlwiRVwiLFwixJhcIjpcIkVcIixcIsmGXCI6XCJFXCIsXCLhurxcIjpcIkVcIixcIuG4mlwiOlwiRVwiLFwi6p2qXCI6XCJFVFwiLFwi4bieXCI6XCJGXCIsXCLGkVwiOlwiRlwiLFwix7RcIjpcIkdcIixcIsSeXCI6XCJHXCIsXCLHplwiOlwiR1wiLFwixKJcIjpcIkdcIixcIsScXCI6XCJHXCIsXCLEoFwiOlwiR1wiLFwixpNcIjpcIkdcIixcIuG4oFwiOlwiR1wiLFwix6RcIjpcIkdcIixcIuG4qlwiOlwiSFwiLFwiyJ5cIjpcIkhcIixcIuG4qFwiOlwiSFwiLFwixKRcIjpcIkhcIixcIuKxp1wiOlwiSFwiLFwi4bimXCI6XCJIXCIsXCLhuKJcIjpcIkhcIixcIuG4pFwiOlwiSFwiLFwixKZcIjpcIkhcIixcIsONXCI6XCJJXCIsXCLErFwiOlwiSVwiLFwix49cIjpcIklcIixcIsOOXCI6XCJJXCIsXCLDj1wiOlwiSVwiLFwi4biuXCI6XCJJXCIsXCLEsFwiOlwiSVwiLFwi4buKXCI6XCJJXCIsXCLIiFwiOlwiSVwiLFwiw4xcIjpcIklcIixcIuG7iFwiOlwiSVwiLFwiyIpcIjpcIklcIixcIsSqXCI6XCJJXCIsXCLErlwiOlwiSVwiLFwixpdcIjpcIklcIixcIsSoXCI6XCJJXCIsXCLhuKxcIjpcIklcIixcIuqduVwiOlwiRFwiLFwi6p27XCI6XCJGXCIsXCLqnb1cIjpcIkdcIixcIuqeglwiOlwiUlwiLFwi6p6EXCI6XCJTXCIsXCLqnoZcIjpcIlRcIixcIuqdrFwiOlwiSVNcIixcIsS0XCI6XCJKXCIsXCLJiFwiOlwiSlwiLFwi4biwXCI6XCJLXCIsXCLHqFwiOlwiS1wiLFwixLZcIjpcIktcIixcIuKxqVwiOlwiS1wiLFwi6p2CXCI6XCJLXCIsXCLhuLJcIjpcIktcIixcIsaYXCI6XCJLXCIsXCLhuLRcIjpcIktcIixcIuqdgFwiOlwiS1wiLFwi6p2EXCI6XCJLXCIsXCLEuVwiOlwiTFwiLFwiyL1cIjpcIkxcIixcIsS9XCI6XCJMXCIsXCLEu1wiOlwiTFwiLFwi4bi8XCI6XCJMXCIsXCLhuLZcIjpcIkxcIixcIuG4uFwiOlwiTFwiLFwi4rGgXCI6XCJMXCIsXCLqnYhcIjpcIkxcIixcIuG4ulwiOlwiTFwiLFwixL9cIjpcIkxcIixcIuKxolwiOlwiTFwiLFwix4hcIjpcIkxcIixcIsWBXCI6XCJMXCIsXCLHh1wiOlwiTEpcIixcIuG4vlwiOlwiTVwiLFwi4bmAXCI6XCJNXCIsXCLhuYJcIjpcIk1cIixcIuKxrlwiOlwiTVwiLFwixYNcIjpcIk5cIixcIsWHXCI6XCJOXCIsXCLFhVwiOlwiTlwiLFwi4bmKXCI6XCJOXCIsXCLhuYRcIjpcIk5cIixcIuG5hlwiOlwiTlwiLFwix7hcIjpcIk5cIixcIsadXCI6XCJOXCIsXCLhuYhcIjpcIk5cIixcIsigXCI6XCJOXCIsXCLHi1wiOlwiTlwiLFwiw5FcIjpcIk5cIixcIseKXCI6XCJOSlwiLFwiw5NcIjpcIk9cIixcIsWOXCI6XCJPXCIsXCLHkVwiOlwiT1wiLFwiw5RcIjpcIk9cIixcIuG7kFwiOlwiT1wiLFwi4buYXCI6XCJPXCIsXCLhu5JcIjpcIk9cIixcIuG7lFwiOlwiT1wiLFwi4buWXCI6XCJPXCIsXCLDllwiOlwiT1wiLFwiyKpcIjpcIk9cIixcIsiuXCI6XCJPXCIsXCLIsFwiOlwiT1wiLFwi4buMXCI6XCJPXCIsXCLFkFwiOlwiT1wiLFwiyIxcIjpcIk9cIixcIsOSXCI6XCJPXCIsXCLhu45cIjpcIk9cIixcIsagXCI6XCJPXCIsXCLhu5pcIjpcIk9cIixcIuG7olwiOlwiT1wiLFwi4bucXCI6XCJPXCIsXCLhu55cIjpcIk9cIixcIuG7oFwiOlwiT1wiLFwiyI5cIjpcIk9cIixcIuqdilwiOlwiT1wiLFwi6p2MXCI6XCJPXCIsXCLFjFwiOlwiT1wiLFwi4bmSXCI6XCJPXCIsXCLhuZBcIjpcIk9cIixcIsafXCI6XCJPXCIsXCLHqlwiOlwiT1wiLFwix6xcIjpcIk9cIixcIsOYXCI6XCJPXCIsXCLHvlwiOlwiT1wiLFwiw5VcIjpcIk9cIixcIuG5jFwiOlwiT1wiLFwi4bmOXCI6XCJPXCIsXCLIrFwiOlwiT1wiLFwixqJcIjpcIk9JXCIsXCLqnY5cIjpcIk9PXCIsXCLGkFwiOlwiRVwiLFwixoZcIjpcIk9cIixcIsiiXCI6XCJPVVwiLFwi4bmUXCI6XCJQXCIsXCLhuZZcIjpcIlBcIixcIuqdklwiOlwiUFwiLFwixqRcIjpcIlBcIixcIuqdlFwiOlwiUFwiLFwi4rGjXCI6XCJQXCIsXCLqnZBcIjpcIlBcIixcIuqdmFwiOlwiUVwiLFwi6p2WXCI6XCJRXCIsXCLFlFwiOlwiUlwiLFwixZhcIjpcIlJcIixcIsWWXCI6XCJSXCIsXCLhuZhcIjpcIlJcIixcIuG5mlwiOlwiUlwiLFwi4bmcXCI6XCJSXCIsXCLIkFwiOlwiUlwiLFwiyJJcIjpcIlJcIixcIuG5nlwiOlwiUlwiLFwiyYxcIjpcIlJcIixcIuKxpFwiOlwiUlwiLFwi6py+XCI6XCJDXCIsXCLGjlwiOlwiRVwiLFwixZpcIjpcIlNcIixcIuG5pFwiOlwiU1wiLFwixaBcIjpcIlNcIixcIuG5plwiOlwiU1wiLFwixZ5cIjpcIlNcIixcIsWcXCI6XCJTXCIsXCLImFwiOlwiU1wiLFwi4bmgXCI6XCJTXCIsXCLhuaJcIjpcIlNcIixcIuG5qFwiOlwiU1wiLFwixaRcIjpcIlRcIixcIsWiXCI6XCJUXCIsXCLhubBcIjpcIlRcIixcIsiaXCI6XCJUXCIsXCLIvlwiOlwiVFwiLFwi4bmqXCI6XCJUXCIsXCLhuaxcIjpcIlRcIixcIsasXCI6XCJUXCIsXCLhua5cIjpcIlRcIixcIsauXCI6XCJUXCIsXCLFplwiOlwiVFwiLFwi4rGvXCI6XCJBXCIsXCLqnoBcIjpcIkxcIixcIsacXCI6XCJNXCIsXCLJhVwiOlwiVlwiLFwi6pyoXCI6XCJUWlwiLFwiw5pcIjpcIlVcIixcIsWsXCI6XCJVXCIsXCLHk1wiOlwiVVwiLFwiw5tcIjpcIlVcIixcIuG5tlwiOlwiVVwiLFwiw5xcIjpcIlVcIixcIseXXCI6XCJVXCIsXCLHmVwiOlwiVVwiLFwix5tcIjpcIlVcIixcIseVXCI6XCJVXCIsXCLhubJcIjpcIlVcIixcIuG7pFwiOlwiVVwiLFwixbBcIjpcIlVcIixcIsiUXCI6XCJVXCIsXCLDmVwiOlwiVVwiLFwi4bumXCI6XCJVXCIsXCLGr1wiOlwiVVwiLFwi4buoXCI6XCJVXCIsXCLhu7BcIjpcIlVcIixcIuG7qlwiOlwiVVwiLFwi4busXCI6XCJVXCIsXCLhu65cIjpcIlVcIixcIsiWXCI6XCJVXCIsXCLFqlwiOlwiVVwiLFwi4bm6XCI6XCJVXCIsXCLFslwiOlwiVVwiLFwixa5cIjpcIlVcIixcIsWoXCI6XCJVXCIsXCLhubhcIjpcIlVcIixcIuG5tFwiOlwiVVwiLFwi6p2eXCI6XCJWXCIsXCLhub5cIjpcIlZcIixcIsayXCI6XCJWXCIsXCLhubxcIjpcIlZcIixcIuqdoFwiOlwiVllcIixcIuG6glwiOlwiV1wiLFwixbRcIjpcIldcIixcIuG6hFwiOlwiV1wiLFwi4bqGXCI6XCJXXCIsXCLhuohcIjpcIldcIixcIuG6gFwiOlwiV1wiLFwi4rGyXCI6XCJXXCIsXCLhuoxcIjpcIlhcIixcIuG6ilwiOlwiWFwiLFwiw51cIjpcIllcIixcIsW2XCI6XCJZXCIsXCLFuFwiOlwiWVwiLFwi4bqOXCI6XCJZXCIsXCLhu7RcIjpcIllcIixcIuG7slwiOlwiWVwiLFwixrNcIjpcIllcIixcIuG7tlwiOlwiWVwiLFwi4bu+XCI6XCJZXCIsXCLIslwiOlwiWVwiLFwiyY5cIjpcIllcIixcIuG7uFwiOlwiWVwiLFwixblcIjpcIlpcIixcIsW9XCI6XCJaXCIsXCLhupBcIjpcIlpcIixcIuKxq1wiOlwiWlwiLFwixbtcIjpcIlpcIixcIuG6klwiOlwiWlwiLFwiyKRcIjpcIlpcIixcIuG6lFwiOlwiWlwiLFwixrVcIjpcIlpcIixcIsSyXCI6XCJJSlwiLFwixZJcIjpcIk9FXCIsXCLhtIBcIjpcIkFcIixcIuG0gVwiOlwiQUVcIixcIsqZXCI6XCJCXCIsXCLhtINcIjpcIkJcIixcIuG0hFwiOlwiQ1wiLFwi4bSFXCI6XCJEXCIsXCLhtIdcIjpcIkVcIixcIuqcsFwiOlwiRlwiLFwiyaJcIjpcIkdcIixcIsqbXCI6XCJHXCIsXCLKnFwiOlwiSFwiLFwiyapcIjpcIklcIixcIsqBXCI6XCJSXCIsXCLhtIpcIjpcIkpcIixcIuG0i1wiOlwiS1wiLFwiyp9cIjpcIkxcIixcIuG0jFwiOlwiTFwiLFwi4bSNXCI6XCJNXCIsXCLJtFwiOlwiTlwiLFwi4bSPXCI6XCJPXCIsXCLJtlwiOlwiT0VcIixcIuG0kFwiOlwiT1wiLFwi4bSVXCI6XCJPVVwiLFwi4bSYXCI6XCJQXCIsXCLKgFwiOlwiUlwiLFwi4bSOXCI6XCJOXCIsXCLhtJlcIjpcIlJcIixcIuqcsVwiOlwiU1wiLFwi4bSbXCI6XCJUXCIsXCLisbtcIjpcIkVcIixcIuG0mlwiOlwiUlwiLFwi4bScXCI6XCJVXCIsXCLhtKBcIjpcIlZcIixcIuG0oVwiOlwiV1wiLFwiyo9cIjpcIllcIixcIuG0olwiOlwiWlwiLFwiw6FcIjpcImFcIixcIsSDXCI6XCJhXCIsXCLhuq9cIjpcImFcIixcIuG6t1wiOlwiYVwiLFwi4bqxXCI6XCJhXCIsXCLhurNcIjpcImFcIixcIuG6tVwiOlwiYVwiLFwix45cIjpcImFcIixcIsOiXCI6XCJhXCIsXCLhuqVcIjpcImFcIixcIuG6rVwiOlwiYVwiLFwi4bqnXCI6XCJhXCIsXCLhuqlcIjpcImFcIixcIuG6q1wiOlwiYVwiLFwiw6RcIjpcImFcIixcIsefXCI6XCJhXCIsXCLIp1wiOlwiYVwiLFwix6FcIjpcImFcIixcIuG6oVwiOlwiYVwiLFwiyIFcIjpcImFcIixcIsOgXCI6XCJhXCIsXCLhuqNcIjpcImFcIixcIsiDXCI6XCJhXCIsXCLEgVwiOlwiYVwiLFwixIVcIjpcImFcIixcIuG2j1wiOlwiYVwiLFwi4bqaXCI6XCJhXCIsXCLDpVwiOlwiYVwiLFwix7tcIjpcImFcIixcIuG4gVwiOlwiYVwiLFwi4rGlXCI6XCJhXCIsXCLDo1wiOlwiYVwiLFwi6pyzXCI6XCJhYVwiLFwiw6ZcIjpcImFlXCIsXCLHvVwiOlwiYWVcIixcIsejXCI6XCJhZVwiLFwi6py1XCI6XCJhb1wiLFwi6py3XCI6XCJhdVwiLFwi6py5XCI6XCJhdlwiLFwi6py7XCI6XCJhdlwiLFwi6py9XCI6XCJheVwiLFwi4biDXCI6XCJiXCIsXCLhuIVcIjpcImJcIixcIsmTXCI6XCJiXCIsXCLhuIdcIjpcImJcIixcIuG1rFwiOlwiYlwiLFwi4baAXCI6XCJiXCIsXCLGgFwiOlwiYlwiLFwixoNcIjpcImJcIixcIsm1XCI6XCJvXCIsXCLEh1wiOlwiY1wiLFwixI1cIjpcImNcIixcIsOnXCI6XCJjXCIsXCLhuIlcIjpcImNcIixcIsSJXCI6XCJjXCIsXCLJlVwiOlwiY1wiLFwixItcIjpcImNcIixcIsaIXCI6XCJjXCIsXCLIvFwiOlwiY1wiLFwixI9cIjpcImRcIixcIuG4kVwiOlwiZFwiLFwi4biTXCI6XCJkXCIsXCLIoVwiOlwiZFwiLFwi4biLXCI6XCJkXCIsXCLhuI1cIjpcImRcIixcIsmXXCI6XCJkXCIsXCLhtpFcIjpcImRcIixcIuG4j1wiOlwiZFwiLFwi4bWtXCI6XCJkXCIsXCLhtoFcIjpcImRcIixcIsSRXCI6XCJkXCIsXCLJllwiOlwiZFwiLFwixoxcIjpcImRcIixcIsSxXCI6XCJpXCIsXCLIt1wiOlwialwiLFwiyZ9cIjpcImpcIixcIsqEXCI6XCJqXCIsXCLHs1wiOlwiZHpcIixcIseGXCI6XCJkelwiLFwiw6lcIjpcImVcIixcIsSVXCI6XCJlXCIsXCLEm1wiOlwiZVwiLFwiyKlcIjpcImVcIixcIuG4nVwiOlwiZVwiLFwiw6pcIjpcImVcIixcIuG6v1wiOlwiZVwiLFwi4buHXCI6XCJlXCIsXCLhu4FcIjpcImVcIixcIuG7g1wiOlwiZVwiLFwi4buFXCI6XCJlXCIsXCLhuJlcIjpcImVcIixcIsOrXCI6XCJlXCIsXCLEl1wiOlwiZVwiLFwi4bq5XCI6XCJlXCIsXCLIhVwiOlwiZVwiLFwiw6hcIjpcImVcIixcIuG6u1wiOlwiZVwiLFwiyIdcIjpcImVcIixcIsSTXCI6XCJlXCIsXCLhuJdcIjpcImVcIixcIuG4lVwiOlwiZVwiLFwi4rG4XCI6XCJlXCIsXCLEmVwiOlwiZVwiLFwi4baSXCI6XCJlXCIsXCLJh1wiOlwiZVwiLFwi4bq9XCI6XCJlXCIsXCLhuJtcIjpcImVcIixcIuqdq1wiOlwiZXRcIixcIuG4n1wiOlwiZlwiLFwixpJcIjpcImZcIixcIuG1rlwiOlwiZlwiLFwi4baCXCI6XCJmXCIsXCLHtVwiOlwiZ1wiLFwixJ9cIjpcImdcIixcIsenXCI6XCJnXCIsXCLEo1wiOlwiZ1wiLFwixJ1cIjpcImdcIixcIsShXCI6XCJnXCIsXCLJoFwiOlwiZ1wiLFwi4bihXCI6XCJnXCIsXCLhtoNcIjpcImdcIixcIselXCI6XCJnXCIsXCLhuKtcIjpcImhcIixcIsifXCI6XCJoXCIsXCLhuKlcIjpcImhcIixcIsSlXCI6XCJoXCIsXCLisahcIjpcImhcIixcIuG4p1wiOlwiaFwiLFwi4bijXCI6XCJoXCIsXCLhuKVcIjpcImhcIixcIsmmXCI6XCJoXCIsXCLhupZcIjpcImhcIixcIsSnXCI6XCJoXCIsXCLGlVwiOlwiaHZcIixcIsOtXCI6XCJpXCIsXCLErVwiOlwiaVwiLFwix5BcIjpcImlcIixcIsOuXCI6XCJpXCIsXCLDr1wiOlwiaVwiLFwi4bivXCI6XCJpXCIsXCLhu4tcIjpcImlcIixcIsiJXCI6XCJpXCIsXCLDrFwiOlwiaVwiLFwi4buJXCI6XCJpXCIsXCLIi1wiOlwiaVwiLFwixKtcIjpcImlcIixcIsSvXCI6XCJpXCIsXCLhtpZcIjpcImlcIixcIsmoXCI6XCJpXCIsXCLEqVwiOlwiaVwiLFwi4bitXCI6XCJpXCIsXCLqnbpcIjpcImRcIixcIuqdvFwiOlwiZlwiLFwi4bW5XCI6XCJnXCIsXCLqnoNcIjpcInJcIixcIuqehVwiOlwic1wiLFwi6p6HXCI6XCJ0XCIsXCLqna1cIjpcImlzXCIsXCLHsFwiOlwialwiLFwixLVcIjpcImpcIixcIsqdXCI6XCJqXCIsXCLJiVwiOlwialwiLFwi4bixXCI6XCJrXCIsXCLHqVwiOlwia1wiLFwixLdcIjpcImtcIixcIuKxqlwiOlwia1wiLFwi6p2DXCI6XCJrXCIsXCLhuLNcIjpcImtcIixcIsaZXCI6XCJrXCIsXCLhuLVcIjpcImtcIixcIuG2hFwiOlwia1wiLFwi6p2BXCI6XCJrXCIsXCLqnYVcIjpcImtcIixcIsS6XCI6XCJsXCIsXCLGmlwiOlwibFwiLFwiyaxcIjpcImxcIixcIsS+XCI6XCJsXCIsXCLEvFwiOlwibFwiLFwi4bi9XCI6XCJsXCIsXCLItFwiOlwibFwiLFwi4bi3XCI6XCJsXCIsXCLhuLlcIjpcImxcIixcIuKxoVwiOlwibFwiLFwi6p2JXCI6XCJsXCIsXCLhuLtcIjpcImxcIixcIsWAXCI6XCJsXCIsXCLJq1wiOlwibFwiLFwi4baFXCI6XCJsXCIsXCLJrVwiOlwibFwiLFwixYJcIjpcImxcIixcIseJXCI6XCJsalwiLFwixb9cIjpcInNcIixcIuG6nFwiOlwic1wiLFwi4bqbXCI6XCJzXCIsXCLhup1cIjpcInNcIixcIuG4v1wiOlwibVwiLFwi4bmBXCI6XCJtXCIsXCLhuYNcIjpcIm1cIixcIsmxXCI6XCJtXCIsXCLhta9cIjpcIm1cIixcIuG2hlwiOlwibVwiLFwixYRcIjpcIm5cIixcIsWIXCI6XCJuXCIsXCLFhlwiOlwiblwiLFwi4bmLXCI6XCJuXCIsXCLItVwiOlwiblwiLFwi4bmFXCI6XCJuXCIsXCLhuYdcIjpcIm5cIixcIse5XCI6XCJuXCIsXCLJslwiOlwiblwiLFwi4bmJXCI6XCJuXCIsXCLGnlwiOlwiblwiLFwi4bWwXCI6XCJuXCIsXCLhtodcIjpcIm5cIixcIsmzXCI6XCJuXCIsXCLDsVwiOlwiblwiLFwix4xcIjpcIm5qXCIsXCLDs1wiOlwib1wiLFwixY9cIjpcIm9cIixcIseSXCI6XCJvXCIsXCLDtFwiOlwib1wiLFwi4buRXCI6XCJvXCIsXCLhu5lcIjpcIm9cIixcIuG7k1wiOlwib1wiLFwi4buVXCI6XCJvXCIsXCLhu5dcIjpcIm9cIixcIsO2XCI6XCJvXCIsXCLIq1wiOlwib1wiLFwiyK9cIjpcIm9cIixcIsixXCI6XCJvXCIsXCLhu41cIjpcIm9cIixcIsWRXCI6XCJvXCIsXCLIjVwiOlwib1wiLFwiw7JcIjpcIm9cIixcIuG7j1wiOlwib1wiLFwixqFcIjpcIm9cIixcIuG7m1wiOlwib1wiLFwi4bujXCI6XCJvXCIsXCLhu51cIjpcIm9cIixcIuG7n1wiOlwib1wiLFwi4buhXCI6XCJvXCIsXCLIj1wiOlwib1wiLFwi6p2LXCI6XCJvXCIsXCLqnY1cIjpcIm9cIixcIuKxulwiOlwib1wiLFwixY1cIjpcIm9cIixcIuG5k1wiOlwib1wiLFwi4bmRXCI6XCJvXCIsXCLHq1wiOlwib1wiLFwix61cIjpcIm9cIixcIsO4XCI6XCJvXCIsXCLHv1wiOlwib1wiLFwiw7VcIjpcIm9cIixcIuG5jVwiOlwib1wiLFwi4bmPXCI6XCJvXCIsXCLIrVwiOlwib1wiLFwixqNcIjpcIm9pXCIsXCLqnY9cIjpcIm9vXCIsXCLJm1wiOlwiZVwiLFwi4baTXCI6XCJlXCIsXCLJlFwiOlwib1wiLFwi4baXXCI6XCJvXCIsXCLIo1wiOlwib3VcIixcIuG5lVwiOlwicFwiLFwi4bmXXCI6XCJwXCIsXCLqnZNcIjpcInBcIixcIsalXCI6XCJwXCIsXCLhtbFcIjpcInBcIixcIuG2iFwiOlwicFwiLFwi6p2VXCI6XCJwXCIsXCLhtb1cIjpcInBcIixcIuqdkVwiOlwicFwiLFwi6p2ZXCI6XCJxXCIsXCLKoFwiOlwicVwiLFwiyYtcIjpcInFcIixcIuqdl1wiOlwicVwiLFwixZVcIjpcInJcIixcIsWZXCI6XCJyXCIsXCLFl1wiOlwiclwiLFwi4bmZXCI6XCJyXCIsXCLhuZtcIjpcInJcIixcIuG5nVwiOlwiclwiLFwiyJFcIjpcInJcIixcIsm+XCI6XCJyXCIsXCLhtbNcIjpcInJcIixcIsiTXCI6XCJyXCIsXCLhuZ9cIjpcInJcIixcIsm8XCI6XCJyXCIsXCLhtbJcIjpcInJcIixcIuG2iVwiOlwiclwiLFwiyY1cIjpcInJcIixcIsm9XCI6XCJyXCIsXCLihoRcIjpcImNcIixcIuqcv1wiOlwiY1wiLFwiyZhcIjpcImVcIixcIsm/XCI6XCJyXCIsXCLFm1wiOlwic1wiLFwi4bmlXCI6XCJzXCIsXCLFoVwiOlwic1wiLFwi4bmnXCI6XCJzXCIsXCLFn1wiOlwic1wiLFwixZ1cIjpcInNcIixcIsiZXCI6XCJzXCIsXCLhuaFcIjpcInNcIixcIuG5o1wiOlwic1wiLFwi4bmpXCI6XCJzXCIsXCLKglwiOlwic1wiLFwi4bW0XCI6XCJzXCIsXCLhtopcIjpcInNcIixcIsi/XCI6XCJzXCIsXCLJoVwiOlwiZ1wiLFwi4bSRXCI6XCJvXCIsXCLhtJNcIjpcIm9cIixcIuG0nVwiOlwidVwiLFwixaVcIjpcInRcIixcIsWjXCI6XCJ0XCIsXCLhubFcIjpcInRcIixcIsibXCI6XCJ0XCIsXCLItlwiOlwidFwiLFwi4bqXXCI6XCJ0XCIsXCLisaZcIjpcInRcIixcIuG5q1wiOlwidFwiLFwi4bmtXCI6XCJ0XCIsXCLGrVwiOlwidFwiLFwi4bmvXCI6XCJ0XCIsXCLhtbVcIjpcInRcIixcIsarXCI6XCJ0XCIsXCLKiFwiOlwidFwiLFwixadcIjpcInRcIixcIuG1ulwiOlwidGhcIixcIsmQXCI6XCJhXCIsXCLhtIJcIjpcImFlXCIsXCLHnVwiOlwiZVwiLFwi4bW3XCI6XCJnXCIsXCLJpVwiOlwiaFwiLFwiyq5cIjpcImhcIixcIsqvXCI6XCJoXCIsXCLhtIlcIjpcImlcIixcIsqeXCI6XCJrXCIsXCLqnoFcIjpcImxcIixcIsmvXCI6XCJtXCIsXCLJsFwiOlwibVwiLFwi4bSUXCI6XCJvZVwiLFwiyblcIjpcInJcIixcIsm7XCI6XCJyXCIsXCLJulwiOlwiclwiLFwi4rG5XCI6XCJyXCIsXCLKh1wiOlwidFwiLFwiyoxcIjpcInZcIixcIsqNXCI6XCJ3XCIsXCLKjlwiOlwieVwiLFwi6pypXCI6XCJ0elwiLFwiw7pcIjpcInVcIixcIsWtXCI6XCJ1XCIsXCLHlFwiOlwidVwiLFwiw7tcIjpcInVcIixcIuG5t1wiOlwidVwiLFwiw7xcIjpcInVcIixcIseYXCI6XCJ1XCIsXCLHmlwiOlwidVwiLFwix5xcIjpcInVcIixcIseWXCI6XCJ1XCIsXCLhubNcIjpcInVcIixcIuG7pVwiOlwidVwiLFwixbFcIjpcInVcIixcIsiVXCI6XCJ1XCIsXCLDuVwiOlwidVwiLFwi4bunXCI6XCJ1XCIsXCLGsFwiOlwidVwiLFwi4bupXCI6XCJ1XCIsXCLhu7FcIjpcInVcIixcIuG7q1wiOlwidVwiLFwi4butXCI6XCJ1XCIsXCLhu69cIjpcInVcIixcIsiXXCI6XCJ1XCIsXCLFq1wiOlwidVwiLFwi4bm7XCI6XCJ1XCIsXCLFs1wiOlwidVwiLFwi4baZXCI6XCJ1XCIsXCLFr1wiOlwidVwiLFwixalcIjpcInVcIixcIuG5uVwiOlwidVwiLFwi4bm1XCI6XCJ1XCIsXCLhtatcIjpcInVlXCIsXCLqnbhcIjpcInVtXCIsXCLisbRcIjpcInZcIixcIuqdn1wiOlwidlwiLFwi4bm/XCI6XCJ2XCIsXCLKi1wiOlwidlwiLFwi4baMXCI6XCJ2XCIsXCLisbFcIjpcInZcIixcIuG5vVwiOlwidlwiLFwi6p2hXCI6XCJ2eVwiLFwi4bqDXCI6XCJ3XCIsXCLFtVwiOlwid1wiLFwi4bqFXCI6XCJ3XCIsXCLhuodcIjpcIndcIixcIuG6iVwiOlwid1wiLFwi4bqBXCI6XCJ3XCIsXCLisbNcIjpcIndcIixcIuG6mFwiOlwid1wiLFwi4bqNXCI6XCJ4XCIsXCLhuotcIjpcInhcIixcIuG2jVwiOlwieFwiLFwiw71cIjpcInlcIixcIsW3XCI6XCJ5XCIsXCLDv1wiOlwieVwiLFwi4bqPXCI6XCJ5XCIsXCLhu7VcIjpcInlcIixcIuG7s1wiOlwieVwiLFwixrRcIjpcInlcIixcIuG7t1wiOlwieVwiLFwi4bu/XCI6XCJ5XCIsXCLIs1wiOlwieVwiLFwi4bqZXCI6XCJ5XCIsXCLJj1wiOlwieVwiLFwi4bu5XCI6XCJ5XCIsXCLFulwiOlwielwiLFwixb5cIjpcInpcIixcIuG6kVwiOlwielwiLFwiypFcIjpcInpcIixcIuKxrFwiOlwielwiLFwixbxcIjpcInpcIixcIuG6k1wiOlwielwiLFwiyKVcIjpcInpcIixcIuG6lVwiOlwielwiLFwi4bW2XCI6XCJ6XCIsXCLhto5cIjpcInpcIixcIsqQXCI6XCJ6XCIsXCLGtlwiOlwielwiLFwiyYBcIjpcInpcIixcIu+sgFwiOlwiZmZcIixcIu+sg1wiOlwiZmZpXCIsXCLvrIRcIjpcImZmbFwiLFwi76yBXCI6XCJmaVwiLFwi76yCXCI6XCJmbFwiLFwixLNcIjpcImlqXCIsXCLFk1wiOlwib2VcIixcIu+shlwiOlwic3RcIixcIuKCkFwiOlwiYVwiLFwi4oKRXCI6XCJlXCIsXCLhtaJcIjpcImlcIixcIuKxvFwiOlwialwiLFwi4oKSXCI6XCJvXCIsXCLhtaNcIjpcInJcIixcIuG1pFwiOlwidVwiLFwi4bWlXCI6XCJ2XCIsXCLigpNcIjpcInhcIn07XG5TdHJpbmcucHJvdG90eXBlLmxhdGluaXNlPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucmVwbGFjZSgvW15BLVphLXowLTlcXFtcXF0gXS9nLGZ1bmN0aW9uKGEpe3JldHVybiBMYXRpbmlzZS5sYXRpbl9tYXBbYV18fGF9KX07XG5TdHJpbmcucHJvdG90eXBlLmxhdGluaXplPVN0cmluZy5wcm90b3R5cGUubGF0aW5pc2U7XG5TdHJpbmcucHJvdG90eXBlLmlzTGF0aW49ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcz09dGhpcy5sYXRpbmlzZSgpfVxuIiwibW9kdWxlLmV4cG9ydHMgPSBvb3JkZW47XG4vKlxuICogRXN0ZSBtw7NkdWxvIG1hbmVqYSBkYXRvcyBkZSBPT1JkZW4gZGUgbGEgb3JnYW5pemFjaW9uXG4gKi9cbnZhciBwcm9taXNlO1xudmFyIG1ldG9kb3NTYWx2YWJsZXMgPSBbJ2NoZXF1ZScsJ3RyYW5zZmVyZW5jaWEnLCAndGFyamV0YSBkZSBjcmVkaXRvJ107XG5cbm9vcmRlbi5pbml0aWFsaXplID0gaW5pdGlhbGl6ZTtcbm9vcmRlbi5vcmdhbml6YWNpb24gID0gbS5wcm9wKCk7XG5vb3JkZW4ub3JnID0gb29yZGVuLm9yZ2FuaXphY2lvbjtcblxuXG5vb3JkZW4ub3JnYW5pemFjaW9uLnVuZm9ybWF0ID0gZnVuY3Rpb24gKHYpIHtcbiAgICB2ID0gTnVtYmVyKHYuc3BsaXQoJywnKS5qb2luKCcnKSlcbiAgICByZXR1cm4gb29yZGVuLm9yZ2FuaXphY2lvbi5yb3VuZCh2KTtcbn1cblxub29yZGVuLm9yZ2FuaXphY2lvbi5mYWN0b3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIE1hdGgucG93KDEwLCBvb3JkZW4ub3JnYW5pemFjaW9uKCkubnVtZXJvX2RlY2ltYWxlcyk7XG59XG5cbm9vcmRlbi5vcmdhbml6YWNpb24uZm9ybWF0ID0gZnVuY3Rpb24gKGFOdW1iZXIpIHtcbiAgICByZXR1cm4gb29yZGVuLm9yZ2FuaXphY2lvbi5udW1iZXJGb3JtYXQoKShhTnVtYmVyKVxufVxuXG5vb3JkZW4ub3JnYW5pemFjaW9uLm51bWJlckZvcm1hdCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZighb29yZGVuLm9yZ2FuaXphY2lvbigpICkge1xuICAgICAgICB0aHJvdyAnT09SREVOIGRhdGEgbm8gY2FyZ2FkYSdcbiAgICB9XG4gICAgcmV0dXJuIGQzLmZvcm1hdCgnLC4nLmNvbmNhdChvb3JkZW4ub3JnYW5pemFjaW9uKCkubnVtZXJvX2RlY2ltYWxlcyB8fCAyLCAnZicpKTtcbn1cblxuXG5cbm9vcmRlbi5vcmdhbml6YWNpb24ucm91bmQgPSBmdW5jdGlvbiAobnVtYmVyKSB7XG4gICAgaWYoIW9vcmRlbi5vcmdhbml6YWNpb24oKSApIHtcbiAgICAgICAgdGhyb3cgJ09PUkRFTiBkYXRhIG5vIGNhcmdhZGEnXG4gICAgfVxuICAgIHZhciBmYWN0b3IgPSBvb3JkZW4ub3JnYW5pemFjaW9uLmZhY3RvcigpO1xuICAgIHJldHVybiBNYXRoLnJvdW5kKG51bWJlciAqIGZhY3RvcikgLyBmYWN0b3I7XG59XG5cblxub29yZGVuLnJlYWR5ID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgb29yZGVuLnJlYWR5LmNhbGxiYWNrcy5wdXNoKGNhbGxiYWNrKVxufVxub29yZGVuLnJlYWR5LmNhbGxiYWNrcyA9IFtdO1xuXG5mdW5jdGlvbiByZWFkeSAoKSB7XG4gICAgb29yZGVuLnJlYWR5LmNhbGxiYWNrcy5mb3JFYWNoKGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICBjYWxsYmFjaygpO1xuICAgIH0pXG59XG5cbm9vcmRlbi5zdWN1cnNhbCAgICAgID0gbS5wcm9wKCk7XG5vb3JkZW4udXN1YXJpbyAgICAgICA9IG0ucHJvcCgpO1xuXG5vb3JkZW4uY3VlbnRhcyAgICAgICA9IG0ucHJvcCgpO1xub29yZGVuLmNjdG9zICAgICAgICAgPSBtLnByb3AoKTtcbm9vcmRlbi50aXBvc0RlQ2FtYmlvID0gbS5wcm9wKCk7XG5vb3JkZW4uaW1wdWVzdG9zICAgICA9IG0ucHJvcCgpO1xub29yZGVuLnJldGVuY2lvbmVzICAgPSBtLnByb3AoKTtcbm9vcmRlbi5tZXRvZG9zRGVQYWdvID0gbS5wcm9wKCk7XG5vb3JkZW4udGlwb3NEZURvY3VtZW50byA9IG0ucHJvcCgpO1xuXG5vb3JkZW4ucGFpc2VzID0gbS5wcm9wKCk7XG5vb3JkZW4ubW9uZWRhcyA9IG0ucHJvcCgpO1xub29yZGVuLnZlbmRlZG9yZXMgPSBtLnByb3AoKTtcblxudmFyIHBhaXNlc1lNb25lZGFzO1xudmFyIHZlbmRlZG9yZXM7XG5cbm9vcmRlbi5tb25lZGFzRGlzcG9uaWJsZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNvZGlnb3NNb25lZGEgPSB7fTtcblxuICAgIG9vcmRlbi50aXBvc0RlQ2FtYmlvKCkuZm9yRWFjaChmdW5jdGlvbiAodGMpIHtcbiAgICAgICAgY29kaWdvc01vbmVkYVt0Yy5jb2RpZ29fbW9uZWRhXSA9IHRydWU7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gb29yZGVuLm1vbmVkYXMoKS5maWx0ZXIoZnVuY3Rpb24gKG1vbikge1xuICAgICAgICByZXR1cm4gY29kaWdvc01vbmVkYVttb24uY29kaWdvX21vbmVkYV0gfHwgZmFsc2U7XG4gICAgfSkubWFwKGZ1bmN0aW9uIChtb24pIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGlkOiBtb24uY29kaWdvX21vbmVkYSAsXG4gICAgICAgICAgICB0ZXh0IDogbW9uLmNvZGlnb19tb25lZGEgKyAnICcgKyBtb24ubm9tYnJlXG4gICAgICAgIH1cbiAgICB9KTtcbn1cblxuXG5vb3JkZW4ucGFpc2VzWU1vbmVkYXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYocGFpc2VzWU1vbmVkYXMpIHJldHVybiBwYWlzZXNZTW9uZWRhcztcblxuICAgIHBhaXNlc1lNb25lZGFzID0gbS5yZXF1ZXN0KHtcbiAgICAgICAgbWV0aG9kIDogJ0dFVCcsXG4gICAgICAgIHVybCA6ICcvYXBpL3BhaXNlcydcbiAgICB9KS50aGVuKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgIG9vcmRlbi5wYWlzZXMoZC5kYXRhLnBhaXNlcyk7XG4gICAgICAgIG9vcmRlbi5tb25lZGFzKGQuZGF0YS5tb25lZGFzKTtcbiAgICB9KTtcblxuICAgIHJldHVybiBwYWlzZXNZTW9uZWRhc1xufVxuXG5cbm9vcmRlbi5nZXRWZW5kZWRvcmVzID0gZnVuY3Rpb24gKCkge1xuICAgIGlmKHZlbmRlZG9yZXMpIHJldHVybiB2ZW5kZWRvcmVzO1xuXG4gICAgdmVuZGVkb3JlcyA9IG0ucmVxdWVzdCh7XG4gICAgICAgIG1ldGhvZCA6ICdHRVQnLFxuICAgICAgICB1cmwgOiAnL29yZ2FuaXphY2lvbi92ZW5kZWRvcmVzJyxcbiAgICAgICAgdW53cmFwU3VjY2VzcyA6IGYoJ2RhdGEnKVxuICAgIH0pLnRoZW4ob29yZGVuLnZlbmRlZG9yZXMpO1xuXG4gICAgcmV0dXJuIHZlbmRlZG9yZXM7XG59XG5cbmZ1bmN0aW9uIG9vcmRlbiAoKSB7XG4gICAgcmV0dXJuIG9vcmRlbi5pbml0aWFsaXplKCk7XG59XG5cbmZ1bmN0aW9uIGdldFByb21pc2UgKCkge1xuICAgIHJldHVybiBwcm9taXNlO1xufVxuXG4vKipcbiAqIE9idGllbmUgbGEgYXl1ZGEgcGFyYSBsb3MgdG9vbHRpcHNcbiAqIEBwYXJhbSBzdHJpbmcgYXl1ZGFcbiAqL1xub29yZGVuLm9idGVuZXJBeXVkYSA9IGZ1bmN0aW9uIChheXVkYSkge1xuICAgIC8qXG4gICAgcmV0dXJuIG0ucmVxdWVzdCh7XG4gICAgICAgIG1ldGhvZCA6ICdHRVQnLFxuICAgICAgICB1cmwgOiAnL2FwaS9heXVkYS8nICsgYXl1ZGEsXG4gICAgICAgIHVud3JhcFN1Y2Nlc3MgOiBmdW5jdGlvbiAocmVzdWx0YWRvKSB7XG4gICAgICAgICAgICB2YXIgdmFsb3IgPSByZXN1bHRhZG8uZGF0YVtPYmplY3Qua2V5cyhyZXN1bHRhZG8uZGF0YSlbMF1dO1xuICAgICAgICAgICAgaWYgKHZhbG9yID09IGZhbHNlKSByZXR1cm4gXCJBeXVkYSBubyBlbmNvbnRyYWRhXCI7XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0YWRvLmRhdGFbT2JqZWN0LmtleXMocmVzdWx0YWRvLmRhdGEpWzBdXTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgICovXG59O1xuXG5cblxuLyoqXG4gKiBMYSB2aXN0YSBjcmVhIHVuIG5vZGUgY29uIDxkaXYgZGF0YS1vb3JkZW4tb3JnPVwiXCI+IHkgbG9zIGRhdG9zIGRlIGxhIG9yZ2FuaXphY2lvbiBpbmNydXN0YWRvc1xuICovXG4gXG5mdW5jdGlvbiBmYXN0SW5pdCAoKSB7XG4gICAgdmFyIGRhdGEgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbZGF0YS1vb3JkZW4tb3JnXScpO1xuICAgIGRhdGEgPSBKU09OLnBhcnNlKGRhdGEuZ2V0QXR0cmlidXRlKCdkYXRhLW9vcmRlbi1vcmcnKSk7XG4gICAgb29yZGVuLm9yZ2FuaXphY2lvbihkYXRhKTtcbiAgICByZWFkeSgpO1xufVxuXG5cbi8qKlxuICogT2J0aWVuZSBsb3MgZGF0b3MgY2FjaGVhZG9zIGRlbCBsb2NhbHN0b3JhZ2UsIGNhZGEgNSBtaW4gc2UgY29uc3VsdGFuXG4gKiBDb24gZWwgZmluIGRlIHJlZHVjaXIgbGFzIGxsYW1hZGFzIGEgZXN0YSBmdW5jacOzblxuICovXG5cbmZ1bmN0aW9uIGdldERhdGEgKCkge1xuICAgIHZhciBpZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWwnKS5nZXRBdHRyaWJ1dGUoJ3gtb29yZGVuLW9yZycpO1xuICAgIHZhciBtZXRha2V5ID0gJ09PUkRFTi9kYXRhLycgKyBpZCwgdGltZSwga2V5LCBkYXRvcztcbiAgICB2YXIgZGVmID0gbS5kZWZlcnJlZCgpO1xuXG4gICAgbm93ID0gbmV3IERhdGUoKTtcbiAgICBub3cgPSBNYXRoLmZsb29yKG5vdy5nZXRUaW1lKCkgLyAxMDAwKTtcblxuICAgIGlmKHRpbWUgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShtZXRha2V5KSkge1xuICAgICAgICAvL1NpIGxhIGtleSBlcyB2aWVqYSBlbGltaW5vIHRvZG9zIGxvcyBjYWNoZXNcbiAgICAgICAgaWYoKG5vdyAtIHRpbWUpID4gMzAwKSB7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhsb2NhbFN0b3JhZ2UpLmZvckVhY2goZnVuY3Rpb24gKGspIHtcbiAgICAgICAgICAgICAgICBpZihrLmluZGV4T2YoJ09PUkRFTi9kYXRhLycpID09IDAgKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGspXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2VcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlmKHRpbWUgJiYgKGRhdG9zID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ09PUkRFTi9kYXRhLycgKyBpZCArICcvJyArIHRpbWUpKSkge1xuICAgICAgICBkYXRvcyA9IEpTT04ucGFyc2UoZGF0b3MpO1xuICAgICAgICBkZWYucmVzb2x2ZShkYXRvcyk7XG4gICAgfVxuXG4gICAgaWYoIWRhdG9zKSB7XG4gICAgICAgIG0ucmVxdWVzdCh7bWV0aG9kOidHRVQnLCB1cmw6Jy9vcmdhbml6YWNpb24vY29uZmlnJ30pXG4gICAgICAgICAgICAudGhlbihmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKG1ldGFrZXksIG5vdyk7XG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ09PUkRFTi9kYXRhLycgKyBpZCArICcvJyArIG5vdywgSlNPTi5zdHJpbmdpZnkoZCkpO1xuICAgICAgICAgICAgICAgIGRlZi5yZXNvbHZlKGQpO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGRlZi5wcm9taXNlO1xufVxuXG5cblxuZnVuY3Rpb24gaW5pdGlhbGl6ZSAoKSB7XG4gICAgcHJvbWlzZSA9IGdldERhdGEoKS50aGVuKHNldERhdGEpO1xuICAgIG9vcmRlbi5pbml0aWFsaXplID0gZ2V0UHJvbWlzZTtcbiAgICByZXR1cm4gcHJvbWlzZTtcbn1cblxuXG5mdW5jdGlvbiBzZXREYXRhKGRhdGEpIHtcblxuICAgb29yZGVuLm9yZ2FuaXphY2lvbihkYXRhLm9yZ2FuaXphY2lvbik7XG4gICBvb3JkZW4uc3VjdXJzYWwoZGF0YS5zdWN1cnNhbCk7XG4gICBvb3JkZW4udXN1YXJpbyhkYXRhLnVzdWFyaW8pO1xuXG4gICAgLy9DVUVOVEFTXG4gICAgb29yZGVuLmN1ZW50YXMoIGRhdGEuY3VlbnRhc0NvbnRhYmxlcyApO1xuICAgIG9vcmRlbi5jdWVudGEgPSBtYWtlR2V0dGVyKG9vcmRlbi5jdWVudGFzKCksICdjdWVudGFfY29udGFibGVfaWQnKTtcblxuXG5cbiAgICAvL0NFTlRST1MgREUgQ09TVE9cbiAgICBvb3JkZW4uY2N0b3MoZGF0YS5jZW50cm9zRGVDb3N0byk7XG4gICAgb29yZGVuLmNjdG9zLmFjdGl2b3MgPSBtLnByb3AoXG4gICAgICAgIGRhdGEuY2VudHJvc0RlQ29zdG8uZmlsdGVyKGZ1bmN0aW9uIChjY3RvKSB7XG4gICAgICAgICAgICByZXR1cm4gQm9vbGVhbihOdW1iZXIoY2N0by5hY3Rpdm8pKTtcbiAgICAgICAgfSlcbiAgICApO1xuICAgIG9vcmRlbi5jY3RvID0gbWFrZUdldHRlcihvb3JkZW4uY2N0b3MoKSwgJ2NlbnRyb19kZV9jb3N0b19pZCcpO1xuXG4gICAgb29yZGVuLmJ1c2NhckNjdG8gPSAoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgY2N0b19lbGVtZW50b3MgPSB7fTtcbiAgICAgICAgb29yZGVuLmNjdG9zKCkuZm9yRWFjaChmdW5jdGlvbiAoY2N0bykge1xuICAgICAgICAgICAgY2N0by5lbGVtZW50b3MuZm9yRWFjaChmdW5jdGlvbiAoZWxlbSkge1xuICAgICAgICAgICAgICAgIGNjdG9fZWxlbWVudG9zW2VsZW0uZWxlbWVudG9fY2Nvc3RvX2lkXSA9IGNjdG87XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZWxlbWVudG9JZCkge1xuICAgICAgICAgICAgcmV0dXJuIGNjdG9fZWxlbWVudG9zW2VsZW1lbnRvSWRdO1xuICAgICAgICB9XG4gICAgfSgpKTtcblxuXG4gICAgb29yZGVuLm9idGVuZXJQYXJhbWV0cm9Qb3JOb21icmUgPSBmdW5jdGlvbihuYW1lLCB1cmwpIHtcbiAgICAgICAgaWYgKCF1cmwpIHVybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuICAgICAgICBuYW1lID0gbmFtZS5yZXBsYWNlKC9bXFxbXFxdXS9nLCBcIlxcXFwkJlwiKTtcbiAgICAgICAgdmFyIHJlZ2V4ID0gbmV3IFJlZ0V4cChcIls/Jl1cIiArIG5hbWUgKyBcIig9KFteJiNdKil8JnwjfCQpXCIpLFxuICAgICAgICAgICAgcmVzdWx0cyA9IHJlZ2V4LmV4ZWModXJsKTtcbiAgICAgICAgaWYgKCFyZXN1bHRzKSByZXR1cm4gbnVsbDtcbiAgICAgICAgaWYgKCFyZXN1bHRzWzJdKSByZXR1cm4gJyc7XG4gICAgICAgIHJldHVybiBkZWNvZGVVUklDb21wb25lbnQocmVzdWx0c1syXS5yZXBsYWNlKC9cXCsvZywgXCIgXCIpKTtcbiAgICB9XG5cbiAgICAvL1RpcG9zIERlIENhbWJpb1xuICAgIG9vcmRlbi50aXBvc0RlQ2FtYmlvKGRhdGEudGlwb3NEZUNhbWJpbyk7XG4gICAgb29yZGVuLnRpcG9EZUNhbWJpbyA9IG1ha2VHZXR0ZXIob29yZGVuLnRpcG9zRGVDYW1iaW8oKSwgJ3RpcG9fZGVfY2FtYmlvJyk7XG4gICAgZ3JvdXAob29yZGVuLnRpcG9zRGVDYW1iaW8sIG9vcmRlbi50aXBvc0RlQ2FtYmlvKCksICdjb2RpZ29fbW9uZWRhJyk7XG4gICAgb29yZGVuLnRpcG9zRGVDYW1iaW8oKS5mb3JFYWNoKGZ1bmN0aW9uICh0ZGMpIHtcbiAgICAgICAgaWYodGRjLmJhc2UgPT0gXCIxXCIpIHtcbiAgICAgICAgICAgIG9vcmRlbi50aXBvRGVDYW1iaW8uYmFzZSA9IG0ucHJvcCh0ZGMpO1xuICAgICAgICB9XG4gICAgfSk7XG5cblxuICAgIC8vVGlwb3MgRGUgRG9jdW1lbnRvXG4gICAgb29yZGVuLnRpcG9zRGVEb2N1bWVudG8oZGF0YS50aXBvc0RlRG9jdW1lbnRvKTtcbiAgICBvb3JkZW4udGlwb0RlRG9jdW1lbnRvID0gbWFrZUdldHRlcihkYXRhLnRpcG9zRGVEb2N1bWVudG8sICd0aXBvX2RvY3VtZW50b19pZCcpO1xuICAgIG9vcmRlbi50aXBvZGVEb2N1bWVudG8gPSBvb3JkZW4udGlwb0RlRG9jdW1lbnRvO1xuICAgIGdyb3VwKG9vcmRlbi50aXBvc0RlRG9jdW1lbnRvLCBvb3JkZW4udGlwb3NEZURvY3VtZW50bygpLCAndGlwb19vcGVyYWNpb24nKTtcblxuXG4gICAgLy9JbXB1ZXN0b3NcbiAgICBvb3JkZW4uaW1wdWVzdG9zKGRhdGEuaW1wdWVzdG9zKTtcbiAgICB2YXIgaW1wdWVzdG9HZXR0ZXIgPSBtYWtlR2V0dGVyKG9vcmRlbi5pbXB1ZXN0b3MoKSwgJ2ltcHVlc3RvX2Nvbmp1bnRvX2lkJyk7XG4gICAgb29yZGVuLmltcHVlc3RvID0gaW1wdWVzdG9HZXR0ZXI7XG5cbiAgICAvL1JldGVuY2lvbmVzXG4gICAgb29yZGVuLnJldGVuY2lvbmVzKGRhdGEucmV0ZW5jaW9uZXMpO1xuICAgIG9vcmRlbi5yZXRlbmNpb24gPSBtYWtlR2V0dGVyKG9vcmRlbi5yZXRlbmNpb25lcygpLCdyZXRlbmNpb25fY29uanVudG9faWQnKTtcblxuXG4gICAgLy9NZXRvZG9zIGRlIFBhZ29cbiAgICBvb3JkZW4ubWV0b2Rvc0RlUGFnbyhkYXRhLm1ldG9kb3NEZVBhZ28pXG4gICAgb29yZGVuLm1ldG9kb0RlUGFnbyA9IG1ha2VHZXR0ZXIob29yZGVuLm1ldG9kb3NEZVBhZ28oKSwgJ2lkJyk7XG4gICAgZGVsZXRlKG9vcmRlbi5tZXRvZG9zRGVQYWdvLnRvSlNPTik7XG5cbiAgICBvb3JkZW4ubWV0b2Rvc0RlUGFnbygpLmZvckVhY2goZnVuY3Rpb24gKG1wKSB7XG4gICAgICAgIHZhciBtZXRvZG8gPSBtcC5tZXRvZG8udG9Mb3dlckNhc2UoKTtcbiAgICAgICAgaWYobWV0b2Rvc1NhbHZhYmxlcy5pbmRleE9mKG1ldG9kbykgPj0gMCkge1xuICAgICAgICAgICAgb29yZGVuLm1ldG9kb3NEZVBhZ29bbWV0b2RvXSA9IG0ucHJvcChtcCk7XG4gICAgICAgIH1cbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gbWFrZUdldHRlcihkYXRhLCBrZXkpIHtcbiAgICB2YXIgbWFwID0ge307XG4gICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChkKSB7IG1hcFtkW2tleV1dID0gZDsgfSk7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh2YWwpIHtcbiAgICAgICAgcmV0dXJuIG1hcFt2YWxdO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZ3JvdXAgKGhvbGRlciwgZGF0YSwga2V5KSB7XG4gICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgIHZhciBrZXlWYWwgPSBkW2tleV07XG4gICAgICAgIHZhciBncm91cFByb3AgPSBob2xkZXJba2V5VmFsXTtcblxuICAgICAgICBpZighZ3JvdXBQcm9wKSB7XG4gICAgICAgICAgICBob2xkZXJba2V5VmFsXSA9IGdyb3VwUHJvcCA9IG0ucHJvcChbXSk7XG4gICAgICAgIH1cblxuICAgICAgICBncm91cFByb3AoKS5wdXNoKGQpO1xuICAgIH0pO1xufVxuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZmFzdEluaXQpO1xuIiwibW9kdWxlLmV4cG9ydHMgPSB0aW1lTWFuYWdlcjtcblxuXG5mdW5jdGlvbiB0aW1lTWFuYWdlciAoKSB7XG4gICAgdmFyIGF0dHIgPSAneC10aW1lLXRpbGwtbm93JztcblxuICAgIHZhciBnZXROb3cgPSAgXy50aHJvdHRsZShmdW5jdGlvbiAoKSB7IHJldHVybiBuZXcgRGF0ZSB9LCA1MDAwKTtcblxuICAgIHZhciBnZXRUaW1lID0gXy5tZW1vaXplKGNyZWF0ZVRpbWUpO1xuXG4gICAgZnVuY3Rpb24gZ2V0VGV4dERpZmYocmVmLCB0aW1lKSB7XG4gICAgICAgIHZhciBkaWZmID0gTWF0aC5mbG9vcigocmVmIC0gdGltZSkvIDEwMDApO1xuICAgICAgICB2YXIgayA9IDE7XG4gICAgICAgIHZhciBsID0gNjA7XG5cbiAgICAgICAgaWYoZGlmZiA8IDEwKSB7XG4gICAgICAgICAgICByZXR1cm4gJ2Fob3JhJztcbiAgICAgICAgfSBlbHNlIGlmKGRpZmYgPCA2MCkge1xuICAgICAgICAgICAgcmV0dXJuICcxIG1pbi4nO1xuICAgICAgICB9IGVsc2UgaWYoZGlmZiA8IDYwICogNjApIHtcbiAgICAgICAgICAgIHJldHVybiBNYXRoLmZsb29yKGRpZmYvNjApICsgJyBtaW5zJztcbiAgICAgICAgfSBlbHNlIGlmKGRpZmYgPCA2MCAqIDYwICogMjQpIHtcbiAgICAgICAgICAgIHJldHVybiBNYXRoLmZsb29yKGRpZmYvKDYwICogNjApKSArICcgaHJzJztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKGRpZmYvKDYwICogNjAgKiAyNCkpICsgJyBkw61hcyc7XG4gICAgfVxuXG4gICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkgeyB1cGRhdGUoZG9jdW1lbnQuYm9keSk7IH0sMTAwMDApO1xuXG4gICAgZnVuY3Rpb24gdXBkYXRlIChub2RlLCBpc0luaXRlZCkge1xuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBpc0luaXRlZCA9PSB0cnVlKSByZXR1cm47XG4gICAgICAgIHZhciBub2RlcyA9IG5vZGUucXVlcnlTZWxlY3RvckFsbCgnWycgKyBhdHRyICsgJ10nKTtcbiAgICAgICAgW10uZm9yRWFjaC5jYWxsKG5vZGVzLCB1cGRhdGVOb2RlKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGVOb2RlIChub2RlKSB7XG4gICAgICAgIHZhciB0aW1lID0gZ2V0VGltZShub2RlLmdldEF0dHJpYnV0ZShhdHRyKSk7XG4gICAgICAgIG5vZGUuaW5uZXJIVE1MID0gZ2V0VGV4dERpZmYoZ2V0Tm93KCksdGltZSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY3JlYXRlVGltZSAodCkge1xuICAgICAgICByZXR1cm4gbmV3IERhdGUodCk7XG4gICAgfVxuXG4gICAgcmV0dXJuICB7dXBkYXRlIDogdXBkYXRlfVxufVxuIiwibW9kdWxlLmV4cG9ydHMgPSBmZWNoYTtcblxuZnVuY3Rpb24gZmVjaGEgKCkge1xuICAgIHZhciBsYWJlbE1lc2VzID0gWycwJywgJ0VuZScsICdGZWInLCAnTWFyJywgJ0FicicsICdNYXknLCAnSnVuJywgJ0p1bCcsICdBZ28nLCAnU2VwJywgJ09jdCcsICdOb3YnLCdEaWMnXTtcbiAgICB2YXIgZnVsbExhYmVsTWVzZXMgPSBbJycsICdFbmVybycsICdGZWJyZXJvJywgJ01hcnpvJywgJ0FicmlsJywgJ01heW8nLCAnSnVuaW8nLCAnSnVsaW8nLCAnQWdvc3RvJywgJ1NlcHRpZW1icmUnLCAnT2N0dWJyZScsICdOb3ZpZW1icmUnLCAnRGljaWVtYnJlJ107XG5cbiAgICB2YXIgbm93ID0gbS5wcm9wKG5ldyBEYXRlKTtcblxuICAgIHByZXR0eUZvcm1hdC55ZWFyID0gZnVuY3Rpb24gKHN0cikge1xuICAgICAgICByZXR1cm4gcHJldHR5Rm9ybWF0KHN0ciwgdHJ1ZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgZnJvbVNRTCA6IGRlc2RlU1FMLFxuICAgICAgICBkZXNkZVNRTCA6IGRlc2RlU1FMLFxuICAgICAgICB0b1NRTCA6IHRvU1FMLFxuICAgICAgICBpbmljaWFsTWVzIDogaW5pY2lhbE1lcyxcbiAgICAgICAgZmluYWxNZXMgOiBmaW5hbE1lcyxcbiAgICAgICAgaW5pY2lhbEFubyA6IGluaWNpYWxBbm8sXG4gICAgICAgIGZpbmFsQW5vIDogZmluYWxBbm8sXG4gICAgICAgIHN0ciA6IHN0cixcbiAgICAgICAgZm9ybWF0IDogbmgubWVtb2l6ZShmb3JtYXQpLFxuICAgICAgICBwcmV0dHlGb3JtYXQgOiBwcmV0dHlGb3JtYXQsXG5cbiAgICAgICAgdG8xMCA6IHRvMTAsXG4gICAgICAgIGxhYmVsTWVzIDogbGFiZWxNZXMsXG4gICAgICAgIGZ1bGxMYWJlbE1lcyA6IGZ1bGxMYWJlbE1lcyxcbiAgICAgICAgdmFsaWRhdGUgOiB2YWxpZGF0ZSxcbiAgICAgICAgdmFsaWRhdGVTUUwgOiB2YWxpZGF0ZVNRTCxcblxuICAgICAgICBtZXNlcyA6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGZ1bGxMYWJlbE1lc2VzIH1cbiAgICB9O1xuXG5cbiAgICBmdW5jdGlvbiBpbmljaWFsTWVzIChmZWNoYSkge1xuICAgICAgICB2YXIgaW5pY2lhbCA9IG5ldyBEYXRlKGZlY2hhKTtcbiAgICAgICAgaW5pY2lhbC5zZXREYXRlKDEpO1xuICAgICAgICByZXR1cm4gaW5pY2lhbDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBmaW5hbE1lcyAoZmVjaGEpIHtcbiAgICAgICAgdmFyIGZmaW5hbCA9IG5ldyBEYXRlKGZlY2hhKTtcbiAgICAgICAgZmZpbmFsLnNldE1vbnRoKGZmaW5hbC5nZXRNb250aCgpICsgMSk7XG4gICAgICAgIGZmaW5hbC5zZXREYXRlKDApO1xuICAgICAgICByZXR1cm4gZmZpbmFsO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluaWNpYWxBbm8gKGZlY2hhKSB7XG4gICAgICAgIHZhciBpID0gbmV3IERhdGUoZmVjaGEpO1xuICAgICAgICBpLnNldE1vbnRoKDApO1xuICAgICAgICBpLnNldERhdGUoMSk7XG4gICAgICAgIHJldHVybiBpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZpbmFsQW5vIChmZWNoYSkge1xuICAgICAgICB2YXIgZiA9IG5ldyBEYXRlKGZlY2hhKTtcbiAgICAgICAgZi5zZXRNb250aCgxMSk7XG4gICAgICAgIGYuc2V0RGF0ZSgzMSk7XG4gICAgICAgIHJldHVybiBmO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGRlc2RlU1FMIChzdHIpIHtcbiAgICAgICAgdmFyIGQgPSBzdHIuc3BsaXQoJy0nKTtcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRlKHRvMTAoZFsxXSkgKyAnLycgKyB0bzEwKGRbMl0pICsgJy8nICsgZFswXSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZm9ybWF0IChzdHIpIHtcbiAgICAgICAgaWYoIXN0cil7XG4gICAgICAgICAgICByZXR1cm4gJy0nO1xuICAgICAgICB9XG4gICAgICAgIHZhciBkID0gbmV3IERhdGUoc3RyKTtcbiAgICAgICAgcmV0dXJuIGQuZ2V0RGF0ZSgpICsgJy0nICsgKGQuZ2V0TW9udGgoKSArIDEpICsgJy0nICsgZC5nZXRGdWxsWWVhcigpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByZXR0eUZvcm1hdCAoc3RyLCBmb3JjZVllYXIpIHtcbiAgICAgICAgdmFyIGluZm8gPSBzdHIuc3BsaXQoJy0nKTtcbiAgICAgICAgdmFyIHllYXIgPSAnICcgKyBpbmZvWzBdO1xuXG4gICAgICAgIGlmKHllYXIgPT0gbm93KCkuZ2V0RnVsbFllYXIoKSAmJiAhZm9yY2VZZWFyKSB7XG4gICAgICAgICAgICB5ZWFyID0gJyc7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRvMTAoTnVtYmVyKGluZm9bMl0pKS5jb25jYXQoJyAnLCBsYWJlbE1lcyhpbmZvWzFdKSwgIHllYXIpO1xuICAgIH1cblxuXG4gICAgZnVuY3Rpb24gdG9TUUwgKGRhdGUpIHtcbiAgICAgICAgcmV0dXJuIGRhdGUuZ2V0RnVsbFllYXIoKSArICctJyArIHRvMTAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLScgKyB0bzEwKGRhdGUuZ2V0RGF0ZSgpKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB0bzEwIChuKSB7XG4gICAgICAgIGlmKG4gPCAxMCkgcmV0dXJuICcwJy5jb25jYXQobik7XG4gICAgICAgIHJldHVybiBTdHJpbmcobik7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3RyIChkKSB7XG4gICAgICAgIHJldHVybiB0b1NRTChkKSArICcgJyArIHRvMTAoZC5nZXRIb3VycygpKSArICc6JyArIHRvMTAoZC5nZXRNaW51dGVzKCkpICsgJzonICsgdG8xMChkLmdldFNlY29uZHMoKSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbGFiZWxNZXMgKGQpIHtcbiAgICAgICAgcmV0dXJuIGxhYmVsTWVzZXNbTnVtYmVyKGQpXTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBmdWxsTGFiZWxNZXMgKGQpIHtcbiAgICAgICAgcmV0dXJuIGZ1bGxMYWJlbE1lc2VzW051bWJlcihkKV07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdmFsaWRhdGUgKGQpIHtcbiAgICAgICAgdmFyIGRhdGEgPSBTdHJpbmcoZCkubWF0Y2goL15cXGR7Mn0tXFxkezJ9LVxcZHs0fSQvKTtcbiAgICAgICAgcmV0dXJuIChkYXRhICYmIGRhdGEubGVuZ3RoKSA/IHRydWU6IGZhbHNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHZhbGlkYXRlU1FMIChkKSB7XG4gICAgICAgIHZhciBkYXRhID0gU3RyaW5nKGQpLm1hdGNoKC9eXFxkezR9LVxcZHsyfS1cXGR7Mn0kLyk7XG4gICAgICAgIHJldHVybiAoZGF0YSAmJiBkYXRhLmxlbmd0aCkgPyB0cnVlOiBmYWxzZTtcbiAgICB9XG59O1xuIiwicmVxdWlyZSgnLi9MYXRpbmlzZScpO1xucmVxdWlyZSgnLi4vbWlzYy9hZHZTZWxlY3RvcicpO1xuXG52YXIgdGltZU1hbmFnZXIgPSByZXF1aXJlKCcuL1RpbWVNYW5hZ2VyJyk7XG52YXIgZmVjaGEgPSByZXF1aXJlKCcuL2ZlY2hhJyk7XG52YXIgY3VlbnRhc1NlbGVjdDIgPSByZXF1aXJlKCcuL3NlbGVjdG9ycy9jdWVudGFzU2VsZWN0MicpO1xudmFyIGN1ZW50YXNTZWxlY3RvciA9IHJlcXVpcmUoJy4vc2VsZWN0b3JzL2N1ZW50YXNTZWxlY3RvcicpO1xud2luZG93LlBhcGEgPSByZXF1aXJlKCcuLi9wYXBhcGFyc2UvcGFwYXBhcnNlJyk7XG5cbnZhciBvb3IgPSB7XG4gICAgc2VsZWN0MiA6IHNlbGVjdDIsXG4gICAgdGVyY2Vyb3NTZWxlY3QyIDogdGVyY2Vyb3NTZWxlY3QyLFxuXG4gICAgbG9hZGluZyA6IGZ1bmN0aW9uIChpbmxpbmUpIHtcbiAgICAgICAgcmV0dXJuIG0oaW5saW5lID8gJ3NwYW4nIDogJ2Rpdi50ZXh0LWNlbnRlcicsIHtrZXkgOiAnJGxvYWRpbmcnfSwgbSgnaS5mYS5mYS1zcGluLmZhLXNwaW5uZXInKSk7XG4gICAgfSxcblxuICAgIGZvbmRvR3JpcyA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnaHRtbCcpLmNzcygnYmFja2dyb3VuZCcsXCIjZjNmM2YzXCIpO1xuICAgICAgICAkKCdib2R5JykuY3NzKCdiYWNrZ3JvdW5kJyxcIiNmM2YzZjNcIik7XG4gICAgfSxcblxuICAgIGF1dG9mb2N1cyA6IGZ1bmN0aW9uIChlbCwgaW5pdGVkKSB7XG4gICAgICAgIGlmKCFpbml0ZWQpIHsgZWwuZm9jdXMoKTsgfVxuICAgIH0sXG5cbiAgICBjdWVudGFzU2VsZWN0MiA6IGN1ZW50YXNTZWxlY3QyLFxuICAgIGN1ZW50YXNTZWxlY3RvciA6IGN1ZW50YXNTZWxlY3RvcixcbiAgICBpbXB1ZXN0b3NTZWxlY3QyIDogaW1wdWVzdG9zU2VsZWN0MixcbiAgICByZXRlbmNpb25lc1NlbGVjdDIgOiByZXRlbmNpb25lc1NlbGVjdDIsXG5cbiAgICBjY3RvU2VsZWN0MiA6IGNjdG9TZWxlY3QyLFxuICAgIHRpcG9Eb2NTZWxlY3QyIDogdGlwb0RvY1NlbGVjdDIsXG5cbiAgICBpbnB1dGVyIDogaW5wdXRlcixcbiAgICB1U2V0dGluZ3MgOiB1c2VyU2V0dGluZ3MsXG5cbiAgICB1dWlkIDogZ3VpZCxcbiAgICBtbSA6IE9vck1vZGVscygpLFxuICAgIGZlY2hhIDogZmVjaGEoKSxcblxuICAgIHRpbWUgOiB0aW1lTWFuYWdlcigpLFxuICAgIHhQcm9wQ29uZmlnIDogeFByb3BDb25maWcsXG4gICAgZGlzcGxheU1lc3NhZ2VzIDogZGlzcGxheU1lc3NhZ2VzLFxuICAgIGxzTWVzc2FnZSA6IGxzTWVzc2FnZUluaXRpYWxpemVyKCksXG4gICAgcGVyZm9ybUhpZ2hsaWdodCA6IHBlcmZvcm1IaWdobGlnaHQsXG4gICAgdG9vbHRpcCA6IHRvb2x0aXAsXG5cbiAgICByZXF1ZXN0IDpyZXF1ZXN0LFxuICAgIGF1dG9Hcm93IDogYXV0b0dyb3dcbn07XG5cbnRlbXBsYXRlRnVuY3Rpb25zKG9vcik7XG5cbm1vZHVsZS5leHBvcnRzID0gb29yO1xuXG5cbmZ1bmN0aW9uIGF1dG9Hcm93IChlbCwgaXNJbml0aWFsaXplZCwgZGF0YSkge1xuICAgIGlmKCFpc0luaXRpYWxpemVkKSB7XG4gICAgICAgIGRhdGEuZ3JvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGVsLnN0eWxlLmhlaWdodCA9ICcxMHB4JztcbiAgICAgICAgICAgIGVsLnNjcm9sbFRvcCA9IDIwMDAwO1xuICAgICAgICAgICAgaWYoZWwuc2Nyb2xsVG9wIDwgMjAwMDApIHtcbiAgICAgICAgICAgICAgICBlbC5zdHlsZS5oZWlnaHQgPSBTdHJpbmcoTWF0aC5tYXgoNTAsMTAgKyBlbC5zY3JvbGxUb3ApKS5jb25jYXQoJ3B4JylcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBkYXRhLmdyb3cuZGVib3VuY2VkID0gXy5kZWJvdW5jZShkYXRhLmdyb3csIDEwMCk7XG4gICAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2lucHV0JywgZGF0YS5ncm93LmRlYm91bmNlZCk7XG4gICAgICAgIGVsLnN0eWxlWydtaW4td2lkdGgnXSA9ICczMDBweCc7XG4gICAgICAgIGRhdGEuZ3JvdygpO1xuICAgIH1cbn1cblxuXG5cblxuZnVuY3Rpb24gcmVxdWVzdCAodXJsLCBtZXRob2QsIGV4dHJhUGFyYW1zKSB7XG5cbiAgICB2YXIgZGF0YSA9IHtcbiAgICAgICAgbWV0aG9kIDogbWV0aG9kIHx8ICdHRVQnLFxuICAgICAgICB1cmwgOiB1cmwsXG4gICAgICAgIHVud3JhcFN1Y2Nlc3MgOiBmKCdkYXRhJylcbiAgICB9O1xuXG4gICAgaWYob29yLnYxID09IHRydWUpIHtcbiAgICAgICAgZGF0YS50eXBlID0gZnVuY3Rpb24gKHIpIHsgcmV0dXJuIHIuZGF0YSB9XG4gICAgfVxuXG4gICAgdmFyIHBhcmFtcyA9IG5oLmV4dGVuZChkYXRhLCBleHRyYVBhcmFtcyk7XG5cbiAgICByZXR1cm4gbS5yZXF1ZXN0KHBhcmFtcyk7XG59XG5cblxuZnVuY3Rpb24gcGVyZm9ybUhpZ2hsaWdodCAoc2VsZWN0b3IsIGNvbG9yLCBzdGFydENvbG9yKSB7XG4gICAgaWYoIXN0YXJ0Q29sb3Ipe1xuICAgICAgICBzdGFydENvbG9yID0gJyNmZmYnXG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZDMuc2VsZWN0QWxsKHNlbGVjdG9yKVxuICAgICAgICAgICAgLnN0eWxlKCdiYWNrZ3JvdW5kJywgc3RhcnRDb2xvcilcbiAgICAgICAgICAgIC50cmFuc2l0aW9uKCkuZHVyYXRpb24oNTAwKVxuICAgICAgICAgICAgLnN0eWxlKCdiYWNrZ3JvdW5kJywgJyM3OWUwNzgnKVxuICAgICAgICAgICAgLnRyYW5zaXRpb24oKS5kZWxheSg1MDApLmR1cmF0aW9uKDUwMClcbiAgICAgICAgICAgIC5zdHlsZSgnYmFja2dyb3VuZCcsIHN0YXJ0Q29sb3IpXG4gICAgICAgICAgICAuZWFjaCgnZW5kJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGQzLnNlbGVjdCh0aGlzKS5zdHlsZSgnYmFja2dyb3VuZCcsIHVuZGVmaW5lZCk7XG4gICAgICAgICAgICB9KVxuXG4gICAgfVxufVxuXG5cblxuLyoqXG4gKiBGdW5jacOzbiBxdWUgbXVlc3RyYSBsb3MgbWVuc2FqZXMgYWxtYWNlbmFkb3MgZW4gbG9jYWxTdG9yYWdlXG4gKi9cbmZ1bmN0aW9uIGxzTWVzc2FnZUluaXRpYWxpemVyICgpIHtcbiAgICB2YXIgTUVTU0FHRV9MT0NBTF9TVE9SQUdFID0gJ01FU1NBR0VfTE9DQUxfU1RPUkFHRSc7XG5cbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG1lc3NhZ2U7XG5cbiAgICAgICAgaWYobWVzc2FnZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKE1FU1NBR0VfTE9DQUxfU1RPUkFHRSkpIHtcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKE1FU1NBR0VfTE9DQUxfU1RPUkFHRSk7XG5cbiAgICAgICAgICAgIG1lc3NhZ2UgPSBKU09OLnBhcnNlKG1lc3NhZ2UpO1xuICAgICAgICAgICAgbWVzc2FnZS50eXBlID0gbWVzc2FnZS50eXBlIHx8ICdzdWNjZXNzJztcbiAgICAgICAgICAgIHRvYXN0clttZXNzYWdlLnR5cGVdKG1lc3NhZ2UudGV4dCk7XG4gICAgICAgIH1cblxuICAgIH0sMjAwKTtcblxuXG4gICAgZnVuY3Rpb24gbHNNZXNzYWdlIChtc2cpIHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oTUVTU0FHRV9MT0NBTF9TVE9SQUdFLCBKU09OLnN0cmluZ2lmeShtc2cpKTtcbiAgICB9XG5cbiAgICB3aW5kb3cubHNNZXNzYWdlID0gbHNNZXNzYWdlO1xuICAgIHJldHVybiBsc01lc3NhZ2U7XG5cbn1cblxuXG5cbm0ucmVkcmF3LmRlYm91bmNlZCA9IF8uZGVib3VuY2UobS5yZWRyYXcsIDUwMCk7XG5cbmZ1bmN0aW9uIGRpc3BsYXlNZXNzYWdlcyAoKSB7XG4gICAgdmFyIG1zZ05vZGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbWVzc2FnZXMtdG9hc3RyJyk7XG4gICAgaWYoIW1zZ05vZGUpIHJldHVybjtcblxuICAgIHZhciBtZXNzYWdlcyA9IEpTT04ucGFyc2UobXNnTm9kZS5pbm5lckhUTUwpO1xuICAgIGlmKCFtZXNzYWdlcykgcmV0dXJuO1xuXG4gICAgT2JqZWN0LmtleXMobWVzc2FnZXMpLmZvckVhY2goZnVuY3Rpb24gKGtpbmQpIHtcbiAgICAgICAgbWVzc2FnZXNba2luZF0uZm9yRWFjaChmdW5jdGlvbiAobXNnKSB7XG4gICAgICAgICAgICB0b2FzdHJba2luZF0obXNnKTtcbiAgICAgICAgfSlcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24geFByb3BDb25maWcoY3R4KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChlbGVtZW50LCBpc0luaXRpYWxpemVkKSB7XG4gICAgICAgIGlmKGlzSW5pdGlhbGl6ZWQpIHJldHVybjtcblxuICAgICAgICAkKGVsZW1lbnQpLm9uKCdpbnB1dCcsICdbeC1wcm9wXScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBpbnB1dCA9ICQodGhpcyk7XG4gICAgICAgICAgICB2YXIgbW9kZWwgPSBmKGlucHV0LmF0dHIoJ3gtbW9kZWwnKSkoY3R4KTtcblxuICAgICAgICAgICAgbW9kZWxbaW5wdXQuYXR0cigneC1wcm9wJyldKGlucHV0LnZhbCgpKVxuXG4gICAgICAgICAgICBtLnJlZHJhdy5kZWJvdW5jZWQoKTtcbiAgICAgICAgfSlcbiAgICB9XG59XG5cbiQoZnVuY3Rpb24gKCkge1xuICAgICQoZG9jdW1lbnQuYm9keSkub24oJ2ZvY3VzJywgJy5tdC1pbnB1dGVyID4gaW5wdXQsIC5tdC1pbnB1dGVyID4gdGV4dGFyZWEnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkuYWRkQ2xhc3MoJ2ZvY3VzJyk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50LmJvZHkpLm9uKCdibHVyJywgJy5tdC1pbnB1dGVyID4gaW5wdXQsIC5tdC1pbnB1dGVyID4gdGV4dGFyZWEnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2ZvY3VzJyk7XG4gICAgfSlcbn0pXG5cblxuLyoqXG4gKiBTZWxlY3QyIEFkYXB0ZXJcbiAqXG4gKi9cblxuc2VsZWN0Mi5OVUxMX1ZBTFVFID0gJzo6TlVMTF9WQUxVRTo6JztcblxuZnVuY3Rpb24gc2VsZWN0MiAocGFyYW1zKSB7XG4gICAgdmFyIG1vZGVsICAgID0gbS5wcm9wKCBwYXJhbXMubW9kZWwgKTtcblxuICAgIHZhciBkYXRhID0gKCB0eXBlb2YgcGFyYW1zLmRhdGEgPT09ICdmdW5jdGlvbicpID8gcGFyYW1zLmRhdGEgOiBtLnByb3AocGFyYW1zLmRhdGEpO1xuICAgIHZhciBpc0luaXRpYWxpemVkID0gbS5wcm9wKCBmYWxzZSk7XG4gICAgdmFyIG9wdGlvbnMgICAgICAgPSBtLnByb3AoIG51bGwgKTtcbiAgICB2YXIgc2VsZWN0ZWQgICAgICA9IG0ucHJvcCggbnVsbCApO1xuICAgIHZhciBlbCAgICAgICAgICAgID0gbS5wcm9wKCBudWxsICk7XG4gICAgdmFyIGN1cnJWYWwgICAgICAgPSBtLnByb3AoIG51bGwgKTtcblxuICAgIHZhciBlbmFibGVkID0gdHJ1ZTtcblxuICAgIHZhciBzZWxmID0ge1xuICAgICAgICB2aWV3ICA6IHZpZXcsXG4gICAgICAgIG1vZGVsIDogbW9kZWwsXG4gICAgICAgIGRhdGEgIDogZGF0YSxcbiAgICAgICAgc2VsZWN0ZWQgOiBzZWxlY3RlZCxcbiAgICAgICAgZWxlbWVudCA6IGVsLFxuICAgICAgICByZWJ1aWxkIDogcmVidWlsZCxcbiAgICAgICAgY29uZmlnIDogY29uZmlnLFxuICAgICAgICBrZXkgOiBwYXJhbXMua2V5LFxuICAgICAgICBvbmNoYW5nZSA6IHBhcmFtcy5vbmNoYW5nZSxcbiAgICAgICAgZW5hYmxlZCA6IHBhcmFtcy5lbmFibGVkLFxuICAgICAgICBlbCA6IGVsXG4gICAgfTtcblxuICAgIHJldHVybiBzZWxmO1xuXG5cbiAgICBmdW5jdGlvbiByZWJ1aWxkICgpIHtcbiAgICAgICAgaWYoaXNJbml0aWFsaXplZCgpID09IGZhbHNlKSByZXR1cm47XG4gICAgICAgIGNvbmZpZyhlbCgpWzBdLCBmYWxzZSk7XG4gICAgfVxuXG5cbiAgICBmdW5jdGlvbiBub3RpZnkgKCkge1xuICAgICAgICB2YXIgdmFsID0gbW9kZWwoKTtcbiAgICAgICAgLy9TZSBhY3R1YWxpemEgZWwgdmFsb3IgZGVsIG1vZGVsb1xuICAgICAgICB2YWwoZWwoKS5zZWxlY3QyKCd2YWwnKSk7XG5cbiAgICAgICAgaWYodHlwZW9mIHBhcmFtcy5maW5kID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHNlbGVjdGVkKCBwYXJhbXMuZmluZCh2YWwoKSkgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHR5cGVvZiBzZWxmLm9uY2hhbmdlID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHNlbGYub25jaGFuZ2UoIHZhbCgpICk7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0eXBlb2YgdmFsLm9uY2hhbmdlID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHZhbC5vbmNoYW5nZSggdmFsKCkgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNldFZhbHVlKGZvcmNlKSB7XG4gICAgICAgIGlmKGZvcmNlID09PSB0cnVlIHx8IChtb2RlbCgpKSgpICE9IGN1cnJWYWwoKSkge1xuICAgICAgICAgICAgY3VyclZhbCggKG1vZGVsKCkpKCkgKTtcbiAgICAgICAgICAgIGlmKCEgZWwoKSApIHJldHVybjtcbiAgICAgICAgICAgIGVsKCkuc2VsZWN0MigndmFsJywgY3VyclZhbCgpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvbmZpZyAobm9kZSwgaW5pdGlhbGl6ZWQpIHtcbiAgICAgICAgdmFyIGVsZW1lbnQgICA9ICQobm9kZSk7XG4gICAgICAgIHZhciBpbml0VmFsdWUgPSAobW9kZWwoKSkoKTtcblxuICAgICAgICBpZihpbml0aWFsaXplZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGVsKGVsZW1lbnQpO1xuICAgICAgICAgICAgdmFyIHNlbGVjdDJQYXJhbXMgPSB7XG4gICAgICAgICAgICAgICAgZGF0YSA6IGRhdGEoKSxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVNlbGVjdGlvbiA6IHBhcmFtcy50ZW1wbGF0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVJlc3VsdCAgICA6IHBhcmFtcy50ZW1wbGF0ZVJlc3VsdCxcbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlciAgICAgICA6IHBhcmFtcy5wbGFjZWhvbGRlclxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYocGFyYW1zLnVybCkge1xuICAgICAgICAgICAgICAgIHNlbGVjdDJQYXJhbXMuYWpheCA9IHtcbiAgICAgICAgICAgICAgICAgICAgdXJsIDogcGFyYW1zLnVybCxcbiAgICAgICAgICAgICAgICAgICAgZGF0YVR5cGUgOiAnanNvbicsXG4gICAgICAgICAgICAgICAgICAgIGRlbGF5IDogMjUwLFxuICAgICAgICAgICAgICAgICAgICBkYXRhIDogIHBhcmFtcy5hamF4RGF0YSxcbiAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc1Jlc3VsdHMgOiBwYXJhbXMuYWpheFByb2Nlc3NSZXN1bHRzLFxuICAgICAgICAgICAgICAgICAgICBjYWNoZSA6IHRydWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGVsZXRlKHNlbGVjdDJQYXJhbXMuZGF0YSk7XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgZnVuY3Rpb24gb25DaGFuZ2UoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZhbCA9IG1vZGVsKCk7XG4gICAgICAgICAgICAgICAgaWYoZWxlbWVudC5zZWxlY3QyKCd2YWwnKSA9PSB2YWwoKSkgcmV0dXJuO1xuXG4gICAgICAgICAgICAgICAgaWYoZWxlbWVudC5zZWxlY3QyKCd2YWwnKSA9PSBzZWxlY3QyLk5VTExfVkFMVUUpe1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnNlbGVjdDIoJ3ZhbCcsIG51bGwpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIG0uc3RhcnRDb21wdXRhdGlvbigpO1xuICAgICAgICAgICAgICAgIG5vdGlmeSgpO1xuICAgICAgICAgICAgICAgIG0uZW5kQ29tcHV0YXRpb24oKTtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBlbGVtZW50LnNlbGVjdDIoc2VsZWN0MlBhcmFtcylcbiAgICAgICAgICAgICAgICAub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlzSW5pdGlhbGl6ZWQoKSAmJiBvbkNoYW5nZSgpXG4gICAgICAgICAgICAgICAgfSlcblxuXG4gICAgICAgICAgICBlbGVtZW50LnBhcmVudCgpLm9uKCdmb2N1cycsICcuc2VsZWN0Mi1zZWxlY3Rpb24nLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zZWxlY3QyKCdvcGVuJyk7XG4gICAgICAgICAgICB9KTtcblxuXG5cbiAgICAgICAgICAgIGlmKCh0eXBlb2Ygc2VsZi5lbmFibGVkID09ICdmdW5jdGlvbicpKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zZWxlY3QyKCdlbmFibGUnLCBzZWxmLmVuYWJsZWQoKSk7XG4gICAgICAgICAgICAgICAgZW5hYmxlZCA9IHNlbGYuZW5hYmxlZCgpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIHNldFZhbHVlKHRydWUpO1xuICAgICAgICAgICAgaXNJbml0aWFsaXplZCh0cnVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKCh0eXBlb2Ygc2VsZi5lbmFibGVkID09ICdmdW5jdGlvbicpICYmIHNlbGYuZW5hYmxlZCgpICE9IGVuYWJsZWQpIHtcbiAgICAgICAgICAgIGVsZW1lbnQuc2VsZWN0MignZW5hYmxlJywgc2VsZi5lbmFibGVkKCkpO1xuICAgICAgICAgICAgZW5hYmxlZCA9IHNlbGYuZW5hYmxlZCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0VmFsdWUoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB2aWV3ICh2UGFyYW1zKSB7XG4gICAgICAgIHZQYXJhbXMgfHwgKHZQYXJhbXMgPSB7fSk7XG4gICAgICAgIHZQYXJhbXMuY29uZmlnID0gY29uZmlnO1xuXG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICBwYXJhbXMucXVpdEJ0biA/IG0oJ2Rpdi5wdWxsLXJpZ2h0Jywge1xuICAgICAgICAgICAgICAgIHN0eWxlOidjdXJzb3I6cG9pbnRlcicsXG4gICAgICAgICAgICAgICAgb25jbGljazpmdW5jdGlvbiAoKSB7IGVsKCkuc2VsZWN0MigndmFsJyxudWxsKSB9XG4gICAgICAgICAgICB9LCAnw5cnKSA6ICcnLFxuICAgICAgICAgICAgbSgnc2VsZWN0JywgdlBhcmFtcylcblxuICAgICAgICBdXG4gICAgfVxufVxuXG4vKipcbiAqIFJldHVybiBUZXJjZXJvc1NlbGVjdG9yXG4gKi9cbmZ1bmN0aW9uIHRlcmNlcm9zU2VsZWN0MiAoaVBhcmFtcykge1xuXG4gICAgdmFyIGRhdGEgPSBtLnByb3AoKTtcblxuICAgIGdldE9wdGlvbnMoKTtcblxuICAgIHZhciBzZWxlY3RvciA9IHNlbGVjdDIoXy5leHRlbmQoe1xuICAgICAgICBkYXRhIDogZGF0YSxcbiAgICAgICAgZGVidWcgOiB0cnVlLFxuICAgICAgICBmaW5kIDogZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgICAgICB2YXIgZiA9IChkYXRhKCkgfHwgW10pLmZpbHRlcihmdW5jdGlvbiAodCkgeyByZXR1cm4gdC5pZCA9PSBpZCB9KVswXTtcbiAgICAgICAgICAgIHJldHVybiBmICYmIGYuZGF0YTtcbiAgICAgICAgfVxuICAgIH0sIGlQYXJhbXMpKTtcblxuICAgIHNlbGVjdG9yLmdldE9wdGlvbnMgPSBnZXRPcHRpb25zO1xuXG4gICAgcmV0dXJuIHNlbGVjdG9yO1xuXG4gICAgZnVuY3Rpb24gZ2V0T3B0aW9ucyAoKSB7XG4gICAgICAgIHZhciBxdWVyeSA9IHttb2RlbG86J3RlcmNlcm9zJ307XG5cblxuXG4gICAgICAgIGlmKGlQYXJhbXMudGlwbyA9PSAnY2xpZW50ZScpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFc0NsaWVudGUnKVxuICAgICAgICAgICAgcXVlcnkuZXNfY2xpZW50ZSA9ICcxJztcbiAgICAgICAgfSBlbHNlIGlmIChpUGFyYW1zLnRpcG8gPT0gJ3Byb3ZlZWRvcicpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFc1Byb3ZlZWRvcicpXG4gICAgICAgICAgICBxdWVyeS5lc19wcm92ZWVkb3IgPSAnMSc7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbS5yZXF1ZXN0KHttZXRob2Q6J0dFVCcsIHVybDonL2FwaXYyJywgZGF0YSA6IHF1ZXJ5fSkudGhlbihmdW5jdGlvbiAodCkge1xuICAgICAgICAgICAgZGF0YShcbiAgICAgICAgICAgICAgICB0LmRhdGEubWFwKGZ1bmN0aW9uICh0Mykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4ge2lkOnQzLnRlcmNlcm9faWQsIHRleHQ6dDMubm9tYnJlICsgJyAtICcgKyB0My5jbGF2ZV9maXNjYWwgKyAnIC0gKCcgKyB0My5jb2RpZ28gKyAnKScsIGRhdGE6dDN9O1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgc2VsZWN0b3IucmVidWlsZCgpO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5cblxuLyoqXG4gKiBSZXR1cm4gVGVyY2Vyb3NTZWxlY3RvclxuICovXG5mdW5jdGlvbiBwcm9kdWN0b3NTZWxlY3QyIChpUGFyYW1zKSB7XG5cbiAgICB2YXIgZGF0YSA9IG0ucHJvcCgpO1xuXG4gICAgZ2V0T3B0aW9ucygpO1xuXG4gICAgdmFyIHNlbGVjdG9yID0gc2VsZWN0MihfLmV4dGVuZCh7XG4gICAgICAgIGRhdGEgOiBkYXRhLFxuICAgICAgICBkZWJ1ZyA6IHRydWUsXG4gICAgICAgIGZpbmQgOiBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgICAgIHZhciBmID0gKGRhdGEoKSB8fCBbXSkuZmlsdGVyKGZ1bmN0aW9uICh0KSB7IHJldHVybiB0LmlkID09IGlkIH0pWzBdO1xuICAgICAgICAgICAgcmV0dXJuIGYgJiYgZi5kYXRhO1xuICAgICAgICB9XG4gICAgfSwgaVBhcmFtcykpO1xuXG4gICAgc2VsZWN0b3IuZ2V0T3B0aW9ucyA9IGdldE9wdGlvbnM7XG5cbiAgICByZXR1cm4gc2VsZWN0b3I7XG5cbiAgICBmdW5jdGlvbiBnZXRPcHRpb25zICgpIHtcbiAgICAgICAgcmV0dXJuIG0ucmVxdWVzdCh7bWV0aG9kOidHRVQnLCB1cmw6Jy9hcGl2Mj9tb2RlbG89cHJvZHVjdG9zJ30pLnRoZW4oZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgICAgIGRhdGEoXG4gICAgICAgICAgICAgICAgdC5kYXRhLm1hcChmdW5jdGlvbiAodDMpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtpZDp0My50ZXJjZXJvX2lkLCB0ZXh0OnQzLmNvZGlnbyArICAnIC0gJyArIHQzLm5vbWJyZSArICcgLSAnICsgdDMuY2xhdmVfZmlzY2FsLCBkYXRhOnQzfTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIHNlbGVjdG9yLnJlYnVpbGQoKTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG4vKipcbiAqIEByZXR1cm4gcmV0ZW5jaW9uZXNTZWxlY3RvclxuICovXG5cblxuZnVuY3Rpb24gcmV0ZW5jaW9uZXNTZWxlY3QyIChpUGFyYW1zKSB7XG5cbiAgICB2YXIgcmV0ZW5jaW9uZXMgPSBpUGFyYW1zLnJldGVuY2lvbmVzXG4gICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKGMpIHsgcmV0dXJuIGMuZXN0YXR1cyA9PSAxfSlcbiAgICAgICAgLm1hcChmdW5jdGlvbiAoYykge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBpZDpjLnJldGVuY2lvbl9jb25qdW50b19pZCxcbiAgICAgICAgICAgICAgICB0ZXh0OmMucmV0ZW5jaW9uX2Nvbmp1bnRvXG4gICAgICAgICAgICB9O1xuICAgICAgICB9KTtcblxuICAgIHJldGVuY2lvbmVzLnVuc2hpZnQoe2lkOnNlbGVjdDIuTlVMTF9WQUxVRSwgdGV4dDonKG5pbmd1bmEpJ30pO1xuXG4gICAgcmV0dXJuIHNlbGVjdDIoXG4gICAgICAgIF8uZXh0ZW5kKHtcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyIDogJyhuaW5ndW5hKScsXG4gICAgICAgICAgICBkYXRhIDogcmV0ZW5jaW9uZXMsXG4gICAgICAgICAgICBmaW5kIDogZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGlQYXJhbXMucmV0ZW5jaW9uZXMuZmlsdGVyKGZ1bmN0aW9uIChjKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjLnJldGVuY2lvbl9jb25qdW50b19pZCA9PSBpZDtcbiAgICAgICAgICAgICAgICB9KVswXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgaVBhcmFtcylcbiAgICApO1xufVxuXG4vKipcbiAqIEByZXR1cm4gaW1wdWVzdG9zU2VsZWN0b3JcbiAqL1xuZnVuY3Rpb24gaW1wdWVzdG9zU2VsZWN0MiAoaVBhcmFtcykge1xuICAgIHJldHVybiBzZWxlY3QyKFxuICAgICAgICBfLmV4dGVuZCh7XG4gICAgICAgICAgICBkYXRhIDogaVBhcmFtcy5pbXB1ZXN0b3NcbiAgICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKGMpIHsgcmV0dXJuIGMuZXN0YXR1cyA9PSAxfSlcbiAgICAgICAgICAgIC5tYXAoZnVuY3Rpb24gKGMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBpZDpjLmltcHVlc3RvX2Nvbmp1bnRvX2lkLFxuICAgICAgICAgICAgICAgICAgICB0ZXh0OmMuaW1wdWVzdG9fY29uanVudG8gKyAnICgnICsgTnVtYmVyKGMudGFzYSkgKyAnJSknXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgZmluZCA6IGZ1bmN0aW9uIChpZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpUGFyYW1zLmltcHVlc3Rvcy5maWx0ZXIoZnVuY3Rpb24gKGMpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGMuaW1wdWVzdG9fY29uanVudG9faWQgPT0gaWQ7XG4gICAgICAgICAgICAgICAgfSlbMF07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIGlQYXJhbXMpXG4gICAgKTtcbn1cblxuXG5mdW5jdGlvbiBjY3RvU2VsZWN0MiAgKGlQYXJhbXMpIHtcbiAgICByZXR1cm4gc2VsZWN0MihcbiAgICAgICAgXy5leHRlbmQoe1xuICAgICAgICAgICAgZGF0YSA6IGlQYXJhbXMuY2VudHJvRGVDb3N0by5lbGVtZW50b3MubWFwKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtpZDplLmVsZW1lbnRvX2Njb3N0b19pZCwgdGV4dDplLmVsZW1lbnRvfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxpUGFyYW1zKVxuICAgICk7XG59XG5cblxuLyoqXG4gKiBAcmV0dXJuIFNlbGVjdG9yIGRlIFRpcG9zIGRlIERvY3VtZW50b1xuICovXG5cbmZ1bmN0aW9uIHRpcG9Eb2NTZWxlY3QyIChpUGFyYW1zKSB7XG4gICAgcmV0dXJuIHNlbGVjdDIoXG4gICAgICAgIF8uZXh0ZW5kKHtcbiAgICAgICAgICAgIGRhdGEgOiBpUGFyYW1zLnRpcG9zRG9jLm1hcChmdW5jdGlvbiAodCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB7aWQ6dC50aXBvX2RvY3VtZW50b19pZCwgdGV4dDp0Lm5vbWJyZX07XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGZpbmQgOiBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaVBhcmFtcy50aXBvc0RvYy5maWx0ZXIoZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHQudGlwb19kb2N1bWVudG9faWQgPT0gaWQ7XG4gICAgICAgICAgICAgICAgfSlbMF07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSwgaVBhcmFtcylcbiAgICApO1xufVxuXG4vKipcbiAqIEByZXR1cm5cbiAqL1xuXG5mdW5jdGlvbiBpbnB1dGVyIChhdHRycywgYXR0cnNJbnB1dGVyLCBhdHRyc0lucHV0KSB7XG4gICAgYXR0cnNJbnB1dGVyID0gXy5leHRlbmQoe30sIGF0dHJzSW5wdXRlciwge2NvbmZpZzogY29uZmlnfSk7XG5cbiAgICBhdHRyc0lucHV0ID0gXy5leHRlbmQoe30sIHtcbiAgICAgICAgcGxhY2Vob2xkZXIgOiBhdHRycy5wbGFjZWhvbGRlciB8fCBhdHRycy5sYWJlbCxcbiAgICAgICAgb25jaGFuZ2UgOiBtLndpdGhBdHRyKCd2YWx1ZScsIGZ1bmN0aW9uICh2KSB7XG4gICAgICAgICAgICBhdHRycy5tb2RlbCh2KTtcbiAgICAgICAgICAgIGlmKHR5cGVvZiBhdHRycy5tb2RlbC5vbmNoYW5nZSA9PT0gJ2Z1bmN0aW9uJykgYXR0cnMubW9kZWwub25jaGFuZ2Uodik7XG4gICAgICAgIH0pLFxuICAgICAgICBvbmtleXVwIDogbS53aXRoQXR0cigndmFsdWUnLGF0dHJzLm1vZGVsKSxcbiAgICAgICAgdmFsdWUgOiBhdHRycy5tb2RlbCgpXG4gICAgfSwgYXR0cnNJbnB1dCk7XG5cbiAgICByZXR1cm4gbSgnZGl2Lm10LWlucHV0ZXInLCBhdHRyc0lucHV0ZXIsIFtcbiAgICAgICAgbSgnbGFiZWwnLCBhdHRycy5sYWJlbCksXG4gICAgICAgIG0oKGF0dHJzLmlucHV0IHx8ICdpbnB1dFt0eXBlPVwidGV4dFwiXScpLmNvbmNhdCggYXR0cnMucmVhZG9ubHkgPyAnW2Rpc2FibGVkXScgOiAnJyksIGF0dHJzSW5wdXQpXG4gICAgXSk7XG5cbiAgICBmdW5jdGlvbiBjb25maWcgKG5vZGUsIGluaXRpYWxpemVkKSB7XG4gICAgICAgIGlmKCFpbml0aWFsaXplZCkge1xuICAgICAgICAgICAgdmFyIGVsID0gJChub2RlKTtcblxuICAgICAgICAgICAgZWwuZGVsZWdhdGUoJ2lucHV0JywgJ2ZvY3VzJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGVsLmFkZENsYXNzKCdmb2N1cycpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGVsLmRlbGVnYXRlKCdpbnB1dCcsICdibHVyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGVsLnJlbW92ZUNsYXNzKCdmb2N1cycpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG5cbi8qKiBVU0VSIFNFVFRJTkdTICAqKi9cblxuZnVuY3Rpb24gdXNlclNldHRpbmdzICgpIHtcblxuICAgIHZhciBzZXR0aW5ncyA9IFtdLm1hcC5jYWxsKGFyZ3VtZW50cywgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBpdGVtLmpvaW4oJzonKTtcbiAgICAgICAgfSkuam9pbignLCcpO1xuXG4gICAgcmV0dXJuIG0ucmVxdWVzdCh7XG4gICAgICAgIG1ldGhvZCA6ICdHRVQnLFxuICAgICAgICB1cmwgOiAnL3Utc2V0dGluZ3Mvc2V0dGluZ3M/JyArICQucGFyYW0oeydzZXR0aW5ncycgOiBzZXR0aW5nc30pLFxuICAgICAgICB1bndyYXBTdWNjZXNzIDogZnVuY3Rpb24gKHIpIHsgcmV0dXJuIHIuZGF0YTsgfVxuICAgIH0pO1xufVxuXG5cbnVzZXJTZXR0aW5ncy5zYXZlID0gZnVuY3Rpb24gKGRhdGEpIHtcbiAgICByZXR1cm4gbS5yZXF1ZXN0KHtcbiAgICAgICAgbWV0aG9kIDogJ1BVVCcsXG4gICAgICAgIHVybCA6ICcvdS1zZXR0aW5ncy9zZXR0aW5ncycsXG4gICAgICAgIGRhdGEgIDogZGF0YVxuICAgIH0pO1xufTtcblxudXNlclNldHRpbmdzLmNob29zZSA9IGZ1bmN0aW9uIChrZXksIGRlZmF1bHRWYWx1ZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGk8YXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZihhcmd1bWVudHNbaV0gJiYgKHR5cGVvZiBhcmd1bWVudHNbaV1ba2V5XSkgIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gYXJndW1lbnRzW2ldW2tleV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcbiAgICB9XG59O1xuXG4vKipcbiAqXG4gKiBVVUlEXG4gKiovXG5cbmZ1bmN0aW9uIHM0KCkge1xuICAgIHJldHVybiBNYXRoLmZsb29yKCgxICsgTWF0aC5yYW5kb20oKSkgKiAweDEwMDAwKS50b1N0cmluZygxNikuc3Vic3RyaW5nKDEpO1xufVxuXG5mdW5jdGlvbiBndWlkKCkge1xuICAgIHJldHVybiBzNCgpICsgczQoKSArICctJyArIHM0KCkgKyAnLScgKyBzNCgpICsgJy0nICsgczQoKSArICctJyArIHM0KCkgKyBzNCgpICsgczQoKTtcbn1cblxuXG5cblxuZnVuY3Rpb24gdG9vbHRpcCAodG9vbHRpcEtleSkge1xuICAgIHJldHVybiB0b29sdGlwS2V5O1xufVxuXG4vKipcbiAqIE1vZGVsb3NcbiAqL1xuXG5mdW5jdGlvbiBPb3JNb2RlbHMgKCkge1xuICAgIGlmKCF3aW5kb3cubXRrKSByZXR1cm47XG4gICAgdmFyIG1tID0gbXRrLk1vZGVsTWFuYWdlcigpO1xuXG4gICAgdmFyIG1hcCA9IHtcbiAgICAgICAgcG9saXphcyA6ICdwb2xpemEnLFxuICAgICAgICB0ZXJjZXJvcyA6ICd0ZXJjZXJvcycsXG4gICAgICAgIG9wZXJhY2lvbmVzIDogJ29wZXJhY2lvbidcbiAgICB9O1xuXG4gICAgbW0uYXBpLnByZWZpeCgnL2FwaXYyJyk7XG5cbiAgICBtbS5hcGkuZ2V0ID0gZnVuY3Rpb24gKGlkKSB7XG5cbiAgICAgICAgdmFyIHFQYXJhbXMgPSB7bW9kZWxvIDogdGhpcy5zZXJ2aWNlTmFtZSgpfTtcbiAgICAgICAgcVBhcmFtc1t0aGlzLmlkS2V5KCldID0gIGlkO1xuXG4gICAgICAgIGlmKHRoaXMuaW5jbHVkZSgpKSB7XG4gICAgICAgICAgICBxUGFyYW1zLmluY2x1ZGUgPSB0aGlzLmluY2x1ZGUoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBtZXRob2QgOiAnR0VUJyxcbiAgICAgICAgICAgIHVybCA6IG1tLmFwaS5wcmVmaXgoKS5jb25jYXQoJz8nLCBtLnJvdXRlLmJ1aWxkUXVlcnlTdHJpbmcocVBhcmFtcykpLFxuICAgICAgICB9XG4gICAgfTtcblxuXG4gICAgbW0uYXBpLmNyZWF0ZSA9IGZ1bmN0aW9uIChyZXNvdXJjZSkge1xuICAgICAgICB2YXIgdXJsO1xuICAgICAgICBjb25zb2xlLmxvZyggbWFwW3RoaXMuc2VydmljZU5hbWUoKV0pO1xuXG4gICAgICAgIGlmKG1hcFt0aGlzLnNlcnZpY2VOYW1lKCldKSB7XG4gICAgICAgICAgICBpZihyZXNvdXJjZS4kbmV3ICYmIHJlc291cmNlLiRuZXcoKSA9PSBmYWxzZSAmJiByZXNvdXJjZS5lZGl0VXJsKSB7XG4gICAgICAgICAgICAgICAgdXJsID0gcmVzb3VyY2UuZWRpdFVybCgpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHVybCA9ICcvYXBpLycgKyBtYXBbdGhpcy5zZXJ2aWNlTmFtZSgpXSArICcvYWdyZWdhcic7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB1cmwgPSBtbS5hcGkucHJlZml4KCkuY29uY2F0KCc/bW9kZWxvPScgKyB0aGlzLnNlcnZpY2VOYW1lKCkgKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgcmV0dXJuICB7XG4gICAgICAgICAgICBtZXRob2QgOiAnUE9TVCcsXG4gICAgICAgICAgICB1cmwgOiB1cmwsXG4gICAgICAgICAgICBkYXRhIDogcmVzb3VyY2UudG9KU09OKClcbiAgICAgICAgfTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIG1tO1xufVxuXG5cbmZ1bmN0aW9uIHRlbXBsYXRlRnVuY3Rpb25zIChvb3IpIHtcblxuICAgIG9vci5wYW5lbCA9IGZ1bmN0aW9uIChzZXR0aW5ncywgYm9keSkge1xuICAgICAgICB2YXIgdGl0bGUgPSBzZXR0aW5ncy50aXRsZTtcbiAgICAgICAgaWYodHlwZW9mIHRpdGxlID09ICdzdHJpbmcnKXtcbiAgICAgICAgICAgIHRpdGxlID0gbSgnaDMnLCB0aXRsZSlcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBtKCdkaXYnLCBbXG4gICAgICAgICAgICBtKCcjdGl0bGUtaGVhZCcsIFtcbiAgICAgICAgICAgICAgICBtKCcuY29sLXhzLTEyLmNvbC1tZC0xMC50aXRsZS1iYXInLCB0aXRsZSksXG4gICAgICAgICAgICAgICAgbSgnLmNvbC14cy0xMi5jb2wtbWQtNi5hY3Rpb24tYmFyJywgWyBzZXR0aW5ncy5idXR0b25zID8gbSgnLnBhbmVsLWJ1dHRvbnMnLCBzZXR0aW5ncy5idXR0b25zKSA6IG51bGwgXSksXG4gICAgICAgICAgICAgICAgbSgnLmNsZWFyJyksXG4gICAgICAgICAgICBdKSxcblxuICAgICAgICAgICAgc2V0dGluZ3MuaW50ZXIgPyBzZXR0aW5ncy5pbnRlciA6IG51bGwsXG5cbiAgICAgICAgICAgIG0oJy5wYW5lbCcsIFtcbiAgICAgICAgICAgICAgICBtKCcucGFuZWwtYm9keScsYm9keSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICBdKTtcbiAgICB9O1xuXG4gICAgb29yLnN0cmlwZWRCdXR0b24gPSBmdW5jdGlvbiAodGFnLGNoaWxkcmVuLGF0dHJzKSB7XG4gICAgICAgIHJldHVybiBtKCh0YWcgfHwgJ2J1dHRvbicpLmNvbmNhdCgnLmJ0bi5idXR0b24tc3RyaXBlZC5idXR0b24tZnVsbC1zdHJpcGVkLmJ0bi1yaXBwbGUuYnRuLXhzJyksYXR0cnMsY2hpbGRyZW4pXG4gICAgfVxuXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGN1ZW50YXNTZWxlY3QyO1xuXG52YXIgQ3VlbnRhRm9ybSA9IHJlcXVpcmUoJy4uLy4uL2FqdXN0ZXMvY3VlbnRhcy1jb250YWJsZXMvQ3VlbnRhRm9ybScpO1xuIHZhciBhZGQgPSAnOjpBREQ6Oidcbi8qKlxuICogcmV0dXJuIEN1ZW50YXNTZWxlY3RvclxuICovXG5mdW5jdGlvbiBjdWVudGFzU2VsZWN0MihpUGFyYW1zKSB7XG5cblxuICAgXG4gICAgdmFyIGN1ZW50YXMgPSBtLnByb3AoKTtcbiAgICB2YXIgbEN1ZW50YXMgPSBidWlsZERhdGEoaVBhcmFtcyk7XG4gICAgdmFyIGN1ZW50YXNNYXAgPSBsQ3VlbnRhcy5tYXA7XG5cbiAgICBjdWVudGFzKGxDdWVudGFzLmN1ZW50YXMpO1xuXG4gICAgdmFyIHNlbGVjdG9yID0gb29yLnNlbGVjdDIoXG4gICAgICAgIF8uZXh0ZW5kKHtcbiAgICAgICAgICAgIGRhdGEgOiBjdWVudGFzLFxuICAgICAgICAgICAgZmluZCA6IGZ1bmN0aW9uIChpZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpUGFyYW1zLmN1ZW50YXMuZmlsdGVyKGZ1bmN0aW9uIChjKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjLmN1ZW50YV9jb250YWJsZV9pZCA9PSBpZDtcbiAgICAgICAgICAgICAgICB9KVswXTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZVJlc3VsdCA6IGZvcm1hdEN1ZW50YSh0cnVlKSxcbiAgICAgICAgICAgIHRlbXBsYXRlU2VsZWN0aW9uIDogZm9ybWF0Q3VlbnRhKClcbiAgICAgICAgfSwgaVBhcmFtcywge1xuICAgICAgICAgICAgb25jaGFuZ2UgOiBmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgICAgIGlmKGQgPT0gYWRkKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgQ3VlbnRhRm9ybS5vcGVuTW9kYWwuY2FsbChzZWxlY3Rvci5lbCgpWzBdLHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vbWJyZSA6ICdDcmVhciBDdWVudGEnLFxuICAgICAgICAgICAgICAgICAgICAgICAgb25zYXZlIDogZnVuY3Rpb24gKHJDdWVudGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3VlbnRhID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShyQ3VlbnRhKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VlbnRhLmFjdW11bGF0aXZhID0gJzAnO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaVBhcmFtcy5jdWVudGFzLnB1c2goY3VlbnRhKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsQ3VlbnRhcyA9IGJ1aWxkRGF0YShpUGFyYW1zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdWVudGFzTWFwID0gbEN1ZW50YXMubWFwO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VlbnRhcyhsQ3VlbnRhcy5jdWVudGFzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdG9yLnJlYnVpbGQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpUGFyYW1zLm1vZGVsKGN1ZW50YS5jdWVudGFfY29udGFibGVfaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG0ucmVkcmF3KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZihuaC5pc0Z1bmN0aW9uIChpUGFyYW1zLm9uY2hhbmdlKSkge1xuICAgICAgICAgICAgICAgICAgICBpUGFyYW1zLm9uY2hhbmdlKGQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICApO1xuXG4gICAgcmV0dXJuIHNlbGVjdG9yO1xuXG5cbiAgICBmdW5jdGlvbiBmb3JtYXRDdWVudGEgKGRldGFsbGUpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICAoaXRlbSkge1xuICAgICAgICAgICAgdmFyIGN1ZW50YSA9IGN1ZW50YXNNYXBbaXRlbS5pZCB8fCBpdGVtLmN0YV9pZF07XG4gICAgICAgICAgICB2YXIgY3VlbnRhUGFkcmUsIG5vbWJyZUN1ZW50YTtcbiAgICAgICAgICAgIGN1ZW50YSB8fMKgKCBjdWVudGEgPSB7fSApO1xuXG4gICAgICAgICAgICBpZihjdWVudGEuc3ViY3VlbnRhX2RlKSB7XG4gICAgICAgICAgICAgICAgY3VlbnRhUGFkcmUgPSBjdWVudGFzTWFwW2N1ZW50YS5zdWJjdWVudGFfZGVdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBub21icmVDdWVudGEgPSAnPGRpdiBjbGFzcz1cImN1ZW50YS1ub21icmVcIj4nO1xuXG4gICAgICAgICAgICBpZihjdWVudGFQYWRyZSkge1xuICAgICAgICAgICAgICAgIG5vbWJyZUN1ZW50YSArPSAnPHNtYWxsPicgK2N1ZW50YVBhZHJlLm5vbWJyZSAgKyAnPC9zbWFsbD4nICsgJyAmcmFxdW87ICdcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbm9tYnJlQ3VlbnRhICs9ICc8c3Ryb25nPicgKyBjdWVudGEubm9tYnJlICsgJzwvc3Ryb25nPic7XG4gICAgICAgICAgICBub21icmVDdWVudGEgKz0gJzwvZGl2Pic7XG5cbiAgICAgICAgICAgIHJldHVybiAkKCc8ZGl2IGNsYXNzPVwiY3VlbnRhLWRpc3BsYXkgJyArIChpdGVtLmlkICYmIGRldGFsbGUgPyAnZGV0YWxsZScgOiAnYWN1bXVsYXRpdmEnKSArICdcIj48ZGl2IGNsYXNzPVwiY3VlbnRhLWN1ZW50YVwiPicgKyBjdWVudGEuY3VlbnRhICsgJzwvZGl2PicgKyBub21icmVDdWVudGEgKyAnPC9kaXY+Jyk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG5cbmZ1bmN0aW9uIGJ1aWxkRGF0YSAoaVBhcmFtcykge1xuICAgIHZhciBjdWVudGFzTWFwID0ge307XG4gICAgdmFyIGluY2x1aXJDdGEgPSB7fTtcbiAgICB2YXIgYWdyZWdhckN0YSA9ICh0eXBlb2YgaVBhcmFtcy5hZ3JlZ2FyQ3RhICA9PSAndW5kZWZpbmVkJykgPyB0cnVlIDogaVBhcmFtcy5hZ3JlZ2FyQ3RhO1xuXG4gICAgXG4gICAgaVBhcmFtcy5jdWVudGFzLmZpbHRlcihmdW5jdGlvbiAoY3RhKSB7XG4gICAgICAgIHJldHVybiBjdGEuYWN1bXVsYXRpdmEgPT0gJzAnO1xuICAgIH0pLmZvckVhY2goZnVuY3Rpb24gKGN0YSkge1xuICAgICAgICBpbmNsdWlyQ3RhW2N0YS5jdWVudGFfY29udGFibGVfaWRdID0gdHJ1ZTtcbiAgICAgICAgaW5jbHVpckN0YVtjdGEuc3ViY3VlbnRhX2RlXSA9IHRydWU7XG4gICAgfSk7XG5cbiAgICBpUGFyYW1zLmN1ZW50YXMuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgICAgICBjdWVudGFzTWFwW2MuY3VlbnRhX2NvbnRhYmxlX2lkXSA9IGM7XG4gICAgfSk7XG5cbiAgICB2YXIgbEN1ZW50YXMgPSBpUGFyYW1zLmN1ZW50YXMuZmlsdGVyKGZ1bmN0aW9uIChjdGEpIHtcbiAgICAgICAgICAgIHJldHVybiBpbmNsdWlyQ3RhW2N0YS5jdWVudGFfY29udGFibGVfaWRdO1xuICAgICAgICB9KVxuICAgICAgICAubWFwKGZ1bmN0aW9uIChjdGEpIHtcbiAgICAgICAgICAgIHZhciBpZCA9IGN0YS5jdWVudGFfY29udGFibGVfaWQ7XG5cbiAgICAgICAgICAgIGlmKGN0YS5hY3VtdWxhdGl2YSA9PSAnMCcpe1xuICAgICAgICAgICAgICAgIHJldHVybiB7aWQ6aWQsIHRleHQ6Y3RhLmN1ZW50YSArICcgJyArIGN0YS5ub21icmV9O1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge2N0YV9pZCA6aWQsIHRleHQ6Y3RhLmN1ZW50YSArICcgJyArIGN0YS5ub21icmV9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgIGlmKGFncmVnYXJDdGEpIHtcblxuICAgICAgICBsQ3VlbnRhcy51bnNoaWZ0KHtcbiAgICAgICAgICAgdGV4dCA6ICdBZ3JlZ2FyIE51ZXZhIEN1ZW50YScsXG4gICAgICAgICAgIGlkIDogYWRkXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGN1ZW50YXNNYXBbYWRkXSA9IHtcbiAgICAgICAgICAgIG5vbWJyZTonJywgXG4gICAgICAgICAgICBjdWVudGE6ICdBZ3JlZ2FyIEN1ZW50YS4uLidcbiAgICAgICAgfTtcbiAgICB9XG5cblxuICAgIHJldHVybiB7XG4gICAgICAgIGN1ZW50YXMgOiBsQ3VlbnRhcyxcbiAgICAgICAgbWFwIDogY3VlbnRhc01hcFxuICAgIH07XG5cbn1cbiIsIlxubW9kdWxlLmV4cG9ydHMgPSBjdWVudGFzU2VsZWN0b3I7XG5cbmZ1bmN0aW9uIGN1ZW50YXNTZWxlY3RvciAocGFyYW1zKSB7XG4gICAgdmFyIGN1ZW50YXNNYXAgPSB7fTtcblxuICAgIHZhciBjdWVudGFzID0gcGFyYW1zLmN1ZW50YXNcbiAgICAgICAgLmZpbHRlciggZignZXN0YXR1cycpLmlzKCcxJykgKVxuICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICBjdWVudGFzTWFwW2QuY3VlbnRhX2NvbnRhYmxlX2lkXSA9IGQ7XG4gICAgICAgICAgICByZXR1cm4gZC5hY3VtdWxhdGl2YSA9PSAwO1xuICAgICAgICB9KTtcblxuICAgIGN1ZW50YXMuZm9yRWFjaChmdW5jdGlvbiAoZCkge1xuICAgICAgICBpZihkLnN1YmN1ZW50YV9kZSkge1xuICAgICAgICAgICAgZC5wYWRyZSA9IGN1ZW50YXNNYXBbZC5zdWJjdWVudGFfZGVdXG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIHJldHVybiB4dFNlbGVjdG9yKFxuICAgICAgICBuaC5leHRlbmQoe1xuICAgICAgICAgICAgZGF0YSA6IGN1ZW50YXMsXG4gICAgICAgICAgICB0ZXh0IDogZnVuY3Rpb24gKGQpIHsgXG4gICAgICAgICAgICAgICAgcmV0dXJuIFN0cmluZyhmKCdjdWVudGEnKShkKSkuY29uY2F0KCcgJyxmKCdub21icmUnKShkKSk7IFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVudGVyIDogZnVuY3Rpb24gKHNlbGVjdGlvbikge1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5odG1sKCcnKTtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb24uYXBwZW5kKCdkaXYnKS50ZXh0KCBmKCdjdWVudGEnKSApO1xuXG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uLmFwcGVuZCgnZGl2JykuYXR0cignY2xhc3MnLCdvcHQtbm9tYnJlLWN1ZW50YScpXG4gICAgICAgICAgICAgICAgICAgIC5odG1sKGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcyA9ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9pZihkLnBhZHJlKSBzID0gcy5jb25jYXQoJzxzbWFsbCBjbGFzcz1cInRleHQtZ3JleVwiPicsIGQucGFkcmUubm9tYnJlICwnPC9zbWFsbD4gJnJhcXVvOyAnKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICBzLmNvbmNhdCgnPHNtYWxsPicsIGQubm9tYnJlIHx8ICcnLCAnPC9zbWFsbD4nKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdmFsdWUgOiBmKCdjdWVudGFfY29udGFibGVfaWQnKVxuICAgICAgICB9LCBwYXJhbXMpXG4gICAgKTtcbn0iLCJ2YXIgQXJjaGl2b3NDb21wb25lbnRlID0gbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgdGl0bGUgICAgOiBtLnByb3AoJ0FyY2hpdm9zJyksXG4gICAgYnRuTGFiZWwgOiBbIG0oJ2kuZmEuZmEtcGFwZXJjbGlwJyksICcgQXJjaGl2b3MnXSxcbiAgICB2aWV3IDogYXJjaGl2b3NWaWV3XG59O1xuXG5cbmZ1bmN0aW9uIGFyY2hpdm9zVmlldyAodm5vZGUpIHtcblxuICAgIHJldHVybiBtKCcucm93Lm9wZXJhY2lvbi1jZmRpJywgW1xuICAgICAgICBtKCdoNicsICdDb21wb25lbnRlIERlc2hhYmlsaXRhZG8nKVxuICAgIF0pO1xuXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICB0aXRsZSAgICA6IG0ucHJvcCgnQml0w6Fjb3JhJyksXG4gICAgYnRuTGFiZWwgOiBbIG0oJ2kuZmEuZmEtbGlzdC1vbCcpLCAnIEJpdMOhY29yYSddLFxuICAgIHZpZXcgOiBiaXRhY29yYVZpZXdcbn07XG5cblxuXG5mdW5jdGlvbiBiaXRhY29yYVZpZXcgKHZub2RlKSB7XG4gICAgcmV0dXJuIG0oJy5yb3cub3BlcmFjaW9uLWNmZGknLCBbXG4gICAgICAgIG0oJ2g2JywgJ0NvbXBvbmVudGUgRGVzaGFiaWxpdGFkbycpXG4gICAgXSk7XG59XG4iLCJcbnZhciBDRkRJQ29tcG9uZW50ZSA9IG1vZHVsZS5leHBvcnRzID0ge1xuICAgIHRpdGxlICAgIDogbS5wcm9wKCdDRkRJJyksXG4gICAgYnRuTGFiZWwgOiBbIG0oJ2kuZmEuZmEtZmlsZS1jb2RlLW8nKSwgJyBDRkRJJ10sXG4gICAgdmlldyA6IGNmZGlWaWV3LFxuICAgIHhtbExpbmsgOiBmdW5jdGlvbiAobykgeyByZXR1cm4gJy9mYWN0dXJhY2lvbi9nZXRYTUwvJyArIG8uZm9saW9fZmlzY2FsIH0sXG4gICAgcGRmTGluayA6IGZ1bmN0aW9uIChvKSB7IHJldHVybiAnL2ZhY3R1cmFjaW9uL2dldFhNTC8nICsgby5mb2xpb19maXNjYWwgfVxufTtcblxuXG5cbmZ1bmN0aW9uIGNmZGlWaWV3ICh2bm9kZSkge1xuXG5cbiAgICByZXR1cm4gbSgnLnJvdy5vcGVyYWNpb24tY2ZkaScsIFtcbiAgICAgICAgbSgndWwuY29sLXhzLTYnLCBbXG4gICAgICAgICAgICBtKCdsaScsW1xuICAgICAgICAgICAgICAgIG0oJ2EuYnRuLmJ0bi1mbGF0LmJ0bi1kZWZhdWx0Jywge2hyZWY6Jy9mYWN0dXJhY2lvbi9nZXRYTUwvJyArIHRoaXMueG1sTGluayh2bm9kZS5hdHRycy5vcGVyYWNpb24pIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnaS5mYS5mYS1maWxlLWNvZGUtbycpLCAnIERlc2NhcmdhciBYTUwnXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIG0oJ2xpJyxbXG4gICAgICAgICAgICAgICAgbSgnYS5idG4uYnRuLWZsYXQuYnRuLWRlZmF1bHQnLCB7aHJlZjonL2ZhY3R1cmFjaW9uL2dldFhNTC8nICsgdGhpcy5wZGZMaW5rKHZub2RlLmF0dHJzLm9wZXJhY2lvbikgfSwgW1xuICAgICAgICAgICAgICAgICAgICBtKCdpLmZhLmZhLWZpbGUtcGRmLW8nKSwgICcgRGVzY2FyZ2FyIFBERidcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSksXG4gICAgICAgIF0pLFxuXG4gICAgICAgIG0oJ2Zvcm0uY29sLXhzLTYnLCBbXG4gICAgICAgICAgICBtKCcuaW5wdXQtZm9ybScsIFtcbiAgICAgICAgICAgICAgICBtKCdsYWJlbCcsICdFbnZpYXIgYTonKSxcbiAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPWVtYWlsXScpLFxuICAgICAgICAgICAgICAgIG0oJ2J1dHRvbi5idG4uYnRuLWZsYXQuYnRuLXByaW1hcnkuYnRuLXhzJywgJ0VudmlhcicpXG4gICAgICAgICAgICBdKVxuXG4gICAgICAgIF0pXG4gICAgXSk7XG59XG4iLCJcbnZhciBFbmNhYmV6YWRvT3BlcmFjaW9uID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcbnZhciBGZWNoYUlucHV0ICA9IHJlcXVpcmUoJy4uLy4uL2lucHV0cy9mZWNoYUlucHV0Jyk7XG52YXIgVGVyY2Vyb1NlbGVjdG9yID0gcmVxdWlyZSgnLi4vLi4vbWlzYy9zZWxlY3RvcnMvVGVyY2Vyb1NlbGVjdG9yLnYxLmpzJyk7XG5cbnZhciB2ZW5jaW1pZW50byA9IG0ucHJvcCgpO1xuXG5FbmNhYmV6YWRvT3BlcmFjaW9uLnZpZXcgPSBmdW5jdGlvbiAodm5vZGUpIHtcbiAgICB2YXIgb3BlcmFjaW9uID0gdm5vZGUuYXR0cnMub3BlcmFjaW9uO1xuXG4gICAgcmV0dXJuIG0oJy5lbmNhYmV6YWRvLW9wZXJhY2lvbicsIFtcblxuICAgICAgICBtKCcuY29sLXNtLTYnLCBbXG4gICAgICAgICAgICBtKFRlcmNlcm9TZWxlY3Rvciwge1xuICAgICAgICAgICAgICAgIG1vZGVsICAgOiB2bm9kZS5hdHRycy50ZXJjZXJvSWQsXG4gICAgICAgICAgICAgICAgdGVyY2VybyA6IG9wZXJhY2lvbi50ZXJjZXJvLFxuICAgICAgICAgICAgICAgIG9uY2hhbmdlIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdGVyY2VybyA9IHRoaXMuc2VsZWN0ZWQoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYodGVyY2Vybykge1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BlcmFjaW9uLnRlcmNlcm8gPSB0aGlzLnNlbGVjdGVkKCkuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wZXJhY2lvbi50ZXJjZXJvID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLFxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgbSgnaHInLCB7c3R5bGU6J21hcmdpbjoycHgnfSksXG5cbiAgICAgICAgICAgIG0oJ2g2Jywge3N0eWxlOidtYXJnaW4tYm90dG9tOjEwcHgnfSwgbSgnc3Bhbi5zbWFsbCcsICdEaXJlY2Npw7NuIEZpc2NhbDogJyksICdKb3NlIEx1aXMgTGFncmFuZ2UgIzEwMycpLFxuXG4gICAgICAgICAgICBtKCdoNicsbSgnc3Bhbi5zbWFsbCcsICdFbnRyZWdhciBFbjogJyksICdKb3NlIEx1aXMgTGFncmFuZ2UgIzEwMycpXG4gICAgICAgICAgICAqL1xuICAgICAgICBdKSxcblxuICAgICAgICBtKCcuY29sLXNtLTYnLCBbXG5cbiAgICAgICAgICAgIC8vbSgnZGl2W3N0eWxlPXBvc2l0aW9uOnJlbGF0aXZlO3RvcDotMjBweDtyaWdodDotMjBweDtiYWNrZ3JvdW5kLWNvbG9yOiNmMGYwZjA7cGFkZGluZzogMnB4IDhweDtib3JkZXItcmFkaXVzOjRweF0nLCBbXG5cblxuXG4gICAgICAgICAgICAgICAgbSgnaDYuc21hbGwucHVsbC1yaWdodCcsIHtcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUgOiAnYmFja2dyb3VuZC1jb2xvcjojZjlmOWY5O3BhZGRpbmc6NHB4O2JvcmRlci1yYWRpdXM6NHB4OyBwb3NpdGlvbjpyZWxhdGl2ZTsgdG9wOi0yMHB4O3JpZ2h0Oi0yMHB4O21hcmdpbi1ib3R0b206LTIwcHgnXG4gICAgICAgICAgICAgICAgfSwgW1xuICAgICAgICAgICAgICAgICAgICAnRXN0YXR1czogJywgbSgnc3Ryb25nLnRleHQtaW5kaWdvJywgJ0FQTElDQURBJyksXG4gICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICBtKCcuY2xlYXInKSxcblxuICAgICAgICAgICAgLy9dKSxcblxuICAgICAgICAgICAgbSgnLmZsZXgtcm93JywgW1xuICAgICAgICAgICAgICAgIG0oJy5tdC1pbnB1dGVyJywge3N0eWxlOidmbGV4OiAxIDUgJ30sIFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnUmVmZXJlbmNpYTogJyksXG4gICAgICAgICAgICAgICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA6IG9wZXJhY2lvbi5yZWZlcmVuY2lhLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQgOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICBtKCcuZmxleC1yb3cnLCBbXG4gICAgICAgICAgICAgICAgbSgnLm10LWlucHV0ZXInLCB7c3R5bGU6J2ZsZXg6IDEgNSAnfSwgW1xuICAgICAgICAgICAgICAgICAgICBtKCdsYWJlbCcsICdWZW5kZWRvcjogJyksXG4gICAgICAgICAgICAgICAgICAgIG0oJ2lucHV0W3R5cGU9dGV4dF0nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA6IG9wZXJhY2lvbi5yZWZlcmVuY2lhLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQgOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICBtKCcuZmxleC1yb3cnLCBbXG4gICAgICAgICAgICAgICAgbSgnLm10LWlucHV0ZXInLCB7c3R5bGU6J2ZsZXg6IDEgNSAnfSxbXG4gICAgICAgICAgICAgICAgICAgIG0oJ2xhYmVsJywgJ1ZlbmNpbWllbnRvOiAnKSxcbiAgICAgICAgICAgICAgICAgICAgbShGZWNoYUlucHV0LmNvbXBvbmVudCwgeyBtb2RlbCA6IHZlbmNpbWllbnRvIH0pXG4gICAgICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlcicsICB7c3R5bGU6J2ZsZXg6IDEgNSAnfSxbXG4gICAgICAgICAgICAgICAgICAgIG0oJ2xhYmVsJywgJ01vbmVkYTonKSxcbiAgICAgICAgICAgICAgICAgICAgbSgnaW5wdXRbdHlwZT10ZXh0XScsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlIDogb3BlcmFjaW9uLm1vbmVkYSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkIDogdHJ1ZVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcblxuXG4gICAgICAgICAgICBtKCcuZmxleC1yb3cnLCBbXG5cbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlcicsIHtzdHlsZTonZmxleDogMSA1ICd9LFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnVGlwbyBkZSBDYW1iaW86ICcpLFxuICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPXRleHRdJywgeyB2YWx1ZSA6IG9wZXJhY2lvbi50aXBvX2RlX2NhbWJpbyB9KVxuICAgICAgICAgICAgICAgIF0pLFxuXG5cbiAgICAgICAgICAgICAgICBtKCcubXQtaW5wdXRlcicsIHtzdHlsZTonZmxleDogMSA1ICd9LFtcbiAgICAgICAgICAgICAgICAgICAgbSgnbGFiZWwnLCAnVGFzYSBkZSBDYW1iaW86ICcpLFxuICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPXRleHRdJywgeyB2YWx1ZSA6IG9wZXJhY2lvbi50YXNhX2RlX2NhbWJpbyB9KVxuICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICBdKSxcblxuXG4gICAgICAgICAgICBtKCdoNi5zbWFsbC50ZXh0LWNlbnRlcicse1xuICAgICAgICAgICAgICAgIHN0eWxlOidtYXJnaW46MCAyMHB4OyBiYWNrZ3JvdW5kLWNvbG9yOiNmMGYwZjA7Ym9yZGVyLXJhZGl1czo0cHg7cGFkZGluZzo0cHgnXG4gICAgICAgICAgICB9LCBbXG4gICAgICAgICAgICAgICAgJ0FmZWN0YWNpw7NuIENvbnRhYmxlOiAnLCBtKCdzdHJvbmcudGV4dC1pbmRpZ28nLCAnRGlhcmlvICMzJylcbiAgICAgICAgICAgIF0pLFxuXG5cbiAgICAgICAgXSlcbiAgICBdKTtcbn1cbiIsInZhciBhcmNoaXZvcyA9IHJlcXVpcmUoJy4vQXJjaGl2b3NDb21wb25lbnRlJyk7XG52YXIgbm90YXMgPSByZXF1aXJlKCcuL05vdGFzQ29tcG9uZW50ZScpXG52YXIgY2ZkaSA9IHJlcXVpcmUoJy4vQ0ZESUNvbXBvbmVudGUnKVxudmFyIGJpdGFjb3JhID0gcmVxdWlyZSgnLi9CaXRhY29yYUNvbXBvbmVudGUnKVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBhcmNoaXZvcyA6IGNvbnZlcnRpcihhcmNoaXZvcyksXG4gICAgYml0YWNvcmEgOiBjb252ZXJ0aXIoYml0YWNvcmEpLFxuICAgIGNmZGkgOiBjb252ZXJ0aXIoY2ZkaSksXG4gICAgbm90YXMgOiBjb252ZXJ0aXIobm90YXMpXG59O1xuXG4vKipcbiAqIExlIHBvbmUgdW4gd3JhcCBhbGFzIGZ1bmNpb25lc1xuICovXG5mdW5jdGlvbiBjb252ZXJ0aXIgKGlDb21wb25lbnRlKSB7XG4gICAgdmFyIG9yaWdpbmFsVmlldyA9IGlDb21wb25lbnRlLnZpZXc7XG5cbiAgICByZXR1cm4gXy5leHRlbmQoe30sIGlDb21wb25lbnRlLCB7XG5cbiAgICAgICAgdmlldyA6IGZ1bmN0aW9uICh2bm9kZSkge1xuICAgICAgICAgICAgcmV0dXJuIG0oJ2RpdltzdHlsZT1wYWRkaW5nOjEwcHggMjBweF0nLCBbXG4gICAgICAgICAgICAgICAgbSgnLmNsb3NlJywgeyBvbmNsaWNrOnZub2RlLmF0dHJzLmNsb3NlIH0sJ8OXJyksXG4gICAgICAgICAgICAgICAgbSgnaDQudGV4dC1pbmRpZ28nLCB2bm9kZS5zdGF0ZS5idG5MYWJlbCksXG4gICAgICAgICAgICAgICAgb3JpZ2luYWxWaWV3LmNhbGwodm5vZGUuc3RhdGUsIHZub2RlKVxuICAgICAgICAgICAgXSk7XG4gICAgICAgIH1cblxuICAgIH0pO1xufVxuIiwibW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgdGl0bGUgICAgOiBtLnByb3AoJ05vdGFzJyksXG4gICAgYnRuTGFiZWwgOiBbIG0oJ2kuZmEuZmEtY29tbWVudHMtbycpLCAnIE5vdGFzJ10sXG4gICAgdmlldyA6IG5vdGFzVmlld1xufTtcblxuZnVuY3Rpb24gbm90YXNWaWV3ICh2bm9kZSkge1xuICAgIHJldHVybiBtKCcucm93Lm9wZXJhY2lvbi1jZmRpJywgW1xuICAgICAgICBtKCdoNicsICdDb21wb25lbnRlIERlc2hhYmlsaXRhZG8nKVxuICAgIF0pO1xufVxuIiwiXG52YXIgTGlzdGFkbyAgICAgID0gcmVxdWlyZSgnLi9saXN0YWRvL2xpc3RhZG8uanMnKTtcbnZhciB2ZXJPcGVyYWNpb24gPSByZXF1aXJlKCcuL3BhbnRhbGxhcy92ZXJPcGVyYWNpb24uanMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgbGlzdGFkbyAgICAgIDogTGlzdGFkbyxcbiAgICB2ZXJPcGVyYWNpb24gOiB2ZXJPcGVyYWNpb25cbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IEFwbGljYXI7XG5cbmZ1bmN0aW9uIEFwbGljYXIoY3R4KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoY3R4LiRzZWwubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0b2FzdHIuZXJyb3IoJ0RlYmVzIHNlbGVjY2lvbmFyIGFsIG1lbm9zIHVuYSBvcGVyYWNpw7NuJyk7XG4gICAgICAgIH1cblxuICAgICAgICBNVE1vZGFsLm9wZW4oe1xuICAgICAgICAgICAgYXJncyA6IHtcbiAgICAgICAgICAgICAgICBvcGVyYWNpb25lcyA6IGN0eC4kc2VsLFxuICAgICAgICAgICAgICAgIG9uRWplY3V0YXIgOiBjdHgucmVsb2FkLmJpbmQoY3R4KVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNvbnRyb2xsZXIgOiBBcGxpY2FyT3BlcmFjaW9uZXNDb250cm9sbGVyLFxuICAgICAgICAgICAgdG9wIDogQXBsaWNhck9wZXJhY2lvbmVzVG9wLFxuICAgICAgICAgICAgY29udGVudCA6IEFwbGljYXJPcGVyYWNpb25lc0NvbnRlbnQsXG4gICAgICAgICAgICBib3R0b20gOiBBcGxpY2FyT3BlcmFjaW9uZXNCb3R0b20sXG4gICAgICAgICAgICBlbCA6IHRoaXMsXG4gICAgICAgICAgICBtb2RhbEF0dHJzIDoge1xuICAgICAgICAgICAgICAgICdjbGFzcycgOiAnbW9kYWwtbWVkaXVtJ1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIEFwbGljYXJPcGVyYWNpb25lc1RvcCAoKSB7XG4gICAgcmV0dXJuIG0oJ2g0JywgJ0FwbGljYXIgT3BlcmFjaW9uZXMnKTtcbn1cblxuZnVuY3Rpb24gQXBsaWNhck9wZXJhY2lvbmVzQm90dG9tIChjdHgpIHtcbiAgICB2YXIgbW9zdHJhckFwbGljYXIgPSBjdHgucmVzdWx0YWRvcygpICYmIGN0eC5yZXN1bHRhZG9zKCkudmFsaWRhcy5sZW5ndGg7XG4gICAgbW9zdHJhckFwbGljYXIgPSBtb3N0cmFyQXBsaWNhciAmJiAhY3R4LmVqZWN1dGFuZG8oKTtcblxuICAgIHJldHVybiBbXG4gICAgICAgIG0oJ2J1dHRvbi5idG4tc21hbGwuYnRuLWZsYXQuYnRuLWRlZmF1bHQucHVsbC1sZWZ0JywgIHtvbmNsaWNrIDogY3R4LiRtb2RhbC5jbG9zZX0sIFtcbiAgICAgICAgICAgICdDYW5jZWxhcidcbiAgICAgICAgXSksXG5cbiAgICAgICAgbW9zdHJhckFwbGljYXIgPyBtKCdidXR0b24uYnRuLXNtYWxsLmJ0bi1mbGF0LmJ0bi1wcmltYXJ5LnB1bGwtcmlnaHQnLCB7b25jbGljayA6IGN0eC5lamVjdXRhckFjY2lvbn0sIFtcbiAgICAgICAgICAgICdBcGxpY2FyJ1xuICAgICAgICBdKSA6IG51bGxcbiAgICBdO1xufVxuXG5mdW5jdGlvbiBBcGxpY2FyT3BlcmFjaW9uZXNDb250ZW50IChjdHgpIHtcbiAgICB2YXIgcmVzdWx0YWRvcyA9IGN0eC5yZXN1bHRhZG9zKCk7XG4gICAgY3R4LmluaWNpYWxpemFyKCk7XG4gICAgaWYoY3R4LmluaWNpYWxpemFuZG8oKSkge1xuICAgICAgICByZXR1cm4gb29yLmxvYWRpbmcoKTtcbiAgICB9XG5cbiAgICByZXR1cm4gW1xuICAgICAgICByZXN1bHRhZG9zLmludmFsaWRhcy5sZW5ndGggPyBtKCcuYWxlcnQuYWxlcnQtd2FybmluZycsIFtcbiAgICAgICAgICAgIHJlc3VsdGFkb3MuaW52YWxpZGFzLmxlbmd0aCwgJyBPcGVyYWNpb25lcyBObyBzZSBwdWVkZW4gQXBsaWNhciAnXG4gICAgICAgIF0pOm51bGwsXG5cbiAgICAgICAgcmVzdWx0YWRvcy52YWxpZGFzLmxlbmd0aCA/IFtcbiAgICAgICAgICAgIG0oJ2g1JywgJ09wZXJhY2lvbmVzIHBvciBBcGxpY2FyOicpLFxuXG4gICAgICAgICAgICByZXN1bHRhZG9zLnZhbGlkYXMubWFwKGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG0oJ2Rpdi50aG1iLW9wZXJhY2lvbicsIHt9LFtcbiAgICAgICAgICAgICAgICAgICAgbSgnZGl2Jywgci5vcGVyYWNpb24ubm9tYnJlX2RvY3VtZW50bywgJyAnLCByLm9wZXJhY2lvbi5zZXJpZSwgJyAnLCByLm9wZXJhY2lvbi5udW1lcm8gfHwgJzxhdXRvPicpLFxuICAgICAgICAgICAgICAgICAgICBtKCcuc21hbGwudGV4dC1ncmV5Jywgci5yZWZlcmVuY2lhKVxuICAgICAgICAgICAgICAgIF0pO1xuICAgICAgICAgICAgfSlcblxuICAgICAgICBdIDogbSgnaDUnLCAnTm8gaGF5IG9wZXJhY2lvbmVzIHBvciBhcGxpY2FyJylcbiAgICBdO1xufVxuXG5cblxuZnVuY3Rpb24gQXBsaWNhck9wZXJhY2lvbmVzQ29udHJvbGxlciAoYXJncykge1xuICAgIHZhciBjdHggID0gdGhpcztcblxuICAgIGN0eC5pbmljaWFsaXphbmRvID0gbS5wcm9wKGZhbHNlKTtcbiAgICBjdHgucmVzdWx0YWRvcyA9IG0ucHJvcCgpO1xuICAgIGN0eC5lamVjdXRhbmRvID0gbS5wcm9wKGZhbHNlKTtcblxuICAgIGN0eC5pbmljaWFsaXphciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoY3R4LmluaWNpYWxpemFuZG8oKSB8fCBjdHgucmVzdWx0YWRvcygpKXtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGN0eC5pbmljaWFsaXphbmRvKHRydWUpO1xuXG4gICAgICAgIG9vci5yZXF1ZXN0KCcvb3BlcmFjaW9uZXMvYWNjaW9uL2FwbGljYXInLCAnUE9TVCcsIHtcbiAgICAgICAgICAgIGRhdGEgOiB7b3BlcmFjaW9uZXMgOiBhcmdzLm9wZXJhY2lvbmVzfVxuICAgICAgICB9KVxuICAgICAgICAudGhlbihmdW5jdGlvbiAocikge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICB2YWxpZGFzIDogci5maWx0ZXIoIGZ1bmN0aW9uIChkKSB7IHJldHVybiBkLmFjY2lvbi5wdWVkZSB9KSxcbiAgICAgICAgICAgICAgICBpbnZhbGlkYXMgOiByLmZpbHRlciggZnVuY3Rpb24gKGQpIHsgcmV0dXJuICFkLmFjY2lvbi5wdWVkZSB9KVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4oY3R4LnJlc3VsdGFkb3MpXG4gICAgICAgIC50aGVuKGN0eC5pbmljaWFsaXphbmRvLmJpbmQoY3R4LCBmYWxzZSkpO1xuICAgIH07XG5cblxuICAgIGN0eC5lamVjdXRhckFjY2lvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoY3R4LmVqZWN1dGFuZG8oKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHZhciBvcGVyYWNpb25lcyA9IGN0eC5yZXN1bHRhZG9zKCkudmFsaWRhcy5tYXAoIGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICByZXR1cm4gZC5vcGVyYWNpb24ub3BlcmFjaW9uX2lkXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGN0eC5lamVjdXRhbmRvKHRydWUpO1xuXG4gICAgICAgIG9vci5yZXF1ZXN0KCcvb3BlcmFjaW9uZXMvYWNjaW9uL2FwbGljYXIvZWplY3V0YXInLCAnUE9TVCcsIHtcbiAgICAgICAgICAgIGRhdGEgOiB7b3BlcmFjaW9uZXMgOiBvcGVyYWNpb25lc31cbiAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzdWx0YWRvcykge1xuXG4gICAgICAgICAgICByZXN1bHRhZG9zLm1hcChmdW5jdGlvbiAocikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHIuYWNjaW9uKVxuICAgICAgICAgICAgfSk7XG5cblxuICAgICAgICAgICAgY3R4LiRtb2RhbC5jbG9zZSgpO1xuICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ8KhQ2FtYmlvcyBSZWFsaXphZG9zIScpO1xuXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgIG5oLmlzRnVuY3Rpb24oYXJncy5vbkVqZWN1dGFyKSAmJiBhcmdzLm9uRWplY3V0YXIoKVxuICAgICAgICB9KVxuICAgIH1cbn1cbiIsIm1vZHVsZS5leHBvcnRzID0gUmVnaXN0cmFyUGFnbztcblxuZnVuY3Rpb24gUmVnaXN0cmFyUGFnbyAoY3R4KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYoY3R4LiRzZWwubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0b2FzdHIuZXJyb3IoJ0RlYmVzIHNlbGVjY2lvbmFyIGFsIG1lbm9zIHVuYSBvcGVyYWNpw7NuJyk7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGFyZ3VtZW50b3NNb2RhbCA9ICB7XG4gICAgICAgICAgICBvcGVyYWNpb25lcyA6IGN0eC4kc2VsLFxuICAgICAgICAgICAgb25FamVjdXRhciA6IGN0eC5yZWxvYWQuYmluZChjdHgpXG4gICAgICAgIH07XG5cbiAgICAgICAgYXJndW1lbnRvc01vZGFsWydWVEEnXSA9ICdpbmdyZXNvJztcbiAgICAgICAgYXJndW1lbnRvc01vZGFsWydWTkMnXSA9ICdlZ3Jlc28nO1xuICAgICAgICBhcmd1bWVudG9zTW9kYWxbJ0NPTSddID0gJ2VncmVzbyc7XG4gICAgICAgIGFyZ3VtZW50b3NNb2RhbFsnQ05DJ10gPSAnaW5ncmVzbyc7XG4gICAgICAgIGFyZ3VtZW50b3NNb2RhbC5zZWNjaW9uID0gY3R4LnNlY2Npb247XG5cbiAgICAgICAgTVRNb2RhbC5vcGVuKHtcbiAgICAgICAgICAgIGFyZ3MgOiBhcmd1bWVudG9zTW9kYWwsXG4gICAgICAgICAgICBjb250cm9sbGVyIDogUmVnaXN0cmFyUGFnb0NvbnRyb2xsZXIsXG4gICAgICAgICAgICAvL3RvcCA6IEFwbGljYXJPcGVyYWNpb25lc1RvcCxcbiAgICAgICAgICAgIGNvbnRlbnQgOiBSZWdpc3RyYXJQYWdvVmlldyxcbiAgICAgICAgICAgIGJvdHRvbSA6IFJlZ2lzdHJhclBhZ29Cb3R0b20sXG4gICAgICAgICAgICBlbCA6IHRoaXMsXG4gICAgICAgICAgICBtb2RhbEF0dHJzIDoge1xuICAgICAgICAgICAgICAgICdjbGFzcycgOiAnbW9kYWwtc21hbGwnXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gUmVnaXN0cmFyUGFnb0NvbnRyb2xsZXIgKGFyZ3MpIHtcbiAgICB2YXIgY3R4ID0gdGhpcztcblxuICAgIGN0eC5yZXN1bHRhZG9zID0gbS5wcm9wKCk7XG4gICAgY3R4LmxvYWRpbmcgPSBtLnByb3AoKTtcbiAgICBjdHguc2VjY2lvbiA9IGFyZ3Muc2VjY2lvbjtcblxuXG4gICAgY3R4LmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmKGN0eC5yZXN1bHRhZG9zKCkgfHwgY3R4LmxvYWRpbmcoKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgb29yLnJlcXVlc3QoJy9vcGVyYWNpb25lcy9hY2Npb24vcGFnYXInLCAnUE9TVCcsIHtcbiAgICAgICAgICAgIGRhdGEgOiB7b3BlcmFjaW9uZXMgOmFyZ3Mub3BlcmFjaW9uZXN9XG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0YWRvcyA9IHtpbmdyZXNvIDogW10sIGVncmVzbyA6IFtdLCBub1B1ZWRlIDogW119O1xuICAgICAgICAgICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICAgICAgdmFyIGtleSA9IGFyZ3Nbci5vcGVyYWNpb24udGlwb19vcGVyYWNpb25dO1xuICAgICAgICAgICAgICAgIGlmKHIuYWNjaW9uLnB1ZWRlICYmIHJlc3VsdGFkb3Nba2V5XSnCoHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0YWRvc1trZXldLnB1c2goci5vcGVyYWNpb24pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdGFkb3Mubm9QdWVkZS5wdXNoKHIub3BlcmFjaW9uKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBjdHgucmVzdWx0YWRvcyhyZXN1bHRhZG9zKTtcblxuICAgICAgICB9KVxuICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzdWx0YWRvcykge1xuICAgICAgICAgICAgdmFyIGRlZmluaXIgPSBmYWxzZTtcbiAgICAgICAgICAgIHZhciBwb3N0VXJsID0gJz9tb3ZpbWllbnRvcz0nO1xuXG4gICAgICAgICAgICByZXN1bHRhZG9zLmluZ3Jlc28uaWRzID0gcmVzdWx0YWRvcy5pbmdyZXNvLm1hcCggZignb3BlcmFjaW9uX2lkJykgKTtcbiAgICAgICAgICAgIHJlc3VsdGFkb3MuZWdyZXNvLmlkcyA9IHJlc3VsdGFkb3MuZWdyZXNvLm1hcCggZignb3BlcmFjaW9uX2lkJykgKTtcblxuICAgICAgICAgICAgcmVzdWx0YWRvcy5pbmdyZXNvLnVybCA9ICcvbW92aW1pZW50b3MvaW5ncmVzbycuY29uY2F0KFxuICAgICAgICAgICAgICAgICc/bW92aW1pZW50b3M9JyxcbiAgICAgICAgICAgICAgICByZXN1bHRhZG9zLmluZ3Jlc28uaWRzLmpvaW4oJywnKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgcmVzdWx0YWRvcy5lZ3Jlc28udXJsID0gJy9tb3ZpbWllbnRvcy9lZ3Jlc28nLmNvbmNhdChcbiAgICAgICAgICAgICAgICAnP21vdmltaWVudG9zPScsXG4gICAgICAgICAgICAgICAgcmVzdWx0YWRvcy5lZ3Jlc28uaWRzLmpvaW4oJywnKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgaWYocmVzdWx0YWRvcy5pbmdyZXNvLmxlbmd0aCAmJiAhcmVzdWx0YWRvcy5lZ3Jlc28ubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgZGVmaW5pciA9ICdpbmdyZXNvJztcbiAgICAgICAgICAgIH0gZWxzZSBpZihyZXN1bHRhZG9zLmVncmVzby5sZW5ndGggJiYgIXJlc3VsdGFkb3MuaW5ncmVzby5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBkZWZpbmlyID0gJ2VncmVzbyc7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKGRlZmluaXIpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSByZXN1bHRhZG9zW2RlZmluaXJdLnVybDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9KVxuICAgICAgICAudGhlbihjdHgubG9hZGluZy5iaW5kKG51bGwsZmFsc2UpKVxuXG4gICAgfVxufVxuXG5mdW5jdGlvbiBSZWdpc3RyYXJQYWdvVmlldyAoY3R4KSB7XG4gICAgdmFyIHJlc3VsdGFkb3MgPSBjdHgucmVzdWx0YWRvcygpO1xuICAgIGN0eC5pbml0aWFsaXplKCk7XG5cbiAgICBpZighcmVzdWx0YWRvcykgcmV0dXJuIG9vci5sb2FkaW5nKCk7XG5cbiAgICByZXR1cm4gbSgnZGl2JyxbXG4gICAgICAgIHJlc3VsdGFkb3Mubm9QdWVkZS5sZW5ndGggPyBtKCdkaXYuYWxlcnQuYWxlcnQtd2FybmluZycsIFtcbiAgICAgICAgICAgIHJlc3VsdGFkb3Mubm9QdWVkZS5sZW5ndGgsICcgT3BlcmFjaW9uZXMgbm8gc2UgcHVlZGVuIHBhZ2FyJ1xuICAgICAgICBdKSA6ICcnLFxuXG4gICAgICAgIG0oJ2g1JywgJ1NlbGVjY2lvbmEgdW5hIG9wY2nDs246JyksXG5cbiAgICAgICAgbSgndWwnLCBbXG4gICAgICAgICAgICByZXN1bHRhZG9zLmVncmVzby5sZW5ndGggPyBtKCdsaScsIFtcbiAgICAgICAgICAgICAgICBtKCdhJywge2hyZWYgOiByZXN1bHRhZG9zLmVncmVzby51cmwgfSwgW1xuICAgICAgICAgICAgICAgICAgICdSZWdpc3RyYXIgZWdyZXNvIHBhcmEgJyxcbiAgICAgICAgICAgICAgICAgICByZXN1bHRhZG9zLmVncmVzby5sZW5ndGgsXG4gICAgICAgICAgICAgICAgICAgJyAnLFxuICAgICAgICAgICAgICAgICAgIGN0eC5zZWNjaW9uKCkgPT0gJ1YnID8gJ05vdGFzIGRlIENyw6lkaXRvJyA6ICdGYWN0dXJhcycsXG4gICAgICAgICAgICAgICBdKVxuICAgICAgICAgICBdKSA6IG51bGwsXG5cbiAgICAgICAgICByZXN1bHRhZG9zLmVncmVzby5sZW5ndGggPyBtKCdsaScsIFtcbiAgICAgICAgICAgICAgIG0oJ2EnLCB7aHJlZiA6IHJlc3VsdGFkb3MuaW5ncmVzby51cmwgfSwgW1xuICAgICAgICAgICAgICAgICAgICdSZWdpc3RyYXIgaW5ncmVzbyBwYXJhICcsXG4gICAgICAgICAgICAgICAgICAgcmVzdWx0YWRvcy5pbmdyZXNvLmxlbmd0aCxcbiAgICAgICAgICAgICAgICAgICAnICcsXG4gICAgICAgICAgICAgICAgICAgIGN0eC5zZWNjaW9uKCkgPT0gJ1YnID8gJ0NvbXByYXMnIDogJ05vdGFzIGRlIENyw6lkaXRvIGRlIENvbXByYScsXG4gICAgICAgICAgICAgICBdKVxuICAgICAgICAgIF0pIDogbnVsbFxuICAgICAgICBdKVxuICAgIF0pO1xufVxuXG5cbmZ1bmN0aW9uIFJlZ2lzdHJhclBhZ29Cb3R0b20gKGN0eCkge1xuICAgIHJldHVybiBtKCdidXR0b24uLmJ0bi1zbS5idG4ucHVsbC1sZWZ0LmJ0bi1kZWZhdWx0Jywge29uY2xpY2s6Y3R4LiRtb2RhbC5jbG9zZX0sIFtcbiAgICAgICAgJ0NhbmNlbGFyJ1xuICAgIF0pO1xufVxuIiwibW9kdWxlLmV4cG9ydHMgPSBBY3Rpb25zT3BlcmFjaW9uZXM7XG5cbnZhciBBcGxpY2FyID0gcmVxdWlyZSgnLi9hY2Npb25lcy9BcGxpY2FyQWNjaW9uLmpzJyk7XG52YXIgUmVnaXN0cmFyUGFnbyA9ICByZXF1aXJlKCcuL2FjY2lvbmVzL1JlZ2lzdHJhclBhZ29BY2Npb24uanMnKTtcblxuZnVuY3Rpb24gQWN0aW9uc09wZXJhY2lvbmVzIChjdHgpIHtcbiAgICB2YXIgc2VsZWN0aW9uID0gY3R4LiRzZWwuc2VsZWN0aW9uKCk7XG5cbiAgICByZXR1cm4gbSgnLnJvdycsIHtzdHlsZToncGFkZGluZzogMTBweCAwJ30sIFtcbiAgICAgICAgbSgnLmNvbC1zbS0xMicsIFtcbiAgICAgICAgICAgIC8qbSgnYnV0dG9uLmJ0bi5idG4tZGVmYXVsdC5idG4tc20uYnRuLWZsYXQuZGlzYWJsZWQnLCBbXG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uLmxlbmd0aCA/IFN0cmluZyhzZWxlY3Rpb24ubGVuZ3RoKS5jb25jYXQoJyBPcGVyYWNpb25lcyBzZWxlY2Npb25hZGFzOicpICA6IG51bGxcbiAgICAgICAgICAgIF0pLCovXG4gICAgICAgICAgICBtKCdidXR0b24uYnRuLmJ0bi1kZWZhdWx0LmJ0bi1zbS5idG4tZmxhdCcsIHtcbiAgICAgICAgICAgICAgICBzdHlsZSA6ICdtYXJnaW4tcmlnaHQ6NXB4JyxcbiAgICAgICAgICAgICAgICBvbmNsaWNrIDogQXBsaWNhcihjdHgpLFxuICAgICAgICAgICAgfSwgW20oJ2kuaW9uLWNoZWNrbWFyay1yb3VuZC50ZXh0LWdyZWVuJykgLCcgQXBsaWNhciddKSxcblxuICAgICAgICAgICAgIG0oJ2J1dHRvbi5idG4uYnRuLWRlZmF1bHQuYnRuLXNtLmJ0bi1mbGF0Jywge1xuICAgICAgICAgICAgICAgIG9uY2xpY2sgOiBSZWdpc3RyYXJQYWdvKGN0eCksXG4gICAgICAgICAgICB9LCBbbSgnaS5pb24tY2FzaC50ZXh0LWluZGlnbycpICwnIFJlZ2lzdHJhciBQYWdvJ10pXG4gICAgICAgIF0pXG4gICAgXSk7XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHt2aWV3Okxpc3RhZG9WaWV3LCBjb250cm9sbGVyOkxpc3RhZG9Db250cm9sbGVyfTtcblxudmFyIFNlYXJjaEJhciAgPSByZXF1aXJlKCcuLi8uLi9taXNjL2NvbXBvbmVudHMvU2VhcmNoQmFyJyk7XG52YXIgRmVjaGFzQmFyICA9IHJlcXVpcmUoJy4uLy4uL21pc2MvY29tcG9uZW50cy9GZWNoYXNCYXIyJyk7XG52YXIgVGFibGFPcGVyYWNpb25lcyA9IHJlcXVpcmUoJy4vdGFibGFPcGVyYWNpb25lcycpO1xudmFyIEFjdGlvbnNPcGVyYWNpb25lcyA9IHJlcXVpcmUoJy4vYWN0aW9uc09wZXJhY2lvbmVzJyk7XG5cbmZ1bmN0aW9uIExpc3RhZG9Db250cm9sbGVyIChwYXJhbXMpIHtcbiAgICB2YXIgY3R4ID0gdGhpcztcbiAgICB2YXIgc2VhcmNoID0gbS5wcm9wKCcnKTtcbiAgICB2YXIgcXVlcnlQYXJhbXMgPSBtLnJvdXRlLnBhcnNlUXVlcnlTdHJpbmcobG9jYXRpb24uc2VhcmNoKTtcbiAgICBvb3IuZm9uZG9HcmlzKCk7XG5cbiAgICB0aGlzLnNlY2Npb24gPSBtLnByb3AocGFyYW1zLnNlY2Npb24pO1xuICAgIHRoaXMubG9hZGluZyA9IG0ucHJvcChmYWxzZSk7XG4gICAgdGhpcy5yZWFkeSA9IG0ucHJvcChmYWxzZSk7XG4gICAgdGhpcy50YWJzID0gbS5wcm9wKFtdKTtcbiAgICB0aGlzLnNlYXJjaCA9IG0ucHJvcCgnJyk7XG4gICAgdGhpcy50YWIgPSBtLnByb3AoKTtcbiAgICB0aGlzLnNvcnREaXJlY3Rpb24gPSBtLnByb3AoJysnKTtcbiAgICB0aGlzLnRvdGFsID0gbS5wcm9wKCk7XG4gICAgdGhpcy5vZmZzZXQgPSBtLnByb3AoKTtcbiAgICB0aGlzLmNvdW50ID0gbS5wcm9wKCk7XG4gICAgdGhpcy5saW1pdCA9IG0ucHJvcCg1MCk7XG4gICAgdGhpcy5zb3J0ID0gbS5wcm9wKCk7XG4gICAgdGhpcy5vcGVyYWNpb25lcyA9IG0ucHJvcCgpO1xuXG4gICAgY3R4LmlkQWNjaW9uICAgID0gbS5wcm9wKHRoaXMuc2VjY2lvbigpID09ICdjb21wcmFzJyA/ICdDb21wcmEnIDogJ1ZlbnRhJyk7XG4gICAgY3R4LnByaW5jaXBhbCAgID0gbS5wcm9wKHRoaXMuc2VjY2lvbigpID09ICdjb21wcmFzJyA/ICdjb20nICAgIDogJ3Z0YScpO1xuICAgIGN0eC5ub3RhQ3JlZGl0byA9IG0ucHJvcCh0aGlzLnNlY2Npb24oKSA9PSAnY29tcHJhcycgPyAnY25jJyAgICA6ICd2bmMnKTtcblxuICAgIC8vRXh0cmEgUGFyYW1zXG4gICAgY3R4LnRUZXJjZXJvSWQgICAgICA9IG0ucHJvcChxdWVyeVBhcmFtcy50VGVyY2Vyb0lkKTtcbiAgICBjdHgub3BWZW5kZWRvcklkICAgID0gbS5wcm9wKHF1ZXJ5UGFyYW1zLm9wVmVuZGVkb3JJZCk7XG4gICAgY3R4Lm9wU3VjdXJzYWxJZCAgICA9IG0ucHJvcChxdWVyeVBhcmFtcy5vcFN1Y3Vyc2FsSWQpO1xuICAgIGN0eC5vcE1vbmVkYSAgICAgICAgPSBtLnByb3AocXVlcnlQYXJhbXMub3BNb25lZGEpO1xuICAgIGN0eC5vcFRpcG9PcGVyYWNpb24gPSBtLnByb3AocXVlcnlQYXJhbXMub3BUaXBvT3BlcmFjaW9uKTtcblxuXG4gICAgY3R4LnNlYXJjaEJhciA9IG5ldyBTZWFyY2hCYXIuY29udHJvbGxlcih7XG4gICAgICAgIHNlYXJjaCA6IHNlYXJjaCxcbiAgICAgICAgb25zZWFyY2ggOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZihjdHguc2VhcmNoKCkgIT0gc2VhcmNoKCkpIHtcbiAgICAgICAgICAgICAgICBjdHguc2VhcmNoKHNlYXJjaCgpKTtcbiAgICAgICAgICAgICAgICBjdHgub2Zmc2V0KDApO1xuICAgICAgICAgICAgICAgIGN0eC5yZWFkeShmYWxzZSk7XG4gICAgICAgICAgICAgICAgbS5yZWRyYXcoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgY3R4LmZlY2hhc0JhciA9IG5ldyBGZWNoYXNCYXIuY29udHJvbGxlcih7XG4gICAgICAgIG9uY2hhbmdlIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY3R4Lm9mZnNldCgwKTtcbiAgICAgICAgICAgIGN0eC5yZWFkeShmYWxzZSk7XG4gICAgICAgICAgICAvL20ucmVkcmF3KCk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuXG4gICAgaWYoIXF1ZXJ5UGFyYW1zLmZlY2hhRGVzZGUgJiYgIXF1ZXJ5UGFyYW1zLmZlY2hhSGFzdGEpIHtcbiAgICAgICAgY3R4LmZlY2hhc0Jhci5lc3RlQW5vKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYocXVlcnlQYXJhbXMuZmVjaGFEZXNkZSkgY3R4LmZlY2hhc0Jhci5mZWNoYURlc2RlKHF1ZXJ5UGFyYW1zLmZlY2hhRGVzZGUpO1xuICAgICAgICBpZihxdWVyeVBhcmFtcy5mZWNoYUhhc3RhKSBjdHguZmVjaGFzQmFyLmZlY2hhSGFzdGEocXVlcnlQYXJhbXMuZmVjaGFIYXN0YSk7XG4gICAgfVxuXG5cblxuICAgIHRoaXMuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYodGhpcy5yZWFkeSgpIHx8IHRoaXMubG9hZGluZygpKSByZXR1cm47XG4gICAgICAgIHRoaXMubG9hZGluZyh0cnVlKTtcblxuICAgICAgICBvb3JkZW4oKS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIFRhYmxhT3BlcmFjaW9uZXMoY3R4KTtcblxuICAgICAgICAgICAgb29yLnJlcXVlc3QoJy9vcGVyYWNpb25lcy9saXN0YWRvLycuY29uY2F0KGN0eC5zZWNjaW9uKCkpLCAnR0VUJywge1xuICAgICAgICAgICAgICAgIGRhdGEgOiB7XG4gICAgICAgICAgICAgICAgICAgIGJ1c3F1ZWRhIDogY3R4LnNlYXJjaCgpID8gY3R4LnNlYXJjaCgpIDogdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgICAgICB0YWIgOiBjdHgudGFiKCksXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyIDogY3R4LnNvcnQoKSA/IGN0eC5zb3J0RGlyZWN0aW9uKCkgKyBjdHguc29ydCgpIDogdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgICAgICBvZmZzZXQgOiBjdHgub2Zmc2V0KCkgPyBjdHgub2Zmc2V0KCkgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgIGxpbWl0IDogY3R4LmxpbWl0KCkgPyBjdHgubGltaXQoKSA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgZmVjaGFfZGVzZGUgOiBjdHguZmVjaGFzQmFyLnZhbHVlLmZlY2hhRGVzZGUoKSA/IGN0eC5mZWNoYXNCYXIudmFsdWUuZmVjaGFEZXNkZSgpIDogdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgICAgICBmZWNoYV9oYXN0YSA6IGN0eC5mZWNoYXNCYXIudmFsdWUuZmVjaGFIYXN0YSgpID8gY3R4LmZlY2hhc0Jhci52YWx1ZS5mZWNoYUhhc3RhKCkgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgIHRUZXJjZXJvSWQgOiBjdHgudFRlcmNlcm9JZCgpID8gY3R4LnRUZXJjZXJvSWQoKSA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgb3BWZW5kZWRvcklkIDogY3R4Lm9wVmVuZGVkb3JJZCgpID8gY3R4Lm9wVmVuZGVkb3JJZCgpIDogdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgICAgICBvcFN1Y3Vyc2FsSWQgOiBjdHgub3BTdWN1cnNhbElkKCkgPyBjdHgub3BTdWN1cnNhbElkKCkgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgIG9wTW9uZWRhIDogY3R4Lm9wTW9uZWRhKCkgPyBjdHgub3BNb25lZGEoKSA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgb3BUaXBvT3BlcmFjaW9uIDogY3R4Lm9wVGlwb09wZXJhY2lvbigpID8gY3R4Lm9wVGlwb09wZXJhY2lvbigpIDogdW5kZWZpbmVkXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKGFzaWduYXJEYXRvcylcbiAgICAgICAgfSk7XG4gICAgfTtcblxuXG4gICAgdGhpcy5yZWxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMucmVhZHkoZmFsc2UpO1xuICAgICAgICB0aGlzLmluaXRpYWxpemUoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBhc2lnbmFyRGF0b3MgKGRhdGEpIHtcbiAgICAgICAgY3R4LmxvYWRpbmcoZmFsc2UpXG4gICAgICAgIGN0eC5yZWFkeSh0cnVlKVxuXG4gICAgICAgIE9iamVjdC5rZXlzKGRhdGEpLmZvckVhY2goZnVuY3Rpb24oa2V5KSB7XG4gICAgICAgICAgICB2YXIgcHJvcCA9IGN0eFtrZXldO1xuICAgICAgICAgICAgaWYobmguaXNGdW5jdGlvbihwcm9wKSkgcHJvcChkYXRhW2tleV0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cblxufVxuXG5mdW5jdGlvbiBMaXN0YWRvVmlldyAoY3R4KSB7XG4gICAgY3R4LmluaXRpYWxpemUoKTtcblxuXG4gICAgcmV0dXJuIG9vci5wYW5lbCh7XG4gICAgICAgIHRpdGxlIDogY3R4LnNlY2Npb24oKS50b1VwcGVyQ2FzZSgpLFxuICAgICAgICBidXR0b25zIDogW1xuICAgICAgICAgICAgb29yLnN0cmlwZWRCdXR0b24oJ2EuYnRuLXN1Y2Nlc3MnLCAnTnVldmEgJy5jb25jYXQoY3R4LmlkQWNjaW9uKCkpLCB7XG4gICAgICAgICAgICAgICAgc3R5bGUgOiAnbWFyZ2luLXJpZ2h0OjE4cHgnLFxuICAgICAgICAgICAgICAgIGhyZWY6Jy9vcGVyYWNpb25lcy8nLmNvbmNhdChjdHgucHJpbmNpcGFsKCkuY29uY2F0KCcvY3JlYXInKSksXG5cbiAgICAgICAgICAgIH0pLFxuXG4gICAgICAgICAgICBvb3Iuc3RyaXBlZEJ1dHRvbignYS5idG4td2FybmluZycsICdOdWV2YSBEZXZvbHVjacOzbicsIHtcbiAgICAgICAgICAgICAgICBzdHlsZSA6ICdtYXJnaW4tcmlnaHQ6MThweCcsXG4gICAgICAgICAgICAgICAgaHJlZjonL29wZXJhY2lvbmVzLycuY29uY2F0KGN0eC5ub3RhQ3JlZGl0bygpLmNvbmNhdCgnL2NyZWFyJykpLFxuXG4gICAgICAgICAgICB9KSxcblxuICAgICAgICAgICAgLypcbiAgICAgICAgICAgIG9vci5kcm9wZG93bih7XG4gICAgICAgICAgICAgICAgYnV0dG9uIDogb29yLnN0cmlwZWRCdXR0b24oJ2J1dHRvbi5idG4tcHJpbWFyeScsIFsnRXhwb3J0YXInLCBtKCdzcGFuLmNhcmV0JyldKSxcbiAgICAgICAgICAgICAgICBvcHRpb25zIDogW1xuICAgICAgICAgICAgICAgICAgICBtKCdhI21ha2UtcGRmW2hyZWY9amF2YXNjcmlwdDo7XScsIHt9LCBbbSgnaS5mYSBmYS1maWxlLXBkZi1vJyksXCIgUERGXCJdKSxcbiAgICAgICAgICAgICAgICAgICAgbSgnYSNtYWtlLWNzdltocmVmPWphdmFzY3JpcHQ6O10nLCB7fSwgW20oJ2kuZmEgZmEtZmlsZS1jc3YtbycpLFwiIENTVlwiXSksXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgIG0oJy5idG4tZ3JvdXAnLCBbXG4gICAgICAgICAgICAgICAgbSgnLmJ0biBidG4tcHJpbWFyeSBidXR0b24tc3RyaXBlZCBidXR0b24tZnVsbC1zdHJpcGVkIGJ1dHRvbi1zdHJpcGVkIGJ0bi1kZWZhdWx0IGJ0bi14cyBkcm9wZG93bi10b2dnbGUgYnRuLXJpcHBsZSAnLCB7J2RhdGEtdG9nZ2xlJzonZHJvcGRvd24nfSxcbiAgICAgICAgICAgICAgICAgICAgWydFeHBvcnRhciAgJyxtKFwic3Bhbi5jYXJldFwiKSx7c3R5bGU6J3dpZHRoOjMwcHgnfV1cbiAgICAgICAgICAgICAgICApLFxuXG4gICAgICAgICAgICAgICAgbShcInVsLmRyb3Bkb3duLW1lbnUuZHJvcGRvd24tbWVudS1yaWdodFwiLHtzdHlsZTonY29sb3I6ICMyMTYxOGMgO2ZvbnQtc2l6ZToxNHB4Oyd9LFtcbiAgICAgICAgICAgICAgICAgICAgbShcImxpXCIsIG0oJ2EjbWFrZS1wZGYtb3BlcmFjaW9uZXMnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBocmVmOidqYXZhc2NyaXB0OicsIG1vZGVsbyA6ICdPcGVyYWNpb25lcycsXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXN0YSA6ICdvcGVyYWNpb25lcy1tdWx0aSd9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbSgnaS5mYSBmYS1maWxlLXBkZi1vJyksXCIgUERGXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICksXG5cbiAgICAgICAgICAgICAgICAgICAgbShcImxpXCIsIG0oJ2EjbWFrZS1jc3Ytb3BlcmFjaW9uZXMnLHtocmVmOidqYXZhc2NyaXB0OicsIG1vZGVsbyA6ICdPcGVyYWNpb25lcycsIHZpc3RhIDogJ29wZXJhY2lvbmVzLW11bHRpJ30sICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbSgnaS5mYSBmYS1maWxlLWV4Y2VsLW8nKSxcIiBFeGNlbFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgIF1cbiAgICB9LCBbXG4gICAgICAgIG0oJy5yb3cnLCBbXG4gICAgICAgICAgICBtKCcuY29sLW1kLTgnLCBGZWNoYXNCYXIudmlldyhjdHguZmVjaGFzQmFyKSksXG4gICAgICAgICAgICBtKCcuY29sLW1kLTQnLCBbXG4gICAgICAgICAgICAgICAgbSgnLmZsZXgtcm93JywgW1xuICAgICAgICAgICAgICAgICAgICBTZWFyY2hCYXIudmlldyhjdHguc2VhcmNoQmFyKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgXSksXG5cbiAgICAgICAgTGlzdGFkb1RhYnMoY3R4KSxcblxuICAgICAgICBjdHgubG9hZGluZygpID8gb29yLmxvYWRpbmcoKSA6IG0oJy50YWJsZS1yZXNwb25zaXZlJywgW1xuICAgICAgICAgICAgIG0oJ3RhYmxlLnRhYmxlJywge2NvbmZpZzogVGFibGFPcGVyYWNpb25lcy5jb25maWcoY3R4KSB9KVxuICAgICAgICBdKVxuICAgIF0pO1xuXG5cbn1cblxuZnVuY3Rpb24gTGlzdGFkb1RhYnMgKGN0eCkge1xuICAgIHJldHVybiBtKCd1bC5uYXYubmF2LXRhYnMnLCBbXG4gICAgICAgIGN0eC50YWJzKCkubWFwKGZ1bmN0aW9uICh0YWIpIHtcbiAgICAgICAgICAgIHZhciBrbGFzcyA9IHRhYi5uYW1lID09IGN0eC50YWIoKSA/ICdhY3RpdmUnIDogJyc7XG4gICAgICAgICAgICByZXR1cm4gbSgnbGknLCB7J2NsYXNzJzprbGFzcywgb25jbGljazpzZWxlY3RUYWIoY3R4LHRhYiksZmlsdHJvOnRhYi5uYW1lfSwgW1xuICAgICAgICAgICAgICAgIG0oJ2EnLCB7aHJlZjonamF2YXNjcmlwdDonfSwgW1xuICAgICAgICAgICAgICAgICAgICB0YWIuY2FwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgJyAnLFxuICAgICAgICAgICAgICAgICAgICB0YWIuYmFkZ2UgPyBtKCdzcGFuLnQuc21hbGwnLCB7c3R5bGU6J2JvcmRlci1yYWRpdXM6MTJweDsgYmFja2dyb3VuZDojZjBmMGYwOyBwYWRkaW5nOjRweDsgd2lkdGg6MmVtJ30sIHRhYi5iYWRnZSkgOiBudWxsXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pO1xuICAgICAgICB9KVxuICAgIF0pO1xufVxuXG5mdW5jdGlvbiBzZWxlY3RUYWIoY3R4LCB0YWIpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZihjdHgudGFiKCkgPT0gdGFiLm5hbWUpIHJldHVybjtcbiAgICAgICAgY3R4LnRhYih0YWIubmFtZSk7XG4gICAgICAgIGN0eC5yZWFkeShmYWxzZSk7XG4gICAgICAgIG0ucmVkcmF3KCk7XG4gICAgfVxufVxuIiwibW9kdWxlLmV4cG9ydHMgPSBUYWJsYU9wZXJhY2lvbmVzO1xuXG52YXIgVGFibGUgPSByZXF1aXJlKCcuLi8uLi9uYWh1aS9kdGFibGUnKTtcbnZhciBGaWVsZCA9IHJlcXVpcmUoJy4uLy4uL25haHVpL25haHVpLmZpZWxkJyk7XG52YXIgY29sdW1ucztcbnZhciBmaWVsZHMgPSB7fTtcblxudmFyIGVzdGF0dXNlcyA9IG0ucHJvcChbXG4gICAgeyBlc3RhdHVzIDogJ1AnLCBub21icmUgOiAnRU4gUFJFUEFSQUNJw5NOJywgY29sb3IgOiAnZ3JleScsIG5vbWJyZV9jb3J0bzogJ0JvcnJhZG9yJ30sXG4gICAgeyBlc3RhdHVzIDogJ1QnLCBub21icmUgOiAnUEVORElFTlRFJywgICAgICBjb2xvciA6ICdhbWJlcicsIG5vbWJyZV9jb3J0bzogJ0ZpbmFsaXphZGEnfSxcbiAgICB7IGVzdGF0dXMgOiAnQScsIG5vbWJyZSA6ICdBUExJQ0FEQScsICAgICAgIGNvbG9yIDpudWxsLCBub21icmVfY29ydG8gOiAnQXBsaWNhZGEnfSxcbiAgICB7IGVzdGF0dXMgOiAnUycsIG5vbWJyZSA6ICdTQUxEQURBJywgICAgICAgIGNvbG9yIDogJ2dyZWVuJyAgLCBub21icmVfY29ydG8gOiAnU2FsZGFkYSd9LFxuICAgIHsgZXN0YXR1cyA6ICdYJywgbm9tYnJlIDogJ0NBTkNFTEFEQScsICAgICAgY29sb3IgOiAncmVkJywgbm9tYnJlX2NvcnRvIDogJ0NhbmNlbGFkYSd9LFxuXSk7XG5cbmVzdGF0dXNlcygpLmZvckVhY2goZnVuY3Rpb24gKHQpIHtcbiAgICBlc3RhdHVzZXNbdC5lc3RhdHVzXSA9IG0ucHJvcCh0KTtcbn0pO1xuXG5mdW5jdGlvbiBUYWJsYU9wZXJhY2lvbmVzIChjdHgpIHtcbiAgICBpZihjdHgudGFibGUpe1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gQXF1w60gc2UgY3JlYSB1bmEgdGFibGEsIGxlIHRlbmVtb3MgcXVlIHBhc2FyIGEgbGEgdGFibGEgdW5hIGZ1bmNpw7NuIHF1ZVxuICAgIC8vIHJlY3VwZXJlIGVsIGlkIGRlIHVuIHJlZ2lzdHJvLCBjb21vIGVsIGlkIGVzdMOhIGVuIGxhIHByb3BpZWRhZCBvcE9wZXJhY2lvbklkXG4gICAgLy8gZignb3BPcGVyYWNpb25JZCcpIGNyZWEgdW5hIGZ1bmNpw7NuIHF1ZSBkZXZ1ZWx2ZSBsYSBwcm9waWVkYWQgb3BPcGVyYWNpb25JZCBkZSBjdWFscXVpZXIgb2JqZXRvLlxuICAgIC8vIEVqZW1wbG86IGYoJ29wT3BlcmFjaW9uSWQnKSh7IG9wT3BlcmFjaW9uSWQgOiA5OX0pID09IDk5XG4gICAgY3R4LnRhYmxlID0gVGFibGUoKS5rZXkoIGYoJ29wT3BlcmFjaW9uSWQnKSApO1xuICAgIGN0eC50YWJsZS5tb3VudGVkID0gbS5wcm9wKGZhbHNlKTtcbiAgICBjdHgudGFibGUucm93cyA9IG0ucHJvcCgpXG4gICAgY3R4LnRhYmxlLnJlZHJhdyA9IG0ucHJvcChmYWxzZSlcblxuICAgIHZhciBmaWVsZHMgPSB7fTtcblxuICAgIC8vTGEgZnVuY2nDs24gRmllbGQgY3JlYSB1biBjYW1wbyBwYXJhIGxhcyB0YWJsYXMsXG4gICAgLy9BY2VwdGEgZG9zIHBhcmFtZXRyb3MgZWwgbmFtZSB5IGxhIGNvbmZpZ3VyYWNpw7NuIGRlbCBGaWVsZFxuICAgIC8vcG9yIGRlZmF1bHQgZWwgbm9tYnJlIGVzIGxhIHByb3BpZWRhZCBhIGxhIHF1ZSBzZSBoYWNlIHJlZmVyZW5jaWFcbiAgICBmaWVsZHMub3BPcGVyYWNpb25JZCA9IEZpZWxkKCdvcE9wZXJhY2lvbklkJyzCoHt9KTtcblxuXG4gICAgLy9TaSBsZSBwb25lbW9zIHVuIGNhcHRpb24gc2UgZWRpdGEgZWwgZW5jYWJlemFkbyBkZWwgcmVuZ2zDs25cbiAgICBmaWVsZHMub3BGZWNoYSA9IEZpZWxkKCdvcEZlY2hhJywge1xuICAgICAgICBjYXB0aW9uIDogJ0ZlY2hhJyxcbiAgICAgICAgZmlsdGVyIDogIG9vci5mZWNoYS5wcmV0dHlGb3JtYXRcbiAgICB9KTtcblxuXG4gICAgLy8gU2kgbGUgcG9uZW1vcyB1bmEgY2xhc3MgbGUgYXNpZ25hbW9zIGxhIGNsYXNlIHF1ZSBsbGV2YW4gbGFzIGNlbGRhcyBkZSBkYXRvc1xuICAgIC8vIFNpIHF1ZXJlbW9zIGVkaXRhciBsYSBjbGFzZSBxdWUgbGUgcG9uZW1vcyBhIGxhcyBjZWxkYXMgZGUgdGl0dWxvIGVzIGhlYWRpbmdDbGFzc1xuICAgIC8vIEZpbHRlcjogZXMgcGFyYSBkYXJsZSBmb3JtYXRvIGEgbG9zIGNhbXBvc1xuICAgIGZpZWxkcy5vcFVwZGF0ZWRBdCA9IEZpZWxkKCdvcFVwZGF0ZWRBdCcsIHtcbiAgICAgICAgY2FwdGlvbiA6ICcgJyxcbiAgICAgICAgJ2NsYXNzJyA6ICd0ZXh0LWdyZXkgc21hbGwnLFxuICAgICAgICBmaWx0ZXIgOiBmdW5jdGlvbiAoZCkgeyByZXR1cm4gZC5zcGxpdCgnICcpLmpvaW4oJ1QnKS5jb25jYXQoJy4wMDBaJyk7fSxcbiAgICAgICAgdXBkYXRlIDogZnVuY3Rpb24gKGQpIHsgZC5hdHRyKCd4LXRpbWUtdGlsbC1ub3cnLCBmKCd0ZXh0JykpOyB9XG4gICAgfSk7XG5cbiAgICAvL0ZpZWxkLnR3aWNlIGF5dWRhIGEgY3JlYXIgY2FtcG9zIGNvbmp1bnRvcywgZW4gbGEgdGFibGEgZGUgb3BlcmFjaW9uZXMsXG4gICAgLy9DYXNpIHRvZG9zIGxvcyBjYW1wb3Mgc29uIGNvbmp1bnRvcyAodGllbmVuIG3DoXMgZGUgdW4gZGF0bylcbiAgICAvL0VuIHN1YmZpZWxkcyBoYXkgcXVlIHBhc2FybGUgdW4gYXJyYXkgY29uIGxvcyBkYXRvcyBxdWUgdmFtb3MgYSBwb25lclxuICAgIGZpZWxkcy5mZWNoYXMgPSBGaWVsZC50d2ljZSgnZmVjaGFzJywge1xuICAgICAgICBzdWJmaWVsZHMgOiBbZmllbGRzLm9wRmVjaGEsIGZpZWxkcy5vcFVwZGF0ZWRBdF1cbiAgICB9KTtcblxuXG4gICAgLy9GaWVsZC5saW5rVG9SZXNvdXJjZSAgY3JlYSB1biB2w61uY3Vsb1xuICAgIC8vaGF5IHF1ZSBwYXNhciBsYSBmdW5jdGlvbiB1cmwgcXVlIHZhIGEgZGV2b2x2ZXIgbG8gcXVlIHF1ZXJlbW9zIHBvbmVyIGVuIGhyZWZcbiAgICBmaWVsZHMub3BUaXR1bG8gPSBGaWVsZC5saW5rVG9SZXNvdXJjZSgnb3BUaXR1bG8nLCB7XG4gICAgICAgIHVybCA6IGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICByZXR1cm4gJy9vcGVyYWNpb25lcy8nLmNvbmNhdChkLm9wVGlwb09wZXJhY2lvbi50b0xvd2VyQ2FzZSgpLCAnLycsIGQub3BPcGVyYWNpb25JZCk7XG4gICAgICAgIH0sXG4gICAgICAgIGNhcHRpb24gOiAnRG9jdW1lbnRvJyxcbiAgICAgICAgJ2NsYXNzJyA6ICd0ZXh0LWluZGlnbydcbiAgICB9KTtcblxuICAgIGZpZWxkcy5vcFJlZmVyZW5jaWEgPSBGaWVsZCgnb3BSZWZlcmVuY2lhJywge1xuICAgICAgICBjYXB0aW9uIDogJ1JlZmVyZW5jaWEnLFxuICAgICAgICAnY2xhc3MnIDogJ3RleHQtZ3JleSBzbWFsbCdcbiAgICB9KTtcblxuICAgIGZpZWxkcy5vcGVyYWNpb24gPSBGaWVsZC50d2ljZSgnb3BlcmFjaW9uJywge1xuICAgICAgICBzdWJmaWVsZHMgOiBbZmllbGRzLm9wVGl0dWxvLCBmaWVsZHMub3BSZWZlcmVuY2lhXVxuICAgIH0pO1xuXG4gICAgZmllbGRzLnROb21icmUgPSBGaWVsZCgndE5vbWJyZScse1xuICAgICAgICAnY2FwdGlvbicgOiBjdHguc2VjY2lvbigpID09ICd2ZW50YXMnID8gJ0NsaWVudGUnIDogJ1Byb3ZlZWRvcicsXG4gICAgfSk7XG5cbiAgICBmaWVsZHMudGVyY2VybyA9IGZpZWxkcy50Tm9tYnJlO1xuXG5cbiAgICBmaWVsZHMub3BUb3RhbCA9IEZpZWxkKCdvcFRvdGFsJywge1xuICAgICAgICBjYXB0aW9uIDogJ1RvdGFsJyxcbiAgICAgICAgJ2NsYXNzJyA6ICcnLFxuICAgICAgICBmaWx0ZXI6IG9vcmRlbi5vcmdhbml6YWNpb24uZm9ybWF0LFxuICAgICAgICBoZWFkaW5nQ2xhc3M6ICAnJ1xuICAgIH0pO1xuXG4gICAgZmllbGRzLm9wTW9uZWRhID0gRmllbGQoJ29wTW9uZWRhJywge1xuICAgICAgICBjYXB0aW9uIDogJ01vbmVkYScsXG4gICAgICAgICdjbGFzcycgOiAndGV4dC1ncmV5IHNtYWxsJyxcbiAgICAgICAgaGVhZGluZ0NsYXNzOiAgJydcbiAgICB9KTtcblxuICAgIGZpZWxkcy50b3RhbE1vbmVkYSA9IEZpZWxkLnR3aWNlKCd0b3RhbE1vbmVkYScsIHtcbiAgICAgICAgc3ViZmllbGRzIDogW2ZpZWxkcy5vcFRvdGFsLCBmaWVsZHMub3BNb25lZGFdXG4gICAgfSk7XG5cblxuICAgIHZhciBvcEVzdGF0dXNlcyA9IGVzdGF0dXNlcygpLmZpbHRlcihmKCdjb2xvcicpKTtcblxuICAgIGZpZWxkcy5vcFNhbGRvID0gRmllbGQoJ29wU2FsZG8nLCB7XG4gICAgICAgIGNhcHRpb24gOiAnU2FsZG8nLFxuICAgICAgICAnY2xhc3MnOiAndGV4dC1yaWdodCcsXG4gICAgICAgIGZpbHRlcjogb29yZGVuLm9yZ2FuaXphY2lvbi5mb3JtYXQsXG4gICAgICAgIGhlYWRpbmdDbGFzczogICd0ZXh0LXJpZ2h0JyxcblxuICAgICAgICB1cGRhdGUgOiBmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG4gICAgICAgICAgICBzZWxlY3Rpb24udGV4dChmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgICAgIHZhciBvcEVzdGF0dXMgPSBkLnJvdy5vcEVzdGF0dXM7XG4gICAgICAgICAgICAgICAgaWYob3BFc3RhdHVzID09ICdBJykgcmV0dXJuIG9vcmRlbi5vcmcuZm9ybWF0KGQucm93Lm9wU2FsZG8pXG4gICAgICAgICAgICAgICAgdmFyIHN0YXR1cyA9IGVzdGF0dXNlc1tvcEVzdGF0dXNdO1xuICAgICAgICAgICAgICAgIHJldHVybiBzdGF0dXMgPyBzdGF0dXMoKS5ub21icmVfY29ydG8gOiAnJztcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBvcEVzdGF0dXNlcy5mb3JFYWNoKGZ1bmN0aW9uIChzdCkge1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5jbGFzc2VkKCd0ZXh0LScuY29uY2F0KHN0LmNvbG9yKSwgZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGQucm93Lm9wRXN0YXR1cyA9PSBzdC5lc3RhdHVzO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuXG5cblxuICAgIGZpZWxkcy5vcEVzdGF0dXMgPSBGaWVsZCgnb3BFc3RhdHVzJywge1xuICAgICAgICBjYXB0aW9uIDogJ0VzdGF0dXMnLFxuICAgICAgICAnY2xhc3MnOiAnJyxcbiAgICAgICAgaGVhZGluZ0NsYXNzOiAgJycsXG5cbiAgICAgICAgZmlsdGVyIDogIGZ1bmN0aW9uIChzdCkge1xuICAgICAgICAgICAgdmFyIHN0YXR1cyA9IG9vci5tbSgnT3BlcmFjaW9uJykuZXN0YXR1c2VzW3N0XTtcbiAgICAgICAgICAgIHJldHVybiBzdGF0dXMgPyBzdGF0dXMoKS5ub21icmVfY29ydG8gOiAnJztcbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGUgOiBmdW5jdGlvbiAoc2VsZWN0aW9uKSB7XG5cbiAgICAgICAgICAgIHNlbGVjdGlvbi50ZXh0KGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGQudmFsdWUgPT0gJ0EnID8gb29yZGVuLm9yZy5mb3JtYXQoZC5yb3cub3BTYWxkbykgOiBkLnRleHQ7XG4gICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICBvcEVzdGF0dXNlcy5mb3JFYWNoKGZ1bmN0aW9uIChzdCkge1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5jbGFzc2VkKCd0ZXh0LScuY29uY2F0KHN0LmNvbG9yKSwgZnVuY3Rpb24gKGQpIHsgY29uc29sZS5sb2coKTsgcmV0dXJuIGQudmFsdWUgPT0gc3QuZXN0YXR1czsgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgZmllbGRzLnNhbGRvRXN0YXR1cyA9IGZpZWxkcy5vcEVzdGF0dXM7XG4gICAgLypcbiAgICBmaWVsZHMuc2FsZG9Fc3RhdHVzID0gRmllbGQudHdpY2UoJ3NhbGRvRXN0YXR1cycsIHtcbiAgICAgICAgc3ViZmllbGRzIDogW2ZpZWxkcy5vcFNhbGRvLCBmaWVsZHMub3BFc3RhdHVzXTdcbiAgICB9KTtcbiAgICAqL1xuXG4gICAgY3R4LiRzZWwgPSBbXTtcblxuICAgIGN0eC4kc2VsLmlzU2VsZWN0ZWQgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgcmV0dXJuIGN0eC4kc2VsLmluZGV4T2YoaWQpID4gLTE7XG4gICAgfVxuXG4gICAgY3R4LiRzZWwuc2VsZWN0ID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIGlmKGN0eC4kc2VsLmlzU2VsZWN0ZWQoaWQpKSByZXR1cm47XG4gICAgICAgIGN0eC4kc2VsLnB1c2goaWQpO1xuICAgIH1cblxuICAgIGN0eC4kc2VsLnVuc2VsZWN0ID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIHZhciBpZHggPSBjdHguJHNlbC5pbmRleE9mKGlkKVxuICAgICAgICBpZihpZHggPT0gLTEpIHJldHVybjtcbiAgICAgICAgY3R4LiRzZWwuc3BsaWNlKGlkeCwxKTtcbiAgICB9XG5cbiAgICBjdHguJHNlbC5zZWxlY3Rpb24gPSBmdW5jdGlvbiAocykge1xuICAgICAgICByZXR1cm4gY3R4LiRzZWw7XG4gICAgfVxuXG5cbiAgICBmaWVsZHMuY2ZkaSA9IEZpZWxkKCdvcEZvbGlvRmlzY2FsJywge1xuICAgICAgICBjYXB0aW9uIDogJ2NmZGknLFxuICAgICAgICBjbGFzcyA6ICdzbWFsbCcsXG4gICAgICAgIGhlYWRpbmdDbGFzcyA6ICd0ZXh0LWNlbnRlcicsXG4gICAgICAgICdjbGFzcycgOiAndGV4dC1jZW50ZXIgc21hbGwnLFxuICAgICAgICBlbnRlciA6IGZ1bmN0aW9uIChzZWwpIHtcbiAgICAgICAgICAgIHNlbC5jYWxsKEZpZWxkLmVudGVyKTtcblxuICAgICAgICAgICAgc2VsLmFwcGVuZCgnYScpXG4gICAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ3RleHQtaW5kaWdvIGNmZGkteG1sJylcbiAgICAgICAgICAgICAgICAuc3R5bGUoJ21hcmdpbicsICcycHgnKS50ZXh0KCdYTUwnKTtcbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGUgOiBmdW5jdGlvbiAoc2VsKSB7XG4gICAgICAgICAgICBzZWwuc2VsZWN0QWxsKCdhJykuc3R5bGUoJ2Rpc3BsYXknLCAgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQudmFsdWUgPyB1bmRlZmluZWQgOiAnbm9uZScgfSk7XG4gICAgICAgICAgICBzZWwuc2VsZWN0KCcuY2ZkaS14bWwnKS5hdHRyKCdocmVmJywgZG93bmxvYWRDRkRJKCd4bWwnKSApXG4gICAgICAgIH1cbiAgICB9KVxuXG5cbiAgICBmdW5jdGlvbiBkb3dubG9hZENGREkoZXh0KSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgcmV0dXJuICcvZmFjdHVyYWNpb24vZ2V0WE1MLycgKyBkLnJvdy5vcEZvbGlvRmlzY2FsO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZmllbGRzLmNoZWNrZXJzID0gRmllbGQoJ2NoZWNrZXJzJywge1xuICAgICAgICB2YWx1ZSA6IGYoJ29wT3BlcmFjaW9uSWQnKSxcbiAgICAgICAgY2FwdGlvbiA6ICcgJyxcbiAgICAgICAgZW50ZXJIZWFkaW5nIDogZnVuY3Rpb24gKHNlbCkge1xuICAgICAgICAgICAgc2VsLmNhbGwoRmllbGQuZW50ZXJIZWFkaW5nKTtcblxuICAgICAgICAgICAgc2VsLmFwcGVuZCgnaW5wdXQnKVxuICAgICAgICAgICAgICAgIC5hdHRyKCd0eXBlJywgJ2NoZWNrYm94JylcbiAgICAgICAgICAgICAgICAuYXR0cigndmFsdWUnLCBmKCd2YWx1ZScpKVxuICAgICAgICAgICAgICAgIC5vbignY2hhbmdlJywgZnVuY3Rpb24gKGQpIHtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgY2hlY2tlZCA9IGQzLnNlbGVjdCh0aGlzKS5wcm9wZXJ0eSgnY2hlY2tlZCcpO1xuXG4gICAgICAgICAgICAgICAgICAgIGN0eC50YWJsZS5yb3dzKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoZignb3BPcGVyYWNpb25JZCcpKVxuICAgICAgICAgICAgICAgICAgICAgICAgLm1hcChjdHguJHNlbFtjaGVja2VkID8gJ3NlbGVjdCcgOiAndW5zZWxlY3QnXSlcblxuICAgICAgICAgICAgICAgICAgICBjdHgudGFibGUucmVkcmF3KHRydWUpO1xuICAgICAgICAgICAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICBlbnRlciA6IGZ1bmN0aW9uIChzZWwpIHtcbiAgICAgICAgICAgIHNlbC5jYWxsKEZpZWxkLmVudGVyKTtcbiAgICAgICAgICAgIHNlbC5hcHBlbmQoJ2lucHV0JylcbiAgICAgICAgICAgICAgICAuYXR0cigndHlwZScsICdjaGVja2JveCcpXG4gICAgICAgICAgICAgICAgLmF0dHIoJ3ZhbHVlJywgZigndmFsdWUnKSlcbiAgICAgICAgICAgICAgICAub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChkKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKGQzLnNlbGVjdCh0aGlzKS5wcm9wZXJ0eSgnY2hlY2tlZCcpID09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN0eC4kc2VsLnNlbGVjdChkLnZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN0eC4kc2VsLnVuc2VsZWN0KGQudmFsdWUpXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBjdHgudGFibGUucmVkcmF3KHRydWUpO1xuICAgICAgICAgICAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH0sXG4gICAgICAgIHVwZGF0ZSA6IGZ1bmN0aW9uIChzZWwpIHtcbiAgICAgICAgICAgIHNlbC5zZWxlY3QoJ2lucHV0JykucHJvcGVydHkoJ2NoZWNrZWQnLCBmdW5jdGlvbiAoZCkgeyByZXR1cm4gY3R4LiRzZWwuaXNTZWxlY3RlZChkLnZhbHVlKTsgfSlcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgY3R4LnRhYmxlLnJvd1VwZGF0ZShmdW5jdGlvbiAoc2VsKSB7XG4gICAgICAgIHNlbC5jbGFzc2VkKCdpbmZvJywgZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgIHJldHVybiBjdHguJHNlbC5pc1NlbGVjdGVkKCBkLm9wT3BlcmFjaW9uSWQgKVxuICAgICAgICB9KVxuICAgIH0pO1xuXG4gICAgY3R4LnRhYmxlLmNvbHVtbnMgPSBbXG4gICAgICAgIGZpZWxkcy5jaGVja2VycyxcbiAgICAgICAgZmllbGRzLm9wRmVjaGEsXG4gICAgICAgIGZpZWxkcy5vcGVyYWNpb24sXG4gICAgICAgIC8vZmllbGRzLm9wUmVmZXJlbmNpYSxcbiAgICAgICAgZmllbGRzLnRlcmNlcm8sXG4gICAgICAgIGZpZWxkcy5vcE1vbmVkYSxcbiAgICAgICAgZmllbGRzLm9wVG90YWwsXG4gICAgICAgIGZpZWxkcy5zYWxkb0VzdGF0dXNcbiAgICBdO1xuXG5cbiAgICBjdHgudGFibGUuY29sdW1ucyA9IFtcbiAgICAgICAgZmllbGRzLmNoZWNrZXJzLFxuICAgICAgICAvL2ZpZWxkcy5vcFVwZGF0ZWRBdCxcbiAgICAgICAgZmllbGRzLm9wRmVjaGEsXG4gICAgICAgIC8vZmllbGRzLmZlY2hhcyxcbiAgICAgICAgLy9maWVsZHMub3BlcmFjaW9uLFxuICAgICAgICBmaWVsZHMub3BUaXR1bG8sXG4gICAgICAgIGZpZWxkcy5vcFJlZmVyZW5jaWEsXG4gICAgICAgIGZpZWxkcy50ZXJjZXJvLFxuICAgICAgICBmaWVsZHMudG90YWxNb25lZGEsXG4gICAgICAgIGZpZWxkcy5vcFNhbGRvXG4gICAgXTtcblxuICAgIGlmKG9vcmRlbi5vcmcoKS5jb2RpZ29fcGFpcyA9PT0gJ01YJykge1xuICAgICAgICBjdHgudGFibGUuY29sdW1ucy5wdXNoKGZpZWxkcy5jZmRpKTtcbiAgICB9XG59XG5cblxuVGFibGFPcGVyYWNpb25lcy5jb25maWcgPSBmdW5jdGlvbiAoY3R4KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChlbGVtZW50LCBpc0luaXRpYWxpemVkKSB7XG4gICAgICAgIGlmKCBjdHgudGFibGUubW91bnRlZCgpICkgcmV0dXJuO1xuXG4gICAgICAgIGlmKGN0eC50YWJsZS5yZWRyYXcoKSA9PSB0cnVlIHx8wqAoY3R4Lm9wZXJhY2lvbmVzKCkgJiYgY3R4Lm9wZXJhY2lvbmVzKCkgIT0gY3R4LnRhYmxlLnJvd3MoKSkgKSB7XG4gICAgICAgICAgICBjdHgudGFibGUucm93cyggY3R4Lm9wZXJhY2lvbmVzKCkgKTtcbiAgICAgICAgICAgIGN0eC50YWJsZS5yZWRyYXcoZmFsc2UpO1xuXG4gICAgICAgICAgICBkMy5zZWxlY3QoZWxlbWVudCkuY2FsbChjdHgudGFibGUsIGN0eC50YWJsZS5jb2x1bW5zLCBjdHgudGFibGUucm93cygpLCB7XG4gICAgICAgICAgICAgICAgc29ydCA6IGN0eC5zb3J0LFxuICAgICAgICAgICAgICAgIHNvcnREaXJlY3Rpb24gOiBjdHguc29ydERpcmVjdGlvbixcbiAgICAgICAgICAgICAgICB0b3RhbCA6IGN0eC50b3RhbCgpLFxuICAgICAgICAgICAgICAgIG9mZnNldCA6IGN0eC5vZmZzZXQoKSxcbiAgICAgICAgICAgICAgICBwYWdlU2l6ZSA6IGN0eC5saW1pdCgpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgb29yLnRpbWUudXBkYXRlKGVsZW1lbnQpXG4gICAgICAgIH1cblxuICAgICAgICBpZihpc0luaXRpYWxpemVkKSByZXR1cm47XG5cblxuICAgICAgICAkKGVsZW1lbnQpLm9uKCdjbGljaycsICdbZGF0YS1zb3J0LWJ5XScsIGZ1bmN0aW9uIChldikge1xuICAgICAgICAgICAgdmFyIHNvcnQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc29ydC1ieScpO1xuICAgICAgICAgICAgaWYoY3R4LnNvcnQoKSA9PSBzb3J0KSB7XG4gICAgICAgICAgICAgICAgY3R4LnNvcnREaXJlY3Rpb24oY3R4LnNvcnREaXJlY3Rpb24oKSA9PSAnKycgPyAnLScgOiAnKycpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGN0eC5zb3J0KHNvcnQpO1xuICAgICAgICAgICAgY3R4LnJlYWR5KGZhbHNlKTtcbiAgICAgICAgICAgIG0ucmVkcmF3KClcbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkKGVsZW1lbnQpLm9uKCdjbGljaycsICdbZGF0YS1wYWdlLXByZXZdJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIG9mZnNldCA9IE51bWJlcihjdHgub2Zmc2V0KCkpIC0gTnVtYmVyKGN0eC5saW1pdCgpKTtcbiAgICAgICAgICAgIGlmKG9mZnNldCA8IDApIHJldHVybjtcblxuICAgICAgICAgICAgY3R4Lm9mZnNldChvZmZzZXQpO1xuICAgICAgICAgICAgY3R4LnJlYWR5KGZhbHNlKTtcbiAgICAgICAgICAgIG0ucmVkcmF3KCk7XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgJChlbGVtZW50KS5vbignY2xpY2snLCAnW2RhdGEtcGFnZS1uZXh0XScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBvZmZzZXQgPSBOdW1iZXIoY3R4Lm9mZnNldCgpKSArIE51bWJlcihjdHgubGltaXQoKSkgO1xuICAgICAgICAgICAgaWYob2Zmc2V0ID49IGN0eC50b3RhbCgpKSByZXR1cm47XG5cbiAgICAgICAgICAgIGN0eC5vZmZzZXQob2Zmc2V0KTtcbiAgICAgICAgICAgIGN0eC5yZWFkeShmYWxzZSk7XG4gICAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICB9KTtcblxuICAgIH1cbn1cbiIsInZhciB2ZXJPcGVyYWNpb24gICAgICAgID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcbnZhciBGZWNoYUlucHV0ICAgICAgICAgID0gcmVxdWlyZSgnLi4vLi4vaW5wdXRzL2ZlY2hhSW5wdXQnKTtcbnZhciBFbmNhYmV6YWRvT3BlcmFjaW9uID0gcmVxdWlyZSgnLi4vY29tcG9uZW50ZXMvRW5jYWJlemFkb09wZXJhY2lvbi5qcycpO1xudmFyIGludGVyICAgICAgICAgICAgICAgPSByZXF1aXJlKCcuLi9jb21wb25lbnRlcy9JbnRlckNvbXBvbmVudGVzLmpzJyk7XG5cblxudmVyT3BlcmFjaW9uLm9uaW5pdCA9IGZ1bmN0aW9uICh2bm9kZSkge1xuICAgIHZhciBpZE9wZXJhY2lvbiA9ICdmZjkwNjdmYS05YzM1LTNmNDMtZGM1OS00NGJkOGM1ZGMwYzcnO1xuXG4gICAgdGhpcy5sb2FkaW5nICAgICAgICAgID0gbS5wcm9wKGZhbHNlKTtcbiAgICB0aGlzLm9wZXJhY2lvbiAgICAgICAgPSBtLnByb3AoKTtcbiAgICB0aGlzLmNvbXBvbmVudGVBY3Rpdm8gPSBtLnByb3AoKTtcblxuICAgIHRoaXMuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYodGhpcy5sb2FkaW5nKCkgfHwgdGhpcy5vcGVyYWNpb24oKSkgcmV0dXJuO1xuXG4gICAgICAgIHRoaXMubG9hZGluZyh0cnVlKTtcblxuICAgICAgICBvb3IucmVxdWVzdCgnL29wZXJhY2lvbi92dGEvJyArIGlkT3BlcmFjaW9uKS5ydW4oZnVuY3Rpb24gKHIpIHtcbiAgICAgICAgICAgIHZub2RlLnN0YXRlLm9wZXJhY2lvbihyKTtcbiAgICAgICAgICAgIHZub2RlLnN0YXRlLmxvYWRpbmcoZmFsc2UpO1xuICAgICAgICAgICAgdm5vZGUuc3RhdGUudGVyY2Vyb0lkID0gbS5wcm9wKHIudGVyY2Vyb19pZCk7XG4gICAgICAgIH0pO1xuICAgIH07XG59XG5cbnZlck9wZXJhY2lvbi52aWV3ID0gZnVuY3Rpb24gKHZub2RlKSB7XG4gICAgdGhpcy5pbml0aWFsaXplKCk7XG4gICAgdmFyIG9wZXJhY2lvbiA9IHRoaXMub3BlcmFjaW9uKCk7XG5cbiAgICByZXR1cm4gb29yLnBhbmVsKHtcbiAgICAgICAgdGl0bGUgICA6IHRoaXMuaGVhZGluZygpLFxuICAgICAgICBpbnRlciAgIDogdGhpcy5pbnRlckNvbXBvbmVudGUodm5vZGUpLFxuICAgICAgICBidXR0b25zIDogdGhpcy5sb2FkaW5nKCkgPyBudWxsIDogdGhpcy5idXR0b25zKClcbiAgICB9LCBbXG4gICAgICAgIHRoaXMubG9hZGluZygpID8gb29yLmxvYWRpbmcoKSA6ICcnLFxuXG4gICAgICAgIHRoaXMubG9hZGluZygpID8gbnVsbCA6IFtcblxuICAgICAgICAgICAgLy9FbmNhYmV6YWRvIGRlIGxhIG9wZXJhY2nDs25cbiAgICAgICAgICAgIG0oRW5jYWJlemFkb09wZXJhY2lvbiwge1xuICAgICAgICAgICAgICAgIHRlcmNlcm9JZCA6IHZub2RlLnN0YXRlLnRlcmNlcm9JZCxcbiAgICAgICAgICAgICAgICBvcGVyYWNpb24gOiBvcGVyYWNpb25cbiAgICAgICAgICAgIH0pLFxuXG4gICAgICAgICAgICBtKCdicicpLFxuXG4gICAgICAgICAgICBtKCcuY2xlYXInKSxcblxuICAgICAgICAgICAgbSgnaDQnLCB7c3R5bGU6J21hcmdpbi10b3A6MTBweCd9LCAnQ29uY2VwdG9zOicpLFxuICAgICAgICAgICAgbSgnYnInKSxcblxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgbSgnLnJvdycsIFtcbiAgICAgICAgICAgICAgICBvb3Iuc3RyaXBlZEJ1dHRvbignYnV0dG9uLnB1bGwtcmlnaHQuYnRuLXByaW1hcnknLCBbXG4gICAgICAgICAgICAgICAgICAgIG0oJ2kuaW9uLWFyY2hpdmUnKSwgJyBHdWFyZGFyJ1xuICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgbSgnLmJ0bi1ncm91cC5wdWxsLXJpZ2h0Jywge3N0eWxlOiAnbWFyZ2luOjAgNDBweCd9LCBbXG5cbiAgICAgICAgICAgICAgICAgICAgb29yLnN0cmlwZWRCdXR0b24oJ2J1dHRvbi5idG4tc3VjY2VzcycsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIG0oJ2kuaW9uLWNoZWNrbWFyay1yb3VuZCcpLCAnIEFwbGljYXInXG4gICAgICAgICAgICAgICAgICAgIF0pLFxuXG4gICAgICAgICAgICAgICAgICAgIG9vci5zdHJpcGVkQnV0dG9uKCdidXR0b24uYnRuLXN1Y2Nlc3MnLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBtKCdpLmZhLmZhLWZpbGUtY29kZS1vJyksICcgVGltYnJhciBDRkRJJ1xuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSksXG5cbiAgICAgICAgICAgIG0oJ3JvdycsIFtcbiAgICAgICAgICAgICAgICBtKCdsYWJlbC5wdWxsLXJpZ2h0JywgW1xuICAgICAgICAgICAgICAgICAgICBtKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLCAnLi4uIHkgcmVncmVzYXIgYSBjYXB0dXJhJ1xuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKVxuXG4gICAgICAgICAgICAqL1xuICAgICAgICBdXG4gICAgXSk7XG59XG5cblxudmVyT3BlcmFjaW9uLm9yZyA9IG9vcmRlbi5vcmc7XG5cbnZlck9wZXJhY2lvbi5pbnRlckNvbXBvbmVudGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYoIXRoaXMuY29tcG9uZW50ZUFjdGl2bygpKSByZXR1cm4gbnVsbDtcbiAgICByZXR1cm4gbSh0aGlzLmNvbXBvbmVudGVBY3Rpdm8oKSwge1xuICAgICAgICBjbG9zZSAgICAgIDogdGhpcy5jb21wb25lbnRlQWN0aXZvLmJpbmQobnVsbCxmYWxzZSksXG4gICAgICAgIG9wZXJhY2lvbiAgOiB0aGlzLm9wZXJhY2lvbigpXG4gICAgfSk7XG59O1xuXG5cbnZlck9wZXJhY2lvbi5idG5Db21wb25lbnRlID0gZnVuY3Rpb24gKGFDb21wb25lbnQpIHtcbiAgICByZXR1cm4gbSgnYnV0dG9uLmJ0bi5idG4tZmxhdC5idG4teHMnLCB7XG4gICAgICAgIG9uY2xpY2sgOiB0aGlzLmNvbXBvbmVudGVBY3Rpdm8uYmluZChudWxsLCBhQ29tcG9uZW50KSxcbiAgICAgICAgJ2NsYXNzJyA6IHRoaXMuY29tcG9uZW50ZUFjdGl2bygpID09IGFDb21wb25lbnQgPyAnYnRuLXByaW1hcnknIDogJ2J0bi1kZWZhdWx0J1xuICAgIH0sIGFDb21wb25lbnQuYnRuTGFiZWwpO1xufVxuXG52ZXJPcGVyYWNpb24uYnV0dG9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gW1xuICAgICAgICBtKCdkaXYuYnRuLWdyb3VwJyxbXG4gICAgICAgICAgICB0aGlzLm9yZygpLmNvZGlnb19wYWlzID09ICdNWCcgPyBbXG4gICAgICAgICAgICAgICAgdGhpcy5vcGVyYWNpb24oKS5mb2xpb19maXNjYWwgPyB0aGlzLmJ0bkNvbXBvbmVudGUoaW50ZXIuY2ZkaSkgOiAndGhpcy5idG5UaW1icmFyQ2ZkaSgpJ1xuICAgICAgICAgICAgXSA6IG51bGwsXG4gICAgICAgICAgICB0aGlzLmJ0bkNvbXBvbmVudGUoaW50ZXIuYXJjaGl2b3MpLFxuICAgICAgICAgICAgdGhpcy5idG5Db21wb25lbnRlKGludGVyLmJpdGFjb3JhKSxcbiAgICAgICAgICAgIHRoaXMuYnRuQ29tcG9uZW50ZShpbnRlci5ub3RhcylcbiAgICAgICAgXSksXG5cbiAgICAgICAgbSgnZGl2LmJ0bi1ncm91cCcsW1xuICAgICAgICAgICAgb29yLnN0cmlwZWRCdXR0b24oJ2J1dHRvbi5idG4tcHJpbWFyeS5idG4teHMnLCBbJ0FjY2lvbmVzICcsIG0oJ3NwYW4uY2FyZXQnKV0sIHt9KVxuICAgICAgICBdKVxuICAgIF07XG59XG5cbnZlck9wZXJhY2lvbi5oZWFkaW5nID0gZnVuY3Rpb24gKCkge1xuICAgIGlmKCEgdGhpcy5vcGVyYWNpb24oKSApIHJldHVybiBudWxsO1xuXG4gICAgcmV0dXJuIFtcbiAgICAgICAgbSgnaDQucHVsbC1sZWZ0Jywge3N0eWxlOidtYXJnaW46MCd9LCBbXG4gICAgICAgICAgICB0aGlzLm9wZXJhY2lvbigpLnRpcG9Eb2N1bWVudG8ubm9tYnJlICsgJyAnLFxuICAgICAgICAgICAgbSgnc3Bhbicse3N0eWxlOidjb2xvcjojYWFhOyBmb250LXNpemU6MC43ZW0nfSwgJyBTZXJpZTogJyksXG4gICAgICAgICAgICBtKCdzcGFuJywgdGhpcy5vcGVyYWNpb24oKS5zZXJpZSksXG4gICAgICAgICAgICBtKCdzcGFuJyx7c3R5bGU6J2NvbG9yOiNhYWE7IGZvbnQtc2l6ZTowLjdlbSd9LCcgIyAnKSxcbiAgICAgICAgICAgIG0oJ3NwYW4nLCB0aGlzLm9wZXJhY2lvbigpLm51bWVybyksXG4gICAgICAgICAgICBtKCdzcGFuJyx7c3R5bGU6J2NvbG9yOiNhYWE7IGZvbnQtc2l6ZTowLjdlbSd9LCcgRmVjaGE6ICcpLFxuICAgICAgICAgICAgbSgnc3BhbicsIG9vci5mZWNoYS5wcmV0dHlGb3JtYXQueWVhcih0aGlzLm9wZXJhY2lvbigpLmZlY2hhKSlcbiAgICAgICAgXSksXG4gICAgXTtcbn1cbiIsIi8qIVxuXHRQYXBhIFBhcnNlXG5cdHY0LjEuMlxuXHRodHRwczovL2dpdGh1Yi5jb20vbWhvbHQvUGFwYVBhcnNlXG4qL1xuKGZ1bmN0aW9uKGdsb2JhbClcbntcblx0XCJ1c2Ugc3RyaWN0XCI7XG5cblx0Y29uc29sZS5sb2coZ2xvYmFsKTtcblxuXHR2YXIgSVNfV09SS0VSID0gIWdsb2JhbC5kb2N1bWVudCAmJiAhIWdsb2JhbC5wb3N0TWVzc2FnZSxcblx0XHRJU19QQVBBX1dPUktFUiA9IElTX1dPUktFUiAmJiAvKFxcP3wmKXBhcGF3b3JrZXIoPXwmfCQpLy50ZXN0KGdsb2JhbC5sb2NhdGlvbi5zZWFyY2gpLFxuXHRcdExPQURFRF9TWU5DID0gZmFsc2UsIEFVVE9fU0NSSVBUX1BBVEg7XG5cdHZhciB3b3JrZXJzID0ge30sIHdvcmtlcklkQ291bnRlciA9IDA7XG5cblx0dmFyIFBhcGEgPSB7fTtcblxuXHRQYXBhLnBhcnNlID0gQ3N2VG9Kc29uO1xuXHRQYXBhLnVucGFyc2UgPSBKc29uVG9Dc3Y7XG5cblx0UGFwYS5SRUNPUkRfU0VQID0gU3RyaW5nLmZyb21DaGFyQ29kZSgzMCk7XG5cdFBhcGEuVU5JVF9TRVAgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKDMxKTtcblx0UGFwYS5CWVRFX09SREVSX01BUksgPSBcIlxcdWZlZmZcIjtcblx0UGFwYS5CQURfREVMSU1JVEVSUyA9IFtcIlxcclwiLCBcIlxcblwiLCBcIlxcXCJcIiwgUGFwYS5CWVRFX09SREVSX01BUktdO1xuXHRQYXBhLldPUktFUlNfU1VQUE9SVEVEID0gIUlTX1dPUktFUiAmJiAhIWdsb2JhbC5Xb3JrZXI7XG5cdFBhcGEuU0NSSVBUX1BBVEggPSBudWxsO1x0Ly8gTXVzdCBiZSBzZXQgYnkgeW91ciBjb2RlIGlmIHlvdSB1c2Ugd29ya2VycyBhbmQgdGhpcyBsaWIgaXMgbG9hZGVkIGFzeW5jaHJvbm91c2x5XG5cblx0Ly8gQ29uZmlndXJhYmxlIGNodW5rIHNpemVzIGZvciBsb2NhbCBhbmQgcmVtb3RlIGZpbGVzLCByZXNwZWN0aXZlbHlcblx0UGFwYS5Mb2NhbENodW5rU2l6ZSA9IDEwMjQgKiAxMDI0ICogMTA7XHQvLyAxMCBNQlxuXHRQYXBhLlJlbW90ZUNodW5rU2l6ZSA9IDEwMjQgKiAxMDI0ICogNTtcdC8vIDUgTUJcblx0UGFwYS5EZWZhdWx0RGVsaW1pdGVyID0gXCIsXCI7XHRcdFx0Ly8gVXNlZCBpZiBub3Qgc3BlY2lmaWVkIGFuZCBkZXRlY3Rpb24gZmFpbHNcblxuXHQvLyBFeHBvc2VkIGZvciB0ZXN0aW5nIGFuZCBkZXZlbG9wbWVudCBvbmx5XG5cdFBhcGEuUGFyc2VyID0gUGFyc2VyO1xuXHRQYXBhLlBhcnNlckhhbmRsZSA9IFBhcnNlckhhbmRsZTtcblx0UGFwYS5OZXR3b3JrU3RyZWFtZXIgPSBOZXR3b3JrU3RyZWFtZXI7XG5cdFBhcGEuRmlsZVN0cmVhbWVyID0gRmlsZVN0cmVhbWVyO1xuXHRQYXBhLlN0cmluZ1N0cmVhbWVyID0gU3RyaW5nU3RyZWFtZXI7XG5cblx0aWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIG1vZHVsZS5leHBvcnRzKVxuXHR7XG5cdFx0Ly8gRXhwb3J0IHRvIE5vZGUuLi5cblx0XHRtb2R1bGUuZXhwb3J0cyA9IFBhcGE7XG5cdH1cblx0ZWxzZSBpZiAoaXNGdW5jdGlvbihnbG9iYWwuZGVmaW5lKSAmJiBnbG9iYWwuZGVmaW5lLmFtZClcblx0e1xuXHRcdC8vIFdpcmV1cCB3aXRoIFJlcXVpcmVKU1xuXHRcdGRlZmluZShmdW5jdGlvbigpIHsgcmV0dXJuIFBhcGE7IH0pO1xuXHR9XG5cdGVsc2Vcblx0e1xuXHRcdC8vIC4uLm9yIGFzIGJyb3dzZXIgZ2xvYmFsXG5cdFx0Z2xvYmFsLlBhcGEgPSBQYXBhO1xuXHR9XG5cblx0aWYgKGdsb2JhbC5qUXVlcnkpXG5cdHtcblx0XHR2YXIgJCA9IGdsb2JhbC5qUXVlcnk7XG5cdFx0JC5mbi5wYXJzZSA9IGZ1bmN0aW9uKG9wdGlvbnMpXG5cdFx0e1xuXHRcdFx0dmFyIGNvbmZpZyA9IG9wdGlvbnMuY29uZmlnIHx8IHt9O1xuXHRcdFx0dmFyIHF1ZXVlID0gW107XG5cblx0XHRcdHRoaXMuZWFjaChmdW5jdGlvbihpZHgpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciBzdXBwb3J0ZWQgPSAkKHRoaXMpLnByb3AoJ3RhZ05hbWUnKS50b1VwcGVyQ2FzZSgpID09IFwiSU5QVVRcIlxuXHRcdFx0XHRcdFx0XHRcdCYmICQodGhpcykuYXR0cigndHlwZScpLnRvTG93ZXJDYXNlKCkgPT0gXCJmaWxlXCJcblx0XHRcdFx0XHRcdFx0XHQmJiBnbG9iYWwuRmlsZVJlYWRlcjtcblxuXHRcdFx0XHRpZiAoIXN1cHBvcnRlZCB8fCAhdGhpcy5maWxlcyB8fCB0aGlzLmZpbGVzLmxlbmd0aCA9PSAwKVxuXHRcdFx0XHRcdHJldHVybiB0cnVlO1x0Ly8gY29udGludWUgdG8gbmV4dCBpbnB1dCBlbGVtZW50XG5cblx0XHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmZpbGVzLmxlbmd0aDsgaSsrKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0cXVldWUucHVzaCh7XG5cdFx0XHRcdFx0XHRmaWxlOiB0aGlzLmZpbGVzW2ldLFxuXHRcdFx0XHRcdFx0aW5wdXRFbGVtOiB0aGlzLFxuXHRcdFx0XHRcdFx0aW5zdGFuY2VDb25maWc6ICQuZXh0ZW5kKHt9LCBjb25maWcpXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHRwYXJzZU5leHRGaWxlKCk7XHQvLyBiZWdpbiBwYXJzaW5nXG5cdFx0XHRyZXR1cm4gdGhpcztcdFx0Ly8gbWFpbnRhaW5zIGNoYWluYWJpbGl0eVxuXG5cblx0XHRcdGZ1bmN0aW9uIHBhcnNlTmV4dEZpbGUoKVxuXHRcdFx0e1xuXHRcdFx0XHRpZiAocXVldWUubGVuZ3RoID09IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoaXNGdW5jdGlvbihvcHRpb25zLmNvbXBsZXRlKSlcblx0XHRcdFx0XHRcdG9wdGlvbnMuY29tcGxldGUoKTtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblxuXHRcdFx0XHR2YXIgZiA9IHF1ZXVlWzBdO1xuXG5cdFx0XHRcdGlmIChpc0Z1bmN0aW9uKG9wdGlvbnMuYmVmb3JlKSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciByZXR1cm5lZCA9IG9wdGlvbnMuYmVmb3JlKGYuZmlsZSwgZi5pbnB1dEVsZW0pO1xuXG5cdFx0XHRcdFx0aWYgKHR5cGVvZiByZXR1cm5lZCA9PT0gJ29iamVjdCcpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0aWYgKHJldHVybmVkLmFjdGlvbiA9PSBcImFib3J0XCIpXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdGVycm9yKFwiQWJvcnRFcnJvclwiLCBmLmZpbGUsIGYuaW5wdXRFbGVtLCByZXR1cm5lZC5yZWFzb24pO1xuXHRcdFx0XHRcdFx0XHRyZXR1cm47XHQvLyBBYm9ydHMgYWxsIHF1ZXVlZCBmaWxlcyBpbW1lZGlhdGVseVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0ZWxzZSBpZiAocmV0dXJuZWQuYWN0aW9uID09IFwic2tpcFwiKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRmaWxlQ29tcGxldGUoKTtcdC8vIHBhcnNlIHRoZSBuZXh0IGZpbGUgaW4gdGhlIHF1ZXVlLCBpZiBhbnlcblx0XHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0ZWxzZSBpZiAodHlwZW9mIHJldHVybmVkLmNvbmZpZyA9PT0gJ29iamVjdCcpXG5cdFx0XHRcdFx0XHRcdGYuaW5zdGFuY2VDb25maWcgPSAkLmV4dGVuZChmLmluc3RhbmNlQ29uZmlnLCByZXR1cm5lZC5jb25maWcpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIGlmIChyZXR1cm5lZCA9PSBcInNraXBcIilcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRmaWxlQ29tcGxldGUoKTtcdC8vIHBhcnNlIHRoZSBuZXh0IGZpbGUgaW4gdGhlIHF1ZXVlLCBpZiBhbnlcblx0XHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBXcmFwIHVwIHRoZSB1c2VyJ3MgY29tcGxldGUgY2FsbGJhY2ssIGlmIGFueSwgc28gdGhhdCBvdXJzIGFsc28gZ2V0cyBleGVjdXRlZFxuXHRcdFx0XHR2YXIgdXNlckNvbXBsZXRlRnVuYyA9IGYuaW5zdGFuY2VDb25maWcuY29tcGxldGU7XG5cdFx0XHRcdGYuaW5zdGFuY2VDb25maWcuY29tcGxldGUgPSBmdW5jdGlvbihyZXN1bHRzKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKGlzRnVuY3Rpb24odXNlckNvbXBsZXRlRnVuYykpXG5cdFx0XHRcdFx0XHR1c2VyQ29tcGxldGVGdW5jKHJlc3VsdHMsIGYuZmlsZSwgZi5pbnB1dEVsZW0pO1xuXHRcdFx0XHRcdGZpbGVDb21wbGV0ZSgpO1xuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdFBhcGEucGFyc2UoZi5maWxlLCBmLmluc3RhbmNlQ29uZmlnKTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gZXJyb3IobmFtZSwgZmlsZSwgZWxlbSwgcmVhc29uKVxuXHRcdFx0e1xuXHRcdFx0XHRpZiAoaXNGdW5jdGlvbihvcHRpb25zLmVycm9yKSlcblx0XHRcdFx0XHRvcHRpb25zLmVycm9yKHtuYW1lOiBuYW1lfSwgZmlsZSwgZWxlbSwgcmVhc29uKTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gZmlsZUNvbXBsZXRlKClcblx0XHRcdHtcblx0XHRcdFx0cXVldWUuc3BsaWNlKDAsIDEpO1xuXHRcdFx0XHRwYXJzZU5leHRGaWxlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblxuXHRpZiAoSVNfUEFQQV9XT1JLRVIpXG5cdHtcblx0XHRnbG9iYWwub25tZXNzYWdlID0gd29ya2VyVGhyZWFkUmVjZWl2ZWRNZXNzYWdlO1xuXHR9XG5cdGVsc2UgaWYgKFBhcGEuV09SS0VSU19TVVBQT1JURUQpXG5cdHtcblx0XHRBVVRPX1NDUklQVF9QQVRIID0gZ2V0U2NyaXB0UGF0aCgpO1xuXG5cdFx0Ly8gQ2hlY2sgaWYgdGhlIHNjcmlwdCB3YXMgbG9hZGVkIHN5bmNocm9ub3VzbHlcblx0XHRpZiAoIWRvY3VtZW50LmJvZHkpXG5cdFx0e1xuXHRcdFx0Ly8gQm9keSBkb2Vzbid0IGV4aXN0IHlldCwgbXVzdCBiZSBzeW5jaHJvbm91c1xuXHRcdFx0TE9BREVEX1NZTkMgPSB0cnVlO1xuXHRcdH1cblx0XHRlbHNlXG5cdFx0e1xuXHRcdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0TE9BREVEX1NZTkMgPSB0cnVlO1xuXHRcdFx0fSwgdHJ1ZSk7XG5cdFx0fVxuXHR9XG5cblxuXG5cblx0ZnVuY3Rpb24gQ3N2VG9Kc29uKF9pbnB1dCwgX2NvbmZpZylcblx0e1xuXHRcdF9jb25maWcgPSBfY29uZmlnIHx8IHt9O1xuXG5cdFx0aWYgKF9jb25maWcud29ya2VyICYmIFBhcGEuV09SS0VSU19TVVBQT1JURUQpXG5cdFx0e1xuXHRcdFx0dmFyIHcgPSBuZXdXb3JrZXIoKTtcblxuXHRcdFx0dy51c2VyU3RlcCA9IF9jb25maWcuc3RlcDtcblx0XHRcdHcudXNlckNodW5rID0gX2NvbmZpZy5jaHVuaztcblx0XHRcdHcudXNlckNvbXBsZXRlID0gX2NvbmZpZy5jb21wbGV0ZTtcblx0XHRcdHcudXNlckVycm9yID0gX2NvbmZpZy5lcnJvcjtcblxuXHRcdFx0X2NvbmZpZy5zdGVwID0gaXNGdW5jdGlvbihfY29uZmlnLnN0ZXApO1xuXHRcdFx0X2NvbmZpZy5jaHVuayA9IGlzRnVuY3Rpb24oX2NvbmZpZy5jaHVuayk7XG5cdFx0XHRfY29uZmlnLmNvbXBsZXRlID0gaXNGdW5jdGlvbihfY29uZmlnLmNvbXBsZXRlKTtcblx0XHRcdF9jb25maWcuZXJyb3IgPSBpc0Z1bmN0aW9uKF9jb25maWcuZXJyb3IpO1xuXHRcdFx0ZGVsZXRlIF9jb25maWcud29ya2VyO1x0Ly8gcHJldmVudCBpbmZpbml0ZSBsb29wXG5cblx0XHRcdHcucG9zdE1lc3NhZ2Uoe1xuXHRcdFx0XHRpbnB1dDogX2lucHV0LFxuXHRcdFx0XHRjb25maWc6IF9jb25maWcsXG5cdFx0XHRcdHdvcmtlcklkOiB3LmlkXG5cdFx0XHR9KTtcblxuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdHZhciBzdHJlYW1lciA9IG51bGw7XG5cdFx0aWYgKHR5cGVvZiBfaW5wdXQgPT09ICdzdHJpbmcnKVxuXHRcdHtcblx0XHRcdGlmIChfY29uZmlnLmRvd25sb2FkKVxuXHRcdFx0XHRzdHJlYW1lciA9IG5ldyBOZXR3b3JrU3RyZWFtZXIoX2NvbmZpZyk7XG5cdFx0XHRlbHNlXG5cdFx0XHRcdHN0cmVhbWVyID0gbmV3IFN0cmluZ1N0cmVhbWVyKF9jb25maWcpO1xuXHRcdH1cblx0XHRlbHNlIGlmICgoZ2xvYmFsLkZpbGUgJiYgX2lucHV0IGluc3RhbmNlb2YgRmlsZSkgfHwgX2lucHV0IGluc3RhbmNlb2YgT2JqZWN0KVx0Ly8gLi4uU2FmYXJpLiAoc2VlIGlzc3VlICMxMDYpXG5cdFx0XHRzdHJlYW1lciA9IG5ldyBGaWxlU3RyZWFtZXIoX2NvbmZpZyk7XG5cblx0XHRyZXR1cm4gc3RyZWFtZXIuc3RyZWFtKF9pbnB1dCk7XG5cdH1cblxuXG5cblxuXG5cblx0ZnVuY3Rpb24gSnNvblRvQ3N2KF9pbnB1dCwgX2NvbmZpZylcblx0e1xuXHRcdHZhciBfb3V0cHV0ID0gXCJcIjtcblx0XHR2YXIgX2ZpZWxkcyA9IFtdO1xuXG5cdFx0Ly8gRGVmYXVsdCBjb25maWd1cmF0aW9uXG5cblx0XHQvKiogd2hldGhlciB0byBzdXJyb3VuZCBldmVyeSBkYXR1bSB3aXRoIHF1b3RlcyAqL1xuXHRcdHZhciBfcXVvdGVzID0gZmFsc2U7XG5cblx0XHQvKiogZGVsaW1pdGluZyBjaGFyYWN0ZXIgKi9cblx0XHR2YXIgX2RlbGltaXRlciA9IFwiLFwiO1xuXG5cdFx0LyoqIG5ld2xpbmUgY2hhcmFjdGVyKHMpICovXG5cdFx0dmFyIF9uZXdsaW5lID0gXCJcXHJcXG5cIjtcblxuXHRcdHVucGFja0NvbmZpZygpO1xuXG5cdFx0aWYgKHR5cGVvZiBfaW5wdXQgPT09ICdzdHJpbmcnKVxuXHRcdFx0X2lucHV0ID0gSlNPTi5wYXJzZShfaW5wdXQpO1xuXG5cdFx0aWYgKF9pbnB1dCBpbnN0YW5jZW9mIEFycmF5KVxuXHRcdHtcblx0XHRcdGlmICghX2lucHV0Lmxlbmd0aCB8fCBfaW5wdXRbMF0gaW5zdGFuY2VvZiBBcnJheSlcblx0XHRcdFx0cmV0dXJuIHNlcmlhbGl6ZShudWxsLCBfaW5wdXQpO1xuXHRcdFx0ZWxzZSBpZiAodHlwZW9mIF9pbnB1dFswXSA9PT0gJ29iamVjdCcpXG5cdFx0XHRcdHJldHVybiBzZXJpYWxpemUob2JqZWN0S2V5cyhfaW5wdXRbMF0pLCBfaW5wdXQpO1xuXHRcdH1cblx0XHRlbHNlIGlmICh0eXBlb2YgX2lucHV0ID09PSAnb2JqZWN0Jylcblx0XHR7XG5cdFx0XHRpZiAodHlwZW9mIF9pbnB1dC5kYXRhID09PSAnc3RyaW5nJylcblx0XHRcdFx0X2lucHV0LmRhdGEgPSBKU09OLnBhcnNlKF9pbnB1dC5kYXRhKTtcblxuXHRcdFx0aWYgKF9pbnB1dC5kYXRhIGluc3RhbmNlb2YgQXJyYXkpXG5cdFx0XHR7XG5cdFx0XHRcdGlmICghX2lucHV0LmZpZWxkcylcblx0XHRcdFx0XHRfaW5wdXQuZmllbGRzID0gX2lucHV0LmRhdGFbMF0gaW5zdGFuY2VvZiBBcnJheVxuXHRcdFx0XHRcdFx0XHRcdFx0PyBfaW5wdXQuZmllbGRzXG5cdFx0XHRcdFx0XHRcdFx0XHQ6IG9iamVjdEtleXMoX2lucHV0LmRhdGFbMF0pO1xuXG5cdFx0XHRcdGlmICghKF9pbnB1dC5kYXRhWzBdIGluc3RhbmNlb2YgQXJyYXkpICYmIHR5cGVvZiBfaW5wdXQuZGF0YVswXSAhPT0gJ29iamVjdCcpXG5cdFx0XHRcdFx0X2lucHV0LmRhdGEgPSBbX2lucHV0LmRhdGFdO1x0Ly8gaGFuZGxlcyBpbnB1dCBsaWtlIFsxLDIsM10gb3IgW1wiYXNkZlwiXVxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gc2VyaWFsaXplKF9pbnB1dC5maWVsZHMgfHwgW10sIF9pbnB1dC5kYXRhIHx8IFtdKTtcblx0XHR9XG5cblx0XHQvLyBEZWZhdWx0IChhbnkgdmFsaWQgcGF0aHMgc2hvdWxkIHJldHVybiBiZWZvcmUgdGhpcylcblx0XHR0aHJvdyBcImV4Y2VwdGlvbjogVW5hYmxlIHRvIHNlcmlhbGl6ZSB1bnJlY29nbml6ZWQgaW5wdXRcIjtcblxuXG5cdFx0ZnVuY3Rpb24gdW5wYWNrQ29uZmlnKClcblx0XHR7XG5cdFx0XHRpZiAodHlwZW9mIF9jb25maWcgIT09ICdvYmplY3QnKVxuXHRcdFx0XHRyZXR1cm47XG5cblx0XHRcdGlmICh0eXBlb2YgX2NvbmZpZy5kZWxpbWl0ZXIgPT09ICdzdHJpbmcnXG5cdFx0XHRcdCYmIF9jb25maWcuZGVsaW1pdGVyLmxlbmd0aCA9PSAxXG5cdFx0XHRcdCYmIFBhcGEuQkFEX0RFTElNSVRFUlMuaW5kZXhPZihfY29uZmlnLmRlbGltaXRlcikgPT0gLTEpXG5cdFx0XHR7XG5cdFx0XHRcdF9kZWxpbWl0ZXIgPSBfY29uZmlnLmRlbGltaXRlcjtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHR5cGVvZiBfY29uZmlnLnF1b3RlcyA9PT0gJ2Jvb2xlYW4nXG5cdFx0XHRcdHx8IF9jb25maWcucXVvdGVzIGluc3RhbmNlb2YgQXJyYXkpXG5cdFx0XHRcdF9xdW90ZXMgPSBfY29uZmlnLnF1b3RlcztcblxuXHRcdFx0aWYgKHR5cGVvZiBfY29uZmlnLm5ld2xpbmUgPT09ICdzdHJpbmcnKVxuXHRcdFx0XHRfbmV3bGluZSA9IF9jb25maWcubmV3bGluZTtcblx0XHR9XG5cblxuXHRcdC8qKiBUdXJucyBhbiBvYmplY3QncyBrZXlzIGludG8gYW4gYXJyYXkgKi9cblx0XHRmdW5jdGlvbiBvYmplY3RLZXlzKG9iailcblx0XHR7XG5cdFx0XHRpZiAodHlwZW9mIG9iaiAhPT0gJ29iamVjdCcpXG5cdFx0XHRcdHJldHVybiBbXTtcblx0XHRcdHZhciBrZXlzID0gW107XG5cdFx0XHRmb3IgKHZhciBrZXkgaW4gb2JqKVxuXHRcdFx0XHRrZXlzLnB1c2goa2V5KTtcblx0XHRcdHJldHVybiBrZXlzO1xuXHRcdH1cblxuXHRcdC8qKiBUaGUgZG91YmxlIGZvciBsb29wIHRoYXQgaXRlcmF0ZXMgdGhlIGRhdGEgYW5kIHdyaXRlcyBvdXQgYSBDU1Ygc3RyaW5nIGluY2x1ZGluZyBoZWFkZXIgcm93ICovXG5cdFx0ZnVuY3Rpb24gc2VyaWFsaXplKGZpZWxkcywgZGF0YSlcblx0XHR7XG5cdFx0XHR2YXIgY3N2ID0gXCJcIjtcblxuXHRcdFx0aWYgKHR5cGVvZiBmaWVsZHMgPT09ICdzdHJpbmcnKVxuXHRcdFx0XHRmaWVsZHMgPSBKU09OLnBhcnNlKGZpZWxkcyk7XG5cdFx0XHRpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKVxuXHRcdFx0XHRkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcblxuXHRcdFx0dmFyIGhhc0hlYWRlciA9IGZpZWxkcyBpbnN0YW5jZW9mIEFycmF5ICYmIGZpZWxkcy5sZW5ndGggPiAwO1xuXHRcdFx0dmFyIGRhdGFLZXllZEJ5RmllbGQgPSAhKGRhdGFbMF0gaW5zdGFuY2VvZiBBcnJheSk7XG5cblx0XHRcdC8vIElmIHRoZXJlIGEgaGVhZGVyIHJvdywgd3JpdGUgaXQgZmlyc3Rcblx0XHRcdGlmIChoYXNIZWFkZXIpXG5cdFx0XHR7XG5cdFx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZmllbGRzLmxlbmd0aDsgaSsrKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKGkgPiAwKVxuXHRcdFx0XHRcdFx0Y3N2ICs9IF9kZWxpbWl0ZXI7XG5cdFx0XHRcdFx0Y3N2ICs9IHNhZmUoZmllbGRzW2ldLCBpKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoZGF0YS5sZW5ndGggPiAwKVxuXHRcdFx0XHRcdGNzdiArPSBfbmV3bGluZTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gVGhlbiB3cml0ZSBvdXQgdGhlIGRhdGFcblx0XHRcdGZvciAodmFyIHJvdyA9IDA7IHJvdyA8IGRhdGEubGVuZ3RoOyByb3crKylcblx0XHRcdHtcblx0XHRcdFx0dmFyIG1heENvbCA9IGhhc0hlYWRlciA/IGZpZWxkcy5sZW5ndGggOiBkYXRhW3Jvd10ubGVuZ3RoO1xuXG5cdFx0XHRcdGZvciAodmFyIGNvbCA9IDA7IGNvbCA8IG1heENvbDsgY29sKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZiAoY29sID4gMClcblx0XHRcdFx0XHRcdGNzdiArPSBfZGVsaW1pdGVyO1xuXHRcdFx0XHRcdHZhciBjb2xJZHggPSBoYXNIZWFkZXIgJiYgZGF0YUtleWVkQnlGaWVsZCA/IGZpZWxkc1tjb2xdIDogY29sO1xuXHRcdFx0XHRcdGNzdiArPSBzYWZlKGRhdGFbcm93XVtjb2xJZHhdLCBjb2wpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYgKHJvdyA8IGRhdGEubGVuZ3RoIC0gMSlcblx0XHRcdFx0XHRjc3YgKz0gX25ld2xpbmU7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBjc3Y7XG5cdFx0fVxuXG5cdFx0LyoqIEVuY2xvc2VzIGEgdmFsdWUgYXJvdW5kIHF1b3RlcyBpZiBuZWVkZWQgKG1ha2VzIGEgdmFsdWUgc2FmZSBmb3IgQ1NWIGluc2VydGlvbikgKi9cblx0XHRmdW5jdGlvbiBzYWZlKHN0ciwgY29sKVxuXHRcdHtcblx0XHRcdGlmICh0eXBlb2Ygc3RyID09PSBcInVuZGVmaW5lZFwiIHx8IHN0ciA9PT0gbnVsbClcblx0XHRcdFx0cmV0dXJuIFwiXCI7XG5cblx0XHRcdHN0ciA9IHN0ci50b1N0cmluZygpLnJlcGxhY2UoL1wiL2csICdcIlwiJyk7XG5cblx0XHRcdHZhciBuZWVkc1F1b3RlcyA9ICh0eXBlb2YgX3F1b3RlcyA9PT0gJ2Jvb2xlYW4nICYmIF9xdW90ZXMpXG5cdFx0XHRcdFx0XHRcdHx8IChfcXVvdGVzIGluc3RhbmNlb2YgQXJyYXkgJiYgX3F1b3Rlc1tjb2xdKVxuXHRcdFx0XHRcdFx0XHR8fCBoYXNBbnkoc3RyLCBQYXBhLkJBRF9ERUxJTUlURVJTKVxuXHRcdFx0XHRcdFx0XHR8fCBzdHIuaW5kZXhPZihfZGVsaW1pdGVyKSA+IC0xXG5cdFx0XHRcdFx0XHRcdHx8IHN0ci5jaGFyQXQoMCkgPT0gJyAnXG5cdFx0XHRcdFx0XHRcdHx8IHN0ci5jaGFyQXQoc3RyLmxlbmd0aCAtIDEpID09ICcgJztcblxuXHRcdFx0cmV0dXJuIG5lZWRzUXVvdGVzID8gJ1wiJyArIHN0ciArICdcIicgOiBzdHI7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gaGFzQW55KHN0ciwgc3Vic3RyaW5ncylcblx0XHR7XG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHN1YnN0cmluZ3MubGVuZ3RoOyBpKyspXG5cdFx0XHRcdGlmIChzdHIuaW5kZXhPZihzdWJzdHJpbmdzW2ldKSA+IC0xKVxuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0fVxuXG5cdC8qKiBDaHVua1N0cmVhbWVyIGlzIHRoZSBiYXNlIHByb3RvdHlwZSBmb3IgdmFyaW91cyBzdHJlYW1lciBpbXBsZW1lbnRhdGlvbnMuICovXG5cdGZ1bmN0aW9uIENodW5rU3RyZWFtZXIoY29uZmlnKVxuXHR7XG5cdFx0dGhpcy5faGFuZGxlID0gbnVsbDtcblx0XHR0aGlzLl9wYXVzZWQgPSBmYWxzZTtcblx0XHR0aGlzLl9maW5pc2hlZCA9IGZhbHNlO1xuXHRcdHRoaXMuX2lucHV0ID0gbnVsbDtcblx0XHR0aGlzLl9iYXNlSW5kZXggPSAwO1xuXHRcdHRoaXMuX3BhcnRpYWxMaW5lID0gXCJcIjtcblx0XHR0aGlzLl9yb3dDb3VudCA9IDA7XG5cdFx0dGhpcy5fc3RhcnQgPSAwO1xuXHRcdHRoaXMuX25leHRDaHVuayA9IG51bGw7XG5cdFx0dGhpcy5pc0ZpcnN0Q2h1bmsgPSB0cnVlO1xuXHRcdHRoaXMuX2NvbXBsZXRlUmVzdWx0cyA9IHtcblx0XHRcdGRhdGE6IFtdLFxuXHRcdFx0ZXJyb3JzOiBbXSxcblx0XHRcdG1ldGE6IHt9XG5cdFx0fTtcblx0XHRyZXBsYWNlQ29uZmlnLmNhbGwodGhpcywgY29uZmlnKTtcblxuXHRcdHRoaXMucGFyc2VDaHVuayA9IGZ1bmN0aW9uKGNodW5rKVxuXHRcdHtcblx0XHRcdC8vIEZpcnN0IGNodW5rIHByZS1wcm9jZXNzaW5nXG5cdFx0XHRpZiAodGhpcy5pc0ZpcnN0Q2h1bmsgJiYgaXNGdW5jdGlvbih0aGlzLl9jb25maWcuYmVmb3JlRmlyc3RDaHVuaykpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciBtb2RpZmllZENodW5rID0gdGhpcy5fY29uZmlnLmJlZm9yZUZpcnN0Q2h1bmsoY2h1bmspO1xuXHRcdFx0XHRpZiAobW9kaWZpZWRDaHVuayAhPT0gdW5kZWZpbmVkKVxuXHRcdFx0XHRcdGNodW5rID0gbW9kaWZpZWRDaHVuaztcblx0XHRcdH1cblx0XHRcdHRoaXMuaXNGaXJzdENodW5rID0gZmFsc2U7XG5cblx0XHRcdC8vIFJlam9pbiB0aGUgbGluZSB3ZSBsaWtlbHkganVzdCBzcGxpdCBpbiB0d28gYnkgY2h1bmtpbmcgdGhlIGZpbGVcblx0XHRcdHZhciBhZ2dyZWdhdGUgPSB0aGlzLl9wYXJ0aWFsTGluZSArIGNodW5rO1xuXHRcdFx0dGhpcy5fcGFydGlhbExpbmUgPSBcIlwiO1xuXG5cdFx0XHR2YXIgcmVzdWx0cyA9IHRoaXMuX2hhbmRsZS5wYXJzZShhZ2dyZWdhdGUsIHRoaXMuX2Jhc2VJbmRleCwgIXRoaXMuX2ZpbmlzaGVkKTtcblx0XHRcdFxuXHRcdFx0aWYgKHRoaXMuX2hhbmRsZS5wYXVzZWQoKSB8fCB0aGlzLl9oYW5kbGUuYWJvcnRlZCgpKVxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcblx0XHRcdHZhciBsYXN0SW5kZXggPSByZXN1bHRzLm1ldGEuY3Vyc29yO1xuXHRcdFx0XG5cdFx0XHRpZiAoIXRoaXMuX2ZpbmlzaGVkKVxuXHRcdFx0e1xuXHRcdFx0XHR0aGlzLl9wYXJ0aWFsTGluZSA9IGFnZ3JlZ2F0ZS5zdWJzdHJpbmcobGFzdEluZGV4IC0gdGhpcy5fYmFzZUluZGV4KTtcblx0XHRcdFx0dGhpcy5fYmFzZUluZGV4ID0gbGFzdEluZGV4O1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAocmVzdWx0cyAmJiByZXN1bHRzLmRhdGEpXG5cdFx0XHRcdHRoaXMuX3Jvd0NvdW50ICs9IHJlc3VsdHMuZGF0YS5sZW5ndGg7XG5cblx0XHRcdHZhciBmaW5pc2hlZEluY2x1ZGluZ1ByZXZpZXcgPSB0aGlzLl9maW5pc2hlZCB8fCAodGhpcy5fY29uZmlnLnByZXZpZXcgJiYgdGhpcy5fcm93Q291bnQgPj0gdGhpcy5fY29uZmlnLnByZXZpZXcpO1xuXG5cdFx0XHRpZiAoSVNfUEFQQV9XT1JLRVIpXG5cdFx0XHR7XG5cdFx0XHRcdGdsb2JhbC5wb3N0TWVzc2FnZSh7XG5cdFx0XHRcdFx0cmVzdWx0czogcmVzdWx0cyxcblx0XHRcdFx0XHR3b3JrZXJJZDogUGFwYS5XT1JLRVJfSUQsXG5cdFx0XHRcdFx0ZmluaXNoZWQ6IGZpbmlzaGVkSW5jbHVkaW5nUHJldmlld1xuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKGlzRnVuY3Rpb24odGhpcy5fY29uZmlnLmNodW5rKSlcblx0XHRcdHtcblx0XHRcdFx0dGhpcy5fY29uZmlnLmNodW5rKHJlc3VsdHMsIHRoaXMuX2hhbmRsZSk7XG5cdFx0XHRcdGlmICh0aGlzLl9wYXVzZWQpXG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRyZXN1bHRzID0gdW5kZWZpbmVkO1xuXHRcdFx0XHR0aGlzLl9jb21wbGV0ZVJlc3VsdHMgPSB1bmRlZmluZWQ7XG5cdFx0XHR9XG5cblx0XHRcdGlmICghdGhpcy5fY29uZmlnLnN0ZXAgJiYgIXRoaXMuX2NvbmZpZy5jaHVuaykge1xuXHRcdFx0XHR0aGlzLl9jb21wbGV0ZVJlc3VsdHMuZGF0YSA9IHRoaXMuX2NvbXBsZXRlUmVzdWx0cy5kYXRhLmNvbmNhdChyZXN1bHRzLmRhdGEpO1xuXHRcdFx0XHR0aGlzLl9jb21wbGV0ZVJlc3VsdHMuZXJyb3JzID0gdGhpcy5fY29tcGxldGVSZXN1bHRzLmVycm9ycy5jb25jYXQocmVzdWx0cy5lcnJvcnMpO1xuXHRcdFx0XHR0aGlzLl9jb21wbGV0ZVJlc3VsdHMubWV0YSA9IHJlc3VsdHMubWV0YTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKGZpbmlzaGVkSW5jbHVkaW5nUHJldmlldyAmJiBpc0Z1bmN0aW9uKHRoaXMuX2NvbmZpZy5jb21wbGV0ZSkgJiYgKCFyZXN1bHRzIHx8ICFyZXN1bHRzLm1ldGEuYWJvcnRlZCkpXG5cdFx0XHRcdHRoaXMuX2NvbmZpZy5jb21wbGV0ZSh0aGlzLl9jb21wbGV0ZVJlc3VsdHMpO1xuXG5cdFx0XHRpZiAoIWZpbmlzaGVkSW5jbHVkaW5nUHJldmlldyAmJiAoIXJlc3VsdHMgfHwgIXJlc3VsdHMubWV0YS5wYXVzZWQpKVxuXHRcdFx0XHR0aGlzLl9uZXh0Q2h1bmsoKTtcblxuXHRcdFx0cmV0dXJuIHJlc3VsdHM7XG5cdFx0fTtcblxuXHRcdHRoaXMuX3NlbmRFcnJvciA9IGZ1bmN0aW9uKGVycm9yKVxuXHRcdHtcblx0XHRcdGlmIChpc0Z1bmN0aW9uKHRoaXMuX2NvbmZpZy5lcnJvcikpXG5cdFx0XHRcdHRoaXMuX2NvbmZpZy5lcnJvcihlcnJvcik7XG5cdFx0XHRlbHNlIGlmIChJU19QQVBBX1dPUktFUiAmJiB0aGlzLl9jb25maWcuZXJyb3IpXG5cdFx0XHR7XG5cdFx0XHRcdGdsb2JhbC5wb3N0TWVzc2FnZSh7XG5cdFx0XHRcdFx0d29ya2VySWQ6IFBhcGEuV09SS0VSX0lELFxuXHRcdFx0XHRcdGVycm9yOiBlcnJvcixcblx0XHRcdFx0XHRmaW5pc2hlZDogZmFsc2Vcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0fTtcblxuXHRcdGZ1bmN0aW9uIHJlcGxhY2VDb25maWcoY29uZmlnKVxuXHRcdHtcblx0XHRcdC8vIERlZXAtY29weSB0aGUgY29uZmlnIHNvIHdlIGNhbiBlZGl0IGl0XG5cdFx0XHR2YXIgY29uZmlnQ29weSA9IGNvcHkoY29uZmlnKTtcblx0XHRcdGNvbmZpZ0NvcHkuY2h1bmtTaXplID0gcGFyc2VJbnQoY29uZmlnQ29weS5jaHVua1NpemUpO1x0Ly8gcGFyc2VJbnQgVkVSWSBpbXBvcnRhbnQgc28gd2UgZG9uJ3QgY29uY2F0ZW5hdGUgc3RyaW5ncyFcblx0XHRcdGlmICghY29uZmlnLnN0ZXAgJiYgIWNvbmZpZy5jaHVuaylcblx0XHRcdFx0Y29uZmlnQ29weS5jaHVua1NpemUgPSBudWxsOyAgLy8gZGlzYWJsZSBSYW5nZSBoZWFkZXIgaWYgbm90IHN0cmVhbWluZzsgYmFkIHZhbHVlcyBicmVhayBJSVMgLSBzZWUgaXNzdWUgIzE5NlxuXHRcdFx0dGhpcy5faGFuZGxlID0gbmV3IFBhcnNlckhhbmRsZShjb25maWdDb3B5KTtcblx0XHRcdHRoaXMuX2hhbmRsZS5zdHJlYW1lciA9IHRoaXM7XG5cdFx0XHR0aGlzLl9jb25maWcgPSBjb25maWdDb3B5O1x0Ly8gcGVyc2lzdCB0aGUgY29weSB0byB0aGUgY2FsbGVyXG5cdFx0fVxuXHR9XG5cblxuXHRmdW5jdGlvbiBOZXR3b3JrU3RyZWFtZXIoY29uZmlnKVxuXHR7XG5cdFx0Y29uZmlnID0gY29uZmlnIHx8IHt9O1xuXHRcdGlmICghY29uZmlnLmNodW5rU2l6ZSlcblx0XHRcdGNvbmZpZy5jaHVua1NpemUgPSBQYXBhLlJlbW90ZUNodW5rU2l6ZTtcblx0XHRDaHVua1N0cmVhbWVyLmNhbGwodGhpcywgY29uZmlnKTtcblxuXHRcdHZhciB4aHI7XG5cblx0XHRpZiAoSVNfV09SS0VSKVxuXHRcdHtcblx0XHRcdHRoaXMuX25leHRDaHVuayA9IGZ1bmN0aW9uKClcblx0XHRcdHtcblx0XHRcdFx0dGhpcy5fcmVhZENodW5rKCk7XG5cdFx0XHRcdHRoaXMuX2NodW5rTG9hZGVkKCk7XG5cdFx0XHR9O1xuXHRcdH1cblx0XHRlbHNlXG5cdFx0e1xuXHRcdFx0dGhpcy5fbmV4dENodW5rID0gZnVuY3Rpb24oKVxuXHRcdFx0e1xuXHRcdFx0XHR0aGlzLl9yZWFkQ2h1bmsoKTtcblx0XHRcdH07XG5cdFx0fVxuXG5cdFx0dGhpcy5zdHJlYW0gPSBmdW5jdGlvbih1cmwpXG5cdFx0e1xuXHRcdFx0dGhpcy5faW5wdXQgPSB1cmw7XG5cdFx0XHR0aGlzLl9uZXh0Q2h1bmsoKTtcdC8vIFN0YXJ0cyBzdHJlYW1pbmdcblx0XHR9O1xuXG5cdFx0dGhpcy5fcmVhZENodW5rID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdGlmICh0aGlzLl9maW5pc2hlZClcblx0XHRcdHtcblx0XHRcdFx0dGhpcy5fY2h1bmtMb2FkZWQoKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHR4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblx0XHRcdFxuXHRcdFx0aWYgKCFJU19XT1JLRVIpXG5cdFx0XHR7XG5cdFx0XHRcdHhoci5vbmxvYWQgPSBiaW5kRnVuY3Rpb24odGhpcy5fY2h1bmtMb2FkZWQsIHRoaXMpO1xuXHRcdFx0XHR4aHIub25lcnJvciA9IGJpbmRGdW5jdGlvbih0aGlzLl9jaHVua0Vycm9yLCB0aGlzKTtcblx0XHRcdH1cblxuXHRcdFx0eGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5faW5wdXQsICFJU19XT1JLRVIpO1xuXHRcdFx0XG5cdFx0XHRpZiAodGhpcy5fY29uZmlnLmNodW5rU2l6ZSlcblx0XHRcdHtcblx0XHRcdFx0dmFyIGVuZCA9IHRoaXMuX3N0YXJ0ICsgdGhpcy5fY29uZmlnLmNodW5rU2l6ZSAtIDE7XHQvLyBtaW51cyBvbmUgYmVjYXVzZSBieXRlIHJhbmdlIGlzIGluY2x1c2l2ZVxuXHRcdFx0XHR4aHIuc2V0UmVxdWVzdEhlYWRlcihcIlJhbmdlXCIsIFwiYnl0ZXM9XCIrdGhpcy5fc3RhcnQrXCItXCIrZW5kKTtcblx0XHRcdFx0eGhyLnNldFJlcXVlc3RIZWFkZXIoXCJJZi1Ob25lLU1hdGNoXCIsIFwid2Via2l0LW5vLWNhY2hlXCIpOyAvLyBodHRwczovL2J1Z3Mud2Via2l0Lm9yZy9zaG93X2J1Zy5jZ2k/aWQ9ODI2NzJcblx0XHRcdH1cblxuXHRcdFx0dHJ5IHtcblx0XHRcdFx0eGhyLnNlbmQoKTtcblx0XHRcdH1cblx0XHRcdGNhdGNoIChlcnIpIHtcblx0XHRcdFx0dGhpcy5fY2h1bmtFcnJvcihlcnIubWVzc2FnZSk7XG5cdFx0XHR9XG5cblx0XHRcdGlmIChJU19XT1JLRVIgJiYgeGhyLnN0YXR1cyA9PSAwKVxuXHRcdFx0XHR0aGlzLl9jaHVua0Vycm9yKCk7XG5cdFx0XHRlbHNlXG5cdFx0XHRcdHRoaXMuX3N0YXJ0ICs9IHRoaXMuX2NvbmZpZy5jaHVua1NpemU7XG5cdFx0fVxuXG5cdFx0dGhpcy5fY2h1bmtMb2FkZWQgPSBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0aWYgKHhoci5yZWFkeVN0YXRlICE9IDQpXG5cdFx0XHRcdHJldHVybjtcblxuXHRcdFx0aWYgKHhoci5zdGF0dXMgPCAyMDAgfHwgeGhyLnN0YXR1cyA+PSA0MDApXG5cdFx0XHR7XG5cdFx0XHRcdHRoaXMuX2NodW5rRXJyb3IoKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLl9maW5pc2hlZCA9ICF0aGlzLl9jb25maWcuY2h1bmtTaXplIHx8IHRoaXMuX3N0YXJ0ID4gZ2V0RmlsZVNpemUoeGhyKTtcblx0XHRcdHRoaXMucGFyc2VDaHVuayh4aHIucmVzcG9uc2VUZXh0KTtcblx0XHR9XG5cblx0XHR0aGlzLl9jaHVua0Vycm9yID0gZnVuY3Rpb24oZXJyb3JNZXNzYWdlKVxuXHRcdHtcblx0XHRcdHZhciBlcnJvclRleHQgPSB4aHIuc3RhdHVzVGV4dCB8fCBlcnJvck1lc3NhZ2U7XG5cdFx0XHR0aGlzLl9zZW5kRXJyb3IoZXJyb3JUZXh0KTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRGaWxlU2l6ZSh4aHIpXG5cdFx0e1xuXHRcdFx0dmFyIGNvbnRlbnRSYW5nZSA9IHhoci5nZXRSZXNwb25zZUhlYWRlcihcIkNvbnRlbnQtUmFuZ2VcIik7XG5cdFx0XHRyZXR1cm4gcGFyc2VJbnQoY29udGVudFJhbmdlLnN1YnN0cihjb250ZW50UmFuZ2UubGFzdEluZGV4T2YoXCIvXCIpICsgMSkpO1xuXHRcdH1cblx0fVxuXHROZXR3b3JrU3RyZWFtZXIucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShDaHVua1N0cmVhbWVyLnByb3RvdHlwZSk7XG5cdE5ldHdvcmtTdHJlYW1lci5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBOZXR3b3JrU3RyZWFtZXI7XG5cblxuXHRmdW5jdGlvbiBGaWxlU3RyZWFtZXIoY29uZmlnKVxuXHR7XG5cdFx0Y29uZmlnID0gY29uZmlnIHx8IHt9O1xuXHRcdGlmICghY29uZmlnLmNodW5rU2l6ZSlcblx0XHRcdGNvbmZpZy5jaHVua1NpemUgPSBQYXBhLkxvY2FsQ2h1bmtTaXplO1xuXHRcdENodW5rU3RyZWFtZXIuY2FsbCh0aGlzLCBjb25maWcpO1xuXG5cdFx0dmFyIHJlYWRlciwgc2xpY2U7XG5cblx0XHQvLyBGaWxlUmVhZGVyIGlzIGJldHRlciB0aGFuIEZpbGVSZWFkZXJTeW5jIChldmVuIGluIHdvcmtlcikgLSBzZWUgaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3EvMjQ3MDg2NDkvMTA0ODg2MlxuXHRcdC8vIEJ1dCBGaXJlZm94IGlzIGEgcGlsbCwgdG9vIC0gc2VlIGlzc3VlICM3NjogaHR0cHM6Ly9naXRodWIuY29tL21ob2x0L1BhcGFQYXJzZS9pc3N1ZXMvNzZcblx0XHR2YXIgdXNpbmdBc3luY1JlYWRlciA9IHR5cGVvZiBGaWxlUmVhZGVyICE9PSAndW5kZWZpbmVkJztcdC8vIFNhZmFyaSBkb2Vzbid0IGNvbnNpZGVyIGl0IGEgZnVuY3Rpb24gLSBzZWUgaXNzdWUgIzEwNVxuXG5cdFx0dGhpcy5zdHJlYW0gPSBmdW5jdGlvbihmaWxlKVxuXHRcdHtcblx0XHRcdHRoaXMuX2lucHV0ID0gZmlsZTtcblx0XHRcdHNsaWNlID0gZmlsZS5zbGljZSB8fCBmaWxlLndlYmtpdFNsaWNlIHx8IGZpbGUubW96U2xpY2U7XG5cblx0XHRcdGlmICh1c2luZ0FzeW5jUmVhZGVyKVxuXHRcdFx0e1xuXHRcdFx0XHRyZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1x0XHQvLyBQcmVmZXJyZWQgbWV0aG9kIG9mIHJlYWRpbmcgZmlsZXMsIGV2ZW4gaW4gd29ya2Vyc1xuXHRcdFx0XHRyZWFkZXIub25sb2FkID0gYmluZEZ1bmN0aW9uKHRoaXMuX2NodW5rTG9hZGVkLCB0aGlzKTtcblx0XHRcdFx0cmVhZGVyLm9uZXJyb3IgPSBiaW5kRnVuY3Rpb24odGhpcy5fY2h1bmtFcnJvciwgdGhpcyk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlXG5cdFx0XHRcdHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyU3luYygpO1x0Ly8gSGFjayBmb3IgcnVubmluZyBpbiBhIHdlYiB3b3JrZXIgaW4gRmlyZWZveFxuXG5cdFx0XHR0aGlzLl9uZXh0Q2h1bmsoKTtcdC8vIFN0YXJ0cyBzdHJlYW1pbmdcblx0XHR9O1xuXG5cdFx0dGhpcy5fbmV4dENodW5rID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdGlmICghdGhpcy5fZmluaXNoZWQgJiYgKCF0aGlzLl9jb25maWcucHJldmlldyB8fCB0aGlzLl9yb3dDb3VudCA8IHRoaXMuX2NvbmZpZy5wcmV2aWV3KSlcblx0XHRcdFx0dGhpcy5fcmVhZENodW5rKCk7XG5cdFx0fVxuXG5cdFx0dGhpcy5fcmVhZENodW5rID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBpbnB1dCA9IHRoaXMuX2lucHV0O1xuXHRcdFx0aWYgKHRoaXMuX2NvbmZpZy5jaHVua1NpemUpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciBlbmQgPSBNYXRoLm1pbih0aGlzLl9zdGFydCArIHRoaXMuX2NvbmZpZy5jaHVua1NpemUsIHRoaXMuX2lucHV0LnNpemUpO1xuXHRcdFx0XHRpbnB1dCA9IHNsaWNlLmNhbGwoaW5wdXQsIHRoaXMuX3N0YXJ0LCBlbmQpO1xuXHRcdFx0fVxuXHRcdFx0dmFyIHR4dCA9IHJlYWRlci5yZWFkQXNUZXh0KGlucHV0LCB0aGlzLl9jb25maWcuZW5jb2RpbmcpO1xuXHRcdFx0aWYgKCF1c2luZ0FzeW5jUmVhZGVyKVxuXHRcdFx0XHR0aGlzLl9jaHVua0xvYWRlZCh7IHRhcmdldDogeyByZXN1bHQ6IHR4dCB9IH0pO1x0Ly8gbWltaWMgdGhlIGFzeW5jIHNpZ25hdHVyZVxuXHRcdH1cblxuXHRcdHRoaXMuX2NodW5rTG9hZGVkID0gZnVuY3Rpb24oZXZlbnQpXG5cdFx0e1xuXHRcdFx0Ly8gVmVyeSBpbXBvcnRhbnQgdG8gaW5jcmVtZW50IHN0YXJ0IGVhY2ggdGltZSBiZWZvcmUgaGFuZGxpbmcgcmVzdWx0c1xuXHRcdFx0dGhpcy5fc3RhcnQgKz0gdGhpcy5fY29uZmlnLmNodW5rU2l6ZTtcblx0XHRcdHRoaXMuX2ZpbmlzaGVkID0gIXRoaXMuX2NvbmZpZy5jaHVua1NpemUgfHwgdGhpcy5fc3RhcnQgPj0gdGhpcy5faW5wdXQuc2l6ZTtcblx0XHRcdHRoaXMucGFyc2VDaHVuayhldmVudC50YXJnZXQucmVzdWx0KTtcblx0XHR9XG5cblx0XHR0aGlzLl9jaHVua0Vycm9yID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHRoaXMuX3NlbmRFcnJvcihyZWFkZXIuZXJyb3IpO1xuXHRcdH1cblxuXHR9XG5cdEZpbGVTdHJlYW1lci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKENodW5rU3RyZWFtZXIucHJvdG90eXBlKTtcblx0RmlsZVN0cmVhbWVyLnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IEZpbGVTdHJlYW1lcjtcblxuXG5cdGZ1bmN0aW9uIFN0cmluZ1N0cmVhbWVyKGNvbmZpZylcblx0e1xuXHRcdGNvbmZpZyA9IGNvbmZpZyB8fCB7fTtcblx0XHRDaHVua1N0cmVhbWVyLmNhbGwodGhpcywgY29uZmlnKTtcblxuXHRcdHZhciBzdHJpbmc7XG5cdFx0dmFyIHJlbWFpbmluZztcblx0XHR0aGlzLnN0cmVhbSA9IGZ1bmN0aW9uKHMpXG5cdFx0e1xuXHRcdFx0c3RyaW5nID0gcztcblx0XHRcdHJlbWFpbmluZyA9IHM7XG5cdFx0XHRyZXR1cm4gdGhpcy5fbmV4dENodW5rKCk7XG5cdFx0fVxuXHRcdHRoaXMuX25leHRDaHVuayA9IGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRpZiAodGhpcy5fZmluaXNoZWQpIHJldHVybjtcblx0XHRcdHZhciBzaXplID0gdGhpcy5fY29uZmlnLmNodW5rU2l6ZTtcblx0XHRcdHZhciBjaHVuayA9IHNpemUgPyByZW1haW5pbmcuc3Vic3RyKDAsIHNpemUpIDogcmVtYWluaW5nO1xuXHRcdFx0cmVtYWluaW5nID0gc2l6ZSA/IHJlbWFpbmluZy5zdWJzdHIoc2l6ZSkgOiAnJztcblx0XHRcdHRoaXMuX2ZpbmlzaGVkID0gIXJlbWFpbmluZztcblx0XHRcdHJldHVybiB0aGlzLnBhcnNlQ2h1bmsoY2h1bmspO1xuXHRcdH1cblx0fVxuXHRTdHJpbmdTdHJlYW1lci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKFN0cmluZ1N0cmVhbWVyLnByb3RvdHlwZSk7XG5cdFN0cmluZ1N0cmVhbWVyLnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IFN0cmluZ1N0cmVhbWVyO1xuXG5cblxuXHQvLyBVc2Ugb25lIFBhcnNlckhhbmRsZSBwZXIgZW50aXJlIENTViBmaWxlIG9yIHN0cmluZ1xuXHRmdW5jdGlvbiBQYXJzZXJIYW5kbGUoX2NvbmZpZylcblx0e1xuXHRcdC8vIE9uZSBnb2FsIGlzIHRvIG1pbmltaXplIHRoZSB1c2Ugb2YgcmVndWxhciBleHByZXNzaW9ucy4uLlxuXHRcdHZhciBGTE9BVCA9IC9eXFxzKi0/KFxcZCpcXC4/XFxkK3xcXGQrXFwuP1xcZCopKGVbLStdP1xcZCspP1xccyokL2k7XG5cblx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0dmFyIF9zdGVwQ291bnRlciA9IDA7XHQvLyBOdW1iZXIgb2YgdGltZXMgc3RlcCB3YXMgY2FsbGVkIChudW1iZXIgb2Ygcm93cyBwYXJzZWQpXG5cdFx0dmFyIF9pbnB1dDtcdFx0XHRcdC8vIFRoZSBpbnB1dCBiZWluZyBwYXJzZWRcblx0XHR2YXIgX3BhcnNlcjtcdFx0XHQvLyBUaGUgY29yZSBwYXJzZXIgYmVpbmcgdXNlZFxuXHRcdHZhciBfcGF1c2VkID0gZmFsc2U7XHQvLyBXaGV0aGVyIHdlIGFyZSBwYXVzZWQgb3Igbm90XG5cdFx0dmFyIF9hYm9ydGVkID0gZmFsc2U7ICAgLy8gV2hldGhlciB0aGUgcGFyc2VyIGhhcyBhYm9ydGVkIG9yIG5vdFxuXHRcdHZhciBfZGVsaW1pdGVyRXJyb3I7XHQvLyBUZW1wb3Jhcnkgc3RhdGUgYmV0d2VlbiBkZWxpbWl0ZXIgZGV0ZWN0aW9uIGFuZCBwcm9jZXNzaW5nIHJlc3VsdHNcblx0XHR2YXIgX2ZpZWxkcyA9IFtdO1x0XHQvLyBGaWVsZHMgYXJlIGZyb20gdGhlIGhlYWRlciByb3cgb2YgdGhlIGlucHV0LCBpZiB0aGVyZSBpcyBvbmVcblx0XHR2YXIgX3Jlc3VsdHMgPSB7XHRcdC8vIFRoZSBsYXN0IHJlc3VsdHMgcmV0dXJuZWQgZnJvbSB0aGUgcGFyc2VyXG5cdFx0XHRkYXRhOiBbXSxcblx0XHRcdGVycm9yczogW10sXG5cdFx0XHRtZXRhOiB7fVxuXHRcdH07XG5cblx0XHRpZiAoaXNGdW5jdGlvbihfY29uZmlnLnN0ZXApKVxuXHRcdHtcblx0XHRcdHZhciB1c2VyU3RlcCA9IF9jb25maWcuc3RlcDtcblx0XHRcdF9jb25maWcuc3RlcCA9IGZ1bmN0aW9uKHJlc3VsdHMpXG5cdFx0XHR7XG5cdFx0XHRcdF9yZXN1bHRzID0gcmVzdWx0cztcblxuXHRcdFx0XHRpZiAobmVlZHNIZWFkZXJSb3coKSlcblx0XHRcdFx0XHRwcm9jZXNzUmVzdWx0cygpO1xuXHRcdFx0XHRlbHNlXHQvLyBvbmx5IGNhbGwgdXNlcidzIHN0ZXAgZnVuY3Rpb24gYWZ0ZXIgaGVhZGVyIHJvd1xuXHRcdFx0XHR7XG5cdFx0XHRcdFx0cHJvY2Vzc1Jlc3VsdHMoKTtcblxuXHRcdFx0XHRcdC8vIEl0J3MgcG9zc2JpbGUgdGhhdCB0aGlzIGxpbmUgd2FzIGVtcHR5IGFuZCB0aGVyZSdzIG5vIHJvdyBoZXJlIGFmdGVyIGFsbFxuXHRcdFx0XHRcdGlmIChfcmVzdWx0cy5kYXRhLmxlbmd0aCA9PSAwKVxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXG5cdFx0XHRcdFx0X3N0ZXBDb3VudGVyICs9IHJlc3VsdHMuZGF0YS5sZW5ndGg7XG5cdFx0XHRcdFx0aWYgKF9jb25maWcucHJldmlldyAmJiBfc3RlcENvdW50ZXIgPiBfY29uZmlnLnByZXZpZXcpXG5cdFx0XHRcdFx0XHRfcGFyc2VyLmFib3J0KCk7XG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0dXNlclN0ZXAoX3Jlc3VsdHMsIHNlbGYpO1xuXHRcdFx0XHR9XG5cdFx0XHR9O1xuXHRcdH1cblxuXHRcdC8qKlxuXHRcdCAqIFBhcnNlcyBpbnB1dC4gTW9zdCB1c2VycyB3b24ndCBuZWVkLCBhbmQgc2hvdWxkbid0IG1lc3Mgd2l0aCwgdGhlIGJhc2VJbmRleFxuXHRcdCAqIGFuZCBpZ25vcmVMYXN0Um93IHBhcmFtZXRlcnMuIFRoZXkgYXJlIHVzZWQgYnkgc3RyZWFtZXJzICh3cmFwcGVyIGZ1bmN0aW9ucylcblx0XHQgKiB3aGVuIGFuIGlucHV0IGNvbWVzIGluIG11bHRpcGxlIGNodW5rcywgbGlrZSBmcm9tIGEgZmlsZS5cblx0XHQgKi9cblx0XHR0aGlzLnBhcnNlID0gZnVuY3Rpb24oaW5wdXQsIGJhc2VJbmRleCwgaWdub3JlTGFzdFJvdylcblx0XHR7XG5cdFx0XHRpZiAoIV9jb25maWcubmV3bGluZSlcblx0XHRcdFx0X2NvbmZpZy5uZXdsaW5lID0gZ3Vlc3NMaW5lRW5kaW5ncyhpbnB1dCk7XG5cblx0XHRcdF9kZWxpbWl0ZXJFcnJvciA9IGZhbHNlO1xuXHRcdFx0aWYgKCFfY29uZmlnLmRlbGltaXRlcilcblx0XHRcdHtcblx0XHRcdFx0dmFyIGRlbGltR3Vlc3MgPSBndWVzc0RlbGltaXRlcihpbnB1dCk7XG5cdFx0XHRcdGlmIChkZWxpbUd1ZXNzLnN1Y2Nlc3NmdWwpXG5cdFx0XHRcdFx0X2NvbmZpZy5kZWxpbWl0ZXIgPSBkZWxpbUd1ZXNzLmJlc3REZWxpbWl0ZXI7XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdF9kZWxpbWl0ZXJFcnJvciA9IHRydWU7XHQvLyBhZGQgZXJyb3IgYWZ0ZXIgcGFyc2luZyAob3RoZXJ3aXNlIGl0IHdvdWxkIGJlIG92ZXJ3cml0dGVuKVxuXHRcdFx0XHRcdF9jb25maWcuZGVsaW1pdGVyID0gUGFwYS5EZWZhdWx0RGVsaW1pdGVyO1xuXHRcdFx0XHR9XG5cdFx0XHRcdF9yZXN1bHRzLm1ldGEuZGVsaW1pdGVyID0gX2NvbmZpZy5kZWxpbWl0ZXI7XG5cdFx0XHR9XG5cblx0XHRcdHZhciBwYXJzZXJDb25maWcgPSBjb3B5KF9jb25maWcpO1xuXHRcdFx0aWYgKF9jb25maWcucHJldmlldyAmJiBfY29uZmlnLmhlYWRlcilcblx0XHRcdFx0cGFyc2VyQ29uZmlnLnByZXZpZXcrKztcdC8vIHRvIGNvbXBlbnNhdGUgZm9yIGhlYWRlciByb3dcblxuXHRcdFx0X2lucHV0ID0gaW5wdXQ7XG5cdFx0XHRfcGFyc2VyID0gbmV3IFBhcnNlcihwYXJzZXJDb25maWcpO1xuXHRcdFx0X3Jlc3VsdHMgPSBfcGFyc2VyLnBhcnNlKF9pbnB1dCwgYmFzZUluZGV4LCBpZ25vcmVMYXN0Um93KTtcblx0XHRcdHByb2Nlc3NSZXN1bHRzKCk7XG5cdFx0XHRyZXR1cm4gX3BhdXNlZCA/IHsgbWV0YTogeyBwYXVzZWQ6IHRydWUgfSB9IDogKF9yZXN1bHRzIHx8IHsgbWV0YTogeyBwYXVzZWQ6IGZhbHNlIH0gfSk7XG5cdFx0fTtcblxuXHRcdHRoaXMucGF1c2VkID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHJldHVybiBfcGF1c2VkO1xuXHRcdH07XG5cblx0XHR0aGlzLnBhdXNlID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdF9wYXVzZWQgPSB0cnVlO1xuXHRcdFx0X3BhcnNlci5hYm9ydCgpO1xuXHRcdFx0X2lucHV0ID0gX2lucHV0LnN1YnN0cihfcGFyc2VyLmdldENoYXJJbmRleCgpKTtcblx0XHR9O1xuXG5cdFx0dGhpcy5yZXN1bWUgPSBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0X3BhdXNlZCA9IGZhbHNlO1xuXHRcdFx0c2VsZi5zdHJlYW1lci5wYXJzZUNodW5rKF9pbnB1dCk7XG5cdFx0fTtcblxuXHRcdHRoaXMuYWJvcnRlZCA9IGZ1bmN0aW9uICgpIHtcblx0XHRcdHJldHVybiBfYWJvcnRlZDtcblx0XHR9XG5cblx0XHR0aGlzLmFib3J0ID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdF9hYm9ydGVkID0gdHJ1ZTtcblx0XHRcdF9wYXJzZXIuYWJvcnQoKTtcblx0XHRcdF9yZXN1bHRzLm1ldGEuYWJvcnRlZCA9IHRydWU7XG5cdFx0XHRpZiAoaXNGdW5jdGlvbihfY29uZmlnLmNvbXBsZXRlKSlcblx0XHRcdFx0X2NvbmZpZy5jb21wbGV0ZShfcmVzdWx0cyk7XG5cdFx0XHRfaW5wdXQgPSBcIlwiO1xuXHRcdH07XG5cblx0XHRmdW5jdGlvbiBwcm9jZXNzUmVzdWx0cygpXG5cdFx0e1xuXHRcdFx0aWYgKF9yZXN1bHRzICYmIF9kZWxpbWl0ZXJFcnJvcilcblx0XHRcdHtcblx0XHRcdFx0YWRkRXJyb3IoXCJEZWxpbWl0ZXJcIiwgXCJVbmRldGVjdGFibGVEZWxpbWl0ZXJcIiwgXCJVbmFibGUgdG8gYXV0by1kZXRlY3QgZGVsaW1pdGluZyBjaGFyYWN0ZXI7IGRlZmF1bHRlZCB0byAnXCIrUGFwYS5EZWZhdWx0RGVsaW1pdGVyK1wiJ1wiKTtcblx0XHRcdFx0X2RlbGltaXRlckVycm9yID0gZmFsc2U7XG5cdFx0XHR9XG5cblx0XHRcdGlmIChfY29uZmlnLnNraXBFbXB0eUxpbmVzKVxuXHRcdFx0e1xuXHRcdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IF9yZXN1bHRzLmRhdGEubGVuZ3RoOyBpKyspXG5cdFx0XHRcdFx0aWYgKF9yZXN1bHRzLmRhdGFbaV0ubGVuZ3RoID09IDEgJiYgX3Jlc3VsdHMuZGF0YVtpXVswXSA9PSBcIlwiKVxuXHRcdFx0XHRcdFx0X3Jlc3VsdHMuZGF0YS5zcGxpY2UoaS0tLCAxKTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKG5lZWRzSGVhZGVyUm93KCkpXG5cdFx0XHRcdGZpbGxIZWFkZXJGaWVsZHMoKTtcblxuXHRcdFx0cmV0dXJuIGFwcGx5SGVhZGVyQW5kRHluYW1pY1R5cGluZygpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIG5lZWRzSGVhZGVyUm93KClcblx0XHR7XG5cdFx0XHRyZXR1cm4gX2NvbmZpZy5oZWFkZXIgJiYgX2ZpZWxkcy5sZW5ndGggPT0gMDtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBmaWxsSGVhZGVyRmllbGRzKClcblx0XHR7XG5cdFx0XHRpZiAoIV9yZXN1bHRzKVxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHRmb3IgKHZhciBpID0gMDsgbmVlZHNIZWFkZXJSb3coKSAmJiBpIDwgX3Jlc3VsdHMuZGF0YS5sZW5ndGg7IGkrKylcblx0XHRcdFx0Zm9yICh2YXIgaiA9IDA7IGogPCBfcmVzdWx0cy5kYXRhW2ldLmxlbmd0aDsgaisrKVxuXHRcdFx0XHRcdF9maWVsZHMucHVzaChfcmVzdWx0cy5kYXRhW2ldW2pdKTtcblx0XHRcdF9yZXN1bHRzLmRhdGEuc3BsaWNlKDAsIDEpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGFwcGx5SGVhZGVyQW5kRHluYW1pY1R5cGluZygpXG5cdFx0e1xuXHRcdFx0aWYgKCFfcmVzdWx0cyB8fCAoIV9jb25maWcuaGVhZGVyICYmICFfY29uZmlnLmR5bmFtaWNUeXBpbmcpKVxuXHRcdFx0XHRyZXR1cm4gX3Jlc3VsdHM7XG5cblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgX3Jlc3VsdHMuZGF0YS5sZW5ndGg7IGkrKylcblx0XHRcdHtcblx0XHRcdFx0dmFyIHJvdyA9IHt9O1xuXG5cdFx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgX3Jlc3VsdHMuZGF0YVtpXS5sZW5ndGg7IGorKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmIChfY29uZmlnLmR5bmFtaWNUeXBpbmcpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dmFyIHZhbHVlID0gX3Jlc3VsdHMuZGF0YVtpXVtqXTtcblx0XHRcdFx0XHRcdGlmICh2YWx1ZSA9PSBcInRydWVcIiB8fCB2YWx1ZSA9PSBcIlRSVUVcIilcblx0XHRcdFx0XHRcdFx0X3Jlc3VsdHMuZGF0YVtpXVtqXSA9IHRydWU7XG5cdFx0XHRcdFx0XHRlbHNlIGlmICh2YWx1ZSA9PSBcImZhbHNlXCIgfHwgdmFsdWUgPT0gXCJGQUxTRVwiKVxuXHRcdFx0XHRcdFx0XHRfcmVzdWx0cy5kYXRhW2ldW2pdID0gZmFsc2U7XG5cdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRcdF9yZXN1bHRzLmRhdGFbaV1bal0gPSB0cnlQYXJzZUZsb2F0KHZhbHVlKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRpZiAoX2NvbmZpZy5oZWFkZXIpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0aWYgKGogPj0gX2ZpZWxkcy5sZW5ndGgpXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdGlmICghcm93W1wiX19wYXJzZWRfZXh0cmFcIl0pXG5cdFx0XHRcdFx0XHRcdFx0cm93W1wiX19wYXJzZWRfZXh0cmFcIl0gPSBbXTtcblx0XHRcdFx0XHRcdFx0cm93W1wiX19wYXJzZWRfZXh0cmFcIl0ucHVzaChfcmVzdWx0cy5kYXRhW2ldW2pdKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdFx0cm93W19maWVsZHNbal1dID0gX3Jlc3VsdHMuZGF0YVtpXVtqXTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAoX2NvbmZpZy5oZWFkZXIpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRfcmVzdWx0cy5kYXRhW2ldID0gcm93O1xuXHRcdFx0XHRcdGlmIChqID4gX2ZpZWxkcy5sZW5ndGgpXG5cdFx0XHRcdFx0XHRhZGRFcnJvcihcIkZpZWxkTWlzbWF0Y2hcIiwgXCJUb29NYW55RmllbGRzXCIsIFwiVG9vIG1hbnkgZmllbGRzOiBleHBlY3RlZCBcIiArIF9maWVsZHMubGVuZ3RoICsgXCIgZmllbGRzIGJ1dCBwYXJzZWQgXCIgKyBqLCBpKTtcblx0XHRcdFx0XHRlbHNlIGlmIChqIDwgX2ZpZWxkcy5sZW5ndGgpXG5cdFx0XHRcdFx0XHRhZGRFcnJvcihcIkZpZWxkTWlzbWF0Y2hcIiwgXCJUb29GZXdGaWVsZHNcIiwgXCJUb28gZmV3IGZpZWxkczogZXhwZWN0ZWQgXCIgKyBfZmllbGRzLmxlbmd0aCArIFwiIGZpZWxkcyBidXQgcGFyc2VkIFwiICsgaiwgaSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0aWYgKF9jb25maWcuaGVhZGVyICYmIF9yZXN1bHRzLm1ldGEpXG5cdFx0XHRcdF9yZXN1bHRzLm1ldGEuZmllbGRzID0gX2ZpZWxkcztcblx0XHRcdHJldHVybiBfcmVzdWx0cztcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBndWVzc0RlbGltaXRlcihpbnB1dClcblx0XHR7XG5cdFx0XHR2YXIgZGVsaW1DaG9pY2VzID0gW1wiLFwiLCBcIlxcdFwiLCBcInxcIiwgXCI7XCIsIFBhcGEuUkVDT1JEX1NFUCwgUGFwYS5VTklUX1NFUF07XG5cdFx0XHR2YXIgYmVzdERlbGltLCBiZXN0RGVsdGEsIGZpZWxkQ291bnRQcmV2Um93O1xuXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGRlbGltQ2hvaWNlcy5sZW5ndGg7IGkrKylcblx0XHRcdHtcblx0XHRcdFx0dmFyIGRlbGltID0gZGVsaW1DaG9pY2VzW2ldO1xuXHRcdFx0XHR2YXIgZGVsdGEgPSAwLCBhdmdGaWVsZENvdW50ID0gMDtcblx0XHRcdFx0ZmllbGRDb3VudFByZXZSb3cgPSB1bmRlZmluZWQ7XG5cblx0XHRcdFx0dmFyIHByZXZpZXcgPSBuZXcgUGFyc2VyKHtcblx0XHRcdFx0XHRkZWxpbWl0ZXI6IGRlbGltLFxuXHRcdFx0XHRcdHByZXZpZXc6IDEwXG5cdFx0XHRcdH0pLnBhcnNlKGlucHV0KTtcblxuXHRcdFx0XHRmb3IgKHZhciBqID0gMDsgaiA8IHByZXZpZXcuZGF0YS5sZW5ndGg7IGorKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBmaWVsZENvdW50ID0gcHJldmlldy5kYXRhW2pdLmxlbmd0aDtcblx0XHRcdFx0XHRhdmdGaWVsZENvdW50ICs9IGZpZWxkQ291bnQ7XG5cblx0XHRcdFx0XHRpZiAodHlwZW9mIGZpZWxkQ291bnRQcmV2Um93ID09PSAndW5kZWZpbmVkJylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRmaWVsZENvdW50UHJldlJvdyA9IGZpZWxkQ291bnQ7XG5cdFx0XHRcdFx0XHRjb250aW51ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAoZmllbGRDb3VudCA+IDEpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0ZGVsdGEgKz0gTWF0aC5hYnMoZmllbGRDb3VudCAtIGZpZWxkQ291bnRQcmV2Um93KTtcblx0XHRcdFx0XHRcdGZpZWxkQ291bnRQcmV2Um93ID0gZmllbGRDb3VudDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZiAocHJldmlldy5kYXRhLmxlbmd0aCA+IDApXG5cdFx0XHRcdFx0YXZnRmllbGRDb3VudCAvPSBwcmV2aWV3LmRhdGEubGVuZ3RoO1xuXG5cdFx0XHRcdGlmICgodHlwZW9mIGJlc3REZWx0YSA9PT0gJ3VuZGVmaW5lZCcgfHwgZGVsdGEgPCBiZXN0RGVsdGEpXG5cdFx0XHRcdFx0JiYgYXZnRmllbGRDb3VudCA+IDEuOTkpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRiZXN0RGVsdGEgPSBkZWx0YTtcblx0XHRcdFx0XHRiZXN0RGVsaW0gPSBkZWxpbTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRfY29uZmlnLmRlbGltaXRlciA9IGJlc3REZWxpbTtcblxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0c3VjY2Vzc2Z1bDogISFiZXN0RGVsaW0sXG5cdFx0XHRcdGJlc3REZWxpbWl0ZXI6IGJlc3REZWxpbVxuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGd1ZXNzTGluZUVuZGluZ3MoaW5wdXQpXG5cdFx0e1xuXHRcdFx0aW5wdXQgPSBpbnB1dC5zdWJzdHIoMCwgMTAyNCoxMDI0KTtcdC8vIG1heCBsZW5ndGggMSBNQlxuXG5cdFx0XHR2YXIgciA9IGlucHV0LnNwbGl0KCdcXHInKTtcblxuXHRcdFx0aWYgKHIubGVuZ3RoID09IDEpXG5cdFx0XHRcdHJldHVybiAnXFxuJztcblxuXHRcdFx0dmFyIG51bVdpdGhOID0gMDtcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgci5sZW5ndGg7IGkrKylcblx0XHRcdHtcblx0XHRcdFx0aWYgKHJbaV1bMF0gPT0gJ1xcbicpXG5cdFx0XHRcdFx0bnVtV2l0aE4rKztcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIG51bVdpdGhOID49IHIubGVuZ3RoIC8gMiA/ICdcXHJcXG4nIDogJ1xccic7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gdHJ5UGFyc2VGbG9hdCh2YWwpXG5cdFx0e1xuXHRcdFx0dmFyIGlzTnVtYmVyID0gRkxPQVQudGVzdCh2YWwpO1xuXHRcdFx0cmV0dXJuIGlzTnVtYmVyID8gcGFyc2VGbG9hdCh2YWwpIDogdmFsO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGFkZEVycm9yKHR5cGUsIGNvZGUsIG1zZywgcm93KVxuXHRcdHtcblx0XHRcdF9yZXN1bHRzLmVycm9ycy5wdXNoKHtcblx0XHRcdFx0dHlwZTogdHlwZSxcblx0XHRcdFx0Y29kZTogY29kZSxcblx0XHRcdFx0bWVzc2FnZTogbXNnLFxuXHRcdFx0XHRyb3c6IHJvd1xuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XG5cblxuXG5cblxuXHQvKiogVGhlIGNvcmUgcGFyc2VyIGltcGxlbWVudHMgc3BlZWR5IGFuZCBjb3JyZWN0IENTViBwYXJzaW5nICovXG5cdGZ1bmN0aW9uIFBhcnNlcihjb25maWcpXG5cdHtcblx0XHQvLyBVbnBhY2sgdGhlIGNvbmZpZyBvYmplY3Rcblx0XHRjb25maWcgPSBjb25maWcgfHwge307XG5cdFx0dmFyIGRlbGltID0gY29uZmlnLmRlbGltaXRlcjtcblx0XHR2YXIgbmV3bGluZSA9IGNvbmZpZy5uZXdsaW5lO1xuXHRcdHZhciBjb21tZW50cyA9IGNvbmZpZy5jb21tZW50cztcblx0XHR2YXIgc3RlcCA9IGNvbmZpZy5zdGVwO1xuXHRcdHZhciBwcmV2aWV3ID0gY29uZmlnLnByZXZpZXc7XG5cdFx0dmFyIGZhc3RNb2RlID0gY29uZmlnLmZhc3RNb2RlO1xuXG5cdFx0Ly8gRGVsaW1pdGVyIG11c3QgYmUgdmFsaWRcblx0XHRpZiAodHlwZW9mIGRlbGltICE9PSAnc3RyaW5nJ1xuXHRcdFx0fHwgUGFwYS5CQURfREVMSU1JVEVSUy5pbmRleE9mKGRlbGltKSA+IC0xKVxuXHRcdFx0ZGVsaW0gPSBcIixcIjtcblxuXHRcdC8vIENvbW1lbnQgY2hhcmFjdGVyIG11c3QgYmUgdmFsaWRcblx0XHRpZiAoY29tbWVudHMgPT09IGRlbGltKVxuXHRcdFx0dGhyb3cgXCJDb21tZW50IGNoYXJhY3RlciBzYW1lIGFzIGRlbGltaXRlclwiO1xuXHRcdGVsc2UgaWYgKGNvbW1lbnRzID09PSB0cnVlKVxuXHRcdFx0Y29tbWVudHMgPSBcIiNcIjtcblx0XHRlbHNlIGlmICh0eXBlb2YgY29tbWVudHMgIT09ICdzdHJpbmcnXG5cdFx0XHR8fCBQYXBhLkJBRF9ERUxJTUlURVJTLmluZGV4T2YoY29tbWVudHMpID4gLTEpXG5cdFx0XHRjb21tZW50cyA9IGZhbHNlO1xuXG5cdFx0Ly8gTmV3bGluZSBtdXN0IGJlIHZhbGlkOiBcXHIsIFxcbiwgb3IgXFxyXFxuXG5cdFx0aWYgKG5ld2xpbmUgIT0gJ1xcbicgJiYgbmV3bGluZSAhPSAnXFxyJyAmJiBuZXdsaW5lICE9ICdcXHJcXG4nKVxuXHRcdFx0bmV3bGluZSA9ICdcXG4nO1xuXG5cdFx0Ly8gV2UncmUgZ29ubmEgbmVlZCB0aGVzZSBhdCB0aGUgUGFyc2VyIHNjb3BlXG5cdFx0dmFyIGN1cnNvciA9IDA7XG5cdFx0dmFyIGFib3J0ZWQgPSBmYWxzZTtcblxuXHRcdHRoaXMucGFyc2UgPSBmdW5jdGlvbihpbnB1dCwgYmFzZUluZGV4LCBpZ25vcmVMYXN0Um93KVxuXHRcdHtcblx0XHRcdC8vIEZvciBzb21lIHJlYXNvbiwgaW4gQ2hyb21lLCB0aGlzIHNwZWVkcyB0aGluZ3MgdXAgKCE/KVxuXHRcdFx0aWYgKHR5cGVvZiBpbnB1dCAhPT0gJ3N0cmluZycpXG5cdFx0XHRcdHRocm93IFwiSW5wdXQgbXVzdCBiZSBhIHN0cmluZ1wiO1xuXG5cdFx0XHQvLyBXZSBkb24ndCBuZWVkIHRvIGNvbXB1dGUgc29tZSBvZiB0aGVzZSBldmVyeSB0aW1lIHBhcnNlKCkgaXMgY2FsbGVkLFxuXHRcdFx0Ly8gYnV0IGhhdmluZyB0aGVtIGluIGEgbW9yZSBsb2NhbCBzY29wZSBzZWVtcyB0byBwZXJmb3JtIGJldHRlclxuXHRcdFx0dmFyIGlucHV0TGVuID0gaW5wdXQubGVuZ3RoLFxuXHRcdFx0XHRkZWxpbUxlbiA9IGRlbGltLmxlbmd0aCxcblx0XHRcdFx0bmV3bGluZUxlbiA9IG5ld2xpbmUubGVuZ3RoLFxuXHRcdFx0XHRjb21tZW50c0xlbiA9IGNvbW1lbnRzLmxlbmd0aDtcblx0XHRcdHZhciBzdGVwSXNGdW5jdGlvbiA9IHR5cGVvZiBzdGVwID09PSAnZnVuY3Rpb24nO1xuXG5cdFx0XHQvLyBFc3RhYmxpc2ggc3RhcnRpbmcgc3RhdGVcblx0XHRcdGN1cnNvciA9IDA7XG5cdFx0XHR2YXIgZGF0YSA9IFtdLCBlcnJvcnMgPSBbXSwgcm93ID0gW10sIGxhc3RDdXJzb3IgPSAwO1xuXG5cdFx0XHRpZiAoIWlucHV0KVxuXHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSgpO1xuXG5cdFx0XHRpZiAoZmFzdE1vZGUgfHwgKGZhc3RNb2RlICE9PSBmYWxzZSAmJiBpbnB1dC5pbmRleE9mKCdcIicpID09PSAtMSkpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciByb3dzID0gaW5wdXQuc3BsaXQobmV3bGluZSk7XG5cdFx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgcm93cy5sZW5ndGg7IGkrKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciByb3cgPSByb3dzW2ldO1xuXHRcdFx0XHRcdGN1cnNvciArPSByb3cubGVuZ3RoO1xuXHRcdFx0XHRcdGlmIChpICE9PSByb3dzLmxlbmd0aCAtIDEpXG5cdFx0XHRcdFx0XHRjdXJzb3IgKz0gbmV3bGluZS5sZW5ndGg7XG5cdFx0XHRcdFx0ZWxzZSBpZiAoaWdub3JlTGFzdFJvdylcblx0XHRcdFx0XHRcdHJldHVybiByZXR1cm5hYmxlKCk7XG5cdFx0XHRcdFx0aWYgKGNvbW1lbnRzICYmIHJvdy5zdWJzdHIoMCwgY29tbWVudHNMZW4pID09IGNvbW1lbnRzKVxuXHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0aWYgKHN0ZXBJc0Z1bmN0aW9uKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGRhdGEgPSBbXTtcblx0XHRcdFx0XHRcdHB1c2hSb3cocm93LnNwbGl0KGRlbGltKSk7XG5cdFx0XHRcdFx0XHRkb1N0ZXAoKTtcblx0XHRcdFx0XHRcdGlmIChhYm9ydGVkKVxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRwdXNoUm93KHJvdy5zcGxpdChkZWxpbSkpO1xuXHRcdFx0XHRcdGlmIChwcmV2aWV3ICYmIGkgPj0gcHJldmlldylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkYXRhID0gZGF0YS5zbGljZSgwLCBwcmV2aWV3KTtcblx0XHRcdFx0XHRcdHJldHVybiByZXR1cm5hYmxlKHRydWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSgpO1xuXHRcdFx0fVxuXG5cdFx0XHR2YXIgbmV4dERlbGltID0gaW5wdXQuaW5kZXhPZihkZWxpbSwgY3Vyc29yKTtcblx0XHRcdHZhciBuZXh0TmV3bGluZSA9IGlucHV0LmluZGV4T2YobmV3bGluZSwgY3Vyc29yKTtcblxuXHRcdFx0Ly8gUGFyc2VyIGxvb3Bcblx0XHRcdGZvciAoOzspXG5cdFx0XHR7XG5cdFx0XHRcdC8vIEZpZWxkIGhhcyBvcGVuaW5nIHF1b3RlXG5cdFx0XHRcdGlmIChpbnB1dFtjdXJzb3JdID09ICdcIicpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQvLyBTdGFydCBvdXIgc2VhcmNoIGZvciB0aGUgY2xvc2luZyBxdW90ZSB3aGVyZSB0aGUgY3Vyc29yIGlzXG5cdFx0XHRcdFx0dmFyIHF1b3RlU2VhcmNoID0gY3Vyc29yO1xuXG5cdFx0XHRcdFx0Ly8gU2tpcCB0aGUgb3BlbmluZyBxdW90ZVxuXHRcdFx0XHRcdGN1cnNvcisrO1xuXG5cdFx0XHRcdFx0Zm9yICg7Oylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQvLyBGaW5kIGNsb3NpbmcgcXVvdGVcblx0XHRcdFx0XHRcdHZhciBxdW90ZVNlYXJjaCA9IGlucHV0LmluZGV4T2YoJ1wiJywgcXVvdGVTZWFyY2grMSk7XG5cblx0XHRcdFx0XHRcdGlmIChxdW90ZVNlYXJjaCA9PT0gLTEpXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdGlmICghaWdub3JlTGFzdFJvdykge1xuXHRcdFx0XHRcdFx0XHRcdC8vIE5vIGNsb3NpbmcgcXVvdGUuLi4gd2hhdCBhIHBpdHlcblx0XHRcdFx0XHRcdFx0XHRlcnJvcnMucHVzaCh7XG5cdFx0XHRcdFx0XHRcdFx0XHR0eXBlOiBcIlF1b3Rlc1wiLFxuXHRcdFx0XHRcdFx0XHRcdFx0Y29kZTogXCJNaXNzaW5nUXVvdGVzXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRtZXNzYWdlOiBcIlF1b3RlZCBmaWVsZCB1bnRlcm1pbmF0ZWRcIixcblx0XHRcdFx0XHRcdFx0XHRcdHJvdzogZGF0YS5sZW5ndGgsXHQvLyByb3cgaGFzIHlldCB0byBiZSBpbnNlcnRlZFxuXHRcdFx0XHRcdFx0XHRcdFx0aW5kZXg6IGN1cnNvclxuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdHJldHVybiBmaW5pc2goKTtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0aWYgKHF1b3RlU2VhcmNoID09PSBpbnB1dExlbi0xKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHQvLyBDbG9zaW5nIHF1b3RlIGF0IEVPRlxuXHRcdFx0XHRcdFx0XHR2YXIgdmFsdWUgPSBpbnB1dC5zdWJzdHJpbmcoY3Vyc29yLCBxdW90ZVNlYXJjaCkucmVwbGFjZSgvXCJcIi9nLCAnXCInKTtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIGZpbmlzaCh2YWx1ZSk7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdC8vIElmIHRoaXMgcXVvdGUgaXMgZXNjYXBlZCwgaXQncyBwYXJ0IG9mIHRoZSBkYXRhOyBza2lwIGl0XG5cdFx0XHRcdFx0XHRpZiAoaW5wdXRbcXVvdGVTZWFyY2grMV0gPT0gJ1wiJylcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cXVvdGVTZWFyY2grKztcblx0XHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdGlmIChpbnB1dFtxdW90ZVNlYXJjaCsxXSA9PSBkZWxpbSlcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0Ly8gQ2xvc2luZyBxdW90ZSBmb2xsb3dlZCBieSBkZWxpbWl0ZXJcblx0XHRcdFx0XHRcdFx0cm93LnB1c2goaW5wdXQuc3Vic3RyaW5nKGN1cnNvciwgcXVvdGVTZWFyY2gpLnJlcGxhY2UoL1wiXCIvZywgJ1wiJykpO1xuXHRcdFx0XHRcdFx0XHRjdXJzb3IgPSBxdW90ZVNlYXJjaCArIDEgKyBkZWxpbUxlbjtcblx0XHRcdFx0XHRcdFx0bmV4dERlbGltID0gaW5wdXQuaW5kZXhPZihkZWxpbSwgY3Vyc29yKTtcblx0XHRcdFx0XHRcdFx0bmV4dE5ld2xpbmUgPSBpbnB1dC5pbmRleE9mKG5ld2xpbmUsIGN1cnNvcik7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZiAoaW5wdXQuc3Vic3RyKHF1b3RlU2VhcmNoKzEsIG5ld2xpbmVMZW4pID09PSBuZXdsaW5lKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHQvLyBDbG9zaW5nIHF1b3RlIGZvbGxvd2VkIGJ5IG5ld2xpbmVcblx0XHRcdFx0XHRcdFx0cm93LnB1c2goaW5wdXQuc3Vic3RyaW5nKGN1cnNvciwgcXVvdGVTZWFyY2gpLnJlcGxhY2UoL1wiXCIvZywgJ1wiJykpO1xuXHRcdFx0XHRcdFx0XHRzYXZlUm93KHF1b3RlU2VhcmNoICsgMSArIG5ld2xpbmVMZW4pO1xuXHRcdFx0XHRcdFx0XHRuZXh0RGVsaW0gPSBpbnB1dC5pbmRleE9mKGRlbGltLCBjdXJzb3IpO1x0Ly8gYmVjYXVzZSB3ZSBtYXkgaGF2ZSBza2lwcGVkIHRoZSBuZXh0RGVsaW0gaW4gdGhlIHF1b3RlZCBmaWVsZFxuXG5cdFx0XHRcdFx0XHRcdGlmIChzdGVwSXNGdW5jdGlvbilcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdGRvU3RlcCgpO1xuXHRcdFx0XHRcdFx0XHRcdGlmIChhYm9ydGVkKVxuXHRcdFx0XHRcdFx0XHRcdFx0cmV0dXJuIHJldHVybmFibGUoKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0aWYgKHByZXZpZXcgJiYgZGF0YS5sZW5ndGggPj0gcHJldmlldylcblx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSh0cnVlKTtcblxuXHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRjb250aW51ZTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIENvbW1lbnQgZm91bmQgYXQgc3RhcnQgb2YgbmV3IGxpbmVcblx0XHRcdFx0aWYgKGNvbW1lbnRzICYmIHJvdy5sZW5ndGggPT09IDAgJiYgaW5wdXQuc3Vic3RyKGN1cnNvciwgY29tbWVudHNMZW4pID09PSBjb21tZW50cylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmIChuZXh0TmV3bGluZSA9PSAtMSlcdC8vIENvbW1lbnQgZW5kcyBhdCBFT0Zcblx0XHRcdFx0XHRcdHJldHVybiByZXR1cm5hYmxlKCk7XG5cdFx0XHRcdFx0Y3Vyc29yID0gbmV4dE5ld2xpbmUgKyBuZXdsaW5lTGVuO1xuXHRcdFx0XHRcdG5leHROZXdsaW5lID0gaW5wdXQuaW5kZXhPZihuZXdsaW5lLCBjdXJzb3IpO1xuXHRcdFx0XHRcdG5leHREZWxpbSA9IGlucHV0LmluZGV4T2YoZGVsaW0sIGN1cnNvcik7XG5cdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBOZXh0IGRlbGltaXRlciBjb21lcyBiZWZvcmUgbmV4dCBuZXdsaW5lLCBzbyB3ZSd2ZSByZWFjaGVkIGVuZCBvZiBmaWVsZFxuXHRcdFx0XHRpZiAobmV4dERlbGltICE9PSAtMSAmJiAobmV4dERlbGltIDwgbmV4dE5ld2xpbmUgfHwgbmV4dE5ld2xpbmUgPT09IC0xKSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHJvdy5wdXNoKGlucHV0LnN1YnN0cmluZyhjdXJzb3IsIG5leHREZWxpbSkpO1xuXHRcdFx0XHRcdGN1cnNvciA9IG5leHREZWxpbSArIGRlbGltTGVuO1xuXHRcdFx0XHRcdG5leHREZWxpbSA9IGlucHV0LmluZGV4T2YoZGVsaW0sIGN1cnNvcik7XG5cdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBFbmQgb2Ygcm93XG5cdFx0XHRcdGlmIChuZXh0TmV3bGluZSAhPT0gLTEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRyb3cucHVzaChpbnB1dC5zdWJzdHJpbmcoY3Vyc29yLCBuZXh0TmV3bGluZSkpO1xuXHRcdFx0XHRcdHNhdmVSb3cobmV4dE5ld2xpbmUgKyBuZXdsaW5lTGVuKTtcblxuXHRcdFx0XHRcdGlmIChzdGVwSXNGdW5jdGlvbilcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRkb1N0ZXAoKTtcblx0XHRcdFx0XHRcdGlmIChhYm9ydGVkKVxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSgpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmIChwcmV2aWV3ICYmIGRhdGEubGVuZ3RoID49IHByZXZpZXcpXG5cdFx0XHRcdFx0XHRyZXR1cm4gcmV0dXJuYWJsZSh0cnVlKTtcblxuXHRcdFx0XHRcdGNvbnRpbnVlO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cblxuXHRcdFx0cmV0dXJuIGZpbmlzaCgpO1xuXG5cblx0XHRcdGZ1bmN0aW9uIHB1c2hSb3cocm93KVxuXHRcdFx0e1xuXHRcdFx0XHRkYXRhLnB1c2gocm93KTtcblx0XHRcdFx0bGFzdEN1cnNvciA9IGN1cnNvcjtcblx0XHRcdH1cblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBBcHBlbmRzIHRoZSByZW1haW5pbmcgaW5wdXQgZnJvbSBjdXJzb3IgdG8gdGhlIGVuZCBpbnRvXG5cdFx0XHQgKiByb3csIHNhdmVzIHRoZSByb3csIGNhbGxzIHN0ZXAsIGFuZCByZXR1cm5zIHRoZSByZXN1bHRzLlxuXHRcdFx0ICovXG5cdFx0XHRmdW5jdGlvbiBmaW5pc2godmFsdWUpXG5cdFx0XHR7XG5cdFx0XHRcdGlmIChpZ25vcmVMYXN0Um93KVxuXHRcdFx0XHRcdHJldHVybiByZXR1cm5hYmxlKCk7XG5cdFx0XHRcdGlmICh0eXBlb2YgdmFsdWUgPT09ICd1bmRlZmluZWQnKVxuXHRcdFx0XHRcdHZhbHVlID0gaW5wdXQuc3Vic3RyKGN1cnNvcik7XG5cdFx0XHRcdHJvdy5wdXNoKHZhbHVlKTtcblx0XHRcdFx0Y3Vyc29yID0gaW5wdXRMZW47XHQvLyBpbXBvcnRhbnQgaW4gY2FzZSBwYXJzaW5nIGlzIHBhdXNlZFxuXHRcdFx0XHRwdXNoUm93KHJvdyk7XG5cdFx0XHRcdGlmIChzdGVwSXNGdW5jdGlvbilcblx0XHRcdFx0XHRkb1N0ZXAoKTtcblx0XHRcdFx0cmV0dXJuIHJldHVybmFibGUoKTtcblx0XHRcdH1cblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBBcHBlbmRzIHRoZSBjdXJyZW50IHJvdyB0byB0aGUgcmVzdWx0cy4gSXQgc2V0cyB0aGUgY3Vyc29yXG5cdFx0XHQgKiB0byBuZXdDdXJzb3IgYW5kIGZpbmRzIHRoZSBuZXh0TmV3bGluZS4gVGhlIGNhbGxlciBzaG91bGRcblx0XHRcdCAqIHRha2UgY2FyZSB0byBleGVjdXRlIHVzZXIncyBzdGVwIGZ1bmN0aW9uIGFuZCBjaGVjayBmb3Jcblx0XHRcdCAqIHByZXZpZXcgYW5kIGVuZCBwYXJzaW5nIGlmIG5lY2Vzc2FyeS5cblx0XHRcdCAqL1xuXHRcdFx0ZnVuY3Rpb24gc2F2ZVJvdyhuZXdDdXJzb3IpXG5cdFx0XHR7XG5cdFx0XHRcdGN1cnNvciA9IG5ld0N1cnNvcjtcblx0XHRcdFx0cHVzaFJvdyhyb3cpO1xuXHRcdFx0XHRyb3cgPSBbXTtcblx0XHRcdFx0bmV4dE5ld2xpbmUgPSBpbnB1dC5pbmRleE9mKG5ld2xpbmUsIGN1cnNvcik7XG5cdFx0XHR9XG5cblx0XHRcdC8qKiBSZXR1cm5zIGFuIG9iamVjdCB3aXRoIHRoZSByZXN1bHRzLCBlcnJvcnMsIGFuZCBtZXRhLiAqL1xuXHRcdFx0ZnVuY3Rpb24gcmV0dXJuYWJsZShzdG9wcGVkKVxuXHRcdFx0e1xuXHRcdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRcdGRhdGE6IGRhdGEsXG5cdFx0XHRcdFx0ZXJyb3JzOiBlcnJvcnMsXG5cdFx0XHRcdFx0bWV0YToge1xuXHRcdFx0XHRcdFx0ZGVsaW1pdGVyOiBkZWxpbSxcblx0XHRcdFx0XHRcdGxpbmVicmVhazogbmV3bGluZSxcblx0XHRcdFx0XHRcdGFib3J0ZWQ6IGFib3J0ZWQsXG5cdFx0XHRcdFx0XHR0cnVuY2F0ZWQ6ICEhc3RvcHBlZCxcblx0XHRcdFx0XHRcdGN1cnNvcjogbGFzdEN1cnNvciArIChiYXNlSW5kZXggfHwgMClcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH07XG5cdFx0XHR9XG5cblx0XHRcdC8qKiBFeGVjdXRlcyB0aGUgdXNlcidzIHN0ZXAgZnVuY3Rpb24gYW5kIHJlc2V0cyBkYXRhICYgZXJyb3JzLiAqL1xuXHRcdFx0ZnVuY3Rpb24gZG9TdGVwKClcblx0XHRcdHtcblx0XHRcdFx0c3RlcChyZXR1cm5hYmxlKCkpO1xuXHRcdFx0XHRkYXRhID0gW10sIGVycm9ycyA9IFtdO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHQvKiogU2V0cyB0aGUgYWJvcnQgZmxhZyAqL1xuXHRcdHRoaXMuYWJvcnQgPSBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0YWJvcnRlZCA9IHRydWU7XG5cdFx0fTtcblxuXHRcdC8qKiBHZXRzIHRoZSBjdXJzb3IgcG9zaXRpb24gKi9cblx0XHR0aGlzLmdldENoYXJJbmRleCA9IGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRyZXR1cm4gY3Vyc29yO1xuXHRcdH07XG5cdH1cblxuXG5cdC8vIElmIHlvdSBuZWVkIHRvIGxvYWQgUGFwYSBQYXJzZSBhc3luY2hyb25vdXNseSBhbmQgeW91IGFsc28gbmVlZCB3b3JrZXIgdGhyZWFkcywgaGFyZC1jb2RlXG5cdC8vIHRoZSBzY3JpcHQgcGF0aCBoZXJlLiBTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS9taG9sdC9QYXBhUGFyc2UvaXNzdWVzLzg3I2lzc3VlY29tbWVudC01Nzg4NTM1OFxuXHRmdW5jdGlvbiBnZXRTY3JpcHRQYXRoKClcblx0e1xuXHRcdHZhciBzY3JpcHRzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpO1xuXHRcdHJldHVybiBzY3JpcHRzLmxlbmd0aCA/IHNjcmlwdHNbc2NyaXB0cy5sZW5ndGggLSAxXS5zcmMgOiAnJztcblx0fVxuXG5cdGZ1bmN0aW9uIG5ld1dvcmtlcigpXG5cdHtcblx0XHRpZiAoIVBhcGEuV09SS0VSU19TVVBQT1JURUQpXG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0aWYgKCFMT0FERURfU1lOQyAmJiBQYXBhLlNDUklQVF9QQVRIID09PSBudWxsKVxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKFxuXHRcdFx0XHQnU2NyaXB0IHBhdGggY2Fubm90IGJlIGRldGVybWluZWQgYXV0b21hdGljYWxseSB3aGVuIFBhcGEgUGFyc2UgaXMgbG9hZGVkIGFzeW5jaHJvbm91c2x5LiAnICtcblx0XHRcdFx0J1lvdSBuZWVkIHRvIHNldCBQYXBhLlNDUklQVF9QQVRIIG1hbnVhbGx5Lidcblx0XHRcdCk7XG5cdFx0dmFyIHdvcmtlclVybCA9IFBhcGEuU0NSSVBUX1BBVEggfHwgQVVUT19TQ1JJUFRfUEFUSDtcblx0XHQvLyBBcHBlbmQgXCJwYXBhd29ya2VyXCIgdG8gdGhlIHNlYXJjaCBzdHJpbmcgdG8gdGVsbCBwYXBhcGFyc2UgdGhhdCB0aGlzIGlzIG91ciB3b3JrZXIuXG5cdFx0d29ya2VyVXJsICs9ICh3b3JrZXJVcmwuaW5kZXhPZignPycpICE9PSAtMSA/ICcmJyA6ICc/JykgKyAncGFwYXdvcmtlcic7XG5cdFx0dmFyIHcgPSBuZXcgZ2xvYmFsLldvcmtlcih3b3JrZXJVcmwpO1xuXHRcdHcub25tZXNzYWdlID0gbWFpblRocmVhZFJlY2VpdmVkTWVzc2FnZTtcblx0XHR3LmlkID0gd29ya2VySWRDb3VudGVyKys7XG5cdFx0d29ya2Vyc1t3LmlkXSA9IHc7XG5cdFx0cmV0dXJuIHc7XG5cdH1cblxuXHQvKiogQ2FsbGJhY2sgd2hlbiBtYWluIHRocmVhZCByZWNlaXZlcyBhIG1lc3NhZ2UgKi9cblx0ZnVuY3Rpb24gbWFpblRocmVhZFJlY2VpdmVkTWVzc2FnZShlKVxuXHR7XG5cdFx0dmFyIG1zZyA9IGUuZGF0YTtcblx0XHR2YXIgd29ya2VyID0gd29ya2Vyc1ttc2cud29ya2VySWRdO1xuXHRcdHZhciBhYm9ydGVkID0gZmFsc2U7XG5cblx0XHRpZiAobXNnLmVycm9yKVxuXHRcdFx0d29ya2VyLnVzZXJFcnJvcihtc2cuZXJyb3IsIG1zZy5maWxlKTtcblx0XHRlbHNlIGlmIChtc2cucmVzdWx0cyAmJiBtc2cucmVzdWx0cy5kYXRhKVxuXHRcdHtcblx0XHRcdHZhciBhYm9ydCA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRhYm9ydGVkID0gdHJ1ZTtcblx0XHRcdFx0Y29tcGxldGVXb3JrZXIobXNnLndvcmtlcklkLCB7IGRhdGE6IFtdLCBlcnJvcnM6IFtdLCBtZXRhOiB7IGFib3J0ZWQ6IHRydWUgfSB9KTtcblx0XHRcdH07XG5cblx0XHRcdHZhciBoYW5kbGUgPSB7XG5cdFx0XHRcdGFib3J0OiBhYm9ydCxcblx0XHRcdFx0cGF1c2U6IG5vdEltcGxlbWVudGVkLFxuXHRcdFx0XHRyZXN1bWU6IG5vdEltcGxlbWVudGVkXG5cdFx0XHR9O1xuXG5cdFx0XHRpZiAoaXNGdW5jdGlvbih3b3JrZXIudXNlclN0ZXApKVxuXHRcdFx0e1xuXHRcdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IG1zZy5yZXN1bHRzLmRhdGEubGVuZ3RoOyBpKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR3b3JrZXIudXNlclN0ZXAoe1xuXHRcdFx0XHRcdFx0ZGF0YTogW21zZy5yZXN1bHRzLmRhdGFbaV1dLFxuXHRcdFx0XHRcdFx0ZXJyb3JzOiBtc2cucmVzdWx0cy5lcnJvcnMsXG5cdFx0XHRcdFx0XHRtZXRhOiBtc2cucmVzdWx0cy5tZXRhXG5cdFx0XHRcdFx0fSwgaGFuZGxlKTtcblx0XHRcdFx0XHRpZiAoYWJvcnRlZClcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGRlbGV0ZSBtc2cucmVzdWx0cztcdC8vIGZyZWUgbWVtb3J5IEFTQVBcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKGlzRnVuY3Rpb24od29ya2VyLnVzZXJDaHVuaykpXG5cdFx0XHR7XG5cdFx0XHRcdHdvcmtlci51c2VyQ2h1bmsobXNnLnJlc3VsdHMsIGhhbmRsZSwgbXNnLmZpbGUpO1xuXHRcdFx0XHRkZWxldGUgbXNnLnJlc3VsdHM7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0aWYgKG1zZy5maW5pc2hlZCAmJiAhYWJvcnRlZClcblx0XHRcdGNvbXBsZXRlV29ya2VyKG1zZy53b3JrZXJJZCwgbXNnLnJlc3VsdHMpO1xuXHR9XG5cblx0ZnVuY3Rpb24gY29tcGxldGVXb3JrZXIod29ya2VySWQsIHJlc3VsdHMpIHtcblx0XHR2YXIgd29ya2VyID0gd29ya2Vyc1t3b3JrZXJJZF07XG5cdFx0aWYgKGlzRnVuY3Rpb24od29ya2VyLnVzZXJDb21wbGV0ZSkpXG5cdFx0XHR3b3JrZXIudXNlckNvbXBsZXRlKHJlc3VsdHMpO1xuXHRcdHdvcmtlci50ZXJtaW5hdGUoKTtcblx0XHRkZWxldGUgd29ya2Vyc1t3b3JrZXJJZF07XG5cdH1cblxuXHRmdW5jdGlvbiBub3RJbXBsZW1lbnRlZCgpIHtcblx0XHR0aHJvdyBcIk5vdCBpbXBsZW1lbnRlZC5cIjtcblx0fVxuXG5cdC8qKiBDYWxsYmFjayB3aGVuIHdvcmtlciB0aHJlYWQgcmVjZWl2ZXMgYSBtZXNzYWdlICovXG5cdGZ1bmN0aW9uIHdvcmtlclRocmVhZFJlY2VpdmVkTWVzc2FnZShlKVxuXHR7XG5cdFx0dmFyIG1zZyA9IGUuZGF0YTtcblxuXHRcdGlmICh0eXBlb2YgUGFwYS5XT1JLRVJfSUQgPT09ICd1bmRlZmluZWQnICYmIG1zZylcblx0XHRcdFBhcGEuV09SS0VSX0lEID0gbXNnLndvcmtlcklkO1xuXG5cdFx0aWYgKHR5cGVvZiBtc2cuaW5wdXQgPT09ICdzdHJpbmcnKVxuXHRcdHtcblx0XHRcdGdsb2JhbC5wb3N0TWVzc2FnZSh7XG5cdFx0XHRcdHdvcmtlcklkOiBQYXBhLldPUktFUl9JRCxcblx0XHRcdFx0cmVzdWx0czogUGFwYS5wYXJzZShtc2cuaW5wdXQsIG1zZy5jb25maWcpLFxuXHRcdFx0XHRmaW5pc2hlZDogdHJ1ZVxuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdGVsc2UgaWYgKChnbG9iYWwuRmlsZSAmJiBtc2cuaW5wdXQgaW5zdGFuY2VvZiBGaWxlKSB8fCBtc2cuaW5wdXQgaW5zdGFuY2VvZiBPYmplY3QpXHQvLyB0aGFuayB5b3UsIFNhZmFyaSAoc2VlIGlzc3VlICMxMDYpXG5cdFx0e1xuXHRcdFx0dmFyIHJlc3VsdHMgPSBQYXBhLnBhcnNlKG1zZy5pbnB1dCwgbXNnLmNvbmZpZyk7XG5cdFx0XHRpZiAocmVzdWx0cylcblx0XHRcdFx0Z2xvYmFsLnBvc3RNZXNzYWdlKHtcblx0XHRcdFx0XHR3b3JrZXJJZDogUGFwYS5XT1JLRVJfSUQsXG5cdFx0XHRcdFx0cmVzdWx0czogcmVzdWx0cyxcblx0XHRcdFx0XHRmaW5pc2hlZDogdHJ1ZVxuXHRcdFx0XHR9KTtcblx0XHR9XG5cdH1cblxuXHQvKiogTWFrZXMgYSBkZWVwIGNvcHkgb2YgYW4gYXJyYXkgb3Igb2JqZWN0IChtb3N0bHkpICovXG5cdGZ1bmN0aW9uIGNvcHkob2JqKVxuXHR7XG5cdFx0aWYgKHR5cGVvZiBvYmogIT09ICdvYmplY3QnKVxuXHRcdFx0cmV0dXJuIG9iajtcblx0XHR2YXIgY3B5ID0gb2JqIGluc3RhbmNlb2YgQXJyYXkgPyBbXSA6IHt9O1xuXHRcdGZvciAodmFyIGtleSBpbiBvYmopXG5cdFx0XHRjcHlba2V5XSA9IGNvcHkob2JqW2tleV0pO1xuXHRcdHJldHVybiBjcHk7XG5cdH1cblxuXHRmdW5jdGlvbiBiaW5kRnVuY3Rpb24oZiwgc2VsZilcblx0e1xuXHRcdHJldHVybiBmdW5jdGlvbigpIHsgZi5hcHBseShzZWxmLCBhcmd1bWVudHMpOyB9O1xuXHR9XG5cblx0ZnVuY3Rpb24gaXNGdW5jdGlvbihmdW5jKVxuXHR7XG5cdFx0cmV0dXJuIHR5cGVvZiBmdW5jID09PSAnZnVuY3Rpb24nO1xuXHR9XG59KSh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyA/IHdpbmRvdyA6IHRoaXMpO1xuIl19
